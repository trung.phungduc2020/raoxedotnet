﻿using Microsoft.AspNetCore.Http;
using raoVatApi.Common;
using raoVatApi.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public class AdminBll
    {
        private HttpRequest _request;

        public HttpRequest Request
        {
            get
            {
                {
                    try
                    {
                        HttpContextAccessor accessor = new HttpContextAccessor();
                        _request = accessor.HttpContext.Request;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        CommonMethods.WriteLog(error);
                    }
                }

                return _request;
            }
            set
            {
                _request = value;
            }
        }

        private RaoVatContext_Custom _context;

        public RaoVatContext_Custom Context
        {
            get
            {
                return new RaoVatContext_Custom();
            }
        }

        private RawSqlRepository _rawSQLRepo;

        public RawSqlRepository RawSQLRepo
        {
            get
            {
                return new RawSqlRepository(new RaoVatContext_Custom());
            }
            set
            {
                _rawSQLRepo = value;
            }
        }

        public AdminBll()
        {
            _context = new RaoVatContext_Custom();
        }

        #region "Commons"

        private bool CheckListIntIsAllowSearch(List<int> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1)
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckListIntIsAllowSearch(List<long> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1)
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

    }
}