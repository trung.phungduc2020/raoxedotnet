﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatHistoryApi.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public class InterfaceBLL
    {
        private HttpRequest _request;

        public HttpRequest Request
        {
            get
            {
                {
                    HttpContextAccessor accessor = new HttpContextAccessor();
                    _request = accessor.HttpContext.Request;
                }
                return _request;
            }
            set
            {
                _request = value;
            }
        }

        private RaoVatContext_Custom _context;

        public RaoVatContext_Custom Context
        {
            get
            {
                {
                    _context = new RaoVatContext_Custom();
                }

                return _context;
            }
        }

        private RawSqlRepository _rawSQLRepo;

        public RawSqlRepository RawSQLRepo
        {
            get
            {
                {
                    _rawSQLRepo = new RawSqlRepository(new RaoVatContext_Custom());
                }
                return _rawSQLRepo;
            }
            set
            {
                _rawSQLRepo = value;
            }
        }

        private AdminBll _adminBLL;
        public AdminBll AdminBLL
        {
            get
            {
                {
                    _adminBLL = new AdminBll();
                }
                return _adminBLL;
            }
            set
            {
                _adminBLL = value;
            }
        }

        private List<int> _lstTrangThai_1 = new List<int>() { 1 };

        public InterfaceBLL(HttpRequest request)
        {
            _context = new RaoVatContext_Custom();
            Request = request;
            _adminBLL = new AdminBll();
        }

        #region "Commons"

        private bool CheckListLongIsAllowSearch(List<long> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1) // nguyencuongcs 20171003: Nếu truyền vào danh sách rỗng ( != null ) thì có nghĩa là muốn tìm theo điều kiện này
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckListIntIsAllowSearch(List<int> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1) // nguyencuongcs 20171003: Nếu truyền vào danh sách rỗng ( != null ) thì có nghĩa là muốn tìm theo điều kiện này
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckContainInStringArray(string strArrayToCheck, List<int> lstValue, bool fullCheckValue)
        {
            bool res = false;

            List<int> lstSourceToCheck = CommonMethods.SplitStringToInt(strArrayToCheck, false);
            foreach (int val in lstValue)
            {
                if (lstSourceToCheck.Contains(val))
                {
                    res = true;

                    if (!fullCheckValue)
                    {
                        return res;
                    }
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        #endregion
    }
}
