using raoVatApi.Models;
using raoVatApi.Models.Base;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public interface IEntityBaseRepositoryHistoryRaoVat<T> : IEntityBaseRepository<T> where T : class, IEntityBase
    {

    }
}