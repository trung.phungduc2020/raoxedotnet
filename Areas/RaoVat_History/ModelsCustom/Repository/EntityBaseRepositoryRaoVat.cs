﻿using Microsoft.AspNetCore.Http;
using raoVatApi.Models;
using raoVatApi.Models.Base;
using System.Threading.Tasks;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public class EntityBaseRepositoryHistoryRaoVat<T> : EntityBaseRepository<T>, IEntityBaseRepositoryHistoryRaoVat<T> where T : class, IEntityBase
    {
        public EntityBaseRepositoryHistoryRaoVat() : base(null)
        {
            Init();
        }

        public EntityBaseRepositoryHistoryRaoVat(HttpRequest request) : base(request)
        {
            Init();
        }

        public EntityBaseRepositoryHistoryRaoVat(HttpRequest request, string connectionString) : base(request)
        {
            Init(connectionString);
        }

        private void Init(string connectionString = "")
        {
            connectionDb = "raovat_history";
            if (!string.IsNullOrEmpty(connectionString))
            {
                // Run after base()
                Context = new RaoVatContext_Custom(connectionString);
                Entities = Context.Set<T>();
            }
            else
            {
                connectionString = Variables.ConnectionStringPostgresql_RaoVat_History;
                Context = new RaoVatContext_Custom(connectionString);
                Entities = Context.Set<T>();
            }
        }

        public EntityBaseRepositoryHistoryRaoVat(RaoVatContext_Custom context, HttpRequest request) : base(context, request)
        {
        }

        //public override async Task ClearPool()
        //{
        //    (Context as RaoVatContext_Custom).ClearPool();
        //}
    }
}