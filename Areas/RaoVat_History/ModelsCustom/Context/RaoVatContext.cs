using Microsoft.EntityFrameworkCore;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class Context : DbContext
    {
        public Context() : base()
        {

        }

        public Context(DbContextOptions options) : base(options)
        {
            // Using to inject connection from Startup.cs            
        }        
    }
}