using GmailApi;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using System;

namespace raoVatHistoryApi.ModelsRaoVat
{
    // Adding missing constructors to default RaoVatContext
    public class RaoVatContext_Custom : Context
    {
        private string _connectionString;
        public RaoVatContext_Custom() : base()
        {
            _connectionString = Variables.ConnectionStringPostgresql_RaoVat_History;
        }

        public RaoVatContext_Custom(string connectionString) : base()
        {
            _connectionString = connectionString;
        }

        public RaoVatContext_Custom(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                optionsBuilder.UseNpgsql(_connectionString);
            }
        }

        public void ClearPool()
        {
            GmailApiCore gac = new GmailApiCore();
            MailService ms = new MailService();
            try
            {
                CommonMethods.WriteLog("Action History: ClearAllPools");
                NpgsqlConnection.ClearAllPools();
                //NpgsqlConnection.ClearPool((NpgsqlConnection)Database.GetDbConnection());

                // Post Hangout
                gac.PostThongBaoHeThong_ClearPool("Th�nh c�ng");

                // Send mail
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("RaoVatContext_Custom - Success: ClearPool ", "", email);
                }
            }
            catch (Exception ex)
            {
                string error = " Error History ClearPool-" + ex.ToString();
                CommonMethods.WriteLog(error);
                gac.PostThongBaoHeThong_ClearPool(error);
            }
        }

        public void KillConnection()
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    string query = CommonParams.postgresCallFunctionKillConnectionstring;
                    NpgsqlCommand command = new NpgsqlCommand(query, connection);
                    var reader = (Int32)command.ExecuteScalar();
                    CommonMethods.WriteLog("Action: KillConnection - " + reader);
                    if (reader > 60)
                    {
                        ClearPool();
                    }
                }
                catch (Exception ex)
                {
                    CommonMethods.WriteLog("Error History KillConnection:" + ex.ToString());
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }
    }
}