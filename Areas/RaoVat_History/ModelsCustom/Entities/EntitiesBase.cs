using raoVatApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryTimePurchased : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryPoint : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryProfile : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryDangTinView : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryDangTinFavorite : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryDangTinLike : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class HistoryDangTinDuyet : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }
    
}