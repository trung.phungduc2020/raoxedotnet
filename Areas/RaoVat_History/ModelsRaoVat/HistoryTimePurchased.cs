﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryTimePurchased
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? ThoiGianDangTinDaMua { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime? NgayTao { get; set; }
        public string GhiChu { get; set; }
        public int? Point { get; set; }
        public int? Rate { get; set; }
    }
}
