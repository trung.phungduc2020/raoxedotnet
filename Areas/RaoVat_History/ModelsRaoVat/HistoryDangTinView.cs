﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryDangTinView
    {
        public long Id { get; set; }
        public long? IdDangTin { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdThanhVien { get; set; }
        public int? Type { get; set; }
        public string DeviceId { get; set; }
    }
}
