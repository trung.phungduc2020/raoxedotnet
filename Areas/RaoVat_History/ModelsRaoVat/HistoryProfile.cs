﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryProfile
    {
        public long Id { get; set; }
        public long? IdUser { get; set; }
        public string ListJson { get; set; }
        public string Note { get; set; }
        public DateTime? NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
    }
}
