﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryDangTinFavorite
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? IdDangTin { get; set; }
        public int? TrangThai { get; set; }
        public bool? IsFavorite { get; set; }
        public string IdDevice { get; set; }
        public DateTime NgayTao { get; set; }
    }
}
