﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class Context : DbContext
    {
        public virtual DbSet<HistoryDangTinDuyet> HistoryDangTinDuyet { get; set; }
        public virtual DbSet<HistoryDangTinFavorite> HistoryDangTinFavorite { get; set; }
        public virtual DbSet<HistoryDangTinLike> HistoryDangTinLike { get; set; }
        public virtual DbSet<HistoryDangTinView> HistoryDangTinView { get; set; }
        public virtual DbSet<HistoryPoint> HistoryPoint { get; set; }
        public virtual DbSet<HistoryProfile> HistoryProfile { get; set; }
        public virtual DbSet<HistoryTimePurchased> HistoryTimePurchased { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=171.244.35.196;Port=30119;Database=raovat_history;Username=postgres;Password=D@ilyxe2019!@@");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<HistoryDangTinDuyet>(entity =>
            {
                entity.Property(e => e.NewListJson).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OldListJson).HasColumnType("character varying");
            });

            modelBuilder.Entity<HistoryDangTinFavorite>(entity =>
            {
                entity.Property(e => e.IdDevice).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryDangTinLike>(entity =>
            {
                entity.Property(e => e.IdDevice).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryDangTinView>(entity =>
            {
                entity.Property(e => e.DeviceId).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryPoint>(entity =>
            {
                entity.Property(e => e.ErrorCode).HasColumnType("character varying");

                entity.Property(e => e.ExtraData).HasColumnType("character varying");

                entity.Property(e => e.MarketingCode).HasColumnType("character varying");

                entity.Property(e => e.MaxValue).HasColumnType("character varying");

                entity.Property(e => e.Message).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).HasColumnType("character varying");

                entity.Property(e => e.OrderId).HasColumnType("character varying");

                entity.Property(e => e.OrderType).HasColumnType("character varying");

                entity.Property(e => e.PayType).HasColumnType("character varying");

                entity.Property(e => e.Price).HasColumnType("numeric");

                entity.Property(e => e.RankPercentDiscount).HasColumnType("numeric");

                entity.Property(e => e.RequestId).HasColumnType("character varying");

                entity.Property(e => e.ResponseTime).HasColumnType("character varying");

                entity.Property(e => e.Signature).HasColumnType("character varying");

                entity.Property(e => e.TransId).HasColumnType("character varying");

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .HasColumnType("character varying");

                entity.Property(e => e.Value).HasColumnType("character varying");
            });

            modelBuilder.Entity<HistoryProfile>(entity =>
            {
                entity.Property(e => e.ListJson).HasColumnType("json");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).HasColumnType("character varying");
            });

            modelBuilder.Entity<HistoryTimePurchased>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
        }
    }
}
