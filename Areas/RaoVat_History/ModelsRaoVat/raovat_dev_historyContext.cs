﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class Context : DbContext
    {
        public virtual DbSet<HistoryDangTinDuyet> HistoryDangTinDuyet { get; set; }
        public virtual DbSet<HistoryDangTinFavorite> HistoryDangTinFavorite { get; set; }
        public virtual DbSet<HistoryDangTinLike> HistoryDangTinLike { get; set; }
        public virtual DbSet<HistoryDangTinView> HistoryDangTinView { get; set; }
        public virtual DbSet<HistoryPoint> HistoryPoint { get; set; }
        public virtual DbSet<HistoryProfile> HistoryProfile { get; set; }
        public virtual DbSet<HistoryTimePurchased> HistoryTimePurchased { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=210.211.116.16;Port=5432;Database=raovat_dev_history;Username=postgres;Password=@dlx2019$");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<HistoryDangTinDuyet>(entity =>
            {
                entity.Property(e => e.NewListJson).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OldListJson).HasColumnType("character varying");
            });

            modelBuilder.Entity<HistoryDangTinFavorite>(entity =>
            {
                entity.Property(e => e.IdDevice).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryDangTinLike>(entity =>
            {
                entity.Property(e => e.IdDevice).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryDangTinView>(entity =>
            {
                entity.Property(e => e.DeviceId).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<HistoryPoint>(entity =>
            {
                entity.Property(e => e.Note).HasColumnType("character varying");

                entity.Property(e => e.Price).HasColumnType("numeric");

                entity.Property(e => e.RankPercentDiscount).HasColumnType("numeric");
            });

            modelBuilder.Entity<HistoryProfile>(entity =>
            {
                entity.Property(e => e.ListJson).HasColumnType("json");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).HasColumnType("character varying");
            });

            modelBuilder.Entity<HistoryTimePurchased>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
        }
    }
}
