﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryPoint
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? Point { get; set; }
        public int? Type { get; set; }
        public int? Rate { get; set; }
        public string Note { get; set; }
        public long? IdAminTao { get; set; }
        public DateTime? NgayTao { get; set; }
        public long? IdRank { get; set; }
        public decimal? RankPercentDiscount { get; set; }
        public decimal? Price { get; set; }
        public long? IdMarketingPlan { get; set; }
        public string MarketingCode { get; set; }
        public string Value { get; set; }
        public int? ValueType { get; set; }
        public string MaxValue { get; set; }
        public long? IdMarketingCode { get; set; }
        public string OrderId { get; set; }
        public string Message { get; set; }
        public string ExtraData { get; set; }
        public string Signature { get; set; }
        public string ErrorCode { get; set; }
        public string OrderType { get; set; }
        public string PayType { get; set; }
        public string ResponseTime { get; set; }
        public string TransId { get; set; }
        public string Uuid { get; set; }
        public string RequestId { get; set; }
    }
}
