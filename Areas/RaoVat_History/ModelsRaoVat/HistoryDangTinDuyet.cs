﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryDangTinDuyet
    {
        public long Id { get; set; }
        public long? IdAdmin { get; set; }
        public DateTime NgayTao { get; set; }
        public string OldListJson { get; set; }
        public string NewListJson { get; set; }
    }
}
