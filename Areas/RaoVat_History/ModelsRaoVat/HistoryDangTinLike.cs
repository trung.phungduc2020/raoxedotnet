﻿using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.ModelsRaoVat
{
    public partial class HistoryDangTinLike
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? IdDangTin { get; set; }
        public bool? IsLike { get; set; }
        public string IdDevice { get; set; }
        public DateTime NgayTao { get; set; }
        public int? TrangThai { get; set; }
    }
}
