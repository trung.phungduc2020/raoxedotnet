﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using raoVatApi.Common;
using raoVatHistoryApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace raoVatApi.Areas.RaoVat_History.Controllers
{
    [Authorize]
    [Route(Variables.C_HistoryApiRouting_2)]
    public class InteractDataController : Controller
    {
        private InterfaceBLL _bll;
        private IDistributedCache _cache;
        private AdminBll _adminBll;

        public InteractDataController(IDistributedCache cache)
        {
            this._cache = cache;
            this._bll = new InterfaceBLL(Request);
            this._adminBll = new AdminBll();
        }

        #region "HistoryProfile"

        [HttpPost]
        public async Task<IActionResult> PostHistoryProfile()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string changePass = string.Empty;
                string changePhone = string.Empty;
                string changeEmail = string.Empty;
                string removeAccount = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    changePass = CommonMethods.ConvertToString(queryParams[CommonParams.changePass]);
                    changePhone = CommonMethods.ConvertToString(queryParams[CommonParams.changePhone]);
                    removeAccount = CommonMethods.ConvertToString(queryParams[CommonParams.removeAccount]);
                    changeEmail = CommonMethods.ConvertToString(queryParams[CommonParams.changeEmail]);

                }
                bool? changePassBool = CommonMethods.ConvertToBoolean(changePass, null);
                bool? changePhoneBool = CommonMethods.ConvertToBoolean(changePhone, null);
                bool? changeEmailBool = CommonMethods.ConvertToBoolean(changeEmail, null);
                bool? removeAccountBool = CommonMethods.ConvertToBoolean(removeAccount, null);

                string jsonInfo = await Request.GetRawBodyStringAsync();
                if (jsonInfo == null || !CommonMethods.IsValidJson(jsonInfo))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    HistoryProfile insEntity = new HistoryProfile();
                    insEntity.IdUser = Request.GetTokenIdThanhVien();
                    insEntity.ListJson = jsonInfo;

                    if (changePassBool == true)
                    {
                        insEntity.Note = "Change pass";
                    }
                    if (changePhoneBool == true)
                    {
                        insEntity.Note = "Change Phone";
                    }
                    if (changeEmailBool == true)
                    {
                        insEntity.Note = "Change Email";
                    }
                    if (removeAccountBool == true)
                    {
                        insEntity.Note = "Remove Account";
                    }

                    EntityBaseRepositoryHistoryRaoVat<HistoryProfile> repoProfileHistory = new EntityBaseRepositoryHistoryRaoVat<HistoryProfile>();
                    cusRes.DataResult = await repoProfileHistory.InsertReturnEntity(insEntity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion
    }
}