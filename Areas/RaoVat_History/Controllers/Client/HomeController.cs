using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatHistoryApi.ModelsRaoVat;
using System.Threading.Tasks;

namespace raoVatHistoryApi.ControllersRaoVat
{
    [Route("RaoVat/")]
    //[Route("[controller]")]
    public class HomeController : Controller
    {

        private IMemoryCache _cache;        
        private InterfaceBLL _bll;        

        public HomeController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._bll = new InterfaceBLL(Request);            
        }
        
        public IActionResult Index()
        {
            //return RedirectToAction("Index", "Read");            
            //return View("RaoVat/Views/Home/Index.cshtml");
            return View("Index");
        }

        [HttpGet("[action]")]
        public IActionResult Read()
        {
            return View("RaoVat/Views/Home/Read.cshtml");
        }

        [HttpGet("[action]")]
        public IActionResult RenderComponent(string pathLoad)
        {
            return PartialView(pathLoad);
        }

        //[HttpGet("([a-z]{2,4})")]
        //public IActionResult Reg()
        //{
        //    return View("RaoVat/Views/Home/Read.cshtml");
        //}
    }
}