﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using raoVatApi.Common;
using raoVatHistoryApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace raoVatApi.Areas.RaoVat_History.Controllers
{
    [AllowAnonymous]
    [Route(Variables.C_HistoryApiRouting_2)]
    public class InterfaceDataController : Controller
    {
        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> _repoHistoryDangTinView;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> RepoHistoryDangTinView { get => _repoHistoryDangTinView == null ? _repoHistoryDangTinView = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView>() : _repoHistoryDangTinView; set => _repoHistoryDangTinView = value; }

        private InterfaceBLL _bll;
        private IDistributedCache _cache;
        private AdminBll _adminBll;

        public InterfaceDataController(IDistributedCache cache)
        {
            this._cache = cache;
            this._bll = new InterfaceBLL(Request);
            this._adminBll = new AdminBll();
        }

        #region "HistoryDangTinView"
        [HttpPost]
        public async Task<IActionResult> PostDangTinView([FromBody]HistoryDangTinView entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();
                    entity.IdThanhVien = idThanhVien;
                    cusRes.DataResult = await RepoHistoryDangTinView.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion
    }
}