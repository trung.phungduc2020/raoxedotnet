﻿using raoVatApi.Common;
using System;
using System.Collections.Generic;

namespace raoVatHistoryApi.Common
{
    public static class ViewDataParams
    {
        public static string dataSEOHead = "dataSEOHead";
        public static string idSEO = "idSEO";
        public static string interfaceBLL = "interfaceBLL";
        public static string requireCss = "requireCss";
        public static string requireJs = "requireJs";
        public static string data = "data";
        public static string componentPathLoad = "componentPathLoad";
        public static string apiController = "apiController";
        public static string apiAction = "apiAction";
        public static string apiParams = "apiParams";
        public static string arrMappingProp = "arrMappingProp";
        public static string mappingProperties = "mappingProperties";
        public static string compData = "compData";
        public static string newComponentData = "newComponentData";
        public static string page = "page";
        public static string title = "title";
        public static string requestId = "requestId";
        public static string dataMenu = "dataMenu";
        public static string dataTinTuc = "dataTinTuc";
        public static string idSeo = "idSeo";
        public static string systemName = "systemName";
        public static string isCreateFileSeo = "isCreateFileSeo";
        public static string dataBaiViet = "dataBaiViet";
        public static string googleMapApiKey = "googleMapApiKey";
        public static string googleMaps = "googleMaps";
        public static string googleMaps_Lat = "googleMaps_Lat";
        public static string googleMaps_Lng = "googleMaps_Lng";
        public static string googleMapsContentString = "googleMapsContentString";
        public static string thongKeToday = "Today";
        public static string thongKeYesterday = "Yesterday";
        public static string thongKeWeek = "CurrentWeek";
        public static string thongKeMonth = "CurrentMonth";
        public static string thongKeTotal = "Total";
    }

    public class InterfaceParam
    {
        public string id { get; set; }
        public long? idThanhVien { get; set; }
        public string ids { get; set; }
        public string notId { get; set; }
        public string notIds { get; set; }

        public string idUsers { get; set; }
        public int displayItems { get; set; }
        public int displayPage { get; set; }
        public string trangThai { get; set; }
        public string allIncludingString { get; set; }
        public bool allIncluding { get { return CommonMethods.ConvertToBoolean(allIncludingString); } set { } }
        public string checkExpire { get; set; }
        public bool? checkExpireBool { get; set; }
        public string idsThanhVien { get; set; }
        public List<long> lstIdThanhVien { get; set; }
        public int? rate { get; set; }
        public int? point { get; set; }
        public string pointTmp { get; set; }
        public string note { get; set; }
        public string email { get; set; }
        public string dienThoai { get; set; }
        public string idTinhThanh { get; set; }
        public string noiDung { get; set; }
        public string noiDung1 { get; set; }
        public string hoTen { get; set; }
        public string capDo { get; set; }
        public string dynamicParam { get; set; }
        public string orderString { get; set; }
        public string childrenStructure { get; set; }
        public bool childrenStructureBool { get; set; }
        public string idMenu { get; set; }
        public string tuKhoaTimKiem { get; set; }
        public string isMain { get; set; }
        public string maCauHinh { get; set; }
        public string token { get; set; }
        public string linkId { get; set; }
        public string idTinDangs { get; set; }
        public string diaChi { get; set; }
        public string ma { get; set; }
        public string giaTu { get; set; }
        public string giaDen { get; set; }
        public string tieuDe { get; set; }
        public string type { get; set; }
        public string isDuyetTin { get; set; }
        public bool? isDuyet { get; set; }
        public string search_dynamicParams { get; set; }
        public string checkPhoneExist { get; set; }
        public List<int> lstCapDo { get; set; }
        public List<long> lstId { get; set; }
        public List<long> lstBigId { get; set; }
        public List<long> lstNotId { get; set; }
        public List<long> lstIdMenuParent { get; set; }
        public List<long> lstIdTinhThanh { get; set; }
        public List<int> lstTrangThai { get; set; }
        public bool isViewType { get; set; }
        public int? isView { get; set; }
        public byte? loaiXacNhan { get; set; }
        public string maXacNhan { get; set; }
        public int? tinhTrang { get; set; }
        public decimal? percentDiscount { get; set; }
        public decimal? price { get; set; }
        public long? idRank { get; set; }
        // Giao dich xe
        public string transactionStatus { get; set; }
        public List<int> lstTransactionStatus { get; set; }
        public byte? commissionStatus { get; set; }
        public byte? transactionMethod { get; set; }
        public int? idNguoiBan { get; set; }

        // PaidType
        public int? paidType { get; set; }
        public bool? isPinTop { get; set; }
        public int? timeEffective { get; set; } // 0: hết hạn || 1: còn hạn

        public int? typeInt { get; set; }
        public string tableName { get; set; }
        public string syncDate { get; set; }

        public string deviceId { get; set; }
        public string deviceName { get; set; }
        public bool? isFromAdmin { get; set; }
        public int typeNotification { get; set; }
        public long? idNotificationSystem { get; set; }

        // statistic
        public int? year { get; set; }
        public int? month { get; set; }
        public int? day { get; set; }

        // Cache
        public int? cacheType { get; set; }
        public string cacheKey { get; set; }
        public bool? isCache { get; set; }

        public string osName { get; set; }

        // ThanhVien
        public bool? daXacThucCmnd {get;set; }
        public bool? daXacThucPassport { get; set; }
        public string listHinhAnhJsonPassport { get; set; }
        public List<long> ListDeletedIdThanhVien { get; set; }
    }
}
