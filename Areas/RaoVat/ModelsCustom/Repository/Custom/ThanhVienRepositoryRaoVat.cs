﻿using Microsoft.AspNetCore.Http;
using raoVatApi.Common;
using raoVatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace raoVatApi.ModelsRaoVat
{
    public class ThanhVienRepositoryRaoVat : EntityBaseRepositoryRaoVat<ThanhVien>, IThanhVienRepositoryRaoVat
    {
        public ThanhVienRepositoryRaoVat(HttpRequest request) : base(request)
        {

        }

        public override async Task<IEnumerable<ThanhVien>> InsertReturnEntity(ThanhVien entity)
        {
            int newId = await Insert(entity);

            InterfaceParam interfaceParam = new InterfaceParam();
            interfaceParam.lstId = new List<long> { newId };

            AdminBll _adminBll = new AdminBll();
            var newEntity = await _adminBll.SearchThanhVien(interfaceParam);

            SendMailModel sendMail = new SendMailModel();
            sendMail.IdUser = newEntity.FirstOrDefault().Id;
            sendMail.IdUser = newEntity.FirstOrDefault().Id;

            await _adminBll.SendMailXacNhanTaiKhoan(sendMail, DateTime.Now);

            return newEntity;
        }

        public override async Task<int> UpdateAfterAutoClone(ThanhVien entity)
        {
            return await base.UpdateAfterAutoClone(entity);
        }

        public virtual async Task<ThanhVien> GetSingleThanhVien(int id)
        {
            return await Context.FindAsync<ThanhVien>(id);
        }

        public virtual async Task<ThanhVien> GetSingleThanhVien(Expression<Func<ThanhVien, bool>> predicate)
        {
            return await GetSingle(predicate);
        }

        public virtual async Task<ThanhVien> FindAsyncThanhVien(int id)
        {
            return await Context.FindAsync<ThanhVien>(id);
        }
    }
}