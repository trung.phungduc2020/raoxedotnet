﻿using Microsoft.AspNetCore.Http;
using raoVatApi.Common;
using raoVatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace raoVatApi.ModelsRaoVat
{
    public class MenuRaoVatRepositoryRaoVat : EntityBaseRepositoryRaoVat<MenuRaoVat>, IMenuRaoVatRepositoryRaoVat
    {
        private RawSqlRepository _rawSQLRepo;
        public RawSqlRepository RawSQLRepo
        {
            get
            {
                return new RawSqlRepository(new RaoVatContext_Custom());
            }
            set
            {
                _rawSQLRepo = value;
            }
        }

        private AdminBll _adminBLL;
        public AdminBll AdminBLL
        {
            get
            {
                return new AdminBll();
            }
            set
            {
                _adminBLL = value;
            }

        }

        public MenuRaoVatRepositoryRaoVat(HttpRequest request) : base(request)
        {

        }

        public override async Task<IEnumerable<MenuRaoVat>> InsertReturnEntity(MenuRaoVat entity)
        {
            int newId = await Insert(entity);

            if (newId > 0)
            {
                // Update IDMenuParentList
                await UpdateAllMenuChildren(newId);
            }

            InterfaceParam interfaceParam = new InterfaceParam();
            interfaceParam.lstId = new List<long> { newId };

            IEnumerable<MenuRaoVat> newMenu = await AdminBLL.SearchMenuRaoVat(interfaceParam);
            if (newMenu != null)
            {
                await AdminBLL.BuildMenuRaoVatExtend(newMenu);
            }

            return newMenu;
        }

        public override async Task<int> UpdateAfterAutoClone(MenuRaoVat entity)
        {
            int res = await base.UpdateAfterAutoClone(entity);

            if (res > 0)
            {
                // Update IDMenuParentList
                await UpdateAllMenuChildren(entity.Id);
            }

            return res;
        }

        public override async Task<MenuRaoVat> GetSingle(long id)
        {
            MenuRaoVat menu = await base.GetSingle(id);

            if (menu != null)
            {
                await AdminBLL.BuildMenuRaoVatExtend(new List<MenuRaoVat>() { menu });
            }

            return menu;
        }

        public override async Task<IEnumerable<MenuRaoVat>> GetDataByIdParent(long idParent, Expression<Func<MenuRaoVat, bool>> predicate)
        {
            Expression<Func<MenuRaoVat, bool>> searchParentIdExpression = s => s.IdMenuParent == idParent;

            IEnumerable<MenuRaoVat> res = await Search(searchParentIdExpression, "ThuTu,TenMenu");

            if (res != null && predicate != null)
            {
                res = res.AsQueryable().Where(predicate);
            }

            return res;
        }

        /* Extend methods */
        public fnBuildMenuSortColumn GetDataBuildMenuSortColumn(long idFolder)
        {
            //string sql = @"SELECT 
            //TextSort = (SELECT TextSort FROM fnBuildMenuSortColumn({0})),
            //IdMenuParentList = (SELECT IdMenuParentList FROM fnBuildMenuSortColumn({0})),
            //CapDo = (SELECT CapDo FROM fnBuildMenuSortColumn({0}))";

            string sql = @"SELECT 
            (SELECT TextSort FROM fnBuildMenuSortColumn({0})) as TextSort,
            (SELECT IdMenuParentList FROM fnBuildMenuSortColumn({0})) as IdMenuParentList,
            (SELECT CapDo FROM fnBuildMenuSortColumn({0})) as CapDo";
            
            IEnumerable<fnBuildMenuSortColumn> res = RawSQLRepo.ExecuteSqlQuery<fnBuildMenuSortColumn>(sql, idFolder).Result;

            return res != null ? res.FirstOrDefault() : null;
        }

        public async Task UpdateAllMenuChildren(long idMenu)
        {
            fnBuildMenuSortColumn data = GetDataBuildMenuSortColumn(idMenu);

            if (data != null)
            {
                string TextSort = data.TextSort;
                string IdMenuParentList = data.IdMenuParentList;
                int CapDo = CommonMethods.ConvertToInt32(data.CapDo, 1).Value;

                await UpdateTextSortMenu(idMenu, IdMenuParentList, TextSort, CapDo);

                IEnumerable<MenuRaoVat> dataChildrenMenu = GetDataByIdParent(idMenu, null).Result;
                foreach (MenuRaoVat children in dataChildrenMenu)
                {
                    await UpdateAllMenuChildren(children.Id);
                }
            }
        }

        public async Task<int> UpdateTextSortMenu(long idMenu, string idMenuParentList, string textSort, int capDo)
        {
            return await UpdateWhere(s => s.Id == idMenu, delegate (MenuRaoVat obj)
            {
                if (obj != null)
                {
                    obj.IdMenuParentList = idMenuParentList;
                    obj.TextSort = textSort;
                    obj.CapDo = CommonMethods.ConvertToByte(capDo);
                }
            });
        }
    }
}
