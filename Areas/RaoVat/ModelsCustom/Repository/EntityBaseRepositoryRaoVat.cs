﻿using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.Models.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace raoVatApi.ModelsRaoVat
{
    public class EntityBaseRepositoryRaoVat<T> : EntityBaseRepository<T>, IEntityBaseRepositoryRaoVat<T> where T : class, IEntityBase
    {
        public EntityBaseRepositoryRaoVat() : base(null)
        {
            Init();
        }

        public EntityBaseRepositoryRaoVat(bool readOnly) : base(null)
        {
            // nguyencuongcs 20191108: luôn luôn dùng để khởi tạo pg slave
            readOnly = true;
            Init(Variables.ConnectionStringPostgresql_RaoVat_Slave);
        }

        public EntityBaseRepositoryRaoVat(HttpRequest request) : base(request)
        {
            Init();
        }

        public EntityBaseRepositoryRaoVat(HttpRequest request, string connectionString) : base(request)
        {
            Init(connectionString);
        }

        private void Init(string connectionString = "")
        {
            connectionDb = "raovat";
            if (!string.IsNullOrEmpty(connectionString))
            {
                // Custom => Write => Master
                // Run after base()
                Context = new RaoVatContext_Custom(connectionString);
                Entities = Context.Set<T>();
            }
            else
            {                
                Context = new RaoVatContext_Custom();
                Entities = Context.Set<T>();
            }
        }

        public EntityBaseRepositoryRaoVat(RaoVatContext_Custom context, HttpRequest request) : base(context, request)
        {
        }

        //public override async Task ClearPool()
        //{
        //    (Context as RaoVatContext_Custom).ClearPool();
        //}
    }
}