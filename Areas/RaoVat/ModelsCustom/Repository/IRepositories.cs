using raoVatApi.Models;
using raoVatApi.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace raoVatApi.ModelsRaoVat
{
    public interface IMenuRaoVatRepositoryRaoVat : IEntityBaseRepository<MenuRaoVat>
    {
    }

    public interface IThanhVienRepositoryRaoVat : IEntityBaseRepository<ThanhVien>
    {
    }
}