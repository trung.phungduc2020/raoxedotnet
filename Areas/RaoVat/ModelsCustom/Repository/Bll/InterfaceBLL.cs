﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using raoVatApi.Models;
using Microsoft.EntityFrameworkCore;
using raoVatApi.Common;
using Microsoft.AspNetCore.Http;
using raoVatApi.Response;
using static Variables;
using raoVatApi.Models.Base;
using Repository;

namespace raoVatApi.ModelsRaoVat
{
    public class InterfaceBLL
    {
        private HttpRequest _request;

        public HttpRequest Request
        {
            get
            {
                //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
                //if (_request == null)
                {
                    HttpContextAccessor accessor = new HttpContextAccessor();
                    _request = accessor.HttpContext == null ? null : accessor.HttpContext.Request;
                }
                return _request;
            }
            set
            {
                _request = value;
            }
        }

        //private RaoVatContext_Custom _context;

        public RaoVatContext_Custom Context
        {
            get
            {
                return new RaoVatContext_Custom();
            }
        }

        private RawSqlRepository _rawSQLRepo;

        public RawSqlRepository RawSQLRepo
        {
            get
            {
                //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
                //if (_rawSQLRepo == null)
                {
                    // nguyencuongcs 20191108: ko được truyền new RaoVatContext_Custom(true) hoặc khởi tạo RaoVatContext_Custom master vào vì sẽ ko work do code bên trong RawSqlRepository
                    // Phải dùng constructor truyền biến true này
                    _rawSQLRepo = new RawSqlRepository(new RaoVatContext_Custom(), true);
                }
                return _rawSQLRepo;
            }
            set
            {
                _rawSQLRepo = value;
            }
        }

        //private AdminBll _adminBLL;
        //public AdminBll AdminBLL
        //{
        //    get
        //    {
        //        //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
        //        //if (_adminBLL == null)
        //        {
        //            _adminBLL = new AdminBll();
        //        }
        //        return _adminBLL;
        //    }
        //    set
        //    {
        //        _adminBLL = value;
        //    }
        //}

        //private List<int> _lstTrangThai_1 = new List<int>() { 1 };

        private MemoryCacheControllerBase _cacheController;
        public MemoryCacheControllerBase CacheController { get => _cacheController == null ? new MemoryCacheControllerBase() : _cacheController; set => _cacheController = value; }

        //private ApiGetDal _apiGetDal;
        //public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        private EntityBaseRepositoryRaoVat<DangTinFavorite> _repoDangTinFavorite;
        public EntityBaseRepositoryRaoVat<DangTinFavorite> RepoDangTinFavorite { get => _repoDangTinFavorite == null ? _repoDangTinFavorite = new EntityBaseRepositoryRaoVat<DangTinFavorite>(true) : _repoDangTinFavorite; set => _repoDangTinFavorite = value; }

        private EntityBaseRepositoryRaoVat<DangTinTraPhi> _repoDangTinTraPhi;
        public EntityBaseRepositoryRaoVat<DangTinTraPhi> RepoDangTinTraPhi { get => _repoDangTinTraPhi == null ? _repoDangTinTraPhi = new EntityBaseRepositoryRaoVat<DangTinTraPhi>(true) : _repoDangTinTraPhi; set => _repoDangTinTraPhi = value; }

        private EntityBaseRepositoryRaoVat<LayoutAlbum> _repoLayoutAlbum;
        public EntityBaseRepositoryRaoVat<LayoutAlbum> RepoLayoutAlbum { get => _repoLayoutAlbum == null ? _repoLayoutAlbum = new EntityBaseRepositoryRaoVat<LayoutAlbum>(true) : _repoLayoutAlbum; set => _repoLayoutAlbum = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>(true) : _repoDangTinContact; set => _repoDangTinContact = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCode;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCode { get => _repoMarketingCode == null ? _repoMarketingCode = new EntityBaseRepositoryRaoVat<MarketingCode>(true) : _repoMarketingCode; set => _repoMarketingCode = value; }

        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUser;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUser { get => _repoMarketingUser == null ? _repoMarketingUser = new EntityBaseRepositoryRaoVat<MarketingUser>(true) : _repoMarketingUser; set => _repoMarketingUser = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>() : _repoThanhVien; set => _repoThanhVien = value; }
        private EntityBaseRepositoryRaoVat<UserRank> _repoUserRank;
        public EntityBaseRepositoryRaoVat<UserRank> RepoUserRank { get => _repoUserRank == null ? _repoUserRank = new EntityBaseRepositoryRaoVat<UserRank>(Request) : _repoUserRank; set => _repoUserRank = value; }
        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPoint;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPoint { get => _repoUserPoint == null ? _repoUserPoint = new EntityBaseRepositoryRaoVat<UserPoint>() : _repoUserPoint; set => _repoUserPoint = value; }

        public InterfaceBLL(HttpRequest request)
        {
            //_context = new RaoVatContext_Custom();
            Request = request;
            //_adminBLL = new AdminBll();
        }

        #region "Commons"

        private bool CheckListIntIsAllowSearch(List<long> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1) // nguyencuongcs 20171003: Nếu truyền vào danh sách rỗng ( != null ) thì có nghĩa là muốn tìm theo điều kiện này
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckListIntIsAllowSearch(List<int> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1) // nguyencuongcs 20171003: Nếu truyền vào danh sách rỗng ( != null ) thì có nghĩa là muốn tìm theo điều kiện này
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckContainInStringArray(string strArrayToCheck, List<int> lstValue, bool fullCheckValue)
        {
            bool res = false;

            List<int> lstSourceToCheck = CommonMethods.SplitStringToInt(strArrayToCheck, false);
            foreach (int val in lstValue)
            {
                if (lstSourceToCheck.Contains(val))
                {
                    res = true;

                    if (!fullCheckValue)
                    {
                        return res;
                    }
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        #endregion

        #region "CauHinhHeThong"

        public async Task<IEnumerable<CauHinhHeThong>> SearchCauHinhHeThong(InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<CauHinhHeThong> repo = new EntityBaseRepositoryRaoVat<CauHinhHeThong>(Request);

            if (string.IsNullOrEmpty(interfaceParam.maCauHinh))
            {
                return await repo.Entities.ToListAsync();
            }

            return await repo.Find(n => n.MaCauHinh == interfaceParam.maCauHinh);
        }

        public async Task<string> GetGiaTriCauHinhHeThongByMa(string maCauHinh)
        {
            EntityBaseRepositoryRaoVat<CauHinhHeThong> repo = new EntityBaseRepositoryRaoVat<CauHinhHeThong>(Request);
            CauHinhHeThong res = await repo.GetSingle(s => s.MaCauHinh == maCauHinh);
            return res != null ? res.GiaTriCauHinh : "";
        }

        #endregion

        #region "SearchUserBlacklist"

        public async Task<IEnumerable<UserBlackList>> SearchUserBlacklist(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserBlackList>(interfaceParam.search_dynamicParams);

            Expression<Func<UserBlackList, bool>> terms = s =>
                     (interfaceParam.idThanhVien.HasValue && s.IdThanhVien == interfaceParam.idThanhVien.Value) &&
                     (s.TuNgay <= DateTime.Now && DateTime.Now <= s.DenNgay) &&
                     (s.TrangThai.HasValue && s.TrangThai.Value == 1) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserBlackList> repo = new EntityBaseRepositoryRaoVat<UserBlackList>(Request);
            IEnumerable<UserBlackList> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "DangTinContact"

        public async Task<IEnumerable<DangTinContact>> SearchDangTinContact(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinContact>(interfaceParam.search_dynamicParams);
            Expression<Func<DangTinContact, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
            (interfaceParam.idThanhVien.HasValue && s.IdThanhVien == interfaceParam.idThanhVien.Value) &&
            (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTinContact> repo = new EntityBaseRepositoryRaoVat<DangTinContact>(Request);
            IEnumerable<DangTinContact> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            return res;
        }

        #endregion

        #region "LayoutAlbum"

        public async Task<Tuple<IEnumerable<LayoutAlbum>, string>> SearchLayoutAlbum(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();

            var dynamicFunctions = await dynParams.BuildDynamicExpression<LayoutAlbum>(interfaceParam.search_dynamicParams);
            Expression<Func<LayoutAlbum, bool>> terms = s =>
            (!string.IsNullOrEmpty(interfaceParam.ma) && s.Ma == interfaceParam.ma) &&
            dynamicFunctions(s);

            IEnumerable<LayoutAlbum> res = null;

            MemoryCacheControllerBase cacheController = new MemoryCacheControllerBase();
            string co = KeyCacheParams.coalesceString;
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalLayoutAlbumByParams, tongHopBien);
            var item2 = string.Empty;

            object cacheValue = await cacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<LayoutAlbum>>(cacheValue);
                item2 = keyCache;
            }
            else
            {
                EntityBaseRepositoryRaoVat<LayoutAlbum> repo = new EntityBaseRepositoryRaoVat<LayoutAlbum>(Request);
                res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

                // Add cache data
                await cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return new Tuple<IEnumerable<LayoutAlbum>, string>(res, item2);
        }

        #endregion

        //#region "UserDevice"

        //public async Task<IEnumerable<UserDevice>> SearchUserDevice(InterfaceParam interfaceParam)
        //{
        //    DynamicParams dynParams = new DynamicParams();
        //    var dynamicFunctions = await dynParams.BuildDynamicExpression<UserDevice>(interfaceParam.search_dynamicParams);

        //    Expression<Func<UserDevice, bool>> terms = s =>
        //           (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
        //           (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
        //           (string.IsNullOrEmpty(interfaceParam.deviceName) || s.DeviceName == interfaceParam.deviceName) &&
        //           dynamicFunctions(s);

        //    EntityBaseRepositoryRaoVat<UserDevice> repo = new EntityBaseRepositoryRaoVat<UserDevice>(Request);
        //    IEnumerable<UserDevice> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

        //    return res;
        //}

        //#endregion

        #region "SearchNotificationMember"

        public IEnumerable<Dictionary<string, object>> SearchNotificationMember(InterfaceParam interfaceParam, out int Total)
        {
            string IdThanhVien = interfaceParam.idThanhVien > 0 ? interfaceParam.idThanhVien.Value.ToString() : "-1";
            string OrderBy = @" Order by st.""NgayTao"" desc";
            string IsLogined = interfaceParam.idThanhVien.HasValue ? "true" : "false";
            // Type
            // 1: Count notification isView = null OR isView = false
            // !=1: Get Notification data
            string IsView = interfaceParam.type == "1" ? @" And(us.""IsView"" is null Or us.""IsView"" = false)" : string.Empty;

            string rawSQL = @"select * from ""NotificationSystem"" st" +
                            @" left join ""NotificationUser"" us on st.""Id"" = us.""IdNotificationSystem"" And us.""IdThanhVien"" = " + IdThanhVien +
                            @" where COALESCE(""TuNgay"",st.""NgayTao"") <= current_timestamp AND (""DenNgay"" is null Or current_timestamp <= ""DenNgay"") And (st.""Type"" = 1" +
                            @" Or (st.""Type"" = 2 And st.""IdThanhVien"" = " + IdThanhVien + ")" +
                            @" Or (st.""Type"" = 3 And " + IsLogined + ")" +
                            @" Or (st.""Type"" = 4 And ',' || st.""GhiChu"" || ',' like '%," + IdThanhVien + ",%'))" +
                            @" And st.""TrangThai"" = 1" +
                            @" And (us.""TrangThai"" is null Or us.""TrangThai"" <> 2)"
                            + IsView
                            + OrderBy;

            var result = RawSQLRepo.ExecuteSqlQuery_ReturnDic(rawSQL, "").Result;

            Total = result.Count();

            result = result.Skip(interfaceParam.displayItems * (interfaceParam.displayPage - 1)).Take(interfaceParam.displayItems);

            return result;
        }

        public async Task<Tuple<IEnumerable<Dictionary<string, object>>, string>> SearchNotificationMemberHasCache(InterfaceParam interfaceParam)
        {
            IEnumerable<Dictionary<string, object>> res = null;
            var total = 0;

            MemoryCacheControllerBase cacheController = new MemoryCacheControllerBase();
            string co = KeyCacheParams.coalesceString;
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalNotificationSystemByParams, tongHopBien);
            var item2 = string.Empty;

            object cacheValue = await cacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<Dictionary<string, object>>>(cacheValue);
                item2 = keyCache;
            }
            else
            {
                string IdThanhVien = interfaceParam.idThanhVien > 0 ? interfaceParam.idThanhVien.Value.ToString() : "-1";
                string OrderBy = @" Order by st.""NgayTao"" desc";
                string IsLogined = interfaceParam.idThanhVien.HasValue ? "true" : "false";
                // Type
                // 1: Count notification isView = null OR isView = false
                // !=1: Get Notification data
                string IsView = interfaceParam.type == "1" ? @" And(us.""IsView"" is null Or us.""IsView"" = false)" : string.Empty;

                string rawSQL = @"select * from ""NotificationSystem"" st" +
                                @" left join ""NotificationUser"" us on st.""Id"" = us.""IdNotificationSystem"" And us.""IdThanhVien"" = " + IdThanhVien +
                                @" where COALESCE(""TuNgay"",st.""NgayTao"") <= current_timestamp AND (""DenNgay"" is null Or current_timestamp <= ""DenNgay"") And (st.""Type"" = 1" +
                                @" Or (st.""Type"" = 2 And st.""IdThanhVien"" = " + IdThanhVien + ")" +
                                @" Or (st.""Type"" = 3 And " + IsLogined + ")" +
                                @" Or (st.""Type"" = 4 And ',' || st.""GhiChu"" || ',' like '%," + IdThanhVien + ",%'))" +
                                @" And st.""TrangThai"" = 1" +
                                @" And (us.""TrangThai"" is null Or us.""TrangThai"" <> 2)"
                                + IsView
                                + OrderBy;

                res = RawSQLRepo.ExecuteSqlQuery_ReturnDic(rawSQL, "").Result;
                total = res.Count();

                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 1;
                    interfaceParam.displayPage = 1;
                }

                res = res.Skip(interfaceParam.displayItems * (interfaceParam.displayPage - 1)).Take(interfaceParam.displayItems);

                foreach (var item in res)
                {
                    item.Add("TotalRow", total);
                }

                // Add cache data
                await cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return new Tuple<IEnumerable<Dictionary<string, object>>, string>(res, item2);
        }

        #endregion

        #region "DangTinRating"

        public async Task<Tuple<IEnumerable<DangTinRating>, string>> SearchDangTinRating(InterfaceParam interfaceParam)
        {
            List<long> lstIdUser = !string.IsNullOrEmpty(interfaceParam.idUsers) ? CommonMethods.SplitStringToInt64(interfaceParam.idUsers) : null;
            List<long> lstIdDangTin = !string.IsNullOrEmpty(interfaceParam.idTinDangs) ? CommonMethods.SplitStringToInt64(interfaceParam.idTinDangs) : null;

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinRating>(interfaceParam.search_dynamicParams);
            Expression<Func<DangTinRating, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (lstIdUser == null || lstIdUser.Count <= 0 || lstIdUser.Contains(s.IdUser ?? 0)) &&
                   (lstIdDangTin == null || lstIdDangTin.Count <= 0 || lstIdDangTin.Contains(s.IdDangTin ?? 0)) &&
                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   dynamicFunctions(s);

            IEnumerable<DangTinRating> res = null;

            MemoryCacheControllerBase cacheController = new MemoryCacheControllerBase();
            string co = KeyCacheParams.coalesceString;
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalDangTinRatingByParams, tongHopBien);
            var item2 = string.Empty;

            object cacheValue = await cacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<DangTinRating>>(cacheValue);
                item2 = keyCache;
            }
            else
            {
                EntityBaseRepositoryRaoVat<DangTinRating> repo = new EntityBaseRepositoryRaoVat<DangTinRating>(Request);
                res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
                //await BuildDangTinRatingExtend(res, interfaceParam);

                // Add cache data
                await cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return new Tuple<IEnumerable<DangTinRating>, string>(res, item2);
        }

        //private async Task<IEnumerable<DangTinRating>> BuildDangTinRatingExtend(IEnumerable<DangTinRating> res, InterfaceParam interfaceParam)
        //{
        //    EntityBaseRepositoryRaoVat<DangTin> repoTinDang = new EntityBaseRepositoryRaoVat<DangTin>();
        //    if (interfaceParam.allIncluding == true && res != null && res.Count() > 0)
        //    {
        //        foreach (var dtr in res)
        //        {
        //            DangTin dt = (await repoTinDang.Find(n => n.Id == dtr.IdDangTin && n.TrangThai == 1 && n.IsDuyet == true)).FirstOrDefault();
        //            if (dt != null)
        //            {
        //                dtr.DangTin_Ext = dt;
        //            }
        //        }
        //    }
        //    return res;
        //}

        #endregion

        #region MenuRaoVat

        public async Task<Tuple<IEnumerable<MenuRaoVat>, string>> SearchMenuRaoVat(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<MenuRaoVat>(interfaceParam.dynamicParam);

            Expression<Func<MenuRaoVat, bool>> terms = s =>
                (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstCapDo, false) || interfaceParam.lstCapDo.Contains(s.CapDo.GetValueOrDefault(0))) &&
                (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
                (interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                ((interfaceParam.lstIdMenuParent == null || interfaceParam.lstIdMenuParent.Count() == 0) || (interfaceParam.lstIdMenuParent.Contains(s.IdMenuParent ?? 0))) &&
                dynamicFunctions(s);

            Expression<Func<MenuRaoVat, object>>[] includeProperties = null;
            IEnumerable<MenuRaoVat> res = null;

            MemoryCacheControllerBase cacheController = new MemoryCacheControllerBase();
            string co = KeyCacheParams.coalesceString;
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalMenuRaoVatByParams, tongHopBien);
            var item2 = string.Empty;

            object cacheValue = await cacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<MenuRaoVat>>(cacheValue);
                item2 = keyCache;
            }
            else
            {
                MenuRaoVatRepositoryRaoVat repo = new MenuRaoVatRepositoryRaoVat(Request);
                res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, includeProperties);
                await BuildMenuRaoVatExtend(res);

                // Add cache data
                await cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return new Tuple<IEnumerable<MenuRaoVat>, string>(res, item2);
        }

        public async Task BuildMenuRaoVatExtend(IEnumerable<MenuRaoVat> source)
        {
            if (source == null)
            {
                return;
            }

            foreach (MenuRaoVat item in source)
            {
                item.TenMenu_Ext = CommonMethods.AddCapDoString(item.CapDo.HasValue ? item.CapDo.Value : 0) + item.TenMenu;
            }
        }

        #endregion

        #region "Đăng tin"

        private async Task<Expression<Func<DangTin, bool>>> ConditionSearchDangTinByIdThanhVienAsync(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                CommonMethods.SplitStringToInt64(interfaceParam.id);
            interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) :
                 CommonMethods.SplitStringToInt64(interfaceParam.notId);
            interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1, 2 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
            interfaceParam.lstIdTinhThanh = string.IsNullOrEmpty(interfaceParam.idTinhThanh) ? new List<long>() : CommonMethods.SplitStringToInt64(interfaceParam.idTinhThanh);
            List<long> lstIdDongXe = string.IsNullOrEmpty(interfaceParam.idDongXes) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idDongXes);
            List<long> lstIdLoaiXe = string.IsNullOrEmpty(interfaceParam.idLoaiXes) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idLoaiXes);

            decimal? GiaTu = string.IsNullOrEmpty(interfaceParam.giaTu) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaTu);
            decimal? GiaDen = string.IsNullOrEmpty(interfaceParam.giaDen) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaDen);
            List<long> lstIdMenu = string.IsNullOrEmpty(interfaceParam.idMenu) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idMenu);
            List<int> lstType = string.IsNullOrEmpty(interfaceParam.type) ? null : CommonMethods.SplitStringToInt(interfaceParam.type, true);
            List<long> filterTinTraPhi = RepoDangTinTraPhi.Find(a => (a.PinStatus == 0 || a.PinStatus == 2) && a.IdThanhVienTao == interfaceParam.idThanhVien &&
                    (a.NgayBatDau.Value <= DateTime.Now && DateTime.Now <= (a.NgayKetThuc.HasValue ? a.NgayKetThuc.Value : a.NgayKetThucDuDinh.Value))).Result.Select(a => a.IdDangTin ?? 0).ToList();

            // If search =. no diffirent pin or not
            bool IsSearch = !string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem);

            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTin>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTin, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (!interfaceParam.tinhTrang.HasValue || interfaceParam.tinhTrang.Equals(s.TinhTrang ?? 0)) &&
                   (interfaceParam.idThanhVien == s.IdThanhVien.Value) &&
                   (!GiaTu.HasValue || (GiaTu.Value <= (s.Gia ?? 0))) &&
                   (!GiaDen.HasValue || (GiaDen.Value >= (s.Gia ?? 0))) &&
                   ((lstIdMenu == null || lstIdMenu.Count <= 0) || lstIdMenu.Contains(s.IdMenu ?? 0)) &&

                   (!interfaceParam.isDuyet.HasValue || interfaceParam.isDuyet == s.IsDuyet) &&

                   (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
                   (
                            (IsSearch == true) || (IsSearch == false &&
                                                    ((!interfaceParam.isPinTop.HasValue) ||
                                                    (interfaceParam.isPinTop.Value == false && (s.IsPinTop.Value == false || s.IsPinTop.Value == true)) ||
                                                    (interfaceParam.isPinTop.Value == true && (filterTinTraPhi.Contains(s.Id) || (s.IsPinTop.Value == true))))
                                                  )
                   ) &&

                   (lstIdDongXe == null || lstIdDongXe.Count <= 0 || lstIdDongXe.Contains(s.IdDongXe ?? 0)) &&
                   (lstIdLoaiXe == null || lstIdLoaiXe.Count <= 0 || lstIdLoaiXe.Contains(s.IdLoaiXe ?? 0)) &&
                   (interfaceParam.lstIdTinhThanh.Count <= 0 || interfaceParam.lstIdTinhThanh.Contains(s.IdTinhThanh ?? 0)) &&
                   (interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   ((lstType == null || lstType.Count <= 0) || lstType.Contains(s.Type ?? 0)) &&
                   dynamicFunctions(s);

            return terms;
        }

        public async Task<IEnumerable<DangTin>> SearchDangTinByIdThanhVien(InterfaceParam interfaceParam)
        {
            Expression<Func<DangTin, bool>> terms = await ConditionSearchDangTinByIdThanhVienAsync(interfaceParam);

            EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
            IEnumerable<DangTin> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            await BuildDangTinExtend(res, interfaceParam);

            return res;
        }

        private async Task<Expression<Func<DangTin, bool>>> ConditionSearchAsync(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();

            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin Handle param");
            List<long> lstIdDongXe = string.IsNullOrEmpty(interfaceParam.idDongXes) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idDongXes);
            List<long> lstIdLoaiXe = string.IsNullOrEmpty(interfaceParam.idLoaiXes) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idLoaiXes);
            List<long> lstIdThuongHieu = string.IsNullOrEmpty(interfaceParam.idThuongHieus) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idThuongHieus);
            interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
            interfaceParam.lstIdTinhThanh = string.IsNullOrEmpty(interfaceParam.idTinhThanh) ? new List<long>() : CommonMethods.SplitStringToInt64(interfaceParam.idTinhThanh);


            decimal? GiaTu = string.IsNullOrEmpty(interfaceParam.giaTu) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaTu);
            decimal? GiaDen = string.IsNullOrEmpty(interfaceParam.giaDen) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaDen);
            List<long> lstIdMenu = string.IsNullOrEmpty(interfaceParam.idMenu) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idMenu);
            List<int> lstType = string.IsNullOrEmpty(interfaceParam.type) ? null : CommonMethods.SplitStringToInt(interfaceParam.type, true);
            //interfaceParam.checkExpireBool = CommonMethods.ConvertToBoolean(interfaceParam.checkExpire, null); // kiểm tra tin đăng có đang còn tồn tại hày không
            interfaceParam.ListDeletedIdThanhVien = Variables.ListDeletedIdThanhVien;
            // Except User with TrangThai != 1

            // If search =. no diffirent pin or not
            bool IsSearch = !string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem);

            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTin>(interfaceParam.search_dynamicParams);

            var tmpDangTinTraPhi = RepoDangTinTraPhi.Find(a => (a.PinStatus == 0 || a.PinStatus == 2)).Result;
            List<long> filterTinTraPhi = tmpDangTinTraPhi.Where(a => (a.NgayBatDau.Value <= DateTime.Now
                                                && DateTime.Now <= a.NgayKetThucDuDinh.Value)).Select(a => a.IdDangTin ?? 0).ToList();

            if (interfaceParam.isPlusChuaDuyet.HasValue && interfaceParam.isPlusChuaDuyet.Value && interfaceParam.idThanhVien.HasValue && interfaceParam.idThanhVien.Value > 0)
            {
                Expression<Func<DangTin, bool>> terms = s =>
                       (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                       (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                       (!interfaceParam.tinhTrang.HasValue || interfaceParam.tinhTrang.Equals(s.TinhTrang ?? 0)) &&

                       (
                           (interfaceParam.idThanhVien == s.IdThanhVien.Value && (!s.IsDuyet.HasValue || s.IsDuyet.HasValue)) ||
                           (s.IsDuyet.HasValue && s.IsDuyet.Value)
                       ) &&

                       (!interfaceParam.paidType.HasValue ||
                            (interfaceParam.paidType.HasValue && ((interfaceParam.paidType.Value == (int)Variables.PaidType.PinTop && s.IsPinTop.Value == true)))) &&
                       (!GiaTu.HasValue || (GiaTu.Value <= (s.Gia ?? 0))) &&
                       (!GiaDen.HasValue || (GiaDen.Value >= (s.Gia ?? 0))) &&
                       (!interfaceParam.soCho.HasValue || (interfaceParam.soCho.HasValue && s.SoChoNgoi == interfaceParam.soCho)) &&
                       ((lstIdMenu == null || lstIdMenu.Count <= 0) || lstIdMenu.Contains(s.IdMenu ?? 0)) &&
                       (
                            (IsSearch == true) || (IsSearch == false &&
                                                    ((!interfaceParam.isPinTop.HasValue) ||
                                                    (interfaceParam.isPinTop.Value == false && (s.IsPinTop.Value == false || s.IsPinTop.Value == true)) ||
                                                    (interfaceParam.isPinTop.Value == true && (filterTinTraPhi.Contains(s.Id) || (s.IsPinTop.Value == true))))
                                                  )
                       ) &&
                       (!interfaceParam.ListDeletedIdThanhVien.Contains(s.IdThanhVien ?? 0)) &&
                       (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
                       (lstIdDongXe == null || lstIdDongXe.Count <= 0 || lstIdDongXe.Contains(s.IdDongXe ?? 0)) &&
                       (lstIdLoaiXe == null || lstIdLoaiXe.Count <= 0 || lstIdLoaiXe.Contains(s.IdLoaiXe ?? 0)) &&
                       (lstIdThuongHieu == null || lstIdThuongHieu.Count <= 0 || lstIdThuongHieu.Contains(s.IdThuongHieu ?? 0)) &&
                       (interfaceParam.lstIdTinhThanh.Count <= 0 || interfaceParam.lstIdTinhThanh.Contains(s.IdTinhThanh ?? 0)) &&
                       (interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                       (lstType == null || lstType.Count <= 0 || lstType.Contains(s.Type ?? 0)) &&
                       (string.IsNullOrEmpty(s.IdDaiLy)
                        || (!interfaceParam.idDaiLy.HasValue || ("," + s.IdDaiLy + ",").Contains("," + interfaceParam.idDaiLy + ","))) &&

                       dynamicFunctions(s);

                return terms;
            }
            else
            {
                Expression<Func<DangTin, bool>> terms = s =>
                       (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                       (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                       (!interfaceParam.tinhTrang.HasValue || interfaceParam.tinhTrang.Equals(s.TinhTrang ?? 0)) &&
                       (!interfaceParam.paidType.HasValue ||
                            (interfaceParam.paidType.HasValue && ((interfaceParam.paidType.Value == (int)Variables.PaidType.PinTop && s.IsPinTop.Value == true)))) &&
                       (!GiaTu.HasValue || (GiaTu.Value <= (s.Gia ?? 0))) &&
                       (!GiaDen.HasValue || (GiaDen.Value >= (s.Gia ?? 0))) &&
                       (s.IsDuyet.HasValue && s.IsDuyet.Value) &&
                       (!interfaceParam.soCho.HasValue || (interfaceParam.soCho.HasValue &&  s.SoChoNgoi == interfaceParam.soCho)) &&
                       (!interfaceParam.idThanhVienDatQuangCao.HasValue || (interfaceParam.idThanhVienDatQuangCao.HasValue && interfaceParam.idThanhVienDatQuangCao.Value == s.IdThanhVien.Value)) &&

                       (
                            (IsSearch == true) || (IsSearch == false &&
                                                    ((!interfaceParam.isPinTop.HasValue) ||
                                                    (interfaceParam.isPinTop.Value == false && (s.IsPinTop.Value == false || s.IsPinTop.Value == true)) ||
                                                    (interfaceParam.isPinTop.Value == true && (filterTinTraPhi.Contains(s.Id) || (s.IsPinTop.Value == true))))
                                                  )
                       ) &&
                       (!interfaceParam.ListDeletedIdThanhVien.Contains(s.IdThanhVien ?? 0)) &&
                       (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
                       (lstIdMenu == null || lstIdMenu.Count <= 0 || lstIdMenu.Contains(s.IdMenu ?? 0)) &&
                       (lstIdDongXe == null || lstIdDongXe.Count <= 0 || lstIdDongXe.Contains(s.IdDongXe ?? 0)) &&
                       (lstIdLoaiXe == null || lstIdLoaiXe.Count <= 0 || lstIdLoaiXe.Contains(s.IdLoaiXe ?? 0)) &&
                       (lstIdThuongHieu == null || lstIdThuongHieu.Count <= 0 || lstIdThuongHieu.Contains(s.IdThuongHieu ?? 0)) &&
                       (interfaceParam.lstIdTinhThanh.Count <= 0 || interfaceParam.lstIdTinhThanh.Contains(s.IdTinhThanh ?? 0)) &&
                       (interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                       (lstType == null || lstType.Count <= 0 || lstType.Contains(s.Type ?? 0)) &&
                       (string.IsNullOrEmpty(s.IdDaiLy)
                        || (!interfaceParam.idDaiLy.HasValue || ("," + s.IdDaiLy + ",").Contains("," + interfaceParam.idDaiLy + ","))) &&

                       dynamicFunctions(s);

                return terms;
            }
        }

        public async Task<Tuple<IEnumerable<DangTin>, string>> SearchDangTin(InterfaceParam interfaceParam)
        {
            //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin InterfaceBll SearchDangTin");
            IEnumerable<DangTin> res = null;

            string co = KeyCacheParams.coalesceString;
            //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-tongHopBien from interfaceParam");
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalDangTinByParams, tongHopBien);
            string item2 = "";

            //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-GetCacheByKey {keyCache}");
            object cacheValue = await CacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-cacheValue != null");
                res = CommonMethods.DeserializeObject<IEnumerable<DangTin>>(cacheValue);
                item2 = keyCache;
            }
            else
            {
                //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-cacheValue == null");

                Expression<Func<DangTin, bool>> terms = await ConditionSearchAsync(interfaceParam);

                EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
                //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-repo.GetAll");
                res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

                //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-BuildDangTinExtend");
                await BuildDangTinExtend(res, interfaceParam);

                //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Add cache data");
                // Add cache data
                await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            //CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-End InterfaceBll SearchDangTin");
            //return res;
            return new Tuple<IEnumerable<DangTin>, string>(res, item2);
        }

        private async Task<Expression<Func<DangTin, bool>>> ConditionSearchAsyncBuildSitemap(InterfaceParam interfaceParam)
        {
            interfaceParam.lstTrangThai = new List<int> { 1 };
            interfaceParam.ListDeletedIdThanhVien = Variables.ListDeletedIdThanhVien;
            var isGetAll = interfaceParam.isGetAll.HasValue && interfaceParam.isGetAll.Value ? true : false;
            var Year = interfaceParam.nam ?? DateTime.Now.Year;
            var Month = interfaceParam.thang ?? DateTime.Now.Month;

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTin>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTin, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (!interfaceParam.tinhTrang.HasValue || interfaceParam.tinhTrang.Equals(s.TinhTrang ?? 0)) &&
                   (!interfaceParam.paidType.HasValue ||
                        (interfaceParam.paidType.HasValue && ((interfaceParam.paidType.Value == (int)Variables.PaidType.PinTop && s.IsPinTop.Value == true)))) &&
                   (s.IsDuyet.HasValue && s.IsDuyet.Value) &&
                   (!interfaceParam.ListDeletedIdThanhVien.Contains(s.IdThanhVien ?? 0)) &&
                   (interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   (isGetAll == true
                        || (isGetAll == false &&
                            Year == s.NgayTao.Value.Year &&
                            Month == s.NgayTao.Value.Month)) &&

                   dynamicFunctions(s);

            return terms;
        }

        public async Task<Tuple<IEnumerable<DangTin>, string>> SearchDangTinBuildSitemap(InterfaceParam interfaceParam)
        {
            IEnumerable<DangTin> res = null;

            string co = KeyCacheParams.coalesceString;
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalDangTinByParams, tongHopBien);
            string item2 = "";

            object cacheValue = await CacheController.GetCacheByKey(keyCache);

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<DangTin>>(cacheValue);
                item2 = keyCache;
            }
            else
            {

                Expression<Func<DangTin, bool>> terms = await ConditionSearchAsyncBuildSitemap(interfaceParam);
                EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
                res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

                await BuildDangTinExtend(res, interfaceParam);

                // Add cache data
                await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return new Tuple<IEnumerable<DangTin>, string>(res, item2);
        }

        public async Task<Tuple<IEnumerable<DangTin>, string>> SearchDangTinTestHasura(InterfaceParam interfaceParam)
        {
            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin InterfaceBll SearchDangTin");
            IEnumerable<DangTin> res = null;

            string co = KeyCacheParams.coalesceString;
            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-tongHopBien from interfaceParam");
            string tongHopBien = CommonMethods.CoalesceParams(co, interfaceParam);
            string keyCache = string.Format(KeyCacheParams.externalDangTinByParams, tongHopBien);
            string item2 = "";

            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-cacheValue == null");
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTin>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTin, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-repo.GetAll");
            res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-BuildDangTinExtend");
            await BuildDangTinExtend(res, interfaceParam);

            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Add cache data");
            // Add cache data
            await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));


            CommonMethods.WriteLogPerformanceTest($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-End InterfaceBll SearchDangTin");
            //return res;
            return new Tuple<IEnumerable<DangTin>, string>(res, item2);
        }

        public async Task BuildDangTinExtend(IEnumerable<DangTin> source, InterfaceParam interfaceParam)
        {
            if (source == null)
            {
                return;
            }

            string keyCache = string.Empty;
            object cacheValue;

            IEnumerable<DangTinContact> sourceDangTinContact = null;
            IEnumerable<DangTinTraPhi> sourceDangTinTraPhi = null;
            IEnumerable<LayoutAlbum> sourceLayoutAlbum = null;
            IEnumerable<ThanhVien> sourceThanhVien = null;

            foreach (DangTin item in source)
            {
                keyCache = string.Format(KeyCacheParams.externalDangTinContactByParams, Variables.ApiRaoXe + ";IdDangTinContact=" + item.IdDangTinContact);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.DangTinContact_Ext = CommonMethods.DeserializeObject<DangTinContact>(cacheValue);
                }
                else
                {
                    if (sourceDangTinContact == null)
                    {
                        sourceDangTinContact = await Context.DangTinContact.ToListAsync();
                    }

                    item.DangTinContact_Ext = sourceDangTinContact != null ? sourceDangTinContact.SingleOrDefault(a => a.Id == item.IdDangTinContact) : null;
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.DangTinContact_Ext));
                }

                keyCache = string.Format(KeyCacheParams.externalDangTinTraPhiByParams, Variables.ApiRaoXe + ";IdDangTin=" + item.Id + ";NgayPinTop=" + item.NgayPinTop);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null && interfaceParam.isCache.HasValue && interfaceParam.isCache.Value)
                {
                    item.DangTinTraPhi_Ext = CommonMethods.DeserializeObject<DangTinTraPhi>(cacheValue);
                }
                else
                {
                    if (sourceDangTinTraPhi == null)
                    {
                        sourceDangTinTraPhi = await Context.DangTinTraPhi.ToListAsync();
                    }

                    item.DangTinTraPhi_Ext = sourceDangTinTraPhi != null ? sourceDangTinTraPhi.SingleOrDefault(a => a.IdDangTin == item.Id && a.NgayBatDau == item.NgayPinTop
                                                && (a.PinStatus != (int)Variables.PinTopType.StopWaiting && a.PinStatus != (int)Variables.PinTopType.StopRuning)) : null;
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.DangTinTraPhi_Ext));
                }

                keyCache = string.Format(KeyCacheParams.externalLayoutAlbumByParams, Variables.ApiRaoXe + ";MaLayoutAlbum=" + item.MaLayoutAlbum);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.LayoutAlbum_Ext = CommonMethods.DeserializeObject<LayoutAlbum>(cacheValue);
                }
                else
                {
                    if (sourceLayoutAlbum == null)
                    {
                        sourceLayoutAlbum = await Context.LayoutAlbum.ToListAsync();
                    }

                    item.LayoutAlbum_Ext = sourceLayoutAlbum != null ? sourceLayoutAlbum.SingleOrDefault(a => a.Ma == item.MaLayoutAlbum) : null;
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.LayoutAlbum_Ext));
                }

                var idThanhVien = item.IdThanhVien ?? 0;
                keyCache = string.Format(KeyCacheParams.externalThanhVienDLXByParams, Variables.ApiRaoXe + ";ids=" + idThanhVien);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.ThanhVien_Ext = CommonMethods.DeserializeObject<ThanhVien>(cacheValue);
                }
                else
                {
                    if (sourceThanhVien == null)
                    {
                        sourceThanhVien = await Context.ThanhVien.ToListAsync();
                    }

                    item.ThanhVien_Ext = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(a => a.Id == item.IdThanhVien) : null;
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.ThanhVien_Ext));
                }

                // No need to cache this => Handle on Client side
                // Type: 1 => Add favorite
                // Type: 2: => View
                if (item.TinhTrang.HasValue)
                {
                    switch (item.TinhTrang.Value)
                    {
                        case (int)DangTinState.New:
                            item.TinhTrang_Ext = Messages.INFO_TINHTRANGMOI;
                            break;
                        case (int)DangTinState.Old:
                            item.TinhTrang_Ext = Messages.INFO_TINHTRANGCU;
                            break;
                        default:
                            item.TinhTrang_Ext = string.Empty;
                            break;
                    }
                }

                if (item.IsDuyet.HasValue)
                {
                    item.IsDuyet_Ext = item.IsDuyet.Value ? Messages.INFO_DADUYET : Messages.INFO_CHUADUYET + " " + (string.IsNullOrEmpty(item.GhiChu) ? string.Empty : " - " + item.GhiChu);
                }

                if (item.NgayUp.HasValue)
                {
                    TimeSpan remainDay = (DateTime.Now - item.NgayUp.Value);
                    if (remainDay.Days > 0)
                    {
                        item.RemainingTimeUp_Ext = "0";
                    }
                    else
                    {
                        TimeSpan remainTime = new TimeSpan(Variables.HourToUpTin, 0, 0) - new TimeSpan(remainDay.Hours, remainDay.Minutes, remainDay.Seconds);
                        item.RemainingTimeUp_Ext = remainTime.TotalHours > 0 ? DateTime.Now.ToString("yyyy-MM-dd") + " " + remainTime.ToString() : "0";
                    }
                }
                else
                {
                    item.RemainingTimeUp_Ext = "0";
                }

                string deadline = (DateTime.Now > (item.DenNgay ?? DateTime.Now)) ? Messages.INFO_HETHAN : Messages.INFO_CONHAN;
                item.FromToPost_Ext = CommonMethods.FormatDateTime_DD_MM_YYYY_FULL_SLASH((item.TuNgay ?? DateTime.Now)) + " - " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL_SLASH((item.DenNgay ?? DateTime.Now)) + deadline;
            }
        }

        #endregion

        #region "SearchMarketingPlan"

        public async Task<IEnumerable<MarketingPlan>> SearchMarketingPlan(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<MarketingPlan>(interfaceParam.search_dynamicParams);
            Expression<Func<MarketingPlan, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
            (
                (s.UnlimitedTime.HasValue && s.UnlimitedTime.Value) ||
                    ((!s.TuNgay.HasValue || s.TuNgay.HasValue && s.TuNgay <= DateTime.Now) &&
                    (!s.DenNgay.HasValue || s.DenNgay.HasValue && DateTime.Now <= s.DenNgay))
            ) &&
            (s.MaCauHinh.ToLower() == Variables.MACAUHINH_SPECIALDAY.ToLower()) &&
            (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
            (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<MarketingPlan> repo = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request);
            IEnumerable<MarketingPlan> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildMarketingPlanExtend(res, interfaceParam.idThanhVien ?? 0);
            return res;
        }

        public async Task BuildMarketingPlanExtend(IEnumerable<MarketingPlan> source, long idThanhVien)
        {
            if (source == null)
            {
                return;
            }

            IEnumerable<MarketingCode> sourceMarketingCode = null;

            foreach (MarketingPlan item in source)
            {
                if (sourceMarketingCode == null)
                {
                    sourceMarketingCode = await Context.MarketingCode.ToListAsync();
                }

                item.MarketingCode_Ext = sourceMarketingCode != null ? sourceMarketingCode.SingleOrDefault(a => a.IdMarketingPlan == item.Id
                                            && (!a.IdThanhVien.HasValue || a.IdThanhVien.Value == idThanhVien)) : null;
            }
        }

        #endregion

        #region "SearchMarketingPlanActive"

        public async Task<IEnumerable<MarketingPlan>> SearchMarketingPlanActive(InterfaceParam interfaceParam)
        {
            var lstMarketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == interfaceParam.idThanhVien).Result.Select(a => a.IdMarketingPlan).ToList();

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<MarketingPlan>(interfaceParam.search_dynamicParams);
            Expression<Func<MarketingPlan, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
            (!lstMarketingUser.Contains(s.Id)) &&
            (
                (s.UnlimitedTime.HasValue && s.UnlimitedTime.Value) ||
                    ((!s.TuNgay.HasValue || s.TuNgay.HasValue && s.TuNgay <= DateTime.Now) &&
                    (!s.DenNgay.HasValue || s.DenNgay.HasValue && DateTime.Now <= s.DenNgay))
            ) &&
            (s.MaCauHinh.ToLower() == Variables.MACAUHINH_SPECIALDAY.ToLower()) &&
            (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
            (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<MarketingPlan> repo = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request);
            IEnumerable<MarketingPlan> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildMarketingPlanExtend(res, interfaceParam.idThanhVien ?? 0);
            return res;
        }

        #endregion

        #region "SearchMarketingPlanUsedByUser"

        public async Task<List<MarketingPlan>> SearchMarketingPlanUsedByUser(InterfaceParam interfaceParam)
        {
            var lstMarketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == interfaceParam.idThanhVien
                                                        && (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(a.IdMarketingPlan ?? 0))).Result.ToList();
            List<MarketingPlan> lstMarketingPlan = new List<MarketingPlan>();
            foreach (var marketingUser in lstMarketingUser)
            {
                //marketingUser.IdMarketingPlan
                interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.lstId = new List<long> { marketingUser.IdMarketingPlan ?? 0 };
                var marketingPlan = (await this.SearchMarketingPlan(interfaceParam)).ToList();
                lstMarketingPlan.AddRange(marketingPlan);
            }

            return lstMarketingPlan;
        }

        #endregion

        #region UserPoint

        public async Task<IEnumerable<UserPoint>> searchUserPoint(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserPoint>(interfaceParam.dynamicParam);

            Expression<Func<UserPoint, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id))
            && interfaceParam.idThanhVien == s.IdThanhVien
            && dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserPoint> repo = new EntityBaseRepositoryRaoVat<UserPoint>(Request);
            IEnumerable<UserPoint> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString);

            await BuildUserPointExtend(res, interfaceParam);
            return res;
        }

        private async Task BuildUserPointExtend(IEnumerable<UserPoint> res, InterfaceParam interfaceParam)
        {
            string ratePointToTimeString = await GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_TIME);
            int ratePointToTime = CommonMethods.ConvertToInt32(ratePointToTimeString);

            IEnumerable<ThanhVien> sourceThanhVien = null;
            IEnumerable<DangTinTraPhi> sourceDangTinTraPhi = null;

            long tongThoiGianDaMua = 0;
            long tongThoiGianDaDung = 0;

            foreach (UserPoint usp in res)
            {
                usp.RatePointToTime_Ext = ratePointToTime;

                if (sourceThanhVien == null)
                {
                    sourceThanhVien = await Context.ThanhVien.ToListAsync();
                }
                var thanhVien = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(n => n.Id == usp.IdThanhVien) : null;

                if (sourceDangTinTraPhi == null)
                {
                    sourceDangTinTraPhi = await Context.DangTinTraPhi.ToListAsync();
                }

                tongThoiGianDaMua = thanhVien != null ? (thanhVien.TotalTime ?? 0) : 0;
                tongThoiGianDaDung = sourceDangTinTraPhi != null ?
                    (sourceDangTinTraPhi.Where(n => n.IdThanhVienTao == usp.IdThanhVien && n.PinStatus != (int)Variables.PinTopType.StopWaiting).Sum(n => n.ThoiGianSuDung ?? n.ThoiGianSuDungDuDinh) ?? 0) : 0;

                usp.TimeDangTin_Ext = tongThoiGianDaMua - tongThoiGianDaDung;
                usp.ThanhVien_Ext = thanhVien;
            }
        }

        #endregion

        #region ThanhVien

        public ThanhVien getThanhVienById(long id)
        {
            return RepoThanhVien.Find(a => a.Id == id).Result.FirstOrDefault();
        }

        #endregion

        #region UserRank

        public UserRank getUserRankById(long id)
        {
            return RepoUserRank.Find(a => a.Id == id).Result.FirstOrDefault();
        }

        #endregion

        #region UserRank

        public UserPoint getUserPointById(long id)
        {
            return RepoUserPoint.Find(a => a.Id == id).Result.FirstOrDefault();
        }

        #endregion

        #region "GetLastLogin"

        public IEnumerable<UserDevice> GetLastLogin(InterfaceParam interfaceParam)
        {
            // handle params
            var lstIdThanhVien = interfaceParam.idsThanhVien.Split(",").Select(Int64.Parse).ToList();

            List<UserDevice> lstUserDevice = new List<UserDevice>();

            EntityBaseRepositoryRaoVat<UserDevice> repo = new EntityBaseRepositoryRaoVat<UserDevice>(Request);

            foreach (var item in lstIdThanhVien)
            {
                var lastLoginDate = repo.Find(a => a.UserId == item).Result.OrderByDescending(a => a.LoginDate).FirstOrDefault();
                if (lastLoginDate != null)
                {
                    lstUserDevice.Add(lastLoginDate);
                }
            }

            return lstUserDevice;
        }

        #endregion

        #region StatisticActivitiesUser

        public List<ThanhVien> GetAllThanhVien()
        {
            var lstThanhVien = Context.ThanhVien.ToList();

            return lstThanhVien;
        }

        public async Task<IEnumerable<StatisticActivitiesUser>> searchThanhVienTichCuc(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<StatisticActivitiesUser>(interfaceParam.dynamicParam);
            var currentDateTime = DateTime.Now;
            interfaceParam.year = interfaceParam.year ?? currentDateTime.Year;
            interfaceParam.month = interfaceParam.month ?? currentDateTime.Month;
            // Sort by target:
            // 1. DangTin
            // 2. UpTin
            // 3. LienHe => pending
            interfaceParam.orderString = "CountDangTin:desc,CountUpTin:desc";
            Expression<Func<StatisticActivitiesUser, bool>> terms = s =>
            interfaceParam.year == s.Year
            && interfaceParam.month == s.Month
            && dynamicFunctions(s);

            interfaceParam.displayPage = -1;
            interfaceParam.displayItems = -1;

            EntityBaseRepositoryRaoVat<StatisticActivitiesUser> repo = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request);
            IEnumerable<StatisticActivitiesUser> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString);

            // only get 5 Top
            res = res.Where(a => (((a.CountDangTin ?? 0) * Variables.DevideCountDangTin) + ((a.CountUpTin ?? 0) * Variables.DevideCountUpTin)) > 0)
                    .OrderByDescending(a => ((a.CountDangTin ?? 0) * Variables.DevideCountDangTin) + ((a.CountUpTin ?? 0) * Variables.DevideCountUpTin))
                    .ThenByDescending(a => a.CountDangTin)
                    .ThenByDescending(a => a.CountUpTin)
                    .ThenByDescending(a => a.NgayCapNhat)
                    .Take(5);

            await BuildThanhVienTichCuc(res, interfaceParam);
            return res;
        }

        private async Task BuildThanhVienTichCuc(IEnumerable<StatisticActivitiesUser> res, InterfaceParam interfaceParam)
        {
            IEnumerable<ThanhVien> sourceThanhVien = null;
            foreach (StatisticActivitiesUser item in res)
            {
                if (sourceThanhVien == null)
                {
                    sourceThanhVien = await Context.ThanhVien.ToListAsync();
                }
                var thanhVien = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(n => n.Id == item.IdThanhVien) : null;
                item.ThanhVien_Ext = thanhVien;
            }
        }

        #endregion

    }
}
