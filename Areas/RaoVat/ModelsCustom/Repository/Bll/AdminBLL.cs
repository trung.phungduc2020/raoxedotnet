﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.Models.Base;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static Variables;

namespace raoVatApi.ModelsRaoVat
{
    public class AdminBll
    {
        private HttpRequest _request;

        public HttpRequest Request
        {
            get
            {
                //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
                //if (_request == null)
                {
                    try
                    {
                        HttpContextAccessor accessor = new HttpContextAccessor();
                        _request = accessor.HttpContext == null ? null : accessor.HttpContext.Request;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        CommonMethods.WriteLog(error);
                    }
                }

                return _request;
            }
            set
            {
                _request = value;
            }
        }

        private RaoVatContext_Custom _context;

        public RaoVatContext_Custom Context
        {
            get
            {
                return new RaoVatContext_Custom();
            }
        }

        private RawSqlRepository _rawSQLRepo;

        public RawSqlRepository RawSQLRepo
        {
            get
            {
                return new RawSqlRepository(new RaoVatContext_Custom());
            }
            set
            {
                _rawSQLRepo = value;
            }
        }

        private List<int> _lstTrangThai_1 = new List<int> { 1 };

        private MemoryCacheControllerBase _cacheController;
        public MemoryCacheControllerBase CacheController { get => _cacheController == null ? new MemoryCacheControllerBase() : _cacheController; set => _cacheController = value; }

        private ApiGetDal _apiGetDal;
        public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        private EntityBaseRepositoryRaoVat<DangTinFavorite> _repoDangTinFavorite;
        public EntityBaseRepositoryRaoVat<DangTinFavorite> RepoDangTinFavorite { get => _repoDangTinFavorite == null ? _repoDangTinFavorite = new EntityBaseRepositoryRaoVat<DangTinFavorite>() : _repoDangTinFavorite; set => _repoDangTinFavorite = value; }

        private EntityBaseRepositoryRaoVat<DangTinTraPhi> _repoDangTinTraPhi;
        public EntityBaseRepositoryRaoVat<DangTinTraPhi> RepoDangTinTraPhi { get => _repoDangTinTraPhi == null ? _repoDangTinTraPhi = new EntityBaseRepositoryRaoVat<DangTinTraPhi>() : _repoDangTinTraPhi; set => _repoDangTinTraPhi = value; }

        private EntityBaseRepositoryRaoVat<LayoutAlbum> _repoLayoutAlbum;
        public EntityBaseRepositoryRaoVat<LayoutAlbum> RepoLayoutAlbum { get => _repoLayoutAlbum == null ? _repoLayoutAlbum = new EntityBaseRepositoryRaoVat<LayoutAlbum>() : _repoLayoutAlbum; set => _repoLayoutAlbum = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>() : _repoDangTinContact; set => _repoDangTinContact = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCode;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCode { get => _repoMarketingCode == null ? _repoMarketingCode = new EntityBaseRepositoryRaoVat<MarketingCode>(true) : _repoMarketingCode; set => _repoMarketingCode = value; }

        public AdminBll()
        {
            _context = new RaoVatContext_Custom();
        }

        #region "Commons"

        private bool CheckListIntIsAllowSearch(List<int> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1)
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        private bool CheckListIntIsAllowSearch(List<long> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1)
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        public async Task<IEnumerable<Dictionary<string, object>>> GetColumnMaximumLength()
        {
            //CommonMethods.WriteLog($"GetColumnMaximumLength _con: {RawSQLRepo.ConnectionString}");
            //CommonMethods.WriteLog($"GetColumnMaximumLength dbName: {RawSQLRepo.DatabaseName}");
            //RawSQLRepo = new RawSqlRepository(new RaoVatContext_Custom(Variables.ConnectionString));
            string dbName = RawSQLRepo.DatabaseName;
            string sql = @"SELECT TABLE_NAME, COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM " + dbName + ".INFORMATION_SCHEMA.COLUMNS WHERE CHARACTER_MAXIMUM_LENGTH > 0 AND TABLE_CATALOG = '" + dbName + "'";
            //CommonMethods.WriteLog($"GetColumnMaximumLength sql: {sql}");
            IEnumerable<Dictionary<string, object>> res = await RawSQLRepo.ExecuteSqlQuery_ReturnDic(sql);
            return res;
        }

        #endregion

        #region MenuRaoVat

        public async Task<IEnumerable<EntityForTreeTable<MenuRaoVat>>> SearchMenuForTreeTable(InterfaceParam interfaceParam)
        {
            List<EntityForTreeTable<MenuRaoVat>> res = new List<EntityForTreeTable<MenuRaoVat>>();

            if ((interfaceParam.lstCapDo == null || interfaceParam.lstCapDo.Count == 0) && (interfaceParam.lstId == null || interfaceParam.lstId.Count == 0)) // không lọc cấp 1 (mặc định) nếu tìm theo id
            {
                interfaceParam.lstCapDo = new List<int>() { 1 }; // chỉ lọc Cấp 1
            }

            IEnumerable<MenuRaoVat> lstMenu = await SearchMenuRaoVat(interfaceParam);
            if (lstMenu != null)
            {
                MenuRaoVatRepositoryRaoVat fRepo = new MenuRaoVatRepositoryRaoVat(Request);
                int totalRow = 0;
                foreach (MenuRaoVat f in lstMenu)
                {
                    totalRow = f.TotalRow;

                    Expression<Func<MenuRaoVat, bool>> terms = s => ((interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count < 1) || interfaceParam.lstTrangThai.Contains(s.TrangThai.GetValueOrDefault(1)))
                    && (interfaceParam.lstNotId == null || interfaceParam.lstNotId.Count < 1 || !interfaceParam.lstNotId.Contains(s.Id));

                    res.Add(new EntityForTreeTable<MenuRaoVat>(fRepo, f.Id, totalRow, terms));
                }
            }

            return res;
        }

        public async Task<IEnumerable<MenuRaoVat>> SearchMenuRaoVat(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<MenuRaoVat>(interfaceParam.dynamicParam);

            Expression<Func<MenuRaoVat, bool>> terms = s =>
                (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstCapDo, false) || interfaceParam.lstCapDo.Contains(s.CapDo.GetValueOrDefault(0))) &&
                (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstTrangThai, true) || interfaceParam.lstTrangThai.Contains(s.TrangThai.GetValueOrDefault(1))) &&
                ((interfaceParam.lstIdMenuParent == null || interfaceParam.lstIdMenuParent.Count() == 0) || (interfaceParam.lstIdMenuParent.Contains(s.IdMenuParent ?? 0))) &&
                dynamicFunctions(s);

            Expression<Func<MenuRaoVat, object>>[] includeProperties = null;

            MenuRaoVatRepositoryRaoVat repo = new MenuRaoVatRepositoryRaoVat(Request);
            IEnumerable<MenuRaoVat> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, includeProperties);
            await BuildMenuRaoVatExtend(res);
            return res;
        }

        public async Task BuildMenuRaoVatExtend(IEnumerable<MenuRaoVat> source)
        {
            if (source == null)
            {
                return;
            }

            foreach (MenuRaoVat item in source)
            {
                item.TenMenu_Ext = CommonMethods.AddCapDoString(item.CapDo.HasValue ? item.CapDo.Value : 0) + item.TenMenu;

                // FielDaiDien
                if (item.IdFileDaiDien.HasValue && item.IdFileDaiDien.Value > 0)
                {
                    //item.FileDaiDien_Ext = new ShortFiles(await Context.FindAsync<Files>(item.IdFileDaiDien.GetValueOrDefault()), 1, "");
                }
            }
        }

        #endregion

        #region "User"

        public async Task<IEnumerable<ThanhVien>> SearchThanhVien(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<ThanhVien>(interfaceParam.search_dynamicParams);

            Expression<Func<ThanhVien, bool>> terms = s =>
                (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                (CheckListIntIsAllowSearch(interfaceParam.lstTrangThai, true) || interfaceParam.lstTrangThai.Contains(s.TrangThai.GetValueOrDefault(1))) &&
                (!interfaceParam.originalFrom.HasValue || (interfaceParam.originalFrom.Value == s.OriginalFrom)) &&
                dynamicFunctions(s);

            Expression<Func<ThanhVien, object>>[] includeProperties = null;

            EntityBaseRepositoryRaoVat<ThanhVien> repo = new EntityBaseRepositoryRaoVat<ThanhVien>();
            IEnumerable<ThanhVien> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, includeProperties);
            await BuildThanhVienExtend(res, interfaceParam);

            return res;
        }

        public async Task BuildThanhVienExtend(IEnumerable<ThanhVien> source, InterfaceParam interfaceParam)
        {
            if (source == null)
            {
                return;
            }

            if (interfaceParam.allIncluding == true)
            {
                InterfaceParam paramCallTinhThanh = new InterfaceParam();
                InterfaceParam paramCallQuanHuyen = new InterfaceParam();

                foreach (ThanhVien item in source)
                {
                    paramCallTinhThanh.ids = item.IdTinhThanh.ToString();
                    paramCallQuanHuyen.ids = item.IdQuanHuyen.ToString();

                    // TenTinhThanh - TenQuanHuyen
                    var TenTinhThanh = await ApiGetDal.SearchTinhThanh(paramCallTinhThanh);
                    var TenQuanHuyen = await ApiGetDal.SearchQuanHuyen(paramCallQuanHuyen);

                    item.TenTinhThanh = TenTinhThanh;
                    item.TenQuanHuyen = TenQuanHuyen;
                }
            }
        }

        public async Task<int> SendMailXacNhanTaiKhoan(SendMailModel mailModel, DateTime ngayYeuCau)
        {
            EntityBaseRepositoryRaoVat<MaXacNhan> maXacNhanRepoMaster = new EntityBaseRepositoryRaoVat<MaXacNhan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master); // phải khởi tạo newRepo ở đây, ko dùng Repo toàn cục vì sẽ ko lưu được hết các instanse

            // Set LoaiXacNhan để kiểm tra khi Xác nhận     
            MaXacNhan newMXN = await RandomMaXacNhanReturnEntity((byte)Variables.LoaiXacNhan.XacNhanTaiKhoan, mailModel, ngayYeuCau);

            if (newMXN == null || string.IsNullOrEmpty(newMXN.Code))
            {
                return -1;
            }

            MaXacNhan insertMXN = (await maXacNhanRepoMaster.InsertReturnEntity(newMXN)).FirstOrDefault();
            //insertMXN.TuDienMaXacNhan_Ext = newMXN.TuDienMaXacNhan_Ext; // để lấy thông tin gửi mail trong hàm SendMailXacNhanTaiKhoan
            if (insertMXN != null && insertMXN.Id > 0)
            {
                // Gửi mail
                SendMail sd = new SendMail();
                await sd.SendMailXacNhanTaiKhoan(mailModel, insertMXN);
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public async Task<MaXacNhan> RandomMaXacNhanReturnEntity(byte loaiXacNhan, SendMailModel thanhVien, DateTime ngayYeuCau, bool checkHopLe = true)
        {
            //// nguyencuongcs 20180306: có một cách để tránh trùng mã random nữa là tạo Random rd bằng biến static trong Variables.cs
            //Random rd = new Random();
            //EntityBaseRepositoryRaoVat<TuDienMaXacNhan> repo = new EntityBaseRepositoryRaoVat<TuDienMaXacNhan>(Request);

            //int randomId = 0;
            //TuDienMaXacNhan tuDien = null;
            //MaXacNhan usedMaXacNhan = null;
            //MaXacNhan res = new MaXacNhan();
            //string ma64 = "";
            //int maxLoop = 20;
            //int maxIdTuDien = await repo.GetMaxId();

            //do
            //{
            //    maxLoop--;
            //    randomId = await RandomIdMaXacNhan(rd); // nếu truyền rd vào sẽ hạn chế bớt lặp lại do mỗi khi new Random() thì sẽ chạy lại random từ đầu -> trùng lại                

            //    while (randomId > maxIdTuDien)
            //    {
            //        randomId = randomId / 2; // * DateTime.Now.Millisecond / DateTime.Now.Minute;
            //    }

            //    tuDien = await Context.FindAsync<TuDienMaXacNhan>(randomId);
            //    ma64 = tuDien != null ? CommonMethods.GetEncryptMD5(tuDien.MaXacNhan) : "";

            //    res.IdAdmin = thanhVien.IdUser;
            //    res.NgayCap = DateTime.Now;
            //    res.MaXacNhan64 = ma64;
            //    res.Email = thanhVien.Email;
            //    res.DienThoai = thanhVien.DienThoai;
            //    res.NgayCap = DateTime.Now; // ngayYeuCau: ko quan tâm ngày yêu cầu vì mối nơi mỗi khác => truyền vào có thể dùng để get milisecond ra random
            //    res.LoaiXacNhan = loaiXacNhan;
            //    res.MappingId = tuDien.Id;

            //    res.TuDienMaXacNhan = tuDien;

            //    if (checkHopLe && !string.IsNullOrEmpty(ma64)) // check hợp lệ (chưa được sử dụng, gửi đi cho thành viên khác xác nhận)
            //    {
            //        usedMaXacNhan = (await SearchMaXacNhanByMa(ma64, "")).FirstOrDefault();
            //        if (usedMaXacNhan != null) // Đã tồn tại trong bảng MaXacNhan là đã được sử dụng
            //        {
            //            ma64 = ""; // gán res = "" để chạy lại vòng lặp
            //        }
            //    }
            //}
            //while (string.IsNullOrEmpty(ma64) && maxLoop > 0); // Nếu res có giá trị thì ko lặp nữa hoặc chạy maxLoop lần, res có khả năng = "" vì MaXacNhan sau khi được sử dụng sẽ xóa ra khỏi TuDienMaXacNhan

            //return res;
            return null;
        }

        #endregion

        #region "MaXacNhan"

        public async Task<IEnumerable<MaXacNhan>> SearchMaXacNhan(InterfaceParam interfaceParam)
        {

            interfaceParam.email = interfaceParam.email.ToASCII().ToLower();
            interfaceParam.dienThoai = interfaceParam.dienThoai.ToASCII().ToLower();

            Expression<Func<MaXacNhan, bool>> terms = s =>
                (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                (interfaceParam.loaiXacNhan < 1 || s.LoaiXacNhan == interfaceParam.loaiXacNhan) &&
                (string.IsNullOrEmpty(interfaceParam.email) || s.Email.ToASCII().ToLower().Contains(interfaceParam.email)) &&
                (string.IsNullOrEmpty(interfaceParam.dienThoai) || s.Phone.ToASCII().ToLower().Contains(interfaceParam.dienThoai)) &&
                (string.IsNullOrEmpty(interfaceParam.maXacNhan) || s.Code.Equals(interfaceParam.maXacNhan));

            EntityBaseRepositoryRaoVat<MaXacNhan> repo = new EntityBaseRepositoryRaoVat<MaXacNhan>(Request);
            IEnumerable<MaXacNhan> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            return res;
        }

        #endregion

        #region "CauHinhHeThong"

        public async Task<IEnumerable<CauHinhHeThong>> SearchCauHinhHeThong(int currentPage, int displayItems, List<long> lstId, string maCauHinh, string giaTriCauHinh, string search_dynamicParams, bool allIncluding,
            string orderString)
        {

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<CauHinhHeThong>(search_dynamicParams);

            Expression<Func<CauHinhHeThong, bool>> terms = s =>
                (CheckListIntIsAllowSearch(lstId, false) || lstId.Contains(s.Id)) &&
                (string.IsNullOrEmpty(maCauHinh) || s.MaCauHinh.NullIsEmpty().Contains(maCauHinh)) &&
                (string.IsNullOrEmpty(giaTriCauHinh) || s.GiaTriCauHinh.NullIsEmpty().Contains(giaTriCauHinh)) &&
                dynamicFunctions(s);

            Expression<Func<CauHinhHeThong, object>>[] includeProperties = null;

            if (allIncluding)
            {
                includeProperties = new Expression<Func<CauHinhHeThong, object>>[] { };
            }

            EntityBaseRepositoryRaoVat<CauHinhHeThong> repo = new EntityBaseRepositoryRaoVat<CauHinhHeThong>(Request);

            if (string.IsNullOrEmpty(orderString))
            {
                orderString = "MaCauHinh";
            }

            return await repo.GetAll(terms, currentPage, displayItems, orderString, includeProperties);
        }

        public async Task<IEnumerable<CauHinhHeThong>> SearchCauHinhHeThongByIds(string ids, string orderString)
        {
            List<long> lstId = CommonMethods.SplitStringToInt64(ids);

            return await SearchCauHinhHeThongByIds(lstId, orderString);
        }

        public async Task<IEnumerable<CauHinhHeThong>> SearchCauHinhHeThongByIds(List<long> lstId, string orderString)
        {
            return await SearchCauHinhHeThong(-1, -1, lstId, "", "", null, false, orderString);
        }

        public async Task<string> GetGiaTriCauHinhHeThongByMa(string maCauHinh)
        {
            EntityBaseRepositoryRaoVat<CauHinhHeThong> repo = new EntityBaseRepositoryRaoVat<CauHinhHeThong>(Request);
            CauHinhHeThong res = await repo.GetSingle(s => s.MaCauHinh == maCauHinh);
            return res != null ? res.GiaTriCauHinh : "";
        }

        public async Task<int> GetCauHinhChieuRongHinhAnh(string maCauHinh)
        {
            string chieuRong = await GetGiaTriCauHinhHeThongByMa(maCauHinh);

            if (!string.IsNullOrEmpty(chieuRong))
            {
                return CommonMethods.ConvertToInt32(chieuRong);
            }
            return Variables.DEFAULT_WIDTH_IMAGE;
        }

        public async Task<int> GetCauHinhChieuCaoHinhAnh(string maCauHinh)
        {
            string chieuCao = await GetGiaTriCauHinhHeThongByMa(maCauHinh);

            if (!string.IsNullOrEmpty(chieuCao))
            {
                return CommonMethods.ConvertToInt32(chieuCao);
            }
            return Variables.DEFAULT_HEIGHT_IMAGE;
        }

        // nguyencuongcs 20191107
        public async Task<IEnumerable<string>> WebFarm_GetDanhSachWebServer()
        {
            if (Variables.WebFarm_ListWebServer != null && Variables.WebFarm_ListWebServer.Count() > 0)
            {
                return Variables.WebFarm_ListWebServer;
            }

            IEnumerable<string> res = null;
            try
            {
                string maCauHinh = "";
                if (!CommonMethods.CheckUseDevelopment())
                {
                    maCauHinh = Variables.MACAUHINH_FARMS_WEBSERVER_PRODUCTION;
                }
                else
                {
                    maCauHinh = Variables.MACAUHINH_FARMS_WEBSERVER_DEVELOPMENT;
                }

                string giaTriCauHinh = await GetGiaTriCauHinhHeThongByMa(maCauHinh);

                if (!string.IsNullOrEmpty(giaTriCauHinh))
                {
                    res = giaTriCauHinh.Split(Variables.COMMON_KEY_SPLIT);
                }

                CommonMethods.WriteLog("WebFarm_GetDanhSachWebServer: " + (res != null ? res.Count() : 0).ToString());
                CommonMethods.WriteLog("  + maCauHinh: " + maCauHinh);
                CommonMethods.WriteLog("  + res: " + res);
                if (res != null)
                {
                    string item = "";

                    for (int i = 0; i < res.Count(); i++)
                    {
                        item = res.ElementAt(i);
                        CommonMethods.WriteLog($"WebFarm {i} : {item}");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                throw;
            }

            return res;
        }

        public async Task<IEnumerable<string>> ApiDailyXeFarm_GetDanhSachWebServer()
        {
            if (Variables.ApiDailyXeFarm_ListWebServer != null && Variables.ApiDailyXeFarm_ListWebServer.Count() > 0)
            {
                return Variables.ApiDailyXeFarm_ListWebServer;
            }

            IEnumerable<string> res = null;
            try
            {
                string maCauHinh = "";
                if (!CommonMethods.CheckUseDevelopment())
                {
                    maCauHinh = Variables.MACAUHINH_FARMS_APIDAILYXE_PRODUCTION;
                }
                else
                {
                    maCauHinh = Variables.MACAUHINH_FARMS_APIDAILYXE_DEVELOPMENT;
                }

                string giaTriCauHinh = await GetGiaTriCauHinhHeThongByMa(maCauHinh);

                if (!string.IsNullOrEmpty(giaTriCauHinh))
                {
                    res = giaTriCauHinh.Split(Variables.COMMON_KEY_SPLIT);

                    Variables.ApiDailyXeFarm_ListWebServer = res;
                }

                CommonMethods.WriteLog("ApiDailyXeFarm_GetDanhSachWebServer: " + (res != null ? res.Count() : 0).ToString());
                CommonMethods.WriteLog("  + maCauHinh: " + maCauHinh);
                CommonMethods.WriteLog("  + res: " + res);
                if (res != null)
                {
                    string item = "";

                    for (int i = 0; i < res.Count(); i++)
                    {
                        item = res.ElementAt(i);
                        CommonMethods.WriteLog($"WebFarm {i} : {item}");
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                throw;
            }

            return res;
        }

        #endregion

        //#region "Giao dich xe"

        //public async Task<IEnumerable<GiaoDichXe>> SearchGiaoDichXe(InterfaceParam interfaceParam)
        //{
        //    DynamicParams dynParams = new DynamicParams();
        //    var dynamicFunctions = await dynParams.BuildDynamicExpression<GiaoDichXe>(interfaceParam.dynamicParam);

        //    Expression<Func<GiaoDichXe, bool>> terms = s =>
        //        (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
        //        (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
        //        (!interfaceParam.idNguoiBan.HasValue || (interfaceParam.idNguoiBan.Value == s.IdNguoiBan.Value)) &&
        //        (CheckListIntIsAllowSearch(interfaceParam.lstTransactionStatus, true) || (interfaceParam.lstTransactionStatus.Contains(s.TransactionStatus.Value))) &&
        //        (!interfaceParam.transactionMethod.HasValue || (interfaceParam.transactionMethod.Value.Equals(s.TransactionMethod.Value))) &&
        //        (!interfaceParam.commissionStatus.HasValue || (interfaceParam.commissionStatus.Value.Equals(s.CommissionStatus.Value))) &&
        //        (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&
        //        (CheckListIntIsAllowSearch(interfaceParam.lstTrangThai, true) || interfaceParam.lstTrangThai.Contains(s.TrangThai.GetValueOrDefault(1))) &&
        //        dynamicFunctions(s);

        //    GiaoDichXeRepositoryRaoVat repo = new GiaoDichXeRepositoryRaoVat(Request);
        //    IEnumerable<GiaoDichXe> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

        //    await BuildGiaoDichXeExtend(res, interfaceParam);

        //    return res;
        //}

        //public async Task BuildGiaoDichXeExtend(IEnumerable<GiaoDichXe> source, InterfaceParam param = null)
        //{
        //    try
        //    {
        //        if (source == null)
        //        {
        //            return;
        //        }

        //        InterfaceParam interfaceParam = new InterfaceParam();

        //        foreach (var item in source)
        //        {
        //            if (item.TransactionStatus.HasValue)
        //            {
        //                switch (item.TransactionStatus.Value)
        //                {
        //                    case (int)Variables.TransactionStatus.ChoXe:
        //                        item.TransactionStatus_Ext = "Chờ xe";
        //                        break;
        //                    case (int)Variables.TransactionStatus.DaKyHopDong:
        //                        item.TransactionStatus_Ext = "Đã ký hợp đồng";
        //                        break;
        //                    case (int)Variables.TransactionStatus.HetHang:
        //                        item.TransactionStatus_Ext = "Hết hàng";
        //                        break;
        //                    case (int)Variables.TransactionStatus.KetThuc:
        //                        item.TransactionStatus_Ext = "Kết thúc";
        //                        break;
        //                    case (int)Variables.TransactionStatus.Treo:
        //                        item.TransactionStatus_Ext = "Treo";
        //                        break;
        //                    case (int)Variables.TransactionStatus.DaChuyenKhoanHoaHong:
        //                        item.TransactionStatus_Ext = "Đã chuyển khoản Hoa Hồng";
        //                        break;
        //                    default:
        //                        item.TransactionStatus_Ext = string.Empty;
        //                        break;
        //                }
        //            }

        //            if (item.TransactionMethod.HasValue)
        //            {
        //                switch (item.TransactionMethod.Value)
        //                {
        //                    case (int)Variables.TransactionMethod.Cash:
        //                        item.TransactionMethod_Ext = "Tiền mặt";
        //                        break;
        //                    case (int)Variables.TransactionMethod.Installment:
        //                        item.TransactionMethod_Ext = "Trả góp";
        //                        break;
        //                    default:
        //                        item.TransactionMethod_Ext = string.Empty;
        //                        break;
        //                }
        //            }

        //            //item.ThanhVienMua_Ext = new JObject();
        //            //item.ThanhVienBan_Ext = new JObject();
        //            //item.Admin_Ext = new JObject();

        //            //CommonMethodsRaoVat commonFunction = new CommonMethodsRaoVat();
        //            //interfaceParam = new InterfaceParam();
        //            //// Buyer
        //            //if (item.IdNguoiMua.HasValue && item.IdNguoiMua.Value > 0)
        //            //{
        //            //    interfaceParam = new InterfaceParam();
        //            //    interfaceParam.id = item.IdNguoiMua.Value.ToString();
        //            //    interfaceParam.token = param != null ? param.token : string.Empty;
        //            //    item.ThanhVienMua_Ext = await commonFunction.GetThanhVien(interfaceParam);
        //            //}

        //            //// Seller
        //            //if (item.IdNguoiBan.HasValue && item.IdNguoiBan.Value > 0)
        //            //{
        //            //    interfaceParam = new InterfaceParam();
        //            //    interfaceParam.id = item.IdNguoiBan.Value.ToString();
        //            //    interfaceParam.token = param != null ? param.token : string.Empty;
        //            //    item.ThanhVienBan_Ext = await commonFunction.GetThanhVien(interfaceParam);
        //            //}

        //            // Admin
        //            //if (item.IdAdmin.HasValue && item.IdAdmin.Value > 0)
        //            //{
        //            //    interfaceParam = new InterfaceParam();
        //            //    interfaceParam.id = item.IdAdmin.Value.ToString();
        //            //    item.Admin_Ext = commonFunction.GetAdminNoAsync(interfaceParam);
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        //public async Task<IEnumerable<HistoryTransaction>> SearcHistoryTransaction(InterfaceParam interfaceParam)
        //{
        //    DynamicParams dynParams = new DynamicParams();
        //    var dynamicFunctions = await dynParams.BuildDynamicExpression<HistoryTransaction>(interfaceParam.dynamicParam);

        //    Expression<Func<HistoryTransaction, bool>> terms = s =>
        //        (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.IdGiaoDichXe ?? 0)) &&
        //        dynamicFunctions(s);

        //    Expression<Func<HistoryTransaction, object>>[] includeProperties = null;

        //    HistoryTransactionRepositoryRaoVat repo = new HistoryTransactionRepositoryRaoVat(Request);
        //    IEnumerable<HistoryTransaction> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, includeProperties);

        //    BuildHistoryTransactionExtend(res);

        //    return res;
        //}

        //public async Task BuildHistoryTransactionExtend(IEnumerable<HistoryTransaction> source)
        //{
        //    if (source == null)
        //    {
        //        return;
        //    }

        //    foreach (HistoryTransaction item in source)
        //    {
        //        item.NgayTao_Ext = item.NgayTao.ToString(Variables.DD_MM_YYYY_FULL);
        //    }
        //}

        //#endregion

        #region "Đăng tin"

        private async Task<Expression<Func<DangTin, bool>>> ConditionSearchAsync(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            decimal? GiaTu = string.IsNullOrEmpty(interfaceParam.giaTu) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaTu);
            decimal? GiaDen = string.IsNullOrEmpty(interfaceParam.giaDen) ? (decimal?)null : CommonMethods.ConvertToDecimal(interfaceParam.giaDen);
            List<long> lstIdMenu = string.IsNullOrEmpty(interfaceParam.idMenu) ? null : CommonMethods.SplitStringToInt64(interfaceParam.idMenu);
            List<int> lstType = string.IsNullOrEmpty(interfaceParam.type) ? null : CommonMethods.SplitStringToInt(interfaceParam.type, true);
            //interfaceParam.checkExpireBool = CommonMethods.ConvertToBoolean(interfaceParam.checkExpire, null); // kiểm tra tin đăng có đang còn tồn tại hày không
            //interfaceParam.ListDeletedIdThanhVien = Variables.ListDeletedIdThanhVien;
            // Except User with TrangThai != 1

            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTin>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTin, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstIdTinhThanh, false) || interfaceParam.lstIdTinhThanh.Contains(s.IdTinhThanh ?? 0)) &&
                   (!interfaceParam.tinhTrang.HasValue || interfaceParam.tinhTrang.Equals(s.TinhTrang ?? 0)) &&
                   (!interfaceParam.idThanhVien.HasValue || interfaceParam.idThanhVien == s.IdThanhVien.Value) &&
                   //(!interfaceParam.timeEffective.HasValue
                   //     || (interfaceParam.timeEffective.Value == 1 && s.TuNgay <= DateTime.Now && DateTime.Now <= s.DenNgay)
                   //     || (interfaceParam.timeEffective.Value == 0 && DateTime.Now < s.TuNgay && DateTime.Now > s.DenNgay)) &&
                   (!interfaceParam.paidType.HasValue ||
                        (interfaceParam.paidType.HasValue && ((interfaceParam.paidType.Value == (int)Variables.PaidType.PinTop && s.IsPinTop.Value == true)))) &&
                   (!GiaTu.HasValue || (GiaTu.Value <= (s.Gia ?? 0))) &&
                   (!GiaDen.HasValue || (GiaDen.Value >= (s.Gia ?? 0))) &&
                   ((lstIdMenu == null || lstIdMenu.Count <= 0) || lstIdMenu.Contains(s.IdMenu ?? 0)) &&
                   //(interfaceParam.checkExpireBool == null || interfaceParam.checkExpireBool == false || (s.TuNgay.NullIsMinValue().Date <= DateTime.Now.Date && DateTime.Now.Date <= s.DenNgay.NullIsMinValue().Date)) &&
                   ((!interfaceParam.isDuyet.HasValue) ||
                       ((!interfaceParam.isDuyet.Value) && (interfaceParam.isDuyet.Value == s.IsDuyet || !s.IsDuyet.HasValue)) ||
                       (interfaceParam.isDuyet == s.IsDuyet)) &&

                   (((!interfaceParam.isPinTop.HasValue) ||
                       ((!interfaceParam.isPinTop.Value) && (interfaceParam.isPinTop.Value == s.IsPinTop || !s.IsPinTop.HasValue)) ||
                       (interfaceParam.isPinTop == s.IsPinTop))) &&
                    (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.ToLower().Contains(interfaceParam.tuKhoaTimKiem.ToLower())) &&

                    (interfaceParam.idAdminDangKyDuyet == -1 || (interfaceParam.idAdminDangKyDuyet == 0 && !s.IdAdminDangKyDuyet.HasValue)
                                                            || (interfaceParam.idAdminDangKyDuyet > 0 && interfaceParam.idAdminDangKyDuyet == s.IdAdminDangKyDuyet)) &&

                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   ((lstType == null || lstType.Count <= 0) || lstType.Contains(s.Type ?? 0)) &&
                   dynamicFunctions(s);

            return terms;

        }

        public async Task<IEnumerable<DangTin>> SearchDangTin(InterfaceParam interfaceParam)
        {
            Expression<Func<DangTin, bool>> terms = await ConditionSearchAsync(interfaceParam);

            EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
            IEnumerable<DangTin> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            await BuildDangTinExtend(res, interfaceParam);

            return res;
        }

        public async Task BuildDangTinExtend(IEnumerable<DangTin> source, InterfaceParam interfaceParam)
        {
            if (source == null)
            {
                return;
            }

            IEnumerable<ThanhVien> sourceThanhVien = null;

            string keyCache = string.Empty;
            object cacheValue;
            foreach (DangTin item in source)
            {
                keyCache = string.Format(KeyCacheParams.externalDangTinContactByParams, Variables.ApiRaoXe + ";IdDangTinContact=" + item.IdDangTinContact);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.DangTinContact_Ext = CommonMethods.DeserializeObject<DangTinContact>(cacheValue);
                }
                else
                {
                    item.DangTinContact_Ext = RepoDangTinContact.Find(a => a.Id == item.IdDangTinContact).Result.FirstOrDefault();
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.DangTinContact_Ext));
                }

                //keyCache = string.Format(KeyCacheParams.externalDangTinTraPhiByParams, Variables.ApiDaiLyXe + ";IdDangTin=" + item.Id + ";NgayPinTop=" + item.NgayPinTop);
                //cacheValue = await CacheController.GetCacheByKey(keyCache);
                //if (cacheValue != null && interfaceParam.isCache.HasValue && interfaceParam.isCache.Value)
                //{
                //    item.DangTinTraPhi_Ext = CommonMethods.DeserializeObject<DangTinTraPhi>(cacheValue);
                //}
                //else
                //{
                //    item.DangTinTraPhi_Ext = RepoDangTinTraPhi.Find(a => a.IdDangTin == item.Id && a.NgayBatDau == item.NgayPinTop
                //                                && a.PinStatus != (int)Variables.PinTopType.StopWaiting).Result.FirstOrDefault();
                //    // Add cache data
                //    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.DangTinTraPhi_Ext));
                //}

                keyCache = string.Format(KeyCacheParams.externalLayoutAlbumByParams, Variables.ApiRaoXe + ";MaLayoutAlbum=" + item.MaLayoutAlbum);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.LayoutAlbum_Ext = CommonMethods.DeserializeObject<LayoutAlbum>(cacheValue);
                }
                else
                {
                    item.LayoutAlbum_Ext = RepoLayoutAlbum.Find(a => a.Ma == item.MaLayoutAlbum).Result.FirstOrDefault();
                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.LayoutAlbum_Ext));
                }

                var idThanhVien = item.IdThanhVien ?? 0;
                keyCache = string.Format(KeyCacheParams.externalThanhVienDLXByParams, Variables.ApiRaoXe + ";ids=" + idThanhVien);
                cacheValue = await CacheController.GetCacheByKey(keyCache);
                if (cacheValue != null)
                {
                    item.ThanhVien_Ext = CommonMethods.DeserializeObject<ThanhVien>(cacheValue);
                }
                else
                {
                    if (sourceThanhVien == null)
                    {
                        sourceThanhVien = await Context.ThanhVien.ToListAsync();
                    }

                    item.ThanhVien_Ext = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(a => a.Id == item.IdThanhVien) : null;

                    // Add cache data
                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(item.ThanhVien_Ext));
                }

                if (item.TinhTrang.HasValue)
                {
                    switch (item.TinhTrang.Value)
                    {
                        case (int)DangTinState.New:
                            item.TinhTrang_Ext = "Mới";
                            break;
                        case (int)DangTinState.Old:
                            item.TinhTrang_Ext = "Đã sử dụng";
                            break;
                        default:
                            item.TinhTrang_Ext = string.Empty;
                            break;
                    }
                }

                if (item.IsDuyet.HasValue)
                {
                    item.IsDuyet_Ext = item.IsDuyet.Value ? "Đã duyệt" : "Chưa duyệt " + (string.IsNullOrEmpty(item.GhiChu) ? string.Empty : " - " + item.GhiChu);
                }

                if (item.NgayUp.HasValue)
                {
                    TimeSpan remainDay = (DateTime.Now - item.NgayUp.Value);
                    if (remainDay.Days > 0)
                    {
                        item.RemainingTimeUp_Ext = "0";
                    }
                    else
                    {
                        TimeSpan remainTime = new TimeSpan(1, 0, 0) - new TimeSpan(remainDay.Hours, remainDay.Minutes, remainDay.Seconds);
                        item.RemainingTimeUp_Ext = remainTime.TotalHours > 0 ? DateTime.Now.ToString("yyyy-MM-dd") + " " + remainTime.ToString() : "0";
                    }
                }
                else
                {
                    item.RemainingTimeUp_Ext = "0";
                }

                string deadline = (DateTime.Now > (item.DenNgay ?? DateTime.Now)) ? "(Hết hạn)" : "(Còn hạn)";
                item.FromToPost_Ext = CommonMethods.FormatDateTime_DD_MM_YYYY_FULL_SLASH((item.TuNgay ?? DateTime.Now)) + " - " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL_SLASH((item.DenNgay ?? DateTime.Now)) + deadline;
            }
        }

        #endregion

        #region "Tin Trả Phí"

        public async Task<IEnumerable<DangTinTraPhi>> SearchTinTraPhi(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinTraPhi>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTinTraPhi, bool>> terms = s =>
                (CheckListIntIsAllowSearch(interfaceParam.lstBigId, false) || interfaceParam.lstBigId.Contains(s.Id)) &&
                dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTinTraPhi> repo = new EntityBaseRepositoryRaoVat<DangTinTraPhi>(Request);
            IEnumerable<DangTinTraPhi> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            return res;
        }

        #endregion

        #region "ThanhVien"

        public void BuildListIdThanhVien()
        {
            Variables.ListDeletedIdThanhVien = GetListDeletedIdThanhVien().Result;
            Variables.ListAvailableIdThanhVien = GetListIdAvailableThanhVien().Result;
        }

        public async Task<List<long>> GetListDeletedIdThanhVien()
        {
            IEnumerable<ThanhVien> lstDeletedAdmin = await Context.ThanhVien.Where(s => s.TrangThai != 1).ToListAsync();
            List<long> lstRes = lstDeletedAdmin != null ? lstDeletedAdmin.Select(s => s.Id).ToList() : new List<long>();
            return lstRes;
        }

        public async Task<List<long>> GetListIdAvailableThanhVien()
        {
            IEnumerable<ThanhVien> lstAdmin = await Context.ThanhVien.Where(s => s.TrangThai == 1).ToListAsync();
            List<long> lstRes = lstAdmin != null ? lstAdmin.Select(s => s.Id).ToList() : new List<long>();
            return lstRes;
        }

        #endregion

        #region "UserPoint"

        public async Task<IEnumerable<UserPoint>> searchUserPoint(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserPoint>(interfaceParam.dynamicParam);

            Expression<Func<UserPoint, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id))
            && (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id))
            && (interfaceParam.isViewType == false || (interfaceParam.isViewType == true && s.PointTmp.HasValue && s.PointTmp.Value > 0))
            && (interfaceParam.lstIdThanhVien == null || interfaceParam.lstIdThanhVien.Count == 0 || interfaceParam.lstIdThanhVien.Contains(s.IdThanhVien ?? 0))
            && dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserPoint> repo = new EntityBaseRepositoryRaoVat<UserPoint>(Request);
            IEnumerable<UserPoint> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString);

            await BuildUserPointExtend(res, interfaceParam);
            return res;
        }

        private async Task BuildUserPointExtend(IEnumerable<UserPoint> res, InterfaceParam interfaceParam)
        {

            string ratePointToTimeString = await GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_TIME);
            int ratePointToTime = CommonMethods.ConvertToInt32(ratePointToTimeString);

            IEnumerable<ThanhVien> sourceThanhVien = null;
            IEnumerable<DangTinTraPhi> sourceDangTinTraPhi = null;

            long tongThoiGianDaMua = 0;
            long tongThoiGianDaDung = 0;

            foreach (UserPoint usp in res)
            {
                usp.RatePointToTime_Ext = ratePointToTime;

                if (sourceThanhVien == null)
                {
                    sourceThanhVien = await Context.ThanhVien.ToListAsync();
                }

                var thanhVien = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(n => n.Id == usp.IdThanhVien) : null;

                if (sourceDangTinTraPhi == null)
                {
                    sourceDangTinTraPhi = await Context.DangTinTraPhi.ToListAsync();
                }

                tongThoiGianDaMua = thanhVien != null ? (thanhVien.TotalTime ?? 0) : 0;
                tongThoiGianDaDung = sourceDangTinTraPhi != null ?
                    (sourceDangTinTraPhi.Where(n => n.IdThanhVienTao == usp.IdThanhVien && n.PinStatus != (int)Variables.PinTopType.StopWaiting).Sum(n => n.ThoiGianSuDung ?? n.ThoiGianSuDungDuDinh) ?? 0) : 0;

                usp.TimeDangTin_Ext = tongThoiGianDaMua - tongThoiGianDaDung;
                usp.ThanhVien_Ext = thanhVien;
            }
        }

        #endregion

        #region "HistoryPoint"

        public async Task<IEnumerable<HistoryPoint>> SearchHistoryPoint(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<HistoryPoint>(interfaceParam.dynamicParam);
            Expression<Func<HistoryPoint, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id))
            && (interfaceParam.lstIdThanhVien == null || interfaceParam.lstIdThanhVien.Count <= 0 || interfaceParam.lstIdThanhVien.Contains(s.IdThanhVien ?? 0))
            && (s.Type == 1 || s.Type == 2 || s.Type == 3 || s.Type == 4 || s.Type == 5 || s.Type == 6 || s.Type == 7)
            && dynamicFunctions(s);

            EntityBaseRepositoryHistoryRaoVat<HistoryPoint> repo = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>();
            IEnumerable<HistoryPoint> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            return res;
        }

        #endregion

        #region "DangTinFavorite"

        public async Task<IEnumerable<DangTinFavorite>> SearchDangTinFavorite(InterfaceParam interfaceParam)
        {
            List<long> lstIdUser = !string.IsNullOrEmpty(interfaceParam.idUsers) ? CommonMethods.SplitStringToInt64(interfaceParam.idUsers) : new List<long>();
            List<long> lstIdDangTin = !string.IsNullOrEmpty(interfaceParam.idTinDangs) ? CommonMethods.SplitStringToInt64(interfaceParam.idTinDangs) : new List<long>();

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinFavorite>(interfaceParam.search_dynamicParams);
            Expression<Func<DangTinFavorite, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&

            (lstIdUser == null || lstIdUser.Count <= 0 || lstIdUser.Contains(s.IdThanhVien ?? 0)) &&
            (lstIdDangTin == null || lstIdDangTin.Count <= 0 || lstIdDangTin.Contains(s.IdDangTin ?? 0)) &&
            dynamicFunctions(s);

            if (string.IsNullOrEmpty(interfaceParam.orderString))
            {
                interfaceParam.orderString = "Id";
            }
            if (interfaceParam.displayPage <= 0)
            {
                interfaceParam.displayPage = 1;
            }
            if (interfaceParam.displayItems <= 0)
            {
                interfaceParam.displayItems = 10;
            }

            EntityBaseRepositoryRaoVat<DangTinFavorite> repo = new EntityBaseRepositoryRaoVat<DangTinFavorite>(Request);
            IEnumerable<DangTinFavorite> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            res = await BuildDangTinFavoriteExtend(res, interfaceParam);

            return res;
        }

        private async Task<IEnumerable<DangTinFavorite>> BuildDangTinFavoriteExtend(IEnumerable<DangTinFavorite> res, InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<DangTin> repoTinDang = new EntityBaseRepositoryRaoVat<DangTin>();
            if (interfaceParam.allIncluding == true && res != null && res.Count() > 0)
            {
                foreach (var dtr in res)
                {
                    DangTin dt = (await repoTinDang.Find(n => n.Id == dtr.IdDangTin)).FirstOrDefault();
                    if (dt != null)
                    {
                        dtr.DangTin_Ext = dt;
                    }
                }

            }
            return res;
        }

        #endregion

        #region "DangTinRating"

        public async Task<IEnumerable<DangTinRating>> SearchDangTinRating(InterfaceParam interfaceParam)
        {
            List<long> lstIdUser = !string.IsNullOrEmpty(interfaceParam.idUsers) ? CommonMethods.SplitStringToInt64(interfaceParam.idUsers) : null;
            List<long> lstIdDangTin = !string.IsNullOrEmpty(interfaceParam.idTinDangs) ? CommonMethods.SplitStringToInt64(interfaceParam.idTinDangs) : null;

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinRating>(interfaceParam.search_dynamicParams);
            Expression<Func<DangTinRating, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (lstIdUser == null || lstIdUser.Count <= 0 || lstIdUser.Contains(s.IdUser ?? 0)) &&
                   (lstIdDangTin == null || lstIdDangTin.Count <= 0 || lstIdDangTin.Contains(s.IdDangTin ?? 0)) &&
                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
                   dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTinRating> repo = new EntityBaseRepositoryRaoVat<DangTinRating>(Request);
            IEnumerable<DangTinRating> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildDangTinRatingExtend(res, interfaceParam);

            return res;
        }

        private async Task<IEnumerable<DangTinRating>> BuildDangTinRatingExtend(IEnumerable<DangTinRating> res, InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<DangTin> repoTinDang = new EntityBaseRepositoryRaoVat<DangTin>();
            if (interfaceParam.allIncluding == true && res != null && res.Count() > 0)
            {
                foreach (var dtr in res)
                {
                    DangTin dt = (await repoTinDang.Find(n => n.Id == dtr.IdDangTin)).FirstOrDefault();
                    if (dt != null)
                    {
                        dtr.DangTin_Ext = dt;
                    }
                }
            }
            return res;
        }

        #endregion

        #region "UserRank"

        public async Task<IEnumerable<UserRank>> SearchUserRank(InterfaceParam interfaceParam)
        {
            interfaceParam.tieuDe = interfaceParam.tieuDe.ToASCIIAndLower();

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserRank>(interfaceParam.search_dynamicParams);

            Expression<Func<UserRank, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   (string.IsNullOrEmpty(interfaceParam.tieuDe) || interfaceParam.tieuDe.Contains(s.TieuDe.ToASCIIAndLower()) &&
                   (interfaceParam.percentDiscount == null || interfaceParam.percentDiscount == s.PercentDiscount)) &&
                   (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserRank> repo = new EntityBaseRepositoryRaoVat<UserRank>(Request);
            IEnumerable<UserRank> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "NotificationSystem"

        public async Task<IEnumerable<NotificationSystem>> SearchNotificationSystemAdmin(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<NotificationSystem>(interfaceParam.search_dynamicParams);

            Expression<Func<NotificationSystem, bool>> terms = s =>
                     (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                     (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                     (!interfaceParam.idThanhVien.HasValue || s.IdThanhVien == interfaceParam.idThanhVien) &&
                     (string.IsNullOrEmpty(interfaceParam.type) || interfaceParam.type.Contains(s.Type.Value.ToString())) &&
                     (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<NotificationSystem> repo = new EntityBaseRepositoryRaoVat<NotificationSystem>(Request);
            IEnumerable<NotificationSystem> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildNotificationSystemAdmin(res, interfaceParam);
            return res;
        }

        private async Task BuildNotificationSystemAdmin(IEnumerable<NotificationSystem> res, InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<NotificationSystem> repoTinDang = new EntityBaseRepositoryRaoVat<NotificationSystem>();
            InterfaceParam paramCallThanhVien = new InterfaceParam();
            paramCallThanhVien.token = interfaceParam.token;

            if (res != null && res.Count() > 0)
            {
                IEnumerable<ThanhVien> sourceThanhVien = null;
                foreach (var item in res)
                {
                    if (sourceThanhVien == null)
                    {
                        sourceThanhVien = await Context.ThanhVien.ToListAsync();
                    }

                    item.ThanhVien_Ext = sourceThanhVien != null ? sourceThanhVien.SingleOrDefault(a => a.Id == item.IdThanhVien) : null;

                    // paramCallThanhVien.ids = (dtr.IdThanhVien ?? 0).ToString();
                    // dtr.TenThanhVien_Ext = await ApiGetDal.SearchThanhVien(paramCallThanhVien);
                }
            }
        }

        #endregion

        #region "NotificationTemplate"

        public async Task<IEnumerable<NotificationTemplate>> SearchNotificationTemplate(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<NotificationTemplate>(interfaceParam.search_dynamicParams);

            Expression<Func<NotificationTemplate, bool>> terms = s =>
                     (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                      (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<NotificationTemplate> repo = new EntityBaseRepositoryRaoVat<NotificationTemplate>(Request);
            IEnumerable<NotificationTemplate> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "NotificationUser"

        public async Task<IEnumerable<NotificationUser>> SearchNotificationUser(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<NotificationUser>(interfaceParam.search_dynamicParams);

            Expression<Func<NotificationUser, bool>> terms = s =>
                     (!interfaceParam.idThanhVien.HasValue || s.IdThanhVien == interfaceParam.idThanhVien) &&
                     (!interfaceParam.idNotificationSystem.HasValue || s.IdNotificationSystem == interfaceParam.idNotificationSystem) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<NotificationUser> repo = new EntityBaseRepositoryRaoVat<NotificationUser>(Request);
            IEnumerable<NotificationUser> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "DangTinViolation"

        public async Task<IEnumerable<DangTinViolation>> SearchDangTinViolation(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinViolation>(interfaceParam.search_dynamicParams);
            Expression<Func<DangTinViolation, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (!interfaceParam.idThanhVien.HasValue || s.IdThanhVienTao == interfaceParam.idThanhVien) &&
                   (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTinViolation> repo = new EntityBaseRepositoryRaoVat<DangTinViolation>(Request);
            IEnumerable<DangTinViolation> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            res = await BuildDangTinViolationExtend(res, interfaceParam);

            return res;
        }

        private async Task<IEnumerable<DangTinViolation>> BuildDangTinViolationExtend(IEnumerable<DangTinViolation> res, InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<DangTin> repoTinDang = new EntityBaseRepositoryRaoVat<DangTin>();
            if (interfaceParam.allIncluding == true && res != null && res.Count() > 0)
            {
                foreach (var dtr in res)
                {
                    DangTin dt = (await repoTinDang.Find(n => n.Id == dtr.IdDangTin)).FirstOrDefault();
                    if (dt != null)
                    {
                        dtr.DangTin_Ext = dt;
                    }
                }
            }
            return res;
        }

        public async Task<IEnumerable<Dictionary<string, object>>> SearchUserDangTinViolation()
        {
            string dbName = RawSQLRepo.DatabaseName;
            string sql = @"select DISTINCT ""IdThanhVienTao"" from ""DangTinViolation"" where ""TrangThai"" = 1";
            IEnumerable<Dictionary<string, object>> res = await RawSQLRepo.ExecuteSqlQuery_ReturnDic(sql);

            return res;
        }

        #endregion

        #region "DangTinTraPhi"

        public async Task<IEnumerable<DangTinTraPhi>> SearchDangTinTraPhi(InterfaceParam interfaceParam)
        {
            List<long> lstIdDangTin = !string.IsNullOrEmpty(interfaceParam.idTinDangs) ? CommonMethods.SplitStringToInt64(interfaceParam.idTinDangs) : null;

            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<DangTinTraPhi>(interfaceParam.search_dynamicParams);

            Expression<Func<DangTinTraPhi, bool>> terms = s =>
            (interfaceParam.idThanhVien.HasValue && s.IdThanhVienTao.Equals(interfaceParam.idThanhVien.Value)) &&
            (s.PinStatus != 3) &&
            (lstIdDangTin != null && lstIdDangTin.Contains(s.IdDangTin ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<DangTinTraPhi> repo = new EntityBaseRepositoryRaoVat<DangTinTraPhi>(Request);
            IEnumerable<DangTinTraPhi> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            res = await BuildDangTinTraPhiExtend(res, interfaceParam);

            return res;
        }

        private async Task<IEnumerable<DangTinTraPhi>> BuildDangTinTraPhiExtend(IEnumerable<DangTinTraPhi> res, InterfaceParam interfaceParam)
        {
            EntityBaseRepositoryRaoVat<DangTin> repoTinDang = new EntityBaseRepositoryRaoVat<DangTin>();
            if (res != null && res.Count() > 0)
            {
                foreach (var dtr in res)
                {
                    DangTin dt = (await repoTinDang.Find(n => n.Id == dtr.IdDangTin)).FirstOrDefault();
                    if (dt != null)
                    {
                        dtr.DangTin_Ext = dt;
                    }
                }
            }
            return res;
        }

        #endregion

        #region "SearchUserBlacklist"

        public async Task<IEnumerable<UserBlackList>> SearchUserBlacklist(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserBlackList>(interfaceParam.search_dynamicParams);

            Expression<Func<UserBlackList, bool>> terms = s =>
                     (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                     (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
                     (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
                     (!interfaceParam.idThanhVien.HasValue || (interfaceParam.idThanhVien.HasValue && s.IdThanhVien.Equals(interfaceParam.idThanhVien.Value))) &&
                     (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserBlackList> repo = new EntityBaseRepositoryRaoVat<UserBlackList>(Request);
            IEnumerable<UserBlackList> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "User"

        public async Task<List<OnlineStatistics>> SearchOnlineStatistics(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<OnlineStatistics>(interfaceParam.search_dynamicParams);

            Expression<Func<OnlineStatistics, bool>> terms = s =>
                (!interfaceParam.year.HasValue || interfaceParam.year.HasValue && interfaceParam.year.Equals(s.OnlineDate.Value.Year)) &&
                (!interfaceParam.month.HasValue || interfaceParam.month.HasValue && interfaceParam.month.Equals(s.OnlineDate.Value.Month)) &&
                (!interfaceParam.day.HasValue || interfaceParam.day.HasValue && interfaceParam.day.Equals(s.OnlineDate.Value.Day)) &&
                dynamicFunctions(s);

            Expression<Func<OnlineStatistics, object>>[] includeProperties = null;

            EntityBaseRepositoryRaoVat<OnlineStatistics> repo = new EntityBaseRepositoryRaoVat<OnlineStatistics>();
            IEnumerable<OnlineStatistics> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, includeProperties);

            if (res != null && res.Count() > 0)
            {
                List<OnlineStatistics> newList = new List<OnlineStatistics>();
                if (interfaceParam.day.HasValue && interfaceParam.month.HasValue && interfaceParam.year.HasValue)
                {
                    var result = res.ToList();

                    var lable = string.Empty;
                    result.ForEach(a => lable += a.OnlineDate.Value.Day);

                    var value = string.Empty;
                    result.ForEach(a => value += a.ListJson);

                    newList.Add(new OnlineStatistics
                    {
                        Label_Ext = lable,
                        Value_Ext = value
                    });

                    return newList;
                }
                else if (interfaceParam.month.HasValue && interfaceParam.year.HasValue)
                {
                    var result = res
                    .Select(a => new OnlineStatistics
                    {
                        TotalRow = a.TotalRow,
                        TotalOnline = a.TotalOnline,
                        Day_Ext = a.OnlineDate.Value.Day,
                        Month_Ext = a.OnlineDate.Value.Month,
                        Year_Ext = a.OnlineDate.Value.Year
                    }).ToList();

                    var lable = string.Empty;
                    result.ForEach(a => lable += a.Day_Ext + ",");

                    var value = string.Empty;
                    result.ForEach(a => value += a.TotalOnline + ",");

                    newList.Add(new OnlineStatistics
                    {
                        TotalOnline = result.FirstOrDefault().TotalOnline,
                        Label_Ext = lable,
                        Value_Ext = value
                    });

                    return newList;
                }
                else if (interfaceParam.year.HasValue)
                {
                    var result = res
                    .GroupBy(a => a.OnlineDate.Value.Month)
                    .Select(a => new OnlineStatistics
                    {
                        TotalRow = a.FirstOrDefault().TotalRow,
                        TotalOnline = a.Sum(s => s.TotalOnline),
                        Month_Ext = a.FirstOrDefault().OnlineDate.Value.Month,
                        Year_Ext = a.FirstOrDefault().OnlineDate.Value.Year,
                    }).OrderBy(a => a.TotalOnline).ToList();

                    var lable = string.Empty;
                    result.ForEach(a => lable += a.Month_Ext + ",");

                    var value = string.Empty;
                    result.ForEach(a => value += a.TotalOnline + ",");

                    newList.Add(new OnlineStatistics
                    {
                        Label_Ext = lable,
                        Value_Ext = value
                    });

                    return newList;
                }
            }

            return null;
        }

        #endregion

        #region "InstallationStatistics"

        public async Task<IEnumerable<InstallationStatistics>> SearchInstallationStatistics(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<InstallationStatistics>(interfaceParam.dynamicParam);
            Expression<Func<InstallationStatistics, bool>> terms = s =>
            (string.IsNullOrEmpty(interfaceParam.osName) || s.OsName.ToUpper() == interfaceParam.osName.ToUpper()) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<InstallationStatistics> repo = new EntityBaseRepositoryRaoVat<InstallationStatistics>();
            IEnumerable<InstallationStatistics> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);

            return res;
        }

        #endregion

        #region "UserDevice"

        public async Task<List<StatisticUserDevice>> SearchUserDevice(InterfaceParam interfaceParam)
        {
            var userDevice = await Context.UserDevice.ToListAsync();
            var totalDevice = userDevice.Select(a => a.DeviceId).Distinct().Count();

            // installed
            var totalInstalled = userDevice.Where(a=> !a.IsUninstalled.HasValue).Select(a => a.DeviceId).Distinct().Count();
            var iosInstalled = userDevice.Where(a => !a.IsUninstalled.HasValue && !string.IsNullOrEmpty(a.OsName) && a.OsName.ToLower() == "ios").Select(a => a.DeviceId).Distinct().Count();
            var androidInstalled = userDevice.Where(a => !a.IsUninstalled.HasValue && !string.IsNullOrEmpty(a.OsName) && a.OsName.ToLower() == "android").Select(a => a.DeviceId).Distinct().Count();
            var unknownInstalled = userDevice.Where(a => !a.IsUninstalled.HasValue && string.IsNullOrEmpty(a.OsName)).Select(a => a.DeviceId).Distinct().Count();

            // uninstsalled
            var totalUninstalled = userDevice.Where(a => a.IsUninstalled.HasValue && a.IsUninstalled.Value).Select(a => a.DeviceId).Distinct().Count();
            var iosUnInstalled = userDevice.Where(a => a.IsUninstalled.HasValue && a.IsUninstalled.Value && !string.IsNullOrEmpty(a.OsName) && a.OsName.ToLower() == "ios").Select(a => a.DeviceId).Distinct().Count();
            var androidUnInstalled = userDevice.Where(a => a.IsUninstalled.HasValue && a.IsUninstalled.Value && !string.IsNullOrEmpty(a.OsName) && a.OsName.ToLower() == "android").Select(a => a.DeviceId).Distinct().Count();
            var unknownUnInstalled = userDevice.Where(a => a.IsUninstalled.HasValue && a.IsUninstalled.Value && string.IsNullOrEmpty(a.OsName)).Select(a => a.DeviceId).Distinct().Count();

            StatisticUserDevice statisticUserDevice = new StatisticUserDevice();
            statisticUserDevice.TotalDevice = totalDevice;

            statisticUserDevice.TotalInstalled = totalInstalled;
            statisticUserDevice.IosInstalled = iosInstalled;
            statisticUserDevice.AndroidInstalled = androidInstalled;
            statisticUserDevice.UnknownInstalled = unknownInstalled;

            statisticUserDevice.TotalUninstalled = totalUninstalled;
            statisticUserDevice.IosUnInstalled = iosUnInstalled;
            statisticUserDevice.AndroidUnInstalled = androidUnInstalled;
            statisticUserDevice.UnknownUnInstalled = unknownUnInstalled;

            List<StatisticUserDevice> res = new List<StatisticUserDevice>();
            res.Add(statisticUserDevice);

            return res;
        }

        #endregion

        #region "UserGroup"

        public async Task<IEnumerable<UserGroup>> SearchUserGroup(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<UserGroup>(interfaceParam.search_dynamicParams);

            Expression<Func<UserGroup, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
                   (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<UserGroup> repo = new EntityBaseRepositoryRaoVat<UserGroup>(Request);
            IEnumerable<UserGroup> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildUserGroupExtend(res, interfaceParam);

            return res;
        }

        private async Task BuildUserGroupExtend(IEnumerable<UserGroup> res, InterfaceParam interfaceParam)
        {
            if (interfaceParam.allIncluding)
            {
                InterfaceParam paramCallThanhVien = new InterfaceParam();
                paramCallThanhVien.token = interfaceParam.token;

                EntityBaseRepositoryRaoVat<ThanhVien> repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>();

                if (res != null && res.Count() > 0)
                {
                    foreach (var userGroup in res)
                    {
                        List<ThanhVien> thanhViens = (await repoThanhVien.Find(n => n.TrangThai == 1 && ("," + n.IdGroups + ",").Contains("," + userGroup.Id + ","))).ToList();

                        List<CustomResult> lstCustomResults = new List<CustomResult>();

                        foreach (var thanhVien in thanhViens)
                        {
                            paramCallThanhVien.ids = thanhVien.Id.ToString();

                            CustomResult ThanhVien_Ext = await ApiGetDal.SearchThanhVien(paramCallThanhVien);
                            lstCustomResults.Add(ThanhVien_Ext);
                        }

                        userGroup.LstThanhVien_Ext = lstCustomResults;
                    }
                }
            }
        }

        #endregion

        #region "LayoutAlbum"

        public async Task<IEnumerable<LayoutAlbum>> SearchLayoutAlbum(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<LayoutAlbum>(interfaceParam.search_dynamicParams);

            Expression<Func<LayoutAlbum, bool>> terms = s =>
                   (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
                   (CheckListIntIsAllowSearch(interfaceParam.lstNotId, true) || !interfaceParam.lstNotId.Contains(s.Id)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<LayoutAlbum> repo = new EntityBaseRepositoryRaoVat<LayoutAlbum>(Request);
            IEnumerable<LayoutAlbum> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            return res;
        }

        #endregion

        #region "SearchMarketingPlan"

        public async Task<IEnumerable<MarketingPlan>> SearchMarketingPlan(InterfaceParam interfaceParam)
        {
            DynamicParams dynParams = new DynamicParams();
            var dynamicFunctions = await dynParams.BuildDynamicExpression<MarketingPlan>(interfaceParam.search_dynamicParams);
            Expression<Func<MarketingPlan, bool>> terms = s =>
            (CheckListIntIsAllowSearch(interfaceParam.lstId, false) || interfaceParam.lstId.Contains(s.Id)) &&
            (string.IsNullOrEmpty(interfaceParam.tuKhoaTimKiem) || s.TuKhoaTimKiem.Contains(interfaceParam.tuKhoaTimKiem)) &&
            (interfaceParam.lstTrangThai == null || interfaceParam.lstTrangThai.Count <= 0 || interfaceParam.lstTrangThai.Contains(s.TrangThai ?? 0)) &&
            dynamicFunctions(s);

            EntityBaseRepositoryRaoVat<MarketingPlan> repo = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request);
            IEnumerable<MarketingPlan> res = await repo.GetAll(terms, interfaceParam.displayPage, interfaceParam.displayItems, interfaceParam.orderString, null);
            await BuildMarketingPlanExtend(res);
            return res;
        }

        public async Task BuildMarketingPlanExtend(IEnumerable<MarketingPlan> source)
        {
            if (source == null)
            {
                return;
            }

            foreach (MarketingPlan item in source)
            {
                item.LstMarketingCode_Ext = await RepoMarketingCode.Find(a => a.IdMarketingPlan == item.Id);
            }
        }

        #endregion
    }
}