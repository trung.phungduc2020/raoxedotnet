﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using raoVatApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using raoVatApi.Common;
using Response;
using System.Text;
using Microsoft.Net.Http.Headers;
using raoVatApi.ControllersRaoVat;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Html;
using raoVatApi.Response;

namespace Repository
{
    public class ElasticSearchBLL
    {
        private ElasticSearchGetDal _eSGetDal;
        private ElasticSearchGetDal ESGetDal
        {
            get
            {
                _eSGetDal = new ElasticSearchGetDal();
                return _eSGetDal;
            }
        }

        #region "Common"

        private const string linkElasticSearchController = @"api/elasticsearch/";
        private const string action_ReIndex = "ReIndex";

        public async Task<Object> ReIndex(object entityDangTin)
        {
            var body = new
            {
                ni = "appfiles_dangtin",
                it = "httppost",
                dt = entityDangTin
            };

            var body64 = new
            {
                at64 = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(body))
            };

            var res = ESGetDal.ExecuteApiToWebApiElasticSearch(CommonMethods.SerializeObject(body64));

            return res;
        }


        public async Task<Object> DeleteIndex(long id)
        {
            var body = new
            {
                ni = "appfiles_dangtin",
                it = "httpdelete",
                dt = new
                {
                    Id = id
                }
            };

            var body64 = new
            {
                at64 = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(body)).Replace("=", "")
            };

            var res = ESGetDal.ExecuteApiToWebApiElasticSearch(CommonMethods.SerializeObject(body64));

            return res;
        }
        #endregion

    }

}