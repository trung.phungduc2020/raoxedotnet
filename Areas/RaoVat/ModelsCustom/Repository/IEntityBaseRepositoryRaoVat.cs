using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using raoVatApi.Models;
using raoVatApi.Models.Base;

namespace raoVatApi.ModelsRaoVat
{
    public interface IEntityBaseRepositoryRaoVat<T> : IEntityBaseRepository<T> where T : class, IEntityBase
    {
        
    }
}