﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Text;
using Response;
using Microsoft.AspNetCore.Http;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.Response;
using raoVatApi.ModelsRaoVat;
using GmailApi;

namespace Repository
{
    public class ApiGetCore<T>
    {
        private HttpContextAccessor _httpContextAccessor;
        public ApiGetCore()
        {
            _httpContextAccessor = new HttpContextAccessor();
        }

        #region Cache

        // Nghien cuu luu cache | clear cache => truy xuat tu cache neu co

        #endregion

        #region Api

        public async Task<HttpResponseMessage> GetAsync(string requestUri, string httpType, object bodyData, string tokenDefaultValue, params HttpParam[] arrParams)
        {
            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            CommonMethods.WriteLog("Start GetAsync:" + CommonMethods.SerializeToJSON(requestUri));
            HttpResponseMessage res = null;

            if (string.IsNullOrEmpty(requestUri))
            {
                return null;
            }

            if (arrParams != null)
            {
                string questionMark = requestUri.Contains(@"?") ? "" : @"?";
                requestUri += questionMark; // append questionMark if any

                // Nếu đã có sẵn dấu "?" mà ko end = "?" thì đã có 1 param phía trước
                // Nên lúc này add sẵn dấu "&" để xử lý cho vòng lặp bên dưới
                requestUri = requestUri.EndsWith(@"?") ? requestUri : (requestUri + "&");

                foreach (HttpParam param in arrParams)
                {
                    requestUri += requestUri.EndsWith(@"&") ? param.BuildQueryString() : ("&" + param.BuildQueryString());
                }
            }

            try
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                CommonMethods.WriteLog("Start HttpClient:" + CommonMethods.SerializeToJSON(requestUri));
                using (var client = new HttpClient(clientHandler))
                //using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))       
                {
                    string tokenValue = "";
                    StringContent stringBodyData = null;

                    try
                    {
                        CommonMethods.WriteLog("HttpClient 1 :");

                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        CommonMethods.WriteLog("HttpClient 2 :");

                        stringBodyData = new StringContent(CommonMethods.SerializeObject(bodyData == null ? "" : bodyData), Encoding.UTF8, "application/json");

                        CommonMethods.WriteLog("HttpClient 3");

                        if (string.IsNullOrEmpty(tokenDefaultValue))
                        {
                            tokenValue = _httpContextAccessor.HttpContext.Request.GetTokenDecrypted();
                        }
                        else
                        {
                            tokenValue = tokenDefaultValue;
                        }

                        CommonMethods.WriteLog("HttpClient 4");

                        if(!string.IsNullOrEmpty(tokenValue))
                        {
                            tokenValue = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("bearer ", tokenValue);
                            // string tokenValue = "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiTmd1eeG7hW4gUGjDuiBDxrDhu51uZyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IjEiLCJJZCI6IjUiLCJVc2VyTmFtZSI6Im5ndXllbmN1b25nY3MiLCJIb1RlbiI6Ik5ndXnhu4VuIFBow7ogQ8aw4budbmciLCJFbWFpbCI6InBodWN1b25nQGdpYW5oYW5ndm4uY29tIiwiRGllblRob2FpIjoiIiwiSWROaG9tUXV5ZW4iOiIxIiwiSXNNYWluIjoiRmFsc2UiLCJuYmYiOjE1MDg0NzcwNTMsImV4cCI6MTUxMTA2OTA1MywiaXNzIjoiQ8O0bmcgdHkgVE5ISCBnaWFuIGjDoG5nIHRy4buxYyB0dXnhur9uIFZOIiwiYXVkIjoiZGFpbHl4ZS5jb20udm4ifQ.Ae06VXfHUbPWsrqtzIoi_Y_ByGm8leV-BgiobjNCUBQ";
                            client.DefaultRequestHeaders.Add("Authorization", tokenValue);
                        }
                        CommonMethods.WriteLog("HttpClient 5");
                    }
                    catch (Exception ex)
                    {
                        CommonMethods.WriteLog("Catch Error:" + Variables.UrlFromClient + ":" + CommonMethods.SerializeToJSON(stringBodyData) + ex.ToString());
                    }

                    CommonMethods.WriteLog("switch (httpType):" + CommonMethods.SerializeToJSON(httpType));
                    switch (httpType)
                    {
                        case Variables.HttpType_Get:
                            CommonMethods.WriteLog("HttpType_Get ");
                            res = await client.GetAsync(requestUri);
                            CommonMethods.WriteLog("switch (httpType) res:" + CommonMethods.SerializeToJSON(res));
                            break;
                        case Variables.HttpType_Post:
                        case Variables.HttpType_Put:
                        case Variables.HttpType_Delete:

                            if (httpType == Variables.HttpType_Post)
                            {
                                if (requestUri.EndsWith("/token"))
                                {
                                    var content = new FormUrlEncodedContent(new[]
                                    {
                                        new KeyValuePair<string, string>(CommonParams.token_username, CommonMethods.ConvertToString(CommonMethods.GetPropertyValue(bodyData, CommonParams.token_username))),
                                        new KeyValuePair<string, string>(CommonParams.token_password, CommonMethods.ConvertToString(CommonMethods.GetPropertyValue(bodyData, CommonParams.token_password)))
                                    });

                                    res = await client.PostAsync(requestUri, content);
                                }
                                else
                                {
                                    res = await client.PostAsync(requestUri, stringBodyData);
                                }
                            }
                            else if (httpType == Variables.HttpType_Put)
                            {
                                res = await client.PutAsync(requestUri, stringBodyData);
                            }
                            else if (httpType == Variables.HttpType_Delete)
                            {
                                res = await client.DeleteAsync(requestUri);
                            }
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog("ERROR GetAsync:" + Variables.UrlFromClient + ":" + CommonMethods.SerializeToJSON(error));
            }

            return res;
        }

        public async Task<HttpResponseMessage> GetAsyncWithoutSerializedBody(string requestUri, string httpType, string bodyData, params HttpParam[] arrParams)
        {
            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            HttpResponseMessage res = null;

            MailService ms = new MailService();

            if (string.IsNullOrEmpty(requestUri))
            {
                return null;
            }

            if (arrParams != null)
            {
                string questionMark = requestUri.Contains(@"?") ? "" : @"?";
                requestUri += questionMark; // append questionMark if any

                // Nếu đã có sẵn dấu "?" mà ko end = "?" thì đã có 1 param phía trước
                // Nên lúc này add sẵn dấu "&" để xử lý cho vòng lặp bên dưới
                requestUri = requestUri.EndsWith(@"?") ? requestUri : (requestUri + "&");

                foreach (HttpParam param in arrParams)
                {
                    requestUri += requestUri.EndsWith(@"&") ? param.BuildQueryString() : ("&" + param.BuildQueryString());
                }
            }

            try
            {
                using (var client = new HttpClient())
                //using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))       
                {
                    string tokenValue = "";
                    StringContent stringBodyData = null;

                    try
                    {
                        //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-ndjson"));
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        //stringBodyData = new StringContent(bodyData, Encoding.UTF8, "application/x-ndjson");
                        stringBodyData = new StringContent(bodyData, Encoding.UTF8, "application/json");
                        tokenValue = _httpContextAccessor.HttpContext.Request.GetTokenDecrypted();

                        if (!string.IsNullOrEmpty(tokenValue))
                        {
                            tokenValue = "bearer " + tokenValue;

                            // string tokenValue = "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjUiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiTmd1eeG7hW4gUGjDuiBDxrDhu51uZyIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IjEiLCJJZCI6IjUiLCJVc2VyTmFtZSI6Im5ndXllbmN1b25nY3MiLCJIb1RlbiI6Ik5ndXnhu4VuIFBow7ogQ8aw4budbmciLCJFbWFpbCI6InBodWN1b25nQGdpYW5oYW5ndm4uY29tIiwiRGllblRob2FpIjoiIiwiSWROaG9tUXV5ZW4iOiIxIiwiSXNNYWluIjoiRmFsc2UiLCJuYmYiOjE1MDg0NzcwNTMsImV4cCI6MTUxMTA2OTA1MywiaXNzIjoiQ8O0bmcgdHkgVE5ISCBnaWFuIGjDoG5nIHRy4buxYyB0dXnhur9uIFZOIiwiYXVkIjoiZGFpbHl4ZS5jb20udm4ifQ.Ae06VXfHUbPWsrqtzIoi_Y_ByGm8leV-BgiobjNCUBQ";
                            client.DefaultRequestHeaders.Add("Authorization", tokenValue);
                        }
                    }
                    catch
                    {

                    }

                    switch (httpType)
                    {
                        case Variables.HttpType_Get:
                            res = await client.GetAsync(requestUri);
                            break;
                        case Variables.HttpType_Post:
                        case Variables.HttpType_Put:
                        case Variables.HttpType_Delete:

                            if (httpType == Variables.HttpType_Post)
                            {
                                CommonMethods.WriteLogApiGetCore(httpType + "-:-" + requestUri + "-" + bodyData);
                                if (requestUri.EndsWith("/token"))
                                {
                                    var content = new FormUrlEncodedContent(new[]
                                    {
                                        new KeyValuePair<string, string>(CommonParams.token_username, CommonMethods.ConvertToString(CommonMethods.GetPropertyValue(bodyData, CommonParams.token_username))),
                                        new KeyValuePair<string, string>(CommonParams.token_password, CommonMethods.ConvertToString(CommonMethods.GetPropertyValue(bodyData, CommonParams.token_password)))
                                    });

                                    res = await client.PostAsync(requestUri, content);
                                }
                                else
                                {
                                    res = await client.PostAsync(requestUri, stringBodyData);
                                }
                            }
                            else if (httpType == Variables.HttpType_Put)
                            {
                                res = await client.PutAsync(requestUri, stringBodyData);
                            }
                            else if (httpType == Variables.HttpType_Delete)
                            {
                                res = await client.DeleteAsync(requestUri);
                            }
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLogApiGetCore(error);
                // Send mail
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("ApiGetCore - Error ", error, email);
                }

                GmailApiCore gac = new GmailApiCore();
                string thongBao = Variables.UrlFromClient + "[-]" + "-" + requestUri + "=>" + error;
                gac.PostThongBaoHeThong_CommitDb(thongBao);
            }

            return res;
        }

        public async Task<string> GetAsyncReturnString(string requestUri, string httpType, object bodyData, string tokenDefaultValue, params HttpParam[] arrParams)
        {
            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            CommonMethods.WriteLog("GetAsyncReturnString:" + CommonMethods.SerializeToJSON(requestUri));
            string res = string.Empty;
            HttpResponseMessage response = await GetAsync(requestUri, httpType, bodyData, tokenDefaultValue, arrParams);

            CommonMethods.WriteLog("response:" + CommonMethods.SerializeToJSON(response));

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    CommonMethods.WriteLog("content:" + CommonMethods.SerializeToJSON(content));

                    res = content.ReadAsStringAsync().Result;

                    // nguyencuongcs 20180426: nhận thấy sự tương đồng CommonMethods.ConvertToString(obj) && content.ReadAsStringAsync().Result
                    // try
                    // {
                    //     var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(res);
                    //     if (obj != null)
                    //     {
                    //         res = CommonMethods.ConvertToString(obj);
                    //     }
                    // }
                    // catch (Exception ex)
                    // {
                    //     string exStr = ex.ToString();
                    // }
                }
            }
            else
            {
                CommonMethods.WriteLog("response.StatusCode:" + CommonMethods.SerializeToJSON(response.StatusCode));
                res = CommonMethods.ConvertToString(response.StatusCode);
            }

            return res;
        }

        public async Task<string> GetAsyncReturnStringWithoutSerializedBody(string requestUri, string httpType, string bodyData, params HttpParam[] arrParams)
        {
            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            string res = string.Empty;
            HttpResponseMessage response = await GetAsyncWithoutSerializedBody(requestUri, httpType, bodyData, arrParams);

            if (response.IsSuccessStatusCode)
            {
                using (HttpContent content = response.Content)
                {
                    res = content.ReadAsStringAsync().Result;

                    // nguyencuongcs 20180426: nhận thấy sự tương đồng CommonMethods.ConvertToString(obj) && content.ReadAsStringAsync().Result
                    // try
                    // {
                    //     var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(res);
                    //     if (obj != null)
                    //     {
                    //         res = CommonMethods.ConvertToString(obj);
                    //     }
                    // }
                    // catch (Exception ex)
                    // {
                    //     string exStr = ex.ToString();
                    // }
                }
            }
            else
            {
                res = CommonMethods.ConvertToString(response.StatusCode);
            }

            return res;
        }

        public async Task<CustomResultType<Dictionary<string, object>>> GetAsyncReturnCustomResultDictionary(string requestUri, string httpType, object bodyData, string tokenDefaultValue, params HttpParam[] arrParams)
        {

            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            CustomResultType<Dictionary<string, object>> cusRes = null;

            try
            {
                string res = await GetAsyncReturnString(requestUri, httpType, bodyData, tokenDefaultValue, arrParams);

                if (!string.IsNullOrEmpty(res))
                {
                    var resObj = JObject.Parse(res);
                    cusRes = new CustomResultType<Dictionary<string, object>>(resObj);
                }
            }
            catch (Exception ex)
            {
                string exStr = ex.ToString();
            }

            return cusRes;
        }

        public async Task<CustomResultType<IEnumerable<T>>> GetAsyncReturnCustomResultType(string requestUri, string httpType, object bodyData, string tokenDefaultValue, params HttpParam[] arrParams)
        {

            //if (System.Web.HttpContext.Current.Cache[KeyCache] != null)
            //    return (DataSet)System.Web.HttpContext.Current.Cache[KeyCache];

            CustomResultType<IEnumerable<T>> cusRes = null;

            try
            {
                string res = await GetAsyncReturnString(requestUri, httpType, bodyData, tokenDefaultValue, arrParams);

                if (!string.IsNullOrEmpty(res))
                {
                    var resObj = JObject.Parse(res);
                    cusRes = new CustomResultType<IEnumerable<T>>(resObj);
                }
            }
            catch (Exception ex)
            {
                string exStr = ex.ToString();
            }

            return cusRes;
        }

        #endregion
    }
}
