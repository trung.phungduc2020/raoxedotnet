﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using raoVatApi.Models;
using raoVatApi.ControllersRaoVat;
using raoVatApi.Common;
using Microsoft.Extensions.Caching.Distributed;
using Response;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;

namespace Repository
{
    public class ElasticSearchGetDal
    {
        #region "Variables"

        private const string linkAdminController = @"/api/RaoVat/admin";
        private const string action_Search = @"_msearch";
        //private IDistributedCache _cache;

        #endregion

        public ElasticSearchGetDal() //IDistributedCache cache
        {
            //this._cache = cache;
        }
        
        public JObject ExecuteApiToWebApiElasticSearch(string body)
        {
            try
            {
                if (!string.IsNullOrEmpty(body))
                {
                    string path = Variables.WebApiElasticSearch + "api/elasticsearch/raoxe_reindex";
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    CustomResultTypeWithoutSerializedBody<JObject> cusRes = new CustomResultTypeWithoutSerializedBody<JObject>(path, Variables.HttpType_Post, body, null);

                    JObject res = null;

                    if (cusRes != null)
                    {
                        res = cusRes.DataResultJObject;
                    }

                    return res;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: ExecuteApiToWebApiElasticSearch", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        //#endregion
    }
}
