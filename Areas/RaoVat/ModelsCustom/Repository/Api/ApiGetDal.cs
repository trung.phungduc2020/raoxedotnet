﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using raoVatApi.Models;
using Response;
using raoVatApi.ControllersRaoVat;
using raoVatApi.Response;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using Newtonsoft.Json;

namespace Repository
{
    public class ApiGetDal
    {

        #region "Commons"

        private CustomResult ConvertCustomResultTypeToCustomResult(CustomResultType<Dictionary<string, object>> cusResType)
        {
            try
            {
                CustomResult cusRes = new CustomResult();
                if (cusResType != null)
                {
                    cusRes.DataResult = cusResType.DataResult;
                    cusRes.Message = cusResType.Message;
                    cusRes.IntResult = cusResType.IntResult;
                    cusRes.StrResult = cusResType.StrResult;
                }

                return cusRes;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: ConvertCustomResultTypeToCustomResult", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        private string addFirstSplash(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.StartsWith("/") ? value : ("/" + value));
            }

            return value;
        }
        
        #endregion

        #region "Dailyxe - API"

        public async Task<CustomResult> IsExistPhoneNumberThanhVien(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.dienThoai))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "IsExistPhoneNumberThanhVien?dienThoai=" + interfaceParam.dienThoai
                                                                + "&isSelfCheck=" + interfaceParam.isSelfCheck;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, interfaceParam.token, null);

                    CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                    return cusRes;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: IsExistPhoneNumberThanhVien", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public async Task<CustomResult> IsExistDienThoaiThanhVienSelfCheck(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.dienThoai))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "IsExistPhoneNumberThanhVien?dienThoai=" + interfaceParam.dienThoai
                                                                + "&notId=" + interfaceParam.notId;

                    CommonMethods.WriteLog("API IsExistDienThoaiThanhVienSelfCheck: " + path);
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();

                    await cusResType.Execute(path, Variables.HttpType_Get, null, interfaceParam.token, null);

                    CommonMethods.WriteLog(CommonMethods.SerializeToJSON(cusResType));

                    CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                    CommonMethods.WriteLog(CommonMethods.SerializeToJSON(cusRes));

                    return cusRes;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: IsExistDienThoaiThanhVienSelfCheck", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public async Task<CustomResult> IsExistEmailThanhVienSelfCheck(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.email))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "IsExistEmailThanhVienSelfCheck?email=" + interfaceParam.email
                                                                + "&notId=" + interfaceParam.notId;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, interfaceParam.token, null);

                    CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                    return cusRes;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: IsExistEmailThanhVienSelfCheck", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public async Task<CustomResult> SearchThanhVien(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.ids))
                {
                    string path = Variables.ApiDaiLyXe + "thanhvien?Ids=" + interfaceParam.ids;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, interfaceParam.token, null);

                    CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                    return cusRes;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchThanhVien", Variables.UrlFromClient + "-" + Variables.ApiDaiLyXe + "thanhvien?Ids=" + interfaceParam.ids + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }

        // Notice: avoid load from Cache
        public async Task<ThanhVien_DLX> SearchThanhVienNoToken(string idThanhVien)
        {
            try
            {
                if (!string.IsNullOrEmpty(idThanhVien))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "searchthanhvien?Ids=" + idThanhVien;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, null, null);
                    
                    var data = cusResType.DataResult.FirstOrDefault();
                    if(data != null && data.Count > 0)
                    {
                        ThanhVien_DLX thanhVien_DLX = new ThanhVien_DLX();
                        thanhVien_DLX.Id = CommonMethods.ConvertToInt64(data.GetValueFromKey("Id", string.Empty).ToString());
                        thanhVien_DLX.DienThoai = data.GetValueFromKey("DienThoai", string.Empty).ToString();
                        thanhVien_DLX.HoTen = data.GetValueFromKey("HoTen", string.Empty).ToString();
                        thanhVien_DLX.Email = data.GetValueFromKey("Email", string.Empty).ToString();
                        thanhVien_DLX.IdFileDaiDien = CommonMethods.ConvertToInt32(data.GetValueFromKey("IdFileDaiDien", string.Empty).ToString());
                        thanhVien_DLX.DiaChi = data.GetValueFromKey("DiaChi", string.Empty).ToString();
                        thanhVien_DLX.IdTinhThanh = CommonMethods.ConvertToInt32(data.GetValueFromKey("IdTinhThanh", string.Empty).ToString());
                        thanhVien_DLX.IdQuanHuyen = CommonMethods.ConvertToInt32(data.GetValueFromKey("IdQuanHuyen", string.Empty).ToString());
                        thanhVien_DLX.GioiTinh = CommonMethods.ConvertToBoolean(data.GetValueFromKey("GioiTinh", string.Empty).ToString());
                        var NgaySinh = data.GetValueFromKey("NgaySinh", string.Empty).ToString();
                        thanhVien_DLX.NgaySinh = string.IsNullOrEmpty(NgaySinh) ? (DateTime?)null :  DateTime.Parse(NgaySinh);

                        return thanhVien_DLX;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchThanhVienNoToken:", Variables.UrlFromClient + "-" + Variables.ApiDaiLyXe + "thanhvien?Ids=" + idThanhVien + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }

        public async Task<CustomResult> UpdateDaXacThucCmnd(InterfaceParam interfaceParam)
        {
            try
            {
                string path = Variables.ApiDaiLyXe + "thanhvien/UpdateDaXacThucCmnd?Ids=" + interfaceParam.ids + "&daXacThucCmnd=" + (interfaceParam.daXacThucCmnd.HasValue && interfaceParam.daXacThucCmnd.Value ? "true" : "false");
                CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                await cusResType.Execute(path, Variables.HttpType_Put, null, interfaceParam.token, null);

                CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                return cusRes;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateDaXacThucCmnd", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public async Task<CustomResult> UpdateDaXacThucPassport(InterfaceParam interfaceParam)
        {
            try
            {
                string path = Variables.ApiDaiLyXe + "thanhvien/UpdateDaXacThucPassport?Ids=" + interfaceParam.ids + "&daXacThucPassport=" + (interfaceParam.daXacThucPassport.HasValue && interfaceParam.daXacThucPassport.Value ? "true" : "false");
                CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                await cusResType.Execute(path, Variables.HttpType_Put, null, interfaceParam.token, null);

                CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                return cusRes;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateDaXacThucPassport", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public async Task<CustomResult> UpdateListHinhAnhJsonPassport(InterfaceParam interfaceParam)
        {
            try
            {
                string path = Variables.ApiDaiLyXe + "thanhvien/UpdateListHinhAnhJsonPassport?Ids=" + interfaceParam.ids + "&listHinhAnhJsonPassport=" + interfaceParam.listHinhAnhJsonPassport;
                CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                await cusResType.Execute(path, Variables.HttpType_Put, null, interfaceParam.token, null);

                CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);

                return cusRes;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateListHinhAnhJsonPassport", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        // https://api.dailyxe.com.vn/api/dailyxe/interfaceData/SearchTinhThanh?ids=1
        public async Task<string> SearchTinhThanh(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.ids))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "SearchTinhThanh?ids=" + interfaceParam.ids;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, null, null);

                    var data = cusResType.DataResult.FirstOrDefault();
                    if (data != null && data.Count > 0)
                    {
                        return data.GetValueFromKey("TenTinhThanh", string.Empty).ToString();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchTinhThanh", Variables.UrlFromClient + "-" + Variables.ApiInterfaceDaiLyXe + "SearchTinhThanh?Ids=" + interfaceParam.ids + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }

        // https://api.dailyxe.com.vn/api/dailyxe/interfaceData/SearchQuanHuyen?ids=1
        public async Task<string> SearchQuanHuyen(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.ids))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "SearchQuanHuyen?ids=" + interfaceParam.ids;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, null, null);

                    var data = cusResType.DataResult.FirstOrDefault();
                    if (data != null && data.Count > 0)
                    {
                        return data.GetValueFromKey("TenQuanHuyen", string.Empty).ToString();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchQuanHuyen", Variables.UrlFromClient + "-" + Variables.ApiInterfaceDaiLyXe + "SearchQuanHuyen?Ids=" + interfaceParam.ids + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }

        // https://api.dailyxe.com.vn/api/dailyxe/interfaceData/SearchDongXe?ids=295
        public async Task<string> SearchDongXe(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.ids))
                {
                    string path = Variables.ApiInterfaceDaiLyXe + "SearchDongXe?ids=" + interfaceParam.ids;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, null, null);

                    var data = cusResType.DataResult.FirstOrDefault();
                    if (data != null && data.Count > 0)
                    {
                        return data.GetValueFromKey("TenDongXe", string.Empty).ToString();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchDongXe", Variables.UrlFromClient + "-" + Variables.ApiInterfaceDaiLyXe + "SearchDongXe?Ids=" + interfaceParam.ids + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }


        // https://api.dailyxe.com.vn/api/dailyxe/Files/InsertListUrlHinhAnh
        public async Task<Dictionary<string,object>> SearchDongXeAllField(InterfaceParam interfaceParam)
        {
            try
            {
                if (!string.IsNullOrEmpty(interfaceParam.ids))
                {
                    string path = Variables.ApiDaiLyXe + "SearchDongXe?ids=" + interfaceParam.ids;
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Get, null, null, null);

                    var data = cusResType.DataResult.FirstOrDefault();
                    if (data != null && data.Count > 0)
                    {
                        return data;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: SearchDongXe", Variables.UrlFromClient + "-" + Variables.ApiInterfaceDaiLyXe + "SearchDongXe?Ids=" + interfaceParam.ids + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }

        //public async Task<IEnumerable<Dictionary<string, object>>> UpdateDaXacThucCmnd(InterfaceParam interfaceParam)
        //{
        //    string path = Variables.DomainApi + linkInterfaceDataController + action_UpdateTinTuc_SoLuotXem;
        //    HttpParam[] arrParams = new HttpParam[] {
        //        new HttpParam(CommonParams.id, ids),
        //    };

        //    CustomResultType<Dictionary<string, object>> cusRes = new CustomResultType<Dictionary<string, object>>();
        //    await cusRes.Execute(path, Variables.HttpType_Get, null, "", arrParams);

        //    IEnumerable<Dictionary<string, object>> res = null;

        //    if (cusRes != null)
        //    {
        //        res = cusRes.DataResult;
        //    }

        //    return res;
        //}

        //public async Task<CustomResult> ClearCacheAuto(string entityName, string id, bool clearCacheActive)
        //{
        //    string path = Variables.DomainApi + linkApiMemoryCache + action_ClearCacheAuto;
        //    Dictionary<string, object> body = new Dictionary<string, object>();

        //    body.Add(CommonParams.entityName, entityName);
        //    body.Add(CommonParams.id, id);
        //    body.Add(CommonParams.clearCacheActive, clearCacheActive);

        //    string bodyEncode = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(body));
        //    var body64 = new Dictionary<string, object>() { };
        //    body64.Add(CommonParams.token_data64, bodyEncode);

        //    // nguyencuongcs 20190624: không hiểu tại sao gọi hàm này lại bị UnAuthorized từ server 12 nên thử truyền token admin vào
        //    CommonFileDal cfDal = new CommonFileDal();
        //    string tokenDefaultAdmin = cfDal.TokenAdmin;

        //    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
        //    await cusResType.Execute(path, Variables.HttpType_Put, body64, tokenDefaultAdmin);
        //    CustomResult cusRes = ConvertCustomResultTypeToCustomResult(cusResType);
        //    return cusRes;
        //}


        //public async Task<IEnumerable<Dictionary<string, object>>> UpdateTinTuc_SoLuotXem(string ids)
        //{
        //    string path = Variables.DomainApi + linkInterfaceDataController + action_UpdateTinTuc_SoLuotXem;
        //    HttpParam[] arrParams = new HttpParam[] {
        //        new HttpParam(CommonParams.id, ids),
        //    };

        //    CustomResultType<Dictionary<string, object>> cusRes = new CustomResultType<Dictionary<string, object>>();
        //    await cusRes.Execute(path, Variables.HttpType_Get, null, "", arrParams);

        //    IEnumerable<Dictionary<string, object>> res = null;

        //    if (cusRes != null)
        //    {
        //        res = cusRes.DataResult;
        //    }

        //    return res;
        //}


        // https://stgcdndlxad02.dailyxe.com.vn/api/dailyxe/Files/InsertListUrlHinhAnh
        public async Task<ListUrlHinhAnh> InsertListUrlHinhAnh(string title, List<string> lstImages, string token)
        {
            try
            {
                //string path = Variables.ApiDaiLyXe + "Files/InsertListUrlHinhAnh";
                string path = "https://stgcdndlxad02.dailyxe.com.vn/api/dailyxe/Files/InsertListUrlHinhAnh";
                CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();

                JObject objBody = new JObject();
                objBody.Add("sfrv", true);
                objBody.Add("name", title);
                objBody.Add("at", JsonConvert.SerializeObject(lstImages));
                

                await cusResType.Execute(path, Variables.HttpType_Post, objBody, token, null);

                var data = cusResType.DataResult;
                if (data != null && data.Count() > 0)
                {
                    ListUrlHinhAnh listUrlHinhAnh = new ListUrlHinhAnh();
                    listUrlHinhAnh.Total = data.Count();
                    string listId = string.Empty;
                    foreach(var item in data)
                    {
                        listId += item.GetValueFromKey("Id", string.Empty).ToString() + ",";
                    }
                    if(!string.IsNullOrEmpty(listId))
                    {
                        listUrlHinhAnh.ListId = "[" + listId.RemoveLastChar() + "]";
                    }

                    return listUrlHinhAnh;
                }

                return null;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: InsertListUrlHinhAnh", Variables.UrlFromClient + "-" + Variables.ApiDaiLyXe + "Files/InsertListUrlHinhAnh" + "(" + ex.Message + ")", email);
                }
                return null;
            }
        }
        #endregion
    }
}
