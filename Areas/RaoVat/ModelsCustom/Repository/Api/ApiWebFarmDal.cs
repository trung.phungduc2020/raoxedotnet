﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using raoVatApi.Models;
using Response;
using raoVatApi.ControllersRaoVat;
using raoVatApi.Response;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;

namespace Repository
{
    public class ApiWebFarmDal
    {
        #region "Variables"

        private const string linkApiMemoryCache = @"/api/memorycache/";
        private const string action_ClearCacheAuto = "ClearCacheAuto";
        private const string action_ClearCacheByKey = "ClearCacheByKey";
        private const string action_ClearAllCache = "ClearAllCache";

        private const string linkApiCommonFile = @"/api/commonfile/";
        private const string action_CreateFileSiteMap = "CreateFileSiteMap";
        private AdminBll _adminBll;

        public AdminBll AdminBll
        {
            get
            {
                if (_adminBll == null)
                {
                    _adminBll = new AdminBll();
                }
                return _adminBll;
            }
            set => _adminBll = value;
        }

        #endregion

        #region "Commons"

        private CustomResult ConvertCustomResultTypeToCustomResult(CustomResultType<Dictionary<string, object>> cusResType)
        {
            CustomResult cusRes = new CustomResult();
            if (cusResType != null)
            {
                cusRes.DataResult = cusResType.DataResult;
                cusRes.Message = cusResType.Message;
                cusRes.IntResult = cusResType.IntResult;
            }

            return cusRes;
        }

        private string addFirstSplash(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.StartsWith("/") ? value : ("/" + value));
            }

            return value;
        }

        public async Task<IEnumerable<Dictionary<string, object>>> ExecuteApi(string apiController, string apiAction, string jsonParamsAsString)
        {
            try
            {
                if (string.IsNullOrEmpty(apiController))
                {
                    return null;
                }

                string path = Variables.DomainApi + addFirstSplash(apiController) + addFirstSplash(apiAction);
                List<HttpParam> lstParams = null;

                Dictionary<string, string> jsonParams = CommonMethods.DeserializeObject<Dictionary<string, string>>(jsonParamsAsString);

                if (jsonParams != null && jsonParams.Count > 0)
                {
                    lstParams = new List<HttpParam>();

                    foreach (var param in jsonParams)
                    {
                        if (!string.IsNullOrEmpty(param.Key))
                        {
                            lstParams.Add(new HttpParam(param.Key, param.Value));
                        }
                    }
                }

                CustomResultType<Dictionary<string, object>> cusRes = new CustomResultType<Dictionary<string, object>>();
                await cusRes.Execute(path, Variables.HttpType_Get, null, "", lstParams.ToArray());

                IEnumerable<Dictionary<string, object>> res = null;

                if (cusRes != null)
                {
                    res = cusRes.DataResult;
                }

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        
        #endregion

        #region "MemoryCache"

        public async Task<CustomResult> ClearCacheAuto(string callFrom, string entityName, string id)
        {
            CustomResult cusRes = new CustomResult();

            string cccc = callFrom + ",";
            IEnumerable<string> webFarmList = await AdminBll.WebFarm_GetDanhSachWebServer();
            if (webFarmList != null && webFarmList.Count() > 0)
            {
                string path = "";
                var body = new Dictionary<string, object>() { };
                body.Add(CommonParams.entityName, entityName);
                body.Add(CommonParams.id, id);
                string bodyBeforeEncode = CommonMethods.SerializeObject(body);
                string bodyEncode = CommonMethods.EncodeTo64_UTF8(bodyBeforeEncode);

                var body64 = new Dictionary<string, object>() { };
                body64.Add(CommonParams.token_data64, bodyEncode);

                foreach (string webUrl in webFarmList)
                {
                    path = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("/", webUrl, linkApiMemoryCache, action_ClearCacheAuto);
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>();
                    await cusResType.Execute(path, Variables.HttpType_Put, body64, null);
                    cusRes = ConvertCustomResultTypeToCustomResult(cusResType);
                    CommonMethods.WriteLogPerformanceTest($"ApiWebFarmDal callFrom:{callFrom}, exec path: {path}, body: {bodyBeforeEncode}");
                }
            }

            // Chỉ mới return cái webUrl cuối, vì cũng chưa có nhu cầu kiểm tra
            return cusRes;
        }

        public async Task<CustomResult> ClearAllCache()
        {
            CustomResult cusRes = new CustomResult();
            IEnumerable<string> webFarmList = null; // await AdminBll.WebFarm_GetDanhSachWebServer();

            if (webFarmList != null && webFarmList.Count() > 0)
            {
                string path = "";
                var body = new Dictionary<string, object>() { };
                body.Add(CommonParams.clearCacheActive, "0");

                string bodyBeforeEncode = CommonMethods.SerializeObject(body);
                string bodyEncode = CommonMethods.EncodeTo64_UTF8(bodyBeforeEncode);

                var body64 = new Dictionary<string, object>() { };
                body64.Add(CommonParams.token_data64, bodyEncode);

                foreach (string webUrl in webFarmList)
                {
                    path = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("/", webUrl, linkApiMemoryCache, action_ClearAllCache);
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>(path, Variables.HttpType_Put, body64, null);
                    cusRes = ConvertCustomResultTypeToCustomResult(cusResType);
                }
            }

            // Chỉ mới return cái webUrl cuối, vì cũng chưa có nhu cầu kiểm tra
            return cusRes;
        }

        #endregion

        #region "Sitemap"

        public async Task<CustomResult> CreateFileSiteMap(Dictionary<string, string> noiDung, string loaiSiteMap)
        {
            CustomResult cusRes = new CustomResult();
            IEnumerable<string> webFarmList = null; // await AdminBll.WebFarm_GetDanhSachWebServer();

            if (webFarmList != null && webFarmList.Count() > 0)
            {
                string path = "";
                var body = new Dictionary<string, object>() { };
                body.Add(CommonParams.noiDung, noiDung);
                body.Add(CommonParams.loaiSiteMap, loaiSiteMap);

                foreach (string webUrl in webFarmList)
                {
                    path = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("/", webUrl, linkApiCommonFile, action_CreateFileSiteMap);
                    CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>(path, Variables.HttpType_Post, body, null);
                    cusRes = ConvertCustomResultTypeToCustomResult(cusResType);
                    CommonMethods.WriteLogPerformanceTest($"ApiWebFarmDal exec path: {path}, body: {body}");
                }
            }

            // Chỉ mới return cái webUrl cuối, vì cũng chưa có nhu cầu kiểm tra
            return cusRes;
        }

        #endregion
    }
}
