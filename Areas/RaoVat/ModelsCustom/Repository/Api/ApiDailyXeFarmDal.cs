﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using raoVatApi.Models;
using Response;
using raoVatApi.ControllersRaoVat;
using raoVatApi.Response;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;

namespace Repository
{
    public class ApiDailyXeFarmDal
    {
        #region "Variables"        
        // nguyencuongcs 20191107: gọi qua api dailyxe nhưng vẫn giữ /raovat/ để Hữu dễ xử lý CheckRemoteConfig chỉ cần replace dnsname ở đầu.
        private const string _linkApiCommonFileForRaovat = @"/api/raovat/commonfile/";
        private const string _action_CreateFileRemoteConfig = "CreateFileRemoteConfig";

        private AdminBll _adminBll;

        public AdminBll AdminBll
        {
            get
            {
                if (_adminBll == null)
                {
                    _adminBll = new AdminBll();
                }
                return _adminBll;
            }
            set => _adminBll = value;
        }

        #endregion

        #region "Commons"

        private CustomResult ConvertCustomResultTypeToCustomResult(CustomResultType<Dictionary<string, object>> cusResType)
        {
            CustomResult cusRes = new CustomResult();
            if (cusResType != null)
            {
                cusRes.DataResult = cusResType.DataResult;
                cusRes.Message = cusResType.Message;
                cusRes.IntResult = cusResType.IntResult;
            }

            return cusRes;
        }

        private string addFirstSplash(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return (value.StartsWith("/") ? value : ("/" + value));
            }

            return value;
        }

        #endregion

        #region "CreateFileRemoteConfig"

        public async Task<CustomResult> CreateFileRemoteConfig(string connectionStrings, string id)
        {
            CustomResult cusRes = new CustomResult();
            
            IEnumerable<string> farmList = await AdminBll.ApiDailyXeFarm_GetDanhSachWebServer();
            int count = farmList != null ? farmList.Count() : 0;
            CommonMethods.WriteLogApiDailyXeFarm("farmList count: " + count);
            if (count > 0)
            {
                string path = "";
                var body = new Dictionary<string, object>() { };
                body.Add(CommonParams.connectionStrings, connectionStrings);
                body.Add(CommonParams.id, id);

                string bodyBeforeEncode = CommonMethods.SerializeObject(body);
                //string bodyEncode = CommonMethods.EncodeTo64_UTF8(bodyBeforeEncode);

                //var body64 = new Dictionary<string, object>() { };
                //body64.Add(CommonParams.token_data64, bodyEncode);
                count = 1;
                foreach (string webUrl in farmList)
                {
                    CommonMethods.WriteLogApiDailyXeFarm($"farmList {count}: webUrl={webUrl}");
                    count++;

                    if (!string.IsNullOrEmpty(webUrl))
                    {
                        path = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("/", webUrl, _linkApiCommonFileForRaovat, _action_CreateFileRemoteConfig);
                        CustomResultType<Dictionary<string, object>> cusResType = new CustomResultType<Dictionary<string, object>>(path, Variables.HttpType_Post, body, null);
                        cusRes = ConvertCustomResultTypeToCustomResult(cusResType);
                        CommonMethods.WriteLogPerformanceTest($"ApiDailyXeFarmDal exec path: {path}, body: {bodyBeforeEncode}");
                    }
                }
            }

            // Chỉ mới return cái webUrl cuối, vì cũng chưa có nhu cầu kiểm tra
            return cusRes;
        }

        #endregion
    }
}
