﻿using raoVatApi.Common;

namespace raoVatApi.ModelsRaoVat
{
    public class HttpParam
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public string BuildQueryString()
        {
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Value))
            {
                return string.Empty;
            }

            return CommonMethods.FilterTitleTag(Name) + "=" + CommonMethods.FilterTitleTag(Value);
        }

        public string BuildQueryStringForCache()
        {
            if (string.IsNullOrEmpty(Name) || string.IsNullOrEmpty(Value))
            {
                return string.Empty;
            }

            return CommonMethods.FilterTitleTag(Name) + KeyCacheParams.equalString + CommonMethods.FilterTitleTag(Value);
        }

        public HttpParam(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
