using raoVatApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTin : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class MenuRaoVat : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class ThanhVien : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class DangTinTraPhi : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class CauHinhHeThong : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class MaXacNhan : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class UserPoint : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class UserDevice : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }
    public partial class DangTinFavorite : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class DangTinRating : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class UserRank : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class OnlineStatistics : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class NotificationSystem : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class NotificationTemplate : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class NotificationUser : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class DangTinViolation : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class UserBlackList : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class InstallationStatistics : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class UserGroup : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class DangTinContact : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class LayoutAlbum : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class MarketingPlan : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class MarketingCode : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class MarketingUser : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class KpiAutoRule : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }

    public partial class StatisticActivitiesUser : IEntityBase
    {
        [NotMapped]
        public int TotalRow { get; set; }
        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }
    }
    
}