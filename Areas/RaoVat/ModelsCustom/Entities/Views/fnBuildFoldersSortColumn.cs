﻿using raoVatApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public class fnBuildFoldersSortColumn : IEntityBase
    {
        [NotMapped]
        public int Id { get; set; }

        [NotMapped]
        public int TotalRow { get; set; }

        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }

        [NotMapped]
        public string TextSort { get; set; }

        [NotMapped]
        public int CapDo { get; set; }

        [NotMapped]
        public string IdFoldersParentList { get; set; }

        public fnBuildFoldersSortColumn() { }

        public fnBuildFoldersSortColumn(string textSort, int capDo, string idFoldersParentList)
        {
            TextSort = textSort;
            CapDo = capDo;
            IdFoldersParentList = idFoldersParentList;
        }
    }
}
