﻿using raoVatApi.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public class fnBuildMenuSortColumn : IEntityBase
    {
        [NotMapped]
        public int Id { get; set; }

        [NotMapped]
        public int TotalRow { get; set; }

        [NotMapped]
        public IEnumerable<string> PropertyList { get; set; }

        [NotMapped]
        public string TextSort { get; set; }

        [NotMapped]
        public int CapDo { get; set; }

        [NotMapped]
        public string IdMenuParentList { get; set; }

        public fnBuildMenuSortColumn() { }

        public fnBuildMenuSortColumn(string textSort, int capDo, string idMenuParentList)
        {
            TextSort = textSort;
            CapDo = capDo;
            IdMenuParentList = idMenuParentList;
        }
    }
}
