﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MarketingPlan
    {
        private long _idThanhVien;
        private MarketingCode _marketingCode;
        private IEnumerable<MarketingCode> _lstMarketingCode;
        private string _code;
        private string _maCauHinh;

        [NotMapped] 
        public long IdThanhVien_Ext { get => _idThanhVien; set => _idThanhVien = value; }

        [NotMapped]
        public MarketingCode MarketingCode_Ext { get => _marketingCode; set => _marketingCode = value; }

        [NotMapped]
        public IEnumerable<MarketingCode> LstMarketingCode_Ext { get => _lstMarketingCode; set => _lstMarketingCode = value; }

        [NotMapped]
        public string MaCauHinh_Ext { get => _maCauHinh; set => _maCauHinh = value; }
        [NotMapped]
        public string Code_Ext { get => _code; set => _code = value; }

    }
}
