﻿using raoVatApi.Response;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserGroup
    {
        private string _lstIdUser_Ext;
        private List<CustomResult> _lstThanhVien_Ext;

        [NotMapped]
        public List<CustomResult> LstThanhVien_Ext { get => _lstThanhVien_Ext; set => _lstThanhVien_Ext = value; }

        [NotMapped]
        public string LstIdUser_Ext { get => _lstIdUser_Ext; set => _lstIdUser_Ext = value; }
    }
}
