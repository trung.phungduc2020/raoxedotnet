﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class StatisticUserDevice
    {
        private int _totalDevice;

        private int _totalInstalled;
        private int _iosInstalled;
        private int _androidInstalled;
        private int _unknownInstalled;

        private int _totalUninstalled;
        private int _iosUnInstalled;
        private int _androidUnInstalled;
        private int _unknownUnInstalled;

        public int TotalDevice { get => _totalDevice; set => _totalDevice = value; }

        public int TotalInstalled { get => _totalInstalled; set => _totalInstalled = value; }
        public int IosInstalled { get => _iosInstalled; set => _iosInstalled = value; }
        public int AndroidInstalled { get => _androidInstalled; set => _androidInstalled = value; }
        public int UnknownInstalled { get => _unknownInstalled; set => _unknownInstalled = value; }

        public int TotalUninstalled { get => _totalUninstalled; set => _totalUninstalled = value; }
        public int IosUnInstalled { get => _iosUnInstalled; set => _iosUnInstalled = value; }
        public int AndroidUnInstalled { get => _androidUnInstalled; set => _androidUnInstalled = value; }
        public int UnknownUnInstalled { get => _unknownUnInstalled; set => _unknownUnInstalled = value; }
    }
}
