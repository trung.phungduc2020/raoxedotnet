﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MenuRaoVat
    {
        //private ShortFiles _fileDaiDien_Ext;
        private string _tenMenu_Ext;

        //[NotMapped]
        //public ShortFiles FileDaiDien_Ext { get => _fileDaiDien_Ext; set => _fileDaiDien_Ext = value; }

        [NotMapped]
        public string TenMenu_Ext { get => _tenMenu_Ext; set => _tenMenu_Ext = value; }
    }
}
