namespace raoVatApi.ModelsRaoVat
{
    public partial class SendMailModel
    {
        public long? IdUser { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public string DienThoai { get; set; }
    }
}