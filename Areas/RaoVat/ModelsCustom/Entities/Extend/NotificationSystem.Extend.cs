﻿using raoVatApi.Response;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class NotificationSystem
    {
        private bool? _isView;
        // private CustomResult _tenThanhVien_Ext;
        private ThanhVien _thanhVien_Ext;

        [NotMapped]
        public bool? IsView { get => _isView; set => _isView = value; }

        // [NotMapped]
        // public CustomResult TenThanhVien_Ext { get => _tenThanhVien_Ext; set => _tenThanhVien_Ext = value; }

        [NotMapped]
        public ThanhVien ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }
    }
}
