﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class ListUrlHinhAnh
    {
        private int _total;
        private string _listId;

        [NotMapped]
        public int Total { get => _total; set => _total = value; }
        [NotMapped]
        public string ListId { get => _listId; set => _listId = value; }
    }
}