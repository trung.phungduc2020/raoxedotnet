﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public class ThanhVien_DLX
    {
        [NotMapped]
        public long Id { get; set; }
        [NotMapped]
        public int? IdFileDaiDien { get; set; }
        [NotMapped]
        public string HoTen { get; set; }
        [NotMapped]
        public string Email { get; set; }
        [NotMapped]
        public string DienThoai { get; set; }
        [NotMapped]
        public string DiaChi { get; set; }
        [NotMapped]
        public int? IdTinhThanh { get; set; }
        [NotMapped]
        public int? IdQuanHuyen { get; set; }
        [NotMapped]
        public bool? GioiTinh { get; set; }
        [NotMapped]
        public DateTime? NgaySinh { get; set; }

    }
}