﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class EncryptedData
    {
        private string _plainText;
        private string _plainKey;

        private string _cipherText;
        private string _cipherKey;
        
        [NotMapped]
        public string PlainText { get => _plainText; set => _plainText = value; }
        [NotMapped]
        public string PlainKey { get => _plainKey; set => _plainKey = value; }
        [NotMapped]
        public string CipherText { get => _cipherText; set => _cipherText = value; }
        [NotMapped]
        public string CipherKey { get => _cipherKey; set => _cipherKey = value; }
    }
}