﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MarketingUser
    {
        private ThanhVien _thanhVienInvite_Ext;
               
        [NotMapped]        
        public ThanhVien ThanhVienInvite_Ext { get => _thanhVienInvite_Ext; set => _thanhVienInvite_Ext = value; }
    }
}
