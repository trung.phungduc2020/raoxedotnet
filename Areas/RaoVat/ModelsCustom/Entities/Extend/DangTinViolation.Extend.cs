﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinViolation
    {
        private DangTin _dangtin_Ext;

        [NotMapped]
        public DangTin DangTin_Ext { get => _dangtin_Ext; set => _dangtin_Ext = value; }
    }
}
