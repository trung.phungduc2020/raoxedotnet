﻿using raoVatApi.Response;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTin
    {
        private string _tinhTrang_Ext;
        private string _isDuyet_Ext;
        private string _fromToPost_Ext;
        private string _remainingTimeUp_Ext;
        //private ShortFiles _fileDaiDien_Ext;
        //private double _averageRating;
        //private bool? _pinTopStatus;
        private bool? _isLike;
        private DateTime? _ngayPinTopKetThucDuDinh { get; set; }
        private DangTinTraPhi _dangTinTraPhi_Ext { get; set; }
        private bool? _isUpTin;
        private DangTinContact _dangTinContact_Ext { get; set; }
        private LayoutAlbum _layoutAlbum_Ext { get; set; }
        private ThanhVien _thanhVien_Ext { get; set; }
        private int? _isFavorite_Ext { get; set; }

        private long? _idThanhVienContact_Ext { get; set; }
        private string _hoTenContact { get; set; }
        private string _dienThoaiContact { get; set; }
        private string _emailContact { get; set; }
        private string _diaChiContact { get; set; }
        private string _tenTinhThanhContact { get; set; }
        private string _tenQuanHuyenContact { get; set; }
        private long? _idFileDaiDienThanhVien_Ext { get; set; }
        private string _hoTenThanhVien_Ext { get; set; }
        private string _emailThanhVien_Ext { get; set; }
        private string _dienThoaiThanhVien_Ext { get; set; }
        private string _diaChiThanhVien_Ext { get; set; }
        private List<string> _listHinhAnh { get; set; }

        [NotMapped]
        public long? IdThanhVienContact_Ext { get => _idThanhVienContact_Ext; set => _idThanhVienContact_Ext = value; }
        [NotMapped]
        public string HoTenContact_Ext { get => _hoTenContact; set => _hoTenContact = value; }
        [NotMapped]
        public string DienThoaiContact_Ext { get => _dienThoaiContact; set => _dienThoaiContact = value; }
        [NotMapped]
        public string EmailContact_Ext { get => _emailContact; set => _emailContact = value; }
        [NotMapped]
        public string DiaChiContact_Ext { get => _diaChiContact; set => _diaChiContact = value; }
        [NotMapped]
        public string TenTinhThanhContact_Ext { get => _tenTinhThanhContact; set => _tenTinhThanhContact = value; }
        [NotMapped]
        public string TenQuanHuyenContact_Ext { get => _tenQuanHuyenContact; set => _tenQuanHuyenContact = value; }
        [NotMapped]
        public long? IdFileDaiDienThanhVien_Ext { get => _idFileDaiDienThanhVien_Ext; set => _idFileDaiDienThanhVien_Ext = value; }

        [NotMapped]
        public ThanhVien ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }
        [NotMapped]
        public string TinhTrang_Ext { get => _tinhTrang_Ext; set => _tinhTrang_Ext = value; }
        [NotMapped]
        public string IsDuyet_Ext { get => _isDuyet_Ext; set => _isDuyet_Ext = value; }
        [NotMapped]
        public string FromToPost_Ext { get => _fromToPost_Ext; set => _fromToPost_Ext = value; }
        [NotMapped]
        public string RemainingTimeUp_Ext { get => _remainingTimeUp_Ext; set => _remainingTimeUp_Ext = value; }
        //[NotMapped]
        //public ShortFiles FileDaiDien_Ext { get => _fileDaiDien_Ext; set => _fileDaiDien_Ext = value; }
        //[NotMapped]
        //public double AverageRating_Ext { get => _averageRating; set => _averageRating = value; }
        //[NotMapped]
        //public bool? PinTopStatus { get => _pinTopStatus; set => _pinTopStatus = value; }
        [NotMapped]
        public bool? IsLike { get => _isLike; set => _isLike = value; }
        [NotMapped]
        public DateTime? NgayPinTopKetThucDuDinh { get => _ngayPinTopKetThucDuDinh; set => _ngayPinTopKetThucDuDinh = value; }
        [NotMapped]
        public DangTinTraPhi DangTinTraPhi_Ext { get => _dangTinTraPhi_Ext; set => _dangTinTraPhi_Ext = value; }
        [NotMapped]
        public bool? IsUpTin { get => _isUpTin; set => _isUpTin = value; }
        [NotMapped]
        public DangTinContact DangTinContact_Ext { get => _dangTinContact_Ext; set => _dangTinContact_Ext = value; }
        [NotMapped]
        public LayoutAlbum LayoutAlbum_Ext { get => _layoutAlbum_Ext; set => _layoutAlbum_Ext = value; }
        [NotMapped]
        public int? IsFavorite_Ext { get => _isFavorite_Ext; set => _isFavorite_Ext = value; }
        [NotMapped]
        public string HoTenThanhVien_Ext { get => _hoTenThanhVien_Ext; set => _hoTenThanhVien_Ext = value; }
        [NotMapped]
        public string EmailThanhVien_Ext { get => _emailThanhVien_Ext; set => _emailThanhVien_Ext = value; }
        [NotMapped]
        public string DienThoaiThanhVien_Ext { get => _dienThoaiThanhVien_Ext; set => _dienThoaiThanhVien_Ext = value; }
        [NotMapped]
        public string DiaChiThanhVien_Ext { get => _diaChiThanhVien_Ext; set => _diaChiThanhVien_Ext = value; }
        [NotMapped]
        public List<string> ListHinhAnh { get => _listHinhAnh; set => _listHinhAnh = value; }
    }
}
