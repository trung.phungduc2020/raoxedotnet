﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class StatisticActivitiesUser
    {
        private ThanhVien _thanhVien_Ext;

        [NotMapped]
        public ThanhVien ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }
    }
}
