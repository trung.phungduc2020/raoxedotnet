﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class ThanhVien
    {
        private string _tenTinhThanh_Ext;
        private string _tenQuanHuyen_Ext;
        ////private ShortFiles _fileDaiDien_Ext;
        ////private ShortFiles _fileCMNDMatTruoc_Ext;
        ////private ShortFiles _fileCMNDMatSau_Ext;
        //private string _daXacNhanThanhVien_Ext;
        //private string _congTacVien_Ext;
        //private string _tenShowRoom;
        //private long? _displayTotalTime;
        //private long? _displayRemainingTime;
        //private long? _thoiGianDangTinDaMua;
        //private string _ghiChuHistory;
        //private string _displayGhiChuHistory;
        //private bool? _daXacThucCmnd_Ext;

        //private ThanhVien_DLX _thanhVien_Ext { get; set; }


        [NotMapped]
        public string TenTinhThanh { get => _tenTinhThanh_Ext; set => _tenTinhThanh_Ext = value; }

        [NotMapped]
        public string TenQuanHuyen { get => _tenQuanHuyen_Ext; set => _tenQuanHuyen_Ext = value; }

        //[NotMapped]
        //public ThanhVien_DLX ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }

        //[NotMapped]
        //public string NgaySinh_Ext { get => _ngaySinh_Ext; set => _ngaySinh_Ext = value; }
        //[NotMapped]
        //public string GioiTinh_Ext { get => _gioiTinh_Ext; set => _gioiTinh_Ext = value; }
        ////[NotMapped]
        ////public ShortFiles FileDaiDien_Ext { get => _fileDaiDien_Ext; set => _fileDaiDien_Ext = value; }
        ////[NotMapped]
        ////public ShortFiles FileCMNDMatTruoc_Ext { get => _fileCMNDMatTruoc_Ext; set => _fileCMNDMatTruoc_Ext = value; }
        ////[NotMapped]
        ////public ShortFiles FileCMNDMatSau_Ext { get => _fileCMNDMatSau_Ext; set => _fileCMNDMatSau_Ext = value; }
        //[NotMapped]
        //public string DaXacNhanThanhVien_Ext { get => _daXacNhanThanhVien_Ext; set => _daXacNhanThanhVien_Ext = value; }
        //[NotMapped]
        //public string CongTacVien_Ext { get => _congTacVien_Ext; set => _congTacVien_Ext = value; }
        //[NotMapped]
        //public string TenShowRoom { get => _tenShowRoom; set => _tenShowRoom = value; }
        //[NotMapped]
        //public long? DisplayTotalTime { get => _displayTotalTime; set => _displayTotalTime = value; }
        //[NotMapped]
        //public long? DisplayRemainingTime { get => _displayRemainingTime; set => _displayRemainingTime = value; }
        //[NotMapped]
        //public long? ThoiGianDangTinDaMua { get => _thoiGianDangTinDaMua; set => _thoiGianDangTinDaMua = value; }
        //[NotMapped]
        //public string GhiChuHistory { get => _ghiChuHistory; set => _ghiChuHistory = value; }
        //[NotMapped]
        //public string DisplayGhiChuHistory { get => _displayGhiChuHistory; set => _displayGhiChuHistory = value; }
        //[NotMapped]
        //public bool? daXacThucCmnd { get => _daXacThucCmnd_Ext; set => _daXacThucCmnd_Ext = value; }

    }
}
