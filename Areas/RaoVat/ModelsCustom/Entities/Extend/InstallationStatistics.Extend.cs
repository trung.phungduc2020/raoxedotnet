using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class InstallationStatistics
    {
        private string _uUID_Ext { get; set; }
        private string _latitudeX_Ext { get; set; }
        private string _latitudeY_Ext { get; set; }

        private string _deviceName_Ext { get; set; }
        private string _osName_Ext { get; set; }
        private string _ipAddress_Ext { get; set; }

        [NotMapped]
        public string UUID_Ext { get => _uUID_Ext; set => _uUID_Ext = value; }

        [NotMapped]
        public string LatitudeX_Ext { get => _latitudeX_Ext; set => _latitudeX_Ext = value; }

        [NotMapped]
        public string LatitudeY_Ext { get => _latitudeY_Ext; set => _latitudeY_Ext = value; }

        [NotMapped]
        public string DeviceName_Ext { get => _deviceName_Ext; set => _deviceName_Ext = value; }
        [NotMapped]
        public string OsName_Ext { get => _osName_Ext; set => _osName_Ext = value; }
        [NotMapped]
        public string IpAddress_Ext { get => _ipAddress_Ext; set => _ipAddress_Ext = value; }
    }
}