﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.ModelsRaoVat
{
    public class Admin
    {
        [JsonProperty("TotalRow")]
        public int? TotalRow { get; set; }
        [JsonProperty("PropertyList")]
        public string PropertyList { get; set; }
        [JsonProperty("TenAdminTao")]
        public string TenAdminTao { get; set; }
        [JsonProperty("TenAdminCapNhat")]
        public string TenAdminCapNhat { get; set; }
        [JsonProperty("TenNhomQuyen")]
        public string TenNhomQuyen { get; set; }
        [JsonProperty("FileDaiDien_FullName")]
        public string FileDaiDien_FullName { get; set; }
        [JsonProperty("FileDaiDien_Ext")]
        public string FileDaiDien_Ext { get; set; }
        [JsonProperty("Id")]
        public int? Id { get; set; }
        [JsonProperty("IdNhomQuyen")]
        public int? IdNhomQuyen { get; set; }
        [JsonProperty("TenDangNhap")]
        public string TenDangNhap { get; set; }
        [JsonProperty("MatKhau")]
        public string MatKhau { get; set; }
        [JsonProperty("HoTen")]
        public string HoTen { get; set; }
        [JsonProperty("DienThoai")]
        public string DienThoai { get; set; }
        [JsonProperty("Email")]
        public string Email { get; set; }
        [JsonProperty("IsMain")]
        public bool IsMain { get; set; }
        [JsonProperty("RewriteUrl")]
        public string RewriteUrl { get; set; }
        [JsonProperty("TuKhoaTimKiem")]
        public string TuKhoaTimKiem { get; set; }
        [JsonProperty("ThuTu")]
        public int? ThuTu { get; set; }
        [JsonProperty("TrangThai")]
        public int? TrangThai { get; set; }
        [JsonProperty("TieuDeTrang")]
        public string TieuDeTrang { get; set; }
        [JsonProperty("MoTaTrang")]
        public string MoTaTrang { get; set; }
        [JsonProperty("IdFileDaiDien")]
        public int? IdFileDaiDien { get; set; }
        [JsonProperty("IdAdminTao")]
        public int? IdAdminTao { get; set; }
        [JsonProperty("NgayTao")]
        public DateTime? NgayTao { get; set; }
        [JsonProperty("IdAdminCapNhat")]
        public int? IdAdminCapNhat { get; set; }
        [JsonProperty("NgayCapNhat")]
        public DateTime? NgayCapNhat { get; set; }

    }
}
