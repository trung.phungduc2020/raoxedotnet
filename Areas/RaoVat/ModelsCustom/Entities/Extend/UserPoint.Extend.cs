﻿using Newtonsoft.Json.Linq;
using raoVatApi.Response;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserPoint
    {
        //private CustomResult _thanhVien_Ext;
        private long _timeHieuLuc;
        private int _ratePointToTime;
        private CustomResult _thanhVienCustomResult_Ext;
        private string _returnUrl_Ext;
        private string _extraData_Ext;
        private JObject _momoObject_Ext;
        public string _requestId_Ext;
        public string _orderId_Ext;
        private ThanhVien _thanhVien_Ext;

        //[NotMapped]
        //public CustomResult ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }
        [NotMapped]
        public long TimeDangTin_Ext { get => _timeHieuLuc; set => _timeHieuLuc = value; }
        [NotMapped]
        public int RatePointToTime_Ext { get => _ratePointToTime; set => _ratePointToTime = value; }
        [NotMapped]
        public CustomResult ThanhVienCustomResult_Ext { get => _thanhVienCustomResult_Ext; set => _thanhVienCustomResult_Ext = value; }
        [NotMapped]
        public string ReturnUrl_Ext { get => _returnUrl_Ext; set => _returnUrl_Ext = value; }
        [NotMapped]
        public string ExtraData_Ext { get => _extraData_Ext; set => _extraData_Ext = value; }
        [NotMapped]
        public JObject MomoObject_Ext { get => _momoObject_Ext; set => _momoObject_Ext = value; }
        [NotMapped]
        public string RequestId_Ext { get => _requestId_Ext; set => _requestId_Ext = value; }
        [NotMapped]
        public string OrderId_Ext { get => _orderId_Ext; set => _orderId_Ext = value; }

        [NotMapped]
        public ThanhVien ThanhVien_Ext { get => _thanhVien_Ext; set => _thanhVien_Ext = value; }
    }
}
