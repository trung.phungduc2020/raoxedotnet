﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinRating
    {
        private DangTin _dangtin_Ext;
        private string _tieuDeDangTin_Ext;

        [NotMapped]
        public DangTin DangTin_Ext { get => _dangtin_Ext; set => _dangtin_Ext = value; }

        [NotMapped]
        public string TieuDeDangTin_Ext { get => _tieuDeDangTin_Ext; set => _tieuDeDangTin_Ext = value; }
    }
}
