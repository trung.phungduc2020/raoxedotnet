﻿using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class OnlineStatistics
    {
        private int _day_Ext;
        private int _month_Ext;
        private int _year_Ext;
        private string _label_Ext;
        private string _value_Ext;

        [NotMapped]
        public int Day_Ext { get => _day_Ext; set => _day_Ext = value; }
        [NotMapped]
        public int Month_Ext { get => _month_Ext; set => _month_Ext = value; }
        [NotMapped]
        public int Year_Ext { get => _year_Ext; set => _year_Ext = value; }
        [NotMapped]
        public string Label_Ext { get => _label_Ext; set => _label_Ext = value; }
        [NotMapped]
        public string Value_Ext { get => _value_Ext; set => _value_Ext = value; }
    }
}
