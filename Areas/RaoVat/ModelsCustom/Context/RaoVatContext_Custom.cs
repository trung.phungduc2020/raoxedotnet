﻿using System;
using GmailApi;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using raoVatApi.Common;

namespace raoVatApi.ModelsRaoVat
{
    // Adding missing constructors to default RaoVatContext
    public class RaoVatContext_Custom : Context
    {
        private string _connectionString;

        public RaoVatContext_Custom(bool readOnly) : base()
        {
            // nguyencuongcs 20191108: tạm thời thêm biến readOnly để dùng cho chức năng khởi tạo Postgres_Slave nên luôn luôn set lại bằng true
            readOnly = true;

            if (readOnly)
            {
                // Default: Read
                _connectionString = Variables.ConnectionStringPostgresql_RaoVat_Slave;
            }
            else
            {
                // Chắc chắn ko rơi vào trường hợp này.
                // Postgres_Master sẽ tạo bằng constructor RaoVatContext_Custom() mặc định bên dưới 
            }

            // nguyencuongcs 20191107: kiểm tra sức khỏe tốt thì mới ktra còn đã xấu thì ko ktra nữa
            // Tạm thời xử lý được ko chekc nữa lúc đứt kết nối thôi. Còn cần phải tìm cách để hạn chế check chỗ này
            if (Variables.Postgresql_RaoVat_Slave_IsHealthy)
            {
                try
                {

                    using (NpgsqlConnection conn = new NpgsqlConnection(_connectionString))
                    {
                        conn.Open();
                        Variables.ConnectPostgresSlave_CountFailed = 0;
                        conn.Close();
                        conn.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    // nguyencuongcs 20191108: điều kiện ngưng số lần thử kết nối vào slave
                    // nếu bị ngưng thì cần kiểm tra lại và dùng CheckConnect ở CommonFileController để reset Postgresql_RaoVat_Slave_IsHealthy && ConnectPostgresSlave_CountFailed
                    Variables.ConnectPostgresSlave_CountFailed++;
                    if (Variables.ConnectPostgresSlave_CountFailed > Variables.ConnectPostgresSlave_MaxTry)
                    {
                        Variables.Postgresql_RaoVat_Slave_IsHealthy = false;
                    }

                    // Failed => Read from master
                    _connectionString = Variables.ConnectionStringPostgresql_RaoVat_Master;

                    GmailApiCore gac = new GmailApiCore();
                    gac.PostThongBaoHeThong_KetNoiPostgreSql("Connect to slave failed: [" + Variables.ConnectionStringPostgresql_RaoVat_Slave + "]. Connect to master:" + Variables.ConnectionStringPostgresql_RaoVat_Master);
                }
            }
            else
            {
                _connectionString = Variables.ConnectionStringPostgresql_RaoVat_Master;
            }            
        }

        public RaoVatContext_Custom() : base()
        {
            // Default: Read
            _connectionString = Variables.ConnectionStringPostgresql_RaoVat_Master;
        }

        public RaoVatContext_Custom(string connectionString) : base()
        {
            _connectionString = connectionString;
        }

        public RaoVatContext_Custom(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                optionsBuilder.UseNpgsql(_connectionString, builder =>
                {
                    builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(60), null);
                });

                base.OnConfiguring(optionsBuilder);
            }
        }

        public void ClearPool()
        {
            GmailApiCore gac = new GmailApiCore();
            MailService ms = new MailService();
            try
            {
                CommonMethods.WriteLog("Action: ClearAllPools");
                NpgsqlConnection.ClearAllPools();
                //NpgsqlConnection.ClearPool((NpgsqlConnection)Database.GetDbConnection());
                
                // Post Hangout
                gac.PostThongBaoHeThong_ClearPool("Thành công");

                // Send mail
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("RaoVatContext_Custom - Success: ClearPool ", "", email);
                }
            }
            catch (Exception ex)
            {
                string error = " Error ClearPool-" + ex.ToString();
                CommonMethods.WriteLog(error);
                gac.PostThongBaoHeThong_ClearPool(error);
            }
        }

        public void KillConnection()
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();
                    string query = CommonParams.postgresCallFunctionKillConnectionstring;
                    NpgsqlCommand command = new NpgsqlCommand(query, connection);
                    var reader = (Int32)command.ExecuteScalar();
                    CommonMethods.WriteLog("Action: KillConnection - " + reader);
                    if (reader > 60)
                    {
                        ClearPool();
                    }
                }
                catch (Exception ex)
                {
                    CommonMethods.WriteLog("Error KillConnection:" + ex.ToString());
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }
    }
}