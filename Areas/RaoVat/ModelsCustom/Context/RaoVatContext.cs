using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.ModelsRaoVat
{
    public partial class Context : DbContext
    {
        public Context() : base()
        {

        }

        public Context(DbContextOptions options) : base(options)
        {
            // Using to inject connection from Startup.cs            
        }        
    }
}