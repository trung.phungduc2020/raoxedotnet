﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using MimeKit;
using MimeKit.Text;
using raoVatApi.Common;
using Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GmailApi
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public EmailAddress()
        {

        }
        public EmailAddress(string name, string address)
        {
            Name = name;
            Address = address;
        }
    }
    public class GmailApiCore
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/gmail-dotnet-quickstart.json
        static string[] _scopes = { GmailService.Scope.GmailCompose };
        static string _applicationName = "DailyXe Email System";
        private string _credentialsGmailApiFolderPath = Variables.DataPath + "Credentials/";
        private string _credentialsGmailApiNoReplyDailyXe = Variables.DataPath + "Credentials/credentials-noreplydailyxecomvn.json";
        private string _tokenGmailApiNoReplyDailyXe = Variables.DataPath + "Credentials/token-noreplydailyxecomvn.json";
        private UserCredential _credential;
        private string _hangoutBotRaovatTriggerApiUrl = "https://chat.googleapis.com/v1/spaces/AAAApfAgC4E/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=eMzIcy6sadGI-EW2s8BJ5v-SMOMdAx2UW4YabdoSFKc%3D";
        private string _hangoutBotRaovatProdTracking = "https://chat.googleapis.com/v1/spaces/AAAApfAgC4E/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=ectMx7THTY0k0J9FVU0XtOA2CW22H8j13OD_zQSo1_8%3D";

        public string CredentialsGmailApiNoReplyDailyXe { get => _credentialsGmailApiNoReplyDailyXe; set => _credentialsGmailApiNoReplyDailyXe = value; }
        public string TokenGmailApiNoReplyDailyXe { get => _tokenGmailApiNoReplyDailyXe; set => _tokenGmailApiNoReplyDailyXe = value; }

        public void Test()
        {
            using (var stream = new FileStream(CredentialsGmailApiNoReplyDailyXe, FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = TokenGmailApiNoReplyDailyXe;
                _credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    _scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = _credential,
                ApplicationName = _applicationName,
            });

            // Define parameters of request.
            UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");

            // List labels.
            IList<Label> labels = request.Execute().Labels;
            Console.WriteLine("Labels:");
            if (labels != null && labels.Count > 0)

            {
                foreach (var labelItem in labels)
                {
                    Console.WriteLine("{0}", labelItem.Name);
                }
            }
            else
            {
                Console.WriteLine("No labels found.");
            }
            Console.Read();
        }

        public Message SendEmailDangKyQuangCaoThanhCong()
        {
            string link = "https://dailyxe.com.vn/email-marketing/2/e21hOiJ4dWFuZHVjQGRhaWx5eGUuY29tLnZuIixvdDoiWHXDom4gxJDhu6ljIixkYzoiMiIsZHQ6IjMiLGdnOiIwNi0xNC0yMDE5IDAxOjE3In0=.html";
            ApiGetCore<string> apiGet = new ApiGetCore<string>();

            string bodyHtml = apiGet.GetAsyncReturnString(link, Variables.HttpType_Get, "", null).Result; // "<div style='color:red'>Chúc mừng Phú Cường hehehehe </div>";

            MimeMessage emailThongBao = CreateEmail_DisplayAsHeThong("phucuong@dailyxe.com.vn", "", "", "Đặt quảng cáo thành công", bodyHtml, true);

            using (var stream = new FileStream(CredentialsGmailApiNoReplyDailyXe, FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = TokenGmailApiNoReplyDailyXe;
                _credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    _scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = _credential,
                ApplicationName = _applicationName,
            });

            var msg = SendMessage(service, "", emailThongBao);
            return msg;
        }

        public Message SendMessage(string receiveName, string ccEmail, string bccEmail, string tieuDe, string noiDung)
        {
            try
            {
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin SendMessage");
                string bodyHtml = noiDung;
                MimeMessage emailThongBao = CreateEmail_DisplayAsHeThong(receiveName, ccEmail, bccEmail, tieuDe, bodyHtml, true);

                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-M1: SendMessage" + receiveName + ":_:" + CredentialsGmailApiNoReplyDailyXe);

                using (var stream = new FileStream(CredentialsGmailApiNoReplyDailyXe, FileMode.Open, FileAccess.Read))
                {
                    // The file token.json stores the user's access and refresh tokens, and is created
                    // automatically when the authorization flow completes for the first time.
                    string credPath = TokenGmailApiNoReplyDailyXe;
                    _credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        _scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                }

                // Create Gmail API service.
                var service = new GmailService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = _credential,
                    ApplicationName = _applicationName,
                });

                var msg = SendMessage(service, "", emailThongBao);
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-End SendMessage:" + msg);
                return msg;
            }
            catch(Exception ex)
            {
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Ex:" + ex.Message);
                return null;
            }
        }

        /*
        * Send an email from the user's mailbox to its recipient.
        *
        * @param service Authorized Gmail API instance.
        * @param userId User's email address. The special value "me"
        * can be used to indicate the authenticated user.
        * @param emailContent Email to be sent.
        * @return The sent message
        * @throws MessagingException
        * @throws IOException
        */
        public Message SendMessage(GmailService service, string userId, MimeMessage emailContent)
        {
            Message message = CreateMessageWithEmail(emailContent);
            userId = string.IsNullOrEmpty(userId) ? "me" : userId;
            Console.WriteLine("Sending Message: " + userId);
            message = service.Users.Messages.Send(message, userId).Execute();
            Console.WriteLine("End Sending Message: " + message);
            return message;
        }

        /**
     * Create a message from an email.
     *
     * @param emailContent Email to be set to raw of message
     * @return a message containing a base64url encoded email
     * @throws IOException
     * @throws MessagingException
     */
        public Message CreateMessageWithEmail(MimeMessage emailContent)
        {
            string encodedEmail = EncodeBase64UrlSafety(emailContent);
            Message message = new Message();
            message.Raw = encodedEmail;
            return message;
        }

        public MimeMessage CreateEmail_DisplayAsHeThong(string to, string cc, string bcc, string subject, string bodyText, bool bodyIsHtml)
        {
            string fromEmail = "noreply@dailyxe.com.vn";
            string fromEmailDisplayName = "Hệ thống DailyXe";
            return CreateEmail(fromEmail, fromEmailDisplayName, to, cc, bcc, subject, bodyText, bodyIsHtml);
        }

        /**
         * Create a MimeMessage using the parameters provided.
         *
         * @param to email address of the receiver
         * @param from email address of the sender, the mailbox account
         * @param subject subject of the email
         * @param bodyText body text of the email
         * @return the MimeMessage to be used to send email
         * @throws MessagingException
         */
        public MimeMessage CreateEmail(string fromEmail, string fromEmailDisplayName, string to, string cc, string bcc, string subject, string bodyText, bool bodyIsHtml)
        {
            //Properties props = new Properties();
            //Session session = Session.getDefaultInstance(props, null);
            string fromEmailAddess = fromEmail;
            string fromEmailName = fromEmailDisplayName; //"DailyXe [noreply]";

            MimeMessage email = new MimeMessage();
            try
            {
                MailboxAddress mbAdd = new MailboxAddress(fromEmailName, fromEmailAddess);
                InternetAddress iAdd = InternetAddress.Parse(fromEmailAddess);
                iAdd.Name = fromEmailName;            
                email.From.Add(iAdd);
                
                string[] arrTo = to.NullIsEmpty().Split(Variables.EMAIL_KEY_SPLIT);
                if (arrTo != null && arrTo.Count() > 0)
                {
                    for (int i = 0; i < arrTo.Count(); i++)
                    {
                        if (!string.IsNullOrEmpty(arrTo[i]) && CommonMethods.IsValidEmail(arrTo[i]))
                        {
                            email.To.Add(InternetAddress.Parse(arrTo[i]));
                        }
                    }
                }

                string[] arrCc = cc.NullIsEmpty().Split(Variables.EMAIL_KEY_SPLIT);
                if (arrCc != null && arrCc.Count() > 0)
                {
                    for (int i = 0; i < arrCc.Count(); i++)
                    {
                        if (!string.IsNullOrEmpty(arrCc[i]) && CommonMethods.IsValidEmail(arrCc[i]))
                        {
                            email.Cc.Add(InternetAddress.Parse(arrCc[i]));
                        }
                    }
                }

                string[] arrBcc = bcc.NullIsEmpty().Split(Variables.EMAIL_KEY_SPLIT);
                if (arrBcc != null && arrBcc.Count() > 0)
                {
                    for (int i = 0; i < arrBcc.Count(); i++)
                    {
                        if (!string.IsNullOrEmpty(arrBcc[i]) && CommonMethods.IsValidEmail(arrBcc[i]))
                        {
                            email.Bcc.Add(InternetAddress.Parse(arrBcc[i]));
                        }
                    }
                }

                //email.Sender = mbAdd;
                email.Subject = subject;
                if (!bodyIsHtml)
                {
                    email.Body = new TextPart(TextFormat.Plain)
                    {
                        Text = bodyText,
                    };
                }
                else
                {
                    email.Body = new TextPart(TextFormat.Html)
                    {
                        Text = bodyText,
                    };
                }
            }
            catch (Exception ex)
            {
                PostThongBaoHeThong_GmailApi(ex.ToString());
            }
            return email;
        }

        // Convert an object to a byte array
        private byte[] ObjectToByteArray(MimeMessage obj)
        {
            if (obj == null)
                return null;

            string filePath = _credentialsGmailApiFolderPath + $"ObjectToByteArray_{CommonMethods.FilterNumber(CommonMethods.FormatDateTimeMMDDYYYY_FULL(DateTime.Now))}" + ".txt";
            obj.WriteTo(filePath);

            string strContentFilePath = "";
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    strContentFilePath = reader.ReadToEnd();
                }
                // Vì xml tin tức ko chứa đúng cấu trúc node xml nên chỉ cần lấy hết text lên, ko cần xử lý lấy node
                //result = await System.IO.File.ReadAllTextAsync(pathFile);
            }

            byte[] res = System.Text.Encoding.UTF8.GetBytes(strContentFilePath);

            File.Delete(filePath);

            return res;
        }

        private string EncodeBase64UrlSafety(MimeMessage emailContent)
        {
            string res = "";
            try
            {
                byte[] bytes = ObjectToByteArray(emailContent);
                char[] padding = { '=' };
                res = System.Convert.ToBase64String(bytes).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
            return res;
        }

        private string DecodeBase64UrlSafety(string returnValue)
        {
            string originalText = "";
            try
            {
                string incoming = returnValue.Replace('_', '/').Replace('-', '+');
                switch (returnValue.Length % 4)
                {
                    case 2: incoming += "=="; break;
                    case 3: incoming += "="; break;
                }
                byte[] bytes = Convert.FromBase64String(incoming);

                originalText = Encoding.ASCII.GetString(bytes);
            }
            catch (Exception ex)
            {

            }

            return originalText;
        }

        public void PostThongBaoHeThong_ClearPool(string messsage)
        {
            try
            {
                string tieuDe = "RaoVatContext ClearPool";
                Variables.CountSentMailClearPool++;
                if (Variables.CountSentMailClearPool > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_CommitDb(string messsage)
        {
            try
            {
                string tieuDe = "EntityBaseRepository CommitDb";
                Variables.CountSentMailCommitDbFromEntityBaseError++;
                if (Variables.CountSentMailCommitDbFromEntityBaseError > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_KetNoiPostgreSql(string messsage)
        {
            try
            {
                string tieuDe = "Error PostgreSql";
                Variables.CountSentMailPostgresSql++;
                if (Variables.CountSentMailPostgresSql > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_KetNoiGoogleDriveFileStream(string messsage)
        {
            try
            {
                string tieuDe = "Không thể kết nối Google drive file stream";
                Variables.CountSentMailGoogleDriveFileStream++;
                if (Variables.CountSentMailGoogleDriveFileStream > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_KetNoiRedisServer(string messsage)
        {
            try
            {
                string tieuDe = "Không thể kết nối Redis server";
                Variables.CountSentMailCacheError++;
                if (Variables.CountSentMailCacheError > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_ExecChiTiet(string messsage)
        {
            try
            {
                string tieuDe = "ExecChiTiet";
                Variables.CountSentMailExecChiTietError++;
                if (Variables.CountSentMailExecChiTietError > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public void PostThongBaoHeThong_GmailApi(string messsage)
        {
            try
            {
                string tieuDe = "GmailApi";
                Variables.CountSentMailGmailApiError++;
                if (Variables.CountSentMailGmailApiError > Variables.MaxCountSentMailErrorCommon)
                {
                    // Tránh spam mail: chờ admin giải quyết, restart IIS api là sẽ reset được biến CountSentMailCacheError
                    return;
                }
                HangoutPostToDailyXeProdTracking(tieuDe + " - " + messsage);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("Exception=" + ex.ToString());
            }
        }

        public string HangoutPostToTriggerApi(string messsage)
        {
            return PostToHangout(_hangoutBotRaovatTriggerApiUrl, messsage);
        }

        public string HangoutPostToDailyXeProdTracking(string messsage)
        {
            return PostToHangout(_hangoutBotRaovatProdTracking, messsage);
        }

        public string PostToHangout(string botUrl, string messsage)
        {
            // nguyencuongcs 20190620: bổ sung điều kiện này !Variables.DomainName.Contains("192.168.11.3") để Hữu test
            if (!Variables.EnvironmentIsProduction)
            {
                return Messages.ERR_007;
            }

            string webHookDailyXeProdTracking = botUrl;
            if (string.IsNullOrEmpty(webHookDailyXeProdTracking))
            {
                return Messages.ERR_002;
            }

            Dictionary<string, string> dicBotMessage = new Dictionary<string, string>();

            string machineName = System.Environment.MachineName;
            string userName = System.Environment.UserName;
            string xuLyMessage = $"{Variables.DomainName}|{Variables.ServerName}|{machineName}|{userName}|{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)} - {messsage}";

            dicBotMessage.Add("text", $"{xuLyMessage}");

            string threadName = CommonMethods.FormatDateTimeDDMMYYYY(DateTime.Now);
            webHookDailyXeProdTracking += $"&threadKey={threadName}";

            string strBotMessage = CommonMethods.SerializeObject(dicBotMessage);

            ApiGetCore<string> apiGet = new ApiGetCore<string>();
            var res = apiGet.GetAsyncReturnString(webHookDailyXeProdTracking, Variables.HttpType_Post, dicBotMessage, "");

            return CommonMethods.ConvertToString(res);
        }
    }
}
