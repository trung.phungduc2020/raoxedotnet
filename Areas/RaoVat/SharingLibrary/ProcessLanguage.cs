﻿using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Runtime.Caching;

public partial class ProcessLanguage
{
    public const string keyCache_dicObscene = "dicObscene";
    public static readonly MemoryCache _cache = MemoryCache.Default;
    public const string _keyCacheDicObscene = "dicObscene";

    #region Xử lý chuỗi thường
    //Bỏ dấu các từ tiếng việt
    public string ConvertUnicodeToASCII(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            text = "";
        }
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\p{IsCombiningDiacriticalMarks}+");
        string strFormD = text.ToLower().Normalize(System.Text.NormalizationForm.FormD);
        string res = regex.Replace(strFormD, String.Empty).Replace('đ', 'd');
        return res;
    }

    //Loại bỏ dấu các ký tự tục tĩu
    public string ConvertObsceneToASCII(string text, string regexObscene = "")
    {
        try
        {
            //if (!_cache.Contains(_keyCacheDicObscene))
            if (string.IsNullOrEmpty(Variables.regexDicObscene))
            {
                using (StreamReader r = new StreamReader("Areas/RaoVat/SharingLibrary/dicObscene.json"))
                {
                    string json = r.ReadToEnd();
                    JObject jsonObj = CommonMethods.ConvertToJObject(json);
                    List<string> lDicObscene = jsonObj.Properties().Select(p => CommonMethods.ConvertToString(p.Value)).ToList();
                    Variables.regexDicObscene = lDicObscene.ConvertListToString("|");
                    MemoryCache.Default.AddOrGetExisting(keyCache_dicObscene, Variables.regexDicObscene, DateTime.Now.AddMinutes(Variables.timeOutCache)); // trong vòng 1 tiếng
                    //_cache.AddCache(_keyCacheDicObscene, Variables.RegexDicObscene, new TimeSpan(DateTime.Now.AddHours(1).Ticks));
                }
            }
        }
        catch (Exception)
        {}

        if (regexObscene.Length == 0)
        {
            regexObscene = Variables.regexDicObscene;//regexDicObscene tạo biến bên ngoài vào
        }

        if (regexObscene.Length > 0)
        {
            text = Regex.Replace(text, regexObscene, delegate (Match match)
            {
                string v = CommonMethods.ConvertToString(match);
                return CommonMethods.ConvertToString(v).ConvertUnicodeToASCII();
            });
        }

        return text;
    }

    //Loại bỏ các khoảng trắng thừa
    public string RemoveWhiteSpace(string text)
    {
        if (text.NullIsEmpty() == string.Empty)
        {
            return string.Empty;
        }
        text = text.Trim();
        text = Regex.Replace(text, @"[ ]+", " ");

        return text;
    }

    //Hàm xử lý cho từ khóa trước khi lưu
    public string ToLowerStandard(string str)
    {
        if (str == null || str.Length == 0)
        {
            return str;
        };
        return ConvertObsceneToASCII(RemoveWhiteSpace(str.Trim().ToLower()));
    }
    #endregion 

    #region Xử lý html
    public string RemoveHtml(string text)
    {
        return Regex.Replace(text, "<[^>]*>", string.Empty).ToTextMinifier();
    }

    public string ToTextMinifier(string text)
    {
        if (text.NullIsEmpty() == string.Empty)
        {
            return string.Empty;
        }
        text = Regex.Replace(text, @"[\n\t\r]+", " ");
        text = Regex.Replace(text, "/\"", "\"");
        text = text.RemoveWhiteSpace();
        return text;
    }
    #endregion
}