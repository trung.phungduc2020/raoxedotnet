﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MaXacNhan
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Code { get; set; }
        public int? LoaiXacNhan { get; set; }
        public DateTime? NgayCap { get; set; }
    }
}
