﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MarketingPlan
    {
        public long Id { get; set; }
        public string TieuDe { get; set; }
        public string MaCauHinh { get; set; }
        public string Value { get; set; }
        public int? ValueType { get; set; }
        public bool? UnlimitedTime { get; set; }
        public bool? UnlimitedUsed { get; set; }
        public string MoTaNgan { get; set; }
        public int? TrangThai { get; set; }
        public string GhiChu { get; set; }
        public DateTime? TuNgay { get; set; }
        public DateTime? DenNgay { get; set; }
        public int? Type { get; set; }
        public DateTime NgayTao { get; set; }
        public string MaxValue { get; set; }
        public int? MaxUsed { get; set; }
        public int? TotalUsed { get; set; }
        public long? IdFileDaiDien { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public int? ThuTu { get; set; }
    }
}
