﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class NotificationTemplate
    {
        public long Id { get; set; }
        public string Ma { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string Link { get; set; }
        public string GhiChu { get; set; }
        public string TuKhoaTimKiem { get; set; }
    }
}
