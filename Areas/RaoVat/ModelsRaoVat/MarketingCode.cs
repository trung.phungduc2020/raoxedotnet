﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MarketingCode
    {
        public long Id { get; set; }
        public long? IdMarketingPlan { get; set; }
        public long? IdThanhVien { get; set; }
        public string Code { get; set; }
        public int? MaxUsed { get; set; }
        public int? TotalUsed { get; set; }
        public DateTime NgayTao { get; set; }
        public int? TrangThai { get; set; }
    }
}
