﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class KpiAutoRule
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public int? Rule { get; set; }
        public int? Type { get; set; }
        public bool? IsApplied { get; set; }
        public string Note { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
    }
}
