﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinContact
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public string HoTen { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public long? IdTinhThanh { get; set; }
        public long? IdQuanHuyen { get; set; }
        public string DiaChi { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public int? TrangThai { get; set; }
        public bool? IsDefault { get; set; }
        public string TenTinhThanh { get; set; }
        public string TenQuanHuyen { get; set; }
    }
}
