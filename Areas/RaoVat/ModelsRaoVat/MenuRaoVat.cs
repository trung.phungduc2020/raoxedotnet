﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MenuRaoVat
    {
        public long Id { get; set; }
        public long? IdMenuParent { get; set; }
        public string IdMenuParentList { get; set; }
        public long? IdFileDaiDien { get; set; }
        public int? CapDo { get; set; }
        public string TenMenu { get; set; }
        public string RewriteUrl { get; set; }
        public string UrlPrexix { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public string TieuDeTrang { get; set; }
        public string MoTaTrang { get; set; }
        public string TextSort { get; set; }
        public string GhiChu { get; set; }
        public int? ThuTu { get; set; }
        public int? TrangThai { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime? NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
    }
}
