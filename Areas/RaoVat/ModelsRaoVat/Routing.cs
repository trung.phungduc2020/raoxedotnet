﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class Routing
    {
        public int Id { get; set; }
        public string RawUrlRule { get; set; }
        public string Controller { get; set; }
        public string ViewName { get; set; }
        public int? PageId { get; set; }
        public string Description { get; set; }
        public int? IdMenu { get; set; }
        public bool? IsReg { get; set; }
        public string Name { get; set; }
        public string RegAfterMatch { get; set; }
        public string RegRule { get; set; }
        public int? ThuTu { get; set; }
        public string SystemName { get; set; }
        public string MaRouting { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public int? IdAdminTao { get; set; }
        public DateTime? NgayTao { get; set; }
        public int? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string MobileController { get; set; }
        public int? MobilePageId { get; set; }
        public string MobileViewName { get; set; }
    }
}
