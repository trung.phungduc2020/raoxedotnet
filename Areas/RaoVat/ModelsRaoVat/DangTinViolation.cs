﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinViolation
    {
        public long Id { get; set; }
        public long? IdDangTin { get; set; }
        public long IdThanhVienTao { get; set; }
        public string LyDo { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string NoiDungXuLy { get; set; }
        public int? TrangThai { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public string TenThanhVienTao { get; set; }
    }
}
