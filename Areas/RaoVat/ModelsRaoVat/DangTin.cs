﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTin
    {
        public long Id { get; set; }
        public long? IdMenu { get; set; }
        public string TenMenu { get; set; }
        public long? IdThuongHieu { get; set; }
        public string TenThuongHieu { get; set; }
        public long? IdThanhVien { get; set; }
        public long? IdFileDaiDien { get; set; }
        public string TieuDe { get; set; }
        public string MoTaNgan { get; set; }
        public string RewriteUrl { get; set; }
        public int? TinhTrang { get; set; }
        public decimal? Gia { get; set; }
        public DateTime? TuNgay { get; set; }
        public DateTime? DenNgay { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public long? SoLuotXem { get; set; }
        public bool? IsDuyet { get; set; }
        public DateTime? NgayDuyet { get; set; }
        public bool? IsPinTop { get; set; }
        public DateTime? NgayPinTop { get; set; }
        public string GhiChu { get; set; }
        public DateTime? NgayUp { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? AdminNgayCapNhat { get; set; }
        public string ListHinhAnhJson { get; set; }
        public int? Type { get; set; }
        public long? IdDongXe { get; set; }
        public string TenDongXe { get; set; }
        public long? IdLoaiXe { get; set; }
        public string TenLoaiXe { get; set; }
        public string DongCo { get; set; }
        public string HopSo { get; set; }
        public int? SoChoNgoi { get; set; }
        public int? SoCua { get; set; }
        public int? NamSanXuat { get; set; }
        public long? IdMauNoiThat { get; set; }
        public string TenMauNoiThat { get; set; }
        public long? IdMauNgoaiThat { get; set; }
        public string TenMauNgoaiThat { get; set; }
        public int? SoKmDaDi { get; set; }
        public int? NamDangKyXe { get; set; }
        public int? NhienLieu { get; set; }
        public decimal? MucTieuHaoNhienLieu100 { get; set; }
        public int? TotalRate { get; set; }
        public int? Rate1 { get; set; }
        public int? Rate2 { get; set; }
        public int? Rate3 { get; set; }
        public int? Rate4 { get; set; }
        public int? Rate5 { get; set; }
        public decimal? AvgRate { get; set; }
        public long? IdDangTinContact { get; set; }
        public long? IdTinhThanh { get; set; }
        public long? IdQuanHuyen { get; set; }
        public int? TongImage { get; set; }
        public string MaLayoutAlbum { get; set; }
        public long? IdAdminDangKyDuyet { get; set; }
        public string ThanhVienLike { get; set; }
        public int? ThanhVienLikeCount { get; set; }
        public string ThanhVienFavorite { get; set; }
        public int? ThanhVienFavoriteCount { get; set; }
        public string ListFilesUploaded { get; set; }
        public string IdDaiLy { get; set; }
        public string TenAdminDangKyDuyet { get; set; }
        public string OsName { get; set; }
        public string OsVersion { get; set; }
        public string AppVersion { get; set; }
        public bool? IsAutoDuyet { get; set; }
        public long? IdAdminTuChoi { get; set; }
        public string TenAdminTuChoi { get; set; }
        public string MoTaChiTiet { get; set; }
    }
}
