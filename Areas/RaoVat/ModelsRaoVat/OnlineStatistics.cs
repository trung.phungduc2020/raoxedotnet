﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class OnlineStatistics
    {
        public long Id { get; set; }
        public long? TotalOnline { get; set; }
        public DateTime? OnlineDate { get; set; }
        public string ListJson { get; set; }
    }
}
