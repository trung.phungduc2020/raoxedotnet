﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class InstallationStatistics
    {
        public long Id { get; set; }
        public string OsName { get; set; }
        public string Version { get; set; }
        public long? Installation { get; set; }
        public string UpdateVersion { get; set; }
        public string DeviceName { get; set; }
        public string DeviceId { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
    }
}
