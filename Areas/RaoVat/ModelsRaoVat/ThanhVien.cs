﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class ThanhVien
    {
        public long Id { get; set; }
        public int? TrangThai { get; set; }
        public long? IdRank { get; set; }
        public long? TotalTime { get; set; }
        public string IdGroups { get; set; }
        public int OriginalFrom { get; set; }
        public string DangTinLike { get; set; }
        public int? DangTinLikeCount { get; set; }
        public string DangTinFavorite { get; set; }
        public int? DangTinFavoriteCount { get; set; }
        public string IdMd5 { get; set; }
        public string AllowKpiRule { get; set; }
        public bool? IsActiveMaGioiThieu { get; set; }
        public string DienThoai { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public long? IdFileDaiDien { get; set; }
        public string DiaChi { get; set; }
        public long? IdTinhThanh { get; set; }
        public long? IdQuanHuyen { get; set; }
        public bool? GioiTinh { get; set; }
        public DateTime? NgaySinh { get; set; }
        public long? IdFileCmndTruoc { get; set; }
        public long? IdFileCmndSau { get; set; }
        public string ListHinhAnhJsonPassport { get; set; }
        public bool? DaXacThucCmnd { get; set; }
        public bool? DaXacThucPassport { get; set; }
        public bool? DaXacNhanEmail { get; set; }
        public bool? DaXacNhanDienThoai { get; set; }
        public string ListHinhAnh { get; set; }
        public string UidThanhVien { get; set; }
    }
}
