﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace raoVatApi.ModelsRaoVat
{
    public partial class Context : DbContext
    {
        public virtual DbSet<CauHinhHeThong> CauHinhHeThong { get; set; }
        public virtual DbSet<DangTin> DangTin { get; set; }
        public virtual DbSet<DangTinContact> DangTinContact { get; set; }
        public virtual DbSet<DangTinFavorite> DangTinFavorite { get; set; }
        public virtual DbSet<DangTinRating> DangTinRating { get; set; }
        public virtual DbSet<DangTinTraPhi> DangTinTraPhi { get; set; }
        public virtual DbSet<DangTinViolation> DangTinViolation { get; set; }
        public virtual DbSet<InstallationStatistics> InstallationStatistics { get; set; }
        public virtual DbSet<KpiAutoRule> KpiAutoRule { get; set; }
        public virtual DbSet<LayoutAlbum> LayoutAlbum { get; set; }
        public virtual DbSet<MaXacNhan> MaXacNhan { get; set; }
        public virtual DbSet<MarketingCode> MarketingCode { get; set; }
        public virtual DbSet<MarketingPlan> MarketingPlan { get; set; }
        public virtual DbSet<MarketingUser> MarketingUser { get; set; }
        public virtual DbSet<MenuRaoVat> MenuRaoVat { get; set; }
        public virtual DbSet<NotificationSystem> NotificationSystem { get; set; }
        public virtual DbSet<NotificationTemplate> NotificationTemplate { get; set; }
        public virtual DbSet<NotificationUser> NotificationUser { get; set; }
        public virtual DbSet<OnlineStatistics> OnlineStatistics { get; set; }
        public virtual DbSet<StatisticActivitiesUser> StatisticActivitiesUser { get; set; }
        public virtual DbSet<ThanhVien> ThanhVien { get; set; }
        public virtual DbSet<UserBlackList> UserBlackList { get; set; }
        public virtual DbSet<UserDevice> UserDevice { get; set; }
        public virtual DbSet<UserGroup> UserGroup { get; set; }
        public virtual DbSet<UserPoint> UserPoint { get; set; }
        public virtual DbSet<UserRank> UserRank { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=171.244.35.196;Port=32664;Database=stg_raovat;Username=postgres;Password=DAilyxe2019");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<CauHinhHeThong>(entity =>
            {
                entity.Property(e => e.GhiChu).HasMaxLength(500);

                entity.Property(e => e.GiaTriCauHinh).HasColumnType("character varying");

                entity.Property(e => e.MaCauHinh).HasColumnType("character varying");
            });

            modelBuilder.Entity<DangTin>(entity =>
            {
                entity.Property(e => e.AppVersion).HasColumnType("character varying");

                entity.Property(e => e.AvgRate).HasColumnType("numeric(16,1)");

                entity.Property(e => e.DongCo).HasColumnType("character varying");

                entity.Property(e => e.Gia).HasColumnType("numeric(16,1)");

                entity.Property(e => e.HopSo).HasColumnType("character varying");

                entity.Property(e => e.IdDaiLy).HasColumnType("character varying");

                entity.Property(e => e.ListFilesUploaded).HasColumnType("character varying");

                entity.Property(e => e.ListHinhAnhJson).HasMaxLength(3900);

                entity.Property(e => e.MaLayoutAlbum).HasColumnType("character varying");

                entity.Property(e => e.MoTaChiTiet).HasColumnType("character varying");

                entity.Property(e => e.MoTaNgan).HasMaxLength(1000);

                entity.Property(e => e.MucTieuHaoNhienLieu100).HasColumnType("numeric(4,1)");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OsName).HasColumnType("character varying");

                entity.Property(e => e.OsVersion).HasColumnType("character varying");

                entity.Property(e => e.RewriteUrl).HasMaxLength(500);

                entity.Property(e => e.TenAdminDangKyDuyet).HasColumnType("character varying");

                entity.Property(e => e.TenAdminTuChoi).HasColumnType("character varying");

                entity.Property(e => e.TenDongXe).HasColumnType("character varying");

                entity.Property(e => e.TenLoaiXe).HasColumnType("character varying");

                entity.Property(e => e.TenMauNgoaiThat).HasColumnType("character varying");

                entity.Property(e => e.TenMauNoiThat).HasColumnType("character varying");

                entity.Property(e => e.TenMenu).HasMaxLength(500);

                entity.Property(e => e.TenThuongHieu).HasMaxLength(500);

                entity.Property(e => e.ThanhVienFavorite).HasColumnType("character varying");

                entity.Property(e => e.ThanhVienLike).HasColumnType("character varying");

                entity.Property(e => e.TieuDe).HasMaxLength(500);

                entity.Property(e => e.TuKhoaTimKiem).HasMaxLength(1000);
            });

            modelBuilder.Entity<DangTinContact>(entity =>
            {
                entity.Property(e => e.DiaChi).HasColumnType("character varying");

                entity.Property(e => e.DienThoai).HasColumnType("character varying");

                entity.Property(e => e.Email).HasColumnType("character varying");

                entity.Property(e => e.HoTen).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TenQuanHuyen).HasColumnType("character varying");

                entity.Property(e => e.TenTinhThanh).HasColumnType("character varying");
            });

            modelBuilder.Entity<DangTinFavorite>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<DangTinRating>(entity =>
            {
                entity.Property(e => e.Description).HasColumnType("character varying");

                entity.Property(e => e.HoTenThanhVien).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Rating).HasColumnType("numeric(1000,1)");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<DangTinTraPhi>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"TinTraPhi_Id_seq\"'::regclass)");

                entity.Property(e => e.NgayTao)
                    .HasColumnType("timestamp(4) without time zone")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<DangTinViolation>(entity =>
            {
                entity.Property(e => e.LyDo).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NoiDungXuLy).HasColumnType("character varying");

                entity.Property(e => e.TenThanhVienTao).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<InstallationStatistics>(entity =>
            {
                entity.Property(e => e.DeviceId).HasColumnType("character varying");

                entity.Property(e => e.DeviceName).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OsName).HasColumnType("character varying");

                entity.Property(e => e.UpdateVersion).HasColumnType("character varying");

                entity.Property(e => e.Version).HasColumnType("character varying");
            });

            modelBuilder.Entity<KpiAutoRule>(entity =>
            {
                entity.Property(e => e.Code).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).HasColumnType("character varying");
            });

            modelBuilder.Entity<LayoutAlbum>(entity =>
            {
                entity.Property(e => e.Ma).HasColumnType("character varying");
            });

            modelBuilder.Entity<MaXacNhan>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"CodeVerified_Id_seq\"'::regclass)");

                entity.Property(e => e.Code).HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(100);
            });

            modelBuilder.Entity<MarketingCode>(entity =>
            {
                entity.Property(e => e.Code).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<MarketingPlan>(entity =>
            {
                entity.Property(e => e.GhiChu).HasColumnType("character varying");

                entity.Property(e => e.MaCauHinh).HasColumnType("character varying");

                entity.Property(e => e.MaxValue).HasColumnType("character varying");

                entity.Property(e => e.MoTaNgan).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TieuDe).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");

                entity.Property(e => e.Value).HasColumnType("character varying");
            });

            modelBuilder.Entity<MarketingUser>(entity =>
            {
                entity.Property(e => e.MarketingCode).HasColumnType("character varying");

                entity.Property(e => e.MaxValue).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NoiDung).HasColumnType("character varying");

                entity.Property(e => e.Value).HasColumnType("character varying");
            });

            modelBuilder.Entity<MenuRaoVat>(entity =>
            {
                entity.Property(e => e.GhiChu).HasMaxLength(500);

                entity.Property(e => e.IdMenuParentList).HasMaxLength(80);

                entity.Property(e => e.MoTaTrang).HasMaxLength(1000);

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RewriteUrl).HasMaxLength(200);

                entity.Property(e => e.TenMenu).HasMaxLength(200);

                entity.Property(e => e.TextSort).HasMaxLength(200);

                entity.Property(e => e.TieuDeTrang).HasMaxLength(500);

                entity.Property(e => e.TuKhoaTimKiem).HasMaxLength(500);

                entity.Property(e => e.UrlPrexix).HasMaxLength(50);
            });

            modelBuilder.Entity<NotificationSystem>(entity =>
            {
                entity.Property(e => e.GhiChu).HasColumnType("character varying");

                entity.Property(e => e.Link).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NoiDung).HasColumnType("character varying");

                entity.Property(e => e.TieuDe).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<NotificationTemplate>(entity =>
            {
                entity.Property(e => e.GhiChu).HasColumnType("character varying");

                entity.Property(e => e.Link).HasColumnType("character varying");

                entity.Property(e => e.Ma).HasColumnType("character varying");

                entity.Property(e => e.NoiDung).HasColumnType("character varying");

                entity.Property(e => e.TieuDe).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<NotificationUser>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<StatisticActivitiesUser>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<ThanhVien>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AllowKpiRule).HasColumnType("character varying");

                entity.Property(e => e.DangTinFavorite).HasColumnType("character varying");

                entity.Property(e => e.DangTinLike).HasColumnType("character varying");

                entity.Property(e => e.DiaChi).HasColumnType("character varying");

                entity.Property(e => e.DienThoai).HasColumnType("character varying");

                entity.Property(e => e.Email).HasColumnType("character varying");

                entity.Property(e => e.HoTen).HasColumnType("character varying");

                entity.Property(e => e.IdGroups).HasColumnType("character varying");

                entity.Property(e => e.IdMd5).HasColumnType("character varying");

                entity.Property(e => e.ListHinhAnh).HasColumnType("character varying");

                entity.Property(e => e.ListHinhAnhJsonPassport).HasColumnType("character varying");

                entity.Property(e => e.UidThanhVien).HasColumnType("character varying");
            });

            modelBuilder.Entity<UserBlackList>(entity =>
            {
                entity.Property(e => e.DienThoai).HasColumnType("character varying");

                entity.Property(e => e.GhiChu).HasColumnType("character varying");

                entity.Property(e => e.ListIdDangTin).HasColumnType("character varying");

                entity.Property(e => e.ListImage).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TenThanhVien).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<UserDevice>(entity =>
            {
                entity.Property(e => e.DeviceId).HasColumnType("character varying");

                entity.Property(e => e.DeviceName).HasColumnType("character varying");

                entity.Property(e => e.DynamicKey).HasColumnType("character varying");

                entity.Property(e => e.DynamicValue).HasColumnType("character varying");

                entity.Property(e => e.IpAddress).HasColumnType("character varying");

                entity.Property(e => e.LatitudeX).HasColumnType("character varying");

                entity.Property(e => e.LatitudeY).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OsName).HasColumnType("character varying");

                entity.Property(e => e.UserEmail).HasColumnType("character varying");

                entity.Property(e => e.UserPhone).HasColumnType("character varying");
            });

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Title).HasColumnType("character varying");

                entity.Property(e => e.TuKhoaTimKiem).HasColumnType("character varying");
            });

            modelBuilder.Entity<UserPoint>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"Point_Id_seq\"'::regclass)");

                entity.Property(e => e.MarketingCode).HasColumnType("character varying");

                entity.Property(e => e.MaxValue).HasColumnType("character varying");

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TotalPrice).HasColumnType("numeric");

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .HasColumnType("character varying");

                entity.Property(e => e.Value).HasColumnType("character varying");
            });

            modelBuilder.Entity<UserRank>(entity =>
            {
                entity.Property(e => e.IdAdminCapNhat).ValueGeneratedOnAdd();

                entity.Property(e => e.IdAdminTao).ValueGeneratedOnAdd();

                entity.Property(e => e.MoTaNgan).HasMaxLength(1000);

                entity.Property(e => e.NgayTao).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PercentDiscount).HasColumnType("numeric(4,1)");

                entity.Property(e => e.TieuDe).HasMaxLength(500);

                entity.Property(e => e.TuKhoaTimKiem).HasMaxLength(500);
            });

            modelBuilder.HasSequence("CodeVerified_Id_seq");

            modelBuilder.HasSequence("Point_Id_seq");

            modelBuilder.HasSequence("TinTraPhi_Id_seq");
        }
    }
}
