﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class MarketingUser
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public string MarketingCode { get; set; }
        public DateTime NgayTao { get; set; }
        public string NoiDung { get; set; }
        public string Value { get; set; }
        public int? ValueType { get; set; }
        public string MaxValue { get; set; }
        public long? IdMarketingPlan { get; set; }
        public long? IdMarketingCode { get; set; }
    }
}
