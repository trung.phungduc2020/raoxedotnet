﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserRank
    {
        public long Id { get; set; }
        public string TieuDe { get; set; }
        public string MoTaNgan { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public long IdAdminTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public int? TrangThai { get; set; }
        public decimal? PercentDiscount { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public long? PointRule { get; set; }
    }
}
