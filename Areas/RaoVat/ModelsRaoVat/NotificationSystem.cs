﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class NotificationSystem
    {
        public long Id { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public int? Type { get; set; }
        public DateTime? NgayTao { get; set; }
        public int? TrangThai { get; set; }
        public string Link { get; set; }
        public long? IdThanhVien { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public long? IdAdminTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public long? IdUserGroup { get; set; }
        public string GhiChu { get; set; }
        public DateTime? TuNgay { get; set; }
        public DateTime? DenNgay { get; set; }
    }
}
