﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserGroup
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public int? TrangThai { get; set; }
    }
}
