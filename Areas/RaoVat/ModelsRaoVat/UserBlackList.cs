﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserBlackList
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public string ListIdDangTin { get; set; }
        public string ListImage { get; set; }
        public string GhiChu { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime? NgayTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public DateTime? TuNgay { get; set; }
        public DateTime? DenNgay { get; set; }
        public int? TrangThai { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public string TenThanhVien { get; set; }
        public string DienThoai { get; set; }
    }
}
