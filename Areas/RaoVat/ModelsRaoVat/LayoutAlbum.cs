﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class LayoutAlbum
    {
        public long Id { get; set; }
        public long? IdFileDaiDien { get; set; }
        public int? TongImage { get; set; }
        public int? TrangThai { get; set; }
        public string Ma { get; set; }
    }
}
