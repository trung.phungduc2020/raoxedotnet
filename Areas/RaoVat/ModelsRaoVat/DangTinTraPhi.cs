﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinTraPhi
    {
        public long Id { get; set; }
        public long? IdLoaiTraPhi { get; set; }
        public long? IdDangTin { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? NgayKetThuc { get; set; }
        public int? ThoiGianSuDung { get; set; }
        public long? IdThanhVienTao { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdThanhVienCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public DateTime? NgayKetThucDuDinh { get; set; }
        public int? ThoiGianSuDungDuDinh { get; set; }
        public int? PinStatus { get; set; }
    }
}
