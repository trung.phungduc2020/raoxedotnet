﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinFavorite
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? IdDangTin { get; set; }
        public DateTime? NgayTao { get; set; }
    }
}
