﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserPoint
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? Point { get; set; }
        public long? PointTmp { get; set; }
        public long? PointUsed { get; set; }
        public long? IdAdminTao { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string MarketingCode { get; set; }
        public long? IdMarketingPlan { get; set; }
        public string Value { get; set; }
        public int? ValueType { get; set; }
        public DateTime? NgayDangKyDuyet { get; set; }
        public long? IdAdminDangKyDuyet { get; set; }
        public decimal? TotalPrice { get; set; }
        public string MaxValue { get; set; }
        public long? IdMarketingCode { get; set; }
        public int? CheckoutType { get; set; }
        public string Uuid { get; set; }
        public DateTime? CheckoutDate { get; set; }
    }
}
