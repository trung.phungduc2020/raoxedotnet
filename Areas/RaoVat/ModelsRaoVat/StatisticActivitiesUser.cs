﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class StatisticActivitiesUser
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public int? CountDangTin { get; set; }
        public int? CountUpTin { get; set; }
        public long? IdThanhVienTao { get; set; }
        public DateTime NgayTao { get; set; }
        public long? IdThanhVienCapNhat { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? CountLienHe { get; set; }
    }
}
