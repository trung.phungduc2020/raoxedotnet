﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class DangTinRating
    {
        public long Id { get; set; }
        public long? IdUser { get; set; }
        public long? IdDangTin { get; set; }
        public decimal? Rating { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public string Description { get; set; }
        public string HoTenThanhVien { get; set; }
        public int? TrangThai { get; set; }
        public string TuKhoaTimKiem { get; set; }
        public long? IdFileDaiDien { get; set; }
    }
}
