﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class NotificationUser
    {
        public long Id { get; set; }
        public long? IdThanhVien { get; set; }
        public long? IdNotificationSystem { get; set; }
        public int? TrangThai { get; set; }
        public DateTime NgayTao { get; set; }
        public bool? IsView { get; set; }
    }
}
