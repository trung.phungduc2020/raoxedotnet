﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class UserDevice
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string UserPhone { get; set; }
        public string UserEmail { get; set; }
        public string DeviceName { get; set; }
        public string OsName { get; set; }
        public string IpAddress { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public string DeviceId { get; set; }
        public string LatitudeX { get; set; }
        public string LatitudeY { get; set; }
        public DateTime NgayTao { get; set; }
        public string DynamicKey { get; set; }
        public string DynamicValue { get; set; }
        public bool? IsUninstalled { get; set; }
    }
}
