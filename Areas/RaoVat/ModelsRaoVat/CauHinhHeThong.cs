﻿using System;
using System.Collections.Generic;

namespace raoVatApi.ModelsRaoVat
{
    public partial class CauHinhHeThong
    {
        public long Id { get; set; }
        public string MaCauHinh { get; set; }
        public string GiaTriCauHinh { get; set; }
        public long? IdAdminCapNhat { get; set; }
        public long? IdAdminTao { get; set; }
        public DateTime? NgayCapNhat { get; set; }
        public DateTime? NgayTao { get; set; }
        public string GhiChu { get; set; }
    }
}
