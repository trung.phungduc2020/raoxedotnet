﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class MarketingPlanController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<MarketingPlan> _repo;
        public EntityBaseRepositoryRaoVat<MarketingPlan> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMaster;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMaster { get => _repoMaster == null ? _repo = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMaster; set => _repoMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCode;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCode { get => _repoMarketingCode == null ? _repoMarketingCode = new EntityBaseRepositoryRaoVat<MarketingCode>() : _repoMarketingCode; set => _repoMarketingCode = value; }
        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCodeMaster;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCodeMaster { get => _repoMarketingCodeMaster == null ? _repoMarketingCodeMaster = new EntityBaseRepositoryRaoVat<MarketingCode>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingCodeMaster; set => _repoMarketingCodeMaster = value; }

        public MarketingPlanController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                IEnumerable<MarketingPlan> data = await _adminBll.SearchMarketingPlan(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MarketingPlan entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Create MarketingPlan
                    var returnMarketingPlan = await RepoMaster.InsertReturnEntity(entity);
                    var idMarketingPlan = returnMarketingPlan.FirstOrDefault().Id;

                    MarketingCode marketingCode = new MarketingCode();
                    if (entity.MaCauHinh.ToUpper() == Variables.MACAUHINH_SPECIALDAY.ToUpper())
                    {
                        var randomCode = string.IsNullOrEmpty(entity.Code_Ext) ? CommonMethods.GetRandomCodeSpecialDay() : entity.Code_Ext;
                        var existRandomCode = RepoMarketingCode.Find(a => a.Code.ToUpper() == randomCode.ToUpper()).Result.Select(a=>a.IdMarketingPlan).ToList();
                        var existPlan = Repo.Find(s => existRandomCode.Contains(s.Id) && s.TrangThai == 1
                                    && ((s.UnlimitedTime.HasValue && s.UnlimitedTime.Value) ||
                                    ((!s.TuNgay.HasValue || s.TuNgay.HasValue && s.TuNgay <= DateTime.Now) &&
                                    (!s.DenNgay.HasValue || s.DenNgay.HasValue && DateTime.Now <= s.DenNgay)))).Result.FirstOrDefault();

                        if (existPlan == null)
                        {
                            // Create Code for MarketingPlan
                            marketingCode.IdMarketingPlan = idMarketingPlan;
                            marketingCode.TrangThai = 1;
                            marketingCode.MaxUsed = returnMarketingPlan.FirstOrDefault().MaxUsed;
                            marketingCode.Code = randomCode;

                            await RepoMarketingCodeMaster.Insert(marketingCode);
                        }
                        else
                        {
                            await RepoMaster.Delete(returnMarketingPlan);
                            cusRes.SetException(null, string.Format(Messages.ERR_006, "Code"));
                            return Response.OkObjectResult(cusRes);
                        }
                    }

                    // Update TuKhoaTimKiem for Marketing Plan.
                    var code = string.IsNullOrEmpty(marketingCode.Code) ? string.Empty : marketingCode.Code;
                    var tuKhoaTimKiem = BuildTuKhoaTimKiem(entity, code);
                    await RepoMaster.UpdateWhere(s => s.Id == idMarketingPlan, delegate (MarketingPlan obj)
                    {
                        obj.TuKhoaTimKiem = tuKhoaTimKiem;
                    });

                    InterfaceParam interfaceParam = new InterfaceParam();
                    interfaceParam.lstId = new List<long> { idMarketingPlan };

                    IEnumerable<MarketingPlan> data = await _adminBll.SearchMarketingPlan(interfaceParam);
                    cusRes.DataResult = data;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MarketingPlan newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    MarketingPlan editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        var code = newEntity.MarketingCode_Ext == null ? string.Empty :
                                (string.IsNullOrEmpty(newEntity.MarketingCode_Ext.Code) ? string.Empty : newEntity.MarketingCode_Ext.Code);

                        newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiem(newEntity, code);
                        cusRes.IntResult = await RepoMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]);
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (MarketingPlan obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        private string BuildTuKhoaTimKiem(MarketingPlan data, string Code)
        {
            string res = string.Empty;

            if (data != null)
            {
                res += string.IsNullOrEmpty(data.TieuDe) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.TieuDe + ",");
                res += string.IsNullOrEmpty(data.MoTaNgan) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.MoTaNgan + ",");
                res += string.IsNullOrEmpty(Code) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(Code + ",");                
            }

            return res;
        }
    }
}