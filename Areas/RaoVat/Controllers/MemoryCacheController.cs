﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.Response;
using System;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class MemoryCacheController : Controller
    {
        private IMemoryCache _cache;

        public MemoryCacheController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ClearCache(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();

                switch (interfaceParam.cacheType)
                {
                    // 1: clear all cache
                    case 1:
                        await cacheControllerBase.ClearAllCache();
                        break;
                    // 2: clear by key
                    case 2:
                        await cacheControllerBase.ClearCacheByKeyAndConditions(interfaceParam.cacheKey, true, false);
                        break;

                    default:
                        await cacheControllerBase.ClearAllCache();
                        break;
                }

                cusRes.IntResult = 1;
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

    }
}