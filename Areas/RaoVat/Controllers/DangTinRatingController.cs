﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class DangTinRatingController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<DangTinRating> _repo;
        public EntityBaseRepositoryRaoVat<DangTinRating> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<DangTinRating>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<DangTinRating> _repoDangTinRatingMaster;
        public EntityBaseRepositoryRaoVat<DangTinRating> RepoDangTinRatingMaster { get => _repoDangTinRatingMaster == null ? _repoDangTinRatingMaster = new EntityBaseRepositoryRaoVat<DangTinRating>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinRatingMaster; set => _repoDangTinRatingMaster = value; }


        public DangTinRatingController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Admin: not cache
                interfaceParam.isFromAdmin = true;

                // Search & Get data
                IEnumerable<DangTinRating> data = await _adminBll.SearchDangTinRating(interfaceParam);
                cusRes.DataResult = data;

                // 
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] DangTinRating newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    DangTinRating editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin tức này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        cusRes.IntResult = await RepoDangTinRatingMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoDangTinRatingMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (DangTinRating obj)
                    {
                        obj.TrangThai = trangThai;
                    });

                    // id of dangtinrating
                    foreach (var id in lstId)
                    {
                        var GetRating = Repo.Find(a => a.Id == id).Result.FirstOrDefault();

                        if(GetRating != null)
                        {
                            // Update Rating in DangTin
                            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                            await commonMethodsRaoVat.UpdateRatingInDangTin(GetRating.IdDangTin ?? 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}
