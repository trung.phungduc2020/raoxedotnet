﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;
using InterfaceBLL = raoVatApi.ModelsRaoVat.InterfaceBLL;

namespace raoVatApi.ControllersRaoVat
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route(Variables.C_ApiVersioning_InterfaceData_2)]
    public class InterfaceDataController : Controller
    {
        private ApiGetDal _apiGetDal;
        public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        #region History

        private EntityBaseRepositoryHistoryRaoVat<HistoryPoint> _repoHistoryPoint;
        public EntityBaseRepositoryHistoryRaoVat<HistoryPoint> RepoHistoryPoint { get => _repoHistoryPoint == null ? _repoHistoryPoint = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>() : _repoHistoryPoint; set => _repoHistoryPoint = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike> _repoHistoryDangTinLike;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike> RepoHistoryDangTinLike { get => _repoHistoryDangTinLike == null ? _repoHistoryDangTinLike = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike>() : _repoHistoryDangTinLike; set => _repoHistoryDangTinLike = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite> _repoHistoryDangTinFavorite;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite> RepoHistoryDangTinFavorite { get => _repoHistoryDangTinFavorite == null ? _repoHistoryDangTinFavorite = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite>() : _repoHistoryDangTinFavorite; set => _repoHistoryDangTinFavorite = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> _repoHistoryDangTinView;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> RepoHistoryDangTinView { get => _repoHistoryDangTinView == null ? _repoHistoryDangTinView = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView>() : _repoHistoryDangTinView; set => _repoHistoryDangTinView = value; }

        #endregion

        private EntityBaseRepositoryRaoVat<InstallationStatistics> _repoInstallationStatistics;
        public EntityBaseRepositoryRaoVat<InstallationStatistics> RepoInstallationStatistics { get => _repoInstallationStatistics == null ? _repoInstallationStatistics = new EntityBaseRepositoryRaoVat<InstallationStatistics>() : _repoInstallationStatistics; set => _repoInstallationStatistics = value; }
        private EntityBaseRepositoryRaoVat<InstallationStatistics> _repoInstallationStatisticsMaster;
        public EntityBaseRepositoryRaoVat<InstallationStatistics> RepoInstallationStatisticsMaster { get => _repoInstallationStatisticsMaster == null ? _repoInstallationStatisticsMaster = new EntityBaseRepositoryRaoVat<InstallationStatistics>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoInstallationStatisticsMaster; set => _repoInstallationStatisticsMaster = value; }

        private EntityBaseRepositoryRaoVat<UserDevice> _repoUserDevice;
        public EntityBaseRepositoryRaoVat<UserDevice> RepoUserDevice { get => _repoUserDevice == null ? _repoUserDevice = new EntityBaseRepositoryRaoVat<UserDevice>() : _repoUserDevice; set => _repoUserDevice = value; }
        private EntityBaseRepositoryRaoVat<UserDevice> _repoUserDeviceMaster;
        public EntityBaseRepositoryRaoVat<UserDevice> RepoUserDeviceMaster { get => _repoUserDeviceMaster == null ? _repoUserDeviceMaster = new EntityBaseRepositoryRaoVat<UserDevice>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserDeviceMaster; set => _repoUserDeviceMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTinFavorite> _repoDangTinFavorite;
        public EntityBaseRepositoryRaoVat<DangTinFavorite> RepoDangTinFavorite { get => _repoDangTinFavorite == null ? _repoDangTinFavorite = new EntityBaseRepositoryRaoVat<DangTinFavorite>() : _repoDangTinFavorite; set => _repoDangTinFavorite = value; }
        private EntityBaseRepositoryRaoVat<DangTinFavorite> _repoDangTinFavoriteMaster;
        public EntityBaseRepositoryRaoVat<DangTinFavorite> RepoDangTinFavoriteMaster { get => _repoDangTinFavoriteMaster == null ? _repoDangTinFavoriteMaster = new EntityBaseRepositoryRaoVat<DangTinFavorite>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinFavoriteMaster; set => _repoDangTinFavoriteMaster = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>() : _repoThanhVien; set => _repoThanhVien = value; }
        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinRepositoryRaoVat;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinRepositoryRaoVat { get => _repoDangTinRepositoryRaoVat == null ? _repoDangTinRepositoryRaoVat = new EntityBaseRepositoryRaoVat<DangTin>(Request) : _repoDangTinRepositoryRaoVat; set => _repoDangTinRepositoryRaoVat = value; }
        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinRepositoryRaoVatMaster;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinRepositoryRaoVatMaster { get => _repoDangTinRepositoryRaoVatMaster == null ? _repoDangTinRepositoryRaoVatMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinRepositoryRaoVatMaster; set => _repoDangTinRepositoryRaoVatMaster = value; }


        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMarketingPlan;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMarketingPlan { get => _repoMarketingPlan == null ? _repoMarketingPlan = new EntityBaseRepositoryRaoVat<MarketingPlan>() : _repoMarketingPlan; set => _repoMarketingPlan = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUser;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUser { get => _repoStatisticActivitiesUser == null ? _repoStatisticActivitiesUser = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request) : _repoStatisticActivitiesUser; set => _repoStatisticActivitiesUser = value; }
        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUserMaster;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUserMaster { get => _repoStatisticActivitiesUserMaster == null ? _repoStatisticActivitiesUserMaster = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoStatisticActivitiesUserMaster; set => _repoStatisticActivitiesUserMaster = value; }

        private InterfaceBLL _interfaceBLL;
        private IDistributedCache _cache;
        private AdminBll _adminBll;

        public InterfaceDataController(IDistributedCache cache)
        {
            this._cache = cache;
            this._adminBll = new AdminBll();
            this._interfaceBLL = new InterfaceBLL(Request);
        }
        // [Cached]
        [HttpGet]
        public async Task<IActionResult> SearchMenuRaoVat([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                   CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notId) ? CommonMethods.SplitStringToInt64(interfaceParam.notId) : null;
                interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.lstCapDo = CommonMethods.SplitStringToInt(interfaceParam.capDo, true);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notId) ? CommonMethods.SplitStringToInt64(interfaceParam.notId) : null;

                // Search & Get data
                var tuple = await _interfaceBLL.SearchMenuRaoVat(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        // [Cached]
        [HttpGet]
        public async Task<IActionResult> SearchDangTin([FromQuery]InterfaceParam interfaceParam)
        {
            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin SearchDangTin");
            //CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start");
            CustomResult cusRes = new CustomResult();
            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) :
                     CommonMethods.SplitStringToInt64(interfaceParam.notId);

                // Handle param
                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 20;
                }
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }

                // Active: Homepage, details
                //CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start Request.GetTokenIdThanhVien()");
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Search & Get data");
                // Search & Get data
                //CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start await _interfaceBLL.SearchDangTin()");
                var tuple = await _interfaceBLL.SearchDangTin(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;

                //IEnumerable<DangTin> data = await _interfaceBLL.SearchDangTin(interfaceParam);
                // nếu search chỉ có 1 id thì update thì update số lượt xem của id đó                
                if (interfaceParam.lstId != null && interfaceParam.lstId.Count == 1 && data != null && data.Count() > 0)
                {
                    CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start Update history");
                    //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Update history SoLuotXem");
                    // Update history SoLuotXem
                    HistoryDangTinView historyDangTinView = new HistoryDangTinView();
                    historyDangTinView.IdDangTin = interfaceParam.lstId.FirstOrDefault();
                    historyDangTinView.IdThanhVien = Request.GetTokenIdThanhVien();
                    historyDangTinView.DeviceId = interfaceParam.deviceId;
                    historyDangTinView.Type = 1;
                    RepoHistoryDangTinView.InsertWithouClearCache(historyDangTinView);

                    RepoDangTinRepositoryRaoVatMaster.UpdateWhereWithoutClearCache(s => interfaceParam.lstId.Contains(s.Id), delegate (DangTin obj)
                    {
                        obj.SoLuotXem = obj.SoLuotXem.GetValueOrDefault(0) + 1;
                    });
                }
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Set DataResult");
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-End SearchDangTin");
                //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Get property name list");
                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Exception: KillConnection", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                cusRes.SetException(ex);
            }
            //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin End");
            //CommonMethods.WriteLogPerformanceTest($"SearchDangTin End Response.OkObjectResult");
            return Response.OkObjectResult(cusRes);
        }

        // [Cached] => To build sitemap
        [HttpGet]
        public async Task<IActionResult> SearchDangTinBuildSitemap([FromQuery]InterfaceParam interfaceParam)
        {
            CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start");
            CustomResult cusRes = new CustomResult();
            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) :
                     CommonMethods.SplitStringToInt64(interfaceParam.notId);

                //// Handle param
                //if (interfaceParam.displayItems <= 0)
                //{
                //    interfaceParam.displayItems = 20;
                //}
                //if (interfaceParam.displayPage <= 0)
                //{
                //    interfaceParam.displayPage = 1;
                //}

                // Active: Homepage, details
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                // Search & Get data
                var tuple = await _interfaceBLL.SearchDangTinBuildSitemap(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> SearchDangTinTestHasura([FromQuery]InterfaceParam interfaceParam)
        {
            CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start");
            CustomResult cusRes = new CustomResult();
            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) :
                     CommonMethods.SplitStringToInt64(interfaceParam.notId);

                // Handle param
                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 1000;
                }
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }

                // Active: Homepage, details
                CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start Request.GetTokenIdThanhVien()");
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Search & Get data");
                // Search & Get data
                CommonMethods.WriteLogPerformanceTest($"SearchDangTin Start await _interfaceBLL.SearchDangTin()");
                var tuple = await _interfaceBLL.SearchDangTinTestHasura(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;

                //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Set DataResult");
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Get property name list");
                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            //Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin End");
            CommonMethods.WriteLogPerformanceTest($"SearchDangTin End Response.OkObjectResult");
            return Response.OkObjectResult(cusRes);
        }

        // [Cached]
        [HttpGet]
        public async Task<IActionResult> UpdateDangTin_SoLuotXem()
        {
            CustomResult cusRes = new CustomResult();
            string idsString = "";
            int soLuotXem = 0;
            IQueryCollection queryParams = Request.Query;
            if (queryParams != null)
            {
                idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]);
                if (string.IsNullOrEmpty(idsString))
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                }

                var paramSoLuotXem = CommonMethods.ConvertToInt32(queryParams[CommonParams.soLuotXem].ToString());
                soLuotXem = paramSoLuotXem > 0 ? paramSoLuotXem : 1;
            }
            try
            {
                List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                EntityBaseRepositoryRaoVat<DangTin> repo = new EntityBaseRepositoryRaoVat<DangTin>(Request);
                cusRes.IntResult = await repo.UpdateWhereWithoutClearCache(s => (lstId != null && lstId.Contains(s.Id)), delegate (DangTin obj)
                {
                    obj.SoLuotXem = obj.SoLuotXem.GetValueOrDefault(0) + soLuotXem;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            // TODO: check condition before insert into DB
            return Response.OkObjectResult(cusRes);
        }

        //[HttpGet]
        //public async Task<IActionResult> SearchUserDevice([FromQuery]InterfaceParam interfaceParam)
        //{
        //    CustomResult cusRes = new CustomResult();
        //    try
        //    {
        //        if (interfaceParam.displayItems <= 0)
        //        {
        //            interfaceParam.displayItems = 20;
        //        }
        //        if (interfaceParam.displayPage <= 0)
        //        {
        //            interfaceParam.displayPage = 1;
        //        }

        //        // Handle param
        //        interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
        //            CommonMethods.SplitStringToInt64(interfaceParam.id);

        //        // Search & Get data
        //        IEnumerable<UserDevice> data = await _interfaceBLL.SearchUserDevice(interfaceParam);
        //        cusRes.DataResult = data;

        //        // 
        //        int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
        //        cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
        //    }
        //    catch (Exception ex)
        //    {
        //        cusRes.SetException(ex);
        //    }
        //    return Response.OkObjectResult(cusRes);
        //}

        #region "DangTinRating"

        // [Cached]
        [HttpGet]
        public async Task<IActionResult> SearchDangTinRating([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }

                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                var tuple = await _interfaceBLL.SearchDangTinRating(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                // Pagination
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "CauHinhHeThong"

        // [No Cache]
        [HttpGet]
        public async Task<IActionResult> SearchCauHinhHeThong([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                IEnumerable<CauHinhHeThong> data = await _interfaceBLL.SearchCauHinhHeThong(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(1, 1, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "NotificationSystem"

        // [Cached] => need improvement
        [HttpGet]
        public async Task<IActionResult> SearchNotificationSystem([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.allIncluding = CommonMethods.ConvertToBoolean(interfaceParam.allIncludingString);

                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                // Search & Get data
                var tuple = await _interfaceBLL.SearchNotificationMemberHasCache(interfaceParam);
                var data = tuple.Item1;
                var keyCache = tuple.Item2;

                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                int totalRows = data != null ? CommonMethods.ConvertToInt32(data.FirstOrDefault().GetValueFromKey("TotalRow", "0")) : 0;
                // Get property name list
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "SearchUserBlacklist"

        // [No Cache]
        [HttpGet]
        public async Task<IActionResult> SearchUserBlacklist([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Search & Get data
                IEnumerable<UserBlackList> data = await _interfaceBLL.SearchUserBlacklist(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "InstallationStatistics"

        [HttpPost]
        public async Task<IActionResult> PostInstallationStatistics([FromBody]InstallationStatistics entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Get UserDevice and Check exist Id
                    UserDevice userDevice = (await RepoUserDevice.Find(n => n.DeviceId.ToUpper() == entity.UUID_Ext.ToUpper())).FirstOrDefault();

                    if (userDevice == null)
                    {
                        // Insert UserDevice first
                        userDevice = new UserDevice();
                        userDevice.DeviceName = entity.DeviceName_Ext;
                        userDevice.OsName = entity.OsName_Ext;
                        userDevice.IpAddress = entity.IpAddress_Ext;
                        userDevice.LatitudeX = entity.LatitudeX_Ext;
                        userDevice.LatitudeY = entity.LatitudeY_Ext;
                        await RepoUserDeviceMaster.Insert(userDevice);

                        // tìm trong db xem đã tồn tại chưa, nếu ròi không cần phải insert
                        InstallationStatistics existEntity = RepoInstallationStatistics.Find(n => n.OsName.ToUpper() == entity.OsName.ToUpper()
                                                    && n.Version.ToUpper() == entity.Version.ToUpper()).Result.FirstOrDefault();
                        if (existEntity == null)
                        {
                            entity.Installation = 1;
                            cusRes.IntResult = await RepoInstallationStatisticsMaster.Insert(entity);
                        }
                        else
                        {
                            existEntity.Installation = existEntity.Installation + 1;
                            cusRes.IntResult = await RepoInstallationStatisticsMaster.UpdateAfterAutoClone(existEntity);
                        }
                    }
                    else // Insert New version
                    {
                        // tìm trong db xem đã tồn tại Version chưa, nếu ròi không cần phải insert
                        InstallationStatistics existEntity = RepoInstallationStatistics.Find(n => n.OsName.ToUpper() == entity.OsName.ToUpper()
                                                    && n.Version.ToUpper() == entity.Version.ToUpper()).Result.FirstOrDefault();
                        if (existEntity == null)
                        {
                            entity.Installation = 1;
                            cusRes.IntResult = await RepoInstallationStatisticsMaster.Insert(entity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "GetServerTime"

        // [No Cache]
        [HttpGet]
        public DateTime GetServerTime()
        {
            var token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6Ik5ndXnhu4VuIFRy4buNbmcgSOG7r3UiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJkbSIsIklkIjoiMTYiLCJVaWRUaGFuaFZpZW4iOiIiLCJJc0FkbWluIjoiVHJ1ZSIsIklzVGhhbmhWaWVuIjoiRmFsc2UiLCJJZE5ob21RdXllbiI6IjEiLCJJc01haW4iOiJUcnVlIiwibmJmIjoxNjIwNzA4MjM4LCJleHAiOjE2MjMzMDAyMzgsImlzcyI6IkPDtG5nIHR5IFROSEggZ2lhbiBow6BuZyB0cuG7sWMgdHV54bq_biBWTiIsImF1ZCI6ImRhaWx5eGUuY29tLnZuIn0.HUSyICXsf7pJhgtH3NXQSCK40EeZb-825O63zKLH-2Y";
            List<string> lstImage = new List<string>();
            lstImage.Add("https://firebasestorage.googleapis.com/v0/b/dailyxe-ionic-raovat.appspot.com/o/ImageStorage%2Fban-chevrolet-blazer-2019-2019_i85357_tv62-CAF825.jpg?alt=media&token=440b58f2-dc28-4c26-9feb-ed6c08bcba18");
            lstImage.Add("https://cdn.dailyxe.com.vn/image/ban-kia-seltos-2021-145537j.jpg");
            lstImage.Add("https://cdn.dailyxe.com.vn/image/ban-kia-seltos-155537j.jpg");
            lstImage.Add("https://firebasestorage.googleapis.com/v0/b/dailyxe-ionic-raovat.appspot.com/o/ImageStorage%2Fban-chevrolet-blazer-2019-2019_i85357_tv62-CAF825.jpg?alt=media&token=440b58f2-dc28-4c26-9feb-ed6c08bcba18");
            lstImage.Add("https://firebasestorage.googleapis.com/v0/b/dailyxe-ionic-raovat.appspot.com/o/ImageStorage%2Fban-chevrolet-blazer-2019-2019_i85357_tv62-CAF825.jpg?alt=media&token=440b58f2-dc28-4c26-9feb-ed6c08bcba19");

            var infoImages = ApiGetDal.InsertListUrlHinhAnh("aaaaa", lstImage, token);

            // nguyencuongcs 20200309
            return DateTime.UtcNow.AddHours(7);
            //return DateTime.Now;
        }

        #endregion

        #region "SearchLayoutAlbum"

        // [Cached]
        [HttpGet]
        public async Task<IActionResult> SearchLayoutAlbum([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Search & Get data
                var tuple = await _interfaceBLL.SearchLayoutAlbum(interfaceParam);
                var data = tuple.Item1;
                string keyCache = tuple.Item2;
                cusRes.StrResult = keyCache;
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "PostHistoryDangTinFavorite"

        //[HttpPost]
        //public async Task<IActionResult> PostHistoryDangTinFavorite([FromBody]HistoryDangTinFavorite entity)
        //{
        //    CustomResult cusRes = new CustomResult();
        //    try
        //    {
        //        if (entity == null)
        //        {
        //            cusRes.SetException(null, Messages.ERR_002);
        //            return Response.OkObjectResult(cusRes);
        //        }
        //        else
        //        {
        //            long idThanhVien = Request.GetTokenIdThanhVien();
        //            entity.TrangThai = 2;
        //            if (idThanhVien > 0)
        //            {
        //                // User login => Visible to user.
        //                entity.TrangThai = 1;

        //                // Handle DangTin First
        //                DangTin existDangTin = (await RepoDangTinRepositoryRaoVat.Find(n => n.Id == entity.IdDangTin)).FirstOrDefault();
        //                var lstThanhVienFavorite = existDangTin != null && !string.IsNullOrEmpty(existDangTin.ThanhVienFavorite) ? existDangTin.ThanhVienFavorite.Split(",").ToList() : new List<string>();

        //                // Handle ThanhVien
        //                ThanhVien existThanhVien = (await RepoThanhVien.Find(n => n.Id == idThanhVien)).FirstOrDefault();
        //                var lstDangTinFavorite = existThanhVien != null && !string.IsNullOrEmpty(existThanhVien.DangTinLike) ? existThanhVien.DangTinLike.Split(",").ToList() : new List<string>();

        //                if (entity.IsFavorite.HasValue && entity.IsFavorite.Value)
        //                {
        //                    if (!lstThanhVienFavorite.Contains(idThanhVien.ToString()))
        //                    {
        //                        lstThanhVienFavorite.Insert(0, idThanhVien.ToString());
        //                        existDangTin.ThanhVienFavoriteCount = (existDangTin.ThanhVienFavoriteCount ?? 0) + 1;
        //                    }

        //                    if (!lstDangTinFavorite.Contains(entity.IdDangTin.ToString()))
        //                    {
        //                        lstDangTinFavorite.Insert(0, entity.IdDangTin.ToString());
        //                        existThanhVien.DangTinFavoriteCount = (existThanhVien.DangTinFavoriteCount ?? 0) + 1;
        //                    }
        //                }
        //                else
        //                {
        //                    if (lstThanhVienFavorite.Contains(idThanhVien.ToString()))
        //                    {
        //                        lstThanhVienFavorite.Remove(idThanhVien.ToString());
        //                        existDangTin.ThanhVienFavoriteCount = (existDangTin.ThanhVienFavoriteCount ?? 0) - 1;
        //                    }

        //                    if (lstDangTinFavorite.Contains(entity.IdDangTin.ToString()))
        //                    {
        //                        lstDangTinFavorite.Remove(entity.IdDangTin.ToString());
        //                        existThanhVien.DangTinFavoriteCount = (existThanhVien.DangTinFavoriteCount ?? 0) - 1;
        //                    }
        //                }

        //                if (lstThanhVienFavorite.Count > Variables.CountLikeAndFavorite)
        //                {
        //                    lstThanhVienFavorite = lstThanhVienFavorite.Take(Variables.CountLikeAndFavorite).ToList();
        //                }
        //                existDangTin.ThanhVienFavorite = lstThanhVienFavorite.Count > 0 ? string.Join(",", lstThanhVienFavorite) : string.Empty;
        //                await RepoDangTinRepositoryRaoVatMaster.UpdateAfterAutoCloneWithoutClearCache(existDangTin);

        //                if (lstDangTinFavorite.Count > Variables.CountLikeAndFavorite)
        //                {
        //                    lstDangTinFavorite = lstDangTinFavorite.Take(Variables.CountLikeAndFavorite).ToList();
        //                }
        //                existThanhVien.DangTinFavorite = lstDangTinFavorite.Count > 0 ? string.Join(",", lstDangTinFavorite) : string.Empty;
        //                await RepoThanhVienMaster.UpdateAfterAutoCloneWithoutClearCache(existThanhVien);

        //                // Insert DangTinFavorite, tìm trong db xem đã tồn tại chưa, nếu ròi không cần phải insert
        //                DangTinFavorite entityDangTinFavorite = RepoDangTinFavorite.Find(n => n.IdThanhVien == idThanhVien && n.IdDangTin == entity.IdDangTin).Result.FirstOrDefault();
        //                if (entity.IsFavorite.HasValue && entity.IsFavorite.Value)
        //                {
        //                    if (entityDangTinFavorite == null)
        //                    {
        //                        entityDangTinFavorite = new DangTinFavorite();
        //                        entityDangTinFavorite.IdDangTin = entity.IdDangTin;
        //                        entityDangTinFavorite.IdThanhVien = idThanhVien;
        //                        await RepoDangTinFavoriteMaster.InsertReturnEntity(entityDangTinFavorite);
        //                    }
        //                }
        //                else
        //                {
        //                    if (entityDangTinFavorite != null)
        //                    {
        //                        await RepoDangTinFavoriteMaster.Delete(entityDangTinFavorite);
        //                    }
        //                }
        //            }

        //            // Insert HistoryDangTinLike
        //            entity.IdThanhVien = idThanhVien;
        //            cusRes.DataResult = await RepoHistoryDangTinFavorite.InsertReturnEntity(entity);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        cusRes.SetException(ex);
        //    }
        //    return Response.OkObjectResult(cusRes);
        //}


        [HttpPost]
        public async Task<IActionResult> PostHistoryDangTinFavorite([FromBody]HistoryDangTinFavorite entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();
                    entity.TrangThai = 2;
                    if (idThanhVien > 0)
                    {
                        // User login => Visible to user.
                        entity.TrangThai = 1;

                        // Insert DangTinFavorite, tìm trong db xem đã tồn tại chưa, nếu ròi không cần phải insert
                        DangTinFavorite entityDangTinFavorite = RepoDangTinFavorite.Find(n => n.IdThanhVien == idThanhVien && n.IdDangTin == entity.IdDangTin).Result.FirstOrDefault();
                        if (entity.IsFavorite.HasValue && entity.IsFavorite.Value)
                        {
                            if (entityDangTinFavorite == null)
                            {
                                entityDangTinFavorite = new DangTinFavorite();
                                entityDangTinFavorite.IdDangTin = entity.IdDangTin;
                                entityDangTinFavorite.IdThanhVien = idThanhVien;
                                await RepoDangTinFavoriteMaster.InsertReturnEntity(entityDangTinFavorite);
                            }
                        }
                        else
                        {
                            if (entityDangTinFavorite != null)
                            {
                                await RepoDangTinFavoriteMaster.Delete(entityDangTinFavorite);
                            }
                        }

                        var dangTinFavByIdThanhVien = RepoDangTinFavorite.Find(n => n.IdThanhVien == idThanhVien).Result.ToList();
                        var strFavByIdThanhVien = dangTinFavByIdThanhVien.Count > 0 ? string.Join(",", dangTinFavByIdThanhVien.Select(a => a.IdDangTin).ToList()) : string.Empty;
                        await RepoThanhVienMaster.UpdateWhereWithoutClearCache(n => n.Id == idThanhVien, delegate (ThanhVien obj)
                        {
                            obj.DangTinFavoriteCount = dangTinFavByIdThanhVien.Count();
                            obj.DangTinFavorite = strFavByIdThanhVien;
                        });

                        var dangTinFavByIdDangTin = RepoDangTinFavorite.Find(n => n.IdDangTin == entity.IdDangTin).Result.ToList();
                        var strFavByIdDangTin = string.Join(",", dangTinFavByIdDangTin.Select(a => a.IdThanhVien).ToList());
                        await RepoDangTinRepositoryRaoVatMaster.UpdateWhereWithoutClearCache(n => n.Id == entity.IdDangTin, delegate (DangTin obj)
                        {
                            obj.ThanhVienFavoriteCount = dangTinFavByIdDangTin.Count();
                            obj.ThanhVienFavorite = strFavByIdDangTin;
                        });
                    }

                    // Insert HistoryDangTinLike
                    entity.IdThanhVien = idThanhVien;
                    cusRes.DataResult = await RepoHistoryDangTinFavorite.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
        #endregion

        #region "PostDangTinLike"

        [HttpPost]
        public async Task<IActionResult> PostHistoryDangTinLike([FromBody]HistoryDangTinLike entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();

                    // not login
                    entity.TrangThai = 2;
                    if (idThanhVien > 0)
                    {
                        // User login => Visible to user.
                        entity.TrangThai = 1;

                        // Handle DangTin First
                        DangTin existDangTin = (await RepoDangTinRepositoryRaoVat.Find(n => n.Id == entity.IdDangTin)).FirstOrDefault();
                        var lstThanhVienLike = existDangTin != null && !string.IsNullOrEmpty(existDangTin.ThanhVienLike) ? existDangTin.ThanhVienLike.Split(",").ToList() : new List<string>();

                        // Handle ThanhVien
                        ThanhVien existThanhVien = (await RepoThanhVien.Find(n => n.Id == idThanhVien)).FirstOrDefault();
                        var lstDangTinLike = existThanhVien != null && !string.IsNullOrEmpty(existThanhVien.DangTinLike) ? existThanhVien.DangTinLike.Split(",").ToList() : new List<string>();

                        if (entity.IsLike.HasValue && entity.IsLike.Value)
                        {
                            if (!lstThanhVienLike.Contains(idThanhVien.ToString()))
                            {
                                lstThanhVienLike.Insert(0, idThanhVien.ToString());
                                existDangTin.ThanhVienLikeCount = (existDangTin.ThanhVienLikeCount ?? 0) + 1;
                            }

                            if (!lstDangTinLike.Contains(entity.IdDangTin.ToString()))
                            {
                                lstDangTinLike.Insert(0, entity.IdDangTin.ToString());
                                existThanhVien.DangTinLikeCount = (existThanhVien.DangTinLikeCount ?? 0) + 1;
                            }
                        }
                        else
                        {
                            if (lstThanhVienLike.Contains(idThanhVien.ToString()))
                            {
                                lstThanhVienLike.Remove(idThanhVien.ToString());
                                existDangTin.ThanhVienLikeCount = (existDangTin.ThanhVienLikeCount ?? 0) - 1;
                            }

                            if (lstDangTinLike.Contains(entity.IdDangTin.ToString()))
                            {
                                lstDangTinLike.Remove(entity.IdDangTin.ToString());
                                existThanhVien.DangTinLikeCount = (existThanhVien.DangTinLikeCount ?? 0) - 1;
                            }
                        }

                        if (lstThanhVienLike.Count > Variables.CountLikeAndFavorite)
                        {
                            lstThanhVienLike = lstThanhVienLike.Take(100).ToList();
                        }
                        existDangTin.ThanhVienLike = string.Join(",", lstThanhVienLike);
                        await RepoDangTinRepositoryRaoVatMaster.UpdateAfterAutoCloneWithoutClearCache(existDangTin);

                        if (lstDangTinLike.Count > Variables.CountLikeAndFavorite)
                        {
                            lstDangTinLike = lstDangTinLike.Take(100).ToList();
                        }
                        existThanhVien.DangTinLike = string.Join(",", lstDangTinLike);
                        await RepoThanhVienMaster.UpdateAfterAutoCloneWithoutClearCache(existThanhVien);
                    }

                    // Insert HistoryDangTinLike
                    entity.IdThanhVien = idThanhVien;
                    cusRes.DataResult = await RepoHistoryDangTinLike.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "MarketingPlan"

        [HttpPost]
        public async Task<IActionResult> MarketingProcess([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                CommonMethods.WriteLogMarketingPlan("Start MarketingProcess:" + interfaceParam.linkId);
                if (!string.IsNullOrEmpty(interfaceParam.linkId))
                {
                    // Split string
                    var strParam = interfaceParam.linkId.Substring(interfaceParam.linkId.LastIndexOf("?") + 1);
                    var lstParam = strParam.Split("&").ToList();
                    CommonMethods.WriteLogMarketingPlan("strParam 1:" + strParam);
                    // AppShare=XXXXXXX&Key1=YYYYY
                    // value is Md5 of IdThanhVien
                    var splitParam = lstParam[0].Split("=");
                    if (splitParam.Count() > 0)
                    {
                        var key = splitParam[0].ToUpper();
                        var value = splitParam[1].ToUpper();
                        CommonMethods.WriteLogMarketingPlan("key 1:" + key + " --- value 1:" + value);
                        // Get Marketing Plan => Get Point to plus
                        //MarketingPlan marketingPlan = RepoMarketingPlan.Find(a => a.MaCauHinh.ToUpper() == key).Result.FirstOrDefault();

                        var listMarketingPlan = RepoMarketingPlan.Find(a => a.MaCauHinh.ToUpper() == key).Result;
                        var marketingPlan = listMarketingPlan.FirstOrDefault();

                        // Plus Point to Member that share link.
                        ThanhVien thanhVien = RepoThanhVien.Find(a => a.IdMd5.ToUpper() == value).Result.FirstOrDefault();
                        if (thanhVien != null)
                        {
                            CommonMethods.WriteLogMarketingPlan("thanhVien:" + thanhVien.Id);
                            // Insert New Device into UserDevice.
                            UserDevice userDevice = RepoUserDevice.Find(a => a.DeviceId == interfaceParam.deviceId).Result.FirstOrDefault();
                            if (userDevice == null)
                            {
                                var point = CommonMethods.ConvertToInt64(marketingPlan.Value);
                                // New device => need to insert
                                userDevice = new UserDevice();
                                userDevice.DeviceId = interfaceParam.deviceId;
                                userDevice.DynamicKey = key;
                                userDevice.DynamicValue = value;
                                await RepoUserDeviceMaster.InsertReturnEntity(userDevice);

                                // Then gift point
                                await RepoUserPointMaster.UpdateWhere(n => n.IdThanhVien == thanhVien.Id, delegate (UserPoint obj)
                                {
                                    if (marketingPlan.Type == 1)
                                    {
                                        obj.Point = obj.Point + point;
                                    }
                                });

                                string pointRateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_VND);
                                int pointRate = CommonMethods.ConvertToInt32(pointRateString);

                                // Insert History Point To member share link
                                HistoryPoint htp = new HistoryPoint()
                                {
                                    IdThanhVien = thanhVien.Id,
                                    Point = point,
                                    Type = (int)Variables.TypeUserPoint.GiftAppMarketing,
                                    Rate = pointRate,
                                    NgayTao = DateTime.Now,
                                    Note = key
                                };
                                await RepoHistoryPoint.Insert(htp);

                                // Insert Notification
                                InterfaceParam paramNotify = new InterfaceParam();
                                paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                                paramNotify.idThanhVien = thanhVien.Id;
                                paramNotify.ma = Variables.Constants.F007; // Install App
                                paramNotify.noiDung = point.ToString();

                                CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                                var noiDungNotify = await commonController.InsertNotification(paramNotify);

                                foreach (var item in listMarketingPlan)
                                {
                                    item.IdThanhVien_Ext = thanhVien.Id;
                                    item.GhiChu = noiDungNotify;
                                }

                                CommonMethods.WriteLogMarketingPlan("Successful");
                                cusRes.DataResult = listMarketingPlan;
                            }
                            else
                            {
                                CommonMethods.WriteLogMarketingPlan("App đã được cài trên thiết bị này");
                                cusRes.IntResult = -1;
                                cusRes.StrResult = "App đã được cài trên thiết bị này";
                            }
                        }
                        else
                        {
                            CommonMethods.WriteLogMarketingPlan("Thành viên không tồn tại");
                            cusRes.IntResult = -1;
                            cusRes.StrResult = "Thành viên không tồn tại";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            CommonMethods.WriteLogMarketingPlan("End MarketingProcess:" + interfaceParam.linkId);
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> SyncMd5ThanhVien([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    await RepoThanhVienMaster.UpdateWhere(s => 1 == 1, delegate (ThanhVien obj)
                    {
                        obj.IdMd5 = CommonMethods.GenerateIdMd5(obj.Id);
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "GetLastLogin"

        [HttpGet]
        public async Task<IActionResult> GetLastLogin([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (string.IsNullOrEmpty(interfaceParam.idsThanhVien))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                // Search & Get data
                IEnumerable<UserDevice> data = _interfaceBLL.GetLastLogin(interfaceParam);
                cusRes.DataResult = data;

                int totalRows = data != null ? data.Count() : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateMoTaNgan([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => s.TrangThai == 1 && s.IsDuyet == true && !string.IsNullOrEmpty(s.MoTaNgan), delegate (DangTin obj)
                    {
                        obj.MoTaNgan = CommonMethods.SmartSubstr(obj.MoTaNgan, Variables.MaxLengthShortDescription);
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTenDongXe([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                cusRes.DataResult = RepoDangTinRepositoryRaoVat.Find(s => s.TrangThai == 1 && string.IsNullOrEmpty(s.TenDongXe)).Result;

                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => s.TrangThai == 1 && string.IsNullOrEmpty(s.TenDongXe), async delegate (DangTin obj)
                    {
                        var Prefix = obj.Type == 1 ? "Bán " : "Mua ";

                        InterfaceParam paramCallDongXe = new InterfaceParam();
                        paramCallDongXe.ids = obj.IdDongXe.ToString();

                        // Update TenDongXe: xxx
                        obj.TenDongXe = await ApiGetDal.SearchDongXe(paramCallDongXe);

                        // Update TieuDe: Ban xxx
                        obj.TieuDe = Prefix + obj.TenDongXe;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

        public async Task<IActionResult> IsExistDienThoaiThanhVienSelfCheck([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult customResult = await ApiGetDal.IsExistDienThoaiThanhVienSelfCheck(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        #region Update Default Data

        [HttpPost]
        public async Task<IActionResult> CalcStatisticActiviesUser([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    var listThanhVien = _interfaceBLL.GetAllThanhVien();
                    var year = interfaceParam.year;
                    var month = interfaceParam.month;
                    //for (var month = 1; month <= 12; month++)
                    //{
                    foreach (var thanhVien in listThanhVien)
                    {
                        var countDangTin = RepoDangTinRepositoryRaoVat.Find(a => a.IdThanhVien == thanhVien.Id && a.IsDuyet == true
                                                && a.NgayTao.Value.Year == year
                                                && a.NgayTao.Value.Month == month).Result.Count();

                        var isExistThanhVienInActivitiesUser = RepoStatisticActivitiesUser.Find(a => a.IdThanhVien == thanhVien.Id
                                                && a.Year == year
                                                && a.Month == month).Result.Count();

                        if (isExistThanhVienInActivitiesUser > 0)
                        {
                            // if exists => update
                            await RepoStatisticActivitiesUserMaster.UpdateWhere(a => a.IdThanhVien == thanhVien.Id
                                                            && a.Year == year
                                                            && a.Month == month, delegate (StatisticActivitiesUser obj)
                                                            {
                                                                obj.CountDangTin = countDangTin;
                                                            });
                        }
                        else
                        {
                            // if not exist => create
                            StatisticActivitiesUser statisticActivitiesUser = new StatisticActivitiesUser();
                            statisticActivitiesUser.IdThanhVien = thanhVien.Id;
                            statisticActivitiesUser.Year = year;
                            statisticActivitiesUser.Month = month;
                            statisticActivitiesUser.CountDangTin = countDangTin;
                            statisticActivitiesUser.CountUpTin = 0;
                            statisticActivitiesUser.CountLienHe = 0;

                            await RepoStatisticActivitiesUserMaster.InsertReturnEntity(statisticActivitiesUser);
                        }
                    }
                    //}
                    cusRes.StrResult = listThanhVien.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> GetThanhVienTichCuc([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Search & Get data
                var data = await _interfaceBLL.searchThanhVienTichCuc(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateIdThuongHieu([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    var lstIdDongXe = _interfaceBLL.Context.DangTin.Select(a => a.IdDongXe).Distinct().ToList();
                    foreach (var idDongXe in lstIdDongXe)
                    {
                        if (idDongXe.HasValue && idDongXe.Value > 0)
                        {
                            InterfaceParam paramCallDongXe = new InterfaceParam();
                            paramCallDongXe.ids = idDongXe.ToString();
                            var dongXe = await ApiGetDal.SearchDongXeAllField(paramCallDongXe);

                            var IdThuongHieu = CommonMethods.ConvertToInt64(dongXe.GetValueFromKey("IdThuongHieu", string.Empty).ToString());
                            var TenThuongHieu = dongXe.GetValueFromKey("TenThuongHieu", string.Empty).ToString();

                            await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(a => a.IdDongXe == idDongXe, delegate (DangTin obj)
                            {
                                obj.IdThuongHieu = IdThuongHieu;
                                obj.TenThuongHieu = TenThuongHieu;
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UninstallDevice([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    await RepoUserDeviceMaster.UpdateWhere(a => a.DeviceId.ToLower() == interfaceParam.deviceId.ToLower(), delegate (UserDevice obj)
                    {
                        obj.IsUninstalled = true;
                    });

                    await RepoInstallationStatisticsMaster.UpdateWhere(a => a.DeviceId.ToLower() == interfaceParam.deviceId.ToLower(), delegate (InstallationStatistics obj)
                    {
                        obj.Installation = 3;
                    });

                    cusRes.IntResult = 1;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        /// <summary>
        /// 1: Install
        /// 2: Update
        /// 3: Uninstall
        /// </summary>
        /// <param name="interfaceParam"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> InstallDevice([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    var newEntity = new InstallationStatistics();
                    newEntity.OsName = interfaceParam.osName;
                    newEntity.Version = interfaceParam.appVersion;
                    newEntity.Installation = 1; // 1: Install || 2: Update
                    newEntity.DeviceId = interfaceParam.deviceId;
                    newEntity.DeviceName = interfaceParam.deviceName;
                    cusRes.IntResult = await RepoInstallationStatisticsMaster.Insert(newEntity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateDevice([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    InstallationStatistics existEntity = RepoInstallationStatistics.Find(n => n.DeviceId.ToUpper() == interfaceParam.deviceId.ToUpper()).Result.FirstOrDefault();
                    if (existEntity == null)
                    {
                        existEntity = new InstallationStatistics();
                        existEntity.OsName = interfaceParam.osName;
                        existEntity.Version = interfaceParam.appVersion;
                        existEntity.Installation = 1; // New install
                        existEntity.DeviceId = interfaceParam.deviceId;
                        existEntity.DeviceName = interfaceParam.deviceName;
                        cusRes.IntResult = await RepoInstallationStatisticsMaster.Insert(existEntity);
                    }
                    else
                    {
                        existEntity.UpdateVersion = interfaceParam.appVersion;
                        existEntity.Installation = 2; // Update
                        cusRes.IntResult = await RepoInstallationStatisticsMaster.UpdateAfterAutoClone(existEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTieuDeDangTin_AddDongXe([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token == "d@ilyx3@12320180720RV")
                {
                    await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(a => string.IsNullOrEmpty(a.TenDongXe),
                        async delegate (DangTin obj)
                    {
                        InterfaceParam paramCallDongXe = new InterfaceParam();
                        paramCallDongXe.ids = obj.IdDongXe.ToString();
                        var dongXe = await ApiGetDal.SearchDongXeAllField(paramCallDongXe);
                        var VietTat = dongXe.GetValueFromKey("VietTat", string.Empty).ToString();
                        var Prefix = obj.Type == 1 ? "Bán " : "Mua ";

                        obj.TenDongXe = VietTat;
                        obj.TieuDe = Prefix + VietTat + " " + (obj.NamSanXuat.HasValue ? obj.NamSanXuat.Value.ToString() : "");
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion
    }

    //[ApiController]
    //[ApiVersion("2.0")]
    //[Route(Variables.C_ApiVersioning_InterfaceData_2)]
    //public class InterfaceData2Controller : Controller
    //{
    //    private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> _repoHistoryDangTinView;
    //    public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView> RepoHistoryDangTinView { get => _repoHistoryDangTinView == null ? _repoHistoryDangTinView = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinView>() : _repoHistoryDangTinView; set => _repoHistoryDangTinView = value; }
    //    private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinRepositoryRaoVatMaster;
    //    public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinRepositoryRaoVatMaster { get => _repoDangTinRepositoryRaoVatMaster == null ? _repoDangTinRepositoryRaoVatMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinRepositoryRaoVatMaster; set => _repoDangTinRepositoryRaoVatMaster = value; }

    //    private InterfaceBLL _interfaceBLL;
    //    private IDistributedCache _cache;
    //    private AdminBll _adminBll;

    //    public InterfaceData2Controller(IDistributedCache cache)
    //    {
    //        this._cache = cache;
    //        this._adminBll = new AdminBll();
    //        this._interfaceBLL = new InterfaceBLL(Request);
    //    }

    //    [HttpGet]
    //    public async Task<IActionResult> SearchDangTin([FromQuery]InterfaceParam interfaceParam)
    //    {
    //        Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin Start");
    //        CustomResult cusRes = new CustomResult();
    //        try
    //        {
    //            // Handle param
    //            interfaceParam.displayItems = 5;
    //            interfaceParam.displayPage = 1;

    //            // Active: Homepage, details
    //            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin Active: Homepage, details");
    //            interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

    //            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Search & Get data");
    //            // Search & Get data
    //            var tuple = await _interfaceBLL.SearchDangTin(interfaceParam);
    //            var data = tuple.Item1;
    //            string keyCache = tuple.Item2;

    //            //IEnumerable<DangTin> data = await _interfaceBLL.SearchDangTin(interfaceParam);
    //            // nếu search chỉ có 1 id thì update thì update số lượt xem của id đó
    //            if (interfaceParam.lstId != null && interfaceParam.lstId.Count == 1 && interfaceParam.isViewType == true && data != null)
    //            {
    //                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Update history SoLuotXem");
    //                // Update history SoLuotXem
    //                HistoryDangTinView historyDangTinView = new HistoryDangTinView();
    //                historyDangTinView.IdDangTin = interfaceParam.lstId.FirstOrDefault();
    //                historyDangTinView.IdThanhVien = Request.GetTokenIdThanhVien();
    //                historyDangTinView.DeviceId = interfaceParam.deviceId;
    //                historyDangTinView.Type = 1;
    //                await RepoHistoryDangTinView.Insert(historyDangTinView);

    //                await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => interfaceParam.lstId.Contains(s.Id), delegate (DangTin obj)
    //                {
    //                    obj.SoLuotXem = obj.SoLuotXem + 1;
    //                });
    //            }
    //            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Set DataResult");
    //            cusRes.StrResult = keyCache;
    //            cusRes.DataResult = data;

    //            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Get property name list");
    //            // Get property name list
    //            int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
    //            cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
    //        }
    //        catch (Exception ex)
    //        {
    //            cusRes.SetException(ex);
    //        }
    //        Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin End");
    //        return Response.OkObjectResult(cusRes);
    //    }
    //}
}