﻿using GmailApi;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [AllowAnonymous]
    [Route(Variables.C_ApiRouting_2)]
    public class EmailController : Controller
    {
        //ApiGetDal _apiGetDal = new ApiGetDal();
        //public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        private AdminBll _adminBll;

        public EmailController()
        {
            this._adminBll = new AdminBll();
        }

        /// <summary>
        /// gửi email để xác nhận thông tin người dùng
        /// </summary>
        /// <param name="interfaceParam">chứa nhiều tham số</param>
        /// <returns>
        /// "IntResult": 1 -> thành công
        /// "IntResult": -3 -> đã gửi mã xác thực trước đó (sẽ không gửi lại mã nữa)
        /// "IntResult": -2 -> lỗi khác 
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> SendVerifyEmail(InterfaceParam interfaceParam)
        {
            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin SendVerifyEmail");
            CustomResult cusRes = new CustomResult();

            try
            {
                // lấy thông tin truyền lên là email
                if (string.IsNullOrEmpty(interfaceParam.email))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                // kiểm tra thông tin trước khi gửi email
                EntityBaseRepositoryRaoVat<MaXacNhan> repoMaXacNhan = new EntityBaseRepositoryRaoVat<MaXacNhan>();
                IEnumerable<MaXacNhan> sendedCode = await repoMaXacNhan.Search(n => n.Email == interfaceParam.email
                                                                            && n.LoaiXacNhan == (int)Variables.LoaiXacNhan.SendVerifyEmail, "NgayCap:Desc", null);
                // nếu đã gửi mxt ròi thì kiểm tra xem có hết hạn chưa
                string durationValidString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_DURATIONVERIFYISVALID);
                if (sendedCode != null && sendedCode.Count() > 0)
                {
                    MaXacNhan lastedRecord = sendedCode.First();
                    DateTime sendedCodeAt = lastedRecord.NgayCap ?? DateTime.MinValue;
                    TimeSpan time = DateTime.Now - sendedCodeAt;
                    double durationValid = CommonMethods.ConvertToDouble(durationValidString);// theo seconds
                    TimeSpan duration = TimeSpan.FromSeconds(durationValid);
                    if (time < duration)
                    {
                        cusRes.Message = string.Format(Messages.ERR_008);
                        cusRes.IntResult = -3;
                        return Response.OkObjectResult(cusRes);
                    }
                }

                EntityBaseRepositoryRaoVat<MaXacNhan> repoMaXacNhanMaster = new EntityBaseRepositoryRaoVat<MaXacNhan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master);
                // nếu mxt đã hết hạn thì xoá hết mã xác thực đi
                await repoMaXacNhanMaster.DeleteWhere(n => sendedCode.Contains(n));

                // tạo mã xác thực
                string code = CommonMethods.GenerateRandomSixDigital();

                // lưu mxt vào db postgre (gán type là 4)
                MaXacNhan insertEntity = new MaXacNhan()
                {
                    Code = code,
                    NgayCap = DateTime.Now,
                    LoaiXacNhan = (int)Variables.LoaiXacNhan.SendVerifyEmail,
                    Email = interfaceParam.email
                };
                cusRes.DataResult = await repoMaXacNhanMaster.InsertReturnEntity(insertEntity);

                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-M1 SendVerifyEmail:" + Variables.EnvironmentIsProduction);
                // send email
                if (Variables.EnvironmentIsProduction == true)
                {
                    GmailApiCore gac = new GmailApiCore();
                    var receiveMail = interfaceParam.email;
                    var ccEmail = string.Empty;
                    var bccEmail = Variables.EmailToCollectError_Name;
                    var tieuDe = Variables.TitleConfirmationCode;
                    var noiDung = string.Format(Variables.ContentConfirmTemplate, code, DateTime.Now.ToString(Variables.DD_MM_YYYY_FULL), durationValidString);

                    var res = gac.SendMessage(receiveMail, ccEmail, bccEmail, tieuDe, noiDung);
                }
                else
                {
                    MailService mailService = new MailService();
                    mailService.SendEmail(Variables.TitleConfirmationCode, code, interfaceParam.email);
                }
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-End SendVerifyEmail");
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }
    }
}
