﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class UserPointController : Controller
    {
        // History
        private EntityBaseRepositoryHistoryRaoVat<HistoryPoint> _repoHistoryPoint;
        public EntityBaseRepositoryHistoryRaoVat<HistoryPoint> RepoHistoryPoint { get => _repoHistoryPoint == null ? _repoHistoryPoint = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>() : _repoHistoryPoint; set => _repoHistoryPoint = value; }
        private EntityBaseRepositoryHistoryRaoVat<HistoryTimePurchased> _repoHistoryPointTimePurchased;
        public EntityBaseRepositoryHistoryRaoVat<HistoryTimePurchased> RepoHistoryPointTimePurchased { get => _repoHistoryPointTimePurchased == null ? _repoHistoryPointTimePurchased = new EntityBaseRepositoryHistoryRaoVat<HistoryTimePurchased>() : _repoHistoryPointTimePurchased; set => _repoHistoryPointTimePurchased = value; }

        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<UserPoint> _repo;
        public EntityBaseRepositoryRaoVat<UserPoint> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<UserPoint>() : _repo; set => _repo = value; }
        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCodeMaster;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCodeMaster { get => _repoMarketingCodeMaster == null ? _repoMarketingCodeMaster = new EntityBaseRepositoryRaoVat<MarketingCode>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingCodeMaster; set => _repoMarketingCodeMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUser;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUser { get => _repoMarketingUser == null ? _repoMarketingUser = new EntityBaseRepositoryRaoVat<MarketingUser>() : _repoMarketingUser; set => _repoMarketingUser = value; }
        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUserMaster;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUserMaster { get => _repoMarketingUserMaster == null ? _repoMarketingUserMaster = new EntityBaseRepositoryRaoVat<MarketingUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingUserMaster; set => _repoMarketingUserMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMarketingPlanMaster;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMarketingPlanMaster { get => _repoMarketingPlanMaster == null ? _repoMarketingPlanMaster = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingPlanMaster; set => _repoMarketingPlanMaster = value; }

        public UserPointController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = CommonMethods.SplitStringToInt64(interfaceParam.notIds);
                interfaceParam.lstIdThanhVien = CommonMethods.SplitStringToInt64(interfaceParam.idsThanhVien);
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }
                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 20;
                }

                // get thong tin token từ request
                if (interfaceParam.allIncluding == true)
                {
                    //var headers = Request.Headers;
                    //var headersDic = headers.AsDictionary();
                    interfaceParam.token = Request.GetTokenFromHeader();
                }

                // Search & Get data
                IEnumerable<UserPoint> data = await _adminBll.searchUserPoint(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserPoint entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                cusRes.DataResult = await RepoUserPointMaster.InsertReturnEntity(entity);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]/{idThanhVien}")]
        public async Task<IActionResult> UpdateUserPointByIdThanhVien(int idThanhVien, [FromBody] UserPoint newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                UserPoint editEntity = _adminBll.Context.UserPoint.Where(n => n.IdThanhVien == idThanhVien).FirstOrDefault();

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "User point này"));
                    return Response.OkObjectResult(cusRes);
                }

                if (editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value > 0)
                {
                    cusRes.SetException(null, Messages.ERR_021);
                    return Response.OkObjectResult(cusRes);
                }

                // CheckoutType == 1 => payment progressing
                if (newEntity.CheckoutType == 1 && editEntity.CheckoutType == 1)
                {
                    cusRes.SetException(null, Messages.ERR_025);
                    return Response.OkObjectResult(cusRes);
                }

                // Tránh tình trạng newEntity sai id trên đường dẫn
                newEntity.Id = editEntity.Id;

                cusRes.IntResult = await RepoUserPointMaster.UpdateAfterAutoClone(newEntity);
                cusRes.DataResult = _adminBll.Context.UserPoint.Where(n => n.IdThanhVien == idThanhVien);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdatePoint(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                // chuẩn bị dữ liệu
                List<long> lstId = CommonMethods.SplitStringToInt64(interfaceParam.ids); // list id thanh vien
                int type = CommonMethods.ConvertToInt32(interfaceParam.type);

                // kiểm tra dữ liệu đầu vào 
                if (string.IsNullOrEmpty(interfaceParam.ids) || lstId == null || lstId.Count <= 0)
                {
                    cusRes.SetException(new Exception(string.Format(Messages.ERR_010, "id")));
                    return Response.OkObjectResult(cusRes);
                }
                if (string.IsNullOrEmpty(interfaceParam.type) || type <= 0)
                {
                    cusRes.SetException(new Exception(string.Format(Messages.ERR_010, "type")));
                    return Response.OkObjectResult(cusRes);
                }

                if (type == (int)Variables.TypeUserPoint.Bought) // mua point
                {
                    //foreach (var id in lstId)
                    //{
                    //    // get phần tử cũ
                    //    UserPoint editUserPoint = (await Repo.Find(n => n.IdThanhVien == id)).FirstOrDefault();

                    //    // cập nhật thông tin mới
                    //    if (editUserPoint == null)
                    //    {
                    //        continue;
                    //    }

                    //    // Store marketingplan data
                    //    var MarketingCode = editUserPoint.MarketingCode;
                    //    var IdMarketingPlan = editUserPoint.IdMarketingPlan;
                    //    var IdMarketingCode = editUserPoint.IdMarketingCode;
                    //    var Value = editUserPoint.Value;
                    //    var ValueType = editUserPoint.ValueType;
                    //    var MaxValue = editUserPoint.MaxValue;
                    //    var pointTmp = editUserPoint.PointTmp;
                    //    var totalPrice = editUserPoint.TotalPrice;
                    //    long? idNewRank = 0;

                    //    var notePromotion = string.Empty;
                    //    // Aprove => Update Point
                    //    if(ValueType == 1)
                    //    {
                    //        //interfaceParam.note = "Khuyến mãi [" +Value  +" Point]. "+ interfaceParam.note;
                    //        notePromotion = "Khuyến mãi [" + Value + " Point].";
                    //        editUserPoint.Point = editUserPoint.Point + pointTmp + CommonMethods.ConvertToInt64(Value);
                    //    }
                    //    else
                    //    {
                    //        if(ValueType == 2)
                    //        {
                    //            //interfaceParam.note = "Khuyến mãi [" + Value + "%]. " + interfaceParam.note;
                    //            notePromotion = "Khuyến mãi [" + Value + "%].";
                    //        }
                    //        else if (ValueType == 3)
                    //        {
                    //            //interfaceParam.note = "Khuyến mãi [" + Value + " VND]. " + interfaceParam.note;
                    //            notePromotion = "Khuyến mãi [" + Value + " VND].";
                    //        }

                    //        editUserPoint.Point += pointTmp;
                    //    }

                    //    var noteDiscountRanking = string.Empty;
                    //    if(interfaceParam.percentDiscount.HasValue && interfaceParam.percentDiscount.Value > 0)
                    //    {
                    //        //interfaceParam.note = "Ưu đãi thành viên [" + interfaceParam.percentDiscount.Value + "%]. " + interfaceParam.note;
                    //        noteDiscountRanking = "Ưu đãi thành viên [" + interfaceParam.percentDiscount.Value + "%].";
                    //    }

                    //    interfaceParam.note = (string.IsNullOrEmpty(notePromotion) ? string.Empty : notePromotion + "<br >")
                    //                   + (string.IsNullOrEmpty(noteDiscountRanking) ? string.Empty : noteDiscountRanking + "<br >")
                    //                   + "[ Admin - Banking Payment ]";

                    //    editUserPoint.PointTmp = 0;
                    //    editUserPoint.MarketingCode = string.Empty;
                    //    editUserPoint.IdMarketingPlan = -1;
                    //    editUserPoint.Value = string.Empty;
                    //    editUserPoint.ValueType = -1;
                    //    editUserPoint.IdAdminDangKyDuyet = -1;
                    //    editUserPoint.MaxValue = string.Empty;
                    //    editUserPoint.TotalPrice = 0;
                    //    editUserPoint.IdMarketingCode = -1;
                    //    int result = await RepoUserPointMaster.UpdateAfterAutoClone(editUserPoint);

                    //    if(result >0)
                    //    {
                    //        // Add to notification
                    //        InterfaceParam paramNotify = new InterfaceParam();
                    //        paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                    //        paramNotify.idThanhVien = id;
                    //        paramNotify.ma = Variables.Constants.F003; // Mua point thành công
                    //        paramNotify.noiDung = (pointTmp ?? 0).ToString();

                    //        CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                    //        await commonController.InsertNotification(paramNotify);
                    //    }
                    //    else
                    //    {
                    //        cusRes.SetException(null, Messages.ERR_019);
                    //        return Response.OkObjectResult(cusRes);
                    //    }

                    //    // If use promotion, Insert To MarketingUser
                    //    if(!string.IsNullOrEmpty(MarketingCode))
                    //    {
                    //        var marketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == id
                    //                            && a.IdMarketingPlan == IdMarketingPlan
                    //                            && a.IdMarketingCode == IdMarketingCode
                    //                            && a.MarketingCode.ToUpper() == MarketingCode.ToUpper()).Result.FirstOrDefault();
                    //        if (marketingUser != null)
                    //        {
                    //            cusRes.SetException(null, Messages.ERR_017);
                    //            return Response.OkObjectResult(cusRes);
                    //        }

                    //        marketingUser = new MarketingUser();
                    //        marketingUser.IdThanhVien = id;
                    //        marketingUser.IdMarketingPlan = IdMarketingPlan;
                    //        marketingUser.IdMarketingCode = IdMarketingCode;
                    //        marketingUser.MarketingCode = MarketingCode.ToUpper();
                    //        marketingUser.NoiDung = "Mua point: " + pointTmp + " [ " + totalPrice + " ]";
                    //        marketingUser.Value = Value;
                    //        marketingUser.ValueType = ValueType;
                    //        marketingUser.MaxValue = MaxValue;

                    //        await RepoMarketingUserMaster.Insert(marketingUser);

                    //        // Update TotalUsed MarketingCode
                    //        await RepoMarketingCodeMaster.UpdateWhere(n => n.IdMarketingPlan == IdMarketingPlan
                    //                && n.Id == IdMarketingCode
                    //                && n.Code == MarketingCode, delegate (MarketingCode obj)
                    //        {
                    //            obj.TotalUsed = (obj.TotalUsed ?? 0) + 1;
                    //        });

                    //        // Update TotalUsed MarketingPlan
                    //        await RepoMarketingPlanMaster.UpdateWhere(n => n.Id == IdMarketingPlan, delegate (MarketingPlan obj)
                    //        {
                    //            obj.TotalUsed = (obj.TotalUsed ?? 0) + 1;
                    //        });
                    //    }

                    //    // insert một dòng record trong historyPoint
                    //    HistoryPoint insEntity = new HistoryPoint()
                    //    {
                    //        Type = type,
                    //        Point = pointTmp,
                    //        IdThanhVien = id,
                    //        NgayTao = DateTime.Now,
                    //        Note = interfaceParam.note,
                    //        Rate = interfaceParam.rate,
                    //        IdRank = interfaceParam.idRank,
                    //        RankPercentDiscount = interfaceParam.percentDiscount,
                    //        Price = totalPrice,
                    //        MarketingCode = MarketingCode,
                    //        IdMarketingPlan = IdMarketingPlan,
                    //        Value = Value,
                    //        ValueType = ValueType,
                    //        MaxValue = MaxValue,
                    //        IdMarketingCode = IdMarketingCode
                    //    };
                    //    await RepoHistoryPoint.Insert(insEntity);

                    //    // Upgrade Rank
                    //    CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                    //    cusRes.StrResult = commonMethodsRaoVat.UpgradeRank(id, out idNewRank);
                    //}
                }

                if (type == (int)Variables.TypeUserPoint.UsedPinTop) // sử dụng point
                {
                    int point = CommonMethods.ConvertToInt32(interfaceParam.point);

                    // list Id ThanhVien
                    foreach(var id in lstId)
                    {
                        // cập nhật thông tin UserPoint
                        await RepoUserPointMaster.UpdateWhere(n => id == (n.IdThanhVien ?? 0), n =>
                        {
                            n.Point -= point;
                            n.PointUsed += point;
                        });

                        // insert historyPoint
                        string rateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_TIME);
                        int rate = CommonMethods.ConvertToInt32(rateString);
                        HistoryPoint insHistoryPoint = new HistoryPoint()
                        {
                            Type = type,
                            Point = point,
                            IdThanhVien = id,
                            Rate = rate,
                            Note = interfaceParam.note,
                            NgayTao = DateTime.Now,
                        };
                        await RepoHistoryPoint.Insert(insHistoryPoint);

                        // insert HistoryTimePurchased
                        HistoryTimePurchased insertHistoryTimePurchased = new HistoryTimePurchased();
                        insertHistoryTimePurchased.Point = point;
                        insertHistoryTimePurchased.Rate = rate;
                        insertHistoryTimePurchased.IdThanhVien = id;
                        insertHistoryTimePurchased.ThoiGianDangTinDaMua = (insertHistoryTimePurchased.Point ?? 0) * (insertHistoryTimePurchased.Rate ?? 0);
                        await RepoHistoryPointTimePurchased.Insert(insertHistoryTimePurchased);

                        // Update total time
                        await RepoThanhVienMaster.UpdateWhere(n => n.Id == id, n =>
                        {
                            n.TotalTime = (n.TotalTime ?? 0) + insertHistoryTimePurchased.ThoiGianDangTinDaMua;
                        });
                    }

                    // trả kết quả về
                    InterfaceParam interfaceParam_return = new InterfaceParam();
                    interfaceParam.lstIdThanhVien = lstId;
                    interfaceParam.token = Request.GetTokenFromHeader();
                    interfaceParam.allIncludingString = "true";
                    cusRes.DataResult = await _adminBll.searchUserPoint(interfaceParam);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> RegistAdminApproveUserPoint(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                if (interfaceParam.lstId.Count <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                long idAdmin = Request.GetTokenIdAdmin();

                foreach (var id in interfaceParam.lstId)
                {
                    UserPoint editEntity = await Repo.GetSingle(id);

                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        if (!editEntity.IdAdminDangKyDuyet.HasValue || editEntity.IdAdminDangKyDuyet.Value <= 0)
                        {
                            cusRes.IntResult = await RepoUserPointMaster.UpdateWhere(s => id == s.Id, delegate (UserPoint obj)
                            {
                                obj.IdAdminDangKyDuyet = idAdmin;
                                obj.NgayDangKyDuyet = DateTime.Now;
                            });

                            var res = await Repo.Find(s => s.Id == id);
                            cusRes.DataResult = res;
                        }
                        else
                        {
                            cusRes.SetException(null, Messages.ERR_013);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> RemoveAdminApproveUserPoint(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                if (interfaceParam.lstId.Count <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                long idAdmin = Request.GetTokenIdAdmin();

                foreach (var id in interfaceParam.lstId)
                {
                    UserPoint editEntity = await Repo.GetSingle(id);

                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        if (editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value == idAdmin)
                        {
                            cusRes.IntResult = await RepoUserPointMaster.UpdateWhere(s => id == s.Id, delegate (UserPoint obj)
                            {
                                obj.IdAdminDangKyDuyet = -1;
                            });

                            var res = await Repo.Find(s => s.Id == id);
                            cusRes.DataResult = res;
                        }
                        else
                        {
                            cusRes.SetException(null, Messages.ERR_014);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }
    }
}
