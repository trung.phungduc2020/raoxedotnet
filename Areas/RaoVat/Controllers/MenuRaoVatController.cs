﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class MenuRaoVatController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;

        private MenuRaoVatRepositoryRaoVat _repo;
        public MenuRaoVatRepositoryRaoVat Repo { get => _repo == null ? _repo = new MenuRaoVatRepositoryRaoVat(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<MenuRaoVat> _repoMenuRaoVatMaster;
        public EntityBaseRepositoryRaoVat<MenuRaoVat> RepoMenuRaoVatMaster { get => _repoMenuRaoVatMaster == null ? _repoMenuRaoVatMaster = new EntityBaseRepositoryRaoVat<MenuRaoVat>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMenuRaoVatMaster; set => _repoMenuRaoVatMaster = value; }

        public MenuRaoVatController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notId) ? CommonMethods.SplitStringToInt64(interfaceParam.notId) : null;
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.lstCapDo = CommonMethods.SplitStringToInt(interfaceParam.capDo, true);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notId) ? CommonMethods.SplitStringToInt64(interfaceParam.notId) : null;
                interfaceParam.childrenStructureBool = CommonMethods.ConvertToBoolean(interfaceParam.childrenStructure, false).Value;

                if (interfaceParam.childrenStructureBool)
                {
                    // For Tree Table
                    IEnumerable<EntityForTreeTable<MenuRaoVat>> data = await _adminBll.SearchMenuForTreeTable(interfaceParam);
                    cusRes.DataResult = data;

                    // Get property name list
                    int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                    cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
                }
                else
                {
                    // Search & Get data
                    IEnumerable<MenuRaoVat> data = await _adminBll.SearchMenuRaoVat(interfaceParam);
                    cusRes.DataResult = data;

                    // Get property name list
                    int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                    cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]MenuRaoVat newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string idControl = string.Empty;
                string msgCheckedData = IsCheckedData(newEntity, ref idControl);
                if (!string.IsNullOrEmpty(msgCheckedData))
                {
                    cusRes.GotoLink = idControl;
                    cusRes.SetException(null, msgCheckedData);
                    return Response.OkObjectResult(cusRes);
                }

                MenuRaoVat editEntity = await Repo.GetSingle(id);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Menu này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Hanld FormData
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;
                    newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiemMenu(newEntity);

                    if (string.IsNullOrEmpty(newEntity.RewriteUrl))
                    {
                        newEntity.RewriteUrl = CommonMethods.GetRewriteString(newEntity.TenMenu);
                    }

                    cusRes.IntResult = await RepoMenuRaoVatMaster.UpdateAfterAutoClone(newEntity);
                    cusRes.DataResult = await Repo.Find(s => s.Id == id);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MenuRaoVat entity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string idControl = string.Empty;
                string msgCheckedData = IsCheckedData(entity, ref idControl);
                if (!string.IsNullOrEmpty(msgCheckedData))
                {
                    cusRes.GotoLink = idControl;
                    cusRes.SetException(null, msgCheckedData);
                    return Response.OkObjectResult(cusRes);
                }

                entity.TuKhoaTimKiem = BuildTuKhoaTimKiemMenu(entity);
                if (string.IsNullOrEmpty(entity.RewriteUrl))
                {
                    entity.RewriteUrl = CommonMethods.GetRewriteString(entity.TenMenu);
                }
                cusRes.DataResult = await RepoMenuRaoVatMaster.InsertReturnEntity(entity);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes); ;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                CustomResult delCusRes = new CustomResult();
                string consolidateMessage = string.Empty;

                foreach (int id in interfaceParam.lstId)
                {
                    delCusRes = new CustomResult();
                    delCusRes = await Delete(id);

                    if (delCusRes.IntResult > 0)
                    {
                        cusRes.IntResult += delCusRes.IntResult;
                    }
                    else
                    {
                        consolidateMessage += delCusRes.Message + " \n";
                    }
                }

                if (!string.IsNullOrEmpty(consolidateMessage) && consolidateMessage.Length > 0)
                {
                    cusRes.SetException(null, consolidateMessage);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                string trangThaiString = CommonMethods.ConvertToString(interfaceParam.trangThai);
                Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);

                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                cusRes.IntResult = await RepoMenuRaoVatMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (MenuRaoVat obj)
                {
                    string message = string.Empty;
                    if (trangThai == 1)
                    {
                        obj.TrangThai = trangThai;
                    }
                    else if (trangThai != 1 && IsCheckSoftDeleteMenu(obj, ref message))
                    {
                        obj.TrangThai = trangThai;
                    }
                    else
                    {
                        cusRes.Message += message;
                    }
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #region private

        private async Task<CustomResult> Delete(int id)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                MenuRaoVat delEntity = await Repo.GetSingle(id);

                if (delEntity == null)
                {
                    cusRes.SetException(null, "Id = " + id + ": " + Messages.ERR_003);
                    return cusRes;
                }
                else
                {
                    string messageError = string.Empty;
                    if (IsCheckDeleteMenu(delEntity, ref messageError))
                    {
                        cusRes.IntResult = await RepoMenuRaoVatMaster.Delete(delEntity);
                    }
                    else
                    {
                        cusRes.Message = messageError;
                        return cusRes;
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return cusRes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="messageError"></param>
        /// <returns>
        /// có trang con trả về false ngược lại trả về true
        /// </returns>
        private bool IsCheckDeleteMenu(MenuRaoVat menu, ref string messageError)
        {
            bool res = true;

            if (menu != null)
            {
                string TenMenu = menu.TenMenu;
                AdminBll bll = new AdminBll();

                InterfaceParam interfaceParam = new InterfaceParam();
                interfaceParam.lstIdMenuParent = new List<long>() { menu.Id };

                IEnumerable<MenuRaoVat> childMenu = bll.SearchMenuRaoVat(interfaceParam).Result;
                if (childMenu != null && childMenu.Count() > 0)
                {
                    messageError = "Xóa không thành công. Vì trang '" + TenMenu + "' có " + childMenu.Count().ToString() + " trang con";
                    res = false;
                }
            }
            else
            {
                res = false;
            }
            return res;
        }

        private bool IsCheckSoftDeleteMenu(MenuRaoVat menu, ref string messageError)
        {
            bool res = true;

            if (menu != null)
            {
                string TenMenu = menu.TenMenu;
                AdminBll bll = new AdminBll();

                InterfaceParam interfaceParam = new InterfaceParam();
                interfaceParam.lstIdMenuParent = new List<long>() { menu.Id };
                interfaceParam.lstTrangThai = new List<int>() { 1, 2 };

                IEnumerable<MenuRaoVat> childMenu = bll.SearchMenuRaoVat(interfaceParam).Result;
                if (childMenu != null && childMenu.Count() > 0)
                {
                    messageError = "Menu " + TenMenu + " có " + childMenu.FirstOrDefault()?.TenMenu + " là trang con ";
                    if (childMenu.FirstOrDefault()?.TrangThai == 2)
                    {
                        messageError += "(đang ẩn)";
                    }
                    res = false;
                }
            }
            else
            {
                res = false;
            }
            return res;
        }

        private string IsCheckedData(MenuRaoVat menu, ref string idControl)
        {
            string TenMenu = "";
            string TieuDeTrang = "";
            string MoTaTrang = "";

            string langKey = string.Empty;
            string langName = string.Empty;

            TenMenu = CommonMethods.FilterTen(menu.TenMenu).Trim();
            if (string.IsNullOrEmpty(TenMenu))
            {
                idControl = "txtTenMenu" + langKey;
                return "Tên trang " + langName + " chưa nhập";
            }

            if (TenMenu.Length < Variables.TITLE_MINLENGTH)
            {
                idControl = "txtTenMenu" + langKey;
                return "Tên trang " + langName + " phải nhiều hơn " + Variables.TITLE_MINLENGTH + "ký tự";
            }

            if (TenMenu.Length > Variables.TITLE_MAXLENGTH)
            {
                idControl = "txtTenMenu" + langKey;
                return "Tên trang " + langName + " phải ít hơn " + Variables.TITLE_MAXLENGTH + "ký tự";
            }

            TieuDeTrang = CommonMethods.FilterTen(menu.TieuDeTrang).Trim();
            if (!string.IsNullOrEmpty(TieuDeTrang))
            {
                if (TieuDeTrang.Length < Variables.TITLE_MINLENGTH)
                {
                    idControl = "txtTieuDeTrang" + langKey;
                    return "SEO Tiêu đề trang " + langName + " phải nhiều hơn " + Variables.TITLE_MINLENGTH + "ký tự";
                }

                if (TieuDeTrang.Length > Variables.TITLE_MAXLENGTH)
                {
                    idControl = "txtTieuDeTrang" + langKey;
                    return "SEO Tiêu đề trang " + langName + " phải ít hơn " + Variables.TITLE_MAXLENGTH + "ký tự";
                }
            }

            MoTaTrang = CommonMethods.FilterTen(menu.MoTaTrang).Trim();
            if (!string.IsNullOrEmpty(MoTaTrang))
            {
                if (MoTaTrang.Length < Variables.DESCRIPTION_MINLENGTH)
                {
                    idControl = "txtMoTaTrang" + langKey;
                    return "SEO Mô tả trang " + langName + " phải nhiều hơn " + Variables.TITLE_MINLENGTH + "ký tự";
                }

                if (MoTaTrang.Length > Variables.DESCRIPTION_MAXLENGTH)
                {
                    idControl = "txtMoTaTrang" + langKey;
                    return "SEO Mô tả trang " + langName + " phải ít hơn " + Variables.TITLE_MAXLENGTH + "ký tự";
                }
            }

            return string.Empty;
        }

        private string BuildTuKhoaTimKiemMenu(MenuRaoVat menu)
        {
            string res = string.Empty;

            if (menu != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(menu.TenMenu);
            }

            return res;
        }

        #endregion
    }
}
