﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class KpiAutoRuleController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<KpiAutoRule> _repo;
        public EntityBaseRepositoryRaoVat<KpiAutoRule> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<KpiAutoRule>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<KpiAutoRule> _repoMaster;
        public EntityBaseRepositoryRaoVat<KpiAutoRule> RepoMaster { get => _repoMaster == null ? _repo = new EntityBaseRepositoryRaoVat<KpiAutoRule>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMaster; set => _repoMaster = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPoint;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPoint { get => _repoUserPoint == null ? _repoUserPoint = new EntityBaseRepositoryRaoVat<UserPoint>(Request) : _repoUserPoint; set => _repoUserPoint = value; }

        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTin;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTin { get => _repoDangTin == null ? _repoDangTin = new EntityBaseRepositoryRaoVat<DangTin>(Request) : _repoDangTin; set => _repoDangTin = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>(Request) : _repoThanhVien; set => _repoThanhVien = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }


        public KpiAutoRuleController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CheckAutoDuyet([FromBody] InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            var kpiType = (int)Variables.KpiAutoType.AutoDuyet;
            int countSuccess = 0;
            try
            {
                List<long> lstIdThanhVien = CommonMethods.SplitStringToInt64(interfaceParam.idsThanhVien);

                if (lstIdThanhVien.Count == 0 || lstIdThanhVien == null)
                {
                    lstIdThanhVien = RepoThanhVien.Entities.Select(a => a.Id).ToList();
                }

                foreach (var idThanhVien in lstIdThanhVien)
                {
                    var lstRule = Repo.Find(a => a.Type == kpiType && a.IsApplied.Value == true).Result;
                    bool isApprove = false;

                    foreach (var rule in lstRule)
                    {
                        var RuleCondition = (rule.Rule ?? 0);
                        switch (rule.Code.ToUpper())
                        {
                            case Variables.MACAUHINH_KPI_POINT:
                                UserPoint userPoint = RepoUserPoint.Find(a => a.IdThanhVien == idThanhVien).Result.FirstOrDefault();
                                if (userPoint != null && userPoint.Point.HasValue && userPoint.Point.Value >= RuleCondition)
                                {
                                    isApprove = true;
                                }
                                else
                                {
                                    isApprove = false;
                                }
                                break;

                            case Variables.MACAUHINH_KPI_POST:
                                int CountDangTin = RepoDangTin.Find(a => a.IdThanhVien == idThanhVien && a.IsDuyet.HasValue && a.IsDuyet.Value == true).Result.Count();
                                if (CountDangTin >= RuleCondition)
                                {
                                    isApprove = true;
                                }
                                else
                                {
                                    isApprove = false;
                                }
                                break;
                        }
                    }

                    countSuccess = countSuccess + 1;
                    if (isApprove)
                    {
                        await RepoThanhVienMaster.UpdateWhere(s => s.Id == idThanhVien, delegate (ThanhVien obj)
                        {
                            List<string> lstApprove = string.IsNullOrEmpty(obj.AllowKpiRule) ? new List<string> { kpiType.ToString() }
                                                        : obj.AllowKpiRule.Split(",").ToList();
                            if (!lstApprove.Contains(kpiType.ToString()))
                            {
                                lstApprove.Add(kpiType.ToString());
                            }
                            obj.AllowKpiRule = string.Join(",", lstApprove);
                        });
                    }
                    else
                    {
                        await RepoThanhVienMaster.UpdateWhere(s => s.Id == idThanhVien, delegate (ThanhVien obj)
                        {
                            List<string> lstApprove = string.IsNullOrEmpty(obj.AllowKpiRule) ? new List<string> { kpiType.ToString() }
                                                        : obj.AllowKpiRule.Split(",").ToList();
                            if (lstApprove.Contains(kpiType.ToString()))
                            {
                                lstApprove.Remove(kpiType.ToString());
                            }
                            obj.AllowKpiRule = string.Join(",", lstApprove);
                        });
                    }

                    cusRes.StrResult = countSuccess.ToString();
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}