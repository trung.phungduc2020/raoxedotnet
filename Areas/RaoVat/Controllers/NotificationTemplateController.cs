﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class NotificationTemplateController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<NotificationTemplate> _repo;
        public EntityBaseRepositoryRaoVat<NotificationTemplate> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<NotificationTemplate>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<NotificationTemplate> _repoNotificationTemplateMaster;
        public EntityBaseRepositoryRaoVat<NotificationTemplate> RepoNotificationTemplateMaster { get => _repoNotificationTemplateMaster == null ? _repoNotificationTemplateMaster = new EntityBaseRepositoryRaoVat<NotificationTemplate>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoNotificationTemplateMaster; set => _repoNotificationTemplateMaster = value; }

        public NotificationTemplateController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.tuKhoaTimKiem = interfaceParam.tuKhoaTimKiem.ToASCIIAndLower();

                // Search & Get data
                IEnumerable<NotificationTemplate> data = await _adminBll.SearchNotificationTemplate(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NotificationTemplate entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    entity.TuKhoaTimKiem = BuildTuKhoaTimKiem(entity);
                    cusRes.DataResult = await RepoNotificationTemplateMaster.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] NotificationTemplate newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    NotificationTemplate editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin tức này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiem(newEntity);
                        cusRes.IntResult = await RepoNotificationTemplateMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        public string BuildTuKhoaTimKiem(NotificationTemplate entity)
        {
            string res = string.Empty;
            if (entity != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(entity.TieuDe) + "," + CommonMethods.ConvertUnicodeToASCII(entity.NoiDung);
            }
            return res;
        }

        #endregion
    }
}
