﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class ThanhVienController : Controller
    {
        private IMemoryCache _cache;
        private ModelsRaoVat.AdminBll _adminBll;

        private EntityBaseRepositoryHistoryRaoVat<HistoryPoint> _repoHistoryPoint;
        public EntityBaseRepositoryHistoryRaoVat<HistoryPoint> RepoHistoryPoint { get => _repoHistoryPoint == null ? _repoHistoryPoint = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>() : _repoHistoryPoint; set => _repoHistoryPoint = value; }

        private ApiGetDal _apiGetDal;
        public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        private ThanhVienRepositoryRaoVat _repo;
        public ThanhVienRepositoryRaoVat Repo { get => _repo == null ? _repo = new ThanhVienRepositoryRaoVat(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPoint;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPoint { get => _repoUserPoint == null ? _repoUserPoint = new EntityBaseRepositoryRaoVat<UserPoint>() : _repoUserPoint; set => _repoUserPoint = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>() : _repoDangTinContact; set => _repoDangTinContact = value; }
        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContactMaster;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContactMaster { get => _repoDangTinContactMaster == null ? _repoDangTinContactMaster = new EntityBaseRepositoryRaoVat<DangTinContact>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinContactMaster; set => _repoDangTinContactMaster = value; }


        public ThanhVienController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new ModelsRaoVat.AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                // Handle param
                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 10;
                }
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }

                // Search & Get data
                IEnumerable<ThanhVien> data = await _adminBll.SearchThanhVien(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]object objEntity)
        {
            CommonMethods.WriteLog("===== Start Logging PUT");
            CommonMethods.WriteLog("Id" + id.ToString());
            CommonMethods.WriteLog("newEntity" + CommonMethods.SerializeToJSON(objEntity));
            CustomResult cusRes = new CustomResult();

            string strObjEntity = CommonMethods.SerializeObject(objEntity);
            ThanhVien newEntity = CommonMethods.DeserializeObject<ThanhVien>(strObjEntity);
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                ThanhVien editEntity = await Repo.GetSingle(id);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Menu này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    await RepoThanhVienMaster.UpdateWhere(s => s.Id == id, delegate (ThanhVien obj)
                    {
                        obj.IdMd5 = CommonMethods.GenerateIdMd5(id);
                        if(newEntity.TrangThai.HasValue)
                        {
                            obj.TrangThai = newEntity.TrangThai;
                        }
                        if (!string.IsNullOrEmpty(newEntity.DienThoai))
                        {
                            obj.DienThoai = newEntity.DienThoai;
                        }
                        if (!string.IsNullOrEmpty(newEntity.HoTen))
                        {
                            obj.HoTen = newEntity.HoTen;
                        }
                        if (!string.IsNullOrEmpty(newEntity.Email))
                        {
                            obj.Email = newEntity.Email;
                        }
                        if (newEntity.IdFileDaiDien.HasValue)
                        {
                            obj.IdFileDaiDien = newEntity.IdFileDaiDien;
                        }
                        if (!string.IsNullOrEmpty(newEntity.DiaChi))
                        {
                            obj.DiaChi = newEntity.DiaChi;
                        }
                        if (newEntity.IdTinhThanh.HasValue)
                        {
                            obj.IdTinhThanh = newEntity.IdTinhThanh;
                        }
                        if (newEntity.IdQuanHuyen.HasValue)
                        {
                            obj.IdQuanHuyen = newEntity.IdQuanHuyen;
                        }
                        if (newEntity.GioiTinh.HasValue)
                        {
                            obj.GioiTinh = newEntity.GioiTinh;
                        }
                        if (newEntity.NgaySinh.HasValue)
                        {
                            obj.NgaySinh = newEntity.NgaySinh;
                        }
                        if (newEntity.IdFileCmndTruoc.HasValue)
                        {
                            obj.IdFileCmndTruoc = newEntity.IdFileCmndTruoc;
                        }
                        if (newEntity.IdFileCmndSau.HasValue)
                        {
                            obj.IdFileCmndSau = newEntity.IdFileCmndSau;
                        }
                        if (!string.IsNullOrEmpty(newEntity.ListHinhAnhJsonPassport))
                        {
                            obj.ListHinhAnhJsonPassport = newEntity.ListHinhAnhJsonPassport;
                        }
                        if (!string.IsNullOrEmpty(newEntity.ListHinhAnh))
                        {
                            obj.ListHinhAnh = newEntity.ListHinhAnh;
                        }
                        if (newEntity.DaXacThucCmnd.HasValue)
                        {
                            obj.DaXacThucCmnd = newEntity.DaXacThucCmnd;
                        }
                        if (newEntity.DaXacThucPassport.HasValue)
                        {
                            obj.DaXacThucPassport = newEntity.DaXacThucPassport;
                        }
                        if (newEntity.DaXacNhanEmail.HasValue)
                        {
                            obj.DaXacNhanEmail = newEntity.DaXacNhanEmail;
                        }
                        if (newEntity.DaXacNhanDienThoai.HasValue)
                        {
                            obj.DaXacNhanDienThoai = newEntity.DaXacNhanDienThoai;
                        }
                        if (!string.IsNullOrEmpty(newEntity.UidThanhVien))
                        {
                            obj.UidThanhVien = newEntity.UidThanhVien;
                        }
                    });

                    cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    _adminBll.BuildListIdThanhVien();
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]object objEntity)
        {
            CommonMethods.WriteLog("===== Start Logging POST");
            CommonMethods.WriteLog("entity" + CommonMethods.SerializeToJSON(objEntity));
            CustomResult cusRes = new CustomResult();
            try
            {
                // When add new user, insert 3 table:
                // 1: ThanhVien => store user's info
                // 2: DangTinContact => store user's contact
                // 3: UserPoint => store user' point
                // 4: HistoryPoint
                // 5: NotificationSystem

                string strObjEntity = CommonMethods.SerializeObject(objEntity);
                ThanhVien entity = CommonMethods.DeserializeObject<ThanhVien>(strObjEntity);
                CommonMethods.WriteLog("===== DeserializeObject" + CommonMethods.SerializeToJSON(entity));
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                InterfaceParam paramCallTinhThanh = new InterfaceParam();
                InterfaceParam paramCallQuanHuyen = new InterfaceParam();

                paramCallTinhThanh.ids = entity.IdTinhThanh.ToString();
                paramCallQuanHuyen.ids = entity.IdQuanHuyen.ToString();

                // TenTinhThanh - TenQuanHuyen
                var TenTinhThanh = await ApiGetDal.SearchTinhThanh(paramCallTinhThanh);
                var TenQuanHuyen = await ApiGetDal.SearchQuanHuyen(paramCallQuanHuyen);

                entity.TenTinhThanh = TenTinhThanh;
                entity.TenQuanHuyen = TenQuanHuyen;
                entity.IdMd5 = CommonMethods.GenerateIdMd5(entity.Id);
                cusRes.IntResult = await RepoThanhVienMaster.Insert(entity);

                try
                {
                    var dangTinContact = RepoDangTinContact.Find(a => a.IdThanhVien == entity.Id).Result.FirstOrDefault();
                    if (dangTinContact == null)
                    {

                        dangTinContact = new DangTinContact();
                        dangTinContact.IdThanhVien = entity.Id;
                        dangTinContact.HoTen = entity.HoTen;
                        dangTinContact.DienThoai = entity.DienThoai;
                        dangTinContact.Email = entity.Email;
                        dangTinContact.IdTinhThanh = entity.IdTinhThanh;
                        dangTinContact.IdQuanHuyen = entity.IdQuanHuyen;
                        dangTinContact.DiaChi = entity.DiaChi;
                        dangTinContact.TrangThai = 1;
                        dangTinContact.IsDefault = true;

                        dangTinContact.TenTinhThanh = TenTinhThanh;
                        dangTinContact.TenQuanHuyen = TenQuanHuyen;
                        await RepoDangTinContactMaster.Insert(dangTinContact);
                    }
                }
                catch (Exception ex)
                {
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("Error: Insert RepoDangTinContactMaster", Variables.UrlFromClient + "-" + ex.Message, email);
                    }
                }

                string pointString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_GIFT_POINT_REGIST);
                int point = CommonMethods.ConvertToInt32(pointString);
                string pointRateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_VND);
                int pointRate = CommonMethods.ConvertToInt32(pointRateString);

                try
                {
                    var userPoint = RepoUserPoint.Find(a => a.IdThanhVien == entity.Id).Result.FirstOrDefault();
                    if (userPoint == null)
                    {
                        userPoint = new UserPoint();
                        userPoint.IdThanhVien = entity.Id;
                        userPoint.Point = point;
                        userPoint.PointTmp = 0;
                        userPoint.PointUsed = 0;
                        await RepoUserPointMaster.Insert(userPoint);
                    }
                }
                catch(Exception ex)
                {
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("Error: Insert RepoUserPointMaster", Variables.UrlFromClient + "-" + ex.Message, email);
                    }
                }

                try
                {
                    // insert vào bảng HistoryPoint của db raovat
                    HistoryPoint htp = new HistoryPoint()
                    {
                        IdThanhVien = entity.Id,
                        Point = point,
                        Type = (int)Variables.TypeUserPoint.GiftRegist,
                        Rate = pointRate,
                        NgayTao = DateTime.Now,
                        Note = "Đăng ký thành viên"
                    };
                    await RepoHistoryPoint.Insert(htp);
                }
                catch(Exception ex)
                {
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("Error: Insert RepoHistoryPoint", Variables.UrlFromClient + "-" + ex.Message, email);
                    }
                }

                try
                {
                    InterfaceParam paramNotify = new InterfaceParam();
                    paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                    paramNotify.idThanhVien = entity.Id;
                    paramNotify.ma = Variables.Constants.F001; // User Registration
                    paramNotify.noiDung = point.ToString();

                    CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                    await commonController.InsertNotification(paramNotify);
                }
                catch(Exception ex)
                {
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("Error: InsertNotification", Variables.UrlFromClient + "-" + ex.Message, email);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SyncThanhVien([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                if (interfaceParam.lstId.Count > 0)
                {
                    int i = 0;
                    string pointString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_GIFT_POINT_REGIST);
                    int pointGifted = CommonMethods.ConvertToInt32(pointString);
                    InterfaceParam paramCallTinhThanh = new InterfaceParam();
                    InterfaceParam paramCallQuanHuyen = new InterfaceParam();

                    foreach (var id in interfaceParam.lstId)
                    {
                        i = i + 1;
                        var thanhVien = Repo.Find(a => a.Id == id).Result.FirstOrDefault();
                        ThanhVien_DLX thanhVien_Ext = await ApiGetDal.SearchThanhVienNoToken(id.ToString());

                        // Not exists => insert full info
                        if (thanhVien == null)
                        {
                            thanhVien = new ThanhVien();
                            thanhVien.Id = id;
                            thanhVien.OriginalFrom = interfaceParam.originalFrom.Value;
                            thanhVien.TrangThai = CommonMethods.ConvertToInt32(interfaceParam.trangThai);
                            thanhVien.IdMd5 = CommonMethods.GenerateIdMd5(id);
                            thanhVien.HoTen = thanhVien_Ext.HoTen;
                            thanhVien.DienThoai = thanhVien_Ext.DienThoai;
                            thanhVien.Email = thanhVien_Ext.Email;
                            thanhVien.IdTinhThanh = thanhVien_Ext.IdTinhThanh;
                            thanhVien.IdQuanHuyen = thanhVien_Ext.IdQuanHuyen;
                            thanhVien.DiaChi = thanhVien_Ext.DiaChi;
                            thanhVien.GioiTinh = thanhVien_Ext.GioiTinh;
                            thanhVien.NgaySinh = thanhVien_Ext.NgaySinh;
                            thanhVien.IdFileDaiDien = thanhVien_Ext.IdFileDaiDien;

                            // Insert ThanhVien
                            await RepoThanhVienMaster.Insert(thanhVien);
                        }
                        else // Exists => update IdMd5 and DangTinContact
                        {
                            await RepoThanhVienMaster.UpdateWhere(s => s.Id == id, delegate (ThanhVien obj)
                            {
                                obj.IdMd5 = CommonMethods.GenerateIdMd5(id);
                                obj.HoTen = thanhVien_Ext.HoTen;
                                obj.DienThoai = thanhVien_Ext.DienThoai;
                                obj.Email = thanhVien_Ext.Email;
                                obj.IdTinhThanh = thanhVien_Ext.IdTinhThanh;
                                obj.IdQuanHuyen = thanhVien_Ext.IdQuanHuyen;
                                obj.DiaChi = thanhVien_Ext.DiaChi;
                                obj.GioiTinh = thanhVien_Ext.GioiTinh;
                                obj.NgaySinh = thanhVien_Ext.NgaySinh;
                                obj.IdFileDaiDien = thanhVien_Ext.IdFileDaiDien;
                            });
                        }

                        var dangTinContact = RepoDangTinContact.Find(a => a.IdThanhVien == id).Result.FirstOrDefault();
                        if(dangTinContact == null)
                        {
                            dangTinContact = new DangTinContact();
                            dangTinContact.IdThanhVien = id;
                            dangTinContact.HoTen = thanhVien_Ext.HoTen;
                            dangTinContact.DienThoai = thanhVien_Ext.DienThoai;
                            dangTinContact.Email = thanhVien_Ext.Email;
                            dangTinContact.IdTinhThanh = thanhVien_Ext.IdTinhThanh;
                            dangTinContact.IdQuanHuyen = thanhVien_Ext.IdQuanHuyen;
                            dangTinContact.DiaChi = thanhVien_Ext.DiaChi;
                            dangTinContact.TrangThai = 1;
                            dangTinContact.IsDefault = true;

                            paramCallTinhThanh.ids = dangTinContact.IdTinhThanh.ToString();
                            paramCallQuanHuyen.ids = dangTinContact.IdQuanHuyen.ToString();
                            dangTinContact.TenTinhThanh = await ApiGetDal.SearchTinhThanh(paramCallTinhThanh);
                            dangTinContact.TenQuanHuyen = await ApiGetDal.SearchQuanHuyen(paramCallQuanHuyen);
                            await RepoDangTinContactMaster.Insert(dangTinContact);
                        }

                        var userPoint = RepoUserPoint.Find(a => a.IdThanhVien == id).Result.FirstOrDefault();
                        if(userPoint == null)
                        {
                            userPoint = new UserPoint();
                            userPoint.IdThanhVien = id;
                            userPoint.Point = pointGifted;
                            userPoint.PointTmp = 0;
                            userPoint.PointUsed = 0;
                            await RepoUserPointMaster.Insert(userPoint);
                        }
                        else
                        {
                            if(!userPoint.Point.HasValue || userPoint.Point.Value == 0)
                            {
                                await RepoUserPointMaster.UpdateWhere(s => s.IdThanhVien == id, delegate (UserPoint obj)
                                {
                                    obj.Point = pointGifted;
                                    obj.PointTmp = 0;
                                    obj.PointUsed = 0;
                                });
                            }
                        }
                    }

                    cusRes.IntResult = i;
                }
                else
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                string trangThaiString = CommonMethods.ConvertToString(interfaceParam.trangThai);
                Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);

                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                cusRes.IntResult = await RepoThanhVienMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (ThanhVien obj)
                {
                    obj.TrangThai = trangThai;
                });

                _adminBll.BuildListIdThanhVien();
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        private string IsCheckedData(ThanhVien entity, ref string idControl)
        {
            string HoTen = string.Empty;
            string DienThoai = string.Empty;

            string langKey = string.Empty;
            string langName = string.Empty;

            //HoTen = CommonMethods.FilterTen(entity.HoTen).Trim();
            //DienThoai = entity.DienThoai;
            //if (string.IsNullOrEmpty(DienThoai))
            //{
            //    idControl = "txtDienThoai" + langKey;
            //    return "Điện thoại " + langName + " chưa nhập";
            //}

            //if (string.IsNullOrEmpty(HoTen))
            //{
            //    idControl = "txtHoTen" + langKey;
            //    return "Họ tên " + langName + " chưa nhập";
            //}

            //if (HoTen.Length < Variables.TITLE_MINLENGTH)
            //{
            //    idControl = "txtHoTen" + langKey;
            //    return "Họ tên " + langName + " phải nhiều hơn " + Variables.TITLE_MINLENGTH + "ký tự";
            //}

            //if (HoTen.Length > Variables.TITLE_MAXLENGTH)
            //{
            //    idControl = "txtHoTen" + langKey;
            //    return "Họ tên " + langName + " phải ít hơn " + Variables.TITLE_MAXLENGTH + "ký tự";
            //}

            return string.Empty;
        }

        private async Task<string> BuildTuKhoaTimKiemThanhVien(ThanhVien thanhVien)
        {
            string res = string.Empty;

            if (thanhVien != null)
            {
                //res += CommonMethods.ConvertUnicodeToASCII(thanhVien.HoTen);
            }

            return res;
        }

        #endregion
    }
}
