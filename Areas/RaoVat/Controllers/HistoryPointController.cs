﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class HistoryPointController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;

        public HistoryPointController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                // handle params
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstIdThanhVien = CommonMethods.SplitStringToInt64(interfaceParam.idsThanhVien);

                // search and get data
                IEnumerable<HistoryPoint> data = await _adminBll.SearchHistoryPoint(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }
    }
}
