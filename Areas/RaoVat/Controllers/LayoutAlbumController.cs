﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class LayoutAlbumController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<LayoutAlbum> _repo;
        public EntityBaseRepositoryRaoVat<LayoutAlbum> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<LayoutAlbum>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<LayoutAlbum> _repoLayoutAlbumMaster;
        public EntityBaseRepositoryRaoVat<LayoutAlbum> RepoLayoutAlbumMaster { get => _repoLayoutAlbumMaster == null ? _repo = new EntityBaseRepositoryRaoVat<LayoutAlbum>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoLayoutAlbumMaster; set => _repoLayoutAlbumMaster = value; }

        public LayoutAlbumController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) : null;

                // Search & Get data
                IEnumerable<LayoutAlbum> data = await _adminBll.SearchLayoutAlbum(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] LayoutAlbum entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    cusRes.DataResult = await RepoLayoutAlbumMaster.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] LayoutAlbum newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    LayoutAlbum editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;
                        cusRes.IntResult = await RepoLayoutAlbumMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoLayoutAlbumMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (LayoutAlbum obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}
