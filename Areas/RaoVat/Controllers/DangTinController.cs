﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class DangTinController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;

        // Read
        private EntityBaseRepositoryRaoVat<DangTin> _repo;
        public EntityBaseRepositoryRaoVat<DangTin> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<DangTin>(Request) : _repo; set => _repo = value; }

        // Write
        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinMaster;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinMaster { get => _repoDangTinMaster == null ? _repoDangTinMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinMaster; set => _repoDangTinMaster = value; }

        // History
        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinDuyet> _repoHistoryDangTinDuyet;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinDuyet> RepoHistoryDangTinDuyet { get => _repoHistoryDangTinDuyet == null ? _repoHistoryDangTinDuyet = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinDuyet>(Request) : _repoHistoryDangTinDuyet; set => _repoHistoryDangTinDuyet = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>() : _repoDangTinContact; set => _repoDangTinContact = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>() : _repoThanhVien; set => _repoThanhVien = value; }

        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUser;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUser { get => _repoStatisticActivitiesUser == null ? _repoStatisticActivitiesUser = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request) : _repoStatisticActivitiesUser; set => _repoStatisticActivitiesUser = value; }
        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUserMaster;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUserMaster { get => _repoStatisticActivitiesUserMaster == null ? _repoStatisticActivitiesUserMaster = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoStatisticActivitiesUserMaster; set => _repoStatisticActivitiesUserMaster = value; }

        public DangTinController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                if (!string.IsNullOrEmpty(interfaceParam.isDuyetTin))
                {
                    interfaceParam.isDuyet = CommonMethods.ConvertToBoolean(interfaceParam.isDuyetTin);
                }

                interfaceParam.typeInt = CommonMethods.ConvertToInt32(interfaceParam.type);

                // Search & Get data
                IEnumerable<DangTin> data = await _adminBll.SearchDangTin(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();
            HistoryDangTinDuyet historyDangTinDuyet = new HistoryDangTinDuyet();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string idControl = string.Empty;
                string msgCheckedData = IsCheckedData(newEntity, ref idControl);
                if (!string.IsNullOrEmpty(msgCheckedData))
                {
                    cusRes.GotoLink = idControl;
                    cusRes.SetException(null, msgCheckedData);
                    return Response.OkObjectResult(cusRes);
                }

                long idAdmin = Request.GetTokenIdAdmin();
                string tenAdmin = Request.GetTokenHoTenAdmin();

                DangTin editEntity = await Repo.GetSingle(id);
                historyDangTinDuyet.OldListJson = CommonMethods.SerializeToJSON(editEntity);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Đăng tin này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                    if (editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value == idAdmin)
                    {
                        var isDuyetEditEntity = (editEntity.IsDuyet.HasValue && editEntity.IsDuyet.Value);

                        // Hanld FormData
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        if (string.IsNullOrEmpty(newEntity.RewriteUrl))
                        {
                            newEntity.RewriteUrl = CommonMethods.GetRewriteString(newEntity.TieuDe);
                        }

                        // isDuyetEditEntity => for DeleteIndex and cache in case of reported when IsDuyet = true
                        if ((newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value) || isDuyetEditEntity)
                        {
                            ElasticSearchBLL elasticSearchBLL = new ElasticSearchBLL();

                            // send notification
                            if (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value)
                            {
                                newEntity.NgayDuyet = DateTime.Now;
                                newEntity.NgayUp = DateTime.Now;
                                newEntity.TenAdminDangKyDuyet = tenAdmin;
                                // Successful => Add to notification
                                InterfaceParam paramNotify = new InterfaceParam();
                                paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                                paramNotify.idThanhVien = newEntity.IdThanhVien;
                                paramNotify.ma = Variables.Constants.F004; // Duyệt tin thành công
                                paramNotify.noiDung = newEntity.TieuDe;
                                paramNotify.linkId = newEntity.Id.ToString();

                                commonMethodsRaoVat.InsertNotification(paramNotify);

                                // ReIndex
                                commonMethodsRaoVat.ReIndexDangTin(newEntity);
                            }
                            else
                            {
                                newEntity.IdAdminTuChoi = idAdmin;
                                newEntity.TenAdminTuChoi = tenAdmin;
                                newEntity.NgayDuyet = new DateTime(1970,1,1);
                                // Fail => Add to notification reason
                                InterfaceParam paramNotify = new InterfaceParam();
                                paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                                paramNotify.idThanhVien = newEntity.IdThanhVien;
                                paramNotify.ma = Variables.Constants.F005; // Duyệt tin thất bại
                                paramNotify.noiDung = newEntity.TieuDe;
                                paramNotify.noiDung1 = newEntity.GhiChu;
                                paramNotify.linkId = newEntity.Id.ToString();

                                commonMethodsRaoVat.InsertNotification(paramNotify);

                                // Delete Index
                                commonMethodsRaoVat.DeleteIndexDangTin(newEntity);
                            }

                            // Clear Cache
                            MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                            cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinByParams, true, false);
                        }
                        else // Deny directly
                        {
                            newEntity.IdAdminTuChoi = idAdmin;
                            newEntity.TenAdminTuChoi = tenAdmin;
                            newEntity.NgayDuyet = new DateTime(1970, 1, 1);
                            // Fail => Add to notification reason
                            InterfaceParam paramNotify = new InterfaceParam();
                            paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                            paramNotify.idThanhVien = newEntity.IdThanhVien;
                            paramNotify.ma = Variables.Constants.F005; // Duyệt tin thất bại
                            paramNotify.noiDung = newEntity.TieuDe;
                            paramNotify.noiDung1 = newEntity.GhiChu;
                            paramNotify.linkId = newEntity.Id.ToString();

                            commonMethodsRaoVat.InsertNotification(paramNotify);
                        }

                        newEntity.AdminNgayCapNhat = DateTime.Now;
                        cusRes.IntResult = await RepoDangTinMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);

                        #region Statistic activities user

                        var countDangTin = Repo.Find(a => a.IdThanhVien == newEntity.IdThanhVien && a.IsDuyet == true
                                            && a.NgayTao.Value.Year == newEntity.NgayTao.Value.Year
                                            && a.NgayTao.Value.Month == newEntity.NgayTao.Value.Month).Result.Count();

                        var isExistThanhVienInActivitiesUser = RepoStatisticActivitiesUser.Find(a => a.IdThanhVien == newEntity.IdThanhVien
                                                && a.Year == newEntity.NgayTao.Value.Year
                                                && a.Month == newEntity.NgayTao.Value.Month).Result.Count();

                        if (isExistThanhVienInActivitiesUser > 0)
                        {
                            // if exists => update
                            RepoStatisticActivitiesUserMaster.UpdateWhereWithoutClearCache(a => a.IdThanhVien == newEntity.IdThanhVien
                                                            && a.Year == newEntity.NgayTao.Value.Year
                                                            && a.Month == newEntity.NgayTao.Value.Month, delegate (StatisticActivitiesUser obj)
                                                            {
                                                                obj.CountDangTin = countDangTin;
                                                            });
                        }
                        else
                        {
                            // if not exist => create
                            StatisticActivitiesUser statisticActivitiesUser = new StatisticActivitiesUser();
                            statisticActivitiesUser.IdThanhVien = newEntity.IdThanhVien;
                            statisticActivitiesUser.Year = newEntity.NgayTao.Value.Year;
                            statisticActivitiesUser.Month = newEntity.NgayTao.Value.Month;
                            statisticActivitiesUser.CountDangTin = countDangTin;
                            statisticActivitiesUser.CountUpTin = 0;
                            statisticActivitiesUser.CountLienHe = 0;

                            RepoStatisticActivitiesUserMaster.InsertReturnEntity(statisticActivitiesUser);
                        }

                        #endregion

                        #region Insert History

                        historyDangTinDuyet.IdAdmin = idAdmin;
                        historyDangTinDuyet.NewListJson = CommonMethods.SerializeToJSON(newEntity);
                        RepoHistoryDangTinDuyet.InsertWithouClearCache(historyDangTinDuyet);

                        #endregion
                    }
                    else
                    {
                        cusRes.SetException(null, Messages.ERR_012);
                        return Response.OkObjectResult(cusRes);
                    }
                }

            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> RegistAdminApprove(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            HistoryDangTinDuyet historyDangTinDuyet = new HistoryDangTinDuyet();

            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                if (interfaceParam.lstId.Count <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                long idAdmin = Request.GetTokenIdAdmin();
                var tenAdmin = Request.GetTokenHoTenAdmin();

                foreach (var id in interfaceParam.lstId)
                {
                    DangTin editEntity = await Repo.GetSingle(id);
                    historyDangTinDuyet.OldListJson = CommonMethods.SerializeToJSON(editEntity);

                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Đăng tin này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        if (!editEntity.IdAdminDangKyDuyet.HasValue || editEntity.IdAdminDangKyDuyet.Value <= 0)
                        {
                            // Hanld FormData
                            cusRes.IntResult = await RepoDangTinMaster.UpdateWhere(s => id == s.Id, delegate (DangTin obj)
                            {
                                obj.IdAdminDangKyDuyet = idAdmin;
                                obj.TenAdminDangKyDuyet = tenAdmin;
                            });

                            var res = await Repo.Find(s => s.Id == id);
                            cusRes.DataResult = res;

                            // Insert History
                            historyDangTinDuyet.IdAdmin = idAdmin;
                            historyDangTinDuyet.NewListJson = CommonMethods.SerializeToJSON(res.FirstOrDefault());
                            await RepoHistoryDangTinDuyet.Insert(historyDangTinDuyet);
                        }
                        else
                        {
                            cusRes.SetException(null, Messages.ERR_013);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> RemoveAdminApprove(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            HistoryDangTinDuyet historyDangTinDuyet = new HistoryDangTinDuyet();

            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                if (interfaceParam.lstId.Count <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                long idAdmin = Request.GetTokenIdAdmin();

                foreach (var id in interfaceParam.lstId)
                {
                    DangTin editEntity = await Repo.GetSingle(id);
                    historyDangTinDuyet.OldListJson = CommonMethods.SerializeToJSON(editEntity);

                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Đăng tin này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        if (editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value == idAdmin)
                        {
                            // Hanld FormData
                            // Tránh tình trạng newEntity sai id trên đường dẫn
                            cusRes.IntResult = await RepoDangTinMaster.UpdateWhere(s => id == s.Id, delegate (DangTin obj)
                            {
                                obj.IdAdminDangKyDuyet = -1;
                                obj.TenAdminDangKyDuyet = string.Empty;
                            });

                            var res = await Repo.Find(s => s.Id == id);
                            cusRes.DataResult = res;

                            // Insert History
                            historyDangTinDuyet.IdAdmin = idAdmin;
                            historyDangTinDuyet.NewListJson = CommonMethods.SerializeToJSON(res.FirstOrDefault());
                            await RepoHistoryDangTinDuyet.Insert(historyDangTinDuyet);
                        }
                        else
                        {
                            cusRes.SetException(null, Messages.ERR_014);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                string trangThaiString = CommonMethods.ConvertToString(interfaceParam.trangThai);
                Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);

                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                cusRes.IntResult = await RepoDangTinMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (DangTin obj)
                {
                    obj.TrangThai = trangThai;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdatePinTop(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                cusRes.IntResult = await RepoDangTinMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (DangTin obj)
                {
                    obj.IsPinTop = interfaceParam.isPinTop;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ReIndexElasticSearch(long id, [FromBody] DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                DangTin editEntity = await Repo.GetSingle(id);
                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Đăng tin này"));
                    return Response.OkObjectResult(cusRes);
                }

                ElasticSearchBLL elasticSearchBLL = new ElasticSearchBLL();
                if (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value)
                {
                    // ReIndex
                    CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                    commonMethodsRaoVat.ReIndexDangTin(newEntity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ReIndexESAll()
        {
            CustomResult cusRes = new CustomResult();

            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();

            try
            {
                var getAllDangTin = Repo.Find(a => a.TrangThai == 1 && a.IsDuyet == true).Result;
                foreach (var item in getAllDangTin)
                {
                    // ReIndex
                    commonMethodsRaoVat.ReIndexDangTin(item);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> DeleteESAll()
        {
            CustomResult cusRes = new CustomResult();

            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();

            try
            {
                var getAllDangTin = Repo.Find(a => (a.TrangThai == 2 || a.TrangThai == 3 || a.IsDuyet == false) && a.IdThanhVien != 62).Result;
                foreach (var item in getAllDangTin)
                {
                    // ReIndex
                    commonMethodsRaoVat.DeleteIndexDangTin(item);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        private string IsCheckedData(DangTin data, ref string idControl)
        {
            string GhiChu = string.Empty;

            string langKey = string.Empty;
            string langName = string.Empty;

            string txtGhiChu = "txtGhiChu";

            if (data.IsDuyet.HasValue && !data.IsDuyet.Value && string.IsNullOrEmpty(data.GhiChu))
            {
                idControl = txtGhiChu + langKey;
                return "Ghi chú " + langName + " chưa nhập";
            }

            return string.Empty;
        }

        #endregion
    }
}