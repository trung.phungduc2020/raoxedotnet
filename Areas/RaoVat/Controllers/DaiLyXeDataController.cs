﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using raoVatApi.Response;
using Repository;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Route(Variables.C_ApiRouting_2)]
    public class DaiLyXeDataController : Controller
    {
        ApiGetDal _apiGetDal = new ApiGetDal();
        public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        [Authorize]
        public async Task<IActionResult> SearchThanhVien(InterfaceParam interfaceParam)
        {
            interfaceParam.token = Request.GetTokenFromHeader();

            CustomResult customResult = await ApiGetDal.SearchThanhVien(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        [AllowAnonymous]
        public async Task<IActionResult> IsExistPhoneNumberThanhVien(InterfaceParam interfaceParam)
        {
            CustomResult customResult = await ApiGetDal.IsExistPhoneNumberThanhVien(interfaceParam);

            return Response.OkObjectResult(customResult);
        }


        [AllowAnonymous]
        public async Task<IActionResult> IsExistDienThoaiThanhVienSelfCheck(InterfaceParam interfaceParam)
        {
            CustomResult customResult = await ApiGetDal.IsExistDienThoaiThanhVienSelfCheck(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        [AllowAnonymous]
        public async Task<IActionResult> IsExistEmailThanhVienSelfCheck(InterfaceParam interfaceParam)
        {
            CustomResult customResult = await ApiGetDal.IsExistEmailThanhVienSelfCheck(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        [Authorize]
        public async Task<IActionResult> UpdateDaXacThucCmnd(InterfaceParam interfaceParam)
        {
            interfaceParam.token = Request.GetTokenFromHeader();

            CustomResult customResult = await ApiGetDal.UpdateDaXacThucCmnd(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        [Authorize]
        public async Task<IActionResult> UpdateDaXacThucPassport(InterfaceParam interfaceParam)
        {
            interfaceParam.token = Request.GetTokenFromHeader();

            CustomResult customResult = await ApiGetDal.UpdateDaXacThucPassport(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

        [Authorize]
        public async Task<IActionResult> UpdateListHinhAnhJsonPassport(InterfaceParam interfaceParam)
        {
            interfaceParam.token = Request.GetTokenFromHeader();

            CustomResult customResult = await ApiGetDal.UpdateListHinhAnhJsonPassport(interfaceParam);

            return Response.OkObjectResult(customResult);
        }

    }
}