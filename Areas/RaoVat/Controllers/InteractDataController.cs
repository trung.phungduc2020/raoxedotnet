﻿using Common;
using GmailApi;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json.Linq;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using Renci.SshNet;
using Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;
using InterfaceBLL = raoVatApi.ModelsRaoVat.InterfaceBLL;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize]
    [Route(Variables.C_ApiVersioning_InteractData_2)]
    public class InteractDataController : Controller
    {
        private ApiGetDal _apiGetDal;
        public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        #region HistoryRaovat

        private EntityBaseRepositoryHistoryRaoVat<HistoryPoint> _repoHistoryPoint;
        public EntityBaseRepositoryHistoryRaoVat<HistoryPoint> RepoHistoryPoint { get => _repoHistoryPoint == null ? _repoHistoryPoint = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>() : _repoHistoryPoint; set => _repoHistoryPoint = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryProfile> _repoProfileHistory;
        public EntityBaseRepositoryHistoryRaoVat<HistoryProfile> RepoProfileHistory { get => _repoProfileHistory == null ? _repoProfileHistory = new EntityBaseRepositoryHistoryRaoVat<HistoryProfile>() : _repoProfileHistory; set => _repoProfileHistory = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike> _repoHistoryDangTinLikeMaster;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike> RepoHistoryDangTinLikeMaster { get => _repoHistoryDangTinLikeMaster == null ? _repoHistoryDangTinLikeMaster = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinLike>() : _repoHistoryDangTinLikeMaster; set => _repoHistoryDangTinLikeMaster = value; }

        private EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite> _repoHistoryDangTinFavoriteMaster;
        public EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite> RepoHistoryDangTinFavoriteMaster { get => _repoHistoryDangTinFavoriteMaster == null ? _repoHistoryDangTinFavoriteMaster = new EntityBaseRepositoryHistoryRaoVat<HistoryDangTinFavorite>() : _repoHistoryDangTinFavoriteMaster; set => _repoHistoryDangTinFavoriteMaster = value; }

        #endregion

        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinRepositoryRaoVat;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinRepositoryRaoVat { get => _repoDangTinRepositoryRaoVat == null ? _repoDangTinRepositoryRaoVat = new EntityBaseRepositoryRaoVat<DangTin>(Request) : _repoDangTinRepositoryRaoVat; set => _repoDangTinRepositoryRaoVat = value; }
        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinRepositoryRaoVatMaster;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinRepositoryRaoVatMaster { get => _repoDangTinRepositoryRaoVatMaster == null ? _repoDangTinRepositoryRaoVatMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinRepositoryRaoVatMaster; set => _repoDangTinRepositoryRaoVatMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTinTraPhi> _repoTinTraPhiRepositoryRaoVat;
        public EntityBaseRepositoryRaoVat<DangTinTraPhi> RepoTinTraPhiRepositoryRaoVat { get => _repoTinTraPhiRepositoryRaoVat == null ? _repoTinTraPhiRepositoryRaoVat = new EntityBaseRepositoryRaoVat<DangTinTraPhi>(Request) : _repoTinTraPhiRepositoryRaoVat; set => _repoTinTraPhiRepositoryRaoVat = value; }
        private EntityBaseRepositoryRaoVat<DangTinTraPhi> _repoTinTraPhiRepositoryRaoVatMaster;
        public EntityBaseRepositoryRaoVat<DangTinTraPhi> RepoTinTraPhiRepositoryRaoVatMaster { get => _repoTinTraPhiRepositoryRaoVatMaster == null ? _repoTinTraPhiRepositoryRaoVatMaster = new EntityBaseRepositoryRaoVat<DangTinTraPhi>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoTinTraPhiRepositoryRaoVatMaster; set => _repoTinTraPhiRepositoryRaoVatMaster = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPoint;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPoint { get => _repoUserPoint == null ? _repoUserPoint = new EntityBaseRepositoryRaoVat<UserPoint>(Request) : _repoUserPoint; set => _repoUserPoint = value; }
        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<UserDevice> _repoUserDevice;
        public EntityBaseRepositoryRaoVat<UserDevice> RepoUserDevice { get => _repoUserDevice == null ? _repoUserDevice = new EntityBaseRepositoryRaoVat<UserDevice>() : _repoUserDevice; set => _repoUserDevice = value; }
        private EntityBaseRepositoryRaoVat<UserDevice> _repoUserDeviceMaster;
        public EntityBaseRepositoryRaoVat<UserDevice> RepoUserDeviceMaster { get => _repoUserDeviceMaster == null ? _repoUserDeviceMaster = new EntityBaseRepositoryRaoVat<UserDevice>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserDeviceMaster; set => _repoUserDeviceMaster = value; }

        private EntityBaseRepositoryRaoVat<MaXacNhan> _repoMaXacNhan;
        public EntityBaseRepositoryRaoVat<MaXacNhan> RepoMaXacNhan { get => _repoMaXacNhan == null ? _repoMaXacNhan = new EntityBaseRepositoryRaoVat<MaXacNhan>() : _repoMaXacNhan; set => _repoMaXacNhan = value; }
        private EntityBaseRepositoryRaoVat<MaXacNhan> _repoMaXacNhanMaster;
        public EntityBaseRepositoryRaoVat<MaXacNhan> RepoMaXacNhanMaster { get => _repoMaXacNhanMaster == null ? _repoMaXacNhanMaster = new EntityBaseRepositoryRaoVat<MaXacNhan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMaXacNhanMaster; set => _repoMaXacNhanMaster = value; }


        private EntityBaseRepositoryRaoVat<DangTinRating> _repoDangTinRating;
        public EntityBaseRepositoryRaoVat<DangTinRating> RepoDangTinRating { get => _repoDangTinRating == null ? _repoDangTinRating = new EntityBaseRepositoryRaoVat<DangTinRating>() : _repoDangTinRating; set => _repoDangTinRating = value; }
        private EntityBaseRepositoryRaoVat<DangTinRating> _repoDangTinRatingMaster;
        public EntityBaseRepositoryRaoVat<DangTinRating> RepoDangTinRatingMaster { get => _repoDangTinRatingMaster == null ? _repoDangTinRatingMaster = new EntityBaseRepositoryRaoVat<DangTinRating>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinRatingMaster; set => _repoDangTinRatingMaster = value; }

        private EntityBaseRepositoryRaoVat<NotificationUser> _repoNotificationUser;
        public EntityBaseRepositoryRaoVat<NotificationUser> RepoNotificationUser { get => _repoNotificationUser == null ? _repoNotificationUser = new EntityBaseRepositoryRaoVat<NotificationUser>() : _repoNotificationUser; set => _repoNotificationUser = value; }
        private EntityBaseRepositoryRaoVat<NotificationUser> _repoNotificationUserMaster;
        public EntityBaseRepositoryRaoVat<NotificationUser> RepoNotificationUserMaster { get => _repoNotificationUserMaster == null ? _repoNotificationUserMaster = new EntityBaseRepositoryRaoVat<NotificationUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoNotificationUserMaster; set => _repoNotificationUserMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTinViolation> _repoDangTinViolationMaster;
        public EntityBaseRepositoryRaoVat<DangTinViolation> RepoDangTinViolationMaster { get => _repoDangTinViolationMaster == null ? _repoDangTinViolationMaster = new EntityBaseRepositoryRaoVat<DangTinViolation>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinViolationMaster; set => _repoDangTinViolationMaster = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>() : _repoThanhVien; set => _repoThanhVien = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>() : _repoDangTinContact; set => _repoDangTinContact = value; }
        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContactMaster;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContactMaster { get => _repoDangTinContactMaster == null ? _repoDangTinContactMaster = new EntityBaseRepositoryRaoVat<DangTinContact>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinContactMaster; set => _repoDangTinContactMaster = value; }

        #region MarketingPlan

        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMarketingPlan;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMarketingPlan { get => _repoMarketingPlan == null ? _repoMarketingPlan = new EntityBaseRepositoryRaoVat<MarketingPlan>() : _repoMarketingPlan; set => _repoMarketingPlan = value; }
        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMarketingPlanMaster;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMarketingPlanMaster { get => _repoMarketingPlanMaster == null ? _repoMarketingPlanMaster = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingPlanMaster; set => _repoMarketingPlanMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCode;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCode { get => _repoMarketingCode == null ? _repoMarketingCode = new EntityBaseRepositoryRaoVat<MarketingCode>() : _repoMarketingCode; set => _repoMarketingCode = value; }
        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCodeMaster;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCodeMaster { get => _repoMarketingCodeMaster == null ? _repoMarketingCodeMaster = new EntityBaseRepositoryRaoVat<MarketingCode>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingCodeMaster; set => _repoMarketingCodeMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUser;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUser { get => _repoMarketingUser == null ? _repoMarketingUser = new EntityBaseRepositoryRaoVat<MarketingUser>() : _repoMarketingUser; set => _repoMarketingUser = value; }
        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUserMaster;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUserMaster { get => _repoMarketingUserMaster == null ? _repoMarketingUserMaster = new EntityBaseRepositoryRaoVat<MarketingUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingUserMaster; set => _repoMarketingUserMaster = value; }

        #endregion
        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUser;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUser { get => _repoStatisticActivitiesUser == null ? _repoStatisticActivitiesUser = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>() : _repoStatisticActivitiesUser; set => _repoStatisticActivitiesUser = value; }

        private EntityBaseRepositoryRaoVat<StatisticActivitiesUser> _repoStatisticActivitiesUserMaster;
        public EntityBaseRepositoryRaoVat<StatisticActivitiesUser> RepoStatisticActivitiesUserMaster { get => _repoStatisticActivitiesUserMaster == null ? _repoStatisticActivitiesUserMaster = new EntityBaseRepositoryRaoVat<StatisticActivitiesUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoStatisticActivitiesUserMaster; set => _repoStatisticActivitiesUserMaster = value; }

        private InterfaceBLL _bll;
        private IDistributedCache _cache;
        private AdminBll _adminBll;

        public InteractDataController(IDistributedCache cache)
        {
            this._cache = cache;
            this._bll = new InterfaceBLL(Request);
            this._adminBll = new AdminBll();
        }

        #region "DangTin"

        [HttpGet]
        public async Task<IActionResult> SearchDangTin([FromQuery]InterfaceParam interfaceParam)
        {
            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin Start");
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 20;
                }
                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }

                // Search & Get data
                IEnumerable<DangTin> data = await _bll.SearchDangTinByIdThanhVien(interfaceParam);

                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-SearchDangTin End");
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> CreateDangTin([FromBody]DangTin entity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                var idThanhVien = Request.GetTokenIdThanhVien();
                entity.NgayUp = DateTime.Now;
                entity.TuKhoaTimKiem = BuildTuKhoaTimKiemTinDang(entity);

                var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                var kpiAutoDuyet = string.IsNullOrEmpty(thanhVien.AllowKpiRule) ? new string[] { } : thanhVien.AllowKpiRule.Split(",");
                if (kpiAutoDuyet.Contains(((int)Variables.KpiAutoType.AutoDuyet).ToString()))
                {
                    entity.IsDuyet = true;
                    entity.GhiChu = "AutoDuyet";
                }
                else
                {
                    entity.IsDuyet = false;
                }

                entity.RewriteUrl = CommonMethods.GetRewriteString(entity.TieuDe);

                // Add contact when contact not exists
                if (!entity.IdDangTinContact.HasValue || entity.IdDangTinContact.Value <= 0)
                {
                    var insertDangTinContact = await RepoDangTinContactMaster.InsertReturnEntity(entity.DangTinContact_Ext);
                    entity.IdDangTinContact = insertDangTinContact.FirstOrDefault().Id;
                }

                // When create => Clear cache to update new (only visible)
                // Clear Cache 
                MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinByParams, true, false);

                // Notify Admin
                var body = "[" + thanhVien.HoTen + "] - " + entity.TieuDe;
                NotificationRequest.sendNotificationDuyetTinToGroupAdmin(Messages.NOTI_NEW_TIEUDE_CHOXETDUYET, body, entity.Id.ToString());

                cusRes.DataResult = await RepoDangTinRepositoryRaoVatMaster.InsertReturnEntity(entity);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> CreateDangTin_v1([FromBody]DangTin entity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                var idThanhVien = Request.GetTokenIdThanhVien();
                entity.NgayUp = DateTime.Now;
                entity.TuKhoaTimKiem = BuildTuKhoaTimKiemTinDang(entity);

                var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                var kpiAutoDuyet = string.IsNullOrEmpty(thanhVien.AllowKpiRule) ? new string[] { } : thanhVien.AllowKpiRule.Split(",");
                if (kpiAutoDuyet.Contains(((int)Variables.KpiAutoType.AutoDuyet).ToString()))
                {
                    entity.IsDuyet = true;
                    entity.GhiChu = "AutoDuyet";
                }
                else
                {
                    entity.IsDuyet = false;
                }

                entity.RewriteUrl = CommonMethods.GetRewriteString(entity.TieuDe);

                // Add contact when contact not exists
                if (!entity.IdDangTinContact.HasValue || entity.IdDangTinContact.Value <= 0)
                {
                    var insertDangTinContact = await RepoDangTinContactMaster.InsertReturnEntity(entity.DangTinContact_Ext);
                    entity.IdDangTinContact = insertDangTinContact.FirstOrDefault().Id;
                }

                // Handle images
                if(entity.ListHinhAnh.Count > 0)
                {
                    var token = Request.GetTokenFromHeader();
                    var infoImages = await ApiGetDal.InsertListUrlHinhAnh(entity.TieuDe, entity.ListHinhAnh, token);
                    entity.TongImage = infoImages.Total;
                    entity.ListHinhAnhJson = infoImages.ListId;
                }
                else
                {
                    entity.TongImage = 0;
                    entity.ListHinhAnhJson = string.Empty;
                }

                cusRes.DataResult = await RepoDangTinRepositoryRaoVatMaster.InsertReturnEntity(entity);

                // When create => Clear cache to update new (only visible)
                // Clear Cache
                MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinByParams, true, false);

                // Notify Admin
                var body = "[" + thanhVien.HoTen + "] - " + entity.TieuDe;
                NotificationRequest.sendNotificationDuyetTinToGroupAdmin(Messages.NOTI_NEW_TIEUDE_CHOXETDUYET, body, entity.Id.ToString());
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDangTin(int id, [FromBody]DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                DangTin editEntity = await RepoDangTinRepositoryRaoVat.GetSingle(id);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                // If admin doen't approve => member cannot edit
                else if ((editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value > 0)
                    && (!editEntity.IsDuyet.HasValue || !editEntity.IsDuyet.Value))
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_011, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;

                    // UpdateAfterAutoClone can not update Null Value
                    newEntity.IdAdminDangKyDuyet = -1;
                    newEntity.TenAdminDangKyDuyet = string.Empty;
                    newEntity.IdAdminTuChoi = -1;
                    newEntity.TenAdminTuChoi = string.Empty;
                    newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiemTinDang(newEntity);
                    newEntity.RewriteUrl = CommonMethods.GetRewriteString(newEntity.TieuDe);
                    newEntity.NgayUp = DateTime.Now;

                    var idThanhVien = Request.GetTokenIdThanhVien();
                    var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                    var kpiAutoDuyet = string.IsNullOrEmpty(thanhVien.AllowKpiRule) ? new string[] { } : thanhVien.AllowKpiRule.Split(",");
                    if (kpiAutoDuyet.Contains(((int)Variables.KpiAutoType.AutoDuyet).ToString()))
                    {
                        newEntity.IsDuyet = true;
                        newEntity.IsAutoDuyet = true;
                        newEntity.GhiChu = "AutoDuyet";
                    }
                    else
                    {
                        newEntity.IsDuyet = false;
                    }

                    // Is about to delete
                    if (newEntity.IsUpTin.HasValue && newEntity.IsUpTin.Value)
                    {
                        newEntity.NgayUp = DateTime.Now;
                        newEntity.IsDuyet = editEntity.IsDuyet;

                        // update statistic
                        var isExistThanhVienInActivitiesUser = RepoStatisticActivitiesUser.Find(a => a.IdThanhVien == idThanhVien
                                                && a.Year == DateTime.Now.Year
                                                && a.Month == DateTime.Now.Month).Result.Count();
                        if (isExistThanhVienInActivitiesUser > 0)
                        {
                            // if exists => update
                            RepoStatisticActivitiesUserMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien
                                                            && a.Year == DateTime.Now.Year
                                                            && a.Month == DateTime.Now.Month, delegate (StatisticActivitiesUser obj)
                                                            {
                                                                obj.CountUpTin = (obj.CountUpTin ?? 0) + 1;
                                                            });
                        }
                        else
                        {
                            // if not exist => create
                            StatisticActivitiesUser statisticActivitiesUser = new StatisticActivitiesUser();
                            statisticActivitiesUser.IdThanhVien = idThanhVien;
                            statisticActivitiesUser.Year = DateTime.Now.Year;
                            statisticActivitiesUser.Month = DateTime.Now.Month;
                            statisticActivitiesUser.CountDangTin = 0;
                            statisticActivitiesUser.CountUpTin = 1;
                            statisticActivitiesUser.CountLienHe = 0;

                            RepoStatisticActivitiesUserMaster.InsertReturnEntity(statisticActivitiesUser);
                        }
                    }

                    if (newEntity.IsDuyet != editEntity.IsDuyet || (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value == true))
                    {
                        // Clear Cache
                        MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                        await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinByParams, true, false);
                    }

                    CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                    if (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value)
                    {
                        // ReIndex
                        commonMethodsRaoVat.ReIndexDangTin(newEntity);
                    }
                    else
                    {
                        commonMethodsRaoVat.DeleteIndexDangTin(newEntity);

                        if(newEntity.TrangThai == 1)
                        {
                            // Notify Admin
                            var body = "[" + thanhVien.HoTen + "] - " + newEntity.TieuDe;
                            NotificationRequest.sendNotificationDuyetTinToGroupAdmin(Messages.NOTI_UPDATE_TIEUDE_CHOXETDUYET, body, newEntity.Id.ToString());
                        }
                    }

                    // Update
                    cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateAfterAutoClone(newEntity);

                    InterfaceParam interfaceParam = new InterfaceParam();
                    interfaceParam.idThanhVien = idThanhVien;
                    interfaceParam.ids = id.ToString();
                    // Search & Get data
                    var data = await _bll.SearchDangTinByIdThanhVien(interfaceParam);
                    cusRes.DataResult = data;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDangTin_v1(int id, [FromBody]DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                DangTin editEntity = await RepoDangTinRepositoryRaoVat.GetSingle(id);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                // If admin doen't approve => member cannot edit
                else if ((editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value > 0)
                    && (!editEntity.IsDuyet.HasValue || !editEntity.IsDuyet.Value))
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_011, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;

                    // UpdateAfterAutoClone can not update Null Value
                    newEntity.IdAdminDangKyDuyet = -1;
                    newEntity.TenAdminDangKyDuyet = string.Empty;
                    newEntity.IdAdminTuChoi = -1;
                    newEntity.TenAdminTuChoi = string.Empty;
                    newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiemTinDang(newEntity);
                    newEntity.RewriteUrl = CommonMethods.GetRewriteString(newEntity.TieuDe);
                    newEntity.NgayUp = DateTime.Now;

                    var idThanhVien = Request.GetTokenIdThanhVien();
                    var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                    var kpiAutoDuyet = string.IsNullOrEmpty(thanhVien.AllowKpiRule) ? new string[] { } : thanhVien.AllowKpiRule.Split(",");
                    if (kpiAutoDuyet.Contains(((int)Variables.KpiAutoType.AutoDuyet).ToString()))
                    {
                        newEntity.IsDuyet = true;
                        newEntity.IsAutoDuyet = true;
                        newEntity.GhiChu = "AutoDuyet";
                    }
                    else
                    {
                        newEntity.IsDuyet = false;
                    }

                    // Is about to delete
                    if (newEntity.IsUpTin.HasValue && newEntity.IsUpTin.Value)
                    {
                        newEntity.NgayUp = DateTime.Now;
                        newEntity.IsDuyet = editEntity.IsDuyet;

                        // update statistic
                        var isExistThanhVienInActivitiesUser = RepoStatisticActivitiesUser.Find(a => a.IdThanhVien == idThanhVien
                                                && a.Year == DateTime.Now.Year
                                                && a.Month == DateTime.Now.Month).Result.Count();
                        if (isExistThanhVienInActivitiesUser > 0)
                        {
                            // if exists => update
                            RepoStatisticActivitiesUserMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien
                                                            && a.Year == DateTime.Now.Year
                                                            && a.Month == DateTime.Now.Month, delegate (StatisticActivitiesUser obj)
                                                            {
                                                                obj.CountUpTin = (obj.CountUpTin ?? 0) + 1;
                                                            });
                        }
                        else
                        {
                            // if not exist => create
                            StatisticActivitiesUser statisticActivitiesUser = new StatisticActivitiesUser();
                            statisticActivitiesUser.IdThanhVien = idThanhVien;
                            statisticActivitiesUser.Year = DateTime.Now.Year;
                            statisticActivitiesUser.Month = DateTime.Now.Month;
                            statisticActivitiesUser.CountDangTin = 0;
                            statisticActivitiesUser.CountUpTin = 1;
                            statisticActivitiesUser.CountLienHe = 0;

                            RepoStatisticActivitiesUserMaster.InsertReturnEntity(statisticActivitiesUser);
                        }
                    }

                    if (newEntity.IsDuyet != editEntity.IsDuyet || (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value == true))
                    {
                        // Clear Cache
                        MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                        await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinByParams, true, false);
                    }

                    CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                    if (newEntity.IsDuyet.HasValue && newEntity.IsDuyet.Value)
                    {
                        // ReIndex
                        commonMethodsRaoVat.ReIndexDangTin(newEntity);
                    }
                    else
                    {
                        commonMethodsRaoVat.DeleteIndexDangTin(newEntity);

                        if (newEntity.TrangThai == 1)
                        {
                            // Notify Admin
                            var body = "[" + thanhVien.HoTen + "] - " + newEntity.TieuDe;
                            NotificationRequest.sendNotificationDuyetTinToGroupAdmin(Messages.NOTI_UPDATE_TIEUDE_CHOXETDUYET, body, newEntity.Id.ToString());
                        }
                    }

                    // Handle images
                    if (newEntity.ListHinhAnh.Count > 0)
                    {
                        var token = Request.GetTokenFromHeader();
                        var infoImages = await ApiGetDal.InsertListUrlHinhAnh(newEntity.TieuDe, newEntity.ListHinhAnh, token);
                        newEntity.TongImage = infoImages.Total;
                        newEntity.ListHinhAnhJson = infoImages.ListId;
                    }
                    else
                    {
                        newEntity.TongImage = 0;
                        newEntity.ListHinhAnhJson = string.Empty;
                    }

                    // Update
                    cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateAfterAutoClone(newEntity);

                    InterfaceParam interfaceParam = new InterfaceParam();
                    interfaceParam.idThanhVien = idThanhVien;
                    interfaceParam.ids = id.ToString();
                    // Search & Get data
                    var data = await _bll.SearchDangTinByIdThanhVien(interfaceParam);
                    cusRes.DataResult = data;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePinTopDangTin(int id, [FromBody]DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                DangTin editEntity = await RepoDangTinRepositoryRaoVat.GetSingle(id);

                ThanhVien thanhvien = RepoThanhVien.Find(a => a.Id == newEntity.IdThanhVien).Result.FirstOrDefault();
                List<DangTinTraPhi> lstTinTraPhi = RepoTinTraPhiRepositoryRaoVat.Find(a => a.IdThanhVienTao == newEntity.IdThanhVien).Result.ToList();

                long GetTotalTime = thanhvien != null ? (thanhvien.TotalTime ?? 0) : 0;
                long GetUsedTime = lstTinTraPhi.Count > 0 ? lstTinTraPhi.Sum(a => a.ThoiGianSuDung.HasValue ? a.ThoiGianSuDung.Value : a.ThoiGianSuDungDuDinh.Value) : 0;

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;
                    newEntity.NgayUp = DateTime.Now;
                    #region Handle DangTinTraPhi

                    if (newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.Runing
                        || newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.Waiting)
                    {
                        if (newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.Runing)
                        {
                            newEntity.NgayPinTop = DateTime.Now;
                        }

                        if (GetTotalTime == 0 || GetTotalTime <= GetUsedTime)
                        {
                            cusRes.SetException(null, "Quý khách đã hết thời gian Pin Top! Xin vui lòng đăng ký thêm!");
                            return Response.OkObjectResult(cusRes);
                        }

                        // Check actived
                        var tinTraPhiActive = RepoTinTraPhiRepositoryRaoVat.Find(a => a.IdDangTin == newEntity.Id && a.PinStatus == 0
                                                    && (a.NgayBatDau <= DateTime.Now && DateTime.Now <= (a.NgayKetThuc.HasValue ? a.NgayKetThuc : a.NgayKetThucDuDinh))).Result.FirstOrDefault();

                        // Check watiting pin
                        var tinTraPhiWaiting = RepoTinTraPhiRepositoryRaoVat.Find(a => a.IdDangTin == newEntity.Id && a.PinStatus == 2
                                                    && (DateTime.Now <= a.NgayBatDau && DateTime.Now <= (a.NgayKetThuc.HasValue ? a.NgayKetThuc : a.NgayKetThucDuDinh))).Result.FirstOrDefault();

                        if (tinTraPhiActive != null || tinTraPhiWaiting != null)
                        {
                            cusRes.SetException(null, string.Format(Messages.ERR_022, "Tin đăng này"));
                            return Response.OkObjectResult(cusRes);
                        }

                        // If On => Add data to DangTinTraPhi
                        var tinTraPhi = new DangTinTraPhi();
                        // 0: Runing - Start Pin
                        // 2: Waiting
                        tinTraPhi.PinStatus = newEntity.DangTinTraPhi_Ext.PinStatus;
                        tinTraPhi.IdLoaiTraPhi = (int)Variables.PaidType.PinTop;
                        tinTraPhi.IdThanhVienTao = newEntity.IdThanhVien;
                        tinTraPhi.IdDangTin = newEntity.DangTinTraPhi_Ext.IdDangTin;
                        tinTraPhi.NgayBatDau = newEntity.NgayPinTop;
                        tinTraPhi.NgayKetThucDuDinh = newEntity.DangTinTraPhi_Ext.NgayKetThucDuDinh;
                        tinTraPhi.ThoiGianSuDungDuDinh = CommonMethods.ConvertToInt32((tinTraPhi.NgayKetThucDuDinh - tinTraPhi.NgayBatDau).Value.TotalMinutes);
                        await RepoTinTraPhiRepositoryRaoVatMaster.Insert(tinTraPhi);
                    }
                    else if (newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.UpdateWaiting)
                    {
                        if (GetTotalTime == 0 || GetTotalTime <= GetUsedTime)
                        {
                            cusRes.SetException(null, "Quý khách đã hết thời gian Pin Top! Xin vui lòng đăng ký thêm!");
                            return Response.OkObjectResult(cusRes);
                        }

                        // 4: Update Waiting
                        DangTinTraPhi tinTraPhi = RepoTinTraPhiRepositoryRaoVat.Find(a => a.IdDangTin == newEntity.Id
                                && a.PinStatus == (int)Variables.PinTopType.Waiting
                                && a.NgayBatDau == editEntity.NgayPinTop).Result.FirstOrDefault();

                        if (tinTraPhi == null)
                        {
                            cusRes.SetException(null, string.Format(Messages.ERR_023, "Dữ liệu"));
                            return Response.OkObjectResult(cusRes);
                        }

                        tinTraPhi.PinStatus = (int)Variables.PinTopType.Waiting;
                        tinTraPhi.NgayBatDau = newEntity.NgayPinTop;
                        tinTraPhi.NgayKetThucDuDinh = newEntity.DangTinTraPhi_Ext.NgayKetThucDuDinh;
                        tinTraPhi.ThoiGianSuDungDuDinh = CommonMethods.ConvertToInt32((tinTraPhi.NgayKetThucDuDinh - tinTraPhi.NgayBatDau).Value.TotalMinutes);
                        await RepoTinTraPhiRepositoryRaoVatMaster.UpdateAfterAutoClone(tinTraPhi);
                    }
                    else if (newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.StopRuning
                           || newEntity.DangTinTraPhi_Ext.PinStatus == (int)Variables.PinTopType.StopWaiting)
                    {
                        // 1: Stop runing
                        // 3: Stop waiting => don't count this record.
                        DangTinTraPhi tinTraPhi = RepoTinTraPhiRepositoryRaoVat.Find(a => a.IdDangTin == newEntity.Id
                                && (a.PinStatus == (int)Variables.PinTopType.Runing || a.PinStatus == (int)Variables.PinTopType.Waiting)
                                && a.NgayBatDau == editEntity.NgayPinTop).Result.FirstOrDefault();

                        if (tinTraPhi == null)
                        {
                            cusRes.SetException(null, string.Format(Messages.ERR_023, "Dữ liệu"));
                            return Response.OkObjectResult(cusRes);
                        }

                        tinTraPhi.PinStatus = newEntity.DangTinTraPhi_Ext.PinStatus;
                        tinTraPhi.NgayKetThuc = newEntity.NgayUp;

                        var realTimeStop = CommonMethods.ConvertToInt32((tinTraPhi.NgayKetThuc - tinTraPhi.NgayBatDau).Value.TotalMinutes);
                        var expireTime = realTimeStop > tinTraPhi.ThoiGianSuDungDuDinh ? tinTraPhi.ThoiGianSuDungDuDinh : realTimeStop;
                        tinTraPhi.ThoiGianSuDung = newEntity.DangTinTraPhi_Ext.PinStatus == 1 ? expireTime : 0;
                        await RepoTinTraPhiRepositoryRaoVatMaster.UpdateAfterAutoClone(tinTraPhi);
                    }
                    else
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Dữ liệu"));
                        return Response.OkObjectResult(cusRes);
                    }

                    // Clear Cache
                    MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                    await cacheControllerBase.ClearCacheByEntityName("dangtin");

                    #endregion

                    // Update
                    cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateAfterAutoClone(newEntity);

                    InterfaceParam interfaceParam = new InterfaceParam();
                    interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();
                    interfaceParam.ids = id.ToString();
                    // Search & Get data
                    var data = await _bll.SearchDangTinByIdThanhVien(interfaceParam);
                    cusRes.DataResult = data;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateHinhAnhDangTin(int id, [FromBody]DangTin newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => s.Id == id, delegate (DangTin obj)
                {
                    obj.ListHinhAnhJson = newEntity.ListHinhAnhJson;
                    obj.ListFilesUploaded = newEntity.ListFilesUploaded;
                    obj.MaLayoutAlbum = newEntity.MaLayoutAlbum;
                    obj.TongImage = newEntity.TongImage;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTrangThaiDangTin([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();

            try
            {
                string trangThaiString = CommonMethods.ConvertToString(interfaceParam.trangThai);
                Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);

                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (DangTin obj)
                {
                    obj.TrangThai = trangThai;

                    if (trangThai == 1)
                    {
                        commonMethodsRaoVat.ReIndexDangTin(obj);
                    }
                    else
                    {
                        commonMethodsRaoVat.DeleteIndexDangTin(obj);
                    }
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut]
        public async Task<IActionResult> UpTopDangTin([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                var idThanhVien = Request.GetTokenIdThanhVien();
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                cusRes.IntResult = await RepoDangTinRepositoryRaoVatMaster.UpdateWhere(s => (interfaceParam.lstId != null && interfaceParam.lstId.Contains(s.Id)), delegate (DangTin obj)
                {
                    obj.NgayUp = DateTime.Now;
                });

                var isExistThanhVienInActivitiesUser = RepoStatisticActivitiesUser.Find(a => a.IdThanhVien == idThanhVien
                                                && a.Year == DateTime.Now.Year
                                                && a.Month == DateTime.Now.Month).Result.Count();
                if (isExistThanhVienInActivitiesUser > 0)
                {
                    // if exists => update
                    RepoStatisticActivitiesUserMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien
                                                    && a.Year == DateTime.Now.Year
                                                    && a.Month == DateTime.Now.Month, delegate (StatisticActivitiesUser obj)
                                                    {
                                                        obj.CountUpTin = (obj.CountUpTin ?? 0) + 1;
                                                    });
                }
                else
                {
                    // if not exist => create
                    StatisticActivitiesUser statisticActivitiesUser = new StatisticActivitiesUser();
                    statisticActivitiesUser.IdThanhVien = idThanhVien;
                    statisticActivitiesUser.Year = DateTime.Now.Year;
                    statisticActivitiesUser.Month = DateTime.Now.Month;
                    statisticActivitiesUser.CountDangTin = 0;
                    statisticActivitiesUser.CountUpTin = 1;
                    statisticActivitiesUser.CountLienHe = 0;

                    RepoStatisticActivitiesUserMaster.InsertReturnEntity(statisticActivitiesUser);
                }

                //// update statistic
                //await RepoStatisticActivitiesUserMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien
                //                                    && a.Year == DateTime.Now.Year
                //                                    && a.Month == DateTime.Now.Month, delegate (StatisticActivitiesUser obj)
                //{
                //    obj.CountUpTin = (obj.CountUpTin ?? 0) + 1;
                //});

            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        private string BuildTuKhoaTimKiemTinDang(DangTin data)
        {
            string res = string.Empty;

            if (data != null)
            {
                res += string.IsNullOrEmpty(data.TieuDe) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.TieuDe + ",");
                res += data.Gia.HasValue ? CommonMethods.ConvertUnicodeToASCII(data.Gia + ",") : string.Empty;
            }

            return res;
        }

        #endregion

        #region "Thành viên"

        // Not use anymore => Move to ThanhVienController [Post]
        /// <summary>
        /// hàm tạo trong db raovat: insert bảng thành viên, insert bảng UserPoint, bảng HistoryPoint
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> RegisterThanhVien([FromBody]ThanhVien entity)
        {

            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null || entity.Id <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                var thanhVien = RepoThanhVien.Find(a => a.Id == entity.Id).Result;
                cusRes.DataResult = thanhVien;

                //EntityBaseRepositoryRaoVat<ThanhVien> repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master);
                //entity.IdMd5 = CommonMethods.GenerateIdMd5(entity.Id);
                //IEnumerable<ThanhVien> lstInsertedThanhVien = await repoThanhVienMaster.InsertReturnEntity(entity);

                //ThanhVien insertedThanhVien = lstInsertedThanhVien.FirstOrDefault();

                //string pointString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_GIFT_POINT_REGIST);
                //int point = CommonMethods.ConvertToInt32(pointString);
                //string pointRateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_VND);
                //int pointRate = CommonMethods.ConvertToInt32(pointRateString);

                //// insert vào bảng UserPoint cuả db raovat
                //var isExistUserPoint = RepoUserPoint.Find(a => a.IdThanhVien == insertedThanhVien.Id).Result.FirstOrDefault();
                //if (isExistUserPoint == null)
                //{
                //    UserPoint usp = new UserPoint()
                //    {
                //        IdThanhVien = insertedThanhVien.Id,
                //        Point = point,
                //        PointTmp = 0,
                //        PointUsed = 0,
                //        NgayTao = DateTime.Now,
                //    };
                //    await RepoUserPointMaster.Insert(usp);
                //}

                //// insert vào bảng HistoryPoint của db raovat
                //HistoryPoint htp = new HistoryPoint()
                //{
                //    IdThanhVien = insertedThanhVien.Id,
                //    Point = point,
                //    Type = (int)Variables.TypeUserPoint.GiftRegist,
                //    Rate = pointRate,
                //    NgayTao = DateTime.Now,
                //    Note = "Đăng ký thành viên"
                //};
                //await RepoHistoryPoint.Insert(htp);

                //// Add to notification -- Mr Trung 20200407 - Stop
                //InterfaceParam paramNotify = new InterfaceParam();
                //paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                //paramNotify.idThanhVien = insertedThanhVien.Id;
                //paramNotify.ma = Variables.Constants.F001; // User Registration
                //paramNotify.noiDung = point.ToString();

                //CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                //await commonController.InsertNotification(paramNotify);

                //cusRes.IntResult = 1;
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTrangThaiThanhVien(int id, [FromBody]ThanhVien entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null || entity.Id <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                string idControl = string.Empty;
                EntityBaseRepositoryRaoVat<ThanhVien> repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master);

                cusRes.IntResult = await repoThanhVienMaster.UpdateWhere(n => n.Id == id, delegate (ThanhVien obj)
                {
                    obj.TrangThai = entity.TrangThai;
                });

                // Update List Deleted IdThanhVien
                ModelsRaoVat.AdminBll adminBll = new ModelsRaoVat.AdminBll();
                adminBll.BuildListIdThanhVien();
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> SearchThanhVien([FromQuery]InterfaceParam interfaceParam)
        {
            interfaceParam.notifyUrl = Variables.UrlFromClient;
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                IEnumerable<ThanhVien> data = await _adminBll.SearchThanhVien(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "MaXacNhan"

        /// <summary>
        /// xác thực số điện thoại, nếu thành công thì xoá dòng record maXacThuc đi
        /// theo mặc định nếu gửi nhiều thông tin như emai, sdt thì sẽ xác nhận sdt
        /// xác nhận email thì sẽ gọi api dlx để update cột DaXacNhanEmail
        /// </summary>
        /// <param name="interfaceParam" type="string">chứa nhiều tham số truyền vào</param>
        /// <returns>
        /// "IntResult": 1 -> thành công
        /// "IntResult": -2 -> mã xác thực đã hết hạn or không tồn tại
        /// </returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> XacThucByMaXacThuc()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // lấy dữ liệu trong body truyền lên
                string body = await Request.GetRawBodyStringAsync();
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string dienThoai = bodyJson.GetValueFromKey_ReturnString(CommonParams.dienThoai, "");
                string email = bodyJson.GetValueFromKey_ReturnString(CommonParams.email, ""); // xác thực email là phải có email và token
                string token = bodyJson.GetValueFromKey_ReturnString(CommonParams.token_accessToken, "");
                string maXacNhan = bodyJson.GetValueFromKey_ReturnString(CommonParams.maXacNhan, "");

                // lấy dữ liệu trong bảng mã xác nhận db rao vặt
                IEnumerable<MaXacNhan> lstRecord = null;
                if (!string.IsNullOrEmpty(dienThoai))
                {
                    lstRecord = await RepoMaXacNhan.Search(n => n.Phone == dienThoai && n.Code == maXacNhan, "NgayCap:desc");
                }
                else if (!string.IsNullOrEmpty(email))
                {
                    lstRecord = await RepoMaXacNhan.Search(n => n.Email == email && n.Code == maXacNhan, "NgayCap:desc");
                }

                // so sánh giá trị
                MaXacNhan lastedRecord = lstRecord.FirstOrDefault();
                if (lastedRecord == null)
                {
                    cusRes.SetException(new Exception(Messages.ERR_007));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // tính thời gian của mã xác thực (mã xác thực chỉ có hiệu lực trong 10 phút)
                    DateTime NgayCap = lastedRecord.NgayCap ?? DateTime.MinValue;
                    TimeSpan time = (DateTime.Now).Subtract(NgayCap);
                    string durationValidString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_DURATIONVERIFYISVALID);
                    double durationValid = CommonMethods.ConvertToDouble(durationValidString);// theo seconds
                    TimeSpan timeExpire = TimeSpan.FromSeconds(durationValid);
                    if (time > timeExpire)
                    {
                        // xoá dòng record đó đi luôn 
                        await RepoMaXacNhanMaster.DeleteWhere(n => n.Id == lastedRecord.Id);

                        cusRes.SetException(new Exception(Messages.ERR_007));
                        return Response.OkObjectResult(cusRes);
                    }

                    // so sánh mã xác thực
                    if (maXacNhan != lastedRecord.Code)
                    {
                        cusRes.SetException(new Exception(Messages.ERR_007));
                        return Response.OkObjectResult(cusRes);
                    }
                    else // xác thực thành công thì xoá dòng record đó trong db
                    {
                        await RepoMaXacNhanMaster.DeleteWhere(n => n.Id == lastedRecord.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [AllowAnonymous]
        [HttpDelete]
        public async Task<IActionResult> DeleteMaXacThucByPhone()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string soDienThoai = string.Empty;
                int type = 0;
                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    soDienThoai = CommonMethods.ConvertToString(queryParams[CommonParams.dienThoai]);
                    type = CommonMethods.ConvertToInt32(queryParams[CommonParams.loaiXacNhan].ToString());
                }

                if (string.IsNullOrEmpty(soDienThoai))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                cusRes.IntResult = await RepoMaXacNhanMaster.DeleteWhere(n => n.Phone == soDienThoai && n.LoaiXacNhan == type);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [AllowAnonymous]
        [HttpDelete]
        public async Task<IActionResult> DeleteMaXacThucByEmail()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string email = string.Empty;
                int type = 0;
                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    email = CommonMethods.ConvertToString(queryParams[CommonParams.email]);
                    type = CommonMethods.ConvertToInt32(queryParams[CommonParams.loaiXacNhan].ToString());
                }

                if (string.IsNullOrEmpty(email))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                cusRes.IntResult = await RepoMaXacNhanMaster.DeleteWhere(n => n.Email == email && n.LoaiXacNhan == type);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
        #endregion

        #region "UserDevice"
        // Insert UserDevice
        // Update HistoryDangTinView
        // Update HistoryDangTinLike
        // Push notification if login to another device
        // Insert to NotificationSystem
        [HttpPost]
        public async Task<IActionResult> LoginUserDevice([FromBody] UserDevice entity)
        {
            CustomResult cusRes = new CustomResult();
            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                if (string.IsNullOrEmpty(entity.DeviceId))
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_010, "Device name"));
                    cusRes.StrResult = "2";
                    return Response.OkObjectResult(cusRes);
                }

                long idThanhVien = Request.GetTokenIdThanhVien();

                // tìm trong db theo 2 trường UserId và device nếu cùng phone và khác device name thì gửi sms đến số phone đó
                // (phải kiểm tra logout)
                UserDevice checkUserDevice = (await RepoUserDevice.Find(n => n.UserId == idThanhVien && n.DeviceId.ToUpper() == entity.DeviceId.ToUpper())).FirstOrDefault();
                DateTime currentDate = DateTime.Now;
                if (checkUserDevice == null)
                {
                    // gửi email nếu tài khoản có thông tin email thì gửi email thông báo

                    CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                    var notificationTemplate = commonController.GetTemplateNotification(Variables.Constants.E001);
                    string TieuDe = notificationTemplate == null ? string.Empty : string.Format(notificationTemplate.TieuDe, string.Empty);
                    var noiDung = notificationTemplate.NoiDung;
                    string NoiDung = notificationTemplate == null ? string.Empty : string.Format(noiDung,
                                                                        entity.IpAddress,
                                                                        entity.OsName,
                                                                        entity.DeviceName,
                                                                        currentDate,
                                                                        Variables.DomainName + notificationTemplate.Link);

                    //string emailUser = entity.UserEmail;
                    //if (!string.IsNullOrEmpty(emailUser))
                    //{
                    //    // send email
                    //    if (Variables.EnvironmentIsProduction == true)
                    //    {
                    //        GmailApiCore gac = new GmailApiCore();
                    //        var receiveMail = emailUser;
                    //        var ccEmail = string.Empty;
                    //        var bccEmail = Variables.EmailToCollectError_Name;
                    //        var tieuDe = TieuDe;
                    //        var noiDung = NoiDung;

                    //        var res = gac.SendMessage(receiveMail, ccEmail, bccEmail, tieuDe, noiDung);
                    //    }
                    //    else
                    //    {
                    //        MailService ms = new MailService();
                    //        ms.SendEmail(TieuDe, NoiDung, emailUser);
                    //    }
                    //}

                    InterfaceParam paramNotify = new InterfaceParam();
                    paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                    paramNotify.idThanhVien = idThanhVien;
                    paramNotify.ma = Variables.Constants.E001;
                    paramNotify.noiDung = NoiDung;
                    paramNotify.isSendEmail = true;
                    paramNotify.email = entity.UserEmail;

                    await commonMethodsRaoVat.InsertNotification(paramNotify, NoiDung);

                    cusRes.StrResult = "1";

                }

                UserDevice deviceNoUserId = (await RepoUserDevice.Find(n => n.DeviceId.ToUpper() == entity.DeviceId.ToUpper() && !n.UserId.HasValue)).FirstOrDefault();
                if (deviceNoUserId != null)
                {
                    deviceNoUserId.UserPhone = entity.UserPhone;
                    deviceNoUserId.UserEmail = entity.UserEmail;
                    deviceNoUserId.DeviceName = entity.DeviceName;
                    deviceNoUserId.OsName = entity.OsName;
                    deviceNoUserId.IpAddress = entity.IpAddress;
                    deviceNoUserId.UserId = entity.UserId;
                    deviceNoUserId.LoginDate = currentDate;
                    deviceNoUserId.LatitudeX = entity.LatitudeX ?? "0";
                    deviceNoUserId.LatitudeY = entity.LatitudeY ?? "0";
                    deviceNoUserId.IsUninstalled = false;
                    cusRes.IntResult = await RepoUserDeviceMaster.UpdateAfterAutoClone(deviceNoUserId);
                }
                else
                {
                    UserDevice deviceUserId = (await RepoUserDevice.Find(n => n.DeviceId.ToUpper() == entity.DeviceId.ToUpper() && n.UserId == idThanhVien)).FirstOrDefault();
                    if (deviceUserId != null)
                    {
                        deviceUserId.UserPhone = entity.UserPhone;
                        deviceUserId.UserEmail = entity.UserEmail;
                        deviceUserId.DeviceName = entity.DeviceName;
                        deviceUserId.OsName = entity.OsName;
                        deviceUserId.IpAddress = entity.IpAddress;
                        deviceUserId.LatitudeX = entity.LatitudeX ?? "0";
                        deviceUserId.LatitudeY = entity.LatitudeY ?? "0";
                        deviceUserId.LoginDate = currentDate;
                        deviceUserId.LogoutDate = new DateTime(1970, 1, 1);
                        deviceUserId.IsUninstalled = false;
                        cusRes.IntResult = await RepoUserDeviceMaster.UpdateAfterAutoClone(deviceUserId);
                    }
                    else
                    {
                        entity.LoginDate = currentDate;
                        cusRes.DataResult = await RepoUserDeviceMaster.InsertReturnEntity(entity);
                    }
                }

                // TrangThai = 2 => like without login
                await RepoHistoryDangTinFavoriteMaster.UpdateWhereWithoutClearCache(n => (!n.IdThanhVien.HasValue || n.IdThanhVien.Value <= 0)
                                                                    && n.IdDevice.ToUpper() == entity.DeviceId.ToUpper(), delegate (HistoryDangTinFavorite obj)
                                                                    {
                                                                        obj.TrangThai = 2;
                                                                        obj.IdThanhVien = idThanhVien;
                                                                    });

                await RepoHistoryDangTinLikeMaster.UpdateWhereWithoutClearCache(n => (!n.IdThanhVien.HasValue || n.IdThanhVien.Value <= 0)
                                                                    && n.IdDevice.ToUpper() == entity.DeviceId.ToUpper(), delegate (HistoryDangTinLike obj)
                                                                    {
                                                                        obj.TrangThai = 2;
                                                                        obj.IdThanhVien = idThanhVien;
                                                                    });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        //[HttpPut("{id}")]
        //public async Task<IActionResult> UpdateUserDevice(int id, [FromBody]UserDevice newEntity)
        //{
        //    CustomResult cusRes = new CustomResult();
        //    try
        //    {
        //        if (newEntity == null)
        //        {
        //            cusRes.SetException(null, Messages.ERR_002);
        //            return Response.OkObjectResult(cusRes);
        //        }
        //        UserDevice editEntity = await RepoUserDevice.GetSingle(id);
        //        if (editEntity == null)
        //        {
        //            cusRes.SetException(null, string.Format(Messages.ERR_001, "UserDevice này"));
        //            return Response.OkObjectResult(cusRes);
        //        }
        //        else
        //        {
        //            // Tránh tình trạng newEntity sai id trên đường dẫn
        //            newEntity.Id = id;

        //            cusRes.IntResult = await RepoUserDeviceMaster.UpdateAfterAutoClone(newEntity);
        //            cusRes.DataResult = await RepoUserDevice.Find(s => s.Id == id);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        cusRes.SetException(ex);
        //    }
        //    return Response.OkObjectResult(cusRes);
        //}

        [AllowAnonymous]
        [HttpPut]
        public async Task<IActionResult> LogoutUserDevice()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string uuid = string.Empty;
                string userIdString = string.Empty;

                string body = await Request.GetRawBodyStringAsync();
                Dictionary<string, object> bodyDic = body.ConvertToObject<Dictionary<string, object>>();

                uuid = bodyDic.GetValueFromKey_ReturnString("DeviceId", "");
                userIdString = bodyDic.GetValueFromKey_ReturnString("UserId", "");

                long userId = CommonMethods.ConvertToInt64(userIdString);

                if (string.IsNullOrEmpty(uuid) || string.IsNullOrEmpty(userIdString))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                cusRes.IntResult = await RepoUserDeviceMaster.UpdateWhere(
                    n => n.DeviceId.ToUpper() == uuid.ToUpper() && n.UserId == userId,
                    n => n.LogoutDate = DateTime.Now);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        // Use for login on web dailyxe.com.vn
        // No use ????
        [HttpPost]
        public async Task<IActionResult> PushNotificationToSuccessfulLogin([FromBody] UserDevice entity)
        {
            CustomResult cusRes = new CustomResult();
            CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                long idThanhVien = Request.GetTokenIdThanhVien();

                // tìm trong db theo 2 trường UserId và device nếu cùng phone và khác device name thì gửi sms đến số phone đó
                // (phải kiểm tra logout)
                UserDevice checkUserDevice = (await RepoUserDevice.Find(n => n.UserId == idThanhVien && n.DeviceId == entity.DeviceId)).FirstOrDefault();
                DateTime currentDate = DateTime.Now;
                if (checkUserDevice == null)
                {
                    entity.LoginDate = currentDate;
                    cusRes.DataResult = await RepoUserDeviceMaster.InsertReturnEntity(entity);

                    // gửi email nếu tài khoản có thông tin email thì gửi email thông báo
                    string emailUser = entity.UserEmail;
                    if (!string.IsNullOrEmpty(emailUser))
                    {
                        CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                        var notificationTemplate = commonController.GetTemplateNotification(Variables.Constants.E001);
                        string TieuDe = notificationTemplate == null ? string.Empty : string.Format(notificationTemplate.TieuDe, string.Empty);
                        string NoiDung = notificationTemplate == null ? string.Empty : string.Format(notificationTemplate.NoiDung,
                                                                            entity.IpAddress,
                                                                            entity.OsName,
                                                                            entity.DeviceName,
                                                                            currentDate,
                                                                            Variables.DomainName + notificationTemplate.Link);

                        // send email
                        if (Variables.EnvironmentIsProduction == true)
                        {
                            GmailApiCore gac = new GmailApiCore();
                            var receiveMail = emailUser;
                            var ccEmail = string.Empty;
                            var bccEmail = Variables.EmailToCollectError_Name;
                            var tieuDe = TieuDe;
                            var noiDung = NoiDung;

                            var res = gac.SendMessage(receiveMail, ccEmail, bccEmail, tieuDe, noiDung);
                        }
                        else
                        {
                            MailService ms = new MailService();
                            ms.SendEmail(TieuDe, NoiDung, emailUser);
                        }

                        InterfaceParam paramNotify = new InterfaceParam();
                        paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                        paramNotify.idThanhVien = idThanhVien;
                        paramNotify.ma = Variables.Constants.E001;
                        paramNotify.noiDung = NoiDung;

                        await commonMethodsRaoVat.InsertNotification(paramNotify, NoiDung);

                        cusRes.StrResult = "1";
                    }
                }
                else
                {
                    entity.LoginDate = currentDate;
                    entity.LogoutDate = null;
                    cusRes.IntResult = await RepoUserDeviceMaster.UpdateAfterAutoClone(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "DangTinFavorite"

        [HttpGet]
        public async Task<IActionResult> SearchDangTinFavorite([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);

                // Search & Get data
                IEnumerable<DangTinFavorite> data = await _adminBll.SearchDangTinFavorite(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "DangTinRating"

        [HttpPost]
        public async Task<IActionResult> PostDangTinRating([FromBody]DangTinRating entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // tìm kiếm record trong db đã tồn tại hay chưa
                    DangTinRating existEntity = (await RepoDangTinRating.Find(n => n.IdUser == entity.IdUser && n.IdDangTin == entity.IdDangTin)).FirstOrDefault();

                    // chưa tồn tại insert
                    if (existEntity == null)
                    {
                        entity.TuKhoaTimKiem = BuildTuKhoaTimKiemDangTinRating(entity);
                        cusRes.DataResult = await RepoDangTinRatingMaster.InsertReturnEntity(entity);
                    }
                    // đã tồn tại update
                    else
                    {
                        entity.Id = existEntity.Id;
                        entity.TuKhoaTimKiem = BuildTuKhoaTimKiemDangTinRating(entity);
                        cusRes.IntResult = await RepoDangTinRatingMaster.UpdateAfterAutoClone(entity);
                        cusRes.DataResult = await RepoDangTinRating.Find(s => s.Id == existEntity.Id);

                        // Clear Cache
                        MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                        await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalDangTinRatingByParams, true, false);

                        // Update Rating in DangTin
                        CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                        await commonController.UpdateRatingInDangTin(entity.IdDangTin ?? 0);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        public string BuildTuKhoaTimKiemDangTinRating(DangTinRating entity)
        {
            string res = string.Empty;
            if (entity != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(entity.HoTenThanhVien)
                + "," + CommonMethods.ConvertUnicodeToASCII(entity.TieuDeDangTin_Ext)
                + "," + CommonMethods.RemoveAllHTMLTag(CommonMethods.ConvertUnicodeToASCII(entity.Description))
                + "," + CommonMethods.ConvertUnicodeToASCII((entity.Rating ?? 0).ToString());
            }
            return res;
        }

        [HttpGet]
        public async Task<IActionResult> SearchDangTinRating([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1, 2 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                IEnumerable<DangTinRating> data = await _adminBll.SearchDangTinRating(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "HistoryProfile"

        [HttpPost]
        public async Task<IActionResult> PostHistoryProfile()
        {
            // Expired
            return null;

            CustomResult cusRes = new CustomResult();
            try
            {
                string changePass = string.Empty;
                string changePhone = string.Empty;
                string changeEmail = string.Empty;
                string removeAccount = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    changePass = CommonMethods.ConvertToString(queryParams[CommonParams.changePass]);
                    changePhone = CommonMethods.ConvertToString(queryParams[CommonParams.changePhone]);
                    removeAccount = CommonMethods.ConvertToString(queryParams[CommonParams.removeAccount]);
                    changeEmail = CommonMethods.ConvertToString(queryParams[CommonParams.changeEmail]);

                }
                bool? changePassBool = CommonMethods.ConvertToBoolean(changePass, null);
                bool? changePhoneBool = CommonMethods.ConvertToBoolean(changePhone, null);
                bool? changeEmailBool = CommonMethods.ConvertToBoolean(changeEmail, null);
                bool? removeAccountBool = CommonMethods.ConvertToBoolean(removeAccount, null);

                string jsonInfo = await Request.GetRawBodyStringAsync();
                if (jsonInfo == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    HistoryProfile insEntity = new HistoryProfile();
                    insEntity.IdUser = Request.GetTokenIdThanhVien();
                    insEntity.ListJson = jsonInfo;

                    if (changePassBool == true)
                    {
                        insEntity.Note = "Change pass";
                    }
                    if (changePhoneBool == true)
                    {
                        insEntity.Note = "Change Phone";
                    }
                    if (changeEmailBool == true)
                    {
                        insEntity.Note = "Change Email";
                    }
                    if (removeAccountBool == true)
                    {
                        insEntity.Note = "Remove Account";
                    }

                    cusRes.DataResult = await RepoProfileHistory.InsertReturnEntity(insEntity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "UserRank"

        [HttpGet]
        public async Task<IActionResult> SearchUserRank([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) : null;
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.displayPage = interfaceParam.displayPage <= 0 ? 1 : interfaceParam.displayPage;
                interfaceParam.tuKhoaTimKiem = interfaceParam.tuKhoaTimKiem.ToASCIIAndLower();
                interfaceParam.orderString = string.IsNullOrEmpty(interfaceParam.orderString) ? "Id:desc" : interfaceParam.orderString;

                // Search & Get data
                IEnumerable<UserRank> data = await _adminBll.SearchUserRank(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "NotificationUser"

        [HttpPost]
        public async Task<IActionResult> PostNotificationUser([FromBody]NotificationUser entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();
                    if (idThanhVien <= 0)
                    {
                        cusRes.SetException(null, Messages.ERR_004);
                        cusRes.IntResult = -2;
                        Response.OkObjectResult(cusRes);
                    }

                    entity.IdThanhVien = idThanhVien;

                    var notificationUser = RepoNotificationUser.Find(a => a.IdThanhVien == entity.IdThanhVien
                    && a.IdNotificationSystem == entity.IdNotificationSystem).Result.FirstOrDefault();

                    if (notificationUser == null)
                    {
                        cusRes.DataResult = await RepoNotificationUserMaster.InsertReturnEntity(entity);
                    }
                    else
                    {
                        cusRes.IntResult = await RepoNotificationUserMaster.UpdateWhere(n => n.IdThanhVien == idThanhVien
                                        && n.IdNotificationSystem == entity.IdNotificationSystem, delegate (NotificationUser obj)
                        {
                            obj.TrangThai = entity.TrangThai;
                            obj.IsView = entity.IsView;
                        });
                    }

                    if (cusRes.IntResult == 1)
                    {
                        // Clear Cache
                        MemoryCacheControllerBase cacheControllerBase = new MemoryCacheControllerBase();
                        await cacheControllerBase.ClearCacheByKeyAndConditions(KeyCacheParams.externalNotificationSystemByParams, true, false);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut()]
        public async Task<IActionResult> UpdateNotificationUser([FromBody]NotificationUser newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();

                cusRes.IntResult = await RepoNotificationUserMaster.UpdateWhere(n => n.IdThanhVien == idThanhVien && n.IdNotificationSystem == newEntity.IdNotificationSystem, delegate (NotificationUser obj)
                {
                    obj.TrangThai = newEntity.TrangThai;
                    obj.IsView = newEntity.IsView;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "DangTinViolation"

        [HttpPost]
        public async Task<IActionResult> PostDangTinViolation([FromBody]DangTinViolation entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVienTao = Request.GetTokenIdThanhVien();
                    if (idThanhVienTao <= 0)
                    {
                        cusRes.SetException(null, Messages.ERR_004);
                        cusRes.IntResult = -2;
                        Response.OkObjectResult(cusRes);
                    }

                    entity.IdThanhVienTao = idThanhVienTao;
                    entity.TuKhoaTimKiem = BuildTuKhoaTimKiemDangTinViolation(entity);

                    cusRes.DataResult = await RepoDangTinViolationMaster.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        private string BuildTuKhoaTimKiemDangTinViolation(DangTinViolation data)
        {
            string res = string.Empty;

            if (data != null)
            {
                res += string.IsNullOrEmpty(data.DangTin_Ext.TieuDe) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.DangTin_Ext.TieuDe) + ",";
                res += string.IsNullOrEmpty(data.LyDo) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.LyDo) + ",";

                var idThanhVien = data.DangTin_Ext.IdThanhVien ?? 0;
                var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();

                res += thanhVien == null ? string.Empty : CommonMethods.ConvertUnicodeToASCII(thanhVien.HoTen.ToString()) + ",";
                res += string.IsNullOrEmpty(data.TenThanhVienTao) ? string.Empty : CommonMethods.ConvertUnicodeToASCII(data.TenThanhVienTao);
            }

            return res;
        }

        #endregion

        #region "DangTinTraPhi"

        [HttpGet]
        public async Task<IActionResult> SearchDangTinTraPhi([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();
                if (idThanhVien <= 0)
                {
                    cusRes.SetException(null, Messages.ERR_004);
                    cusRes.IntResult = -2;
                    Response.OkObjectResult(cusRes);
                }

                interfaceParam.idThanhVien = idThanhVien;

                // Search & Get data
                IEnumerable<DangTinTraPhi> data = await _adminBll.SearchDangTinTraPhi(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "DangTinContact"

        [HttpGet]
        public async Task<IActionResult> SearchDangTinContact([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                IEnumerable<DangTinContact> data = await this._bll.SearchDangTinContact(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> PostDangTinContact([FromBody]DangTinContact entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();
                    if (idThanhVien <= 0)
                    {
                        cusRes.SetException(null, Messages.ERR_004);
                        cusRes.IntResult = -2;
                        Response.OkObjectResult(cusRes);
                    }

                    // Check if IsDefault = true => The others false
                    if (entity.IsDefault.HasValue && entity.IsDefault.Value)
                    {
                        await RepoDangTinContactMaster.UpdateWhere(n => n.IdThanhVien == entity.IdThanhVien, delegate (DangTinContact obj)
                        {
                            obj.IsDefault = false;
                        });
                    }

                    entity.IdThanhVien = idThanhVien;
                    cusRes.DataResult = await RepoDangTinContactMaster.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDangTinContact(int id, [FromBody]DangTinContact newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                DangTinContact editEntity = await RepoDangTinContact.GetSingle(id);

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin đăng này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;

                    // Check if IsDefault = true => The others false
                    if (newEntity.IsDefault.HasValue && newEntity.IsDefault.Value)
                    {
                        await RepoDangTinContactMaster.UpdateWhere(n => n.IdThanhVien == newEntity.IdThanhVien, delegate (DangTinContact obj)
                        {
                            obj.IsDefault = false;
                        });
                    }

                    // Update
                    cusRes.IntResult = await RepoDangTinContactMaster.UpdateAfterAutoClone(newEntity);
                    cusRes.DataResult = await RepoDangTinContact.Find(s => s.Id == id);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region PushNotification

        // Only admin use this function
        // 1. DangTinRating => SaveRating
        [HttpPost]
        public async Task<IActionResult> AdminPushNotification([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                cusRes.StrResult = await commonController.InsertNotification(interfaceParam);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        // Only member use this function.
        // 1. User update email (on dailyxe). Use RaoXe to push + insert notification
        [HttpPost]
        public async Task<IActionResult> MemberPushNotification([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();
                var thanhVienExt = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();

                CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                interfaceParam.idThanhVien = idThanhVien;
                interfaceParam.email = thanhVienExt.Email;
                cusRes.StrResult = await commonController.InsertNotification(interfaceParam);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
        #endregion

        #region MarketingPlan

        [HttpPost]
        public async Task<IActionResult> GetMaGioiThieu()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();

                var marketingPlan = RepoMarketingPlan.Find(a => a.MaCauHinh.ToLower() == Variables.MACAUHINH_MAGIOITHIEU.ToLower() && a.TrangThai == 1).Result.FirstOrDefault();

                if (marketingPlan != null)
                {
                    var existMarketingCode = RepoMarketingCode.Find(a => a.IdMarketingPlan == marketingPlan.Id && a.IdThanhVien == idThanhVien).Result;

                    if (existMarketingCode.Count() > 0)
                    {
                        cusRes.DataResult = existMarketingCode;
                    }
                    else
                    {
                        var marketingCode = new MarketingCode();
                        marketingCode.IdMarketingPlan = marketingPlan.Id;
                        marketingCode.IdThanhVien = idThanhVien;
                        marketingCode.MaxUsed = marketingPlan.MaxUsed ?? 0;
                        marketingCode.TrangThai = 1;
                        marketingCode.Code = CommonMethods.GetRandomMaGioiThieu(idThanhVien);

                        cusRes.DataResult = await RepoMarketingCodeMaster.InsertReturnEntity(marketingCode);
                    }
                }
                else
                {
                    cusRes.SetException(null, Messages.ERR_015);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> KichHoatMaGioiThieu([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();

                var thanhVien = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                if (thanhVien.IsActiveMaGioiThieu.HasValue && thanhVien.IsActiveMaGioiThieu.Value == true)
                {
                    cusRes.SetException(null, Messages.ERR_020);
                    return Response.OkObjectResult(cusRes);
                }

                var marketingPlan = RepoMarketingPlan.Find(a => a.MaCauHinh.ToLower() == Variables.MACAUHINH_MAGIOITHIEU.ToLower() && a.TrangThai == 1).Result.FirstOrDefault();

                if (marketingPlan != null)
                {
                    var existMarketingCode = RepoMarketingCode.Find(a => a.IdMarketingPlan == marketingPlan.Id && a.Code.ToUpper() == interfaceParam.ma.ToUpper()
                                                                        && a.IdThanhVien != idThanhVien && a.TrangThai == 1).Result.FirstOrDefault();

                    if (existMarketingCode != null)
                    {
                        // Insert To MarketingUser
                        var marketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == idThanhVien
                                            && a.IdMarketingPlan == marketingPlan.Id
                                            && a.IdMarketingCode == existMarketingCode.Id
                                            && a.MarketingCode.ToUpper() == interfaceParam.ma.ToUpper()).Result.FirstOrDefault();
                        if (marketingUser != null)
                        {
                            cusRes.SetException(null, Messages.ERR_017);
                            return Response.OkObjectResult(cusRes);
                        }

                        // Check only active once.
                        EntityBaseRepositoryRaoVat<ThanhVien> repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master);
                        await repoThanhVienMaster.UpdateWhere(n => n.Id == idThanhVien, delegate (ThanhVien obj)
                        {
                            obj.IsActiveMaGioiThieu = true;
                        });

                        // Update TotalUsed MarketingPlan
                        await RepoMarketingPlanMaster.UpdateWhere(n => n.Id == marketingPlan.Id, delegate (MarketingPlan obj)
                        {
                            obj.TotalUsed = (obj.TotalUsed ?? 0) + 1;
                        });

                        // Update TotalUsed
                        existMarketingCode.TotalUsed = (existMarketingCode.TotalUsed ?? 0) + 1;
                        await RepoMarketingCodeMaster.UpdateAfterAutoClone(existMarketingCode);

                        var valuePoint = CommonMethods.ConvertToInt64(marketingPlan.Value);

                        marketingUser = new MarketingUser();
                        marketingUser.IdThanhVien = idThanhVien;
                        marketingUser.MarketingCode = interfaceParam.ma;
                        marketingUser.NoiDung = marketingPlan.TieuDe;
                        marketingUser.Value = marketingPlan.Value;
                        marketingUser.ValueType = marketingPlan.ValueType;
                        marketingUser.MaxValue = marketingPlan.MaxValue;
                        marketingUser.IdMarketingPlan = marketingPlan.Id;
                        marketingUser.IdMarketingCode = existMarketingCode.Id;

                        marketingUser.ThanhVienInvite_Ext = RepoThanhVien.Find(a => a.Id == existMarketingCode.IdThanhVien).Result.FirstOrDefault();
                        //await ApiGetDal.SearchThanhVienNoToken(existMarketingCode.IdThanhVien.ToString());
                        cusRes.DataResult = await RepoMarketingUserMaster.InsertReturnEntity(marketingUser);

                        // Handle Gifted Point && History
                        string pointRateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_VND);
                        int pointRate = CommonMethods.ConvertToInt32(pointRateString);

                        // Update gifted point for IdThanhVien that invite
                        await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == existMarketingCode.IdThanhVien, delegate (UserPoint obj)
                                                                    {
                                                                        obj.Point = CommonMethods.CalcValueMarketingPlan(obj.Point ?? 0, marketingPlan.Value, marketingPlan.ValueType ?? 0, marketingPlan.MaxValue);
                                                                    });

                        // Upgrade Rank
                        CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                        long? idRankThanhVien = 0;
                        commonMethodsRaoVat.UpgradeRank(existMarketingCode.IdThanhVien ?? 0, out idRankThanhVien);

                        var thanhVienInputCode = RepoThanhVien.Find(a => a.Id == idThanhVien).Result.FirstOrDefault();
                        //await ApiGetDal.SearchThanhVienNoToken(idThanhVien.ToString());
                        var noteTextInputCode = thanhVienInputCode != null ? thanhVienInputCode.HoTen + " - " + thanhVienInputCode.DienThoai : "";

                        // insert vào bảng HistoryPoint của db raovat
                        // A: Chia sẻ code cho B (thăng đang nhập code)
                        HistoryPoint htpShare = new HistoryPoint()
                        {
                            IdThanhVien = existMarketingCode.IdThanhVien,
                            Point = valuePoint,
                            Type = (int)Variables.TypeUserPoint.GiftInviteCode,
                            Rate = pointRate,
                            NgayTao = DateTime.Now,
                            Note = "Chia sẻ mã giới thiệu cho thành viên [" + noteTextInputCode + "]",
                            IdMarketingPlan = marketingPlan.Id,
                            MarketingCode = string.Empty,
                            Value = marketingPlan.Value,
                            ValueType = marketingPlan.ValueType,
                            MaxValue = marketingPlan.MaxValue,
                            IdMarketingCode = existMarketingCode.Id,
                            IdRank = idRankThanhVien
                        };
                        await RepoHistoryPoint.Insert(htpShare);


                        // Update gifted point for IdThanhVien input code
                        await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                        {
                            obj.Point = CommonMethods.CalcValueMarketingPlan(obj.Point ?? 0, marketingPlan.Value, marketingPlan.ValueType ?? 0, marketingPlan.MaxValue);
                        });

                        // Upgrade Rank
                        commonMethodsRaoVat = new CommonMethodsRaoVat();
                        idRankThanhVien = 0;
                        commonMethodsRaoVat.UpgradeRank(idThanhVien, out idRankThanhVien);

                        var noteTextInvite = marketingUser.ThanhVienInvite_Ext != null ? marketingUser.ThanhVienInvite_Ext.HoTen + " - " + marketingUser.ThanhVienInvite_Ext.DienThoai : "";
                        // insert vào bảng HistoryPoint của db raovat
                        // B: nhập code từ A (thăng chia sẻ)
                        HistoryPoint htpInput = new HistoryPoint()
                        {
                            IdThanhVien = idThanhVien,
                            Point = valuePoint,
                            Type = (int)Variables.TypeUserPoint.GiftInputCode,
                            Rate = pointRate,
                            NgayTao = DateTime.Now,
                            Note = "Nhập mã giới thiệu từ thành viên: [" + noteTextInvite + "]",
                            IdMarketingPlan = marketingPlan.Id,
                            MarketingCode = string.Empty,
                            Value = marketingPlan.Value,
                            ValueType = marketingPlan.ValueType,
                            MaxValue = marketingPlan.MaxValue,
                            IdMarketingCode = existMarketingCode.Id,
                            IdRank = idRankThanhVien
                        };
                        await RepoHistoryPoint.Insert(htpInput);
                    }
                    else
                    {
                        cusRes.SetException(null, Messages.ERR_016);
                    }
                }
                else
                {
                    cusRes.SetException(null, Messages.ERR_015);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> SearchMarketingPlan([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                // Search & Get data
                IEnumerable<MarketingPlan> data = await this._bll.SearchMarketingPlanActive(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> ActiveMarketingCode([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            var paramCode = interfaceParam.ma.ToUpper();
            var paramIdMarketingPlan = interfaceParam.idMarketingPlan;

            long idThanhVien = Request.GetTokenIdThanhVien();
            IEnumerable<MarketingPlan> marketingPlan = null;
            MarketingCode marketingCode = null;

            try
            {

                if (paramIdMarketingPlan.HasValue && paramIdMarketingPlan.Value > 0)
                {
                    marketingCode = RepoMarketingCode.Find(a => a.IdMarketingPlan == paramIdMarketingPlan &&
                                                            a.Code.ToUpper() == paramCode && a.TrangThai == 1).Result.FirstOrDefault();

                    // Check if active code
                    interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                    interfaceParam.lstId = new List<long> { paramIdMarketingPlan ?? 0 };
                    var existMarketingPlan = (await this._bll.SearchMarketingPlan(interfaceParam));
                    if (existMarketingPlan != null)
                    {
                        marketingPlan = existMarketingPlan;
                    }
                }
                else
                {
                    var existMarketingCode = RepoMarketingCode.Find(a => a.Code.ToUpper() == paramCode && a.TrangThai == 1).Result.ToList();

                    foreach (var item in existMarketingCode)
                    {
                        // Check if active code
                        interfaceParam.lstTrangThai = string.IsNullOrEmpty(interfaceParam.trangThai) ? new List<int> { 1 } : CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                        interfaceParam.lstId = new List<long> { item.IdMarketingPlan ?? 0 };
                        var existMarketingPlan = (await this._bll.SearchMarketingPlan(interfaceParam));
                        if (existMarketingPlan != null && existMarketingPlan.Count() > 0)
                        {
                            marketingPlan = existMarketingPlan;
                            marketingCode = item;
                        }
                    }
                }

                if (marketingCode == null)
                {
                    // Update UserPoint: MarketingCode, IdMarketingPlan
                    await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                    {
                        obj.MarketingCode = string.Empty;
                        obj.IdMarketingPlan = null;
                        obj.Value = string.Empty;
                        obj.ValueType = null;
                        obj.MaxValue = string.Empty;
                        obj.TotalPrice = 0;
                        obj.IdMarketingCode = null;
                    });

                    cusRes.SetException(null, Messages.ERR_016);
                    return Response.OkObjectResult(cusRes);
                }

                // if MarketingPlan is not active => Error
                if (marketingPlan == null || marketingPlan.Count() == 0)
                {
                    // Update UserPoint: MarketingCode, IdMarketingPlan
                    await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                    {
                        obj.MarketingCode = string.Empty;
                        obj.IdMarketingPlan = null;
                        obj.Value = string.Empty;
                        obj.ValueType = null;
                        obj.MaxValue = string.Empty;
                        obj.TotalPrice = 0;
                        obj.IdMarketingCode = null;
                    });

                    cusRes.SetException(null, Messages.ERR_018);
                    return Response.OkObjectResult(cusRes);
                }

                // Check if code used by user
                var marketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == idThanhVien
                                            && a.IdMarketingPlan == marketingPlan.FirstOrDefault().Id
                                            && a.IdMarketingCode == marketingCode.Id
                                            && a.MarketingCode.ToUpper() == paramCode).Result.FirstOrDefault();
                if (marketingUser != null)
                {
                    // Update UserPoint: MarketingCode, IdMarketingPlan
                    await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                    {
                        obj.MarketingCode = string.Empty;
                        obj.IdMarketingPlan = null;
                        obj.Value = string.Empty;
                        obj.ValueType = null;
                        obj.MaxValue = string.Empty;
                        obj.TotalPrice = 0;
                        obj.IdMarketingCode = null;
                    });

                    cusRes.SetException(null, Messages.ERR_017);
                    return Response.OkObjectResult(cusRes);
                }

                var userPoint = RepoUserPoint.Find(n => n.IdThanhVien == idThanhVien).Result.FirstOrDefault();

                if (userPoint.IdAdminDangKyDuyet.HasValue && userPoint.IdAdminDangKyDuyet.Value > 0
                    && userPoint.IdMarketingPlan.HasValue && userPoint.IdMarketingPlan.Value != marketingCode.IdMarketingPlan)
                {
                    cusRes.SetException(null, Messages.ERR_021);
                    return Response.OkObjectResult(cusRes);
                }

                if (!string.IsNullOrEmpty(userPoint.Uuid))
                {
                    // Checkk if Timeout (<=2) => Error if Diff UUID
                    if (userPoint.CheckoutDate.HasValue && (DateTime.Now - userPoint.CheckoutDate).Value.TotalMinutes <= Variables.TimeOutMomoPayment)
                    {
                        if (userPoint.Uuid != interfaceParam.Uuid)
                        {
                            cusRes.SetException(null, Messages.ERR_025);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }

                // Update UserPoint: MarketingCode, IdMarketingPlan
                await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                                {
                                    obj.MarketingCode = paramCode;
                                    obj.IdMarketingPlan = marketingCode.IdMarketingPlan;
                                    obj.Value = marketingPlan.FirstOrDefault().Value ?? string.Empty;
                                    obj.ValueType = marketingPlan.FirstOrDefault().ValueType;
                                    obj.MaxValue = marketingPlan.FirstOrDefault().MaxValue ?? string.Empty;
                                    obj.IdMarketingCode = marketingCode.Id;
                                });

                cusRes.DataResult = marketingPlan;
            }
            catch (Exception ex)
            {
                // Update UserPoint: MarketingCode, IdMarketingPlan
                await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                {
                    obj.MarketingCode = string.Empty;
                    obj.IdMarketingPlan = null;
                    obj.Value = string.Empty;
                    obj.ValueType = null;
                    obj.MaxValue = string.Empty;
                    obj.TotalPrice = 0;
                    obj.IdMarketingCode = null;
                });

                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> RemoveMarketingCode([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                long idThanhVien = Request.GetTokenIdThanhVien();

                // Update UserPoint: MarketingCode, IdMarketingPlan
                await RepoUserPointMaster.UpdateWhere(a => a.IdThanhVien == idThanhVien, delegate (UserPoint obj)
                {
                    obj.MarketingCode = string.Empty;
                    obj.IdMarketingPlan = null;
                    obj.Value = string.Empty;
                    obj.ValueType = null;
                    obj.MaxValue = string.Empty;
                    obj.TotalPrice = 0;
                    obj.IdMarketingCode = null;
                });
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpGet]
        public async Task<IActionResult> SearchMarketingPlanUsedByUser([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                   CommonMethods.SplitStringToInt64(interfaceParam.id);
                // Handle param
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                // Search & Get data
                List<MarketingPlan> data = await this._bll.SearchMarketingPlanUsedByUser(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region UserPoint

        [HttpGet]
        public async Task<IActionResult> SearchUserPoint([FromQuery]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.idThanhVien = Request.GetTokenIdThanhVien();

                if (interfaceParam.displayPage <= 0)
                {
                    interfaceParam.displayPage = 1;
                }
                if (interfaceParam.displayItems <= 0)
                {
                    interfaceParam.displayItems = 20;
                }

                // get thong tin token từ request
                if (interfaceParam.allIncluding == true)
                {
                    interfaceParam.token = Request.GetTokenFromHeader();
                }

                // Search & Get data
                IEnumerable<UserPoint> data = await this._bll.searchUserPoint(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region Payment

        //Id: this.userPoint.Id,
        //PointTmp: newPointTmp,
        //MaxValue: this.dataUuDai? this.dataUuDai.MaxValue : null,
        //TotalPrice: this.calculatorMoney()
        //Uuid = Variables.uniqueDeviceID;
        //ReturnUrl_Ext = "xxxx"; // cái này gán tạm (server tự tạo ReturnUrl)
        [HttpPost]
        public async Task<IActionResult> MomoRequest([FromBody] UserPoint newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                CommonMethods.WriteLogMarketingPlan("MomoRequest - Start Update Point");

                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                UserPoint editEntity = RepoUserPoint.Find(a => a.Id == newEntity.Id).Result.FirstOrDefault();

                if (editEntity == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                    return Response.OkObjectResult(cusRes);
                }

                if (editEntity.IdAdminDangKyDuyet.HasValue && editEntity.IdAdminDangKyDuyet.Value > 0)
                {
                    cusRes.SetException(null, Messages.ERR_021);
                    return Response.OkObjectResult(cusRes);
                }

                if (!string.IsNullOrEmpty(editEntity.Uuid))
                {
                    // Checkk if Timeout (<=2) => Error if Diff UUID
                    if (editEntity.CheckoutDate.HasValue && (DateTime.Now - editEntity.CheckoutDate).Value.TotalMinutes <= Variables.TimeOutMomoPayment)
                    {
                        if (newEntity.Uuid != editEntity.Uuid)
                        {
                            cusRes.SetException(null, Messages.ERR_025);
                            return Response.OkObjectResult(cusRes);
                        }
                    }
                }

                var UpdateResult = await RepoUserPointMaster.UpdateWhere(s => s.Id == newEntity.Id, delegate (UserPoint obj)
                {
                    obj.PointTmp = newEntity.PointTmp;
                    obj.TotalPrice = newEntity.TotalPrice;
                    obj.Uuid = newEntity.Uuid;
                    obj.CheckoutDate = DateTime.Now;
                });

                CommonMethods.WriteLogMarketingPlan("MomoRequest - End Update Point:" + UpdateResult);

                if (UpdateResult > 0)
                {
                    long idThanhVien = Request.GetTokenIdThanhVien();

                    if (editEntity.IdThanhVien != idThanhVien)
                    {
                        cusRes.SetException(null, Messages.ERR_Http_UnAuthorized);
                        return Response.OkObjectResult(cusRes);
                    }

                    CommonMethods.WriteLogMarketingPlan("MomoRequest - Start NotifyUrl:" + idThanhVien);

                    //request params need to request to MoMo system
                    string endpoint = Variables.DomainMomoTransaction;
                    string orderInfo = "RaoXe - Thanh toán " + newEntity.PointTmp + " Point";
                    string amount = newEntity.TotalPrice.Value.ToString();
                    string orderid = Guid.NewGuid().ToString();
                    string requestId = Guid.NewGuid().ToString();
                    string extraData = newEntity.ExtraData_Ext;

                    JObject messageReturnObject = new JObject
                    {
                        { "requestId", requestId },
                        { "amount", amount },
                        { "orderId", orderid },
                        { "orderInfo", orderInfo },
                        { "extraData", extraData },
                        { "idThanhVien", idThanhVien.ToString() },
                        { "point", newEntity.PointTmp.Value.ToString() }
                    };

                    var messageReturnObject64 = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(messageReturnObject));
                    // string deeplink = Variables.DomainDaiLyXeMomoRequest + messageReturnObject64; // newEntity.ReturnUrl_Ext;
                    // var dynamicLink = string.Format(Variables.DomainRaoXeDynamicLink, deeplink, deeplink, deeplink);

                    var returnUrl = Variables.DomainReturnUrl + messageReturnObject64;

                    string notifyurl = string.Empty;
                    if (string.IsNullOrEmpty(Variables.ApiRaoXe))
                    {
                        notifyurl = "https://stgapirv01.dailyxe.com.vn/api/raovat/momo/HandleMomoSuccess?randomToken=d@ilyx3@12320180720RV&id=" + newEntity.Id;
                    }
                    else
                    {
                        notifyurl = Variables.ApiRaoXe + "momo/HandleMomoSuccess?randomToken=d@ilyx3@12320180720RV&id=" + newEntity.Id;
                    }

                    //Before sign HMAC SHA256 signature
                    string rawHash = "partnerCode=" +
                        Variables.PartnerCode + "&accessKey=" +
                        Variables.AccessKey + "&requestId=" +
                        requestId + "&amount=" +
                        amount + "&orderId=" +
                        orderid + "&orderInfo=" +
                        orderInfo + "&returnUrl=" +
                        returnUrl + "&notifyUrl=" +
                        notifyurl + "&extraData=" +
                        extraData;
                    MoMoSecurity crypto = new MoMoSecurity();
                    //sign signature SHA256
                    string signature = crypto.signSHA256(rawHash, Variables.SerectKey);
                    //build body json request
                    JObject message = new JObject
                    {
                        { "partnerCode", Variables.PartnerCode },
                        { "accessKey", Variables.AccessKey },
                        { "requestId", requestId },
                        { "amount", amount },
                        { "orderId", orderid },
                        { "orderInfo", orderInfo },
                        { "returnUrl", returnUrl },
                        { "notifyUrl", notifyurl },
                        { "extraData", extraData },
                        { "requestType", "captureMoMoWallet" },
                        { "signature", signature }
                    };

                    var paymentRequest = PaymentRequest.sendPaymentRequest(endpoint, message.ToString());
                    var returnData = await RepoUserPoint.Find(a => a.Id == newEntity.Id);

                    var thanhVien = _bll.getThanhVienById(idThanhVien);
                    var userRank = _bll.getUserRankById(thanhVien.IdRank ?? 0);

                    // insert một dòng record trong historyPoint
                    HistoryPoint oldEntity = new HistoryPoint()
                    {
                        Type = (int)Variables.TypeUserPoint.OldRecord,
                        Point = newEntity.PointTmp,
                        IdThanhVien = idThanhVien,
                        NgayTao = DateTime.Now,
                        Note = "Old Record:" + paymentRequest,
                        Price = newEntity.TotalPrice,
                        MarketingCode = returnData.FirstOrDefault().MarketingCode,
                        IdMarketingPlan = returnData.FirstOrDefault().IdMarketingPlan,
                        Value = returnData.FirstOrDefault().Value,
                        ValueType = returnData.FirstOrDefault().ValueType,
                        MaxValue = returnData.FirstOrDefault().MaxValue,
                        IdMarketingCode = returnData.FirstOrDefault().IdMarketingCode,
                        Uuid = newEntity.Uuid,
                        OrderId = orderid,
                        IdRank = thanhVien.IdRank,
                        RankPercentDiscount = userRank.PercentDiscount,
                        RequestId = requestId
                    };

                    await RepoHistoryPoint.Insert(oldEntity);

                    returnData.FirstOrDefault().MomoObject_Ext = CommonMethods.ConvertToJObject(paymentRequest);
                    cusRes.DataResult = returnData;
                    CommonMethods.WriteLogMarketingPlan("MomoRequest - End NotifyUrl:" + paymentRequest);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(null, ex.ToString());
                return Response.OkObjectResult(cusRes);
            }

            return Response.OkObjectResult(cusRes);
        }

        // Check when resume app
        [HttpPost]
        public IActionResult MomoTransactionStatus([FromBody] UserPoint newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                var historyPoint = RepoHistoryPoint.Find(a => a.OrderId == newEntity.OrderId_Ext).Result.OrderByDescending(a => a.NgayTao).FirstOrDefault();

                if (historyPoint == null)
                {
                    cusRes.SetException(null, string.Format(Messages.ERR_010, "Record này"));
                    return null;
                }

                //Before sign HMAC SHA256 signature
                string rawHash = "partnerCode=" +
                    Variables.PartnerCode + "&accessKey=" +
                    Variables.AccessKey + "&requestId=" +
                    historyPoint.RequestId + "&orderId=" +
                    historyPoint.OrderId + "&requestType=" +
                    "transactionStatus";
                MoMoSecurity crypto = new MoMoSecurity();
                //sign signature SHA256
                string signature = crypto.signSHA256(rawHash, Variables.SerectKey);

                JObject message = new JObject
                    {
                        { "partnerCode", Variables.PartnerCode },
                        { "accessKey", Variables.AccessKey },
                        { "requestId", historyPoint.RequestId },
                        { "orderId", historyPoint.OrderId },
                        { "requestType", "transactionStatus" },
                        { "signature", signature }
                    };

                JObject jObject = CommonMethods.ConvertToJObject(PaymentRequest.receiveTransactionStatusRequest(Variables.DomainMomoTransaction, message.ToString()));

                // Transaction init => Giao dịch khởi tạo (trạng thái mặc định)
                if (jObject["errorCode"].ToString() == "-1")
                {
                    cusRes.SetException(null, jObject["localMessage"].ToString());
                    cusRes.StrResult = jObject["errorCode"].ToString();
                    cusRes.IntResult = 0;
                    return Response.OkObjectResult(cusRes);
                }
                else if (jObject["errorCode"].ToString() != "0")
                {
                    cusRes.SetException(null, jObject["localMessage"].ToString());
                    cusRes.StrResult = jObject["errorCode"].ToString();
                    cusRes.IntResult = -1;
                    return Response.OkObjectResult(cusRes);
                }

                if (historyPoint.ErrorCode != "0")
                {
                    cusRes.SetException(null, historyPoint.Message);
                    cusRes.StrResult = historyPoint.ErrorCode;
                    cusRes.IntResult = -2;
                    return Response.OkObjectResult(cusRes);
                }

                List<JObject> lstReturnObj = new List<JObject> { jObject };
                cusRes.DataResult = lstReturnObj;
            }
            catch (Exception ex)
            {
                cusRes.SetException(null, ex.ToString());
                return Response.OkObjectResult(cusRes);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region Linux handle
        [AllowAnonymous]
        [HttpPost]
        public IActionResult LinuxExecCommand([FromBody]InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (interfaceParam.token != "d@ilyx3@12320180720RV")
                {
                    cusRes.StrResult = "Permission Denied";
                    return Response.OkObjectResult(cusRes);
                }

                var con = CommonMethods.ConnectToLinux();
                using (var client = new SshClient(con))
                {
                    client.Connect();
                    var cmd = client.RunCommand(interfaceParam.noiDung);
                    var output = cmd.Result;
                    Console.WriteLine("====", output);
                    cusRes.StrResult = output;
                    client.Disconnect();
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #endregion
    }

}
