using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;
using raoVatApi.Models;
using raoVatApi.Response;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using Microsoft.AspNetCore.Authorization;

namespace raoVatApi.ControllersRaoVat
{
    [Route("RaoVat/")]
    //[Route("[controller]")]
    public class HomeController : Controller
    {

        private IMemoryCache _cache;        
        private InterfaceBLL _bll;        

        public HomeController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._bll = new InterfaceBLL(Request);            
        }
        
        public IActionResult Index()
        {
            //return RedirectToAction("Index", "Read");            
            //return View("RaoVat/Views/Home/Index.cshtml");
            return View("Index");
        }

        [HttpGet("[action]")]
        public IActionResult Read()
        {
            return View("RaoVat/Views/Home/Read.cshtml");
        }

        [HttpGet("[action]")]
        public IActionResult RenderComponent(string pathLoad)
        {
            return PartialView(pathLoad);
        }

        //[HttpGet("([a-z]{2,4})")]
        //public IActionResult Reg()
        //{
        //    return View("RaoVat/Views/Home/Read.cshtml");
        //}
    }
}