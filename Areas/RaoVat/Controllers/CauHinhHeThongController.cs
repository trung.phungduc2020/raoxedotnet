using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class CauHinhHeThongController : Controller
    {
        // Read
        private EntityBaseRepository<CauHinhHeThong> _repo;
        public EntityBaseRepository<CauHinhHeThong> Repo { get => _repo == null ? _repo = new EntityBaseRepository<CauHinhHeThong>(Request) : _repo; set => _repo = value; }

        // Write
        private EntityBaseRepositoryRaoVat<CauHinhHeThong> _repoCauHinhHeThongMaster;
        public EntityBaseRepositoryRaoVat<CauHinhHeThong> RepoCauHinhHeThongMaster { get => _repoCauHinhHeThongMaster == null ? _repoCauHinhHeThongMaster = new EntityBaseRepositoryRaoVat<CauHinhHeThong>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoCauHinhHeThongMaster; set => _repoCauHinhHeThongMaster = value; }
        
        private AdminBll _adminBll;

        public CauHinhHeThongController()
        {
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string search_ids = string.Empty;
                string search_maCauHinh = string.Empty;
                string search_giaTriCauHinh = string.Empty;

                string displayItems = string.Empty;
                string displayPage = string.Empty;
                string allIncludingString = string.Empty;
                bool allIncluding = false;
                string search_dynamicParams = string.Empty;

                string orderString = string.Empty;
                bool getPropertyNameList = false;

                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    search_ids = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    if (string.IsNullOrEmpty(search_ids))
                    {
                        search_ids = CommonMethods.ConvertToString(queryParams[CommonParams.id]);
                    }

                    search_maCauHinh = CommonMethods.ConvertToString(queryParams[CommonParams.maCauHinh]);
                    search_giaTriCauHinh = CommonMethods.ConvertToString(queryParams[CommonParams.giaTriCauHinh]);

                    displayItems = CommonMethods.ConvertToString(queryParams[CommonParams.displayItems]);
                    displayPage = CommonMethods.ConvertToString(queryParams[CommonParams.displayPage]);
                    allIncludingString = queryParams[CommonParams.allIncluding];
                    allIncluding = CommonMethods.ConvertToBoolean(allIncludingString);
                    search_dynamicParams = CommonMethods.ConvertToString(queryParams[CommonParams.dynamicParam]);

                    orderString = CommonMethods.ConvertToString(queryParams[CommonParams.orderString]);
                    getPropertyNameList = CommonMethods.ConvertToBoolean(CommonMethods.ConvertToString(queryParams[CommonParams.getPropertyNameList]), false).Value;
                }

                int currentPage = CommonMethods.ConvertToInt32(displayPage);
                int pageSize = CommonMethods.ConvertToInt32(displayItems);

                List<long> lstId = CommonMethods.SplitStringToInt64(search_ids);

                int totalRows = 0;

                IEnumerable<CauHinhHeThong> data = await _adminBll.SearchCauHinhHeThong(currentPage, pageSize, lstId, search_maCauHinh, search_giaTriCauHinh, search_dynamicParams, allIncluding, orderString);

                cusRes.DataResult = data;

                totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(currentPage, pageSize, data != null ? data.Count() : 0, totalRows);
                if (getPropertyNameList)
                {
                    cusRes.AddPropertyNameList(CommonMethods.GetPropertyNameList(data.FirstOrNewObject()));
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                string orderString = string.Empty;
                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    orderString = CommonMethods.ConvertToString(queryParams[CommonParams.orderString]);
                }

                IEnumerable<CauHinhHeThong> data = await _adminBll.SearchCauHinhHeThongByIds(id, orderString);
                cusRes.DataResult = data;
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CauHinhHeThong entity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string idControl = string.Empty;
                string msgCheckedData = IsCheckedData(entity, ref idControl);
                if (!string.IsNullOrEmpty(msgCheckedData))
                {
                    cusRes.GotoLink = idControl;
                    cusRes.SetException(null, msgCheckedData);
                    return Response.OkObjectResult(cusRes);
                }

                cusRes.DataResult = await RepoCauHinhHeThongMaster.InsertReturnEntity(entity);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes); ;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]CauHinhHeThong newEntity)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string idControl = string.Empty;
                string msgCheckedData = IsCheckedData(newEntity, ref idControl);
                if (!string.IsNullOrEmpty(msgCheckedData))
                {
                    cusRes.GotoLink = idControl;
                    cusRes.SetException(null, msgCheckedData);
                    return Response.OkObjectResult(cusRes);
                }

                CauHinhHeThong editEntity = await Repo.GetSingle(id);

                if (editEntity == null)
                {
                    //return BadRequest();                
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "CauHinhHeThong này"));
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    // Tránh tình trạng newEntity sai id trên đường dẫn
                    newEntity.Id = id;
                    cusRes.IntResult = await RepoCauHinhHeThongMaster.UpdateAfterAutoClone(newEntity);
                    cusRes.DataResult = await Repo.Find(s => s.Id == id);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            // TODO: check condition before insert into DB

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateByMa([FromBody]Dictionary<string, object> body)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                string maCauHinh = string.Empty;
                string giaTriCauHinh = string.Empty;

                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    maCauHinh = CommonMethods.ConvertToString(queryParams[CommonParams.maCauHinh]);
                    giaTriCauHinh = CommonMethods.ConvertToString(queryParams[CommonParams.giaTriCauHinh]);
                }

                // nguyencuongcs - 20171121: update theo danh sách                
                if (body != null && body.Count > 0)
                {
                    int res = 0;

                    foreach (var cauHinh in body)
                    {
                        res += await RepoCauHinhHeThongMaster.UpdateWhere(s => s.MaCauHinh == cauHinh.Key, delegate (CauHinhHeThong obj)
                        {
                            obj.GiaTriCauHinh = CommonMethods.ConvertToString(cauHinh.Value);
                        });
                    }

                    cusRes.IntResult = res;
                }
                else // update kiểu cũ theo từng mã per request
                {
                    List<CauHinhHeThong> editEntity = Repo.Find(s => s.MaCauHinh == maCauHinh).Result.ToList();

                    if (editEntity == null || editEntity.Count() == 0)
                    {
                        //return BadRequest();                
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "CauHinhHeThong này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        CauHinhHeThong updateEntity = editEntity[0];

                        cusRes.IntResult = await RepoCauHinhHeThongMaster.UpdateWhere(s => s.Id == updateEntity.Id, delegate (CauHinhHeThong obj)
                        {
                            obj.GiaTriCauHinh = giaTriCauHinh;
                        });

                        if (cusRes.IntResult > 0)
                        {
                            cusRes.DataResult = await Repo.Find(s => s.Id == updateEntity.Id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            // TODO: check condition before insert into DB

            return Response.OkObjectResult(cusRes);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            CustomResult cusRes = new CustomResult();
            try
            {

                string idsString = string.Empty;

                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                }

                List<int> lstIds = CommonMethods.SplitStringToInt(idsString, false);
                CustomResult delCusRes = new CustomResult();
                string consolidateMessage = string.Empty;

                foreach (int val in lstIds)
                {
                    delCusRes = new CustomResult();
                    delCusRes = await Delete(val);

                    if (delCusRes.IntResult > 0)
                    {
                        cusRes.IntResult += delCusRes.IntResult;
                    }
                    else
                    {
                        consolidateMessage += delCusRes.Message + " \n";
                    }
                }

                if (!string.IsNullOrEmpty(consolidateMessage) && consolidateMessage.Length > 0)
                {
                    cusRes.SetException(null, consolidateMessage);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        public async Task<CustomResult> Delete(int id)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                CauHinhHeThong delEntity = await Repo.GetSingle(id);

                if (delEntity == null)
                {
                    cusRes.SetException(null, "Id = " + id + ": " + Messages.ERR_003);
                    return cusRes;
                }
                else
                {
                    // TODO: check relevant data before delete
                    string messageError = string.Empty;
                    if (IsCheckDeleteMenu(delEntity, ref messageError))
                    {
                        cusRes.IntResult = await RepoCauHinhHeThongMaster.Delete(delEntity);
                    }
                    else
                    {
                        cusRes.Message = messageError;
                        return cusRes;
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return cusRes;
        }

        private bool IsCheckUpdateMenu(int idMenu, int oldIDLoaiMenu, ref string messageError)
        {
            bool res = true;
            return res;
        }

        public bool IsCheckDeleteMenu(CauHinhHeThong mdMenu, ref string messageError)
        {
            bool res = true;
            return res;
        }

        private string IsCheckedData(CauHinhHeThong mdMenu, ref string idControl)
        {
            return string.Empty;
        }

        [HttpPost("memorycache/[action]")]
        public async Task<IActionResult> Insert([FromBody]string newKeyCache)
        {
            CustomResult cusRes = new CustomResult();
            string error = "";

            try
            {
                if (string.IsNullOrEmpty(newKeyCache))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string[] arrKeyCache = newKeyCache.Split(KeyCacheParams.splitKeyCacheChar);

                if (arrKeyCache != null && arrKeyCache.Count() > 0)
                {
                    string memoryCacheValue = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_MEMORYCACHE_LOCAL);
                    string[] arrMemoryCacheValue = memoryCacheValue != null ? memoryCacheValue.Split(KeyCacheParams.splitKeyCacheChar) : new string[] { };
                    List<string> newMemoryCacheValue = arrMemoryCacheValue.ToList();

                    foreach (string item in arrKeyCache)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (arrMemoryCacheValue.Contains(item))
                            {
                                error += item + ",";
                            }
                            else
                            {
                                newMemoryCacheValue.Add(item);
                            }
                        }
                    }

                    if (!newMemoryCacheValue.Equals(memoryCacheValue))
                    {
                        cusRes.IntResult = await RepoCauHinhHeThongMaster.UpdateWhere(s => s.MaCauHinh == Variables.MACAUHINH_MEMORYCACHE_LOCAL, delegate (CauHinhHeThong obj)
                        {
                            obj.GiaTriCauHinh = string.Join(KeyCacheParams.splitKeyCacheChar, newMemoryCacheValue);
                        });
                    }
                }

                if (!string.IsNullOrEmpty(error))
                {
                    error = "Keys đã tồn tại: " + error;
                    if (error.EndsWith(","))
                    {
                        error = error.Substring(0, error.Length - 1);
                        cusRes.Message = error;
                    }
                }


            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);

        }

        [HttpDelete("memorycache/[action]")]
        public async Task<IActionResult> Delete([FromBody]string removeKeyCache)
        {
            CustomResult cusRes = new CustomResult();

            try
            {
                if (string.IsNullOrEmpty(removeKeyCache))
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }

                string[] arrKeyCache = removeKeyCache.Split(KeyCacheParams.splitKeyCacheChar);

                if (arrKeyCache != null && arrKeyCache.Count() > 0)
                {
                    string memoryCacheValue = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_MEMORYCACHE_LOCAL);
                    string[] arrMemoryCacheValue = memoryCacheValue != null ? memoryCacheValue.Split(KeyCacheParams.splitKeyCacheChar) : new string[] { };
                    List<string> newMemoryCacheValue = arrMemoryCacheValue.ToList();

                    foreach (string item in arrKeyCache)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (arrMemoryCacheValue.Contains(item))
                            {
                                newMemoryCacheValue.Remove(item);
                            }
                        }
                    }

                    if (!newMemoryCacheValue.Equals(memoryCacheValue))
                    {
                        cusRes.IntResult = await RepoCauHinhHeThongMaster.UpdateWhere(s => s.MaCauHinh == Variables.MACAUHINH_MEMORYCACHE_LOCAL, delegate (CauHinhHeThong obj)
                        {
                            obj.GiaTriCauHinh = string.Join(KeyCacheParams.splitKeyCacheChar, newMemoryCacheValue);
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }
    }
}