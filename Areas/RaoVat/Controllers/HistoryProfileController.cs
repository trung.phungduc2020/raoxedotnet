﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using System;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class HistoryProfileController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryHistoryRaoVat<HistoryProfile> _repo;
        public EntityBaseRepositoryHistoryRaoVat<HistoryProfile> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryHistoryRaoVat<HistoryProfile>(Request) : _repo; set => _repo = value; }

        public HistoryProfileController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string daXacThucCmnd = string.Empty;
                string daXacThucPassport = string.Empty;
                string idThanhVien = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    daXacThucCmnd = CommonMethods.ConvertToString(queryParams[CommonParams.daXacThucCmnd]);
                    daXacThucPassport = CommonMethods.ConvertToString(queryParams[CommonParams.daXacThucPassport]);
                    idThanhVien = CommonMethods.ConvertToString(queryParams[CommonParams.idThanhVien]);
                }

                bool? daXacThucCmndBool = CommonMethods.ConvertToBoolean(daXacThucCmnd, null);
                bool? daXacThucPassportBool = CommonMethods.ConvertToBoolean(daXacThucPassport, null);

                string jsonInfo = await Request.GetRawBodyStringAsync();
                if (jsonInfo == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    HistoryProfile insEntity = new HistoryProfile();
                    insEntity.ListJson = jsonInfo;
                    insEntity.IdUser = CommonMethods.ConvertToInt64(idThanhVien);
                    insEntity.Note = daXacThucCmndBool.HasValue && daXacThucCmndBool.Value ? "Identity Card confirmation" : "Passport confirmation";
                    insEntity.IdAdminCapNhat = Request.GetTokenIdAdmin();

                    cusRes.DataResult = await Repo.InsertReturnEntity(insEntity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}
