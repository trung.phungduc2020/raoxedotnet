﻿using Common;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using raoVatHistoryApi.ModelsRaoVat;
using System;
using System.Linq;
using System.Threading.Tasks;
using AdminBll = raoVatApi.ModelsRaoVat.AdminBll;
using InterfaceBLL = raoVatApi.ModelsRaoVat.InterfaceBLL;

namespace WebApiMoMo.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class MoMoController : ControllerBase
    {
        // History
        private EntityBaseRepositoryHistoryRaoVat<HistoryPoint> _repoHistoryPoint;
        public EntityBaseRepositoryHistoryRaoVat<HistoryPoint> RepoHistoryPoint { get => _repoHistoryPoint == null ? _repoHistoryPoint = new EntityBaseRepositoryHistoryRaoVat<HistoryPoint>() : _repoHistoryPoint; set => _repoHistoryPoint = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingCode> _repoMarketingCodeMaster;
        public EntityBaseRepositoryRaoVat<MarketingCode> RepoMarketingCodeMaster { get => _repoMarketingCodeMaster == null ? _repoMarketingCodeMaster = new EntityBaseRepositoryRaoVat<MarketingCode>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingCodeMaster; set => _repoMarketingCodeMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUser;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUser { get => _repoMarketingUser == null ? _repoMarketingUser = new EntityBaseRepositoryRaoVat<MarketingUser>() : _repoMarketingUser; set => _repoMarketingUser = value; }
        private EntityBaseRepositoryRaoVat<MarketingUser> _repoMarketingUserMaster;
        public EntityBaseRepositoryRaoVat<MarketingUser> RepoMarketingUserMaster { get => _repoMarketingUserMaster == null ? _repoMarketingUserMaster = new EntityBaseRepositoryRaoVat<MarketingUser>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingUserMaster; set => _repoMarketingUserMaster = value; }

        private EntityBaseRepositoryRaoVat<MarketingPlan> _repoMarketingPlanMaster;
        public EntityBaseRepositoryRaoVat<MarketingPlan> RepoMarketingPlanMaster { get => _repoMarketingPlanMaster == null ? _repoMarketingPlanMaster = new EntityBaseRepositoryRaoVat<MarketingPlan>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoMarketingPlanMaster; set => _repoMarketingPlanMaster = value; }

        private AdminBll _adminBll;
        private InterfaceBLL _interfaceBLL;

        public MoMoController()
        {
            this._adminBll = new AdminBll();
            this._interfaceBLL = new InterfaceBLL(Request);
        }


        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<JObject> HandleMomoSuccess(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();

            // insert history
            await ErrorInsertHistoryPoint(interfaceParam, null, string.Empty, "TEST");

            string signatureRaoXe = string.Empty;
            string errorCode = string.Empty;
            string paymentMethod = "qua ví Momo";
            string body = string.Empty;
            long idThanhVien = 0;

            try
            {
                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - Start");

                string pointRateString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_RATE_POINT_TO_VND);
                interfaceParam.rate = CommonMethods.ConvertToInt32(pointRateString);

                if (interfaceParam.randomToken != "d@ilyx3@12320180720RV")
                {
                    CommonMethods.WriteLogMarketingPlan("Password to connect is wrong, check security problems");

                    // insert history
                    await ErrorInsertHistoryPoint(interfaceParam, null, signatureRaoXe, "Random password wrong");

                    //cusRes.SetException(null, string.Format(Messages.ERR_010, "Record này"));
                    return null;
                }

                var id = CommonMethods.ConvertToInt64(interfaceParam.id);

                // get phần tử cũ
                var editUserPoint = _interfaceBLL.getUserPointById(id);
                if (editUserPoint == null)
                {
                    CommonMethods.WriteLogMarketingPlan("Record is not right, check security problems");
                    // insert history
                    await ErrorInsertHistoryPoint(interfaceParam, editUserPoint, signatureRaoXe, "Userpoint doesn't have Id");

                    //cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                    return null;
                }

                idThanhVien = editUserPoint.IdThanhVien ?? 0;

                if (interfaceParam.errorCode != "0")
                {
                    CommonMethods.WriteLogMarketingPlan("Error Code Wrong From Momo");

                    // insert history
                    await ErrorInsertHistoryPoint(interfaceParam, editUserPoint, signatureRaoXe, interfaceParam.errorCode + "-" + interfaceParam.localMessage);

                    NotificationRequest.sendNotification(idThanhVien.ToString(), "Thanh toán thất bại", interfaceParam.localMessage);

                    //cusRes.SetException(null, string.Format(Messages.ERR_010, "Record này"));
                    return null;
                }

                // Store marketingplan data
                var MarketingCode = editUserPoint.MarketingCode;
                var IdMarketingPlan = editUserPoint.IdMarketingPlan;
                var IdMarketingCode = editUserPoint.IdMarketingCode;
                var Value = editUserPoint.Value;
                var ValueType = editUserPoint.ValueType;
                var MaxValue = editUserPoint.MaxValue;
                var pointTmp = editUserPoint.PointTmp;
                var totalPrice = editUserPoint.TotalPrice;
                var uuid = editUserPoint.Uuid;
                
                var thanhVien = _interfaceBLL.getThanhVienById(idThanhVien);
                var userRank = _interfaceBLL.getUserRankById(thanhVien.IdRank ?? 0);

                if (thanhVien == null)
                {
                    CommonMethods.WriteLogMarketingPlan("ThanhVien is not right, check security problems");
                    cusRes.SetException(null, string.Format(Messages.ERR_001, "Member này"));
                    return null;
                }

                var noteDiscountRanking = string.Empty;
                if (userRank != null && userRank.PercentDiscount.HasValue && userRank.PercentDiscount.Value > 0)
                {
                    interfaceParam.percentDiscount = userRank.PercentDiscount;
                    noteDiscountRanking = "Ưu đãi thành viên [" + userRank.PercentDiscount.Value + "%].";
                }

                long? idRank = thanhVien.IdRank ?? 0;

                // Check Signature
                var checkSignature = PaymentRequest.checkSignature(interfaceParam, out signatureRaoXe);
                if (checkSignature == false)
                {
                    // insert history
                    await ErrorInsertHistoryPoint(interfaceParam, editUserPoint, signatureRaoXe, Messages.ERR_026);

                    NotificationRequest.sendNotification(idThanhVien.ToString(), "Thanh toán bị lỗi", "Liên hệ quản trị hệ thống để biết thêm chi tiết");

                    CommonMethods.WriteLogMarketingPlan("Signature is wrong");
                    //cusRes.SetException(null, Messages.ERR_026);
                    return null;
                }

                // Check TransactionStatus
                var checkTransactionStatus = PaymentRequest.checkTransactionStatus(interfaceParam, out errorCode);
                if (checkTransactionStatus == false)
                {
                    // insert history
                    await ErrorInsertHistoryPoint(interfaceParam, editUserPoint, signatureRaoXe, Messages.ERR_027 + ": ErrorCode=" + errorCode);

                    NotificationRequest.sendNotification(idThanhVien.ToString(), "Thanh toán bị lỗi", "Liên hệ quản trị hệ thống để biết thêm chi tiết");

                    CommonMethods.WriteLogMarketingPlan("CheckTransactionStatus is wrong");
                    //cusRes.SetException(null, Messages.ERR_027);
                    return null;
                }

                var notePromotion = string.Empty;
                // Aprove => Update Point
                if (ValueType == 1)
                {
                    notePromotion = "Khuyến mãi [" + Value + " Point].";
                    editUserPoint.Point = editUserPoint.Point + pointTmp + CommonMethods.ConvertToInt64(Value);
                }
                else
                {
                    if (ValueType == 2)
                    {
                        notePromotion = "Khuyến mãi [" + Value + "%].";
                    }
                    else if (ValueType == 3)
                    {
                        notePromotion = "Khuyến mãi [" + Value + " VND].";
                    }

                    editUserPoint.Point += pointTmp;
                }

                interfaceParam.note = (string.IsNullOrEmpty(notePromotion) ? string.Empty : notePromotion + "<br >")
                                        + (string.IsNullOrEmpty(noteDiscountRanking) ? string.Empty : noteDiscountRanking + "<br >")
                                        + "[ Momo Payment ]";

                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - Start  UpdateAfterAutoClone (Update mua point):");

                editUserPoint.PointTmp = 0;
                editUserPoint.MarketingCode = string.Empty;
                editUserPoint.IdMarketingPlan = -1;
                editUserPoint.Value = string.Empty;
                editUserPoint.ValueType = -1;
                editUserPoint.IdAdminDangKyDuyet = -1;
                editUserPoint.MaxValue = string.Empty;
                editUserPoint.TotalPrice = 0;
                editUserPoint.IdMarketingCode = -1;
                editUserPoint.Uuid = string.Empty;

                // If success => convert to 0 (payment is available)
                editUserPoint.CheckoutType = 0;
                int result = await RepoUserPointMaster.UpdateAfterAutoClone(editUserPoint);
                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End  UpdateAfterAutoClone (Update mua point):" + result);
                if (result > 0)
                {
                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - Start Insert Notification:");
                    // Add to notification
                    InterfaceParam paramNotify = new InterfaceParam();
                    paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                    paramNotify.idThanhVien = editUserPoint.IdThanhVien;
                    paramNotify.ma = Variables.Constants.F003; // Mua point thành công
                    paramNotify.tieuDe = paymentMethod;
                    paramNotify.noiDung = (pointTmp ?? 0).ToString();
                    paramNotify.noiDung1 = CommonMethods.ConvertToCurrency(interfaceParam.amount);

                    CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                    body = await commonController.InsertNotification(paramNotify);
                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End Insert Notification:");
                }
                else
                {
                    NotificationRequest.sendNotification(idThanhVien.ToString(), "Thanh toán bị lỗi", "Cập nhật dữ liệu bị lỗi");

                    await ErrorInsertHistoryPoint(interfaceParam, editUserPoint, signatureRaoXe, "Cập nhật dữ liệu bị lỗi");

                    cusRes.SetException(null, Messages.ERR_019);
                    return null;
                }

                // If use promotion, Insert To MarketingUser
                if (!string.IsNullOrEmpty(MarketingCode))
                {
                    var marketingUser = RepoMarketingUser.Find(a => a.IdThanhVien == editUserPoint.IdThanhVien
                                        && a.IdMarketingPlan == IdMarketingPlan
                                        && a.IdMarketingCode == IdMarketingCode
                                        && a.MarketingCode.ToUpper() == MarketingCode.ToUpper()).Result.FirstOrDefault();

                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - Start marketingUser:");

                    if (marketingUser != null)
                    {
                        NotificationRequest.sendNotification(idThanhVien.ToString(), "Thanh toán bị lỗi", "Chương trình ưu đãi hết hiệu lực");

                        cusRes.SetException(null, Messages.ERR_017);
                        return null;
                    }

                    marketingUser = new MarketingUser();
                    marketingUser.IdThanhVien = editUserPoint.IdThanhVien;
                    marketingUser.IdMarketingPlan = IdMarketingPlan;
                    marketingUser.IdMarketingCode = IdMarketingCode;
                    marketingUser.MarketingCode = MarketingCode.ToUpper();
                    marketingUser.NoiDung = "Mua point: " + pointTmp + " [ " + totalPrice + " ]";
                    marketingUser.Value = Value;
                    marketingUser.ValueType = ValueType;
                    marketingUser.MaxValue = MaxValue;

                    await RepoMarketingUserMaster.Insert(marketingUser);

                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End marketingUser:");

                    // Update TotalUsed MarketingCode
                    await RepoMarketingCodeMaster.UpdateWhere(n => n.IdMarketingPlan == IdMarketingPlan
                            && n.Id == IdMarketingCode
                            && n.Code == MarketingCode, delegate (MarketingCode obj)
                            {
                                obj.TotalUsed = (obj.TotalUsed ?? 0) + 1;
                            });

                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End update RepoMarketingCodeMaster:");
                    // Update TotalUsed MarketingPlan
                    await RepoMarketingPlanMaster.UpdateWhere(n => n.Id == IdMarketingPlan, delegate (MarketingPlan obj)
                    {
                        obj.TotalUsed = (obj.TotalUsed ?? 0) + 1;
                    });
                    CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End update RepoMarketingPlanMaster:");
                }

                long? idNewRank = 0;
                // Upgrade Rank
                CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                cusRes.StrResult = commonMethodsRaoVat.UpgradeRank(editUserPoint.IdThanhVien ?? 0, out idNewRank);
                string noteNewRank = (idNewRank.HasValue && idNewRank  != idRank ? ("New Rank:" + idNewRank) : "Old Rank:" + idRank);

                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End Update Rank:");

                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - Start insert HistoryPoint:");

                // insert một dòng record trong historyPoint
                HistoryPoint insEntity = new HistoryPoint()
                {
                    Type = (int)Variables.TypeUserPoint.Bought,
                    Point = pointTmp,
                    IdThanhVien = editUserPoint.IdThanhVien,
                    NgayTao = DateTime.Now,
                    Note = interfaceParam.note,
                    Rate = interfaceParam.rate,
                    IdRank = idRank,
                    RankPercentDiscount = interfaceParam.percentDiscount,
                    Price = totalPrice,
                    MarketingCode = MarketingCode,
                    IdMarketingPlan = IdMarketingPlan,
                    Value = Value,
                    ValueType = ValueType,
                    MaxValue = MaxValue,
                    IdMarketingCode = IdMarketingCode,
                    OrderId = interfaceParam.orderId,
                    ExtraData = noteNewRank + " || " + interfaceParam.extraData + "|| InterfaceParam:" + CommonMethods.SerializeObject(interfaceParam),
                    Signature = "RaoXe:" + signatureRaoXe + "  MOMO:" + interfaceParam.signature,
                    Message = interfaceParam.message,
                    ErrorCode = interfaceParam.errorCode,
                    OrderType = interfaceParam.orderType,
                    PayType = interfaceParam.payType,
                    ResponseTime = interfaceParam.responseTime,
                    TransId = interfaceParam.transId,
                    Uuid = uuid,
                    RequestId = interfaceParam.requestId
                };
                await RepoHistoryPoint.Insert(insEntity);

                CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End insert HistoryPoint:");

                JObject dataMoMo = new JObject();
                dataMoMo.Add("requestId", interfaceParam.requestId);
                dataMoMo.Add("amount", interfaceParam.amount);
                dataMoMo.Add("point", pointTmp);
                dataMoMo.Add("orderId", interfaceParam.orderId);
                dataMoMo.Add("orderInfo", interfaceParam.orderInfo);
                dataMoMo.Add("orderType", interfaceParam.orderType);
                dataMoMo.Add("transId", interfaceParam.transId);
                dataMoMo.Add("errorCode", interfaceParam.errorCode);
                dataMoMo.Add("message", interfaceParam.message);
                dataMoMo.Add("localMessage", interfaceParam.localMessage);
                dataMoMo.Add("responseTime", interfaceParam.responseTime);
                dataMoMo.Add("extraData", interfaceParam.extraData);

                JObject data = new JObject();
                data.Add("key", "MoMo");
                data.Add("dataMoMo", dataMoMo); // Check status order, but not use.

                NotificationRequest.sendNotification(idThanhVien.ToString(), string.Empty, body, string.Empty, data);
            }
            catch (Exception ex)
            {
                // insert history
                await ErrorInsertHistoryPoint(interfaceParam, null, string.Empty, ex.Message.ToString());

                cusRes.SetException(ex);
            }

            JObject message = new JObject
                {
                    { "partnerCode", Variables.PartnerCode },
                    { "accessKey", Variables.AccessKey },
                    { "requestId", interfaceParam.requestId },
                    { "orderId", interfaceParam.orderId },
                    { "errorCode", interfaceParam.errorCode },
                    { "message", interfaceParam.message },
                    { "responseTime", interfaceParam.responseTime },
                    { "extraData", interfaceParam.extraData },
                    { "signature", signatureRaoXe },
                };

            CommonMethods.WriteLogMarketingPlan("HandleMomoSuccess - End Send Message:");

            return message;
        }

        public async Task ErrorInsertHistoryPoint(InterfaceParam interfaceParam, UserPoint userPoint, string signatureRaoXe, string note)
        {
            // insert một dòng record trong historyPoint
            HistoryPoint failEntity = new HistoryPoint()
            {
                Type = note.ToLower() == "test" ? (int)Variables.TypeUserPoint.DoNothing : (int)Variables.TypeUserPoint.Error,
                Point = userPoint != null ? userPoint.PointTmp : 0,
                IdThanhVien = userPoint != null ? userPoint.IdThanhVien : 0,
                NgayTao = DateTime.Now,
                Note = note,
                Rate = interfaceParam.rate,
                IdRank = interfaceParam.idRank,
                RankPercentDiscount = interfaceParam.percentDiscount,
                Price = userPoint != null ? userPoint.TotalPrice : 0,
                MarketingCode = userPoint != null ? userPoint.MarketingCode: string.Empty,
                IdMarketingPlan = userPoint != null ? userPoint.IdMarketingPlan : 0,
                Value = userPoint != null ? userPoint.Value : string.Empty,
                ValueType = userPoint != null ? userPoint.ValueType : 0,
                MaxValue = userPoint != null ? userPoint.MaxValue : string.Empty,
                IdMarketingCode = userPoint != null ? userPoint.IdMarketingCode: 0,
                OrderId = interfaceParam.orderId,
                ExtraData = interfaceParam.extraData + "|| InterfaceParam:" + CommonMethods.SerializeObject(interfaceParam),
                Signature = "RaoXe:" + signatureRaoXe + "  MOMO:" + interfaceParam.signature,
                Message = interfaceParam.message,
                ErrorCode = interfaceParam.errorCode,
                OrderType = interfaceParam.orderType,
                PayType = interfaceParam.payType,
                ResponseTime = interfaceParam.responseTime,
                TransId = interfaceParam.transId,
                Uuid = userPoint != null ? userPoint.Uuid : string.Empty,
                RequestId = interfaceParam.requestId
            };

            await RepoHistoryPoint.Insert(failEntity);
        }
    }
}
