﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class UserRankController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<UserRank> _repo;
        public EntityBaseRepositoryRaoVat<UserRank> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<UserRank>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<UserRank> _repoUserRankMaster;
        public EntityBaseRepositoryRaoVat<UserRank> RepoUserRankMaster { get => _repoUserRankMaster == null ? _repoUserRankMaster = new EntityBaseRepositoryRaoVat<UserRank>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserRankMaster; set => _repoUserRankMaster = value; }

        public UserRankController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) : null;
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.displayPage = interfaceParam.displayPage <= 0 ? 1 : interfaceParam.displayPage;
                interfaceParam.tuKhoaTimKiem = interfaceParam.tuKhoaTimKiem.ToASCIIAndLower();
                interfaceParam.orderString = string.IsNullOrEmpty(interfaceParam.orderString) ? "Id:desc" : interfaceParam.orderString;

                // Search & Get data
                IEnumerable<UserRank> data = await _adminBll.SearchUserRank(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserRank entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    entity.NgayTao = DateTime.Now;
                    entity.TuKhoaTimKiem = BuildTuKhoaTimKiemUserRank(entity);
                    cusRes.DataResult = await RepoUserRankMaster.InsertReturnEntity(entity);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserRank newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    UserRank editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin tức này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        newEntity.NgayCapNhat = DateTime.Now;
                        newEntity.NgayTao = editEntity.NgayTao;
                        newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiemUserRank(newEntity);

                        cusRes.IntResult = await RepoUserRankMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoUserRankMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (UserRank obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        public string BuildTuKhoaTimKiemUserRank(UserRank entity)
        {
            string res = string.Empty;
            if (entity != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(entity.TieuDe)
                + "," + CommonMethods.ConvertUnicodeToASCII(entity.MoTaNgan)
                + "," + entity.PercentDiscount.ToString();
            }
            return res;
        }

        #endregion
    }
}
