﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class OnlineStatisticsController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<OnlineStatistics> _repo;
        public EntityBaseRepositoryRaoVat<OnlineStatistics> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<OnlineStatistics>(Request) : _repo; set => _repo = value; }

        public OnlineStatisticsController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param

                // Search & Get data
                IEnumerable<OnlineStatistics> data = await _adminBll.SearchOnlineStatistics(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }
    }
}