﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class UserGroupController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<UserGroup> _repo;
        public EntityBaseRepositoryRaoVat<UserGroup> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<UserGroup>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<UserGroup> _repoUserGroupMaster;
        public EntityBaseRepositoryRaoVat<UserGroup> RepoUserGroupMaster { get => _repoUserGroupMaster == null ? _repoUserGroupMaster = new EntityBaseRepositoryRaoVat<UserGroup>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserGroupMaster; set => _repoUserGroupMaster = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }

        public UserGroupController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.displayPage = interfaceParam.displayPage <= 0 ? 1 : interfaceParam.displayPage;
                interfaceParam.tuKhoaTimKiem = interfaceParam.tuKhoaTimKiem.ToASCIIAndLower();
                interfaceParam.orderString = string.IsNullOrEmpty(interfaceParam.orderString) ? "Id:desc" : interfaceParam.orderString;
                interfaceParam.token = Request.GetTokenFromHeader();

                // Search & Get data
                IEnumerable<UserGroup> data = await _adminBll.SearchUserGroup(interfaceParam);
                cusRes.DataResult = data;

                // Get property name list
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserGroup entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    entity.NgayTao = DateTime.Now;
                    entity.TuKhoaTimKiem = BuildTuKhoaTimKiemUserGroup(entity);
                    var returnEntity = await RepoUserGroupMaster.InsertReturnEntity(entity);
                    //cusRes.DataResult = returnEntity;
                    long newIdUserGroup = returnEntity.FirstOrDefault().Id;
                    if (!string.IsNullOrEmpty(entity.LstIdUser_Ext))
                    {
                        var lstUser = entity.LstIdUser_Ext.Split(",");
                        await RepoThanhVienMaster.UpdateWhere(s => (lstUser != null && lstUser.Contains(s.Id.ToString())), delegate (ThanhVien obj)
                        {
                            var idGroup = string.IsNullOrEmpty(obj.IdGroups) ? newIdUserGroup.ToString() : obj.IdGroups + "," + newIdUserGroup.ToString();
                            obj.IdGroups = idGroup;
                        });
                    }
                    InterfaceParam interfaceParam = new InterfaceParam();
                    interfaceParam.allIncludingString = "true";
                    interfaceParam.lstId = new List<long> { newIdUserGroup };
                    interfaceParam.token = Request.GetTokenFromHeader();
                    var returnData = await _adminBll.SearchUserGroup(interfaceParam);
                    cusRes.DataResult = returnData;

                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserGroup newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    UserGroup editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Record này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;
                        newEntity.NgayCapNhat = DateTime.Now;
                        newEntity.NgayTao = editEntity.NgayTao;
                        newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiemUserGroup(newEntity);
                        cusRes.IntResult = await RepoUserGroupMaster.UpdateAfterAutoClone(newEntity);

                        InterfaceParam interfaceParam = new InterfaceParam();
                        interfaceParam.lstId = new List<long> { id };
                        interfaceParam.allIncludingString = "true";
                        interfaceParam.token = Request.GetTokenFromHeader();
                        var returnData = await _adminBll.SearchUserGroup(interfaceParam);
                        cusRes.DataResult = returnData;
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoUserGroupMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (UserGroup obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> AddUserToGroup()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idGroup = string.Empty;
                string idThanhVien = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idGroup = CommonMethods.ConvertToString(queryParams[CommonParams.idGroup]);
                    idThanhVien = CommonMethods.ConvertToString(queryParams[CommonParams.idThanhVien]);

                    long longIdThanhVien = CommonMethods.ConvertToInt64(idThanhVien);
                    
                    cusRes.IntResult = await RepoThanhVienMaster.UpdateWhere(s => s.Id == longIdThanhVien, delegate (ThanhVien obj)
                    {
                        if (string.IsNullOrEmpty(obj.IdGroups))
                        {
                            obj.IdGroups = idGroup.ToString();
                        }
                        else
                        {
                            var splitIdGroup = obj.IdGroups.Split(",");
                            if (!splitIdGroup.Contains(idGroup.ToString()))
                            {
                                obj.IdGroups = obj.IdGroups + "," + idGroup.ToString();
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> RemoveUserFromGroup()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idGroup = string.Empty;
                string idThanhVien = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idGroup = CommonMethods.ConvertToString(queryParams[CommonParams.idGroup]);
                    idThanhVien = CommonMethods.ConvertToString(queryParams[CommonParams.idThanhVien]);

                    long longIdThanhVien = CommonMethods.ConvertToInt64(idThanhVien);
                    cusRes.IntResult = await RepoThanhVienMaster.UpdateWhere(s => s.Id == longIdThanhVien, delegate (ThanhVien obj)
                    {
                        if (!string.IsNullOrEmpty(obj.IdGroups))
                        {
                            // [6,7,8] => -6
                            var splitIdGroup = obj.IdGroups.Split(",");
                            var newIdGroup = String.Join(",", splitIdGroup.Where(a => !a.Contains(idGroup.ToString())));
                            obj.IdGroups = newIdGroup;
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        public string BuildTuKhoaTimKiemUserGroup(UserGroup entity)
        {
            string res = string.Empty;
            if (entity != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(entity.Title);
            }
            return res;
        }

        #endregion
    }
}