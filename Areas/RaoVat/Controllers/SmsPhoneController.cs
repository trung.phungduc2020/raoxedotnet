﻿using GmailApi;
using Microsoft.AspNetCore.Mvc;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Areas.RaoVat.Controllers
{
    [Route(Variables.C_ApiRouting_2)]
    public class SmsPhoneController : Controller
    {
        //ApiGetDal _apiGetDal = new ApiGetDal();
        //public ApiGetDal ApiGetDal { get => _apiGetDal == null ? new ApiGetDal() : _apiGetDal; set => _apiGetDal = value; }

        private AdminBll _adminBll;

        public SmsPhoneController()
        {
            this._adminBll = new AdminBll();
        }

        /// <summary>
        /// interfaceParam.loaiXacNhan (1: đăng ký, 2: quên mật khẩu, 3: cập nhật thông tin cá nhân): nếu không được truyền lên mặc định là 1
        /// interfaceParam.checkPhoneExist (mặc định là true): kiểm tra sdt đó có trong db dailyxe không ròi mới gửi sms
        /// gửi tin nhắn đến interfaceParam.dienThoai
        /// </summary>
        /// <param name="interfaceParam" type="string">chứa nhiều tham số truyền vào</param>
        /// <returns>
        /// "IntResult": 1 -> thành công
        /// "IntResult": -3 -> số điện thoại không tồn tại trong db dailyxe
        /// "IntResult": -4 -> đã gửi mã xác thực trước đó (sẽ không gửi lại mã nữa)
        /// "IntResult": -2 -> lỗi khác
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> SendSms(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                CommonMethodsRaoVat commonMethodsRaoVat = new CommonMethodsRaoVat();
                string soDienThoai = interfaceParam.dienThoai;
                int loaiXacNhan = interfaceParam.loaiXacNhan ?? 1;

                // người dùng có truyền số điện thoại lên hay không
                if (string.IsNullOrEmpty(soDienThoai))
                {
                    cusRes.SetException(new Exception(Messages.ERR_002));
                    return Response.OkObjectResult(cusRes);
                }

                #region "kiểm tra dữ liệu trước khi gửi tin nhắn

                // Tìm trong db -> đã gửi mã xác thực chưa
                EntityBaseRepository<MaXacNhan> repoMaXacNhan = new EntityBaseRepository<MaXacNhan>(new RaoVatContext_Custom(), Request);
                IEnumerable<MaXacNhan> sendedCode = await repoMaXacNhan.Search(n => n.Phone == soDienThoai && n.LoaiXacNhan == loaiXacNhan, "NgayCap:Desc", null);

                // 1. Nếu đã gửi ròi thì kiểm tra xem có hết hạn chưa
                // 2. Alway get lastest code
                string durationValidString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_DURATIONVERIFYISVALID);
                if (sendedCode != null && sendedCode.Count() > 0)
                {
                    MaXacNhan lastedRecord = sendedCode.First();
                    DateTime sendedCodeAt = lastedRecord.NgayCap ?? DateTime.MinValue;
                    TimeSpan time = DateTime.Now - sendedCodeAt;
                    double durationValid = CommonMethods.ConvertToDouble(durationValidString);// theo seconds
                    TimeSpan duration = TimeSpan.FromSeconds(durationValid);
                    if (time < duration)
                    {
                        cusRes.Message = string.Format(Messages.ERR_008);
                        cusRes.IntResult = -4;
                        return Response.OkObjectResult(cusRes);
                    }
                }

                // xin mã 6 số
                string code = CommonMethods.GenerateRandomSixDigital();

                EntityBaseRepository<MaXacNhan> repoMaXacNhanMaster = new EntityBaseRepository<MaXacNhan>(new RaoVatContext_Custom(Variables.ConnectionStringPostgresql_RaoVat_Master), Request);

                MaXacNhan insertEntity = new MaXacNhan()
                {
                    Code = code,
                    NgayCap = DateTime.Now,
                    LoaiXacNhan = loaiXacNhan,
                    Phone = soDienThoai
                };

                await repoMaXacNhanMaster.InsertReturnEntity(insertEntity);

                #endregion

                #region "gửi sms"

                string activeSendSMSPhone = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_ACTIVESENDSMSPHONE);
                if (activeSendSMSPhone == "1")
                {
                    string result = string.Empty;
                    if (!commonMethodsRaoVat.SendSms(soDienThoai, code, out result))
                    {
                        cusRes.SetException(new Exception(result));
                        return Response.OkObjectResult(cusRes);
                    }
                }

                #endregion

                cusRes.IntResult = 1;
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCodeSms(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string soDienThoai = interfaceParam.dienThoai;
                int loaiXacNhan = interfaceParam.loaiXacNhan ?? 1;

                // người dùng có truyền số điện thoại lên hay không
                if (string.IsNullOrEmpty(soDienThoai))
                {
                    cusRes.SetException(new Exception(Messages.ERR_002));
                    return Response.OkObjectResult(cusRes);
                }

                // Tìm trong db -> đã gửi mã xác thực chưa
                EntityBaseRepository<MaXacNhan> repoMaXacNhan = new EntityBaseRepository<MaXacNhan>(new RaoVatContext_Custom(), Request);
                IEnumerable<MaXacNhan> sendedCode = await repoMaXacNhan.Search(n => n.Phone == soDienThoai && n.LoaiXacNhan == loaiXacNhan, "NgayCap:Desc", null);

                // 1. Nếu đã gửi ròi thì kiểm tra xem có hết hạn chưa
                // 2. Alway get lastest code
                string durationValidString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_DURATIONVERIFYISVALID);
                if (sendedCode != null && sendedCode.Count() > 0)
                {
                    MaXacNhan lastedRecord = sendedCode.First();
                    DateTime sendedCodeAt = lastedRecord.NgayCap ?? DateTime.MinValue;
                    TimeSpan time = DateTime.Now - sendedCodeAt;
                    double durationValid = CommonMethods.ConvertToDouble(durationValidString);// theo seconds
                    TimeSpan duration = TimeSpan.FromSeconds(durationValid);
                    if (time < duration)
                    {
                        cusRes.Message = string.Format(Messages.ERR_008);
                        cusRes.IntResult = -4;
                        return Response.OkObjectResult(cusRes);
                    }
                }

                // xin mã 6 số
                string code = CommonMethods.GenerateRandomSixDigital();

                EntityBaseRepository<MaXacNhan> repoMaXacNhanMaster = new EntityBaseRepository<MaXacNhan>(new RaoVatContext_Custom(Variables.ConnectionStringPostgresql_RaoVat_Master), Request);

                #region "lưu mã xác thực vào bảng mã xác nhận"

                MaXacNhan insertEntity = new MaXacNhan()
                {
                    Code = code,
                    NgayCap = DateTime.Now,
                    LoaiXacNhan = loaiXacNhan,
                    Phone = soDienThoai
                };

                await repoMaXacNhanMaster.InsertReturnEntity(insertEntity);

                #endregion

                cusRes.IntResult = 1;
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        /// <summary>
        /// trả về thời gian còn lại của mã xác nhận mới nhất theo interfaceParam.dienThoai
        /// </summary>
        /// <param name="interfaceParam" type="string">chứa nhiều tham số truyền vào</param>
        /// <returns>
        /// "IntResult": 1 -> thành công: trả second còn lại
        /// "IntResult": -3 -> không tìm thấy record theo số điện thoại trong bảng mã xác nhận dbraovat
        /// "IntResult": -4 -> mã xác thực mới nhất theo số điện thoại đã hết hạn
        /// "IntResult": -2 -> lỗi khác
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> TimeRemainOfMaXacThuc(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // chuẩn bị dữ
                string soDienThoai = interfaceParam.dienThoai;

                // người dùng có truyền số điện thoại lên hay không
                if (string.IsNullOrEmpty(soDienThoai))
                {
                    cusRes.SetException(new Exception(Messages.ERR_002));
                    return Response.OkObjectResult(cusRes);
                }

                // lấy dữ liệu từ db rao vặt
                EntityBaseRepository<MaXacNhan> repoMaXacNhan = new EntityBaseRepository<MaXacNhan>(new RaoVatContext_Custom(), Request);
                IEnumerable<MaXacNhan> sendedCode = await repoMaXacNhan.Search(n => n.Phone == soDienThoai, "NgayCap:Desc", null);
                MaXacNhan lastedRecord = sendedCode.FirstOrDefault();
                if (lastedRecord == null)
                {
                    cusRes.Message = Messages.ERR_004;
                    cusRes.IntResult = -3;
                    return Response.OkObjectResult(cusRes);
                }

                // tính thời gian 
                if (lastedRecord.NgayCap != null)
                {
                    DateTime ngayCap = lastedRecord.NgayCap.Value;
                    string durationValidString = await _adminBll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_DURATIONVERIFYISVALID);
                    double durationValid = CommonMethods.ConvertToDouble(durationValidString);// theo seconds
                    TimeSpan durationRemain = TimeSpan.FromSeconds(durationValid);
                    TimeSpan remainTime = durationRemain - (DateTime.Now - ngayCap);
                    Debug.WriteLine("remainTime: " + remainTime + " now: " + DateTime.Now + " ngayCap: " + ngayCap + " duration remain time " + durationRemain);
                    if (remainTime <= TimeSpan.FromSeconds(0))
                    {
                        cusRes.Message = Messages.ERR_009;
                        cusRes.IntResult = -4;
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        cusRes.StrResult = remainTime.TotalSeconds.ToString("N0");
                        return Response.OkObjectResult(cusRes);
                    }
                }
                else
                {
                    cusRes.SetException(new Exception(String.Format(Messages.ERR_001, "Ngày cấp")));
                    return Response.OkObjectResult(cusRes);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}
