﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class DangTinViolationController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<DangTinViolation> _repo;
        public EntityBaseRepositoryRaoVat<DangTinViolation> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<DangTinViolation>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<DangTinViolation> _repoDangTinViolationMaster;
        public EntityBaseRepositoryRaoVat<DangTinViolation> RepoDangTinViolationMaster { get => _repoDangTinViolationMaster == null ? _repoDangTinViolationMaster = new EntityBaseRepositoryRaoVat<DangTinViolation>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinViolationMaster; set => _repoDangTinViolationMaster = value; }

        public DangTinViolationController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);
                interfaceParam.allIncluding = CommonMethods.ConvertToBoolean(interfaceParam.allIncludingString);

                // Search & Get data
                IEnumerable<DangTinViolation> data = await _adminBll.SearchDangTinViolation(interfaceParam);
                cusRes.DataResult = data;

                // 
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetUserDangTinViolation()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Search & Get data
                IEnumerable<Dictionary<string, object>> data = await _adminBll.SearchUserDangTinViolation();
                cusRes.DataResult = data;

                // 
                //int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                //cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] DangTinViolation newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    DangTinViolation editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin tức này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;

                        cusRes.IntResult = await RepoDangTinViolationMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoDangTinViolationMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (DangTinViolation obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }
    }
}
