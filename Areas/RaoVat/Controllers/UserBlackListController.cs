﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Controllers
{
    [Authorize]
    [Route(Variables.C_ApiRouting_1)]
    public class UserBlackListController : Controller
    {
        private IMemoryCache _cache;
        private AdminBll _adminBll;
        private EntityBaseRepositoryRaoVat<UserBlackList> _repo;
        public EntityBaseRepositoryRaoVat<UserBlackList> Repo { get => _repo == null ? _repo = new EntityBaseRepositoryRaoVat<UserBlackList>(Request) : _repo; set => _repo = value; }

        private EntityBaseRepositoryRaoVat<UserBlackList> _repoUserBlackListMaster;
        public EntityBaseRepositoryRaoVat<UserBlackList> RepoUserBlackListMaster { get => _repoUserBlackListMaster == null ? _repoUserBlackListMaster = new EntityBaseRepositoryRaoVat<UserBlackList>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserBlackListMaster; set => _repoUserBlackListMaster = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>(Request) : _repoThanhVien; set => _repoThanhVien = value; }

        public UserBlackListController(IMemoryCache memoryCache)
        {
            this._cache = memoryCache;
            this._adminBll = new AdminBll();
        }

        [HttpGet]
        public async Task<IActionResult> Get(InterfaceParam interfaceParam)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                // Handle param
                interfaceParam.lstId = !string.IsNullOrEmpty(interfaceParam.ids) ? CommonMethods.SplitStringToInt64(interfaceParam.ids) :
                    CommonMethods.SplitStringToInt64(interfaceParam.id);
                interfaceParam.lstNotId = !string.IsNullOrEmpty(interfaceParam.notIds) ? CommonMethods.SplitStringToInt64(interfaceParam.notIds) : null;
                interfaceParam.lstTrangThai = CommonMethods.SplitStringToInt(interfaceParam.trangThai, true);

                // Search & Get data
                IEnumerable<UserBlackList> data = await _adminBll.SearchUserBlacklist(interfaceParam);
                cusRes.DataResult = data;

                // 
                int totalRows = data != null ? data.FirstOrNewObject().TotalRow : 0;
                cusRes.AddPagination(interfaceParam.displayPage, interfaceParam.displayItems, data != null ? data.Count() : 0, totalRows);
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserBlackList entity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (entity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    entity.TuKhoaTimKiem = BuildTuKhoaTimKiem(entity);
                    cusRes.DataResult = await RepoUserBlackListMaster.InsertReturnEntity(entity);

                    // Add to notification
                    InterfaceParam paramNotify = new InterfaceParam();
                    paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                    paramNotify.idThanhVien = entity.IdThanhVien;
                    paramNotify.ma = Variables.Constants.F006; // Warning
                    paramNotify.noiDung = (entity.TuNgay.HasValue ? entity.TuNgay.Value.ToString(Variables.DD_MM_YYYY_FULL) : string.Empty);
                    paramNotify.noiDung1 = (entity.DenNgay.HasValue ? entity.DenNgay.Value.ToString(Variables.DD_MM_YYYY_FULL) : string.Empty);
                    paramNotify.noiDung2 = entity.GhiChu;
                    var thanhVien = RepoThanhVien.Find(a => a.Id == entity.IdThanhVien).Result.FirstOrDefault();
                    paramNotify.email = thanhVien != null && !string.IsNullOrEmpty(thanhVien.Email) ? thanhVien.Email : string.Empty;

                    CommonMethodsRaoVat commonController = new CommonMethodsRaoVat();
                    await commonController.InsertNotification(paramNotify);
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserBlackList newEntity)
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                if (newEntity == null)
                {
                    cusRes.SetException(null, Messages.ERR_002);
                    return Response.OkObjectResult(cusRes);
                }
                else
                {
                    UserBlackList editEntity = await Repo.GetSingle(id);
                    if (editEntity == null)
                    {
                        cusRes.SetException(null, string.Format(Messages.ERR_001, "Tin tức này"));
                        return Response.OkObjectResult(cusRes);
                    }
                    else
                    {
                        // Tránh tình trạng newEntity sai id trên đường dẫn
                        newEntity.Id = id;
                        newEntity.NgayCapNhat = DateTime.Now;
                        newEntity.TuKhoaTimKiem = BuildTuKhoaTimKiem(newEntity);

                        cusRes.IntResult = await RepoUserBlackListMaster.UpdateAfterAutoClone(newEntity);
                        cusRes.DataResult = await Repo.Find(s => s.Id == id);
                    }
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateTrangThai()
        {
            CustomResult cusRes = new CustomResult();
            try
            {
                string idsString = string.Empty;
                string trangThaiString = string.Empty;

                IQueryCollection queryParams = Request.Query;
                if (queryParams != null)
                {
                    idsString = CommonMethods.ConvertToString(queryParams[CommonParams.id]); ;
                    if (string.IsNullOrEmpty(idsString))
                    {
                        idsString = CommonMethods.ConvertToString(queryParams[CommonParams.ids]);
                    }
                    trangThaiString = CommonMethods.ConvertToString(queryParams[CommonParams.trangThai]);

                    Byte trangThai = CommonMethods.ConvertToByte(trangThaiString);
                    List<long> lstId = CommonMethods.SplitStringToInt64(idsString);
                    cusRes.IntResult = await RepoUserBlackListMaster.UpdateWhere(s => (lstId != null && lstId.Contains(s.Id)), delegate (UserBlackList obj)
                    {
                        obj.TrangThai = trangThai;
                    });
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }
            return Response.OkObjectResult(cusRes);
        }

        #region "private"

        public string BuildTuKhoaTimKiem(UserBlackList entity)
        {
            string res = string.Empty;
            if (entity != null)
            {
                res += CommonMethods.ConvertUnicodeToASCII(entity.TenThanhVien)
                + "," + CommonMethods.ConvertUnicodeToASCII(entity.DienThoai)
                + "," + CommonMethods.ConvertUnicodeToASCII(entity.GhiChu);
            }
            return res;
        }

        #endregion
    }
}
