using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using raoVatApi.AppCodesRaoVat;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControllersFileManager
{
    [Route("api/RaoVat/[controller]")]
    // Mô phỏng giống bên localhost:5000 của source RaoVatui để trả urlFile về cho Hữu xử lý
    public class CommonFileController : Controller
    {
        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinMaster;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinMaster { get => _repoDangTinMaster == null ? _repoDangTinMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinMaster; set => _repoDangTinMaster = value; }

        private AdminBll _adminBLL;
        private AdminBll AdminBLL
        {
            get
            {
                //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
                //if (_adminBLL == null)
                {
                    _adminBLL = new AdminBll();
                }
                return _adminBLL;
            }
        }

        private InterfaceBLL _interfaceBLL;
        private InterfaceBLL InterfaceBLL
        {
            get
            {
                //nguyencuongcs 20180316: tắt để test api có còn bị đơ đơ ko
                //if (_interfaceBLL == null)
                {
                    _interfaceBLL = new InterfaceBLL(null);
                }
                return _interfaceBLL;
            }
        }

        private readonly IViewRenderService _viewRenderService;
        public CommonFileController(IViewRenderService viewRenderService)
        {
            //this._cache = Variables.MemoryCache;
            this._viewRenderService = viewRenderService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateFileChiTietDangTin([FromBody]Dictionary<string, object> body)
        {
            CustomResult cusRes = new CustomResult();
            cusRes.IntResult = 0;

            try
            {
                string id = @"";
                string noiDung = @"";
                bool isJsonData = false;
                string shortDescription = null;

                // IQueryCollection queryParams = Request.Query;

                if (body != null)
                {
                    id = CommonMethods.ConvertToString(body.GetValueFromKey(CommonParams.id, null));
                    noiDung = CommonMethods.ConvertToStringDefaultNull(body.GetValueFromKeyDefaultNull(CommonParams.noiDung, null));
                    if(noiDung != null)
                    {
                        shortDescription = CommonMethods.SmartSubstr(noiDung, Variables.MaxLengthShortDescription);
                    }
                    //shortDescription = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(cropText);
                }

                if (!string.IsNullOrEmpty(id)) // Dùng string để tạo được các filename khác số nếu cần
                {
                    Int64 longId = CommonMethods.ConvertToInt64(id);
                    if(shortDescription != null)
                    {
                        await RepoDangTinMaster.UpdateWhere(n => n.Id == longId, delegate (DangTin obj)
                        {
                            obj.MoTaNgan = shortDescription;
                            obj.MoTaChiTiet = noiDung;
                        });
                    }

                    FileData fd = new FileData();
                    await fd.CreateChiTietDangTin(id, noiDung, isJsonData);
                    cusRes.IntResult = 1;
                }
                else
                {
                    cusRes.Message = string.Format(Messages.ERR_005, "Id");
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }

            return new OkObjectResult(CommonMethods.SerializeToJSON(cusRes));
        }

        [HttpGet("[action]")]
        public async Task<string> GetChiTietDangTin()
        {
            string res = string.Empty;

            try
            {
                string id = @"";

                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    id = CommonMethods.ConvertToString(queryParams[CommonParams.id]);

                    if (!string.IsNullOrEmpty(id))
                    {
                        FileData fd = new FileData();
                        res = await fd.GetChiTietDangTin(id);
                    }
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }
            return CommonMethods.SerializeToJSON(res);
        }

        #region "Encrypt & Decrypt"

        [HttpGet("[action]")]
        public string GetEncryptMD5()
        {
            string res = string.Empty;

            try
            {
                string searchString = @"";

                IQueryCollection queryParams = Request.Query;

                if (queryParams != null)
                {
                    searchString = CommonMethods.ConvertToString(queryParams[CommonParams.searchString]);
                }

                if (!string.IsNullOrEmpty(searchString))
                {
                    res = CommonMethods.GetEncryptMD5(searchString);
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return res;
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> EncryptToken()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string data = bodyJson.GetValueFromKey_ReturnString(CommonParams.data, "");
                string publicKey = bodyJson.GetValueFromKey_ReturnString(CommonParams.publicKey, "");
                string res = CommonMethods.EncryptRSA(data);

                cusRes.StrResult = res;
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                cusRes.SetException(ex);
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> DecryptToken()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string data = bodyJson.GetValueFromKey_ReturnString(CommonParams.data, "");
                string res = CommonMethods.DecryptRSA(data);

                cusRes.StrResult = res;
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                cusRes.SetException(ex);
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> EncryptData()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string data = bodyJson.GetValueFromKey_ReturnString(CommonParams.data, "");
                string publicKey = bodyJson.GetValueFromKey_ReturnString(CommonParams.publicKey, "");

                string encryptedData = CommonMethods.Encrypt3DES(data, publicKey);
                string encryptedKey = CommonMethods.EncryptRSA(publicKey);

                object objRes = new { at = encryptedData, uk = encryptedKey };

                cusRes.StrResult = encryptedData;
                cusRes.DataResult = new List<object>() { objRes };
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                cusRes.SetException(ex);
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> DecryptData()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string data = bodyJson.GetValueFromKey_ReturnString(CommonParams.data, "");
                string publicKey = bodyJson.GetValueFromKey_ReturnString(CommonParams.publicKey, "");

                string decryptedKey = CommonMethods.DecryptRSA(publicKey);
                string decryptedData = CommonMethods.Decrypt3DES(data, decryptedKey);

                object objRes = new { at = decryptedData }; //uk = decryptedKey 

                //cusRes.StrResult = decryptedData;
                cusRes.DataResult = new List<object>() { objRes };
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                cusRes.SetException(ex);
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> DecryptDataForUI()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                string data = bodyJson.GetValueFromKey_ReturnString(CommonParams.data, "");
                string publicKey = bodyJson.GetValueFromKey_ReturnString(CommonParams.publicKey, "");

                string decryptedKey = CommonMethods.DecryptRSAForUI(publicKey);
                string decryptedData = CommonMethods.Decrypt3DES(data, decryptedKey);

                object objRes = new { at = decryptedData }; //uk = decryptedKey 

                //cusRes.StrResult = decryptedData;
                cusRes.DataResult = new List<object>() { objRes };
            }
            catch (Exception ex)
            {
                string strEx = ex.ToString();
                cusRes.SetException(ex);
            }

            // nguyencuongcs 20180325
            //return CommonMethods.SerializeToJSON(res);
            return Response.OkObjectResult(cusRes);
        }

        #endregion

        #region "Config remote app settings"

        [HttpPost("[action]")]
        public IActionResult CheckConnect()
        {
            CustomResult cusRes = new CustomResult();
            object checkConnectRedis;
            object checkGoogleDriveFileStream;

            Dictionary<string, object> dicRes = new Dictionary<string, object>();
            try
            {
                checkConnectRedis = CommonMethods.CheckConnectToRedisServer();
                dicRes.Add("checkConnectRedis", CommonMethods.SerializeObject(checkConnectRedis));

                string res = "";
                try
                {
                    RawSqlRepository rawSql = new RawSqlRepository(new RaoVatContext_Custom(Variables.ConnectionStringPostgresql_RaoVat_SlaveForTest));
                    string dbName = rawSql.DatabaseName;
                    string sql = @"select Current_Timestamp";
                    res = $"dbName: {dbName} - select Current_Timestamp: {rawSql.ExecuteSqlQuery_ReturnString(sql).Result}";

                    CommonMethods.ResetPostgresSlaveVariables();

                }
                catch (Exception ex)
                {
                    res = ex.ToString();
                }
                dicRes.Add("checkConnectSlaveForTest", res);

                try
                {
                    res = "";
                    RaoVatContext_Custom context = new RaoVatContext_Custom(Variables.ConnectionStringPostgresql_RaoVat_SlaveForTest);

                    IEnumerable<ThanhVien> lstDeletedAdmin = context.ThanhVien.ToList();
                    List<long> lstRes = lstDeletedAdmin != null ? lstDeletedAdmin.Select(s => s.Id).ToList() : new List<long>();
                    res =  lstRes != null && lstRes.Count > 0 ? string.Join(",", lstRes) : "";

                    CommonMethods.ResetPostgresSlaveVariables();
                }
                catch (Exception ex)
                {
                    res = ex.ToString();
                }
                dicRes.Add("checkConnectSlaveForTest RaoVatContext_Custom", res);

                cusRes.DataResult = new List<Dictionary<string, object>>() { dicRes };
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> GetAppSetting()
        {
            CustomResult cusRes = new CustomResult();
            string appSettingName;
            object appSettingData;

            Dictionary<string, object> dicRes = new Dictionary<string, object>();
            try
            {
                FileData fd = new FileData();

                appSettingName = Variables.Environment;
                appSettingData = await fd.GetContentFile_NoXml(Variables.AppSettingPathFile);

                string gitInfo = $"GitBranch: {ThisAssembly.Git.Branch} - GitCommit: {ThisAssembly.Git.Commit} - GitSha: {ThisAssembly.Git.Sha}";

                dicRes.Add("Git info", $"{gitInfo}");
                dicRes.Add("Git commits", $"{ThisAssembly.Git.Commits}");
                dicRes.Add("Variables.DockerImageName", Variables.BuildImageName);
                dicRes.Add("Variables.DockerStartTime", Variables.BuildStartTime);                
                dicRes.Add("appSettingName", appSettingName);
                dicRes.Add("appSettingData", CommonMethods.DeserializeObject<Dictionary<string, object>>(appSettingData));
                dicRes.Add("Variables.Postgresql_RaoVat_Slave_IsHealthy", Variables.Postgresql_RaoVat_Slave_IsHealthy);

                cusRes.DataResult = new List<Dictionary<string, object>>() { dicRes };
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost("[action]")]
        public IActionResult ReloadAppSetting()
        {
            CustomResult cusRes = new CustomResult();

            try
            {

                // nguyencuongcs 20190911: nâng cấp, reload từ xa khi nhận dicSettingName
                // ghi đè file appsetting & reload lại dữ liệu từ file
                //if (dicSettingName != null)
                {
                    // ghi đề file appsetting theo các key có trong dicSettingName này
                }

                string body = Request.GetRawBodyStringAsync().Result;
                Dictionary<string, object> dicBody = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                // Kiểm tra là admin (đã đăng nhập & còn session || cookie) hoặc mật mã do nguyencuongcs chạy bằng postman
                string matMa = dicBody.GetValueFromKey_ReturnString(CommonParams.token_password, "");
                bool isChecked = false;

                if (matMa.Equals(Variables.Postman_Password))
                {
                    isChecked = true;
                    // do nothing, để khỏi vô hàm CheckAdmin() bên dưới
                }
                else
                {
                    if (!Request.CheckIsAdmin())
                    {
                        // Hệ thống admin khi nhận kết quả này sẽ tự redirect về trang login
                        cusRes.SetExceptionInvalidAccount();
                    }
                    else
                    {
                        isChecked = true;
                    }
                }

                isChecked = true;
                if (isChecked)
                {

                    string res = CommonMethods.LoadAppSetting();

                    // nguyencuongcs 20191108: reset biến này để lấy mới từ CauHinhHeThong
                    Variables.ApiDailyXeFarm_ListWebServer = null;
                    // cái này chưa xài nhưng reset sẵn
                    Variables.WebFarm_ListWebServer = null;

                    cusRes.StrResult = res;
                }
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CheckRemoteConfig()
        {
            CustomResult cusRes = new CustomResult();
            string res = "";
            
            string body = await Request.GetRawBodyStringAsync();            

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);                

                string id = bodyJson.GetValueFromKey_ReturnString(CommonParams.id, "");
                string ngayCapNhat = bodyJson.GetValueFromKey_ReturnString(CommonParams.ngayCapNhat, "");
                string aw = bodyJson.GetValueFromKey_ReturnString(CommonParams.token_password, "");

                FileData fd = new FileData();
                Dictionary<string, object> dicRes = await fd.CheckRemoteConfigAndReturnDataJsonStructure(id, ngayCapNhat);

                if(dicRes != null)
                {
                    CommonMethodsRaoVat cm = new CommonMethodsRaoVat();

                    // dùng chung phương thức khi mã hóa token
                    string jsonRes = CommonMethods.SerializeObject(dicRes);
                    var response3DES = cm.Encrypt3DESForUI(jsonRes);

                    object objRes = new { at = response3DES.CipherText, uk = response3DES.CipherKey };
                    res = CommonMethods.SerializeObject(objRes);

                    // nguyencuongcs: test dữ liệu rõ
                    if(aw == CommonParams.token_password)
                    {
                        res = CommonMethods.SerializeObject(dicRes);
                    }

                }                                
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return new OkObjectResult(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateFileRemoteConfig()
        {
            CustomResult cusRes = new CustomResult();            
            string body = await Request.GetRawBodyStringAsync();

            try
            {
                Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);

                string id = bodyJson.GetValueFromKey_ReturnString(CommonParams.id, "");
                string connectionStrings = bodyJson.GetValueFromKey_ReturnString(CommonParams.connectionStrings, "");

                FileData fd = new FileData();
                bool res = await fd.CreateChiTietRemoteConfig(id, connectionStrings);
                cusRes.IntResult = 1;
                
            }
            catch (Exception ex)
            {
                cusRes.SetException(ex);
            }

            return Response.OkObjectResult(cusRes);
        }

        #endregion

    }
}