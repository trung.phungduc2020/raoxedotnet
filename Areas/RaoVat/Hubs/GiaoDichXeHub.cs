﻿//using Microsoft.AspNetCore.SignalR;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace raoVatApi.Areas.RaoVat.Hubs
//{
//    public class NotificationEntity
//    {
//        public long UserId { get; set; }
//        public string ConnectionId { get; set; }
//    }

//    public class GiaoDichXeHub : Hub
//    {
//        public static class NotificationGroup
//        {
//            public static List<NotificationEntity> listReceived = new List<NotificationEntity>();
//            public static string Group = "Group";
//        }

//        public override async Task OnDisconnectedAsync(Exception exception)
//        {
//            foreach (var item in NotificationGroup.listReceived.FindAll(a => a.ConnectionId == Context.ConnectionId))
//            {
//                NotificationGroup.listReceived.Remove(item);
//                var GroupName = NotificationGroup.Group + item.UserId;
//                await Groups.RemoveFromGroupAsync(item.ConnectionId, GroupName);
//            }

//            await base.OnDisconnectedAsync(exception);
//        }

//        public async Task OutOfGroupMember(long userId)
//        {
//            if (userId > 0)
//            {
//                var GroupName = NotificationGroup.Group + userId;

//                foreach (var item in NotificationGroup.listReceived.FindAll(a => a.UserId == userId && a.ConnectionId == Context.ConnectionId))
//                {
//                    NotificationGroup.listReceived.Remove(item);
//                    await Groups.RemoveFromGroupAsync(item.ConnectionId, GroupName);
//                }
//            }
//        }

//        public async Task InitGroupMember(long userId)
//        {
//            if (userId > 0)
//            {
//                NotificationEntity notificationEntity = new NotificationEntity();
//                notificationEntity.UserId = userId;
//                notificationEntity.ConnectionId = Context.ConnectionId;

//                NotificationGroup.listReceived.Add(notificationEntity);
//            }
//        }

//        public async Task Send(string message, long userId)
//        {
//            var GroupName = NotificationGroup.Group + userId;

//            foreach (var item in NotificationGroup.listReceived.FindAll(a => a.UserId == userId))
//            {
//                await Groups.AddToGroupAsync(item.ConnectionId, GroupName);
//            }

//            await Clients.Groups(GroupName).SendAsync("Receive", message);
//        }
//    }
//}
