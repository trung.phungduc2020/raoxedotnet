﻿//using Microsoft.AspNetCore.SignalR;
//using Newtonsoft.Json;
//using raoVatApi.ModelsRaoVat;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace raoVatApi.Areas.RaoVat.Hubs
//{
//    public class OnlineStatisticHub : Hub
//    {
//        private static int _userOnline = 0;
//        private Dictionary<int, string> lstJson = new Dictionary<int, string>(){
//                                                {1,""},
//                                                {2,""},
//                                                {3,""},
//                                                {4,""},
//                                                {5,""},
//                                                {6,""},
//                                                {7,""},
//                                                {8,""},
//                                                {9,""},
//                                                {10,""},
//                                                {11,""},
//                                                {12,""},
//                                                {13,""},
//                                                {14,""},
//                                                {15,""},
//                                                {16,""},
//                                                {17,""},
//                                                {18,""},
//                                                {19,""},
//                                                {20,""},
//                                                {21,""},
//                                                {22,""},
//                                                {23,""},
//                                                {24,""},
//                                            };

//        public override async Task OnDisconnectedAsync(Exception exception)
//        {
//            _userOnline--;

//            await Clients.All.SendAsync("Receive", _userOnline);
//        }

//        public override async Task OnConnectedAsync()
//        {
//            _userOnline++;

//            var currentDate = DateTime.Now;

//            // Update history SoLuotXem
//            EntityBaseRepositoryRaoVat<OnlineStatistics> repoOnlineStatistics = new EntityBaseRepositoryRaoVat<OnlineStatistics>();
//            OnlineStatistics onlineStatistics = repoOnlineStatistics.Find(a => a.OnlineDate.Value.Date == currentDate.Date).Result.FirstOrDefault();

//            if (onlineStatistics != null)
//            {
//                Dictionary<int, string> deserializeJson = JsonConvert.DeserializeObject<Dictionary<int, string>>(onlineStatistics.ListJson);
//                if (string.IsNullOrEmpty(deserializeJson[currentDate.Hour]))
//                {
//                    deserializeJson[currentDate.Hour] = "1";
//                }
//                else
//                {
//                    deserializeJson[currentDate.Hour] = (Convert.ToInt32(deserializeJson[currentDate.Hour]) + 1).ToString();
//                }

//                onlineStatistics.TotalOnline = (onlineStatistics.TotalOnline ?? 0) + 1;
//                onlineStatistics.ListJson = JsonConvert.SerializeObject(deserializeJson);
//                await repoOnlineStatistics.UpdateAfterAutoClone(onlineStatistics);
//            }
//            else
//            {
//                lstJson[currentDate.Hour] = "1";

//                onlineStatistics = new OnlineStatistics();
//                onlineStatistics.OnlineDate = currentDate;
//                onlineStatistics.TotalOnline = (onlineStatistics.TotalOnline ?? 0) + 1;
//                onlineStatistics.ListJson = JsonConvert.SerializeObject(lstJson);
//                await repoOnlineStatistics.Insert(onlineStatistics);
//            }

//            await Clients.All.SendAsync("Receive", _userOnline);
//        }
//    }
//}
