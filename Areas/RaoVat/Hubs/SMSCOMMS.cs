﻿using System;
using System.IO.Ports;

public class SMSCOMMS
{
    private SerialPort SMSPort;
    public static bool _Continue = false;
    public static bool _ContSMS = false;
    public static bool _ReadPort = false;
    public delegate void SendingEventHandler(bool Done);
    public delegate void DataReceivedEventHandler(string Message);

    public SMSCOMMS()
    {
        SMSPort = new SerialPort();
        SMSPort.PortName = "COM8";
        SMSPort.BaudRate = 9600;
        SMSPort.Parity = Parity.None;
        SMSPort.DataBits = 8;
        SMSPort.StopBits = StopBits.One;
        SMSPort.Handshake = Handshake.RequestToSend;
        SMSPort.DtrEnable = true;
        SMSPort.RtsEnable = true;
        SMSPort.NewLine = System.Environment.NewLine;
    }

    public bool SendSMS(string CellNumber, string SMSMessage)
    {
        try
        {
            string MyMessage = null;
            //Check if Message Length <= 160
            if (SMSMessage.Length <= 160)
                MyMessage = SMSMessage;
            else
                MyMessage = SMSMessage.Substring(0, 160);
            if (SMSPort.IsOpen == true)
            {
                SMSPort.WriteLine("AT+CMGF=1" + Environment.NewLine);
                System.Threading.Thread.Sleep(200);
                SMSPort.WriteLine("AT+CSCS=GSM" + Environment.NewLine);
                System.Threading.Thread.Sleep(200);
                SMSPort.WriteLine("AT+CMGS=" + (char)34 + CellNumber
                + (char)34 + Environment.NewLine);
                System.Threading.Thread.Sleep(200);
                SMSPort.WriteLine(SMSMessage + (char)26);
                System.Threading.Thread.Sleep(1);
                SMSPort.Close();

                return true;
            }

            return false;
        }
        catch
        {
            return false;
        }
    }

    public void Open()
    {
        if (SMSPort.IsOpen == false)
        {
            SMSPort.Open();
        }
    }

    public void Close()
    {
        if (SMSPort.IsOpen == true)
        {
            SMSPort.Close();
        }
    }
}