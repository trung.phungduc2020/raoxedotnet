using Newtonsoft.Json;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

public class SendMail
{
    #region "Common"

    public void Send(string toEmail, string ccEmail, string bccEmail, string tieuDe, string noiDung)
    {
        // Thong tin email gui
        string fromEmailSend = Variables.EmailSend_Name;
        string passwordEmailSend = Variables.EmailSend_Password;
        string fromHoTenSend = Variables.EmailSend_HoTen;
        string hostSend = Variables.EmailSend_Host;
        int portSend = Variables.EmailSend_Port;
        bool isSSLSend = Variables.EmailSend_IsSSL;
        
        Send(fromEmailSend, passwordEmailSend, fromHoTenSend, toEmail, ccEmail,
                    bccEmail, tieuDe, noiDung, true, hostSend, portSend, isSSLSend);
    }

    public void Send(string fromMail, string password, string fromHoTen, string receiverAddresses, string ccEmail, string bccEmail,
                                string subject, string content, bool isBodyHTML, string host, int port, bool isSSL)
    {
        try
        {
            // Create Mail Message Object 
            System.Net.Mail.MailMessage myMailMessage = new System.Net.Mail.MailMessage();

            myMailMessage.From = new System.Net.Mail.MailAddress(fromMail, fromHoTen);

            myMailMessage.Body = content;
            myMailMessage.Subject = subject;

            string[] arrTemp = receiverAddresses.Split(';');
            foreach (string temp in arrTemp)
            {
                if (!string.IsNullOrEmpty(temp))
                {
                    myMailMessage.To.Add(temp);
                }
            }

            if (!string.IsNullOrEmpty(ccEmail))
            {
                arrTemp = ccEmail.Split(';');
                foreach (string temp in arrTemp)
                {
                    if (!string.IsNullOrEmpty(temp))
                    {
                        myMailMessage.CC.Add(temp);
                    }
                }
            }

            if (!string.IsNullOrEmpty(bccEmail))
            {
                arrTemp = bccEmail.Split(';');
                foreach (string temp in arrTemp)
                {
                    if (!string.IsNullOrEmpty(temp))
                    {
                        myMailMessage.Bcc.Add(temp);
                    }
                }
            }

            // Set mail format
            myMailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            myMailMessage.IsBodyHtml = isBodyHTML;

            string rawPassword = CommonMethods.DecodeFrom64_ASCII(password);
            //Proper Authentication Details need to be passed when sending email from gmail
            System.Net.NetworkCredential mailAuthentication = new System.Net.NetworkCredential(fromMail, rawPassword);

            // Create smtp client
            System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient(host, port);

            // Enable SSL
            if (isSSL)
            {
                mailClient.EnableSsl = true;
            }

            // Set Credentials
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = mailAuthentication;
            //mailClient.UseDefaultCredentials = true; // no work

            // Send mail
            mailClient.SendAsync(myMailMessage, mailAuthentication);
        }
        catch (Exception ex)
        {
            // donothing 
            string error = ex.ToString();
        }
    }

    #endregion

    //public async Task SendMailLienHe(string companyName, string hoTen, string dienThoai, string email, string tieuDeLienHe, string noiDungLienHe, string sentFromLink)
    //{
    //    try
    //    {
    //        InterfaceBLL bll = new InterfaceBLL();
    //        IEnumerable<TemplateEmail> lstTemplateEmail = await bll.SearchTemplateEmailByMa(Variables.TemplateEmail_ContactUs, true, "");
    //        TemplateEmail item = null;

    //        string tieuDeNullInfo = "Tiêu đề chưa nhập";
    //        //string noiDungNullInfo = "Nội dung chưa nhập";

    //        if (lstTemplateEmail != null && lstTemplateEmail.Count() > 0)
    //        {
    //            item = lstTemplateEmail.ElementAt(0);
    //            string tieuDe = string.IsNullOrEmpty(item.TieuDe) ? tieuDeNullInfo : item.TieuDe;
    //            string noiDung = string.IsNullOrEmpty(item.NoiDung) ? tieuDeNullInfo : item.NoiDung;

    //            string ccEmail = item.CcEmail;
    //            ccEmail = string.IsNullOrEmpty(ccEmail) ? Variables.EmailReceive_Cc : ccEmail;

    //            string bccEmail = item.BccEmail;
    //            bccEmail = string.IsNullOrEmpty(bccEmail) ? Variables.EmailReceive_LaThuTinh : bccEmail;

    //            // TieuDe - {0}: Tieu de Email; 
    //            // NoiDung - {0}: Ho ten, {1}: Dien thoai, {2}: Email khach hang, {3}: Noi dung
    //            tieuDe = tieuDe.Replace("[[fullname]]", hoTen);
    //            noiDung = noiDung.Replace("[[company]]", companyName);
    //            noiDung = noiDung.Replace("[[website]]", CommonMethods.DomainName);
    //            noiDung = noiDung.Replace("[[fullname]]", hoTen);
    //            noiDung = noiDung.Replace("[[phone]]", dienThoai);
    //            noiDung = noiDung.Replace("[[email]]", email);
    //            noiDung = noiDung.Replace("[[title]]", tieuDeLienHe);
    //            noiDung = noiDung.Replace("[[content]]", noiDungLienHe);
    //            noiDung = noiDung.Replace("[[sentfromlink]]", sentFromLink);
    //            await Send(Variables.EmailReceive_Name, ccEmail, bccEmail, tieuDe, noiDung);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        CommonMethods.WriteLog("Exception=" + ex.ToString());
    //    }
    //}

    //public async Task SendMailXacNhanTaiKhoan(ThanhVien thanhVien, MaXacNhan maXacNhan)
    //{
    //    try
    //    {
    //        if(thanhVien == null || maXacNhan == null || maXacNhan.TuDienMaXacNhan == null)
    //        {
    //            return;
    //        }

    //        InterfaceBLL bll = new InterfaceBLL();
    //        IEnumerable<TemplateEmail> lstTemplateEmail = await bll.SearchTemplateEmailByMa(Variables.TemplateEmail_MaXacNhan_TaiKhoan, true, "");
    //        TemplateEmail item = null;

    //        string tieuDeInfo = "[" + bll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_COMPANY).Result + "] - Xác nhận tài khoản";
    //        string noiDungNullInfo = "Nội dung chưa nhập";

    //        if (lstTemplateEmail != null && lstTemplateEmail.Count() > 0)
    //        {
    //            item = lstTemplateEmail.ElementAt(0);
    //            string tieuDe = string.IsNullOrEmpty(item.TieuDe) ? tieuDeInfo : item.TieuDe;
    //            string noiDung = string.IsNullOrEmpty(item.NoiDung) ? noiDungNullInfo : item.NoiDung;
    //            string ccEmail = "";
    //            string bccEmail = "";
    //            string maXacNhanRaw = maXacNhan.TuDienMaXacNhan.MaXacNhan;

    //            // CommonParams.maxacnhan_ma = mm
    //            // CommonParams.maxacnhan_email = me
    //            // CommonParams.maxacnhan_requireMaXacNhan = em
    //            object objEmailAndMaXacNhan64 = new { me = thanhVien.Email, mm = maXacNhan.MaXacNhan64, em = true };
    //            string emailAndMaXacNhan64 = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(objEmailAndMaXacNhan64));

    //            string urlDomainChinh = CommonMethods.GetPathDefaultDomainChinh();
    //            string linkXacNhan = $"{urlDomainChinh}/xac-nhan-tai-khoan?{CommonParams.token_data64}={emailAndMaXacNhan64}";

    //            // TieuDe - {0}: Tieu de Email; 
    //            // NoiDung - {0}: Ho ten, {1}: Dien thoai, {2}: Email khach hang, {3}: Noi dung
    //            noiDung = noiDung.Replace("[[fullname]]", thanhVien.HoTen);
    //            noiDung = noiDung.Replace("[[maxacnhan]]", maXacNhanRaw);
    //            noiDung = noiDung.Replace("[[linkxacnhan]]", linkXacNhan);
    //            //noiDung = noiDung.Replace("[[website]]", CommonMethods.DomainName);                              
    //            //noiDung = noiDung.Replace("[[email]]", thanhVien.Email);
    //            //noiDung = noiDung.Replace("[[title]]", tieuDeInfo);                
                
    //            await Send(thanhVien.Email, ccEmail, bccEmail, tieuDe, noiDung);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        CommonMethods.WriteLog("Exception=" + ex.ToString());
    //    }
    //}

    public async Task SendMailXacNhanTaiKhoan(SendMailModel sendMailModel, MaXacNhan maXacNhan)
    {
        try
        {
            if (sendMailModel == null || maXacNhan == null)
            {
                return;
            }
            // 192.168.11.3\api\dailyxe\template?maxacnhan=1

            string json;
            using (var client = new HttpClient())
            {
                // var url = new Uri($"http://192.168.11.3/api/dailyxe/templateemail?maTemplate=contactus");
                var url = new Uri(Variables.ApiDaiLyXe + "/templateemail?maTemplate=contactus");
                var response = await client.GetAsync(url);
               
                using (var content = response.Content)
                {
                    json = await content.ReadAsStringAsync();
                }
                //var dataJson = JsonConvert.DeserializeObject<GiphyModel>(json);
            }

            Send(sendMailModel.Email, json, string.Empty, string.Empty, string.Empty);

            //InterfaceBLL bll = new InterfaceBLL();
            //IEnumerable<TemplateEmail> lstTemplateEmail = await bll.SearchTemplateEmailByMa(Variables.TemplateEmail_MaXacNhan_TaiKhoan, true, "");
            //TemplateEmail item = null;

            //string tieuDeInfo = "[" + bll.GetGiaTriCauHinhHeThongByMa(Variables.MACAUHINH_COMPANY).Result + "] - Xác nhận tài khoản";
            //string noiDungNullInfo = "Nội dung chưa nhập";

            //if (lstTemplateEmail != null && lstTemplateEmail.Count() > 0)
            //{
            //    item = lstTemplateEmail.ElementAt(0);
            //    string tieuDe = string.IsNullOrEmpty(item.TieuDe) ? tieuDeInfo : item.TieuDe;
            //    string noiDung = string.IsNullOrEmpty(item.NoiDung) ? noiDungNullInfo : item.NoiDung;
            //    string ccEmail = "";
            //    string bccEmail = "";
            //    string maXacNhanRaw = maXacNhan.TuDienMaXacNhan.MaXacNhan;

            //    // CommonParams.maxacnhan_ma = mm
            //    // CommonParams.maxacnhan_email = me
            //    // CommonParams.maxacnhan_requireMaXacNhan = em
            //    object objEmailAndMaXacNhan64 = new { me = sendMailModel.Email, mm = maXacNhan.MaXacNhan64, em = true };
            //    string emailAndMaXacNhan64 = CommonMethods.EncodeTo64_UTF8(CommonMethods.SerializeObject(objEmailAndMaXacNhan64));

            //    string urlDomainChinh = CommonMethods.GetPathDefaultDomainChinh();
            //    string linkXacNhan = $"{urlDomainChinh}/xac-nhan-tai-khoan?{CommonParams.token_data64}={emailAndMaXacNhan64}";

            //    // TieuDe - {0}: Tieu de Email; 
            //    // NoiDung - {0}: Ho ten, {1}: Dien thoai, {2}: Email khach hang, {3}: Noi dung
            //    noiDung = noiDung.Replace("[[fullname]]", sendMailModel.HoTen);
            //    noiDung = noiDung.Replace("[[maxacnhan]]", maXacNhanRaw);
            //    noiDung = noiDung.Replace("[[linkxacnhan]]", linkXacNhan);
            //    //noiDung = noiDung.Replace("[[website]]", CommonMethods.DomainName);                              
            //    //noiDung = noiDung.Replace("[[email]]", thanhVien.Email);
            //    //noiDung = noiDung.Replace("[[title]]", tieuDeInfo);                

            //    await Send(sendMailModel.Email, ccEmail, bccEmail, tieuDe, noiDung);
            //}
        }
        catch (Exception ex)
        {
            CommonMethods.WriteLog("Exception=" + ex.ToString());
        }
    }
}
