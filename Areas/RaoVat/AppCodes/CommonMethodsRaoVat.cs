﻿using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.ModelsRaoVat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using GmailApi;
using Common;
using Repository;

namespace raoVatApi.AppCodesRaoVat
{
    public class CommonMethodsRaoVat : Controller
    {
        private EntityBaseRepositoryRaoVat<DangTinRating> _repoDangTinRating;
        public EntityBaseRepositoryRaoVat<DangTinRating> RepoDangTinRating { get => _repoDangTinRating == null ? _repoDangTinRating = new EntityBaseRepositoryRaoVat<DangTinRating>(Request) : _repoDangTinRating; set => _repoDangTinRating = value; }
        private EntityBaseRepositoryRaoVat<DangTin> _repoDangTinMaster;
        public EntityBaseRepositoryRaoVat<DangTin> RepoDangTinMaster { get => _repoDangTinMaster == null ? _repoDangTinMaster = new EntityBaseRepositoryRaoVat<DangTin>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoDangTinMaster; set => _repoDangTinMaster = value; }

        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPoint;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPoint { get => _repoUserPoint == null ? _repoUserPoint = new EntityBaseRepositoryRaoVat<UserPoint>() : _repoUserPoint; set => _repoUserPoint = value; }
        private EntityBaseRepositoryRaoVat<UserPoint> _repoUserPointMaster;
        public EntityBaseRepositoryRaoVat<UserPoint> RepoUserPointMaster { get => _repoUserPointMaster == null ? _repoUserPointMaster = new EntityBaseRepositoryRaoVat<UserPoint>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoUserPointMaster; set => _repoUserPointMaster = value; }

        private EntityBaseRepositoryRaoVat<UserRank> _repoUserRank;
        public EntityBaseRepositoryRaoVat<UserRank> RepoUserRank { get => _repoUserRank == null ? _repoUserRank = new EntityBaseRepositoryRaoVat<UserRank>() : _repoUserRank; set => _repoUserRank = value; }

        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVien;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVien { get => _repoThanhVien == null ? _repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>() : _repoThanhVien; set => _repoThanhVien = value; }
        private EntityBaseRepositoryRaoVat<ThanhVien> _repoThanhVienMaster;
        public EntityBaseRepositoryRaoVat<ThanhVien> RepoThanhVienMaster { get => _repoThanhVienMaster == null ? _repoThanhVienMaster = new EntityBaseRepositoryRaoVat<ThanhVien>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master) : _repoThanhVienMaster; set => _repoThanhVienMaster = value; }

        private EntityBaseRepositoryRaoVat<DangTinContact> _repoDangTinContact;
        public EntityBaseRepositoryRaoVat<DangTinContact> RepoDangTinContact { get => _repoDangTinContact == null ? _repoDangTinContact = new EntityBaseRepositoryRaoVat<DangTinContact>() : _repoDangTinContact; set => _repoDangTinContact = value; }

        #region SMS phone

        public bool SendSms(string phoneNumber, string code, out string resultMessage)
        {
            resultMessage = string.Empty;

            try
            {
                // gửi sms cái này 
                SMSCOMMS SMSEngine = new SMSCOMMS();
                SMSEngine.Open();
                SMSEngine.SendSMS(phoneNumber, code);
                SMSEngine.Close();

                // trả về kết quả
                resultMessage = code;
                return true;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                return false;
            }
        }

        #endregion

        #region Insert Notification

        public async Task<string> InsertNotification(InterfaceParam interfaceParam, string content = "")
        {
            var notificationTemplate = GetTemplateNotification(interfaceParam.ma);

            string TieuDe = notificationTemplate == null ? "" : string.Format(notificationTemplate.TieuDe, interfaceParam.tieuDe);
            string NoiDung = string.Empty;
            if (string.IsNullOrEmpty(content))
            {
                NoiDung = notificationTemplate == null ? "" : string.Format(notificationTemplate.NoiDung,
                                                                       string.IsNullOrEmpty(interfaceParam.noiDung) ? string.Empty : interfaceParam.noiDung,
                                                                       string.IsNullOrEmpty(interfaceParam.noiDung1) ? string.Empty : interfaceParam.noiDung1,
                                                                       string.IsNullOrEmpty(interfaceParam.noiDung2) ? string.Empty : interfaceParam.noiDung2);
            }
            else
            {
                NoiDung = content;
            }

            string Link = notificationTemplate == null || string.IsNullOrEmpty(notificationTemplate.Link) ? "" : string.Format(notificationTemplate.Link, interfaceParam.linkId);
            int Type = interfaceParam.typeNotification;
            long? IdThanhVien = interfaceParam.idThanhVien;

            var IsSendEmail = (!interfaceParam.isSendEmail.HasValue || interfaceParam.isSendEmail.Value == true) ? true : false;
            if (!string.IsNullOrEmpty(interfaceParam.email) && IsSendEmail == true)
            {
                // send email
                if (Variables.EnvironmentIsProduction == true)
                {
                    GmailApiCore gac = new GmailApiCore();
                    var receiveMail = interfaceParam.email;
                    var ccEmail = string.Empty;
                    var bccEmail = Variables.EmailToCollectError_Name;
                    gac.SendMessage(receiveMail, ccEmail, bccEmail, TieuDe, NoiDung);
                }
                else
                {
                    MailService ms = new MailService();
                    ms.SendEmail(TieuDe, NoiDung, interfaceParam.email);
                }
            }

            NotificationSystem notify = new NotificationSystem();
            notify.Type = Type;
            notify.IdThanhVien = IdThanhVien;
            notify.TieuDe = TieuDe;
            notify.NoiDung = NoiDung;
            notify.Link = Link;
            notify.TrangThai = 1;
            EntityBaseRepositoryRaoVat<NotificationSystem> repoNotificationMaster = new EntityBaseRepositoryRaoVat<NotificationSystem>(Request, Variables.ConnectionStringPostgresql_RaoVat_Master);
            var result = await repoNotificationMaster.InsertWithouClearCache(notify);

            if(result > 0)
            {
                var noiDungToSend = NoiDung.Contains("<br>") ? NoiDung.Replace("<br>", "") : NoiDung;
                // Push notification
                await NotificationRequest.sendNotification(IdThanhVien.Value, TieuDe, noiDungToSend, Link);
            }

            return NoiDung;
        }

        public NotificationTemplate GetTemplateNotification(string code)
        {
            EntityBaseRepositoryRaoVat<NotificationTemplate> repoNotificationTemplate = new EntityBaseRepositoryRaoVat<NotificationTemplate>();

            return repoNotificationTemplate.Find(a => a.Ma == code).Result.FirstOrDefault();
        }

        #endregion

        #region Update Rating in DangTin

        public async Task UpdateRatingInDangTin(long IdDangTin)
        {
            var LstRating = RepoDangTinRating.Find(a => a.IdDangTin == IdDangTin && a.TrangThai == 1).Result.ToList();

            int TotalRate = 0;
            decimal AvgRate = 0;
            int Rate1 = 0;
            int Rate2 = 0;
            int Rate3 = 0;
            int Rate4 = 0;
            int Rate5 = 0;

            if (LstRating.Count() > 0)
            {
                TotalRate = LstRating.Count();
                AvgRate = LstRating.Average(a => a.Rating ?? 0);
                Rate1 = LstRating.Where(a => a.Rating == 1).Count();
                Rate2 = LstRating.Where(a => a.Rating == 2).Count();
                Rate3 = LstRating.Where(a => a.Rating == 3).Count();
                Rate4 = LstRating.Where(a => a.Rating == 4).Count();
                Rate5 = LstRating.Where(a => a.Rating == 5).Count();
            }

            await RepoDangTinMaster.UpdateWhere(s => s.Id == IdDangTin, delegate (DangTin obj)
            {
                obj.TotalRate = TotalRate;
                obj.Rate1 = Rate1;
                obj.Rate2 = Rate2;
                obj.Rate3 = Rate3;
                obj.Rate4 = Rate4;
                obj.Rate5 = Rate5;
                obj.AvgRate = AvgRate;
            });
        }

        #endregion

        #region "Encrypt"

        public EncryptedData Encrypt3DESForUI(string plainText)
        {
            EncryptedData res = new EncryptedData();
            res.CipherText = CommonMethods.Encrypt3DES(plainText, "");
            res.CipherKey = CommonMethods.EncryptRSAForUI(TrippleDESLib.Key);

            return res;
        }

        #endregion

        #region

        public string UpgradeRank(long idThanhVien, out long? idNewRank)
        {
            if (idThanhVien > 0)
            {
                EntityBaseRepositoryRaoVat<ThanhVien> repoThanhVien = new EntityBaseRepositoryRaoVat<ThanhVien>();
                ThanhVien thanhVien = repoThanhVien.Find(n => n.Id == idThanhVien).Result.FirstOrDefault();

                UserPoint savedUserPoint = RepoUserPoint.Find(n => n.IdThanhVien == idThanhVien).Result.FirstOrDefault();

                // tính rank nếu rank thay đổi thì update ranking
                long totalPoint = (savedUserPoint.Point ?? 0) + (savedUserPoint.PointUsed ?? 0);
                IQueryable<UserRank> lstRankSuccess = RepoUserRank.Find(n => n.PointRule <= totalPoint && n.TrangThai == 1).Result.AsQueryable();
                if (lstRankSuccess.Count() > 0)
                {
                    UserRank matchedRank = lstRankSuccess.OrderByDescending(n => n.PointRule).FirstOrDefault();
                    if (thanhVien != null)
                    {
                        if (thanhVien.IdRank != matchedRank.Id)
                        {
                            thanhVien.IdRank = matchedRank.Id;
                            RepoThanhVienMaster.UpdateAfterAutoClone(thanhVien);

                            // Add to notification
                            InterfaceParam paramNotify = new InterfaceParam();
                            paramNotify.typeNotification = (int)Variables.NotificationSystemType.SendToUser;
                            paramNotify.idThanhVien = thanhVien.Id;
                            paramNotify.ma = Variables.Constants.F002; // Ranking Upgrade
                            paramNotify.noiDung = matchedRank.TieuDe;

                            InsertNotification(paramNotify);

                            idNewRank = thanhVien.IdRank;
                            return matchedRank.TieuDe;
                        }
                    }
                    else
                    {
                        thanhVien = new ThanhVien();
                        thanhVien.IdRank = matchedRank.Id;
                        RepoThanhVienMaster.Insert(thanhVien);
                    }
                }
            }

            idNewRank = 0;
            return string.Empty;
        }

        #endregion

        #region Elastic Search

        public void ReIndexDangTin(DangTin entity)
        {
            try
            {
                ElasticSearchBLL elasticSearchBLL = new ElasticSearchBLL();

                // ReIndex
                if (entity.IdDangTinContact.HasValue && entity.IdDangTinContact.Value > 0)
                {
                    var dangTinContact = RepoDangTinContact.Find(a => a.Id == entity.IdDangTinContact).Result.FirstOrDefault();

                    if (dangTinContact != null)
                    {
                        entity.IdThanhVienContact_Ext = dangTinContact.IdThanhVien;
                        entity.HoTenContact_Ext = dangTinContact.HoTen;
                        entity.DienThoaiContact_Ext = dangTinContact.DienThoai;
                        entity.EmailContact_Ext = dangTinContact.Email;
                        entity.DiaChiContact_Ext = dangTinContact.DiaChi;
                        entity.TenTinhThanhContact_Ext = dangTinContact.TenTinhThanh;
                        entity.TenQuanHuyenContact_Ext = dangTinContact.TenQuanHuyen;
                    }
                }

                var thanhVienDailyXe = RepoThanhVien.Find(a => a.Id == entity.IdThanhVien).Result.FirstOrDefault();
                entity.IdFileDaiDienThanhVien_Ext = thanhVienDailyXe != null ? thanhVienDailyXe.IdFileDaiDien : 0;
                entity.HoTenThanhVien_Ext = thanhVienDailyXe != null ? thanhVienDailyXe.HoTen : string.Empty;
                entity.EmailThanhVien_Ext = thanhVienDailyXe != null ? thanhVienDailyXe.Email : string.Empty;
                entity.DienThoaiThanhVien_Ext = thanhVienDailyXe != null ? thanhVienDailyXe.DienThoai : string.Empty;
                entity.DiaChiThanhVien_Ext = thanhVienDailyXe != null ? thanhVienDailyXe.DiaChi : string.Empty;

                elasticSearchBLL.ReIndex(entity);
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("ReIndexDangTin", Variables.UrlFromClient + "-" + ex.Message, email);
                }
            }
        }

        public void DeleteIndexDangTin(DangTin entity)
        {
            try
            {
                ElasticSearchBLL elasticSearchBLL = new ElasticSearchBLL();

                elasticSearchBLL.DeleteIndex(entity.Id);
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("DeleteIndexDangTin", Variables.UrlFromClient + "-" + ex.Message, email);
                }
            }
        }

        #endregion
    }
}
