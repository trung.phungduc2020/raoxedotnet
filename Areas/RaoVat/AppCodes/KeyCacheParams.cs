﻿using raoVatApi.Models;
using raoVatApi.ModelsRaoVat;
using System;

public static class KeyCacheParams
{
    public static string ByParams = "ByParams:{0}"; // default
    public static string ById = "ById:{0}";
    public static string ByMa = "ByMa:{0}";
    public static string ByIdComponentData = "ByIdComponentData:{0}";
    public static string ByIdComponentDataWithoutParams = "ByIdComponentData";
    public static string ByMaComponentData = "ByMaComponentData:{0}";
    public static string ByIdDaiLy = "ByIdDaiLy:{0}";
    public static string GroupByThuongHieu = "GroupByThuongHieu:{0}";
    public static string GetMoTaChiTietDangTin = "GetMoTaChiTietDangTin:{0}";

    // Dùng để build chuỗi tương tự, sử dụng trong MemoryCacheController
    public static string BuildKeyCacheByVariables(string entityName, string var = "")
    {
        string keyCache = var;
        return BuildKeyCache(entityName, keyCache);
    }

    public static string BuildKeyCacheByVariables<T>(string var = "")
    {
        if (string.IsNullOrEmpty(var))
        {
            var = ByParams;
        }

        string keyCache = var;
        return BuildKeyCache<T>(keyCache);
    }

    public static string BuildKeyCache<T>(string keyCache)
    {
        string typeName = "";

        try
        {
            /*
            AdminBll bll = new AdminBll();
            var mapping = bll.Context.Model.FindEntityType(typeof(T)).Relational();
            var schema = mapping.Schema;
            var tableName = mapping.TableName;
            */

            typeName = typeof(T).Name;
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
        }
        return BuildKeyCache(typeName, keyCache);
    }

    public static string BuildKeyCache(string entityName, string keyCache)
    {
        string res = $"{externalParams}{tagTableOpen}{entityName}{tagTableClose}" + Variables.ApiRaoXe + keyCache;
        return res.ToLower();
    }

    // Quy tắc: nội dung keycache ko bao gồm dấu "?" "&" "," (dùng để split khi clearcache ở memorycachecontroller)    
    public static string splitKeyCacheChar = "]{!";
    public static string coalesceString = "@!";
    public static string equalString = "!!@";
    public static string tagTableOpen = "<tbl>";
    public static string tagTableClose = "</tbl>";

    public static string externalParams = "external";

    public static string externalDangTinByParams = BuildKeyCacheByVariables<DangTin>();
    public static string externalDangTinRatingByParams = BuildKeyCacheByVariables<DangTinRating>();
    public static string externalDangTinContactByParams = BuildKeyCacheByVariables<DangTinContact>();
    public static string externalDangTinTraPhiByParams = BuildKeyCacheByVariables<DangTinTraPhi>();
    public static string externalLayoutAlbumByParams = BuildKeyCacheByVariables<LayoutAlbum>();
    public static string externalDangTinFavoriteByParams = BuildKeyCacheByVariables<DangTinFavorite>();
    public static string externalMenuRaoVatByParams = BuildKeyCacheByVariables<MenuRaoVat>();

    public static string externalNotificationSystemByParams = BuildKeyCacheByVariables<NotificationSystem>();

    public static string externalThanhVienDLXByParams = BuildKeyCacheByVariables<ThanhVien>();

    // Get chi tiet from file 
    public static string externalMoTaChiTietDangTinByParams = BuildKeyCacheByVariables<DangTin>(GetMoTaChiTietDangTin);

    //public static string externalEqualString = externalParams + "TemplateEmailByParams:{0}";
    public static string externalElasticSearchHttpGet = externalParams + "elasticSearchHttpGet:{0}";
    public static string externalElasticSearchHttpPost = externalParams + "elasticSearchHttpPost:{0}";
}


