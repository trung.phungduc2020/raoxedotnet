﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

public class Location
{
    public enum eChangeFrequency
    {
        always,
        hourly,
        daily,
        weekly,
        monthly,
        yearly,
        never
    }

    [XmlElement("loc")]
    public string Url { get; set; }

    [XmlElement("changefreq")]
    public eChangeFrequency? ChangeFrequency { get; set; }
    public bool ShouldSerializeChangeFrequency() { return ChangeFrequency.HasValue; }

    [XmlElement("lastmod")]
    public DateTime? LastModified { get; set; }
    public bool ShouldSerializeLastModified() { return LastModified.HasValue; }

    [XmlElement("priority")]
    public double? Priority { get; set; }
    public bool ShouldSerializePriority() { return Priority.HasValue; }
}