﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[XmlRoot("sitemapindex", Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
public class SitemapIndex
{
    private ArrayList map;

    public SitemapIndex()
    {
        map = new ArrayList();
    }

    [XmlElement("sitemap")]
    public Location[] Locations
    {
        get
        {
            Location[] items = new Location[map.Count];
            map.CopyTo(items);
            return items;
        }
        set
        {
            if (value == null)
                return;
            Location[] items = (Location[])value;
            map.Clear();
            foreach (Location item in items)
                map.Add(item);
        }
    }

    public int Add(Location item)
    {
        return map.Add(item);
    }
}