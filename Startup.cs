using Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using raoVatApi.Middlewares;
using raoVatApi.ModelsRaoVat;
using raoVatApi.Services;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
//using Microsoft.Extensions.Caching.Redis;

namespace raoVatApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true) // Phải có file này trong thư mục publish
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true); // Phải có file này trong thư mục publish

            if (env.IsDevelopment())
            {
                //builder.AddUserSecrets<Startup>();
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709                                        
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            // nguyencuongcs 20190911:
            Variables.AppPath = CommonMethods.GetAppath(env.ContentRootPath);
            Variables.AppSettingPathFile = Configuration.GetConnectionString("AppSettingPathFile");

            Variables.BuildImageName = Assembly.GetExecutingAssembly().GetBuildDateFromAssembly();
            Variables.BuildStartTime = DateTime.Now.ToString(Variables.DD_MM_YYYY_FULL);

            Variables.IsBuildDockerImage = true;
            if (Variables.IsBuildDockerImage)
            {
                Console.WriteLine("Variables.DockerImageName = " + Variables.BuildImageName);
                // DockerData sẽ mount vào volume bên ngoài
                Variables.AppSettingPathFile = $"DockerData/AppSetting/appsettingsdata.{env.EnvironmentName}.json";
            }

            Variables.Environment = $"Build environment: {env.EnvironmentName}";
            //CommonMethods.WriteLogDocker("Variables.AppSettingPathFile: " + Variables.AppSettingPathFile);
            CommonMethods.LoadAppSetting();

            CommonFileManager cmRaoVat = new CommonFileManager();
            string pathAppData = Variables.DataPath + cmRaoVat.FolderAppData;
            if (!System.IO.Directory.Exists(pathAppData))
            {
                System.IO.Directory.CreateDirectory(pathAppData);
            }
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IViewRenderService, ViewRenderService>();

            services.AddScoped(typeof(IEntityBaseRepositoryRaoVat<>), typeof(EntityBaseRepositoryRaoVat<>));

            services.AddMvc(
                mvcOptions =>
                {
                    mvcOptions.CacheProfiles.Add("ImageControllerCache",
                    new CacheProfile()
                    {
                        Duration = CommonMethods.ConvertToInt32(Variables.CacheMaxAgeForImage)
                    });
                    mvcOptions.CacheProfiles.Add("FileControllerCache",
                    new CacheProfile()
                    {
                        Duration = CommonMethods.ConvertToInt32(Variables.CacheMaxAgeForFiles)
                    });
                }
            )
            .AddRazorOptions(options =>
            {
                options.ViewLocationExpanders.Add(new ViewLocationExpanderCustom());
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            ;

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.ApiVersionReader = new MediaTypeApiVersionReader();
                o.ApiVersionSelector = new CurrentImplementationApiVersionSelector(o);
                o.DefaultApiVersion = new ApiVersion(1, 0);
                o.UseApiBehavior = false;
            });

            // Don't understand ??
            // https://stackoverflow.com/questions/46917437/stackexchange-connectionmultiplexer-getserver-not-working
            try
            {
                if (Variables.RedisOn)
                {
                    // Test redis connection
                    using (var _redis = StackExchange.Redis.ConnectionMultiplexer.Connect(Variables.RedisFullConnectionString))
                    {
                        //var server = _redis.GetServer(Variables.RedisFullConnectionString);
                        var resRedis = _redis.GetEndPoints().Select(endpoint =>
                                            {
                                                var server = _redis.GetServer(endpoint);
                                                Console.WriteLine("Redis is online:" + server.IsConnected);
                                                return server.IsConnected;
                                            }).ToList();
                    }

                    // services.AddDistributedRedisCache(
                    //options =>
                    //{
                    //    options.Configuration = Variables.RedisConnectionString + ",AllowAdmin=true,Password=R@ei2D018@nhangVN"; // nguyencuongcs 20180326: hiện tại cấu hình local, nếu sau này dựng webfarm (distributed web server) sẽ cấu hình lại chỗ này sau.                                      
                    //    options.InstanceName = Variables.RedisServerName;
                    //});
                    Variables.CurrentCache = "RedisCache";
                }
                else
                {
                    services.AddDistributedMemoryCache();
                    Variables.CurrentCache = "DistributedMemoryCache";
                }

            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("AddDistributedRedisCache error:", ex.ToString());
                services.AddDistributedMemoryCache();
                Variables.CurrentCache = "DistributedMemoryCache";
            }

            ProcessURL pu = new ProcessURL(null);
            // Enable Cors            
            services.AddCors(o => o.AddPolicy("CorsPolicy_Production", builder => // "AllowAll" | "CorsPolicy"
            {
                builder//.AllowAnyOrigin()
                        .WithOrigins(pu.RewriteLinkFullDomainChinh(""))
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            services.AddCors(o => o.AddPolicy("CorsPolicy_Development", builder => // "AllowAll" | "CorsPolicy"
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            services.AddMvc(options =>
            {
                //set global format to xml
                // options.Filters.Add(new Microsoft.AspNetCore.Mvc.ProducesAttribute("application/xml"));
                // options.RespectBrowserAcceptHeader = true;
                // options.OutputFormatters.RemoveType<Microsoft.AspNetCore.Mvc.Formatters.TextOutputFormatter>();
                // options.OutputFormatters.RemoveType<Microsoft.AspNetCore.Mvc.Formatters.HttpNoContentOutputFormatter>();
            })
            .AddRazorOptions(options =>
            {
                options.ViewLocationExpanders.Add(new ViewLocationExpanderCustom());
            });


            #region [Hữu 23082017] Check token

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = Variables.SigningKey,
                ValidateIssuer = false,
                ValidIssuer = Variables.Issue,
                ValidateAudience = false,
                ValidAudience = Variables.Audience,
                ValidateLifetime = true,
                //ClockSkew = TimeSpan.Zero,
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;


            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = tokenValidationParameters;
                options.RequireHttpsMetadata = false;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(CommonParams.policy_requireAdminRoleName, policy => policy.RequireRole(CommonParams.token_roleAdmin));
                options.AddPolicy(CommonParams.policy_requireThanhVienRoleName, policy => policy.RequireRole(CommonParams.token_roleThanhVien));
                options.AddPolicy(CommonParams.policy_requireAdminOrThanhVienRoleName, policy => policy.RequireRole(CommonParams.token_roleAdmin, CommonParams.token_roleThanhVien));
            });


            #endregion

            services.AddResponseCompression(options =>
            {
                //options.Providers.Add<GzipCompressionProvider>();
                //options.MimeTypes = ResponseCompressionMimeTypes.Defaults;

                options.Providers.Add<BrotliCompressionProvider>();
                options.EnableForHttps = true;
                // nguyencuongcs 20180703: 20% smaller than gzip. 20x times decode faster than gzip
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] {
                    "application/xhtml+xml",
                    "application/atom+xml",
                    "image/svg+xml",
                });
            });

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy_Production"));
                options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy_Development"));
            });

            services.Configure<GzipCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest; //Compression should complete as quickly as possible, even if the resulting output is not optimally compressed.
                //options.Level = CompressionLevel.NoCompression; //No compression should be performed.
                //options.Level = CompressionLevel.Optimal; //Responses should be optimally compressed, even if the compression takes more time to complete.
            });

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            //services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                //app.UseBrowserLink();                
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                app.UseMiddleware(typeof(ErrorHandlingMiddleware));
                /*
                app.UseExceptionHandler(
                  builder =>
                  {
                      builder.Run(
                        async context =>
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            //context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                            var error = context.Features.Get<IExceptionHandlerFeature>();
                            if (error != null)
                            {
                                var ex = "";
                                if (!env.IsDevelopment())
                                {
                                    ex = error.Error.ToString(); //.Message;
                                }
                                else
                                {
                                    ex = error.Error.Message;
                                }

                                context.Response.AddApplicationError(ex);
                                await context.Response.WriteAsync(ex, encoding: Encoding.UTF8).ConfigureAwait(false);

                            }
                        });
                  });
                  */
            }

            var options = new RewriteOptions();

            // nguyencuongcs 20191107: nghi chỗ này làm bị lỗi khi chạy docker
            // thử thêm điều kiện. Với lại nginx đã tự redirect trong cấu hình
            // Đã xác thực: đúng là AddRedirectToHttpsPermanent() làm lỗi khi chạy docker trên linux
            if (!Variables.IsBuildDockerImage)
            {
                if (CommonMethods.CheckUseSSL())
                {
                    options
                    .AddRedirectToHttpsPermanent() // 301 => 443
                                                   //.AddRedirectToHttps(301, 5001)                
                    ;
                }
            }

            options
                .AddRewrite(@"crm/image/.*-([a-z0-9]+)(j)([0-9x]*).([a-z0-9]{2,4})$", @"crm/image?id=$1&wxh=$3&ext=$4", skipRemainingRules: true)
                //.AddRewrite(@"RaoVat/image/.*-([a-z0-9]+)(j)([0-9x]*).([a-z0-9]{2,4})$", @"RaoVat/image?id=$1&wxh=$3&ext=$4", skipRemainingRules: true) // nguyencuongcs 20180319: chuyển về /image/ cho hệ thống RaoVat
                .AddRewrite(@"image/.*-([a-z0-9]+)(j)([0-9x]*).([a-z0-9]{2,4})$", @"RaoVat/image?id=$1&wxh=$3&ext=$4", skipRemainingRules: true)
                //.AddRewrite(@"image/.*-([a-z0-9]+)(j)([0-9x]*).([a-z0-9]{2,4})([=a-z0-9])(?=/?)", @"RaoVat/image?id=$1&wxh=$3&ext=$4&$5", skipRemainingRules: true) // Bổ sung queryString. Ví dụ: q=20
                .AddRewrite(@"crm/file/.*-([a-z0-9]+)(f)([0-9x]*).([a-z0-9]{2,4})$", @"crm/file?id=$1&wxh=$3&ext=$4", skipRemainingRules: true)
                //.AddRewrite(@"RaoVat/file/.*-([a-z0-9]+)(f)([0-9x]*).([a-z0-9]{2,4})$", @"RaoVat/file?id=$1&wxh=$3&ext=$4", skipRemainingRules: true)
                .AddRewrite(@"file/.*-([a-z0-9]+)(f)([0-9x]*).([a-z0-9]{2,4})$", @"RaoVat/file?id=$1&wxh=$3&ext=$4", skipRemainingRules: true)
                //.Add(new RewriteUrlRule())
                ;

            app.UseRewriter(options);

            // Set up custom content types -associating file extension to MIME type
            var provider = new FileExtensionContentTypeProvider();
            // Add new mappings
            provider.Mappings[".myapp"] = "application/x-msdownload";
            provider.Mappings[".htm3"] = "text/html";
            provider.Mappings[".image"] = "image/png";
            // Replace an existing mapping
            provider.Mappings[".rtf"] = "application/x-msdownload";
            // Remove MP4 videos.
            provider.Mappings.Remove(".mp4");

            app
                .UseResponseCompression()
                // for wwwroot
                .UseStaticFiles(
                    new StaticFileOptions()
                    {
                        OnPrepareResponse = ctx => // cache
                        {
                            ctx.Context.Response.Headers.Append("Cache-Control", $"public,max-age={Variables.CacheMaxAgeForStaticFiles}");
                        },
                    }
                )
                // for Resources
                .UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                    RequestPath = new PathString("/Resources"),
                    OnPrepareResponse = ctx => // cache 10 mins
                    {
                        ctx.Context.Response.Headers.Append("Cache-Control", $"public,max-age={Variables.CacheMaxAgeForStaticFiles}");
                    },
                    //ContentTypeProvider = provider,
                });

            //if (env.IsDevelopment())
            //{
            //    CommonMethods.WriteLog("Start CorsPolicy_Development");
            //    app.UseCors("CorsPolicy_Development");
            //}
            //else
            //{
            // nguyencuongcs 20180712: để khi chạy trên 11.3 ko bị chặn api của oringin local
            if (!CommonMethods.CheckUseDevelopment())
            {
                CommonMethods.WriteLog("Start CorsPolicy_Production");
                app.UseCors("CorsPolicy_Production");
            }
            else
            {
                CommonMethods.WriteLog("Start CorsPolicy_Development");
                app.UseCors("CorsPolicy_Development");
            }
            //}

            app.UseAuthentication();

            #region [Hữu 23082017] Create token

            app.UseMiddleware<TokenProviderMiddleware>(Options.Create(new TokenProviderOptions
            {
                Audience = Variables.Audience,
                Issuer = Variables.Issue,
                SigningCredentials = new SigningCredentials(Variables.SigningKey, SecurityAlgorithms.HmacSha256),
            }));

            #endregion

            app.UseMiddleware<DistributedCacheMiddleware>();

            #region Load default cache

            LoadDefaultData();

            #endregion               

            //app.UseSignalR(routers =>
            //{
            //    //routers.MapHub<GiaoDichXeHub>("/giaoDichXeHub");
            //    //routers.MapHub<DangTinHub>("/dangTinHub");
            //    //routers.MapHub<OnlineStatisticHub>("/onlineStatisticHub");
            //    routers.MapHub<NotificationSystemHub>("/notificationSystemHub");
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        public void LoadDefaultData()
        {
            ModelsRaoVat.AdminBll adminBll = new ModelsRaoVat.AdminBll();

            Variables.Schema_ListColumnMaximumLengthFromDb = adminBll.GetColumnMaximumLength().Result;

            adminBll.BuildListIdThanhVien();
        }
    }
}
