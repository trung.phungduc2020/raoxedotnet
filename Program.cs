﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace raoVatApi
{
    public class Program                                                                                                                 
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }
        a b
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .ConfigureKestrel((context, options) =>
                {
                    // Set properties and call methods on options
                });
        // nguyencuongcs 20191017: không gán UseUrls nữa vì khi chạy docker nhiều container sẽ bị đụng port
        //.UseUrls("http://localhost:5006"); // 5007 dùng để release lên raovatapi.dailyxe.com.vn
        //.UseUrls("http://localhost:5006");// 5006 dùng để release lên dev16_raovatapi.dailyxe.com.vn hoặc chạy local
        //.UseUrls("http://localhost:5008");// 5008 dùng để release lên testraovatapi.dailyxe.com.vn
    }
}
