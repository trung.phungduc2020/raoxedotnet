# FROM node:10 AS buildnode
# WORKDIR /app
# COPY package*.json ./
# RUN npm install

# FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
# WORKDIR /app

# # Copy csproj and restore as distinct layers
# COPY *.csproj ./
# RUN dotnet restore

# # Copy everything else and build
# COPY . ./
# RUN set ASPNETCORE_ENVIRONMENT=Production
# RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
# FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS runtime

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
# RUN echo "Asia/Ho_Chi_Minh" > /etc/timezone
# RUN dpkg-reconfigure -f noninteractive tzdata

WORKDIR /app

# COPY --from=buildnode /app .
# COPY --from=build /app/out .
COPY bin/Release/netcoreapp2.2/publish .

EXPOSE 5006/tcp
ENV ASPNETCORE_URLS http://*:5006
# ENTRYPOINT ["dotnet", "DaiLyXeUI.dll", "--host", "0.0.0.0"]
ENTRYPOINT ["dotnet", "raoVatApi.dll"]
