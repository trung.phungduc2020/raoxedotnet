﻿using Google.Cloud.Firestore;
using Google.Cloud.Firestore.V1;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ResponsePushModel
    {
        public string multicast_id { get; set; }
        public string success { get; set; }
        public string failure { get; set; }
        public string canonical_ids { get; set; }
    }

    class NotificationRequest
    {
        public NotificationRequest()
        {
        }

        public static string sendNotificationRequest(string endpoint, string postJsonString)
        {
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);

                var postData = postJsonString;

                var data = Encoding.UTF8.GetBytes(postData);

                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";
                httpWReq.Headers.Add("Authorization", Variables.FirebaseSecretKey);

                httpWReq.ContentLength = data.Length;
                httpWReq.ReadWriteTimeout = 30000;
                httpWReq.Timeout = 15000;
                Stream stream = httpWReq.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();

                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

                string jsonresponse = "";

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string temp = null;
                    while ((temp = reader.ReadLine()) != null)
                    {
                        jsonresponse += temp;
                    }
                }

                CommonMethods.WriteLog(jsonresponse);
                //todo parse it
                return jsonresponse;
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog(ex.Message);
                return "";
            }
        }

        public static void sendNotification(string to, string title, string body, string action = "", JObject data = null)
        {
            sendToIonic(to, title, body, action, data);
            sendToFlutter(to, title, body, action, data);
        }
        public static async Task sendNotification(long to, string title, string body, string action = "", JObject data = null)
        {
            await sendToMultiUserTokenAsync(to, title, body, action, data);
        }

        public static void sendToIonic(string to, string title, string body, string action = "", JObject data = null)
        {
            var actionLink = !string.IsNullOrEmpty(action) && action.Contains("raovat/") ? "InApp_" + action : "OutApp_" + action;

            JObject notification = new JObject();
            notification.Add("title", title);
            notification.Add("body", body);
            notification.Add("sound", "default");
            notification.Add("click_action", actionLink);

            if (data == null)
            {
                data = new JObject();
                data.Add("sendername", title);
                data.Add("message", body);
                data.Add("body", body);
                data.Add("data", string.Empty);
                data.Add("pathRedirect", actionLink);
            }

            JObject jobMessage = new JObject();
            jobMessage.Add("notification", notification);
            jobMessage.Add("data", data);
            jobMessage.Add("to", "/topics/ThanhVien-" + to);

            CommonMethods.WriteLog("sendToIonic::" + to + "::" + title + "::" + body + "::" + action + "::" + data + CommonMethods.SerializeToJSON(jobMessage));
            NotificationRequest.sendNotificationRequest("https://fcm.googleapis.com/fcm/send", jobMessage.ToString());
        }

        public static void sendToFlutter(string to, string title, string body, string action = "", JObject data = null)
        {
            var actionLink = !string.IsNullOrEmpty(action) && action.Contains("raovat/") ? "InApp_" + action : "OutApp_" + action;

            JObject notification = new JObject();
            notification.Add("title", title);
            notification.Add("body", body);
            notification.Add("sound", "default");
            notification.Add("click_action", "FLUTTER_NOTIFICATION_CLICK");

            if (data == null)
            {
                data = new JObject();
                data.Add("sendername", title);
                data.Add("message", body);
                data.Add("data", string.Empty);
                data.Add("pathRedirect", actionLink);
            }

            JObject jobMessage = new JObject();
            jobMessage.Add("notification", notification);
            jobMessage.Add("data", data);
            jobMessage.Add("to", "/topics/Flutter-ThanhVien-" + to);

            NotificationRequest.sendNotificationRequest("https://fcm.googleapis.com/fcm/send", jobMessage.ToString());
        }

        public static async Task sendToMultiUserTokenAsync(long to, string title, string body, string action = "", JObject data = null)
        {
            try
            {
                var actionLink = !string.IsNullOrEmpty(action) && action.Contains("raovat/") ? "InApp_" + action : "OutApp_" + action;

                JObject notification = new JObject();
                notification.Add("title", title);
                notification.Add("body", body);
                notification.Add("sound", "default");
                notification.Add("click_action", "FLUTTER_NOTIFICATION_CLICK");

                if (data == null)
                {
                    data = new JObject();
                    data.Add("sendername", title);
                    data.Add("message", body);
                    data.Add("pathRedirect", actionLink);
                }

                // 1. Find firebase json to connect
                var lstInvalidUID = new List<string>();
                var jsonString = System.IO.File.ReadAllText(Variables.KeyFirebaseFileJson);
                var builder = new FirestoreClientBuilder { JsonCredentials = jsonString };
                FirestoreDb db = FirestoreDb.Create(Variables.KeyFirebaseProjectId, builder.Build());

                // 2. Query to get token of user device on firestore
                CollectionReference usersRef = db.Collection("pushtokens");
                Query query = usersRef.WhereEqualTo("idThanhVien", to);
                QuerySnapshot snapshot = await query.GetSnapshotAsync();

                // 3. Loop each token to push notification
                foreach (DocumentSnapshot document in snapshot.Documents)
                {
                    Dictionary<string, object> documentDictionary = document.ToDictionary();

                    JObject jobMessage = new JObject();
                    jobMessage.Add("notification", notification);
                    jobMessage.Add("data", data);
                    jobMessage.Add("to", documentDictionary["tokenThanhVien"].ToString());

                    var res = NotificationRequest.sendNotificationRequest("https://fcm.googleapis.com/fcm/send", jobMessage.ToString());
                    var objResponse = JsonConvert.DeserializeObject<ResponsePushModel>(res);
                    if (objResponse.failure == "1") //Invalid token
                    {
                        lstInvalidUID.Add(document.Id);
                    }
                }

                // 4. Remove invalid token device
                foreach (var item in lstInvalidUID)
                {
                    await usersRef.Document(item).DeleteAsync();
                }
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog(ex.Message);
            }
        }

        // Use only for admin to approve posts
        public static void sendNotificationDuyetTinToGroupAdmin(string title, string body, string id)
        {
            var actionLink = Variables.Constants.InAppAdminDuyetTin + id;

            JObject notification = new JObject();
            notification.Add("title", title);
            notification.Add("body", body);
            notification.Add("sound", "default");
            notification.Add("click_action", actionLink);

            JObject data = new JObject();
            data.Add("sendername", title);
            data.Add("message", body);
            data.Add("pathRedirect", actionLink);

            JObject jobMessage = new JObject();
            jobMessage.Add("notification", notification);
            jobMessage.Add("data", data);
            jobMessage.Add("to", "/topics/Admin");

            NotificationRequest.sendNotificationRequest("https://fcm.googleapis.com/fcm/send", jobMessage.ToString());
        }
    }
}
