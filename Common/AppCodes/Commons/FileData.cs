﻿using raoVatApi.Common;
using raoVatApi.Models.Base;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Repository;

namespace raoVatApi.Common
{
    public class FileData
    {
        private static string _maNgonNgu = "vi";// CommonMethods.NgonNguGoc;
        private static bool _isTranslate = false;
        public static string _defaultFileExt = "cshtml";
        public const string _defaultFileExtWithDot = ".cshtml";

        public const string PATH_FILE_PRODUCT = "AppData/RaoVat/Data/Products/";
        public const string PATH_FILE_CONTENT = "AppData/RaoVat/Data/Contents/";
        public const string PATH_FILE_SERVICE = "AppData/RaoVat/Data/Services/";
        public const string PATH_FILE_NEWS = "AppData/RaoVat/Data/News/";
        public const string PATH_FILE_BAIVIET = "AppData/RaoVat/Data/BaiViet/";
        public const string PATH_FILE_CHINHSUAGIAODIEN = "AppData/RaoVat/Data/BaiViet/";
        public const string PATH_FILE_CHINHSUACHANTRANG = "AppData/RaoVat/Data/BaiViet/";
        public const string PATH_FILE_QUANGCAO = "AppData/RaoVat/Data/QuangCao/";
        public const string PATH_FILE_DINHNGHIASANPHAM = "AppData/RaoVat/Data/DinhNghiaSanPham/";
        public const string PATH_FILE_DANGTIN = "AppData/RaoVat/Data/DangTin/";
        public const string PATH_FILE_DAILY = "AppData/RaoVat/Data/DaiLy/";
        public const string PATH_FILE_MENU = "AppData/RaoVat/Data/Menus/";
        public const string PATH_FILE_MODULE = "AppData/RaoVat/Data/Modules/";
        public const string PATH_FILE_SEO = "AppData/RaoVat/Data/Seo/";
        public const string PATH_FILE_SEOCOMPONENT = "AppData/RaoVat/SeoComponent/";
        public const string PATH_FILE_COMPONENTS = "Views/Shared/";
        public const string PATH_FILE_NHOMTHUOCTINH = "AppData/RaoVat/Data/NhomThuocTinh/";
        public const string PATH_FILE_DONGXE = "AppData/RaoVat/Data/DongXe/";
        public const string PATH_FILE_HANGXE = "AppData/RaoVat/Data/HangXe/";
        public const string PATH_FILE_THUONGHIEU = "AppData/RaoVat/Data/ThuongHieu/";
        public const string PATH_FILE_REMOTECONFIG = "AppData/RaoVat/Data/RemoteConfig/";

        private const string FILENAME_SITEMAP_CHUNG = "AppData/RaoVat/Data/wwwroot/sitemap.xml";
        private const string FILENAME_SITEMAP_MAIN = "AppData/RaoVat/Data/wwwroot/sitemap-main.xml";
        private const string FILENAME_SITEMAP_BANGGIAXE = "AppData/RaoVat/Data/wwwroot/sitemap-banggiaxe.xml";
        private const string FILENAME_SITEMAP_RaoVat = "AppData/RaoVat/Data/wwwroot/sitemap-RaoVat.xml";
        private const string FILENAME_SITEMAP_THUONGHIEU = "AppData/RaoVat/Data/wwwroot/sitemap-thuonghieu.xml";
        private const string FILENAME_SITEMAP_OTO = "AppData/RaoVat/Data/wwwroot/sitemap-oto.xml";
        private const string FILENAME_SITEMAP_TINTUC = "AppData/RaoVat/Data/wwwroot/sitemap-tintuc.xml";
        private const string FILENAME_SITEMAP_KHAC = "AppData/RaoVat/Data/wwwroot/sitemap-khac.xml";
        private Dictionary<string, string> _dicMappingSitemapPath = new Dictionary<string, string>()
        {
            { Variables.LoaiSitemap_SitemapChung, FILENAME_SITEMAP_CHUNG},
            { Variables.LoaiSitemap_SitemapBangGiaXe, FILENAME_SITEMAP_BANGGIAXE},
            { Variables.LoaiSitemap_SitemapRaoVat, FILENAME_SITEMAP_RaoVat},
            { Variables.LoaiSitemap_SitemapKhac, FILENAME_SITEMAP_KHAC},
            { Variables.LoaiSitemap_SitemapMain, FILENAME_SITEMAP_MAIN},
            { Variables.LoaiSitemap_SitemapOto, FILENAME_SITEMAP_OTO},
            { Variables.LoaiSitemap_SitemapThuongHieu, FILENAME_SITEMAP_THUONGHIEU},
            { Variables.LoaiSitemap_SitemapTinTuc, FILENAME_SITEMAP_TINTUC}
        };

        public const string FILENAME_CHANTRANG = "footer";
        private const string FILENAME_DINHNGHIASANPHAM_CHITIET = "chitiet.cshtml";
        private const string FILENAME_DINHNGHIASANPHAM_THONGSO = "thongso.cshtml";

        private const string FILENAME_DANGTIN_CHITIET = "chitiet.cshtml";

        // DisplayInfo
        public const string PATH_FILE_DISPLAYINFO_THUONGHIEU = "AppData/RaoVat/Data/DisplayInfo/ThuongHieu/";

        public const string PathRawData_BangGiaXe = "AppData/RawData/BangGiaXe/";
        public const string BangGiaXe_Vnexpress = "vnexpress_[[nam]]_[[thang]]";

        #region "Commons"

        public async Task CreateFile(string pathFile, string content)
        {
            await CreateFile_NoXml(pathFile, content, false);
        }

        public void CreateFolder(string folderPath)
        {
            if (!System.IO.Directory.Exists(folderPath))
            {
                System.IO.Directory.CreateDirectory(folderPath);
            }
        }

        public async Task<string> GetContentFile(string pathFile)
        {
            string result = "";

            //if (System.IO.File.Exists(pathFile))
            {
                try
                {
                    result = await GetContentFile_NoXml(pathFile);
                }
                catch
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(pathFile);
                        result = doc.InnerXml;
                    }
                    catch (Exception ex2)
                    {
                        CommonMethods.WriteLog("GetContentFile_NoXml: " + ex2.ToString());
                    }
                }
            }
            return result;
        }

        private string StandardPathFileForWindowOrLinux(string pathFile)
        {
            pathFile = pathFile.NullIsEmpty();
            if (Variables.IsBuildDockerImage)
            {
                pathFile = pathFile.Replace("\\", "/");
            }
            else
            {
                pathFile = pathFile.Replace("/", "\\");
            }

            return pathFile;
        }

        public bool CheckExistPathFile(string pathFile)
        {
            string fileName = StandardPathFileForWindowOrLinux(pathFile);
            return File.Exists(fileName);
        }

        public async Task CreateFile_NoXml(string pathFile, string content, bool isJsonData)
        {
            if (string.IsNullOrEmpty(pathFile))
            {
                return;
            }

            try
            {
                pathFile = StandardPathFileForWindowOrLinux(pathFile);

                if (!File.Exists(pathFile))
                {
                    CheckPathAndCreate(pathFile);
                }

                // Replace dấu " chit xuất hiện 1 mình trong bài viết. Ví dụ: 17" (inch), để tránh lỗi khi UI parse data json
                if (isJsonData)
                {
                    content = content.Replace("&quot;", "''");
                }
                content = HttpUtility.HtmlDecode(content);
                if (!isJsonData)
                {
                    // Nếu ko phải lưu json data thì remove dấu \ trước " để render được component động trong file
                    content = content.Replace("/\"", "\"");
                }

                // nguyencuongcs 20181022: tạo bản copy để backup và khôi phục dữ liệu
                await CreateHistory(pathFile, content);

                // Xóa trắng file
                await File.WriteAllTextAsync(pathFile, "");
                using (StreamWriter sw = new StreamWriter(pathFile, true))
                {
                    // Ghi đè nội dung mới
                    // nguyencuongcs 20181022: vì lý do ngày xưa dùng hàm ReadAllLines để đọc file, hàm này nhận biết bằng line trống mặc định khi dùng WriteLineAsync
                    // Vì vậy khi so sánh dữ liệu để tạo history luôn luôn rơi vào trường hợp phải tạo vì dư dòng \r\n ở cuối.
                    // Hiện tại đã thay đổi cách read files vì lý do: cần đọc được dữ liệu khi file đang mở bằng chương trình khác. Nên thay đổi luôn cách ghi để ko
                    // dư line cuối nữa.
                    //await sw.WriteLineAsync(content);
                    await sw.WriteAsync(content);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("CreateFile: " + ex.ToString());
            }
        }

        public void CheckPathAndCreate(string pathFile)
        {
            var pathFileInfo = new FileInfo(pathFile);
            string pathFileDirectoryPath = pathFileInfo.DirectoryName;
            if (!Directory.Exists(pathFileDirectoryPath))
            {
                Directory.CreateDirectory(pathFileDirectoryPath);
            }

            File.Create(pathFile).Close();
        }

        // nguyencuongcs 20181022: mục đích chỉ lưu trữ khi nội dung bị thay đổi. Chỉ ở mức căn bản chú ko lưu trữ tất cả dữ liệu thay đổi như google. 
        public async Task CreateHistory(string pathFile, string content)
        {
            try
            {
                bool needToCreateHistory = false;

                // Buoc 1. kiểm tra content có thay đổi với content hiện tại ko: nếu có mới tạo history.
                string currentContent = await GetContentFile(pathFile);
                if (currentContent.Equals(content))
                {
                    bool comp = String.Equals(currentContent, content, StringComparison.CurrentCulture);
                    if (!comp)
                    {
                        needToCreateHistory = true;
                    }
                }
                else
                {
                    needToCreateHistory = true;
                }

                // Bước 2. thay đường dẫn pathFile -> historyPathFile bằng cách:
                //         - replace AppData -> HistoryData
                //         - cộng chuỗi thời gian vào cuối pathFile (trước dấu . phân cách đuôi file)
                if (needToCreateHistory)
                {
                    string historyPathFile = "";
                    string extension = "";
                    string fileNameWithoutExt = "";
                    //var splitString = pathFile.Split(".");
                    //tmp = splitString[0];
                    //extension = splitString[1];
                    //extension = string.IsNullOrEmpty(extension) ? "" : $".{extension}";

                    //var directory = Path.GetDirectoryName(pathFile);
                    //Directory.CreateDirectory(directory);

                    var fileInfo = new FileInfo(pathFile);
                    if (fileInfo != null)
                    {
                        string directoryPath = fileInfo.DirectoryName;
                        extension = fileInfo.Extension;
                        // Get filename without ext. Name = "1.1.cshtml", FullName = "C:/{path}/1.1.cshtml"
                        fileNameWithoutExt = Path.GetFileNameWithoutExtension(fileInfo.FullName);
                        historyPathFile = Path.Combine(directoryPath.Replace("/AppData/", "/HistoryData/"), $"{fileNameWithoutExt}" + $"_{CommonMethods.FilterNumber(CommonMethods.FormatDateTimeMMDDYYYY_FULL(DateTime.Now))}");
                        historyPathFile = historyPathFile + extension;

                        // Bước. tạo file history mới và lưu content cũ vào. Có nghĩa là copy data từ pathFile qua histoyPathFile.
                        if (!File.Exists(historyPathFile))
                        {
                            var histoyFileInfo = new FileInfo(historyPathFile);
                            string historyDirectoryPath = histoyFileInfo.DirectoryName;
                            if (!Directory.Exists(historyDirectoryPath))
                            {
                                Directory.CreateDirectory(historyDirectoryPath);
                            }

                            File.Create(historyPathFile).Close();
                        }

                        await CopyFile(pathFile, historyPathFile);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("CreateHistory: " + ex.ToString());
            }
        }

        public async Task<string> GetContentFile_NoXml(string pathFile)
        {
            string result = "";
            pathFile = StandardPathFileForWindowOrLinux(pathFile);

            // nguyencuongcs 20180828: frame cái này lại để có thể lấy theo .xml nếu .cshtml ko có (vì có tin tức cũ lưu dạng xml)
            if (!File.Exists(pathFile))
            {
                pathFile = pathFile.Replace(_defaultFileExtWithDot, ".xml");
            }

            if (File.Exists(pathFile))
            {
                try
                {
                    using (var stream = File.Open(pathFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            //while (!reader.EndOfStream)
                            //{
                            //    result = await reader.ReadToEndAsync();
                            //}
                            result = await reader.ReadToEndAsync();
                        }
                        // Vì xml tin tức ko chứa đúng cấu trúc node xml nên chỉ cần lấy hết text lên, ko cần xử lý lấy node
                        //result = await System.IO.File.ReadAllTextAsync(pathFile);
                    }
                }
                catch (Exception ex)
                {
                    CommonMethods.WriteLog("GetContentFile_NoXml: " + ex.ToString());
                }
            }

            return result;
        }

        public bool DeleteFile(string pathFile)
        {
            try
            {
                if (System.IO.File.Exists(pathFile))
                {
                    System.IO.File.Delete(pathFile);
                    return true;
                }
            }
            catch { }
            return false;
        }

        public async Task<bool> CopyFile(string sourcePathFile, string destinationPathFile)
        {
            try
            {
                if (System.IO.File.Exists(sourcePathFile))
                {
                    if (!File.Exists(destinationPathFile))
                    {
                        File.Create(destinationPathFile).Close();
                    }

                    string sourceFileContent = await GetContentFile_NoXml(sourcePathFile);

                    System.IO.File.Copy(sourcePathFile, destinationPathFile, true);
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("CopyFile: " + ex.ToString());
            }
            return false;
        }

        #endregion

        #region "SanPham"

        public async Task CreateFileChiTietSanPham(string idSanPham, string chiTietNoiDung)
        {
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_PRODUCT, idSanPham, "");
            await CreateFile(fileName, chiTietNoiDung);
        }

        public async Task<string> GetChiTietSanPham(string idSanPham)
        {
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_PRODUCT, idSanPham, "");
            return await GetContentFile(fileName);
        }

        public async Task<string> GetChiTietSanPhamByKey(string idSanPham, string pKey)
        {
            string chitiet = await GetChiTietSanPham(idSanPham);
            return chitiet ?? string.Empty;
        }

        public void DeleteChiTietSanPham(string idSanPham)
        {
            if (!string.IsNullOrEmpty(idSanPham))
            {
                string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_PRODUCT, idSanPham, "");
                DeleteFile(fileName);
            }
        }

        public async Task<bool> CopyChiTietSanPham(string fromId, string toId)
        {
            if (!string.IsNullOrEmpty(fromId))
            {
                string fromPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_PRODUCT, fromId, "");
                string toPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_PRODUCT, toId, "");
                return await CopyFile(fromPathFile, toPathFile);
            }

            return false;
        }

        #endregion

        #region "Dinh nghia san pham"

        #region "DNSP - Chi tiet"

        public async Task<string> GetChiTietDangTin(string id)
        {
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DANGTIN, id + "/" + FILENAME_DANGTIN_CHITIET, "", "");
            return await GetContentFile_NoXml(fileName);
        }

        public async Task CreateChiTietDangTin(string id, string noidung, bool isJsonData)
        {
            string folderName = Variables.DataPath + PATH_FILE_DANGTIN + id + "/";
            CreateFolder(folderName);
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DANGTIN, id + "/" + FILENAME_DANGTIN_CHITIET, "", "");
            await CreateFile_NoXml(fileName, noidung, isJsonData);
        }

        public async Task<string> GetChiTietDinhNghiaSanPham(string id)
        {
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, id + "/" + FILENAME_DINHNGHIASANPHAM_CHITIET, "", "");
            return await GetContentFile_NoXml(fileName);
        }

        public async Task<string> GetChiTietDinhNghiaSanPhamByKey(string id, string pKey)
        {
            string chitiet = await GetChiTietDinhNghiaSanPham(id);
            return chitiet ?? string.Empty;
        }

        public void DeleteChiTietDinhNghiaSanPham(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, id + "/" + FILENAME_DINHNGHIASANPHAM_CHITIET, "", "");
                DeleteFile(fileName);
            }
        }

        public async Task<bool> CopyChiTietDinhNghiaSanPham(string fromId, string toId)
        {
            if (!string.IsNullOrEmpty(fromId))
            {
                string fromPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, fromId + "/" + FILENAME_DINHNGHIASANPHAM_CHITIET, "", "");
                string toPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, toId + "/" + FILENAME_DINHNGHIASANPHAM_CHITIET, "", "");
                return await CopyFile(fromPathFile, toPathFile);
            }

            return false;
        }

        #endregion

        #region "DNSP - Thong so ky thuat"
        public async Task CreateFileThongSoDinhNghiaSanPham(string id, string noidung, bool isJsonData)
        {
            string folderName = Variables.DataPath + PATH_FILE_DINHNGHIASANPHAM + id + "/";
            CreateFolder(folderName);
            //string fileName = folderName + FILENAME_DINHNGHIASANPHAM_THONGSO;
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, id + "/" + FILENAME_DINHNGHIASANPHAM_THONGSO, "", "");
            await CreateFile_NoXml(fileName, noidung, isJsonData);
        }

        public async Task<string> GetThongSoDinhNghiaSanPham(string id)
        {
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, id + "/" + FILENAME_DINHNGHIASANPHAM_THONGSO, "", "");

            return await GetContentFile_NoXml(fileName);
        }

        public async Task<string> GetThongSoDinhNghiaSanPhamByKey(string id, string pKey)
        {
            string chitiet = await GetThongSoDinhNghiaSanPham(id);
            return chitiet ?? string.Empty;
        }

        public void DeleteThongSoDinhNghiaSanPham(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, id + "/" + FILENAME_DINHNGHIASANPHAM_THONGSO, "", "");
                DeleteFile(fileName);
            }
        }

        public async Task<bool> CopyThongSoDinhNghiaSanPham(string fromId, string toId)
        {
            if (!string.IsNullOrEmpty(fromId))
            {
                string fromPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, fromId + "/" + FILENAME_DINHNGHIASANPHAM_THONGSO, "", "");
                string toPathFile = BuildFilePathByMaNgonNgu(PATH_FILE_DINHNGHIASANPHAM, toId + "/" + FILENAME_DINHNGHIASANPHAM_THONGSO, "", "");
                return await CopyFile(fromPathFile, toPathFile);
            }

            return false;
        }

        #endregion

        #endregion

        /* nguyencuongcs 20191106 */
        #region "Remote config for app RaoXe mobile"

        // id: chính là tên các biến môi trường ghi rõ. Vì ku Hữu gửi lên sẽ là các biến đã mã hóa dạng viết tắt
        // Lưu file & get file ở đây là nội dung rõ. Ở commonfilecontroller trước khi trả về sẽ mã hóa 3DES + RSA như token
        /*
         rl:
                {
                    connectionStrings: {
                        enviroment: "rl",
                        limitByteForImage: 300000,
                        limitQualityForImage: 85,
                        asyceUploadFile: false,
                        actionDeepLink: "rao-xe",
                        autoUpdateLayoutImages: false,
                        useNativeHTTP: true,
                        apiHost: "https://tmpstgapirv01.dailyxe.com.vn", //"https://raovatapi.dailyxe.com.vn",
                        apiHost_Sufix_DB: /api/raovat/,
                        apiHistoryHost_Sufix_DB: /api/raovat_history/,
                        apiDaiLyXe: "https://stgapidlxad02.dailyxe.com.vn",
                        apiDaiLyXe_Sufix_DB: "/api/dailyxe/",
                        apiCdn: "https://stgcdndlxad02.dailyxe.com.vn",
                        domainDynamicLinks: "https://raoxe.dailyxe.com.vn/rao",
                        domainDeepLinks: "https://dailyxe.com.vn",
                        apiHostDailyXe: "https://docker.dailyxe.com.vn",
                        timeoutApi: 2
                    },
                    ngayCapNhat: "2019-11-06T17:55:06"

                }
            }
         */
        // Chỉ lưu nội dung bên trong connectionStrings
        // Sẽ có hàm khác get noidung ra và build thành entity {connectionStrings:"", ngayCapNhat:"lấy từ FileInfo"}
        public async Task<string> GetChiTietRemoteConfig(string id)
        {
            string res = "";
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_REMOTECONFIG, id, "");
            res = await GetContentFile_NoXml(fileName);
            return res;
        }

        // Quy tắc đã thống nhất với Hữu:
        // 1. Nếu dữ liệu đã mới thì return ""
        // 2. Ngược lại thì trả về đúng cấu trúc json theo ví dụ bên trên là {connectionStrings:"", ngayCapNhat:"lấy từ FileInfo"}
        // ngayCapNhat sẽ get ra khi trả về tính tới giấy (ss) và format string nên khi ktra cũng sẽ check tới ss bằng hàm chung dạng string
        public async Task<Dictionary<string, object>> CheckRemoteConfigAndReturnDataJsonStructure(string id, string ngayCapNhatToCheck)
        {
            try
            {                
                // Kiểm tra xem id truyền vào có đúng với quy ước của nhau hay không. Nếu không thì return "" luôn. Xem như ko lấy được dữ liệu
                switch (id)
                {
                    // Nếu đúng 1 trong 3 loại này thì ko làm gì cả
                    case CommonParams.development:
                    case CommonParams.productionStage:
                    case CommonParams.productionLive:
                        break;
                    default: // ngược lại thì return null
                        return null;
                }

                Dictionary<string, object> objRes = null;

                string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_REMOTECONFIG, id, "");
                if (File.Exists(fileName))
                {
                    FileInfo fi = new FileInfo(fileName);
                    DateTime ngayCapNhatFile = fi.LastWriteTime;
                    string formatNgayCapNhatFile = CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(ngayCapNhatFile);

                    // Chỉ cần khác không cần kiểm tra lớn hay nhỏ, bao gồm cả TH ngayCapNhatToCheck = ""
                    if (ngayCapNhatToCheck != formatNgayCapNhatFile)
                    {
                        objRes = new Dictionary<string, object>();
                        string connectionStrings = await GetChiTietRemoteConfig(id);

                        objRes.Add(CommonParams.connectionStrings, connectionStrings);
                        objRes.Add(CommonParams.ngayCapNhat, formatNgayCapNhatFile);
                    }
                                       
                    return objRes;
                }

                return null;
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("CheckRemoteConfigAndReturnDataJsonStructure error: ", ex.ToString());
                return null;
            }
        }

        // nguyencuongcs 20190611: 
        // noidung này chính là value của connectionStrings bên trên. Không bao gồm key connectionStrings và các ký tự {}
        // nên nội dung file này mặc định là json=false. Nhưng nếu truyền json=false vào CreateFile_NoXml thì sẽ bị replace dấu / trong "/api.dailyxe/" ở cuối vì vi phạm cấu trúc customcomponent
        // nên truyền json = true
        public async Task<bool> CreateChiTietRemoteConfig(string id, string noidung)
        {
            // Kiểm tra xem id truyền vào có đúng với quy ước của nhau hay không. Nếu không thì return "" luôn. Xem như ko lấy được dữ liệu
            switch (id)
            {
                // Nếu đúng 1 trong 3 loại này thì ko làm gì cả
                case CommonParams.development:
                case CommonParams.productionStage:
                case CommonParams.productionLive:
                    break;
                default: // ngược lại thì return null
                    return false;
            }
            
            string fileName = BuildFilePathByMaNgonNgu(PATH_FILE_REMOTECONFIG, id, "");
            await CreateFile_NoXml(fileName, noidung, true);

            ApiDailyXeFarmDal apiDailyXeFarm = new ApiDailyXeFarmDal();
            apiDailyXeFarm.CreateFileRemoteConfig(noidung, id);

            return true;
        }

        #endregion

        #region "Style CSS"

        public async Task CreateFileStyleCSS(string pStyleCSS)
        {
            string fileName = Variables.DataPath + Variables.Path_Style;
            await System.IO.File.WriteAllTextAsync(fileName, pStyleCSS);
        }

        public async Task<string> GetStyleCSS()
        {
            string fileName = Variables.DataPath + Variables.Path_Style;
            if (System.IO.File.Exists(fileName))
            {
                return await System.IO.File.ReadAllTextAsync(fileName);
            }
            return string.Empty;
        }

        #endregion

        #region "MultiLanguage"

        public string BuildFilePathByMaNgonNgu(string pathFile, string id, string addDataPathAtFirst, string ext = ".cshtml")
        {
            //CommonMethods.GetMaNgonNguAndCheckTranslation(Request, ref _maNgonNgu, ref _isTranslate);

            string suffixLanguage = "";
            if (_isTranslate)
            {
                suffixLanguage = "." + _maNgonNgu;
            }

            if (!string.IsNullOrEmpty(ext) && !ext.StartsWith("."))
            {
                ext = "." + ext;
            }

            if (string.IsNullOrEmpty(addDataPathAtFirst))
            {
                addDataPathAtFirst = Variables.DataPath;
            }
            string fileName = addDataPathAtFirst + pathFile + id + suffixLanguage + ext;

            return fileName;
        }

        #endregion
    }
}