using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using Microsoft.VisualBasic.CompilerServices;
using System.Threading.Tasks;
using System.Text;
using raoVatApi.Common;

public static class DynamicOperators
{
    public static string[] and = { "&&", "%26%26", "and" };
    public static string[] or = { "||", "or" };
    public static string[] inOperator = { "in" };
    public static string[] equals = { "==", "===" };
    public static string[] greaterThan = { ">" };
    public static string[] lessThan = { "<" };
    public static string[] notEquals = { "!=", "!==", "<>" };
    public static string[] greaterThanOrEqualTo = { ">=" };
    public static string[] lessThanOrEqualTo = { "<=" };
    public static string[] like = { "like" };
}

public class DynamicParam
{
    public object Obj1 { get; set; }
    public static string C_Obj1 = "Obj1";
    public object Operator { get; set; }
    public static string C_Operator = "Operator";
    private object _obj2;
    public object Obj2
    {
        get
        {
            return _obj2;
        }
        set
        {
            _obj2 = value;
        }
    }
    public static string C_Obj2 = "Obj2";
    public DynamicParam() { }
    public DynamicParam(object obj1, Operators op, object obj2)
    {
        this.Obj1 = obj1;
        this.Operator = op;
        this.Obj2 = obj2;
    }
}

public class DynamicParams
{
    List<Func<TEntity, TCriteria, bool>> GetCriteriaFunctions<TEntity, TCriteria>()
    {

        var criteriaFunctions = new List<Func<TEntity, TCriteria, bool>>();

        // searching for nullable properties of criteria
        var criteriaProperties = typeof(TCriteria)
            .GetProperties()
            .Where(p => /*p.PropertyType.IsGenericType &&*/ p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>));

        foreach (var property in criteriaProperties)
        {
            // this is entity parameter
            var entityParameterExpression = Expression.Parameter(typeof(TEntity));
            // this is criteria parameter
            var criteriaParameterExpression = Expression.Parameter(typeof(TCriteria));
            // this is criteria property access: "criteria.SomeProperty"
            var criteriaPropertyExpression = Expression.Property(criteriaParameterExpression, property);
            // this is testing for equality between criteria property and entity property;
            // note, that criteria property should be converted first;
            // also, this code makes assumption, that entity and criteria properties have the same names
            var testingForEqualityExpression = Expression.Equal(
                Expression.Convert(criteriaPropertyExpression, property.PropertyType.GetGenericArguments()[0]),
                Expression.Constant(null));

            // criteria.SomeProperty == null ? true : ((EntityPropertyType)criteria.SomeProperty == entity.SomeProperty)
            var body = Expression.Condition(
                Expression.Equal(criteriaPropertyExpression, Expression.Constant(null)),
                Expression.Constant(true),
                testingForEqualityExpression);

            // let's compile lambda to method
            var criteriaFunction = Expression.Lambda<Func<TEntity, TCriteria, bool>>(body, entityParameterExpression, criteriaParameterExpression).Compile();

            criteriaFunctions.Add(criteriaFunction);
        }

        return criteriaFunctions;
    }

    public async Task<Func<TEntity, bool>> BuildDynamicExpression<TEntity>(string jsonDynamicParams)
    {
        Func<TEntity, bool> criteriaFunction = s => 1 == 1;

        try
        {

            if (string.IsNullOrEmpty(jsonDynamicParams))
            {
                return criteriaFunction;
            }

            // this is entity parameter
            var entityParameterExpression = Expression.Parameter(typeof(TEntity));
            var dynExpression = await CompileToExpression<TEntity>(entityParameterExpression, jsonDynamicParams, null);

            // entity == null ? true : dynamicExpression)
            var body = Expression.Condition(
                Expression.Equal(entityParameterExpression, Expression.Constant(null)),
                Expression.Constant(true),
                dynExpression);

            // let's compile lambda to method
            criteriaFunction = Expression.Lambda<Func<TEntity, bool>>(body, entityParameterExpression).Compile();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return criteriaFunction;
    }

    public async Task<Expression> CompileToExpression<TEntity>(Expression entityParameterExpression, string jsonDynamicParams, DynamicParam currentDynamicParam)
    {
        Expression res = null;

        if (!string.IsNullOrEmpty(jsonDynamicParams))
        {
            DynamicParam dynamicParam = CommonMethods.DeserializeObject<DynamicParam>(jsonDynamicParams);
            Expression obj1 = null;
            string op = string.Empty;
            Expression obj2 = null;
            // Type propertyType = typeof(object); 

            if (!dynamicParam.IsNullOrEmpty())
            {
                string obj1String = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Obj1));
                string obj2String = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Obj2));
                obj1 = await CompileToExpression<TEntity>(entityParameterExpression, obj1String, dynamicParam);
                op = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Operator));
                obj2 = await CompileToExpression<TEntity>(entityParameterExpression, obj2String, dynamicParam);

                return BuildExpressionOperators(obj1, op, obj2);
            }
            else
            {
                try
                {
                    PropertyInfo property = typeof(TEntity).GetProperties().Where(s => s.Name.ToLower().Equals(jsonDynamicParams.ToLower())).FirstOrDefault();

                    if (property != null)
                    {
                        var entityPropertyExpression = Expression.Property(entityParameterExpression, property);
                        res = (MemberExpression)entityPropertyExpression;
                    }
                    else
                    {
                        string obj1Param = CommonMethods.ConvertToString(currentDynamicParam.GetValue(DynamicParam.C_Obj1));
                        string opParam = CommonMethods.ConvertToString(currentDynamicParam.GetValue(DynamicParam.C_Operator));

                        if (DontNeedConvertToCorrespondingType(opParam))
                        {
                            res = Expression.Constant(jsonDynamicParams);
                        }
                        else
                        {
                            PropertyInfo obj1Property = typeof(TEntity).GetProperties().Where(s => s.Name.ToLower().Equals(obj1Param.ToLower())).FirstOrDefault();
                            if (obj1Property != null)
                            {
                                // Convert obj2 to typeof(obj1)
                                Type obj1Type = Type.GetType(obj1Property.PropertyType.FullName);
                                res = Expression.Convert(Expression.Constant(ConvertToCorrespondingType(jsonDynamicParams, obj1Type)), obj1Type);
                            }
                            else
                            {
                                res = Expression.Constant(jsonDynamicParams);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return res;
            }
        }

        if (currentDynamicParam == null)
        {
            res = CommonMethods.DefaultExpression(true);
        }
        else
        {
            res = Expression.Constant(jsonDynamicParams);
        }

        return res;
    }

    // nguyencuongcs 20180713    
    public async Task<string> BuildSqlString<TEntity>(string jsonDynamicParams)
    {
        string sqlCondition = "";

        try
        {
            if (string.IsNullOrEmpty(jsonDynamicParams))
            {
                return sqlCondition;
            }
            // this is entity parameter
            var entityParameterExpression = Expression.Parameter(typeof(TEntity));
            sqlCondition = await CompileToSqlString<TEntity>(entityParameterExpression, jsonDynamicParams, null);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return sqlCondition;
    }

    public async Task<string> CompileToSqlString<TEntity>(Expression entityParameterExpression, string jsonDynamicParams, DynamicParam currentDynamicParam)
    {
        string res = "";

        if (!string.IsNullOrEmpty(jsonDynamicParams))
        {
            DynamicParam dynamicParam = CommonMethods.DeserializeObject<DynamicParam>(jsonDynamicParams);
            Expression obj1 = null;
            string op = string.Empty;
            Expression obj2 = null;
            // Type propertyType = typeof(object); 

            if (!dynamicParam.IsNullOrEmpty())
            {
                string obj1String = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Obj1));
                string obj2String = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Obj2));
                obj1 = await CompileToExpression<TEntity>(entityParameterExpression, obj1String, dynamicParam);
                op = CommonMethods.ConvertToString(dynamicParam.GetValue(DynamicParam.C_Operator));
                obj2 = await CompileToExpression<TEntity>(entityParameterExpression, obj2String, dynamicParam);

                res = BuildSqlOperators(obj1, op, obj2);
            }
        }

        return res;
    }

    public bool DontNeedConvertToCorrespondingType(string opParam)
    {
        opParam = opParam.NullIsEmpty().ToLower();
        return (
                DynamicOperators.inOperator.Contains(opParam)
            || DynamicOperators.like.Contains(opParam)
        );
    }

    public dynamic ConvertToCorrespondingType(string val, Type targetType)
    {
        if (targetType == typeof(Byte) || targetType == typeof(Byte?)
            || targetType == typeof(decimal) || targetType == typeof(decimal?)
            || targetType == typeof(double) || targetType == typeof(double?)
            || targetType == typeof(float) || targetType == typeof(float?)
            || targetType == typeof(int) || targetType == typeof(int?)
            || targetType == typeof(long) || targetType == typeof(long?)
            || targetType == typeof(sbyte) || targetType == typeof(sbyte?)
            || targetType == typeof(short) || targetType == typeof(short?)
            || targetType == typeof(uint) || targetType == typeof(uint?)
            || targetType == typeof(ulong) || targetType == typeof(ulong?)
            || targetType == typeof(ushort) || targetType == typeof(ushort?)
        )
        {
            val = CommonMethods.FilterNumber(val);
            return CommonMethods.ConvertToInt32(val);
        }
        else if (targetType == typeof(bool) || targetType == typeof(bool?))
        {
            // Solve case val = 1 || 0 as boolean
            switch (val)
            {
                case "1":
                    val = "true";
                    break;
                case "0":
                    val = "false";
                    break;
            }

            return CommonMethods.ConvertToBoolean(val, null);
        }

        return val;
    }

    public object BuildOperators(object obj1, string op, object obj2)
    {
        if (DynamicOperators.and.Contains(op))
        {
            return Operators.AndObject(obj1, obj2);
        }
        else if (DynamicOperators.or.Contains(op))
        {
            return Operators.OrObject(obj1, obj2);
        }
        else if (DynamicOperators.equals.Contains(op))
        {
            return Operators.CompareObjectEqual(obj1, obj2, false);
        }
        else if (DynamicOperators.greaterThan.Contains(op))
        {
            return Operators.CompareObjectGreater(obj1, obj2, false);
        }
        else if (DynamicOperators.greaterThanOrEqualTo.Contains(op))
        {
            return Operators.CompareObjectGreaterEqual(obj1, obj2, false);
        }
        else if (DynamicOperators.lessThan.Contains(op))
        {
            return Operators.CompareObjectLess(obj1, obj2, false);
        }
        else if (DynamicOperators.lessThanOrEqualTo.Contains(op))
        {
            return Operators.CompareObjectLessEqual(obj1, obj2, false);
        }
        else if (DynamicOperators.notEquals.Contains(op))
        {
            return Operators.CompareObjectNotEqual(obj1, obj2, false);
        }

        return null;
    }

    public Expression BuildExpressionOperators(Expression obj1, string op, Expression obj2)
    {
        try
        {
            op = op.ToLower();
            Expression expressionRes = CommonMethods.DefaultExpression(false);

            if (DynamicOperators.and.Contains(op))
            {
                expressionRes = Expression.And(obj1, obj2);
            }
            else if (DynamicOperators.or.Contains(op))
            {
                expressionRes = Expression.Or(obj1, obj2);
            }
            else
            {
                // Visit(ref obj1, ref obj2);

                if (DynamicOperators.equals.Contains(op))
                {
                    if (obj1.ExIsType(typeof(string)))
                    {
                        expressionRes = obj1.ExStringEquals(Expression.Constant(obj2.ExGetValue(), typeof(string)));
                    }
                    else
                    {
                        expressionRes = Expression.Equal(obj1, obj2);
                    }
                }
                else if (DynamicOperators.like.Contains(op))
                {
                    // if (obj1.ExIsType(typeof(string)))
                    {
                        expressionRes = obj1.ExStringContains(Expression.Constant(obj2.ExGetValue().ToLower(), typeof(string)), true);
                    }
                }
                else if (DynamicOperators.inOperator.Contains(op))
                {
                    expressionRes = obj1.ExStringIn(obj2, true);

                }
                else if (DynamicOperators.greaterThan.Contains(op))
                {
                    expressionRes = Expression.GreaterThan(obj1, obj2);
                }
                else if (DynamicOperators.greaterThanOrEqualTo.Contains(op))
                {
                    expressionRes = Expression.GreaterThanOrEqual(obj1, obj2);
                }
                else if (DynamicOperators.lessThan.Contains(op))
                {
                    expressionRes = Expression.LessThan(obj1, obj2);
                }
                else if (DynamicOperators.lessThanOrEqualTo.Contains(op))
                {
                    expressionRes = Expression.LessThanOrEqual(obj1, obj2);
                }
                else if (DynamicOperators.notEquals.Contains(op))
                {
                    expressionRes = Expression.NotEqual(obj1, obj2);
                }
            }

            if (obj1.IsConditionalNode() || obj1.IsLogicalNode())
            {
                return expressionRes;
            }
            else
            {
                if (obj1.IsMemberNode() && (obj1.Type == typeof(bool) || obj1.Type == typeof(bool?)))
                {
                    return Expression.Condition(
                            Expression.Equal(obj1, Expression.Convert(Expression.Constant(null), obj1.Type)),
                            Expression.Constant(false),
                            expressionRes);
                }
                else
                {
                    if (!obj1.Type.IsNullableType())
                    {
                        return expressionRes;
                    }
                    else
                    {
                        return Expression.Condition(
                                Expression.Equal(obj1, Expression.Constant(null)),
                                Expression.Constant(false),
                                expressionRes);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            // CommonMethods.DefaultExpression(false);
            throw ex;
        }
    }

    public string BuildSqlOperators(Expression obj1, string op, Expression obj2)
    {
        try
        {
            string obj1ParamName = obj1.ExGetParamName();

            op = op.ToLower();
            string sqlRes = "1=1";

            if (DynamicOperators.and.Contains(op))
            {
                sqlRes = $" ({obj1ParamName} AND {obj2})";
            }
            else if (DynamicOperators.or.Contains(op))
            {
                sqlRes = $" ({obj1ParamName} OR {obj2})";
            }
            else
            {
                // Visit(ref obj1, ref obj2);

                if (DynamicOperators.equals.Contains(op))
                {
                    if (obj1.ExIsType(typeof(string)))
                    {
                        sqlRes = $" ({obj1ParamName} = '{obj2.ExGetValue()}')";
                    }
                    else
                    {
                        sqlRes = $" ({obj1ParamName} = {obj2.ExGetValue()})";
                    }
                }
                else if (DynamicOperators.like.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} LIKE N'%{obj2.ExGetValue().ToASCIIAndLower()}%')";
                }
                else if (DynamicOperators.inOperator.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} IN({obj2.ExGetValue()}))";
                }
                else if (DynamicOperators.greaterThan.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} > {obj2.ExGetValue()})";
                }
                else if (DynamicOperators.greaterThanOrEqualTo.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} >= {obj2.ExGetValue()})";
                }
                else if (DynamicOperators.lessThan.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} < {obj2.ExGetValue()})";
                }
                else if (DynamicOperators.lessThanOrEqualTo.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} <= {obj2.ExGetValue()})";
                }
                else if (DynamicOperators.notEquals.Contains(op))
                {
                    sqlRes = $" ({obj1ParamName} != {obj2.ExGetValue()})";
                }
            }

            return sqlRes;

            /*
            if (obj1.IsConditionalNode() || obj1.IsLogicalNode())
            {
                return sqlRes;
            }
            else
            {
                if (obj1.IsMemberNode() && (obj1.Type == typeof(bool) || obj1.Type == typeof(bool?)))
                {
                    return Expression.Condition(
                            Expression.Equal(obj1, Expression.Convert(Expression.Constant(null), obj1.Type)),
                            Expression.Constant(false),
                            expressionRes);
                }
                else
                {
                    if (!obj1.Type.IsNullableType())
                    {
                        return expressionRes;
                    }
                    else
                    {
                        return Expression.Condition(
                                Expression.Equal(obj1, Expression.Constant(null)),
                                Expression.Constant(false),
                                expressionRes);
                    }
                }
            }
            */
        }
        catch (Exception ex)
        {
            // CommonMethods.DefaultExpression(false);
            throw ex;
        }
    }

    //private string TranslateExpressionToSql(Expression obj1, string op, Expression obj2)
    //{
    //    return 
    //}

    private void Visit(ref Expression left, ref Expression right)
    {
        Type leftType = left.Type;
        Type rightType = right.Type;
        TypeCode leftTypeCode = Type.GetTypeCode(leftType);
        TypeCode rightTypeCode = Type.GetTypeCode(rightType);

        if (leftTypeCode == rightTypeCode)
            return;
        try
        {
            right = Expression.Convert(Expression.Constant(right.ExGetValue()), leftType);
        }
        catch (Exception ex)
        {
            throw ex;
            //right = Expression.Convert(right, Type.GetType(left.Type.FullName));
        }
    }

}

