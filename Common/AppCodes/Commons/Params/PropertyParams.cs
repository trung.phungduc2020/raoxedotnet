﻿using System.Collections.Generic;

public static class PropertyParams
{
    // nguyencuongcs 20180711
    /* SELECT DISTINCT COLUMN_NAME FROM RaoVat.INFORMATION_SCHEMA.COLUMNS */

    public const string Id = "Id";
    public const string IdAdminTao = "IdAdminTao";
    public const string IdAdminCapNhat = "IdAdminCapNhat";
    public const string NgayCapNhat = "NgayCapNhat";
    public const string Seo = "Seo";
    public const string IdSeo = "IdSeo";
    public const string Routing = "Routing";
    public const string IdRouting = "IdRouting";
}
