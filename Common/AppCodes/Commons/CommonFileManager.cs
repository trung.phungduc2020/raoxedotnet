﻿

using ImageMagick;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Common
{

    public class CommonFileManager
    {
        #region "Variables"

        public static string Driver_Domain = "http://localhost:5005"; // http://driver.gianhangvn.com
        public static string Driver_DomainNameAndPort = "ceoquang.com:7000";
        public static string BackgroundColorImage = "#FFFFFF";
        public string FolderAppData = "AppData/";
        public string FolderUpload = "AppData/FileUploads/";
        public string FolderCache = "AppData/FileCaches/";
        public const string FileNotExist = "/Resources/Images/hinh-anh-khong-ton-tai.jpg"; // Các đường dẫn này phải cộng Variables.AppPath chứ ko phải DataPath
        public const string NoImageAvailable_0x0 = "/Resources/Images/HinhAnhKhongTonTai/0x0.jpg";
        public const string NoImageAvailableFolder = "/Resources/Images/HinhAnhKhongTonTai/";
        public static string Data_NotFound = "Không tìm thấy dữ liệu bạn cần hoặc dữ liệu này bạn không có quyền chỉnh sửa";
        public const int Max_Size = 1600;
        public const int Default_Size = 300;
        public string ProjectName = ""; // Hiện tại là crm || RaoVat
        public int MinCountFilesToStartAsync = 200;

        public static Dictionary<string, bool> SizeCaches = new Dictionary<string, bool>
    {
        { "0x0",true} ,
        { "100x67",true},
        { "250x167",true},
        { "300x200",true},
        { "375x250",true},
        { "450x300",true},
        { "480x320",true},
        { "600x400",true},
        { "750x500",true} ,
        { "900x600",true}
    };

        public static Dictionary<int, int[]> Sizes = new Dictionary<int, int[]>
    {
        { 0,new int[]{0,0}},
        { 1,new int[]{100,67}},
        { 2,new int[]{250,167}},
        { 3,new int[]{300,200}},
        { 4,new int[]{375,250}},
        { 5,new int[]{450,300}},
        { 6,new int[]{480,320}},
        { 7,new int[]{600,400}},
        { 8,new int[]{750,500}},
        { 9,new int[]{900,600}}
    };

        public enum SizeImage
        {
            SizeFull = 0,
            Size100x67 = 1,
            Size250x167 = 2,
            Size300x200 = 3,
            Size375x250 = 4,
            Size450x300 = 5,
            Size480x320 = 6,
            Size600x400 = 7,
            Size750x500 = 8,
            Size900x600 = 9,
            SizeFull_Mobile = 8,
            Size100x67_Mobile = 1,
            Size250x167_Mobile = 1,
            Size300x200_Mobile = 1,
            Size375x250_Mobile = 2,
            Size450x300_Mobile = 2,
            Size480x320_Mobile = 2,
            Size600x400_Mobile = 3,
            Size750x500_Mobile = 7,
            Size900x600_Mobile = 7
        }

        #endregion

        public CommonFileManager() { }

        public CommonFileManager(string folderAppData, string folderUpload, string folderCache)
        {
            FolderAppData = !string.IsNullOrEmpty(folderAppData) ? folderAppData : FolderAppData;
            FolderUpload = !string.IsNullOrEmpty(folderUpload) ? folderUpload : FolderUpload;
            FolderCache = !string.IsNullOrEmpty(folderCache) ? folderCache : FolderCache;
        }

        #region "Commons"

        public string RewriteFile(string Link)
        {
            // RaoVat.com.com/image/ten-hinh-anh-100f500x500.jpg            
            string regurl = @"-([a-z0-9]+)(j|f)([0-9x]*).([a-z0-9]{2,4})$"; // [index][j ( jpg ), f ( file )][size].[extension]
            System.Text.RegularExpressions.Match mfile = System.Text.RegularExpressions.Regex.Match(Link, regurl, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (mfile.Success)
            {
                int ID = 0;
                Int32.TryParse(mfile.Groups[1].Value, out ID);

                string Key = mfile.Groups[2].Value;
                string page = "File";
                if (string.Compare(Key, "j", true) == 0)
                {
                    page = "Image";
                }
                else
                {
                    return "/RaoVat/File?id=" + ID + "&ext=" + mfile.Groups[4].Value;
                }

                // RaoVat.com.com/image/ten-hinh-anh-100f.jpg 
                if (string.IsNullOrEmpty(mfile.Groups[3].Value))
                {
                    return "/RaoVat/" + page + ".aspx?id=" + ID + "&ext=" + mfile.Groups[4].Value;
                }

                int width = 0;
                int height = 0;
                string[] values = mfile.Groups[3].Value.Split('x');
                if (values.Length > 1) // RaoVat.com.com/image/ten-hinh-anh-100f500x500.jpg
                {
                    width = CommonMethods.ConvertToInt32(values[0]);
                    height = CommonMethods.ConvertToInt32(values[1]);
                }
                else // RaoVat.com.com/image/ten-hinh-anh-100f2.jpg
                {
                    int prefixSize = CommonMethods.ConvertToInt32(values[0]);
                    if (Sizes.ContainsKey(prefixSize))
                    {
                        width = Sizes[prefixSize][0];
                        height = Sizes[prefixSize][1];
                    }
                }
                return "/RaoVat/" + page + "?id=" + ID + "&w=" + width + "&h=" + height + "&ext=" + mfile.Groups[4].Value;
            }
            return string.Empty;
        }

        // Driver_Domain + @"/image/.*-([0-9]+)(j|f)([0-9x]*).([a-z0-9]{2,4})$" => $1 = mFile1, $2 = mFile2, ...
        public string RewriteFileByRegRes(string mFiles) // mFiles = "mFile1|mFile2|mFile3|mFile4"
        {
            int id = 0;

            string[] regRes = mFiles.Split('|');
            int length = regRes.Length;
            string mFile1 = length >= 1 ? regRes[0] : string.Empty;
            string mFile2 = length >= 2 ? regRes[1] : string.Empty;
            string mFile3 = length >= 3 ? regRes[2] : string.Empty;
            string mFile4 = length >= 4 ? regRes[3] : string.Empty;


            string key = mFile2;
            string page = "file";
            if (string.Compare(key, "j", true) == 0)
            {
                page = "image";
            }
            else
            {
                return "file?id=" + id + "&ext=" + mFile4;
            }

            // RaoVat.com.com/image/ten-hinh-anh-100f.jpg 
            if (string.IsNullOrEmpty(mFile3))
            {
                return page + "?id=" + id + "&ext=" + mFile4;
            }

            int width = 0;
            int height = 0;
            string[] values = mFile3.Split('x');
            if (values.Length > 1) // RaoVat.com.com/image/ten-hinh-anh-100f500x500.jpg
            {
                width = CommonMethods.ConvertToInt32(values[0]);
                height = CommonMethods.ConvertToInt32(values[1]);
            }
            else // RaoVat.com.com/image/ten-hinh-anh-100f2.jpg
            {
                int prefixSize = CommonMethods.ConvertToInt32(values[0]);
                if (Sizes.ContainsKey(prefixSize))
                {
                    width = Sizes[prefixSize][0];
                    height = Sizes[prefixSize][1];
                }
            }
            return page + "id=" + id + "&w=" + width + "&h=" + height + "&ext=" + mFile4;
        }

        //public string GetLinkImageNotExisted(bool addDriverDomainAtFirst)
        //{
        //    string driver = addDriverDomainAtFirst ? Driver_Domain : "";
        //    return driver + NoImageAvailableFolder + "0x0.jpg";
        //    //return Driver_Domain + NoImageAvailableFolder + "0x0.svg";
        //}

        public string GetLinkImageNotExisted(int pChieuRong, int pChieuCao, bool addDriverDomainAtFirst)
        {
            string driver = addDriverDomainAtFirst ? Driver_Domain : "";

            if (pChieuRong < 1 || pChieuCao < 1)
            {
                return driver + NoImageAvailableFolder + "0x0.jpg";
            }

            return driver + NoImageAvailableFolder + pChieuRong + "x" + pChieuCao + ".jpg";
            //return Driver_Domain + NoImageAvailableFolder + "3x2.svg";
        }

        public string GetDataPathImageNotExisted()
        {
            return Variables.AppPath + NoImageAvailableFolder + "0x0.jpg";
            //return Variables.DataPath + NoImageAvailableFolder + "0x0.svg";
        }

        public string GetDataPathImageNotExisted(int pChieuRong, int pChieuCao)
        {
            return Variables.AppPath + NoImageAvailableFolder + pChieuRong + "x" + pChieuCao + ".jpg";
            //return Variables.DataPath + NoImageAvailableFolder + "3x2.svg";
        }

        public byte[] GetFileBytesNoImageAvailable(int? width, int? height)
        {
            // Check file exist in cache -> show noImageAvailable in cache
            string noImageAvailablePath = GetDataPathImageNotExisted(width.GetValueOrDefault(0), height.GetValueOrDefault(0));

            // Nếu không tồn tại noImage mặc định cho width & height này thì set lại mặc định hình gốc => 0x0
            if (!System.IO.File.Exists(noImageAvailablePath))
            {
                noImageAvailablePath = GetDataPathImageNotExisted();
            }

            return CommonMethods.GetFileBytes(noImageAvailablePath);
        }

        /// <summary>
        /// resize for image (jpg)
        /// </summary>
        public Bitmap ResizeImage(string pPathFile, int pWidth, int pHeight, string backgroundColorImageFromRequest)
        {
            Bitmap originalImage = null;
            if (string.IsNullOrEmpty(backgroundColorImageFromRequest))
            {
                backgroundColorImageFromRequest = BackgroundColorImage;
            }

            try
            {
                originalImage = new Bitmap(pPathFile);
                if (pWidth == 0 || pHeight == 0)
                {
                    return originalImage;
                }
                int newW = originalImage.Width;
                int newH = originalImage.Height;
                if (originalImage.Width > pWidth || originalImage.Height > pHeight)
                {
                    double percentw = (double)pWidth / (double)originalImage.Width;
                    double percenth = (double)pHeight / (double)originalImage.Height;
                    double percent = Math.Min(percentw, percenth);
                    newW = (int)(percent * originalImage.Width);
                    newH = (int)(percent * originalImage.Height);
                }
                Bitmap b = new Bitmap(pWidth, pHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                Graphics gr = Graphics.FromImage(b);
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                ColorConverter colorConvert = new ColorConverter();
                gr.Clear((Color)colorConvert.ConvertFromString(backgroundColorImageFromRequest));

                gr.DrawImage(originalImage, (pWidth - newW) / 2, (pHeight - newH) / 2, newW, newH);
                gr.Dispose();

                return b;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (originalImage != null)
                {
                    originalImage.Dispose();
                }
            }
        }

        private Image ResizeImageNew(Image imgToResize, int pWidth, int pHeight)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)pWidth / (float)sourceWidth);
            nPercentH = ((float)pHeight / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        public byte[] ConvertImgToBytes(string pathFile, int width, int height, int qualityLevel)
        {
            MagickImage magickImg = IM_ResizeFromImagePath(pathFile, width, height, qualityLevel, BackgroundColorImage);
            return magickImg.ToByteArray();
        }

        public async Task SaveFile(string pPathFileSave, string pPathFile, int pWidth, int pHeight, int? qualityLevel, string backgroundColorImageFromRequest)
        {
            //System.Drawing.Bitmap objBitmap = null;            
            try
            {
                if (!qualityLevel.HasValue)
                {
                    qualityLevel = Variables.ImageQualityLevel;
                }

                IM_ResizeImageToPath(pPathFile, pPathFileSave, pWidth, pHeight, qualityLevel.Value, backgroundColorImageFromRequest);                
            }
            catch { }
            finally
            {
                //if (objBitmap != null)
                //{
                //objBitmap.Dispose();
                //}
            }
        }

        public void DeleteFilesInFolderCache(string idFile)
        {
            string foldercache = Variables.DataPath + FolderCache + idFile;

            if (System.IO.Directory.Exists(foldercache))
            {
                string[] arrFiles = System.IO.Directory.GetFiles(foldercache);
                for (int k = 0; k < arrFiles.Length; k++)
                {
                    System.IO.File.Delete(arrFiles[k]);
                }
            }
        }

        public int DeleteFilesInFileUploadAndCache(string[] idFiles)
        {
            int result = 0;
            string pathFile = string.Empty;
            string folderPath = Variables.DataPath + FolderUpload;

            for (int i = 0; i < idFiles.Length; i++)
            {
                pathFile = folderPath + idFiles[i];

                // Xóa files trong FileUploads
                if (System.IO.File.Exists(pathFile))
                {
                    System.IO.File.Delete(pathFile);
                }

                // Xóa files trong FileCaches
                DeleteFilesInFolderCache(idFiles[i]);

                result++;
            }
            return result;
        }

        //public async Task SaveDefaultFileCaches(string fileID)
        //{
        //    string foldercache = Variables.DataPath + FolderCache + fileID;
        //    string pathFile = Variables.DataPath + FolderUpload + fileID;

        //    if (!System.IO.Directory.Exists(foldercache))
        //    {
        //        System.IO.Directory.CreateDirectory(foldercache);
        //    }
        //    string pathFileCache;
        //    int widthProduct = AdminBll.GetCauHinhChieuRongHinhAnh(Variables.MACAUHINH_WIDTHPRODUCT);
        //    int heightProduct = AdminBll.GetCauHinhChieuCaoHinhAnh(Variables.MACAUHINH_HEIGHTPRODUCT);
        //    int widthNews = AdminBll.GetCauHinhChieuRongHinhAnh(Variables.MACAUHINH_WIDTHNEWS);
        //    int heightNews = AdminBll.GetCauHinhChieuCaoHinhAnh(Variables.MACAUHINH_HEIGHTNEWS);

        //    // Product
        //    if (widthProduct > 0 && heightProduct > 0)
        //    {
        //        pathFileCache = foldercache + "/" + widthProduct + "x" + heightProduct;
        //        if (!System.IO.File.Exists(pathFileCache))
        //        {
        //            await SaveFile(pathFileCache, pathFile, widthProduct, heightProduct);
        //        }
        //    }
        //    // News
        //    if (widthNews > 0 && heightNews > 0)
        //    {
        //        pathFileCache = foldercache + "/" + widthNews + "x" + heightNews;
        //        if (!System.IO.File.Exists(pathFileCache))
        //        {
        //            await SaveFile(pathFileCache, pathFile, widthNews, heightNews);
        //        }
        //    }
        //}

        public ImageCodecInfo GetEncoder(ImageFormat format)
        {

            //ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            //foreach (ImageCodecInfo codec in codecs)
            //{
            //    if (codec.FormatID == format.Guid)
            //    {
            //        return codec;
            //    }
            //}
            //return null;

            // nguyencuongcs 20171102
            ImageCodecInfo codecInfo = ImageCodecInfo.GetImageEncoders()
            .Where(r => r.CodecName.ToUpperInvariant().Contains(format.ToString().ToUpperInvariant()))
            .Select(r => r).FirstOrDefault();
            return codecInfo;
        }

        public string BuildProjectNameWithImageString()
        {
            string res = "";

            if (!string.IsNullOrEmpty(ProjectName))
            {
                res = (ProjectName.StartsWith(@"/") ? "" : @"/") + ProjectName;
            }

            return res + (res.EndsWith(@"/") ? "" : @"/") + @"image/";
        }

        public string BuildProjectNameWithFileString()
        {
            string res = "";

            if (!string.IsNullOrEmpty(ProjectName))
            {
                res = (ProjectName.StartsWith(@"/") ? "" : @"/") + ProjectName;
            }

            return res + (res.EndsWith(@"/") ? "" : @"/") + @"file/";
        }

        #endregion

        #region "Build link Images"

        public string BuilLinkImageDefaultSize(int fileId, string fileName, bool addDriverDomainAtFirst)
        {
            if (fileId > 0)
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    return BuilLinkImageByFileName(fileId, fileName, Default_Size, Default_Size, addDriverDomainAtFirst);
                }
                else
                {
                    return BuilLinkImageDefaultSize(fileId, fileId.ToString(), addDriverDomainAtFirst);
                }
            }
            return GetLinkImageNotExisted(-1, -1, addDriverDomainAtFirst);
        }

        public string BuilLinkImage(string textLink, long fileId, int prefixSize, bool addDriverDomainAtFirst = true)
        {
            string driver = addDriverDomainAtFirst ? Driver_Domain : "";
            if (fileId > 0)
            {
                if (!Sizes.ContainsKey(prefixSize))
                {
                    return driver + "image/" + textLink + "-" + fileId + "j" + ".jpg";
                }
                return driver + "image/" + textLink + "-" + fileId + "j" + prefixSize + ".jpg";
            }
            else if (Sizes.ContainsKey(prefixSize))
            {
                return GetLinkImageNotExisted(Sizes[prefixSize][0], Sizes[prefixSize][1], addDriverDomainAtFirst);
            }
            return GetLinkImageNotExisted(-1, -1, addDriverDomainAtFirst);
        }

        public async Task<string> BuilLinkImage(string fileName, long fileId, string wxhOrPrefixSize, int chieuRong, int chieuCao, bool addDriverDomainAtFirst)
        {
            // nguyencuongcs 20180808
            // Ưu tiên wxhOrPrefixSize, sau đó mới tới width,height

            if (fileId > 0)
            {
                if (!string.IsNullOrEmpty(wxhOrPrefixSize))
                {
                    string[] values = wxhOrPrefixSize.Split('x');
                    if (values.Length > 1) // bcc.com.com/image/ten-hinh-anh-100f500x500.jpg
                    {
                        chieuRong = CommonMethods.ConvertToInt32(values[0]);
                        chieuCao = CommonMethods.ConvertToInt32(values[1]);
                    }
                    else // bcc.com.com/image/ten-hinh-anh-100f2.jpg
                    {
                        int prefixSize = CommonMethods.ConvertToInt32(values[0]);
                        if (Sizes.ContainsKey(prefixSize))
                        {
                            chieuRong = Sizes[prefixSize][0];
                            chieuCao = Sizes[prefixSize][1];
                        }
                    }
                }

                return BuilLinkImageByFileName(fileId, fileName, chieuRong, chieuCao, addDriverDomainAtFirst);
            }
            return GetLinkImageNotExisted(chieuRong, chieuCao, addDriverDomainAtFirst);
        }

        public string BuilLinkImage(string textLink, long fileId, int chieuRong, int chieuCao, bool addDriverDomainAtFirst)
        {
            string driver = addDriverDomainAtFirst ? Driver_Domain : "";

            if (fileId > 0)
            {
                if (chieuRong < 0 || chieuCao < 0)
                {
                    return driver + "image/" + textLink + "-" + fileId + "j.jpg";
                }

                int prefixSize = -1;
                foreach (int key in Sizes.Keys)
                {
                    if (Sizes[key][0] == chieuRong && Sizes[key][1] == chieuCao)
                    {
                        prefixSize = key;
                        break;
                    }
                }
                if (prefixSize > -1)
                {
                    return driver + "image/" + textLink + "-" + fileId + "j" + prefixSize + ".jpg";
                }

                return driver + "image/" + textLink + "-" + fileId + "j" + chieuRong + "x" + chieuCao + ".jpg";
            }
            return GetLinkImageNotExisted(chieuRong, chieuCao, addDriverDomainAtFirst);
        }

        public string BuilLinkImageByFileName(long fileId, string fileName, int chieuRong, int chieuCao, bool addDriverDomainAtFirst)
        {
            if (fileId > 0)
            {
                fileName = fileName.Trim();
                string TextLink = fileName;

                if (!string.IsNullOrEmpty(fileName) && fileName.Contains("."))
                {
                    TextLink = fileName.Substring(0, fileName.LastIndexOf('.'));
                }
                return BuilLinkImage(TextLink, fileId, chieuRong, chieuCao, addDriverDomainAtFirst);
            }
            return GetLinkImageNotExisted(chieuRong, chieuCao, addDriverDomainAtFirst);
        }

        #endregion

        #region "Build Link Files"

        public string GetLinkFile(object textLink, object fileId, string extension, bool addDriverDomainAtFirst)
        {
            string driver = addDriverDomainAtFirst ? Driver_Domain : "";
            return driver + "file/" + textLink + "-" + fileId + "f" + "." + extension;
        }

        public string GetLinkFile(long fileId, string fileName, bool addDriverDomainAtFirst)
        {
            if (fileId > 0)
            {
                fileName = fileName.Trim();
                string TextLink = fileName;
                string Extension = "";
                if (!string.IsNullOrEmpty(fileName))
                {
                    if (fileName.Contains("."))
                    {
                        TextLink = fileName.Substring(0, fileName.LastIndexOf('.'));
                        Extension = fileName.Substring(fileName.LastIndexOf('.') + 1);
                    }
                    else
                    {
                        Extension = "jpg";
                    }
                }

                return GetLinkFile(TextLink, fileId, Extension, addDriverDomainAtFirst);
            }
            return GetLinkImageNotExisted(-1, -1, addDriverDomainAtFirst);
        }

        #endregion

        #region ImageMagick

        public async Task<byte[]> IM_ResizeImageReturnByteArray(MagickImage magickImg, string pathFile, int width, int height, int qualityLevel)
        {
            Bitmap originalImage = null;
            MagickGeometry magickSize = null;

            try
            {
                originalImage = new Bitmap(pathFile);
                if (width == 0 || height == 0)
                {
                    return CommonMethods.ConvertBitmapToBytes(originalImage);
                }

                int originW = originalImage.Width;
                int originH = originalImage.Height;

                int newW = 0;
                int newH = 0;
                if (originalImage.Width > width || originalImage.Height > height)
                {
                    double percentw = (double)width / (double)originalImage.Width;
                    double percenth = (double)height / (double)originalImage.Height;
                    double percent = Math.Min(percentw, percenth);
                    newW = (int)(percent * originalImage.Width);
                    newH = (int)(percent * originalImage.Height);
                }

                if (newW == 0 || newH == 0)
                {
                    magickSize = new MagickGeometry(originW, originH);
                }
                else
                {
                    magickSize = new MagickGeometry(width, height);
                }

                using (MagickImage image = new MagickImage(CommonMethods.ConvertBitmapToBytes(originalImage)))
                {
                    IM_ResizeFromMagickImage(image, magickSize);
                    return image.ToByteArray();
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog("CommonFileManager ResizeImage: " + error);
                return null;
            }
            finally
            {
                if (originalImage != null)
                {
                    originalImage.Dispose();
                }
            }
        }

        public void IM_ResizeImageToPath(string imagePathFile, string destinationPathFile, int width, int height, int qualityLevel, string backgroundColor)
        {
            try
            {
                if (string.IsNullOrEmpty(backgroundColor))
                {
                    backgroundColor = BackgroundColorImage;
                }
                // Read from file
                using (MagickImage image = new MagickImage(imagePathFile))
                {
                    IM_ResizeFromMagickImage(image, width, height, qualityLevel, backgroundColor);

                    // Save the result
                    image.Write(destinationPathFile);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog("CommonFileManager ResizeImage: " + error);
            }
            finally
            {
            }
        }

        public MagickImage IM_ResizeFromImagePath(string pathFile, int width, int height, int qualityLevel, string backgroundColor)
        {
            MagickImage magickImg = new MagickImage(pathFile);
            if (magickImg != null)
            {
                IM_ResizeFromMagickImage(magickImg, width, height, qualityLevel, backgroundColor);
                return magickImg;
            }

            return null;
        }

        //public async Task<MagickImage> IM_ResizeFromImagePath_New(string pathFile, int width, int height, int qualityLevel, string backgroundColor)
        //{
        //    MagickImage magickImg = new MagickImage(pathFile);
        //    if (magickImg != null)
        //    {                
        //        var storageType = StorageType.Char;
        //        var mapping = "R";
        //        var pixelStorageSettings = new PixelStorageSettings(width, height, storageType, mapping);

        //        return new MagickImage(fileName, pixelStorageSettings);

        //        await IM_ResizeFromMagickImage(magickImg, width, height, qualityLevel, backgroundColor);
        //        return magickImg;
        //    }

        //    return null;
        //}

        public void IM_ResizeFromMagickImage(MagickImage magickImg, int width, int height, int qualityLevel, string backgroundColor)
        {
            if (magickImg != null)
            {
                MagickGeometry size = new MagickGeometry(width, height);
                // size.IgnoreAspectRatio = true;

                IM_ResizeFromMagickImage(magickImg, size);

                magickImg.Format = MagickFormat.Jpeg;
                magickImg.Quality = qualityLevel;
                magickImg.BackgroundColor = new MagickColor(backgroundColor);
            }
        }

        public void IM_ResizeFromMagickImage(MagickImage magickImg, MagickGeometry magickGeometry)
        {
            if (magickImg != null)
            {
                magickImg.Resize(magickGeometry);
                //img.AdaptiveResize(width, height);
                //img.Strip();
            }
        }

        public void IM_GetImageWidthHeight(string idFile, ref int width, ref int height)
        {
            string pathFile = CommonMethods.GetPhysicalFilePath(FolderUpload, idFile);
            try
            {
                using (MagickImage image = new MagickImage(pathFile))
                {
                    if (image != null)
                    {
                        width = image.Width;
                        height = image.Height;
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog($"IM_GetImageWidthHeight: {error}");
            }
        }

        #endregion
    }
}