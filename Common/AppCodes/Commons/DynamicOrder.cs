using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

public static class DynamicOrder
{

    public class OrderMethod
    {
        public static string OrderBy = "OrderBy";
        public static string OrderByDescending = "OrderByDescending";
        public static string ThenBy = "ThenBy";
        public static string ThenByDescending = "ThenByDescending";
    }

    public const char OrderStringSplitChar = ',';
    public const char OrderTypeSplitChar = ':';

    public class OrderType
    {
        public static string Asc = "asc";
        public static string Desc = "desc";
    }

    public class OrderExpressionRaw
    {
        public OrderExpressionRaw()
        {
            OrderType = DynamicOrder.OrderType.Asc;
        }

        public string OrderType { get; set; }
        public string PropertyName { get; set; }
    }

    public class OrderExpression
    {
        public string MethodName { get; set; }
        public string PropertyName { get; set; }
    }

    public static string GetKeyField(Type type)
    {
        var allProperties = type.GetProperties();

        var keyProperty = allProperties.SingleOrDefault(p => p.IsDefined(typeof(KeyAttribute)));

        return keyProperty != null ? keyProperty.Name : null;
    }

    // Order String Template: "id,hodem:desc,ngaytao:asc"
    // => OrderBy(id).ThenByDescending(hodem).ThenBy(ngaytao)
    private static IEnumerable<OrderExpressionRaw> BuildOrderExpressionRawFromString(string orderString)
    {
        List<OrderExpressionRaw> res = new List<OrderExpressionRaw>();

        if (!string.IsNullOrEmpty(orderString))
        {
            string[] splitOrder = orderString.Split(OrderStringSplitChar);
            if (splitOrder != null && splitOrder.Count() > 0)
            {
                foreach (string od in splitOrder)
                {
                    OrderExpressionRaw raw = BuildUnitOrderExpressionRawFromString(od);
                    if (raw != null)
                    {
                        res.Add(raw);
                    }
                }
            }
        }

        return res;
    }

    private static OrderExpressionRaw BuildUnitOrderExpressionRawFromString(string orderString)
    {
        OrderExpressionRaw res = null;

        if (!string.IsNullOrEmpty(orderString))
        {
            string[] splitValue = orderString.Split(OrderTypeSplitChar);

            if (splitValue != null && splitValue.Count() > 0)
            {
                res = new OrderExpressionRaw();
                res.PropertyName = splitValue[0];
                res.OrderType = OrderType.Asc; // Mặc định là asc

                if (splitValue.Count() > 1)
                {
                    string orderType = splitValue[1].NullIsEmpty();
                    if (string.IsNullOrEmpty(orderType))
                    {
                        res.OrderType = OrderType.Asc;
                    }
                    else
                    {
                        if (orderType.ToLower().Equals(OrderType.Desc))
                        {
                            res.OrderType = OrderType.Desc;
                        }
                        else // Nhập sai cũng lấy mặc định là asc
                        {
                            res.OrderType = OrderType.Asc;
                        }
                    }
                }
            }
        }

        return res;
    }

    private static IEnumerable<OrderExpression> BuildOrderExpressionFromRaw(IEnumerable<OrderExpressionRaw> lstOrderExpressionRaw)
    {
        List<OrderExpression> res = new List<OrderExpression>();

        if (lstOrderExpressionRaw != null)
        {
            int i = 0;
            foreach (OrderExpressionRaw raw in lstOrderExpressionRaw)
            {
                res.Add(BuildOrderExpressionFromRaw(raw, i));
                i++;
            }
        }

        return res;
    }

    private static OrderExpression BuildOrderExpressionFromRaw(OrderExpressionRaw orderExpressionRaw, int index)
    {
        OrderExpression res = new OrderExpression();

        if (orderExpressionRaw != null)
        {
            res.PropertyName = orderExpressionRaw.PropertyName;

            if (index == 0)
            {
                if (orderExpressionRaw.OrderType.ToLower() == OrderType.Asc)
                {
                    res.MethodName = OrderMethod.OrderBy;
                }
                else if (orderExpressionRaw.OrderType.ToLower() == OrderType.Desc)
                {
                    res.MethodName = OrderMethod.OrderByDescending;
                }
            }
            else
            {
                if (orderExpressionRaw.OrderType.ToLower() == OrderType.Asc)
                {
                    res.MethodName = OrderMethod.ThenBy;
                }
                else if (orderExpressionRaw.OrderType.ToLower() == OrderType.Desc)
                {
                    res.MethodName = OrderMethod.ThenByDescending;
                }
            }
        }

        return res;
    }

    public static IQueryable<T> ExecuteOrderExpression<T>(this IQueryable<T> source, string orderString)
    {
        IQueryable<T> res = null;

        IEnumerable<OrderExpressionRaw> lstRaw = BuildOrderExpressionRawFromString(orderString);
        if (lstRaw != null && lstRaw.Count() > 0)
        {
            IEnumerable<OrderExpression> lstOd = BuildOrderExpressionFromRaw(lstRaw);
            res = source.ExecuteOrderExpression(lstOd);
        }
        else
        {
            res = source;
        }

        return res;
    }

    private static IQueryable<T> ExecuteOrderExpression<T>(this IQueryable<T> source, IEnumerable<OrderExpression> lstOrderExpression)
    {
        IQueryable<T> res = null;

        if (source != null && lstOrderExpression != null)
        {
            res = source;

            foreach (OrderExpression od in lstOrderExpression)
            {
                if (od.PropertyName.Contains("."))
                {
                    // Hiện tại chỉ order dynamic được cho khóa ngoại dạng 1 - 1,                     
                    // BuildExtend để order nếu cần
                    try
                    {
                        res = res.GetOrderByFromNavigationQuery(od.PropertyName, od.MethodName);
                    }
                    catch
                    {
                        res = source;
                    }
                }
                else
                {
                    res = res.GetOrderByQuery(od.PropertyName, od.MethodName);
                }
            }
        }

        return res;
    }

    public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string orderBy)
    {
        return source.GetOrderByQuery(orderBy, OrderMethod.OrderBy);
    }

    public static IQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string orderBy)
    {
        return source.GetOrderByQuery(orderBy, OrderMethod.OrderByDescending);
    }

    public static IQueryable<T> ThenBy<T>(this IQueryable<T> source, string orderBy)
    {
        return source.GetOrderByQuery(orderBy, OrderMethod.ThenBy);
    }

    public static IQueryable<T> ThenByDescending<T>(this IQueryable<T> source, string orderBy)
    {
        return source.GetOrderByQuery(orderBy, OrderMethod.ThenByDescending);
    }

    private static IQueryable<T> GetOrderByQuery<T>(this IQueryable<T> source, string orderBy, string methodName)
    {
        try
        {
            var sourceType = typeof(T);
            var property = sourceType.GetProperty(orderBy);
            var parameterExpression = Expression.Parameter(sourceType, "x");
            var getPropertyExpression = Expression.MakeMemberAccess(parameterExpression, property);
            var orderByExpression = Expression.Lambda(getPropertyExpression, parameterExpression);
            var resultExpression = Expression.Call(typeof(Queryable), methodName,
                                                   new[] { sourceType, property.PropertyType }, source.Expression,
                                                   orderByExpression);

            return source.Provider.CreateQuery<T>(resultExpression);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private static IQueryable<T> GetOrderByFromNavigationQuery<T>(this IQueryable<T> source, string orderBy, string methodName)
    {

        try
        {

            string[] orderSplitArray = orderBy.Split('.');

            if (orderSplitArray != null && orderSplitArray.Length > 1)
            {
                string navigationName = orderSplitArray[0];
                string navigationProperty = orderSplitArray[1];

                var sourceType = typeof(T);
                var entityParameterExpression = Expression.Parameter(sourceType, "x");

                var navigation = sourceType.GetProperty(navigationName);
                var getNavigationMemberExpression = Expression.MakeMemberAccess(entityParameterExpression, navigation);

                var navType = getNavigationMemberExpression.Type;
                //var navParameterExpression = Expression.Parameter(navType, "x." + orderBy);
                var navOrderPropertyType = navType.GetProperty(navigationProperty);
                var navOrderPropertyExpression = Expression.MakeMemberAccess(getNavigationMemberExpression, navOrderPropertyType);

                var orderByExpression = Expression.Lambda(navOrderPropertyExpression, entityParameterExpression);

                var resultExpression = Expression.Call(typeof(Queryable), methodName,
                                                       new[] { sourceType, navOrderPropertyType.PropertyType }, source.Expression,
                                                       orderByExpression);

                return source.Provider.CreateQuery<T>(resultExpression);
            }

            return source;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}