using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.Distributed;
using System;

public class Variables
{

    public static string TokenAuthorization = "";

    #region "Enums"

    // Loại tin trả phí
    public enum PaidType
    {
        PinTop = 1
    }

    // Loại tin trả phí
    public enum PinTopType
    {
        Runing = 0,
        StopRuning = 1,
        Waiting = 2,
        StopWaiting = 3,
        UpdateWaiting = 4
    }

    // Tình trạng Giao Dịch
    public enum TransactionStatus
    {
        Treo = 1,
        HetHang = 2,
        ChoXe = 3,
        KetThuc = 4,
        DaKyHopDong = 5,
        DaChuyenKhoanHoaHong = 6
    }

    // Hình thức Giao Dịch
    public enum TransactionMethod
    {
        Cash = 1,
        Installment = 2
    }

    public enum PartnerType
    {
        CTV = 1,
        Member = 2,
        Anonymous = 3
    }

    public enum LoaiCauHinh
    {
        SanPham = 1
    }

    public enum DangTinState
    {
        New = 1,
        Old = 2
    }

    public enum LoaiTrang
    {
        ToanBoTrang = 1,
        TrangChu = 2,
        DanhSachSanPham = 3,
        ChiTietSanPham = 4,
        DanhSachTinTuc = 5,
        ChiTietTinTuc = 6,
        ChiTietBaiViet = 7,
        TimKiemSanPham = 8,
        LienHe = 9
    }

    public enum LoaiMenu
    {
        MenuHeThong = 1,
        MenuLink = 2,
        MenuSanPham = 3,
        MenuTinTuc = 4,
        MenuBaiViet = 5,
        MenuAlbum = 6,
        MenuDichVu = 7
    }

    public enum LoaiTinTuc
    {
        ThongThuong = 1,
        TinTucMoi = 2,
        TinTucHot = 3,
        TinTucKhuyenMai = 4
    }

    public enum LoaiModule
    {
        ModuleMenu = 1,
        ModuleSanPham = 2,
        ModuleTinTuc = 3,
        ModuleBaiViet = 4,
        ModuleQuangCao = 5,
        ModuleAlbum = 6,
        ModuleFanPage = 7,
        ModuleDichVu = 8,
        ModuleKhac = 9,
    }

    public enum ViTriModule
    {
        BenTren = 1,
        BenTrai = 2,
        OGiua = 3,
        BenPhai = 4,
        AllPage = 5,
        BenDuoi = 6,
        ToanBoWebsite = 7,
        HongTrai = 8,
        HongPhai = 9
    }

    public enum PhanLoaiSanPham
    {
        ThongThuong = 1,
        KhuyenMai = 2,
        GiamGia = 3,
        VuiLongGoi = 4
    }

    public enum TrangThaiSanPham
    {
        HienThi = 1,
        An = 2,
        HetHang = 3
    }

    public enum TrangThaiDonHang
    {
        MoiDatHang = 1,
        DaLienHe = 2,
        DaGiaoHang = 3,
        DaThanhToan = 4,
        DaNhanHang = 5
    }

    public enum PhuongThucThanhToan
    {
        COD = 1,
        ChuyenKhoan = 2,
        VanPhong = 3
    }

    public enum LoaiIconHienThi
    {
        KhongIcon = 1,
        IconMoi = 2,
        IconHot = 3
    }

    public enum DonViTien
    {
        VND = 1,
        USD = 2
    }

    public enum StatusLogin
    {
        Login = 1,
        Logout = 2
    }

    public enum KieuHienThi
    {
        ModuleSanPham_Slide = 1,
        ModuleSanPham_Normal = 2,
    }

    public enum LoaiTags
    {
        TagSanPham = 1,
        TagTinTuc = 2
    }

    public enum NhomQuyen
    {
        Admin = 1,
        BaiViet = 2
    }

    #endregion

    // Constants từ ProcessURL
    #region "Constants"

    public const string QUERY_ID = "id";
    public const string QUERY_IDNGUOIVIET = "idauthor";
    public const string QUERY_IDNGUONTIN = "idWeb";

    public const string QUERY_NGUOIVIET = "a";
    public const string QUERY_NGUONTIN = "w";
    public const string QUERY_CHITIET_TINTUC = "d";


    public const string DEFAULT_PAGE_NOT_KEY = "chitietsanpham.aspx";
    public const string PAGE_TINTUC = "tintuc.aspx";
    public const string PAGE_BAIVIET = "baiviet.aspx";

    // BỎ
    public const string QUERY_IDBAO = "idbao";
    public const string PAGEKEY_ERROR = "e";

    public const string QUERY_MENU_SANPHAM = "p";
    public const string QUERY_MENU_DICHVU = "s";
    public const string QUERY_MENU_TINTUC = "n";
    public const string QUERY_MENU_BAIVIET = "c";
    public const string QUERY_MENU_REFERENCENEWSPAPER = "bao";

    public const string QUERY_FROMPRICE = "fpr";
    public const string QUERY_TOPRICE = "tpr";
    public const string QUERY_DEVICE = "device";
    public const string DEVICE_MOBILE = "mobile";
    public const string DEVICE_DESKTOP = "desktop";



    public const string QUERY_CHITIET_DICHVU = "v";
    public const string QUERY_CHITIET_UYQUYENHANG = "uqh";

    public const string REGULAR_LANGUAGE = "(en|vi|fr)/";

    // RESTFUL API
    public const string HttpType_Get = "httpget";
    public const string HttpType_Post = "httppost";
    public const string HttpType_Put = "httpput";
    public const string HttpType_Delete = "httpdelete";

    #endregion

    #region "Caches"

    // Interface
    public const string CacheMaxAgeForStaticFiles = "15552000"; // 6 months
    public const string CacheMaxAgeForImage = "15552000"; // 6 months
    public const string CacheMaxAgeForFiles = "15552000"; // 6 months
    public const double CacheMaxAgeForSession = 10; // 10min 259200 min = 6 months
    public const string MACAUHINH_CACHEGIAODIENPAGE = "cachegiaodienpage";
    public const string MACAUHINH_CACHEGIAODIENCOMPONENTDATA = "cachegiaodiencomponentdata";
    public const string MACAUHINH_MEMORYCACHE_LOCAL = "memorycache";
    public const string MACAUHINH_MEMORYCACHE_EXTERNAL = "memorycache_external";
    public const string MACAUHINH_PREVENTDDOS = "preventddos";
    public const string MACAUHINH_CAUHINHCHITIETDNSP = "cauhinhchitietdinhnghiasanpham";
    public const string MACAUHINH_CAUHINHCHITIETDNDL = "cauhinhchitietdinhnghiadaily";

    // Editor Interface 
    // TranXuanDuc 20180604 MaBaiViet lưu trong AppData/BaiViet/
    public const string MACHINHSUAGIAODIEN = "chinhsuagiaodien";
    public const string MACHINHSUACHANTRANG = "chinhsuachantrang";

    // Application
    public const string ONLINE_USERS = "OnlineUsers";

    // Cache
    public static IDistributedCache MemoryDistributedCache;

    #endregion

    // FORMAT DATE               
    public const string DDMMYYYY = "ddMMyyyy";
    public const string DDMMYYYY_FULL = "ddMMyyyy HHmmss";
    public const string DD_MM_YYYY = "dd-MM-yyyy";
    public const string MM_DD_YYYY = "MM-dd-yyyy";
    public const string DD_MM_YYYY_SLASH = "dd/MM/yyyy";
    public const string MM_DD_YYYY_FULL = "MM-dd-yyyy HH:mm:ss";
    public const string DD_MM_YYYY_FULL = "dd-MM-yyyy HH:mm:ss";
    public const string YYYY_MM_DD_SEO = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";

    public const string ADMIN_LANGUAGE = ""; //"vi";[nguyencuongcs] - 20170321: khi nào sử dụng đa ngôn ngữ thì mở lên
    public const string DEFAULT_LANGUAGE = "";
    public const string PARAMETER_LANG = "lang";
    public const int MINUTES_CACHES = 43200; // 60 * 24 * 30 = 43200 minutes in month
    public const int MAX_THUTU = 9999;
    public const decimal MIN_PRICE = 0;
    public const int MAX_LENGTH_TIEUDETRANG = 70;
    public const int MAX_LENGTH_MOTATRANG = 160;
    public const int DEFAULT_WIDTH_IMAGE = 300;
    public const int DEFAULT_HEIGHT_IMAGE = 200;
    public const int DEFAULT_SIZE_GALLERY_PRODUCT = 400;
    public const long DEFAULTMAXFILESIZE = 300000000; // 300000000 Byte = 300MB
    public const string DEFAULT_PRICE_TITLE = "";
    public const string COMMON_KEY_SPLIT = ",";
    public const string EMAIL_KEY_SPLIT = ";";
    
    // Input value
    public const int TITLE_MINLENGTH = 1;
    public const int TITLE_MAXLENGTH = 200;
    public const int DESCRIPTION_MINLENGTH = 2;
    public const int DESCRIPTION_MAXLENGTH = 500;
    public const int SUMMARY_MAXLENGTH = 200;
    public const int MENUMANAGER_MAXLENGTH = 100;

    // CauHinh
    public const string MACAUHINH_FAVICO = "favico";
    public const string MACAUHINH_WIDTHPRODUCT = "widthproduct";
    public const string MACAUHINH_HEIGHTPRODUCT = "heightproduct";
    public const string MACAUHINH_WIDTHSERVICE = "widthservice";
    public const string MACAUHINH_HEIGHTSERVICE = "heightservice";
    public const string MACAUHINH_WIDTHNEWS = "widthnews";
    public const string MACAUHINH_HEIGHTNEWS = "heightnews";
    public const string MACAUHINH_WIDTHALBUM = "widthalbum";
    public const string MACAUHINH_HEIGHTALBUM = "heightalbum";

    // Company
    public const string MACAUHINH_LOGO = "logo";
    public const string MACAUHINH_COMPANY = "company";
    public const string MACAUHINH_ADDRESS = "address";
    public const string MACAUHINH_HOTLINE = "hotline";
    public const string MACAUHINH_TELEPHONE = "telephone";
    public const string MACAUHINH_FAX = "fax";
    public const string MACAUHINH_EMAIL = "email";
    public const string MACAUHINH_WEBSITE = "website";
    public const string MACAUHINH_GOOGLE_MAPS = "googlemaps";
    public const string MACAUHINH_GOOGLE_MAPS_API_KEY = "googlemapsapikey";
    public const string MACAUHINH_SOCIAL_FANPAGE = "fanpage";
    public const string MACAUHINH_SOCIAL_GOOGLEPLUS = "googleplus";
    public const string MACAUHINH_SOCIAL_TWITTER = "twitter";
    public const string MACAUHINH_SOCIAL_YOUTUBE = "youtube";
    public const string MACAUHINH_SOCIAL_LINKEDIN = "linkedin";
    public const string MACAUHINH_SOCIAL_INSTAGRAM = "instagram";
    public const string MACAUHINH_SOCIAL_PINTEREST = "pinterest";

    public const string MACAUHINH_GOOGLEWEBMASTERS = "googlewebmasters";
    public const string MACAUHINH_GOOGLEANALYTICS = "googleanalytics";

    // SEO Home
    public const string MACAUHINH_SEO_TITLE = "seotitle";
    public const string MACAUHINH_SEO_DESCRIPTION = "seodescription";
    public const string MACAUHINH_SEO_IDFILETRANG = "seoidfiletrang";

    // AutoNews
    public const string MACAUHINH_AUTONEWS_PROCESSDESCRIPTION = "autoprocessdescription";

    // MaXacNhan
    public const string MACAUHINH_TUDIENMAXACNHAN_SIZE = "tudienmaxacnhansize";
    public const string MACAUHINH_TUDIENMAXACNHAN_NAME = "tudienmaxacnhanname"; // Upper_G
    public const int MaxAvailableMaXacNhanInMinutes = 10;

    // Filesize
    public const string MACAUHINH_FILESIZE_MAX = "maxfilesize";
    public const string MACAUHINH_FILESIZE_TOTAL = "tongfilesize";

    // Web server farms
    public static string MACAUHINH_FARMS_WEBSERVER_DEVELOPMENT = "farmswebserver_development"; // development
    public static string MACAUHINH_FARMS_WEBSERVER_PRODUCTION = "farmswebserver_production"; // production

    // ApiDailyXe server farms
    // Dấu . trong farmsapidailyxe.production firebase key không nhận nên đổi thành farmsapidailyxe_production
    public static string MACAUHINH_FARMS_APIDAILYXE_DEVELOPMENT = "farmsapidailyxe_development"; // development    
    public static string MACAUHINH_FARMS_APIDAILYXE_PRODUCTION = "farmsapidailyxe_production"; // production

    public const string MACAUHINH_URLPREFIX_DNSP = "urlprefix_dnsp";
    public const string MACAUHINH_MAMODULE_BANNERTOP = "bannertop";
    public const string MACAUHINH_MAMODULE_MENUMAIN = "menumain";

    // raovat
    public const string MACAUHINH_GIFT_POINT_REGIST = "GiftPointRegist";
    public const string MACAUHINH_DURATIONVERIFYISVALID = "DurationVerifyIsValid";
    public const string MACAUHINH_APPVERSION = "AppVersion";                    // 0.0.24
    public const string MACAUHINH_RATE_POINT_TO_VND = "RatePointToVND";         // 1:2 (1 P = 2000 VND)
    public const string MACAUHINH_RATE_POINT_TO_TIME = "RatePointToTime";       // 1:30 (1 P = 30 mins)
    public const string MACAUHINH_ACTIVESENDSMSPHONE = "ActiveSendSmsPhone";

    // MarketingPlan MaCauHinh
    public const string MACAUHINH_MAGIOITHIEU = "MAGIOITHIEU";
    public const string MACAUHINH_PREFIX_MAGIOITHIEU = "GT";

    public const string MACAUHINH_SPECIALDAY = "SPECIALDAY";
    public const string MACAUHINH_PREFIX_SPECIALDAY = "SD";

    // KPI Auto Rule
    public const string MACAUHINH_KPI_POST = "POST";
    public const string MACAUHINH_KPI_POINT = "POINT";


    #region "Messages"

    public const string MessageSessionTimeOut = "Phiên làm việc đã hết, bạn vui lòng đăng nhập lại để tiếp tục";
    public const string MessageSessionInvalidRole = "Không có quyền truy cập vào trang này. Bạn vui lòng đăng nhập lại";

    #endregion

    #region "Session", "Cookie"        

    public const string SMobile = "mobile";
    public static string SVIEW_PRODUCT = "viewproduct";
    public static string SVIEW_SERVICE = "viewservice";
    public static string SVIEW_NEWS = "viewnews";
    public static string SVIEW_ALBUM = "viewalbum";
    public static string Session_StartDatetime = "sessionstartdate";
    public static string Session_Prefix_Admin = $"<{ProjectName}.dm>";
    public static string Session_Prefix_Client = $"<{ProjectName}.li>";

    public static string CMobile = "mobile";
    public static int Cookie_MaxLichSuTinTuc = 30; // Số lượng id tin tức đã xem nhiều nhất lưu trong cookie
    public static string Cookie_LichSuTinTuc = "istt";
    public static string Cookie_Group_Admin = $".{ProjectName}.dm";
    public static string Cookie_Group_Client = $".{ProjectName}.li";

    #endregion

    /* nguyencuongcs 20180610 */
    #region "ElasticSearch"

    public const string ElasticSearch_IndexName_TinTuc_Db = "sqldb_tintuc";
    public const string ElasticSearch_IndexName_TinTuc_Files = "appfiles_news";
    public const string ElasticSearch_IndexName_BaiViet_Db = "sqldb_baiviet";
    public const string ElasticSearch_IndexName_BaiViet_Files = "appfiles_baiviet";
    public const string ElasticSearch_IndexName_DaiLy_Db = "sqldb_daily";
    public const string ElasticSearch_IndexName_DaiLy_Files = "appfiles_daily";
    public const string ElasticSearch_IndexName_Dnsp_Db = "sqldb_dinhnghiasanpham";
    public const string ElasticSearch_IndexName_Dnsp_Files = "appfiles_dinhnghiasanpham";
    public const string ElasticSearch_IndexName_ThuongHieu_Db = "sqldb_thuonghieu";
    public const string ElasticSearch_IndexName_ThuongHieu_Files = "appfiles_thuonghieu";

    public static string ElasticSearch_Template_1 = "{\"index\":\"[[index]]\"}"
    + ",\n{\"query\": {\"query_string\" : {\"query\" : \"*[[searchstring]]*\"}}, \"from\" : [[from]], \"size\" : [[size]]"
    + "}\n";
    public static string ElasticSearch_Template_2 = "{\"index\":\"[[index]]\"}"
    + ",\n{\"query\": {\"query_string\" : {\"query\" : \"*[[searchstring]]*\"}}, \"from\" : [[from]], \"size\" : [[size]]" // Các biến năm chung trong query ko được \n đầu dòng
    + ",\"highlight\":{\"pre_tags\":[\"<strong>\"],\"post_tags\":[\"</strong>\"],\"fields\":{\"*\":{}}}" // Các biến năm chung trong query ko được \n đầu dòng
    + ",\"_source\":{\"includes\":[\"*\"],\"excludes\":[\"Content\"]}" // Các biến năm chung trong query ko được \n đầu dòng
    + "}\n";

    #endregion

    #region "Variables"

    public static string KeyFirebaseFileJson = string.Empty;
    public static string KeyFirebaseProjectId = string.Empty;

    public static string Linux_IpMaster = "171.244.35.196"; // 210.211.116.16 // 171.244.35.196
    public static string Linux_SshKey = "sshkey.pem";
    public static string Linux_PassPharse = "@acotedemoi!!!";
    public static string Linux_UserName = "root";

    public static string UrlFromClient = "";
    public static string AppPath = "";
    public static string DataPath = "";
    public static string AppSettingPathFile = "";
    public static bool IsBuildDockerImage = false;
    public static string DockerImageName = "";
    public static string DockerStartTime = "";
    // nguyencuongcs 20191108
    //public static string MainConnectionConnectedToRead = "";
    public static string BuildImageName = "";
    public static string BuildStartTime = "";

    //public static string ConnectionString = "";
    //public static string ConnectionString_RaoVat = "";
    // 2 dòng dưới chủ yếu được dùng trong CommonMethodsRaoVat

    // MOMO PROD
    public static string PartnerCode = "MOMOOTBF20191125";
    public static string AccessKey = "FUOMPxPG0RaCrgMN";
    public static string SerectKey = "dZ8Y73TsD3bY4gmt4x0vRWe9NTppAFYE";
    public static string FirebaseSecretKey = string.Empty;

    // MOMO TEST
    //public static string PartnerCode = "MOMO";
    //public static string AccessKey = "F8BBA842ECF85";
    //public static string SerectKey = "K951B6PE1waDMi640xX08PD3vg6EkVlz";

    public static int TimeOutMomoPayment = 2;

    //API Dailyxe
    public static string ApiInterfaceDaiLyXe = string.Empty;
    public static string ApiDaiLyXe = string.Empty;
    public static string ApiRaoXe = string.Empty;
    
    public static string DomainReturnUrl = "raoxe://dailyxe.com.vn?dataMoMo=";
    //public static string DomainDaiLyXeMomoRequest = "https://dailyxe.com.vn/rao-xe/confirm-momo?dataMoMo=";
    //public static string DomainRaoXeDynamicLink = "https://raoxe.page.link?link={0}&apn=vn.com.raoxe&ibi=vn.com.raoxe&afl={1}&ifl={2}&isi=1486119532&efr=1";
    public static string DomainMomoTransaction = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
    public static string PreMd5 = "d@i";

    public static string ConnectionStringPostgresql_RaoVat_Master = "";
    public static string ConnectionStringPostgresql_RaoVat_Slave = "";
    public static bool Postgresql_RaoVat_Slave_IsHealthy = true;
    public static int ConnectPostgresSlave_MaxTry = 5;
    public static int ConnectPostgresSlave_CountFailed = 5;

    public static string ConnectionStringPostgresql_RaoVat_SlaveForTest = "";
    public static string ConnectionStringPostgresql_RaoVat_History = "";
    //public static string ConnectionString_DaiLyXeSupport = "";
    public static string Environment = "";
    public static bool EnvironmentIsProduction = false;
    public static bool EnvironmentIsProductionStage = false;
    public static bool EnvironmentIsProductionLive = false;
    public static string DomainName = "ceoquang.com";
    public static string DomainNameChinh = "localhost:5000";
    public static string DomainApi = "http://192.168.0.3"; // app setting sẽ override nên ko cần sửa
    public static bool DomainUseSSL = false;
    public static bool DomainHasMobileVersion = true;
    public static string MobilePahtLoadStartWith = @"$mobile$";
    public static string MobileViewsPathLoad = @"~/Mobile/Views";
    public static string[] MobileVersionSystemName = new string[] { };
    public static string MobileFolderString = @"/Mobile";
    public static Dictionary<string, string> DomainList;
    public static int Default_IDMenuTinTuc = 2;
    public static string Default_PageSearch = @"/tin-tuc-" + Variables.Default_IDMenuTinTuc + "n.html";
    public static string Module_PageSearch = @"/module-san-pham.html";
    public static string DefaultPathImage = string.Empty;
    public static string FolderUserControl = "UserControls";
    public static string FolderUserControlForMobile = @"Mobile/UserControls";
    public static int CountRowOnPageProduct = 40;
    public static int CountRowOnPageNews = 14;
    public static int CountRowOnPageAlbum = 32;
    public static int MaxProductsOnModuleSanPham_Middle = 12;
    public static int MaxProductsOnModuleSanPham_Side = 4;
    public static string NhomQuyen_Admin = "1";
    public static string OwlCarouselSyncIDPrefix = "product_detail_gallery_sync";
    public static int ImageQualityLevel = 80;
    public static string CacheExternalStartWithString = RedisServerName + KeyCacheParams.externalParams;
    public const bool RedisOn = true;
    public const string RedisServerName = "raovatapi_";
    public static string RedisConnectionString = "127.0.0.1:6379";
    public static string RedisPassword = "@dlx2019$";
    public static string RedisFullConnectionString = "";

    public static string WebApiElasticSearch = "http://localhost:9200/";
    public static string AutoIndexElasticSearchApi = "http://localhost:8200";
    public static bool CacheHealthy = true;
    public static string CurrentCache = "";
    public static int DefaultDisplayItem = 10;
    public static int DefaultDisplayPage = 1;
    public static IEnumerable<Dictionary<string, object>> Schema_ListColumnMaximumLengthFromDb;
    public static string SchemaColumn_TableName = "TABLE_NAME";
    public static string SchemaColumn_ColumnName = "COLUMN_NAME";
    public static string SchemaColumn_DataType = "DATA_TYPE";
    public static string SchemaColumn_CharacterMaximumLength = "CHARACTER_MAXIMUM_LENGTH";
    public static IEnumerable<string> WebFarm_ListWebServer = null; //mặc định
    public static IEnumerable<string> ApiDailyXeFarm_ListWebServer = null; //mặc định
    public static string regexDicObscene = string.Empty;
    public static double timeOutCache = 60;
    public static int CountLikeAndFavorite = 100;
    public static int MaxLengthShortDescription = 300;
    public static decimal DevideCountDangTin = 1;
    public static decimal DevideCountUpTin = 0.5M;
    public static string ServerName = "local";

    //public static string RaoVat_ViewTemplate_SharedComponents_Folder = @"RaoVat/Views/Shared/Components/";
    //public static string RaoVat_ViewTemplate_SharedLayoutPage_Folder = @"RaoVat/Views/Shared/";
    public static string RaoVat_Controller_RenderComponentById = @"/Render/RenderComponentById/";
    public const string ProjectName = "RaoVat";
    // Routing api template
    public const string C_ApiRouting_1 = "api/RaoVat/[controller]";
    public const string C_ApiRouting_2 = "api/RaoVat/[controller]/[action]";

    // For versioning api
    public const string C_ApiVersioning_InterfaceData_2 = "api/RaoVat/InterfaceData/[action]";
    public const string C_ApiVersioning_InteractData_2 = "api/RaoVat/InteractData/[action]";

    public const string C_HistoryApiRouting_1 = "api/RaoVat_History/[controller]";
    public const string C_HistoryApiRouting_2 = "api/RaoVat_History/[controller]/[action]";

    // Extension Files
    public static string[] ConfigImageExtension = { ".jpg", ".jpeg", ".gif", ".png", ".bmp" };
    public static string[] ConfigFlashExtension = { ".swf" };
    public static string[] ConfigMediaExtension = { ".avi", ".mp3", ".mp4", ".mpg3", ".mpg4", ".wma", ".wmv", ".wav" };
    public static string[] ConfigValidExtension = { ".jpg", ".jpeg", ".png", ".bmp", ".gif", ".swf", ".txt", ".flv", ".doc", ".docx", ".xls", ".xlsx", ".pdf", ".rar", ".zip", ".chm", ".ppt", ".pptx", ".rtf", ".avi", ".mp3", ".mp4", ".mpg3", ".mpg4", ".wma", ".wmv", ".avi", ".wav", ".PPS", ".ogg", ".tif", ".tiff", ".jpe", ".jfif", ".dib" };

    // FileType group to folder image
    public static string[] FileTypeVideoExtension = { ".avi", ".mp4", ".wma", ".wmv", ".wav" }; // Media collapse
    public static string[] FileTypeSoundExtension = { ".mp3", ".mpg3" }; // Media collapse
    public static string[] FileTypeWordExtension = { ".doc", ".docx" };
    public static string[] FileTypeExcelExtension = { ".xls", ".xlsx" };
    public static string[] FileTypePowerpointExtension = { ".ppt", ".pptx", ".pptm" };
    public static string[] FileTypePdfExtension = { ".pdf" };

    public static string Postman_Password = "R0h2bkAwNDA3MjAxMyQ=";
    // Email Send
    public static string EmailSend_Name = "thongbao@gianhang.info";
    public static string EmailSend_Password = "VGVybWluYXRvcjE0ODg2";
    public static string EmailSend_HoTen = "Hệ thống 1OnNet";
    public static string EmailSend_Host = "mail.gianhang.info";
    public static int EmailSend_Port = 25;
    public static bool EmailSend_IsSSL = false;

    // Verify Confirmation Code
    public static string TitleConfirmationCode = "RaoXe - Mã xác thực từ hệ thống tự động";
    public static string ContentConfirmTemplate = "- Mã: {0} <br />" +
                                                  "- Gửi lúc: {1} <br />" +
                                                  "- Thời gian hiệu lực: {2} giây <br />" +
                                                  "Trân trọng.  <br />" +
                                                  "RaoXe Team.";

    // Email to collect Error
    public static string EmailToCollectError_Name = "trungminhchu@gmail.com";

    // Email Support DLX
    public static string[] EmailSupportError_Name = { "trungminhchu@gmail.com" };


    // Template Email
    public static string TemplateEmail_OrderProduct = "orderproduct";
    public static string TemplateEmail_ContactUs = "contactus";

    // Path CSS
    public static string Path_Style = @"/Style/Themes/main.min.css";
    public static string Path_Unveil_Image = @"\Style\Themes\images\unveil_image.gif";

    public static List<long> ListDeletedIdThanhVien = null;
    public static List<long> ListAvailableIdThanhVien = null;

    public static int MaxCountSentMailErrorCommon = 5;
    public static int CountSentMailCacheError = 0;
    public static int CountSentMailExecChiTietError = 0;
    public static int CountSentMailGmailApiError = 0;
    public static int CountSentMailGoogleDriveFileStream = 0;
    public static int CountSentMailCommitDbFromEntityBaseError = 0;
    public static int CountSentMailClearPool = 0;
    public static int CountSentMailPostgresSql = 0;
    public static int HourToUpTin = 1;

    #endregion

    #region email

    public class Constants
    {
        internal static string EmailSend_host = "smtp.gmail.com";
        internal static string EmailSend_name = "";  //ionicraovat@gmail.com => Get From Appsetting
        internal static string EmailSend_pass = "";  //#S123456s
        internal static int EmailSend_port = 587;
        internal static bool EMAILSEND_ISSSL = true;
        internal static string F001 = "F001"; // Đăng ký thành công
        internal static string F002 = "F002"; // Thăng cấp Ranking thành công
        internal static string F003 = "F003"; // Mua point thành công
        internal static string F004 = "F004"; // Duyệt tin thành công
        internal static string F005 = "F005"; // Duyệt tin thất bại
        internal static string F006 = "F006"; // Warning: {0}
        internal static string F007 = "F007"; // MarketingPlan Install App
        internal static string F008 = "F008"; // RaoXe - Gửi đánh giá thành công - {0}
        internal static string F009 = "F009"; // RaoXe - Gửi đánh giá thất bại - {0}
        internal static string F010 = "F010"; // RaoXe - Tạo mới email thành công.
        internal static string F011 = "F011"; // RaoXe - Cập nhật email thành công.

        internal static string E001 = "E001"; // Send mail - Login from a new device

        // LINK NOTIFICATION
        internal static string InAppAdminDuyetTin = "InAppAdminDuyetTin_";
    }

    #endregion

    #region [Hữu 23082017] Token

    //Chữ Ký * bắt buột > 120 bit
    public static readonly string SecretKey = "VGVy&^bWluYXR!!vcjE0O@@Dg2";
    //Là thông tin người phát hành(issuer) của token => không hiểu
    public static readonly string Issue = "Công ty TNHH gian hàng trực tuyến VN";
    //Là audience của token, nội dung này có thể hiểu là resource server mà chấp nhận token này =>? không hiểu
    public static readonly string Audience = "RaoVat.com.vn";
    public static SymmetricSecurityKey SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Variables.SecretKey));

    #endregion

    #region "Core Template"

    public class RawSqlOrderType
    {
        public const string Asc = "asc";
        public const string Desc = "desc";
    }

    public class RawSqlOrder
    {
        private string _propertyName;
        private string _type;
        public string PropertyName { get => _propertyName; set => _propertyName = value; }
        public string Type { get => _type; set => _type = value; }
        public string FullText { get => _fullText; set => _fullText = value; }

        private string _fullText;

        public RawSqlOrder(string propertyName, string orderType = RawSqlOrderType.Asc)
        {
            _propertyName = propertyName;
            _type = orderType;
            _fullText = _propertyName + " " + _type;
        }
    }

    public class RawSqlParameter
    {
        private string _name;
        private object _value;

        public string Name { get => _name; set => _name = value; }
        public object Value { get => _value; set => _value = value; }

        public RawSqlParameter(string name, object value)
        {
            _name = name;
            _value = value;
        }
    }

    #endregion

    #region "Sitemap"

    public const string LoaiSitemap_SitemapChung = "Chung";
    public const string LoaiSitemap_SitemapMain = "Main";
    public const string LoaiSitemap_SitemapBangGiaXe = "BangGiaXe";
    public const string LoaiSitemap_SitemapRaoVat = "RaoVat";
    public const string LoaiSitemap_SitemapThuongHieu = "ThuongHieu";
    public const string LoaiSitemap_SitemapOto = "Oto";
    public const string LoaiSitemap_SitemapTinTuc = "TinTuc";
    public const string LoaiSitemap_SitemapKhac = "Khac";

    public static List<string> ListLoaiSiteMap = new List<string>()
    {
        LoaiSitemap_SitemapChung,
        LoaiSitemap_SitemapMain,
        LoaiSitemap_SitemapBangGiaXe,
        LoaiSitemap_SitemapRaoVat,
        LoaiSitemap_SitemapThuongHieu,
        LoaiSitemap_SitemapOto,
        LoaiSitemap_SitemapTinTuc,
        LoaiSitemap_SitemapKhac
    };

    #endregion

    #region "MaXacNhan"

    // nguyencuongcs 20180603: Enum này ko lưu dưới db, tự quy định, nên ko được đổi số khi đã start dự án
    public enum LoaiXacNhan
    {
        XacNhanTaiKhoan = 1, // Register
        ForgetPassword = 2,
        UpdateUserProfile = 3,
        SendVerifyEmail = 4
    }

    public enum TypeUserPoint
    {
        GiftRegist = 1, // Tặng khi đăng ký thành viên
        Bought = 2, // Mua point
        UsedPinTop = 4, // Sử dụng đăng tin pin top
        GiftAppMarketing = 5, // Gift when Share/Install
        GiftInputCode = 6, // Gift when input code
        GiftInviteCode = 7, // Gift when invite code
        Error = 8,
        OldRecord = 9,
        DoNothing = 10,
    }

    public enum NotificationSystemType
    {
        SendToAll = 1,
        SendToUser = 2,
        SendToLogin = 3
    }

    public enum KpiAutoType
    {
        AutoDuyet = 1,
    }
    #endregion
}
