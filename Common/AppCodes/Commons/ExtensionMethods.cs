using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using raoVatApi.ModelsCrm;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using raoVatApi.Models;
using raoVatApi.Common;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using System.Globalization;
using Newtonsoft.Json;
using System.IO;

public static class ExtensionMethods
{
    /* ========== String ========== */
    public static string NullIsEmpty(this String value)
    {
        return value == null ? string.Empty : value;
    }
    public static string NullIsDefaultValue(this String value, string defaultValue)
    {
        return string.IsNullOrEmpty(value) ? defaultValue : value;
    }
    public static string ToASCII(this String value)
    {
        return CommonMethods.ConvertUnicodeToASCII(value);
    }
    public static string ToASCIIAndLower(this String value)
    {
        return CommonMethods.ConvertUnicodeToASCII(value).ToLower();
    }
    public static string AddFirstSplash(this String value)
    {
        //if (!string.IsNullOrEmpty(value))
        //{
        //    return (value.StartsWith("/") ? value : ("/" + value));
        //}

        return value.AddFirstChar("/");
    }
    public static string AddLastSplash(this String value)
    {
        //if (!string.IsNullOrEmpty(value))
        //{
        //    return (value.EndsWith("/") ? value : (value + "/"));
        //}

        return value.AddLastChar("/");
    }
    public static string RemoveFirstSplash(this String value)
    {
        //if (!string.IsNullOrEmpty(value))
        //{
        //    return (value.StartsWith("/") ? value : ("/" + value));
        //}

        return value.RemoveFirstChar("/");
    }
    public static string RemoveLastSplash(this String value)
    {
        return value.RemoveLastChar("/");
    }

    public static string ConvertUnicodeToASCII(this string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            text = "";
        }
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\p{IsCombiningDiacriticalMarks}+");
        string strFormD = text.ToLower().Normalize(System.Text.NormalizationForm.FormD);
        string res = regex.Replace(strFormD, String.Empty).Replace('đ', 'd');
        return res;
    }
    public static string ToTextMinifier(this string text)
    {
        if (text == null)
        {
            text = "";
        }
        text = text.NullIsEmpty().Trim();
        text = System.Text.RegularExpressions.Regex.Replace(text, @"[\n\t\r]+", " ");
        text = text.Replace("/\"", "\"");
        text = text.RemoveWhiteSpace();
        return text;
    }
    public static string RemoveWhiteSpace(this string text)
    {
        if (text == null)
        {
            text = "";
        }
        text = text.NullIsEmpty().Trim();
        text = System.Text.RegularExpressions.Regex.Replace(text, @"[ ]+", " ");
        return text;
    }

    public static string AddFirstChar(this String value, string charValue)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return (value.StartsWith(charValue) ? value : (charValue + value));
        }

        return value;
    }
    public static string AddLastChar(this String value, string charValue)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return (value.EndsWith(charValue) ? value : (value + charValue));
        }

        return value;
    }
    public static string RemoveFirstChar(this String value, string charValue)
    {
        if (!string.IsNullOrEmpty(value))
        {
            return (value.StartsWith(charValue) ? value.Substring(1, value.Length - 1) : value);
        }

        return value;
    }
    public static string RemoveLastChar(this String value, string charValue = "")
    {
        if (!string.IsNullOrEmpty(value))
        {
            if (string.IsNullOrEmpty(charValue))
            {
                return value.Substring(0, value.Length - 1);
            }
            else
            {
                return (value.EndsWith(charValue) ? value.Substring(0, value.Length - 1) : value);
            }
        }

        return value;
    }
    /* ========== DateTime ========== */
    public static DateTime NullIsMinValue(this DateTime? value)
    {
        return !value.HasValue ? DateTime.MinValue : value.Value;
    }

    /* ========== IEntityBase ========== */
    public static object GetValue(this IEntityBase obj, string propertyName)
    {
        return CommonMethods.GetPropertyValue(obj, propertyName);
    }

    public static void SetValue(this IEntityBase obj, string propertyName, object val)
    {
        CommonMethods.SetPropertyValue(obj, propertyName, val);
    }

    public static void SetValue(this IEntityBase_Int64 obj, string propertyName, object val)
    {
        CommonMethods.SetPropertyValue(obj, propertyName, val);
    }

    public static T FirstOrNewObject<T>(this IEnumerable<T> source)
    {

        if (source == null)
        {
            return Activator.CreateInstance<T>();
        }

        T obj = source.FirstOrDefault();

        if (obj == null)
        {
            obj = Activator.CreateInstance<T>();
        }

        return obj;
    }

    public static T FirstOrNewObject<T>(this IEnumerable<T> source, Expression<Func<T, bool>> predicate)
    {

        if (source == null)
        {
            return Activator.CreateInstance<T>();
        }

        T obj = source.AsQueryable().Where(predicate).FirstOrDefault();

        if (obj == null)
        {
            obj = Activator.CreateInstance<T>();
        }

        return obj;
    }

    /* ========== object ========== */
    public static object GetValue(this object obj, string propertyName)
    {
        return CommonMethods.GetPropertyValue(obj, propertyName);
    }

    public static bool HasProperty(this object obj, string propertyName)
    {
        return CommonMethods.HasProperty(obj, propertyName);
    }

    public static PropertyInfo GetPropertyInfo(this object obj, string propertyName)
    {
        return CommonMethods.GetPropertyInfo(obj, propertyName);
    }

    public static Type ExGetType(this PropertyInfo obj)
    {
        if (obj == null)
        {
            return typeof(object);
        }
        else
        {
            try
            {
                try
                {
                    // avoide nullable type
                    return Nullable.GetUnderlyingType(obj.PropertyType) ?? obj.PropertyType.GetGenericArguments()[0];
                }
                catch
                {
                    return Type.GetType("System." + obj.PropertyType.Name);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public static bool ToBoolean(this object obj, bool? defaultValue)
    {
        return CommonMethods.ConvertToBoolean(obj, defaultValue).Value;
    }

    public static bool IsString(this object obj)
    {
        Type t = obj.GetType();
        if (t != null)
        {
            return t.FullName.ToLower().Contains(".string") || t.UnderlyingSystemType.ToString().ToLower().Contains(".string");
        }

        return false;
    }

    public static bool IsBoolean(this object obj)
    {
        Type t = obj.GetType();
        if (t != null)
        {
            return t.FullName.ToLower().Contains(".bool") || t.UnderlyingSystemType.ToString().ToLower().Contains(".bool");
        }

        return false;
    }

    public static bool IsNumber(this object obj)
    {
        Type t = obj.GetType();
        if (t != null)
        {
            bool res = (t.FullName.ToLower().Contains(".int") || t.UnderlyingSystemType.ToString().ToLower().Contains(".int")
                //||  t.FullName.ToLower().Contains("byte") || t.UnderlyingSystemType.ToString().ToLower().Contains("byte")
                //|| t.FullName.ToLower().Contains("double") || t.UnderlyingSystemType.ToString().ToLower().Contains("double")
                //|| t.FullName.ToLower().Contains("float") || t.UnderlyingSystemType.ToString().ToLower().Contains("float")
                );

            return res;
        }

        return false;
    }

    public static bool IsDatetime(this object obj)
    {
        Type t = obj.GetType();
        if (t != null)
        {
            bool res = t.FullName.ToLower().Contains(".datetime") || t.UnderlyingSystemType.ToString().ToLower().Contains(".datetime");
            return res;
        }

        return false;
    }

    /* ========== PropertyInfo ========== */
    public static bool IsDateTime(this PropertyInfo propInfo)
    {
        return propInfo.PropertyType.FullName.ToLower().Contains("datetime") || propInfo.PropertyType.UnderlyingSystemType.ToString().ToLower().Contains("datetime");
    }

    public static bool IsString(this PropertyInfo propInfo)
    {
        return propInfo.PropertyType.FullName.ToLower().Contains("string") || propInfo.PropertyType.UnderlyingSystemType.ToString().ToLower().Contains("string");
    }

    /* ========== DynamicParam ========== */
    public static bool IsNullOrEmpty(this DynamicParam param)
    {
        return param == null || (param.Obj1 == null && param.Operator == null && param.Obj2 == null);
    }

    /* ========== Type ========== */
    public static bool IsNullableType(this Type type)
    {
        if (type == null)
        {
            return false;
        }

        return type.FullName.ToLower().Contains("nullable");
    }

    /* ========== Expression ========== */
    public static string GetNodeType(this Expression ex)
    {
        if (ex == null)
        {
            return string.Empty;
        }
        return CommonMethods.ConvertToString(ex.GetValue("NodeType"));
    }

    public static bool IsConstantNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "constant";
    }

    public static bool IsConvertNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "convert";
    }

    public static bool IsMemberNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "memberaccess";
    }

    public static bool IsParameterNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "parameter";
    }

    public static bool IsConditionalNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "conditional";
    }

    public static bool IsLogicalNode(this Expression ex)
    {
        return ex.GetNodeType().ToLower() == "and" || ex.GetNodeType().ToLower() == "or";
    }

    public static Type ExGetType(this Expression ex)
    {
        if (ex == null)
        {
            return typeof(object);
        }
        else
        {
            try
            {
                try
                {
                    // avoide nullable type
                    return Nullable.GetUnderlyingType(ex.Type) ?? ex.Type.GenericTypeArguments[0];
                }
                catch
                {
                    return Type.GetType("System." + ex.Type.Name); //FullName
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

    public static string ExGetValue(this Expression ex)
    {
        try
        {
            if (ex.IsConstantNode())
            {
                return CommonMethods.ConvertToString(ex.GetValue("Value"));
            }
            else if (ex.IsConvertNode())
            {
                return CommonMethods.ConvertToString(ex.GetValue("Operand").GetValue("Value"));
            }
            else if (ex.IsMemberNode())
            {
                return GetMemberExpressionValueAsString(ex);
            }
            else if (ex.IsParameterNode())
            {
                return ex.ToString();
            }
            return ex.ToString();
        }
        catch (Exception e)
        {
            string exString = e.ToString();
            return string.Empty;
        }
    }

    // public static object GetMemberExpressionValue(Expression member)
    // {
    //     try
    //     {

    //         // var objectMember = Expression.Convert(member, memberType);            

    //         // var getterLambda = Expression.Lambda<Func<Byte?>>(objectMember);
    //         // // dynamic getterLambda = CommonMethods.ConvertToByte(Expression.Lambda<Func<Byte?>>(Expression.Constant(member)).Compile(), null);

    //         // var getter = getterLambda.Compile();

    //         // return getter;

    //         Type memberType = member.Type;
    //         var getterLambda = Expression.Lambda<Func<Byte?>>(member);

    //         var me = (MemberExpression)((MemberExpression)getterLambda.Body).Expression;
    //         var ce = (ConstantExpression)me.Expression;
    //         var fieldInfo = ce.Value.GetType().GetField(me.Member.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
    //         var value = (NhomKhachHang)fieldInfo.GetValue(ce.Value);

    //         return value;
    //     }
    //     catch (Exception ex)
    //     {
    //         throw ex;
    //     }
    // }

    public static string GetMemberExpressionValueAsString(Expression member)
    {
        return string.Empty; // CommonMethods.ConvertToString(GetMemberExpressionValue(member));
    }

    public static bool ExIsType(this Expression ex, Type compareType)
    {
        Type exType = ex.ExGetType();

        return exType != null && exType.FullName.ToLower().Equals(compareType.FullName.ToLower());
    }

    public static Expression ExStringContains(this Expression ex, Expression valueCompare, bool ignoreCase)
    {
        try
        {
            // MethodInfo miS = typeof(String).GetMethod("StartsWith", new Type[] { typeof(String) });
            // MethodInfo miC = typeof(String).GetMethod("Contains", new Type[] { typeof(String) });
            // MethodInfo miE = typeof(String).GetMethod("EndsWith", new Type[] { typeof(String) });

            var toStringMethod = typeof(object).GetMethod("ToString");
            // Convert data to string
            ex = Expression.Call(ex, toStringMethod);

            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            if (ignoreCase)
            {
                MethodInfo toLower = typeof(string).GetMethod("ToLower", Type.EmptyTypes);
                ex = Expression.Call(ex, toLower);
            }

            var containsMethodExp = Expression.Call(
                ex,
                method,
                valueCompare);
            return containsMethodExp; // Expression.Constant(Expression.Lambda<Func<bool>>(containsMethodExp).Compile()());
        }
        catch
        {
            return CommonMethods.DefaultExpression(false);
        }
    }

    public static Expression ExStringEquals(this Expression ex, Expression valueCompare)
    {
        try
        {
            MethodInfo method = typeof(string).GetMethod("Equals", new[] { typeof(string) });
            var containsMethodExp = Expression.Call(ex, method, valueCompare);
            return containsMethodExp; // Expression.Constant(Expression.Lambda<Func<bool>>(containsMethodExp).Compile()());
        }
        catch
        {
            return CommonMethods.DefaultExpression(false);
        }
    }

    public static Expression ExStringIn(this Expression ex, Expression exValue, bool ignoreCase, char splitChar = ',')
    {
        try
        {
            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });

            var toStringMethod = typeof(object).GetMethod("ToString");
            var containsMethod = typeof(List<string>).GetMethod("Contains");

            string stringValue = exValue.ExGetValue();

            if (ignoreCase)
            {
                stringValue = stringValue.ToLower();
            }

            List<string> lstValue = stringValue.Split(splitChar).ToList();

            var containsCall = Expression.Call(
                    Expression.Constant(lstValue),
                    containsMethod,
                    Expression.Call(ex, toStringMethod));

            return containsCall; // Expression.Constant(Expression.Lambda<Func<bool>>(containsMethodExp).Compile()());
        }
        catch
        {
            return CommonMethods.DefaultExpression(false);
        }
    }

    // Chỉ xử lý cho trờng hợp Expression đơn, có nghĩa là tồn tại 1 Param. Ví dụ: Param_0.TieuDeTinTuc => Lấy ra TieuDeTinTuc
    public static string ExGetParamName(this Expression ex)
    {
        string res = "";
        try
        {
            res = ex != null ? ex.ToString() : "";
            string[] arrRes = res.Split(".");
            if (arrRes != null && arrRes.Count() >= 2)
            {
                res = arrRes[1];
            }
        }
        catch (Exception e)
        {
            string exString = e.ToString();
            return "";
        }

        return res;
    }

    public static string ConvertListToString(this List<string> l, string strSeparator = ",", string def = "")
    {
        try
        {
            return l.ToArray().ConvertArrayToString(strSeparator, def);
        }
        catch (System.Exception)
        {
            return def;
        }
    }

    public static string ConvertArrayToString(this string[] l, string strSeparator = ",", string def = "")
    {
        try
        {
            return String.Join(strSeparator, l);
        }
        catch (System.Exception)
        {
            return def;
        }
    }

    /* ========== Dictionanry ========== */
    public static object GetValueFromKey(this Dictionary<string, object> dic, string key, string defaultValue)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return dic[key] == null ? string.Empty : dic[key];
        }

        return defaultValue;
    }

    public static object GetValueFromKeyDefaultNull(this Dictionary<string, object> dic, string key, string defaultValue)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return dic[key] == null ? null : dic[key];
        }

        return defaultValue;
    }

    public static string GetValueFromKey_ReturnString(this Dictionary<string, object> dic, string key, string defaultValue)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.ConvertToString(dic[key]);
        }

        return defaultValue.NullIsEmpty();
    }

    public static int GetValueFromKey_ReturnInt32(this Dictionary<string, object> dic, string key, int defaultValue = -1)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.ConvertToInt32(dic.GetValueFromKey_ReturnString(key, ""), defaultValue).Value;
        }

        return defaultValue;
    }

    public static long GetValueFromKey_ReturnInt64(this Dictionary<string, object> dic, string key, long defaultValue = -1)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.ConvertToInt64(dic.GetValueFromKey_ReturnString(key, ""), defaultValue).Value;
        }

        return defaultValue;
    }

    public static decimal GetValueFromKey_ReturnDecimal(this Dictionary<string, object> dic, string key, decimal defaultValue = 0)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.ConvertToDecimal(dic.GetValueFromKey_ReturnString(key, ""), defaultValue).Value;
        }

        return defaultValue;
    }

    public static bool GetValueFromKey_ReturnBool(this Dictionary<string, object> dic, string key, bool defaultValue = false)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.ConvertToBoolean(dic.GetValueFromKey_ReturnString(key, ""), defaultValue).Value;
        }

        return defaultValue;
    }

    public static Dictionary<string, object> GetValueFromKey_ReturnDic(this Dictionary<string, object> dic, string key)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.DeserializeObject<Dictionary<string, object>>((dic.GetValueFromKey_ReturnString(key, "")));
        }

        return new Dictionary<string, object>();
    }

    public static IEnumerable<Dictionary<string, object>> GetValueFromKey_ReturnIEnumerableDic(this Dictionary<string, object> dic, string key)
    {
        if (dic != null && dic.Keys.Contains(key))
        {
            return CommonMethods.DeserializeObject<IEnumerable<Dictionary<string, object>>>((dic.GetValueFromKey_ReturnString(key, "")));
        }

        return new List<Dictionary<string, object>>();
    }

    // nguyencuongcs 20180712
    public static T ToObject<T>(this Dictionary<string, object> source)
        where T : class, IEntityBase
    {
        T destinationObject = Activator.CreateInstance<T>(); //  new T();
                                                             //var someObjectType = destinationObject.GetType();        

        foreach (var it in source)
        {
            //someObjectType
            //         .GetProperty(it.Key)
            //         .SetValue(destinationObject, it.Value, null);
            CommonMethods.SetPropertyValue(destinationObject, it.Key, it.Value);
        }

        return destinationObject;
    }

    // nguyencuongcs 20180712
    public static Dictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
    {
        return source.GetType().GetProperties(bindingAttr).ToDictionary
        (
            propInfo => propInfo.Name,
            propInfo => propInfo.GetValue(source, null)
        );

    }

    /* ========== IEnumerable<Dictionanry> ========== */
    public static IEnumerable<T> ToObject<T>(this IEnumerable<Dictionary<string, object>> source)
    {
        var watch = System.Diagnostics.Stopwatch.StartNew();
        var start = DateTime.Now;

        if (source == null || source.Count() < 1)
        {
            return new List<T>();
        }

        IEnumerable<T> res;
        string way = "";
        way = "way 'DeserializeObject Json'";

        CultureInfo culture = new CultureInfo("en-US");

        var format = "dd/MM/yyyy h:mm:ss tt"; // "dd/MM/yyyy"; // your datetime format
        var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format, Culture = culture };

        string strSourceJson = CommonMethods.SerializeObject(source, dateTimeConverter);
        res = CommonMethods.DeserializeObject<IEnumerable<T>>(strSourceJson);

        /* PP này chậm mất khoảng vài giây
        catch
        {
            List<T> lstFilesTmp = new List<T>();
            T entity;
            way = "way 'ToObject' foreach";
            foreach (Dictionary<string, object> item in source)
            {
                if (item != null && item.Count > 0)
                {
                    entity = item.ToObject<T>();
                    lstFilesTmp.Add(entity);
                }
            }
            res = lstFilesTmp.AsEnumerable();
        }
        */
        watch.Stop();
        var elapsedMs = watch.ElapsedMilliseconds;
        CommonMethods.WriteLogPerformanceTest($"ToObject<T> IEnumerable ({way}) for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

        return res;
    }

    /************** JObject  ***************/
    public static Dictionary<string, object> ToDictionary(this JObject obj)
    {
        if (obj == null)
        {
            return new Dictionary<string, object>();
        }
        var result = obj.ToObject<Dictionary<string, object>>();

        var JObjectKeys = (from r in result
                           let key = r.Key
                           let value = r.Value
                           where value.GetType() == typeof(JObject)
                           select key).ToList();

        var JArrayKeys = (from r in result
                          let key = r.Key
                          let value = r.Value
                          where value.GetType() == typeof(JArray)
                          select key).ToList();

        JArrayKeys.ForEach(key => result[key] = ((JArray)result[key]).Values().Select(x => ((JValue)x).Value).ToArray());
        JObjectKeys.ForEach(key => result[key] = ToDictionary(result[key] as JObject));

        return result;
    }

    public static T ToEntity<T>(this JObject obj) where T : class, IEntityBase
    {
        try
        {
            if (obj == null)
            {
                return null;
            }
            var result = obj.ToObject<T>();

            return result;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static T ConvertToObject<T>(this JObject obj)
    {
        if (obj == null)
        {
            return default(T);
        }

        try
        {
            return obj.ToObject<T>();
        }
        catch (Exception)
        {
            return default(T);
        }
    }

    /************** JToken  ***************/
    public static T ConvertToObject<T>(this JToken obj)
    {
        if (obj == null)
        {
            return default(T);
        }

        try
        {
            return obj.ToObject<T>();
        }
        catch (Exception)
        {
            return default(T);
        }
    }

    public static JToken GetValueFromKey(this JToken obj, string key)
    {
        try
        {
            return obj[key];
        }
        catch (Exception)
        {
            return null;
        }
    }

    /* ========== Array ============ */
    public static T[] SubArray<T>(this T[] data, int index, int length)
    {
        T[] result = new T[length];
        try
        {
            Array.Copy(data, index, result, 0, length);
        }
        catch
        {
            // Nếu length > số phần tử còn lại
            length = data.Length - index;
            Array.Copy(data, index, result, 0, length);
        }
        return result;
    }

    /* ========== long, int64 ============= */
    public static long NegativeIsZero(this long data)
    {
        long value = CommonMethods.ConvertToInt64(data);
        if (value < 0)
        {
            value = 0;
        }
        return value;
    }

    /* ========== string Json ============= */
    public static T ConvertToObject<T>(this string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return default(T);
        }
        try
        {
            T result = JsonConvert.DeserializeObject<T>(text);
            return result;
        }
        catch
        {
            return default(T);
        }
    }

    // Assembly
    // GetLinkerTime
    public static string GetBuildDateFromAssembly(this Assembly assembly, TimeZoneInfo target = null)
    {
        DateTime? localTime = null;
        string res = "";
        try
        {
            var filePath = assembly.Location;

            FileInfo fi = new FileInfo(filePath);
            localTime = fi.LastWriteTime;
        }
        catch (Exception ex)
        {
            CommonMethods.WriteLog("GetBuildDateFromAssembly: " + ex.ToString());
            res = "GetBuildDateFromAssembly lỗi";
        }

        if (localTime.HasValue)
        {
            res = CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(localTime.Value);
        }

        return res;
    }

}