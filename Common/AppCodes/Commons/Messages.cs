public class Messages
{
    // ERR_: Error
    public const string ERR_001 = "{0} không tồn tại.";
    public const string ERR_002 = "Vui lòng nhập dữ liệu.";
    public const string ERR_003 = "Vui lòng chọn dữ liệu.";
    public const string ERR_004 = "Không có dữ liệu.";
    public const string ERR_005 = "Chưa nhập {0}";
    public const string ERR_006 = "{0} đã tồn tại.";
    public const string ERR_007 = "Mã xác thực không hợp lệ hoặc đã hết hạn";
    public const string ERR_008 = "Đã gửi mã xác thực trước đó";
    public const string ERR_009 = "Mã xác thực đã hết hạn";
    public const string ERR_010 = "{0} chưa được nhập hoặc không hợp lệ.";
    public const string ERR_011 = "{0} đang được duyệt.";
    public const string ERR_012 = "Bạn chưa đăng ký duyệt tin này.";
    public const string ERR_013 = "Đã có thành viên đăng ký duyệt tin này.";
    public const string ERR_014 = "Bạn không có quyền thao tác.";
    public const string ERR_015 = "Error config.";
    public const string ERR_016 = "Mã không hợp lệ.";
    public const string ERR_017 = "Mã đã sử dụng.";
    public const string ERR_018 = "Không tồn tại chiến dịch.";
    public const string ERR_019 = "Insert thất bại.";
    public const string ERR_020 = "Tài khoản đã kích hoạt mã giới thiệu.";
    public const string ERR_021 = "Admin đang duyệt. Không thể thay đổi dữ liệu.";
    public const string ERR_022 = "{0} đã Pin Top.";
    public const string ERR_023 = "{0} đã thay đổi, hãy thoát ra và vào lại để làm mới dữ liệu.";
    public const string ERR_024 = "Sai dữ liệu.";
    public const string ERR_025 = "Đang trong quá trình thanh toán hoặc thanh toán không cùng 1 thiết bị! Hãy thử lại sau 2 phút hoặc thanh toán trên thiết bị cũ.";
    public const string ERR_026 = "Signature failed.";
    public const string ERR_027 = "TransactionStatus failed.";
    public const string ERR_Http_BadRequest = "BadRequest";
    public const string ERR_Http_UnAuthorized = "UnAuthorized";

    // INFO_: Infomation
    public const string INFO_TINHTRANGCU = "Đã qua sử dụng";
    public const string INFO_TINHTRANGMOI = "Xe mới";
    public const string INFO_DADUYET = "Đã duyệt";
    public const string INFO_CHUADUYET = "Chưa duyệt";
    public const string INFO_HETHAN = "(Hết hạn)";
    public const string INFO_CONHAN = "(Còn hạn)";

    // WARN_: Warning

    // NOTI_; Notification
    public const string NOTI_NEW_TIEUDE_CHOXETDUYET = "Tin mới - Chờ xét duyệt";
    public const string NOTI_UPDATE_TIEUDE_CHOXETDUYET = "Update tin mới - Chờ xét duyệt";

}