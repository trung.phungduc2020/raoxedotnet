﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using raoVatApi.Response;
using raoVatApi.Common;
using System.Threading.Tasks;
using raoVatApi.Models;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using System.Xml.Serialization;
using raoVatApi.ModelsRaoVat;

public class ProcessURL
{
    #region "Constants"

    //public const string QUERY_ID = "id";
    public const string QUERY_IDMENU = "idMenu";
    public const string QUERY_IDTINTUC = "idTinTuc";
    public const string QUERY_IDMONHOC = "idMonHoc";
    public const string QUERY_IDKHOAHOC = "idKhoaHoc";
    public const string QUERY_IDTAILIEU = "idTaiLieu";
    public const string QUERY_IDBAIVIET = "idBaiViet";
    public const string QUERY_REWRITEURL = "rewriteUrl";
    public const string QUERY_IDNGUOIVIET = "idAuthor";
    public const string QUERY_IDNGUONTIN = "idWeb";
    public const string QUERY_IDDNSP = "idDinhNghiaSanPham";
    public const string QUERY_IDDAILY = "idDaiLy";
    public const string QUERY_IDTHUONGHIEU = "idThuongHieu";
    public const string QUERY_IDDONGXE = "idDongXe";
    public const string QUERY_DAILYNOIBAT = "dailyNoiBat";
    public const string QUERY_TINKHUYENMAI = "tinKhuyenMai";

    public const string QUERY_NGUOIVIET = "a";
    public const string QUERY_CHITIET_THUONGHIEU = "b";
    public const string QUERY_CHITIET_TINTUC = "d";
    public const string QUERY_CHITIET_DNSP = "e";
    public const string QUERY_CHITIET_DAILY = "h";
    public const string QUERY_CHITIET_BAIVIET = "i";
    public const string QUERY_CHITIET_QUANGCAO = "j";
    public const string QUERY_CHITIET_DONGXE = "k";
    public const string QUERY_CHITIET_TINKHUYENMAI = "m";
    public const string QUERY_PAGE = "p";
    public const string QUERY_SEARCH = "s";
    public const string QUERY_NGUONTIN = "w";
    public const string QUERY_THUONGHIEU = "f";
    public const string QUERY_THUONGHIEU_BRAND = "brand";
    public const string QUERY_DAILY = "l";
    public const string QUERY_TINHTHANH = "g";
    public const string QUERY_CHITIET_MONHOC = "m";
    public const string QUERY_CHITIET_TAILIEU = "t";
    public const string QUERY_CHITIET_MAXACNHAN = "mm";

    public const string DEFAULT_PAGE_NOT_KEY = "/chitietsanpham";
    public const string PAGE_TINTUC = "/tintuc";
    public const string PAGE_BAIVIET = "/baiviet";

    // BỎ
    public const string QUERY_IDBAO = "idbao";
    public const string PAGEKEY_ERROR = "e";

    public const string QUERY_MENU_SANPHAM = "p";
    public const string QUERY_MENU_DICHVU = "s";
    public const string QUERY_MENU_TINTUC = "n";
    public const string QUERY_MENU_BAIVIET = "c";
    public const string QUERY_MENU_REFERENCENEWSPAPER = "bao";

    public const string QUERY_FROMPRICE = "fpr";
    public const string QUERY_TOPRICE = "tpr";
    public const string QUERY_DEVICE = "device";
    public const string DEVICE_MOBILE = "mobile";
    public const string DEVICE_DESKTOP = "desktop";

    // TranXuanDuc 20180522 lọc bảng giá xe theo tháng/năm
    public const string QUERY_THANG = "thang";
    public const string QUERY_NAM = "nam";



    public const string QUERY_CHITIET_DICHVU = "v";
    public const string QUERY_CHITIET_UYQUYENHANG = "uqh";
    public const string QUERY_DATA64 = "at64";

    public const string REGULAR_LANGUAGE = "(en|vi|fr)/";

    //tronghuu95 05112018 bổ dung một số query
    public const string QUERY_HANGXE = "hx";

    public const string QUERY_DONGXE = "dx";
    public const string QUERY_MAUXE = "mx";
    public const string QUERY_NHOMMAU = "nmx";

    public const string QUERY_LOAIXE = "lx";
    public const string QUERY_NSX = "nsx";


    #endregion

    #region "Variables"    

    public static Dictionary<string, object> PageKeys = new Dictionary<string, object>()
    {
        // {QUERY_MENU_TINTUC, "/tintuc/" },
        // {QUERY_NGUOIVIET, "/tintuc/" },
        // {QUERY_NGUONTIN, "/tintuc/" },
        // {QUERY_MENU_BAIVIET, "/baiviet/" },
        // {QUERY_CHITIET_TINTUC,"/chitiettintuc/"},
        // {PAGEKEY_ERROR,"/error/"}
    };

    public List<string> ListNotRewriteLinkWhenStartWith = new List<string> {
            "/admin/",
            "/admin",
            "/client/",
            "/client",
            "/api/",
            "/render/",
            "/render",
            "/resources/",
            "/resources",
            "/assets/",
            "/assets",
            "/dist",
            "/dist/",
            "/image-test",
            "/image-test/",
    };

    public List<string> ListNotRewriteLinkWhenEndWith = new List<string> {
            ".css",
            ".js",
            ".svg",
    };

    public List<string> ListNotRewriteLinkWhenContains = new List<string> {
            "{{",
            "}}",
    };

    public static Dictionary<string, object> FixPages = new Dictionary<string, object>
    {
        // {"","/"},
        // {"/","/"},
        // {"/admin","/admin/"}, // ở RewriteUrl đã kiểm tra /admin/ thì ko thực hiện RewriteLink
        // {"/mobile/","/Mobile/"},

        // {"/tin-tuc/","/"},
        // {"/tin-tuc-moi/","/tintuc/"},
        // {"/tim-kiem-tin-tuc/","/tintuc/"},

        // {"/lien-he.html","/lienhe"},
        // {"/contact-us.html","/lienhe"},
        // {"/ban-do.html","/bando"},
        // {"/maps.html","/bando"},
        // {"/rss.rss","/rss"},
        // {"/sitemap.xml","/sitemap"},
        // {"/page-not-found.html", "/error" },


        // {"/open.html", "/openemail" }// test email
    };

    private HttpContext _httpContext;
    public HttpContext HttpContext
    {
        get
        {
            // nguyencuongcs 20180321
            //if (_httpContext == null)
            {
                _httpContext = new HttpContextAccessor().HttpContext;
            }

            return _httpContext;
        }
        set
        {
            _httpContext = value;
        }
    }

    private CommonFileManager _cm;
    public CommonFileManager Cm
    {
        get
        {
            // nguyencuongcs 20180321
            // if (_cm == null)
            {
                _cm = new CommonFileManager();
            }
            return _cm;
        }
    }

    #endregion

    #region "Common"

    public async Task CheckAdmin()
    {
        //if (HttpContext != null)
        //{
        //    AdminBll admiBll = new AdminBll();
        //    Admin data = await admiBll.SearchAdminByIdsDontCareIsMain(HttpContext.Request.GetTokenIdAdmin(), 1);

        //    if (data == null || data.Id < 1)
        //    {
        //        RedirectToLogin(null, null);
        //    }
        //}
        //else
        //{
        //    RedirectToLogin(null, null);
        //}
    }

    public static void RedirectToLogin(RewriteContext rc, ProcessURL pu)
    {
        if (rc == null)
        {
            rc = new RewriteContext();

            rc.HttpContext = new HttpContextAccessor().HttpContext;
        }

        if (pu == null)
        {
            pu = new ProcessURL(rc.HttpContext);
        }

        if (rc.HttpContext == null)
        {
            rc.HttpContext = new HttpContextAccessor().HttpContext;
        }

        rc.Redirect302(pu.BuilLinkPageLogin());
    }

    #endregion

    public ProcessURL(HttpContext context)
    {
        HttpContext = context;
    }

    /// <summary>
    /// Lấy Query dạng 200x100
    /// </summary>
    /// <param name="code"></param>
    /// <returns></returns>
    private string GetCodeQuery(string code)
    {
        System.Text.StringBuilder Sb = new System.Text.StringBuilder();
        string reg = @"([0-9]*)([a-z])";
        System.Text.RegularExpressions.MatchCollection m = System.Text.RegularExpressions.Regex.Matches(code, reg, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        if (m.Count > 0)
        {
            for (int i = 0; i < m.Count; i++)
            {
                if (Sb.Length > 0)
                    Sb.Append("&");
                Sb.Append(m[i].Groups[2].Value + "=" + m[i].Groups[1].Value);
            }
            return Sb.ToString();
        }
        return code;
    }

    public string BuildURL(string textlink, string[] keys, object[] datas)
    {
        if (keys == null)
            return textlink.AddFirstSplash() + "-" + datas[0] + ".html";

        System.Text.StringBuilder sbLink = new System.Text.StringBuilder();
        for (int i = 0; i < keys.Length; i++)
        {
            sbLink.Append(datas[i] + keys[i]);
        }
        return textlink.AddFirstSplash() + "-" + sbLink + ".html";
    }

    public string BuilLinkPageDoNotAttack()
    {
        return CommonMethods.GetPathDefaultDomain() + "/do-not-attack.html";
    }

    public string BuilLinkPageError()
    {
        return CommonMethods.GetPathDefaultDomain() + "/page-not-found.html";
    }

    public string BuilLinkPageLogin()
    {
        return CommonMethods.GetPathDefaultDomain() + "/admin/login";
    }

    private bool IsCheckExtension(string link)
    {
        if (link.Contains(".ashx"))
        {
            return true;
        }
        if (link.Contains(".js"))
        {
            return true;
        }
        if (link.Contains(".css"))
        {
            return true;
        }
        if (link.Contains(".map"))
        {
            return true;
        }
        return false;
    }

    public string GetLinkRewrite(RewriteContext rewriteContext, string link, ref int idSeo, ref string systemName)
    {
        string result = "";
        // nguyencuongcs: đã chuyển ra startup
        /* 
        // Url file
        result = Cm.RewriteFile(link);
        if (!string.IsNullOrEmpty(result))
        {
            return result;
        }*/

        // Url data
        result = RewriteLinkNew(rewriteContext, link, ref idSeo, ref systemName);
        if (!string.IsNullOrEmpty(result))
        {
            return result;
        }

        if (!IsCheckExtension(link))
        {
            // Redirect 404            
            // HttpContext.Response.Redirect(BuilLinkPageError());
            rewriteContext.Redirect404(BuilLinkPageError());
        }

        return link;
    }

    public string RewriteLinkNew(RewriteContext rewriteContext, string link, ref int idSeo, ref string systemName)
    {
        string res = string.Empty;
        bool foundSystemRouting = false;
        InterfaceBLL bll = new InterfaceBLL(null);

        link = !link.EndsWith("/") ? (link + "/") : link;
        InterfaceBLL _bll = new InterfaceBLL(null);

        IEnumerable<Routing> sourceRouting = null; // bll.SearchAllRouting().Result;
        /*
        if (sourceRouting != null && sourceRouting.Count() > 0)
        {
            IEnumerable<Routing> sourceRawUrl = sourceRouting.Where(s => s.IsReg == false);
            sourceRawUrl = sourceRawUrl != null ? sourceRawUrl.ToList() : null;
            Routing foundRouting = sourceRawUrl.FirstOrDefault(s => s.RawUrlRule.NullIsEmpty().Equals(link, StringComparison.OrdinalIgnoreCase));

            if (foundRouting != null)
            {
                foundSystemRouting = true;
                int idMenu = foundRouting.IdMenu.GetValueOrDefault(-1);
                systemName = foundRouting.SystemName;

                if (idSeo < 0) // Nếu không tìm thấy idSeo trong bảng SeoPage ở ngoài RewriteUrlRule thì lấy theo idSeo ở bảng Menu map bằng idMenu đã lưu trong bảng Routing
                {
                    idSeo = foundRouting.IdSeo; // api đang lấy IdSeo của Menu khóa ngoại trong bảng Routing
                }

                int pageId = -1;
                string manualController = "";
                string manualViewName = "";

                GetDataRoutingFromFoundItem(rewriteContext.HttpContext.Request, foundRouting, systemName, ref pageId, ref manualController, ref manualViewName);
                if (pageId > 0)
                {
                    res = "/routing/?pageId=" + CommonMethods.ConvertToString(pageId);
                }
                else
                {
                    res = manualController + "?viewname=" + manualViewName;
                }

                res += "&idMenu=" + idMenu;
                res = CreateNewLinkWithQuery(res);
            }
            else // trường hợp này nếu không tìm thấy idSeo thì sẽ lấy theo chi tiết tin tức || sản phẩm trong từng page .cshtml để tiết kiệm lượt gọi đến api
            {
                IEnumerable<Routing> sourceReg = sourceRouting.Where(s => s.IsReg == true);
                sourceReg = sourceReg != null ? sourceReg.ToList() : null;

                if (sourceReg != null && sourceReg.Count() > 0)
                {
                    // nguyencuongcs 20180718: thêm params vào link check reg
                    string queryString = rewriteContext.HttpContext.Request.QueryString.ToString();
                    // link đã tự động add dấu "/" vào cuối còn Request.Path thì ko
                    string pathAndQueryString = rewriteContext.HttpContext.Request.Path + queryString + "/";
                    Routing item;
                    for (int i = 0; i < sourceReg.Count(); i++)
                    {
                        item = sourceReg.ElementAt(i);
                        if (item != null)
                        {
                            string regRule = item.RegRule.NullIsEmpty();

                            if (!string.IsNullOrEmpty(regRule))
                            {
                                System.Text.RegularExpressions.Match m2 = System.Text.RegularExpressions.Regex.Match(pathAndQueryString, regRule, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                                if (m2.Success)
                                {
                                    foundSystemRouting = true;
                                    systemName = item.SystemName.NullIsEmpty();
                                    int countGroup = m2.Groups.Count;
                                    string additionalQueryParam = item.RegAfterMatch.NullIsEmpty();

                                    // nguyencuongcs 20180329: tránh bị ddos vào chi tiết tin tức | sản phẩm bằng cách thay id
                                    // chuỗi regex routing phải tạo đúng trình tự nhận rewriteUrl là $1
                                    if (additionalQueryParam.Contains(Variables.MACAUHINH_PREVENTDDOS))
                                    {
                                        try
                                        {
                                            string rewriteUrl = m2.Groups[1].Value;
                                            if (rewriteUrl.Length <= 5) // nếu đúng tiêu đề tin tức 1 câu chữ bình thường phải có khoảng trắng (sẽ được thay bằng)
                                            {
                                                if (!rewriteUrl.Equals("hddos"))
                                                {
                                                    rewriteContext.Redirect404(BuilLinkPageDoNotAttack());
                                                }
                                            }
                                            else if (!rewriteUrl.Contains("-"))
                                            {
                                                rewriteContext.Redirect404(BuilLinkPageDoNotAttack());
                                            }
                                        }
                                        catch { };
                                    }

                                    for (int j = 1; j <= countGroup; j++)
                                    {
                                        additionalQueryParam = additionalQueryParam.Replace($"${j}", m2.Groups[j].Value);
                                    }

                                    int pageId = -1;
                                    string manualController = "";
                                    string manualViewName = "";

                                    GetDataRoutingFromFoundItem(rewriteContext.HttpContext.Request, item, systemName, ref pageId, ref manualController, ref manualViewName);

                                    if (pageId > 0)
                                    {
                                        res = "/routing/?pageId=" + CommonMethods.ConvertToString(pageId);
                                    }
                                    else
                                    {
                                        res = manualController + "?viewname=" + manualViewName;
                                    }

                                    res += additionalQueryParam.StartsWith("&") ? additionalQueryParam : ("&" + additionalQueryParam);
                                    res = CreateNewLinkWithQuery(res);

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!foundSystemRouting)
        {
            rewriteContext.Redirect404(BuilLinkPageError());
        }
        */
        return res;
    }

    private void GetDataRoutingFromFoundItem(HttpRequest request, Routing foundItem, string systemName, ref int pageId, ref string controller, ref string viewName)
    {
        if (foundItem != null)
        {
            bool isMobileVersion = request.GetMobileVersionFromSessionAndCookieForClient();
            // nguyencuongcs 20180724 - Mobile version: Hiện tại chỉ routing mobile cho system RaoVat
            // Nếu muốn chuyển luôn cho hệ thống tin tức thì làm, cấu hình routing mobile xong xuôi hết thì bổ sung vào MobileVersionSystemName sau
            if (Variables.MobileVersionSystemName.Contains(systemName) && isMobileVersion)
            {
                //pageId = foundItem.MobilePageId;
                //controller = foundItem.MobileController;
                //viewName = foundItem.MobileViewName;
            }
            else
            {
                pageId = foundItem.PageId.GetValueOrDefault(-1);
                controller = foundItem.Controller;
                viewName = foundItem.ViewName;
            }
        }
    }



    // public string RewriteLink(string link)
    // {
    //     string res = string.Empty;

    //     InterfaceBLL bll = new InterfaceBLL();

    //     FixPages = bll.SearchFixPages().Result;
    //     PageKeys = bll.SearchPageKeys().Result;

    //     // Xử lý mấy trang fix như /tin-tuc/ hoặc /tin-tuc-moi/ hoặc lien-he.html
    //     if (FixPages != null && FixPages.ContainsKey(link))
    //     {
    //         res = CreateNewLinkWithQuery(CommonMethods.ConvertToString(FixPages[link]));
    //     }
    //     else if (FixPages.ContainsKey(link + "/"))
    //     {
    //         res = CreateNewLinkWithQuery(CommonMethods.ConvertToString(FixPages[link + "/"]));
    //     }
    //     else
    //     {
    //         // Link Chi tiết tin tức
    //         string reg = "-([0-9]+)([a-z]*)([0-9a-z]*).html";
    //         System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(link, reg, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
    //         if (m.Success)
    //         {
    //             string pagekey = m.Groups[2].Value;
    //             if (string.IsNullOrEmpty(pagekey) && string.IsNullOrEmpty(m.Groups[3].Value))
    //             {
    //                 res = DEFAULT_PAGE_NOT_KEY + "?id=" + m.Groups[1].Value;
    //             }
    //             else if (PageKeys != null && PageKeys.ContainsKey(pagekey))
    //             {
    //                 // string searchquery = HttpContext.Request.QueryString.ToString();
    //                 // if (!string.IsNullOrEmpty(searchquery))
    //                 // {
    //                 //     if (searchquery.StartsWith("?"))
    //                 //     {
    //                 //         searchquery = searchquery.Remove(0, 1);
    //                 //     }
    //                 //     searchquery = "&" + searchquery;
    //                 // }

    //                 switch (pagekey)
    //                 {
    //                     case QUERY_NGUOIVIET:
    //                         res = QUERY_IDNGUOIVIET;
    //                         break;
    //                     case QUERY_NGUONTIN:
    //                         res = QUERY_IDNGUONTIN;
    //                         break;
    //                     default:
    //                         res = QUERY_ID;
    //                         break;
    //                 }
    //                 // res = PageKeys[pagekey] + "?" + res + "=" + m.Groups[1].Value + searchquery;
    //                 res = CreateNewLinkWithQuery(PageKeys[pagekey] + "?" + res + "=" + m.Groups[1].Value);
    //             }
    //         }
    //         // Link menu
    //         else
    //         {
    //             // frame by nguyencuongcs 20171012 

    //             // Link không chứa dấu / ở cuối
    //             // if (!link.EndsWith("/"))                 
    //             // {
    //             //     link = CreateNewLinkWithQuery(link + "/");
    //             //     HttpContext.Redirect301(CommonMethods.GetPathDefaultDomain() + link);
    //             // }
    //             // else
    //             {
    //                 link = !link.EndsWith("/") ? (link + "/") : link;
    //                 string[] arrRewriteUrl = link.Split('/');
    //                 if (arrRewriteUrl.Length > 2)
    //                 {
    //                     string RewriteUrl = arrRewriteUrl[arrRewriteUrl.Length - 2];
    //                     if (!string.IsNullOrEmpty(RewriteUrl))
    //                     {
    //                         InterfaceBLL _bll = new InterfaceBLL();
    //                         IEnumerable<Dictionary<string, object>> dicData = _bll.SearchMenuByRewriteUrl(RewriteUrl, string.Empty).Result;
    //                         if (dicData != null && dicData.Count() > 0)
    //                         {
    //                             int IdMenu = CommonMethods.ConvertToInt32(dicData.ElementAt(0)["Id"]);
    //                             // res = PAGE_TINTUC;

    //                             // nguyencuongcs 20171011: chỗ này muốn dùng idRouting để xác định controller hoặc pageId nào sẽ được gọi

    //                             int IdLoaiMenu = CommonMethods.ConvertToInt32(dicData.ElementAt(0)["IdLoaiMenu"]);

    //                             switch (IdLoaiMenu)
    //                             {
    //                                 case (int)Variables.LoaiMenu.MenuTinTuc:
    //                                     res = PAGE_TINTUC;
    //                                     break;
    //                                 case (int)Variables.LoaiMenu.MenuBaiViet:
    //                                     res = PAGE_BAIVIET;
    //                                     break;
    //                             }

    //                             if (!string.IsNullOrEmpty(res))
    //                             {
    //                                 // string searchquery = HttpContext.Request.QueryString.ToString();
    //                                 // if (!string.IsNullOrEmpty(searchquery))
    //                                 // {
    //                                 //     if (searchquery.StartsWith("?"))
    //                                 //     {
    //                                 //         searchquery = searchquery.Remove(0, 1);
    //                                 //     }
    //                                 //     searchquery = "&" + searchquery;
    //                                 // }
    //                                 res = CreateNewLinkWithQuery(res + "?id=" + IdMenu);
    //                             }
    //                         }
    //                         else
    //                         {
    //                             IEnumerable<Dictionary<string, object>> sourceRouting = bll.SearchRouting().Result;

    //                             if (sourceRouting != null && sourceRouting.Count() > 0)
    //                             {
    //                                 Dictionary<string, object> item;
    //                                 for (int i = 0; i < sourceRouting.Count(); i++)
    //                                 {
    //                                     item = sourceRouting.ElementAt(i);
    //                                     if (item != null && CommonMethods.ConvertToString(item["RawUrlRule"]).Equals(link, StringComparison.OrdinalIgnoreCase))
    //                                     {
    //                                         // res = CommonMethods.ConvertToString(item["Controller"]) + "?viewname=" + CommonMethods.ConvertToString(item["ViewName"]);
    //                                         res = CommonMethods.ConvertToString(item["Controller"]) + "?pageId=" + CommonMethods.ConvertToString(item["PageId"]);
    //                                         res = CreateNewLinkWithQuery(res);
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }

    //     //if (Variables.DomainHasMobileVersion)
    //     //{
    //     //    bool mobileversion = InterfaceBLL.GetMobileVersionFromSessionAndCookie();
    //     //    List<string> ignoreList = new List<string>() { "sitemap", "robots" };

    //     //    if (mobileversion && !link.Contains("ajaxpro"))
    //     //    {
    //     //        if (!res.Contains(@"/Mobile/"))
    //     //        {
    //     //            res = "/Mobile/" + res;
    //     //        }
    //     //    }
    //     //    else if (link.StartsWith(@"/mobile/"))
    //     //    {
    //     //        if (!res.Contains(@"/Mobile/"))
    //     //        {
    //     //            res = "/Mobile/" + res;
    //     //        }
    //     //    }
    //     //}

    //     return res;
    // }

    private string CreateNewLinkWithQuery(string link)
    {
        string query = HttpContext.Request.QueryString.ToString();
        if (!string.IsNullOrEmpty(query))
        {
            if (!link.Contains("?"))
            {
                query = query.StartsWith("?") ? query : ("?" + query);
            }
            else
            {
                query = query.StartsWith("?") ? ("&" + query.Remove(0, 1)) : ("&" + query);
            }
        }
        return link + query;
    }

    public string RewriteLinkFullDomain(string link, bool isMobileVersion = false)
    {
        string res = link;

        if (!string.IsNullOrEmpty(res) && !res.StartsWith(@"/"))
        {
            res = @"/" + res;
        }
        //string currentHost = HttpContext.Request.GetPathCurrentHost();
        string currentHost = Variables.DomainNameChinh;
        currentHost = currentHost.StartsWith("http") ? currentHost : CommonMethods.FormatUrlPath(currentHost);

        res = (res.StartsWith(currentHost) ? "" : currentHost) + (isMobileVersion ? Variables.MobileFolderString : string.Empty) + res;
        return res;
    }

    public string RewriteLinkFullDomainChinh(string link, bool isMobileVersion = false)
    {
        string res = link;

        if (!string.IsNullOrEmpty(res) && !res.StartsWith(@"/"))
        {
            res = @"/" + res;
        }
        //string currentHost = HttpContext.Request.GetPathCurrentHost();
        string currentHost = Variables.DomainNameChinh;
        currentHost = currentHost.StartsWith("http") ? currentHost : CommonMethods.FormatUrlPath(currentHost);

        res = (res.StartsWith(currentHost) ? "" : currentHost) + (isMobileVersion ? Variables.MobileFolderString : string.Empty) + res;
        return res;
    }

    #region "Menu"

    public string RewriteMenuLink(int idMenu, string rewriteUrl)
    {
        if (string.IsNullOrEmpty(rewriteUrl))
        {
            return "/";
        }

        if (rewriteUrl.Contains(".html"))
        {
            return "/" + rewriteUrl;
        }

        return (rewriteUrl.StartsWith("/") ? rewriteUrl : "/" + rewriteUrl) + "/"; //nguyencuongcs 20180318
        // return HttpContext.Request.GetPathCurrentHost() + (rewriteUrl.StartsWith("/") ? rewriteUrl : "/" + rewriteUrl) + "/"; // nguyencuongcs 20170910
    }

    public string RewriteMenuLinkWithDomain(string idMenu, string rewriteUrl, bool isMobileVersion = false)
    {
        string res = RewriteMenuLink(CommonMethods.ConvertToInt32(idMenu), rewriteUrl);
        return RewriteLinkFullDomain(res, isMobileVersion);
    }

    #endregion

    #region "SanPham"
    public string RewriteChiTietSanPhamLink(int pIDSanPham, string rewriteUrl)
    {
        return BuildURL(rewriteUrl, null, new object[] { pIDSanPham });
    }

    public string RewriteChiTietSanPhamLinkWithDomain(int pIDSanPham, string rewriteUrl, bool isMobileVersion = false)
    {
        string res = RewriteChiTietSanPhamLink(pIDSanPham, rewriteUrl);
        return RewriteLinkFullDomain(res, isMobileVersion);
    }

    #endregion

    public string RewriteTagsLink(int loaiTag, int idTag, string rewriteUrl, bool isMobileVersion = false)
    {
        string res = string.Empty;
        string newRewriteUrl = string.Empty;

        switch (loaiTag)
        {
            case (int)Variables.LoaiTags.TagSanPham:
                newRewriteUrl = RewriteLinkFullDomain(@"/san-pham.html?tag={0}-{1}", isMobileVersion);
                res = string.Format(newRewriteUrl, rewriteUrl, idTag);
                break;
            case (int)Variables.LoaiTags.TagTinTuc:
                newRewriteUrl = RewriteLinkFullDomain(@"/tin-tuc.html?tag={0}-{1}", isMobileVersion);
                res = string.Format(newRewriteUrl, rewriteUrl, idTag);
                break;
            default:
                res = BuilLinkPageError();
                break;
        }
        return res;
    }

    public string RewriteChiTietMonHocLink(string id, string rewriteUrl)
    {
        return BuildURL(rewriteUrl, new string[] { QUERY_CHITIET_MONHOC }, new object[] { id });
    }

    public string RewriteChiTietMonHocLinkWithDomain(string id, string rewriteUrl, bool isMobileVersion = false)
    {
        string res = RewriteChiTietMonHocLink(id, rewriteUrl);
        return RewriteLinkFullDomain(res, isMobileVersion);
    }

    public string RewriteChiTietTaiLieuLink(string id, string rewriteUrl)
    {
        return BuildURL(rewriteUrl, new string[] { QUERY_CHITIET_TAILIEU }, new object[] { id });
    }

    public string RewriteChiTietTaiLieuLinkWithDomain(string id, string rewriteUrl, bool isMobileVersion = false)
    {
        string res = RewriteChiTietTaiLieuLink(id, rewriteUrl);
        return RewriteLinkFullDomain(res, isMobileVersion);
    }

    //public string RewriteUocTinhGiaTheoDNSPLink(string idDnsp, string rewriteUrl)
    //{
    //    InterfaceBLL bll = new InterfaceBLL();
    //    string pageUocTinhGiaUrl = bll.GetPageUrlByMaRouting(Variables.MaRouting_PageUocTinhGiaXeOto).Result;
    //    if (!string.IsNullOrEmpty(pageUocTinhGiaUrl))
    //    {
    //        rewriteUrl = CommonMethods.CoalesceParamsWithoutDuplicateCoachar("/", pageUocTinhGiaUrl, rewriteUrl);
    //    }
    //    return BuildURL(rewriteUrl, new string[] { QUERY_CHITIET_DNSP }, new object[] { idDnsp });
    //}
    //public string RewriteUocTinhGiaTheoDNSPLinkWithDomain(string idDnsp, string rewriteUrl, bool isMobileVersion = false)
    //{
    //    string res = RewriteUocTinhGiaTheoDNSPLink(idDnsp, rewriteUrl);
    //    return RewriteLinkFullDomain(res, isMobileVersion);
    //}    
}