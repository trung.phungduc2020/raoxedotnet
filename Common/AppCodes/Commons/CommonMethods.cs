using GmailApi;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using raoVatApi.ModelsRaoVat;
using Renci.SshNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace raoVatApi.Common
{

    public static class CommonMethods
    {

        #region Variables

        public static DateTime MinSqlDateTimeValue = new DateTime(1753, 1, 1);

        public static string patternA_Lower = "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ";
        public static string patternA_Upper = "À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ";
        public static string patternE_Lower = "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ";
        public static string patternE_Upper = "È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ";
        public static string patternI_Lower = "ì|í|ị|ỉ|ĩ";

        public static string patternI_Upper = "Ì|Í|Ị|Ỉ|Ĩ";
        public static string patternO_Lower = "ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ";
        public static string patternO_Upper = "Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ";
        public static string patternU_Lower = "ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ";
        public static string patternU_Upper = "Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ";
        public static string patternY_Lower = "ỳ|ý|ỵ|ỷ|ỹ";
        public static string patternY_Upper = "Ỳ|Ý|Ỵ|Ỷ|Ỹ";
        public static string patternD_Lower = "đ";
        public static string patternD_Upper = "Đ";

        public enum CacheRule
        {
            KhongCache = 1,
            CacheToanBo = 2,
            CacheTheoDieuKien = 3
        }

        public static IEnumerable<string> ListDevelopmentDomainName = new List<string>(){
            "ceoquang",
            "localhost",
            "127.0.0",
            "192.168.0",
            "192.168.11",
            "local."
        };

        public static IEnumerable<string> ListProductionStageDomainName = new List<string>(){
            "stgapirv01",
            "stgapirv02"
        };

        public static IEnumerable<string> ListProductionLiveDomainName = new List<string>(){
            "prodapirv01",
            "prodapirv02"
        };

        #endregion

        #region "Commons"

        public static string LoadAppSetting()
        {

            string error = "";
            try
            {
                FileData fd = new FileData();

                Console.WriteLine("Variables.AppSettingPathFile: " + Variables.AppSettingPathFile);
                Console.WriteLine("Variables.AppSettingPathFile isExist: " + fd.CheckExistPathFile(Variables.AppSettingPathFile));
                string strAppSetting = CommonMethods.DeserializeObject<string>(fd.GetContentFile(Variables.AppSettingPathFile).Result);
                Console.WriteLine("Variables.AppSettingPathFile strAppSetting: " + strAppSetting);

                // string appSettingPathFile2 = $"app/AppData/AppSetting/appsettings.docker.Production.json";
                // string appSettingPathFile3 = $"/AppData/AppSetting/appsettings.docker.Production.json";
                // style 2 & 3 ở trên không work
                // cách số 4 bên dưới work tương tự /app/AppData/
                // string appSettingPathFile4 = $"AppData/AppSetting/appsettings.docker.Production.json";

                //CommonMethods.WriteLogDocker("Variables.AppSettingPathFile strAppSetting: " + strAppSetting);

                Dictionary<string, object> dicAppSetting = CommonMethods.DeserializeObject<Dictionary<string, object>>(strAppSetting);

                Console.WriteLine("dicAppSetting count: " + dicAppSetting.Count);
                /*
                foreach (var item in dicAppSetting)
                {
                    Console.WriteLine("dicAppSetting key: " + item.Key);
                    Console.WriteLine("dicAppSetting value: " + item.Value);
                }*/

                string strConnectionStrings = CommonMethods.ConvertToString(dicAppSetting["ConnectionStrings"]);
                Dictionary<string, object> dicConnectionStrings = CommonMethods.DeserializeObject<Dictionary<string, object>>(strConnectionStrings);
                Console.WriteLine("strConnectionStrings: " + strConnectionStrings);
                Console.WriteLine("dicConnectionStrings count: " + dicConnectionStrings.Count);

                try
                {
                    Variables.DataPath = dicConnectionStrings.GetValueFromKey_ReturnString("DataPath", Variables.DataPath);
                }
                catch
                { // Do nothing
                }

                if (string.IsNullOrEmpty(Variables.DataPath))
                {
                    Variables.DataPath = Variables.AppPath + "DockerData/";
                }

                Variables.DomainName = dicConnectionStrings.GetValueFromKey_ReturnString("DomainName", Variables.DomainName);
                Variables.DomainNameChinh = dicConnectionStrings.GetValueFromKey_ReturnString("DomainNameChinh", Variables.DomainNameChinh);
                Variables.ConnectionStringPostgresql_RaoVat_Master = dicConnectionStrings.GetValueFromKey_ReturnString("ConnectionStringPostgresql_RaoVat_Master", Variables.ConnectionStringPostgresql_RaoVat_Master);
                Variables.ConnectionStringPostgresql_RaoVat_Slave = dicConnectionStrings.GetValueFromKey_ReturnString("ConnectionStringPostgresql_RaoVat_Slave", Variables.ConnectionStringPostgresql_RaoVat_Slave);
                Variables.ConnectionStringPostgresql_RaoVat_SlaveForTest = dicConnectionStrings.GetValueFromKey_ReturnString("ConnectionStringPostgresql_RaoVat_SlaveForTest", Variables.ConnectionStringPostgresql_RaoVat_SlaveForTest);
                Variables.ConnectionStringPostgresql_RaoVat_History = dicConnectionStrings.GetValueFromKey_ReturnString("ConnectionStringPostgresql_RaoVat_History", Variables.ConnectionStringPostgresql_RaoVat_History);
                Variables.RedisConnectionString = dicConnectionStrings.GetValueFromKey_ReturnString("RedisConnectionString", Variables.RedisConnectionString);
                Variables.RedisPassword = dicConnectionStrings.GetValueFromKey_ReturnString("RedisPassword", Variables.RedisPassword);
                Variables.RedisFullConnectionString = Variables.RedisConnectionString + $",AllowAdmin=true,Password={Variables.RedisPassword}";

                Variables.ApiDaiLyXe = dicConnectionStrings.GetValueFromKey_ReturnString("ApiDaiLyXe", Variables.ApiDaiLyXe);
                Variables.ApiInterfaceDaiLyXe = dicConnectionStrings.GetValueFromKey_ReturnString("ApiInterfaceDaiLyXe", Variables.ApiInterfaceDaiLyXe);
                Variables.WebApiElasticSearch = dicConnectionStrings.GetValueFromKey_ReturnString("WebApiElasticSearch", Variables.WebApiElasticSearch);
                Variables.Constants.EmailSend_name = dicConnectionStrings.GetValueFromKey_ReturnString("EmailSend_name", Variables.Constants.EmailSend_name);
                Variables.Constants.EmailSend_pass = dicConnectionStrings.GetValueFromKey_ReturnString("EmailSend_pass", Variables.Constants.EmailSend_pass);
                //Variables.ConnectionString = Configuration.GetConnectionString("SQLConnection"); // SQLConnection || SQLConnection_Server03
                //Variables.ConnectionString_RaoVat = Configuration.GetConnectionString("SQLConnectionRaoVat");
                //Variables.ConnectionString_DaiLyXeSupport = Configuration.GetConnectionString("ConnectionString_DaiLyXeSupport");
                //Variables.ElasticSearchApi = Configuration.GetConnectionString("ElasticSearchApi");
                CommonFileManager.BackgroundColorImage = dicConnectionStrings.GetValueFromKey_ReturnString("BackgroundColorImage", CommonFileManager.BackgroundColorImage);
                string logFolderPath = dicConnectionStrings.GetValueFromKey_ReturnString("WriteLogFolder", "");
                // nguyencuongcs 20190918:                
                if (Variables.IsBuildDockerImage && string.IsNullOrEmpty(logFolderPath))
                {
                    logFolderPath = "DockerData/Logs/";
                }

                CommonMethods.WriteLogFolderPath = string.IsNullOrEmpty(logFolderPath) ? CommonMethods.WriteLogFolderPath : logFolderPath;
                CommonFileManager.Driver_Domain = dicConnectionStrings.GetValueFromKey_ReturnString("Driver_Domain", CommonFileManager.Driver_Domain);
                Variables.ServerName = dicConnectionStrings.GetValueFromKey_ReturnString("ServerName", Variables.ServerName);
                Variables.DomainApi = dicConnectionStrings.GetValueFromKey_ReturnString("DomainApi", Variables.DomainApi);
                Variables.Postgresql_RaoVat_Slave_IsHealthy = dicConnectionStrings.GetValueFromKey_ReturnBool("Postgresql_RaoVat_Slave_IsHealthy", Variables.Postgresql_RaoVat_Slave_IsHealthy);
                Variables.ApiRaoXe = dicConnectionStrings.GetValueFromKey_ReturnString("ApiRaoXe", Variables.ApiRaoXe);
                Variables.FirebaseSecretKey = dicConnectionStrings.GetValueFromKey_ReturnString("FirebaseSecretKey", Variables.FirebaseSecretKey);
                Variables.KeyFirebaseFileJson = dicConnectionStrings.GetValueFromKey_ReturnString("KeyFirebaseFileJson", Variables.KeyFirebaseFileJson);
                Variables.KeyFirebaseProjectId = dicConnectionStrings.GetValueFromKey_ReturnString("KeyFirebaseProjectId", Variables.KeyFirebaseProjectId);

                if (string.IsNullOrEmpty(Variables.ApiRaoXe))
                {
                    Variables.ApiRaoXe = "https://stgapirv01.dailyxe.com.vn/api/raovat/";
                }

                if (string.IsNullOrEmpty(Variables.FirebaseSecretKey))
                {
                    Variables.FirebaseSecretKey = "key=AAAAANytRRI:APA91bHhoMWnHCUfjPe9Pz6IgcwCZcA2evy8G2xQrNh0DofVU7_THMeueULTBC8q6jlzIcO-WQf-Wp1XBNTVqMjQqiutRwtoOxj6g9_pBT6d-snSgMeSqVnRfzKpsMsVzJZJj8cBZShg";
                }

                //Variables.PartnerCode = dicConnectionStrings.GetValueFromKey_ReturnString("PartnerCode", Variables.PartnerCode);
                //Variables.AccessKey = dicConnectionStrings.GetValueFromKey_ReturnString("AccessKey", Variables.AccessKey);
                //Variables.SerectKey = dicConnectionStrings.GetValueFromKey_ReturnString("SerectKey", Variables.SerectKey);

                // nguyencuongcs 20180712: để khi chạy trên 0.3 sẽ lấy các thông số development
                if (CommonMethods.CheckUseDevelopment())
                {
                    Variables.DomainUseSSL = false;
                    Variables.EnvironmentIsProduction = false;
                }
                else
                {
                    Variables.DomainUseSSL = true;
                    Variables.EnvironmentIsProduction = true;
                    Variables.EnvironmentIsProductionLive = CheckUseProductionLive();
                    Variables.EnvironmentIsProductionStage = CheckUseProductionStage();
                }

                Console.WriteLine("LoadAppSetting done");
                CommonMethods.WriteLog("LoadAppSetting done: ");
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                Console.WriteLine("LoadAppSetting Error: " + error);
                CommonMethods.WriteLog("LoadAppSetting Error: " + error);
            }

            return error;
        }

        public static string WriteLogFolderPath = "Logs/RaoVatApi/";

        public static void WriteLog(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = WriteLogFolderPath;
                }

                Console.WriteLine("Begin WriteLog Dir: " + diskName + ":" + folderPath);
                string text = diskName + folderPath;
                if (!System.IO.Directory.Exists(text))
                {
                    System.IO.Directory.CreateDirectory(text);
                }
                Console.WriteLine("Dir => WriteLog: " + text);
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(text + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt", true);
                streamWriter.WriteLine("------------------------------------------------------------------------------------");
                streamWriter.WriteLine(System.DateTime.Now.ToLongTimeString() + ": " + message);
                streamWriter.WriteLine("------------------------------------------------------------------------------------");
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog(ex.ToString());
            }
        }
        public static void WriteLogMarketingPlan(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}MarketingPlan/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogPerformanceTest(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin WriteLogPerformanceTest:" + folderPath);
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}PerformanceTest/";
                }

                Console.WriteLine($"{CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(DateTime.Now)}-Begin WriteLogPerformanceTest:" + folderPath);
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogCoreTemplate(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}CoreTemplate/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogCoreTemplateError(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}CoreTemplate_Error/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogEntityBaseToDbTest(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}CommitToDb/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogElasticController(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}ElasticController/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogApiGetCore(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}ApiGetCore/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogMemoryCacheController(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}MemoryCacheController/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }
        public static void WriteLogApiDailyXeFarm(string message, string diskName = "", string folderPath = "")
        {
            try
            {
                if (string.IsNullOrEmpty(folderPath))
                {
                    folderPath = $"{WriteLogFolderPath}ApiDailyXeFarm/";
                }
                WriteLog(message, diskName, folderPath);
            }
            catch { }
        }

        public static string GetDomainByUrl(string pUrl)
        {
            Uri myUri = new Uri(pUrl);
            return myUri.Host;
        }

        public static string GetRandomString(int Length)
        {
            string Str = "abcdefghijklmnpqrtuvswzxy123456789";
            string Result = string.Empty;
            Random rand = new Random();
            for (int i = 0; i < Length; i++)
            {
                Result += Str[rand.Next(Str.Length)];
            }
            return Result;
        }

        public static int GetRandomNumber(int minVal = 4, int maxVal = 6)
        {
            int result = 0;

            Random ran = new Random();
            result = ran.Next(minVal, maxVal);

            if (result == 6)
            {
                result = 5;
            }

            return result;
        }

        public static string FormatChuHoaDauTu(string str)
        {
            string strResult = str.NullIsEmpty();
            if (!string.IsNullOrEmpty(str))
            {
                str = str.ToLower();
                char[] charArr = str.ToCharArray();
                charArr[0] = Char.ToUpper(charArr[0]);
                foreach (System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(str, @"(\s\S)"))
                {
                    charArr[m.Index + 1] = m.Value.ToUpper().Trim()[0];
                }
                strResult = new String(charArr);
            }
            return strResult;
        }

        public static string GetRewriteString(string data)
        {
            string result = ConvertUnicodeToASCII(data.NullIsEmpty().ToLower().Trim());

            result = System.Text.RegularExpressions.Regex.Replace(result, "[^-a-z0-9 ]", "");
            result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "-");

            return result;
        }

        public static string GetRewriteStringAllowSplash(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return "";
            }

            string alternate = "-az9889za-";
            string splash = "/";

            string result = data.Replace(splash, alternate);

            result = GetRewriteString(result);

            result = result.Replace(alternate, splash);

            return result;
        }

        public static string GetRewriteTagString(string data)
        {
            string result = data.NullIsEmpty().ToLower().Trim();

            result = System.Text.RegularExpressions.Regex.Replace(result, string.Format("[^a-z0-9{0}{1}{2}{3}{4}{5}{6} ]", patternA_Lower,
                        patternE_Lower, patternI_Lower, patternO_Lower, patternU_Lower, patternY_Lower, patternD_Lower), "");
            result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "+");

            return result;
        }

        public static string RemovePrefixRewriteUrl(string pRewriteUrl)
        {
            string[] arrRewriteUrl = pRewriteUrl.Split('/');
            return arrRewriteUrl[arrRewriteUrl.Length - 1];
        }

        public static string LoaiBoKyTuThua(string text, string schar)
        {
            text = text.NullIsEmpty().Trim();
            text = System.Text.RegularExpressions.Regex.Replace(text, $"[{schar}]+", $"{schar}");
            return text;
        }

        public static string LoaiBoKhoangTrangThua(string text)
        {
            text = text.NullIsEmpty().Trim();
            text = System.Text.RegularExpressions.Regex.Replace(text, @"[ ]+", " ");
            return text;
        }

        public static string LoaiBoKyTuODau(string text, string kyTu)
        {
            while (text.StartsWith(kyTu))
            {
                text = text.Substring(1, text.Length - 1);
            }

            return text;
        }

        public static string LoaiBoKyTuOCuoi(string text, string kyTu)
        {
            int length = 0;
            while (text.EndsWith(kyTu))
            {
                length = text.Length;
                text = text.Substring(0, length - 1);
            }

            return text;
        }

        public static string LoaiBoKyTuODauVaCuoi(string text, string kyTu)
        {
            text = LoaiBoKyTuODau(text, kyTu);
            text = LoaiBoKyTuOCuoi(text, kyTu);
            return text;
        }

        public static string ThayTheKhoangTrangThua(string text, string replaceString = "-")
        {
            text = text.Trim();
            text = System.Text.RegularExpressions.Regex.Replace(text.NullIsEmpty(), @"[ ]{1,}", replaceString);
            return text;
        }

        public static string LoaiBoKyTuDienThoai(string text)
        {
            return System.Text.RegularExpressions.Regex.Replace(text.NullIsEmpty(), "[^0-9]+", string.Empty);
        }

        public static string GetPhoneByString(string pValue)
        {
            string[] arrValues = pValue.NullIsEmpty().Split('-');
            if (arrValues.Length > 0)
            {
                return LoaiBoKyTuDienThoai(arrValues[0]);
            }
            return LoaiBoKyTuDienThoai(pValue);
        }

        public static string CropText(string value, int length = 200)
        {
            if (value == null || value.Length < length || value.IndexOf(" ", length) == -1)
                return value;

            return value.Substring(0, value.IndexOf(" ", length));
        }

        public static string FilterHtmlTag(string pValue)
        {
            return pValue.NullIsEmpty().Replace("<", "&lt;").Replace(">", "&gt;");
        }

        public static string FilterHtmlTagMultiLanguage(string pValue)
        {
            pValue = FilterSpecialCharErrorXml(pValue.NullIsEmpty());
            return FilterHtmlTag(pValue);
        }

        public static string FilterSpecialCharErrorXml(string pValue)
        {
            // & -> &amp;
            // < -> &lt;
            // > -> &gt;
            // " -> &quot;
            // ' -> &apos;
            string goodAmpersand = "&amp;";
            Regex badAmpersand = new Regex("&(?![a-zA-Z]{2,6};|#[0-9]{2,4};)");
            return badAmpersand.Replace(pValue, goodAmpersand);
        }

        public static string FilterNumber(string text)
        {
            return System.Text.RegularExpressions.Regex.Replace(text, "[^0-9]+", string.Empty);
        }

        public static string FilterTitleTag(string pValue)
        {
            return pValue.Replace("\"", "");
        }

        public static string ConvertObsceneToASCII(string text, string regexObscene = "")
        {
            ProcessLanguage pl = new ProcessLanguage();
            return pl.ConvertObsceneToASCII(text, regexObscene);
        }

        // nguyencuongcs 20180328
        public static string FilterSpecialCharForFilename(string value)
        {
            string res = ConvertUnicodeToASCII(value);

            // nguyencuongcs 20180827: bổ sung để remove chữ có dấu
            //res = FilterSpecialCharForElasticSearchQuery(value);

            // thay khoảng trắng = -
            res = LoaiBoKhoangTrangThua(res);
            res = ThayTheKhoangTrangThua(res);
            res = LoaiBoKyTuThua(res, "-");
            res = LoaiBoKyTuThua(res, ".");

            Regex badAmpersand = new Regex("[^/-/._A-Za-z0-9]"); //[ ]+|[^/._A-Za-z0-9]+
            res = badAmpersand.Replace(res, "");

            Regex extensionReg = new Regex(string.Join("|", Variables.ConfigValidExtension));
            res = extensionReg.Replace(res, "");
            return res;
        }

        public static string FilterSpecialCharForElasticSearchQuery(string value)
        {
            string res = "";

            Regex badAmpersand = new Regex(string.Format("[^ A-Za-z0-9{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}]",
                patternA_Lower, patternE_Lower, patternI_Lower, patternO_Lower, patternU_Lower, patternY_Lower, patternD_Lower,
                patternA_Upper, patternE_Upper, patternI_Upper, patternO_Upper, patternU_Upper, patternY_Upper, patternD_Upper));

            // result = System.Text.RegularExpressions.Regex.Replace(result, "[ ]{1,}", "+"); Thay khoảng trắng = dấu +

            res = badAmpersand.Replace(value, "");
            return res;
        }

        public static string ConvertUnicodeToASCII(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                text = "";
            }
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.ToLower().Normalize(System.Text.NormalizationForm.FormD);
            string res = regex.Replace(strFormD, String.Empty).Replace('đ', 'd');
            return res;
        }

        public static Byte ConvertToByte(object pNumber)
        {
            try
            {
                return Convert.ToByte(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static Byte? ConvertToByte(object pNumber, Byte? defaultValue)
        {
            try
            {
                return (Byte?)Convert.ToByte(pNumber);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static short ConvertToInt16(object pNumber)
        {
            try
            {
                return Convert.ToInt16(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static int ConvertToInt32(object pNumber)
        {
            try
            {
                return Convert.ToInt32(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static int? ConvertToInt32(object pNumber, int? defaultValue)
        {
            try
            {
                return Convert.ToInt32(pNumber);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static long ConvertToInt64(object pNumber)
        {
            try
            {
                return Convert.ToInt64(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static long? ConvertToInt64(object pNumber, long? defaultValue)
        {
            try
            {
                return Convert.ToInt64(pNumber);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static decimal ConvertToDecimal(object pNumber)
        {
            try
            {
                return Convert.ToDecimal(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static decimal? ConvertToDecimal(object pNumber, decimal? defaultValue)
        {
            try
            {
                return Convert.ToDecimal(pNumber);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static double ConvertToDouble(object pNumber)
        {
            try
            {
                return Convert.ToDouble(pNumber);
            }
            catch
            {
                return 0;
            }
        }

        public static string ConvertToString(object pString)
        {
            try
            {
                return Convert.ToString(pString);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string ConvertToStringDefaultNull(object pString)
        {
            try
            {
                return Convert.ToString(pString);
            }
            catch
            {
                return null;
            }
        }

        public static DateTime ConvertToDateTime_Full(string pDate)
        {
            return DateTime.ParseExact(pDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public static DateTime ConvertToDateTime(string pDate)
        {
            return ConvertToDateTime(pDate, "dd-MM-yyyy");
        }

        public static ArrayList ConvertToArrayList(object pObject)
        {
            try
            {
                return (ArrayList)pObject;
            }
            catch
            {
                return null;
            }
        }

        public static DateTime ConvertToDateTime(string pDate, string pFormat)
        {
            try
            {
                return DateTime.ParseExact(pDate, pFormat, null);
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static DateTime ParseToDateTime(string pDate)
        {
            try
            {
                return DateTime.Parse(pDate);
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public static DateTime ToDateTime(object pDate)
        {
            try
            {
                return Convert.ToDateTime(pDate);
            }
            catch
            {
                // nguyencuongcs 20181022: exception thường là do format trả về dạng ddMMyyyy -> sai culture mặc định là MMddyyyy                
                var format = "dd/MM/yyyy h:mm:ss tt"; // "dd/MM/yyyy"; // your datetime format            
                try
                {
                    DateTime newDataFormat = ConvertToDateTime(pDate.ToString(), format);
                    return newDataFormat;
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    return DateTime.MinValue;
                }
            }
        }

        public static DateTime? ToDateTime(object pDate, DateTime? defaultValue)
        {
            try
            {
                return Convert.ToDateTime(pDate);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static DateTime? CheckMinSqlDateTime(PropertyInfo propInfo, object propValue)
        {
            DateTime? propValueAsDateTime = ToDateTime(propValue);

            if (propValueAsDateTime <= CommonMethods.MinSqlDateTimeValue)
            {
                propValueAsDateTime = null;
            }

            return propValueAsDateTime;
        }

        public static bool ConvertToBoolean(object pBoolean)
        {
            try
            {
                string booleanString = ConvertToString(pBoolean);
                if (ConvertToString(pBoolean) == "1")
                {
                    booleanString = "true";
                }
                else if (ConvertToString(pBoolean) == "0")
                {
                    booleanString = "false";
                }
                return Convert.ToBoolean(booleanString);
            }
            catch
            {
                return false;
            }
        }

        public static bool? ConvertToBoolean(object pBoolean, bool? defaultValue)
        {
            try
            {
                string booleanString = ConvertToString(pBoolean);
                if (ConvertToString(pBoolean) == "1")
                {
                    booleanString = "true";
                }
                else if (ConvertToString(pBoolean) == "0")
                {
                    booleanString = "false";
                }
                return Convert.ToBoolean(booleanString);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string FormatDateTime(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.DD_MM_YYYY);
        }

        public static string FormatDateTimeMMDDYYYY(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.MM_DD_YYYY);
        }

        public static string FormatDateTimeMMDDYYYY_FULL(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.MM_DD_YYYY_FULL);
        }

        public static string FormatDateTime_DD_MM_YYYY_FULL_SLASH(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.DD_MM_YYYY_SLASH);
        }

        public static string FormatDateTime_DD_MM_YYYY_FULL(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.DD_MM_YYYY_FULL);
        }

        public static string FormatDateTimeDDMMYYYY(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.DDMMYYYY);
        }

        public static string FormatDateTimeDD_MM_YYYY(DateTime pDateTime)
        {
            return FormatDateTime(pDateTime, Variables.DD_MM_YYYY);
        }

        public static string FormatDateTime(DateTime pDateTime, string pFormat)
        {
            try
            {
                return pDateTime.ToString(pFormat);
            }
            catch
            {
                return "";
            }
        }

        public static string ConvertTimeRangeToString(DateTime pDatetime, DbContext context) // nguyencuongcs-20170927: context phải truyền từ nơi gọi vào & != null
        {
            DateTime currentServerDatetime = CommonMethods.GetServerDate(context);
            string res = string.Empty;
            if (pDatetime >= currentServerDatetime)
            {
                res = "0 phút";
                return res;
            }

            try
            {
                TimeSpan datediff = currentServerDatetime.Subtract(pDatetime);
                if (datediff != null)
                {
                    double temp = datediff.TotalMinutes;
                    res = Math.Round(temp, 0) + " phút";
                    if (temp >= 60) // 60 mins
                    {
                        temp = datediff.TotalHours;
                        res = Math.Round(temp, 0) + " giờ";

                        if (temp >= 24) // 24 hours
                        {
                            temp = datediff.TotalDays;
                            res = Math.Round(temp, 0) + " ngày";
                            if (temp >= 30) // 30 days
                            {
                                temp = (float)temp / float.Parse("30");
                                res = Math.Round(temp, 0) + " tháng";
                                if (temp >= 12)
                                {
                                    temp = (float)temp / float.Parse("12");
                                    res = Math.Round(temp, 0) + " năm";
                                }
                            }
                        }
                    }
                }

                //res = string.IsNullOrEmpty(res) ? res : res + " | " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(pDatetime) + " | " + CommonMethods.FormatDateTime_DD_MM_YYYY_FULL(currentServerDatetime);
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("ConvertTimeRangeToString: " + ex.ToString());
            }

            return res;
        }

        public static string FormatNumber(decimal pNumber)
        {
            return (pNumber != 0) ? pNumber.ToString("###,###") : "0";
        }

        public static string ConvertToCurrency(string pNumber)
        {
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
            return string.IsNullOrEmpty(pNumber) ? "0" : double.Parse(pNumber).ToString("#,###", cul.NumberFormat);
        }

        public static string FormatNumber(decimal pNumber, string pSplitChar)
        {
            return (pNumber != 0) ? pNumber.ToString("###" + pSplitChar + "###") : "0";
        }

        public static string FormatNumber(int pNumber, int pLengthString)
        {
            return pNumber.ToString().PadLeft(pLengthString, '0');
        }

        //public static void WriteLog(string message, string diskName = "C")
        //{
        //    try
        //    {
        //        string text = diskName + ":/Logs/CMSBigNet/";
        //        if (!System.IO.Directory.Exists(text))
        //        {
        //            System.IO.Directory.CreateDirectory(text);
        //        }
        //        System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(text + System.DateTime.Now.ToString("dd-MM-yyyy") + ".txt", true);
        //        streamWriter.WriteLine("------------------------------------------------------------------------------------");
        //        streamWriter.WriteLine(message);
        //        streamWriter.WriteLine("------------------------------------------------------------------------------------");
        //        streamWriter.Close();
        //    }
        //    catch { }
        //}

        public static string FilterTen(string pString)
        {
            pString = FilterHtmlTagMultiLanguage(pString);
            pString = LoaiBoKhoangTrangThua(pString);
            return pString;
        }

        public static string FormatMoTaNgan(string pString)
        {
            pString = pString.Replace("[b]", "<strong>").Replace("[/b]", "</strong>");
            return FormatHtmlEndLine(pString);
        }

        public static string FormatHtmlEndLine(string pString)
        {
            pString = pString.Replace(Environment.NewLine, "<br/>");
            pString = pString.Replace("\r\n", "<br/>");
            return pString;
        }

        public static string RemoveCharactersMoTaNgan(string pString)
        {
            pString = pString.Replace("[b]", "").Replace("[/b]", "");
            pString = pString.Replace(Environment.NewLine, ", ");
            pString = pString.Replace("\r\n", ", ");
            return pString;
        }

        public static string ModifyLengthString(string Content, int MaxLeng)
        {
            if (Content.Length <= MaxLeng)
                return Content;
            string Temp = Content.Substring(0, MaxLeng);
            int spaceindex = Temp.LastIndexOf(' ');
            return Temp.Substring(0, spaceindex) + "...";
        }

        public static string RemoveAllHTMLTag(string HtmlContent)
        {
            HtmlContent = WebUtility.HtmlDecode(HtmlContent);
            string res = System.Text.RegularExpressions.Regex.Replace(HtmlContent, "<([^>]*)>", string.Empty);
            res = System.Text.RegularExpressions.Regex.Replace(res, "([ ]{2,})", " ");
            res = res.Replace(Environment.NewLine, string.Empty);
            res = res.Replace("\r\n", string.Empty);
            return res;
        }

        public static void DownloadFileByUrl(string pUrl, string pPathFile)
        {
            try
            {
                // WebClient webClient = new WebClient();
                // webClient.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36");
                // webClient.Headers.Add(HttpRequestHeader.Accept, "ext/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                // webClient.Proxy = System.Net.IWebProxy.GetDefaultProxy();
                // webClient.DownloadFile(pUrl, pPathFile);
            }
            catch { }
        }

        public static Dictionary<String, String> ParseDomainList(string pDomainList)
        {
            Dictionary<String, String> lstResult = new Dictionary<String, String>();
            string[] lstDomain = pDomainList.Split('@');
            string[] lstTemp;
            foreach (string domain in lstDomain)
            {
                lstTemp = domain.Split('|');
                if (lstTemp.Length > 1)
                {
                    lstResult.Add(lstTemp[0].Trim().ToLower(), lstTemp[1].Trim().ToLower());
                }
            }
            return lstResult;
        }

        // public static string GetServerDateAsString(string pFormat = Variables.DD_MM_YYYY)
        // {
        //     CoreBigNet.CoreTemplate coreDal = new CoreBigNet.CoreTemplate();
        //     return CommonMethods.FormatDateTime(CommonMethods.ToDateTime(coreDal.GetDate()), pFormat);
        // }

        // public static DateTime GetServerDate()
        // {
        //     CoreBigNet.CoreTemplate coreDal = new CoreBigNet.CoreTemplate();
        //     return CommonMethods.ToDateTime(coreDal.GetDate());
        // }
        public static bool CheckUseSSL()
        {
            bool res = Variables.DomainUseSSL && !CheckUseDevelopment();
            return res;
        }

        public static bool CheckUseDevelopment()
        {
            bool res = false;

            for (int i = 0; i < ListDevelopmentDomainName.Count(); i++)
            {
                res = Variables.DomainName.StartsWith(ListDevelopmentDomainName.ElementAt(i));
                if (res)
                {
                    return true;
                }
            }

            return res;
        }

        public static bool CheckUseProductionStage()
        {
            bool res = false;

            for (int i = 0; i < ListProductionStageDomainName.Count(); i++)
            {
                res = Variables.DomainName.StartsWith(ListProductionStageDomainName.ElementAt(i));
                if (res)
                {
                    return true;
                }
            }

            return res;
        }

        public static bool CheckUseProductionLive()
        {
            bool res = false;

            for (int i = 0; i < ListProductionLiveDomainName.Count(); i++)
            {
                res = Variables.DomainName.StartsWith(ListProductionLiveDomainName.ElementAt(i));
                if (res)
                {
                    return true;
                }
            }

            return res;
        }

        // public static int GetIDAdmin()
        // {
        //     return CommonMethods.ConvertToInt32(HttpContext.Current.Session[Variables.SADMIN_ID]);
        // }
        // public static bool GetIsRootAdmin()
        // {
        //     return CommonMethods.ConvertToBoolean(HttpContext.Current.Session[Variables.SADMIN_ISROOT]);
        // }
        public static string FormatNguoiVietWithMark(string pNguoiViet)
        {
            if (string.IsNullOrEmpty(pNguoiViet))
            {
                return string.Empty;
            }
            return /*"@" +*/ pNguoiViet;
        }
        // public static string BuildLinkDetailPostFacebook(string pIDPost)
        // {
        //     string res = @"https://www.facebook.com/" + Variables.FanpageName + @"/" + Variables.FanpagePostsFolder + @"/";
        //     try
        //     { 
        //         if (string.IsNullOrEmpty(pIDPost))
        //         {
        //             return res;
        //         }
        //         else
        //         {
        //             if (pIDPost.Contains("_"))
        //             {
        //                 pIDPost = pIDPost.Split('_')[1];
        //             }
        //         }
        //     }
        //     catch(Exception ex)
        //     {
        //         CommonMethods.WriteLog("BuildLinkDetailPostFacebook: " + ex.ToString());
        //     }
        //     return res + pIDPost;
        // }

        public static Byte[] GetFileBytes(string pathFile)
        {
            Byte[] res = null;
            try
            {
                pathFile = CorrectPathData(pathFile);
                res = System.IO.File.ReadAllBytes(pathFile);
            }
            catch
            {
                res = System.IO.File.ReadAllBytes(Variables.AppPath + CommonFileManager.NoImageAvailable_0x0);

            }
            return res;
        }

        public static string CorrectPathData(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return "";
            }

            // nguyencuongcs 20181003: tạm dùng đoạn điều kiện dưới để trỏ các link chạy vào AppData theo đường dẫn cũ (nằm trong thư mục publish) về đường dẫn mới
            // pathFile.Contains("AppData/")     
            if (!path.StartsWith(Variables.AppPath) && !path.StartsWith(Variables.DataPath))
            {
                path = Variables.DataPath + path;
            }

            return path;
        }

        public static async Task Download(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (
                Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, false))
            {
                contentStream.CopyTo(stream);
            }
        }

        public static async Task<int> DownloadAsync(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            using (
                Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, true))
            {
                await contentStream.CopyToAsync(stream);
                return CommonMethods.ConvertToInt32(stream.Length);
            }
        }

        public static async Task DownloadGZipAsync(string requestUri, string pathFile)
        {
            int bufferSize = 100 * 1024; // 10Mb

            using (
                Stream stream = new FileStream(pathFile, FileMode.Create, FileAccess.Write, FileShare.None, bufferSize, true))
            {
                Byte[] b = System.Text.Encoding.ASCII.GetBytes(GetContentWebsite_GZip(requestUri).Result);
                Stream contentStream = new MemoryStream(b);
                await contentStream.CopyToAsync(stream);
            }
        }

        public static async Task<string> GetContentWebsite_GZip(string link)
        {
            string content = string.Empty;
            try
            {
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
                //request.AutomaticDecompression = DecompressionMethods.GZip;

                //// Right now... this is what our HTTP Request has been built in to...
                ///*
                //    GET http://google.com/ HTTP/1.1
                //    Host: google.com
                //    Accept-Encoding: gzip
                //    Connection: Keep-Alive
                //*/

                //// Wrap everything that can be disposed in using blocks... 
                //// They dispose of objects and prevent them from lying around in memory...
                //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())  // Go query google
                //using (System.IO.Stream responseStream = response.GetResponseStream())               // Load the response stream
                //using (System.IO.StreamReader streamReader = new System.IO.StreamReader(responseStream))       // Load the stream reader to read the response
                //{
                //    content = streamReader.ReadToEnd(); // Read the entire response and store it in the siteContent variable
                //}

                HttpClientHandler handler = new HttpClientHandler();
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (var httpClient = new HttpClient(handler))
                {
                    var apiUrl = (link);

                    //setup HttpClient
                    httpClient.BaseAddress = new Uri(apiUrl);
                    //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //httpClient.GetStreamAsync

                    //make request
                    return await httpClient.GetStringAsync(apiUrl);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void WriteStream(Stream stream)
        {
            if (stream != null)
            {
                int bufferSize = 100 * 1024; // 10Mb
                byte[] buffer = new byte[bufferSize];
                int readed = 0;

                using (stream)
                {
                    do
                    {
                        readed = stream.Read(buffer, 0, bufferSize);
                        stream.WriteAsync(buffer, 0, readed);
                    }
                    while (readed > 0);
                }
            }
        }

        // public static bool CheckIsRoot(string pTenDangNhap, string pNgayTao, string pIsRootString)
        // {
        //     bool res = false;
        //     res = pIsRootString == BuildIsRoot(pTenDangNhap, pNgayTao);
        //     return res;      
        // }
        // public static string BuildIsRoot(string pTenDangNhap, string pNgayTao)
        // {
        //     return GetEncryptMD5(pTenDangNhap + pNgayTao);
        // }

        public static List<int> SplitStringToInt(string data, bool returnNullWhenDataIsMinus1, string splitChar = Variables.COMMON_KEY_SPLIT)
        {
            List<int> lstId = null;

            if (string.IsNullOrEmpty(data) || data.Length < 1)
            {
                return lstId;
            }

            try
            {
                lstId = new List<int>();

                string[] arrData = data.Split(splitChar);
                int temp = -1;

                foreach (string val in arrData)
                {
                    if (Int32.TryParse(val, out temp))
                    {
                        lstId.Add(temp);
                    }
                }

                if (returnNullWhenDataIsMinus1 && lstId.Count == 1 && (lstId[0] == -1 || lstId[0] == 0))
                {
                    lstId = null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstId;
        }

        public static List<long> SplitStringToInt64(string data, char splitChar = ',')
        {
            List<long> lstId = new List<long>();

            if (string.IsNullOrEmpty(data) || data.Length < 1)
            {
                return lstId;
            }

            try
            {
                string[] arrData = data.Split(splitChar);
                long temp = -1;

                foreach (string val in arrData)
                {
                    if (Int64.TryParse(val, out temp))
                    {
                        lstId.Add(temp);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstId;
        }

        public static void SetPropertyValue(object obj, string propertyName, object val)
        {
            try
            {
                if (obj != null)
                {
                    Type myType = obj.GetType();
                    if (myType != null)
                    {
                        PropertyInfo prop = null;
                        try
                        {
                            prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                        }
                        catch
                        {
                            prop = myType.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance); // Xử lý trường hợp IDAdmin_DangKy => IdaminDangKy. Add Extend IdAdminDangKy để chuẩn hóa property, khi get propinfo dạng ignorecase sẽ bị trùng
                        }

                        if (prop != null)
                        {
                            prop.SetValue(obj, val);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string exString = ex.ToString();
                // throw ex;
            }
        }

        public static object GetPropertyValue(object obj, string propertyName)
        {
            object res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    PropertyInfo prop = null;
                    try
                    {
                        prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                        //Console.WriteLine("Property: " + prop.Name);
                        //foreach (CustomAttributeData att in prop.CustomAttributes)
                        //{
                        //    Console.WriteLine("\tAttribute: " + att.AttributeType.Name);
                        //    foreach (CustomAttributeTypedArgument arg in att.ConstructorArguments)
                        //    {
                        //        Console.WriteLine("\t\t" + arg.ArgumentType.Name + ": " + arg.Value); arg.Value = JsonPropertyName
                        //    }
                        //}
                    }
                    catch
                    {
                        prop = myType.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance); // Xử lý trường hợp IDAdmin_DangKy => IdaminDangKy. Add Extend IdAdminDangKy để chuẩn hóa property, khi get propinfo dạng ignorecase sẽ bị trùng
                    }

                    if (prop != null)
                    {
                        res = prop.GetValue(obj, null);
                    }
                }
            }
            return res;
        }

        public static PropertyInfo GetPropertyInfo(object obj, string propertyName)
        {
            PropertyInfo res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    res = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                }
            }
            return res;
        }

        public static IEnumerable<string> GetPropertyNameList(object obj)
        {
            IEnumerable<string> res = null;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    res = GetPropertyNameList(myType);
                }
            }
            return res;
        }

        public static IEnumerable<string> GetPropertyNameList(Type type)
        {
            List<string> res = null;

            if (type != null)
            {
                PropertyInfo[] arrPropInfo = type.GetProperties();

                if (arrPropInfo != null && arrPropInfo.Length > 0)
                {
                    res = new List<string>();
                    foreach (PropertyInfo propInfo in arrPropInfo)
                    {
                        res.Add(propInfo.Name);
                    }
                }
            }

            return res;
        }

        public static bool HasProperty(object obj, string propertyName)
        {
            bool res = false;
            if (obj != null)
            {
                Type myType = obj.GetType();
                if (myType != null)
                {
                    PropertyInfo prop = myType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    return prop != null;
                }
            }
            return res;
        }

        public static string SerializeObject(object jsonData, IsoDateTimeConverter formatDatetime = null)
        {
            try
            {
                if (formatDatetime == null)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(jsonData, formatDatetime);
                }
            }
            catch
            {
                return CommonMethods.ConvertToString(jsonData);
            }
        }

        public static T DeserializeObject<T>(object objDataAsJson)
        {
            string jsonString = "";

            try
            {
                jsonString = CommonMethods.ConvertToString(objDataAsJson);
                jsonString = jsonString.Replace(" CH\"", "PM\"");
                jsonString = jsonString.Replace(" SA\"", "AM\"");
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch
            {
                try
                {
                    // Serialized lại trước khi Deserialized
                    CultureInfo culture = new CultureInfo("en-US");

                    var format = "dd/MM/yyyy h:mm:ss tt"; // "dd/MM/yyyy"; // your datetime format
                    var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format, Culture = culture };
                    jsonString = CommonMethods.SerializeObject(objDataAsJson, dateTimeConverter);
                    jsonString = jsonString.Replace(" CH\"", "PM\"");
                    jsonString = jsonString.Replace(" SA\"", "AM\"");
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
                }
                catch
                {
                    try
                    {
                        // Serialized lại trước khi Deserialized
                        CultureInfo culture = new CultureInfo("en-US");

                        var format = "dd/MM/yyyy h:mm:ss tt"; // "dd/MM/yyyy"; // your datetime format
                        var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format, Culture = culture };
                        // Serialized thêm 1 lần nữa
                        jsonString = CommonMethods.SerializeObject(objDataAsJson, dateTimeConverter);
                        jsonString = jsonString.Replace(" CH\"", "PM\"");
                        jsonString = jsonString.Replace(" SA\"", "AM\"");
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
                    }
                    catch (Exception ex)
                    {
                        CommonMethods.WriteLog("DeserializeObject error: " + ex.ToString());
                        CommonMethods.WriteLog("DeserializeObject jsonData: " + jsonString);
                        return (T)Activator.CreateInstance<T>();
                    }
                }
            }
        }

        public static object DeserializeObjectAsObject<T>(object jsonData)
        {
            try
            {
                return (object)Newtonsoft.Json.JsonConvert.DeserializeObject<T>(CommonMethods.ConvertToString(jsonData));
            }
            catch
            {
                return null;
            }
        }

        public static T ExChangeType<T>(object value)
        {
            Type t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)value;
            // (value == null || DBNull.Value.Equals(value)) ?
            //    default(T) : (T)Convert.ChangeType(value, t);
        }

        public static Expression DefaultExpression(bool defaultTrue)
        {
            return Expression.Equal(
                Expression.Constant(defaultTrue ? 1 : 2),
                Expression.Constant(1)
            );
        }

        public static DateTime GetServerDate(DbContext context)
        {
            // nguyencuongcs 20180528: vì api đặt chung với DB nên chỉ cần lấy Datetime.Now, tránh gọi xuống DB nhiều lần có khả năng nghẽn khi client (UI) gọi
            return DateTime.Now;

            /*

            RawSQLRepository repo = new RawSQLRepository(context);
            string sql = @"SELECT sqlDate = GETDATE()";

            // Cách 1: get member case insensitive
            DynamicDictionary res = repo.ExecuteSqlQuery(sql).Result.FirstOrDefault();

            DateTime date = res != null ? (CommonMethods.ToDateTime(res["sqldate"])) : DateTime.MinValue;

            // Cách 2: get value from dic by key (case sensitive) / nguyencuongcs update 20170927: fixed to case insensitive in ExecuteSqlQuery_ReturnDic
            //Dictionary<string, object> res = repo.ExecuteSqlQuery_ReturnDic(sql).FirstOrNewObject();
            //DateTime date = res != null ? (CommonMethods.ToDateTime(res["sqlDate"])) : DateTime.MinValue;

            return date;
            */
        }

        public static List<string> GetIncludePropertyFromOrderString(string orderString)
        {
            if (string.IsNullOrEmpty(orderString))
            {
                return null;
            }

            List<string> res = new List<string>();
            string[] splitCriteria = orderString.Split(',');
            if (splitCriteria != null)
            {
                string[] splitOrderExpression;
                string[] splitEntityAndProperty;
                for (int i = 0; i < splitCriteria.Length; i++)
                {
                    splitOrderExpression = null;
                    splitEntityAndProperty = null;
                    if (!string.IsNullOrEmpty(splitCriteria[i]))
                    {
                        splitOrderExpression = splitCriteria[i].Split(':');
                        if (splitOrderExpression != null && splitOrderExpression.Length > 0)
                        {
                            splitEntityAndProperty = splitOrderExpression[0].Split('.');
                            if (splitEntityAndProperty != null && splitEntityAndProperty.Length > 1 && !res.Contains(splitEntityAndProperty[0])) // splitEntityAndProperty.Length > 1 để chắc chắn là định dạng entity.Property
                            {
                                res.Add(splitEntityAndProperty[0]);
                            }
                        }
                    }
                }
            }

            return res;
        }

        public static string BuildTuKhoaTimKiem(string splitChar = ";", params string[] buildStringArray)
        {
            string res = string.Empty;

            if (buildStringArray != null)
            {
                foreach (var value in buildStringArray)
                {
                    res += string.IsNullOrEmpty(res) ? "" : splitChar;
                    res += CommonMethods.ConvertUnicodeToASCII(value.NullIsEmpty());
                }
            }

            return res;
        }

        public static string CoalesceParams(string coaChar, InterfaceParam interfaceParam)
        {
            string JoinParam = string.Empty;

            Type myType = interfaceParam.GetType();

            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());

            foreach (var item in props)
            {
                if (!item.Name.Contains("token") || !item.Name.Contains("deviceId") || !item.Name.Contains("daXacThucCmnd")
                    || !item.Name.Contains("daXacThucPassport") || !item.Name.Contains("listHinhAnhJsonPassport") || !item.Name.Contains("ListDeletedIdThanhVien")
                    || !item.Name.Contains("childrenStructure") || !item.Name.Contains("childrenStructureBool") || !item.Name.Contains("daXacThucPassport")
                    || !item.Name.Contains("rate") || !item.Name.Contains("point") || !item.Name.Contains("pointTmp"))
                {
                    var values = interfaceParam.GetValue(item.Name);

                    if (values != null)
                    {
                        if (values is IList && values.GetType().IsGenericType)
                        {
                            JoinParam += item.Name + "=" + "(";
                            foreach (var value in values as IEnumerable)
                            {
                                JoinParam += value + ",";
                            }
                            JoinParam += ");";
                        }
                        else
                        {
                            JoinParam += item.Name + "=" + values + ";";
                        }
                    }
                }
            }

            return Variables.ApiRaoXe + ";" + JoinParam;
        }

        public static string CoalesceParams(string coaChar, params object[] paramsData)
        {
            if (paramsData == null || paramsData.Count() < 1)
            {
                return "";
            }
            else
            {
                return string.Join(coaChar, paramsData);
            }
        }

        public static string CoalesceParamsWithoutDuplicateCoachar(string coaChar, params string[] paramsData)
        {
            if (paramsData == null || paramsData.Count() < 1)
            {
                return "";
            }
            else
            {
                List<string> newParams = new List<string>();
                if (paramsData != null && paramsData.Count() > 0)
                {
                    int length = 0;
                    string newItem = "";
                    foreach (string item in paramsData)
                    {
                        if (string.IsNullOrEmpty(item))
                        {
                            continue;
                        }
                        // Bỏ ký tự / ở đầu nếu có        
                        length = item.Length;
                        newItem = !item.StartsWith("/") ? item : item.Substring(1, length - 1);

                        // Bỏ ký tự / ở cuối nếu có
                        length = newItem.Length;
                        newItem = !newItem.EndsWith("/") ? newItem : newItem.Substring(0, length - 1);
                        if (!string.IsNullOrEmpty(newItem))
                        {
                            newParams.Add(newItem);
                        }
                    }
                }
                return string.Join(coaChar, newParams);
            }
        }

        static readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public static string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        public static bool CheckListIntIsAllowSearch(List<int> lstIntToCheck, bool checkAllowWhenMinusOne = false)
        {
            bool res = false;

            if (lstIntToCheck == null || lstIntToCheck.Count < 1)
            {
                res = true;
            }
            else if (checkAllowWhenMinusOne)
            {
                if (lstIntToCheck.Count == 1 && lstIntToCheck[0] == -1) // Trường hợp client truyền -1 lên. Ví dụ: trangThai = -1 => không lọc TrangThai
                {
                    res = true;
                }
            }

            return res;
        }

        public static bool CheckConnectToRedisServer(bool sentMailIfFailed = true)
        {
            bool connected = false;

            try
            {
                if (Variables.RedisOn)
                {
                    // Test redis connection
                    using (var _redis = StackExchange.Redis.ConnectionMultiplexer.Connect(Variables.RedisFullConnectionString))
                    {
                        var server = _redis.GetServer(Variables.RedisConnectionString); // nguyencuongcs 20190909: ko được gắn RedisFullConnectionString vào đây, vì sẽ ko connect được
                    }

                    // services.AddDistributedRedisCache(
                    //options =>
                    //{
                    //    options.Configuration = Variables.RedisConnectionString + ",AllowAdmin=true,Password=R@ei2D018@nhangVN"; // nguyencuongcs 20180326: hiện tại cấu hình local, nếu sau này dựng webfarm (distributed web server) sẽ cấu hình lại chỗ này sau.                                      
                    //    options.InstanceName = Variables.RedisServerName;
                    //});
                    Variables.CurrentCache = "RedisCache";
                    connected = true;
                }
            }
            catch (Exception ex)
            {
                //SendMail sd = new SendMail();
                GmailApiCore ga = new GmailApiCore();
                WriteLogMemoryCacheController($"CheckConnectToRedisServer {Variables.RedisFullConnectionString}: " + ex.ToString());
                if (sentMailIfFailed)
                {
                    ga.PostThongBaoHeThong_KetNoiRedisServer(ex.ToString());
                    //sd.SendMailThongBaoHeThong_KetNoiRedisServer(ex.ToString());
                }
            }

            return connected;
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static JObject ConvertToJObject(string json)
        {
            try
            {
                return JObject.Parse(json.ToString());
            }
            catch
            {
                return new JObject();
            }
        }

        public static string SmartSubstr(string str, int len)
        {
            var temp = CropText(str, len);

            if (temp.LastIndexOf('<') > temp.LastIndexOf('>'))
            {
                temp = str.Substring(0, temp.LastIndexOf('<'));
            }
            return temp;
        }

        public static bool IsValidJson(string strInput)
        {
            CommonMethods.WriteLog("IsValidJson");
            CommonMethods.WriteLog(strInput);
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    CommonMethods.WriteLog("IsValidJson: true");
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    CommonMethods.WriteLog("IsValidJson: false");
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("JSON ERROR", Variables.UrlFromClient + strInput + "-" + jex.Message, email);
                    }
                    CommonMethods.WriteLog("JSON ERROR:"+jex.Message);
                    CommonMethods.WriteLog(strInput);
                    return false;
                }
                catch (Exception ex)
                {
                    CommonMethods.WriteLog("IsValidJson: false");
                    MailService ms = new MailService();
                    foreach (var email in Variables.EmailSupportError_Name)
                    {
                        ms.SendEmailToDev("JSON ERROR", Variables.UrlFromClient + strInput + "-" + ex.Message, email);
                    }
                    CommonMethods.WriteLog("OTHER ERROR:" + ex.Message);
                    CommonMethods.WriteLog(strInput);
                    return false;
                }
            }
            else
            {
                CommonMethods.WriteLog("IsValidJson");
                CommonMethods.WriteLog(strInput);
                CommonMethods.WriteLog("IsValidJson: false");
                return false;
            }
        }

        #endregion

        #region "Get Path"

        public static string GetAppath(string DataPath)
        {

            if (string.IsNullOrEmpty(DataPath))
                DataPath = "/";
            else if (!DataPath.EndsWith("/"))
                DataPath += "/";
            return DataPath;
        }

        public static string FormatUrlPath(string pPath)
        {
            if (pPath.StartsWith(@"/") && pPath.Length > 1)
            {
                int length = pPath.Length;
                pPath = pPath.Substring(1, length - 1);
            }
            else if (pPath == @"/")
            {
                pPath = "";
            }

            if (!string.IsNullOrEmpty(pPath))
            {
                pPath.Replace(@"http://", "").Replace(@"https://", "");
            }

            return (CheckUseSSL() ? "https://" : "http://") + pPath;
        }

        public static string GetPathDefaultDomain()
        {
            return FormatUrlPath(Variables.DomainName);
        }

        private static JsonSerializerSettings DefaultJsonSerializerSettings = new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore,
            NullValueHandling = NullValueHandling.Include,
            // PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            //MetadataPropertyHandling = MetadataPropertyHandling.Ignore

        };

        public static string GetPhysicalFilePath(string folderUpload, object id)
        {
            return Variables.DataPath + folderUpload + ConvertToString(id);
        }

        public static string SerializeToJSON(object obj)
        {
            return SerializeToJSON(obj, null);
        }

        public static string SerializeToJSON(object obj, JsonSerializerSettings settings)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {

                if (settings == null)
                {
                    settings = DefaultJsonSerializerSettings;
                }

                // nguyencuongcs 20191025: Formatting.Indented là giữ cho json pretty -> dễ đọc, tuy nhiên khoảng trắng nhiều, nặng
                // Nên tối ưu bằng cho bản production bằng cách ko giữ pretty nữa, nếu có nhu cầu test thì copy nội dung nhận được vào json online

                //return JsonConvert.SerializeObject(obj, Formatting.Indented, settings);
                return JsonConvert.SerializeObject(obj, Formatting.None, settings);
            }
        }

        #endregion

        #region "Files"

        public static string BuildNameWithExtension(string name, string extension)
        {
            return name + (string.IsNullOrEmpty(extension) ? "" : ("." + extension));
        }

        public static string BuildFullnameReturnNameAndExt(string requestName, ref string name, ref string extension)
        {
            if (string.IsNullOrEmpty(requestName))
            {
                requestName = "New-File.jpg";
            }
            string[] fileNameAndExt = requestName.Split('.');

            //int firstDotIndex = requestName.IndexOf('.');
            int lastDotIndex = requestName.LastIndexOf('.');

            if (lastDotIndex > 0)
            {
                extension = requestName.Substring(lastDotIndex + 1, requestName.Length - (lastDotIndex + 1)); // lastDotIndex + 1 để bỏ qua dấu . ở chính vị trí of nó
                extension = extension.NullIsEmpty().ToLower();
                if (!Variables.ConfigValidExtension.Contains("." + extension))
                {
                    //if (firstDotIndex != lastDotIndex)
                    //{
                    //    name = requestName.Substring(0, firstDotIndex);
                    //}
                    //else
                    //{ 
                    name = requestName;
                    //}
                    extension = "jpg";
                }
                else
                {
                    //name = requestName.Substring(0, firstDotIndex);
                    name = requestName.Substring(0, lastDotIndex);
                }
            }
            else
            {
                name = requestName;
                extension = "jpg";
            }

            if (string.IsNullOrEmpty(extension))
            {
                extension = "jpg";
            }

            return name + "." + extension;
        }

        public static string BuildDuplicateName(string name, string extension, long duplicateNumber)
        {
            string res = BuildNameWithExtension(name, extension);

            if (duplicateNumber < 1)
            {
                return res;
            }

            res = name + "(" + duplicateNumber + ")" + (string.IsNullOrEmpty(extension) ? "" : ("." + extension));

            return res;
        }

        #endregion

        #region "Check Extension Files"

        public static bool IsImage(string extension)
        {
            return Array.IndexOf(Variables.ConfigImageExtension, extension.ToLower()) >= 0;
        }

        public static bool IsFlash(string extension)
        {
            return Array.IndexOf(Variables.ConfigFlashExtension, extension.ToLower()) >= 0;
        }

        public static bool IsMedia(string extension)
        {
            return Array.IndexOf(Variables.ConfigMediaExtension, extension.ToLower()) >= 0;
        }

        public static bool IsAllowExtension(string extension)
        {
            return Array.IndexOf(Variables.ConfigValidExtension, extension.ToLower()) >= 0;
        }

        #endregion

        #region "Encrypt & Decrypt"

        public static string GetEncryptMD5(string pValue)
        {
            MD5 algorithm = MD5.Create();
            byte[] data = algorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(pValue));
            string result = "";
            for (int i = 0; i < data.Length; i++)
            {
                result += data[i].ToString("x2").ToUpperInvariant();
            }
            return result;
        }

        public static string EncodeTo64_ASCII(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string EncodeTo64_ASCII(string toEncode, int length)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes, 1, length);
            return returnValue;
        }

        static public string DecodeFrom64_ASCII(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64_UTF8(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64_UTF8(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64_UNICODE(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64_UNICODE(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncryptRSA(string text)
        {
            RSALib rsaLib = new RSALib();
            string res = rsaLib.Encrypt(text);
            return res;
        }

        public static string EncryptRSAForUI(string text)
        {
            RSALib rsaLib = new RSALib(true);
            string res = rsaLib.Encrypt(text);
            return res;
        }

        public static string DecryptRSA(string cipherText)
        {
            RSALib rsaLib = new RSALib();
            string res = rsaLib.Decrypt(cipherText);
            return res;
        }

        public static string DecryptRSAForUI(string cipherText)
        {
            RSALib rsaLib = new RSALib(true);
            string res = rsaLib.Decrypt(cipherText);
            return res;
        }

        public static string Encrypt3DES(string text, string key)
        {
            string res = TrippleDESLib.Encrypt(text, key);
            return res;
        }

        public static string Decrypt3DES(string cipherText, string key)
        {
            string res = TrippleDESLib.Decrypt(cipherText, key);
            return res;
        }

        public static string GetTokenFromTokenObjectEcryptedFromUI(string encryptedData, string encryptedKey)
        {
            // Dữ liệu này được api mã hóa 3DES bằng key + RSA(key) bằng publicKey của UI

            string plainKey = DecryptRSAForUI(encryptedKey);
            string plainData = Decrypt3DES(encryptedData, plainKey);

            Dictionary<string, object> dicToken = CommonMethods.DeserializeObject<Dictionary<string, object>>(plainData);

            var access_token = dicToken.GetValueFromKey_ReturnString(CommonParams.token_accessToken, "");
            //var expires_in = rootJToken[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJToken[CommonParams.expires_in]) : string.Empty;
            //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJToken[CommonParams.admin]);

            return access_token;
        }

        public static void ResetPostgresSlaveVariables()
        {
            Variables.Postgresql_RaoVat_Slave_IsHealthy = true;
            Variables.ConnectPostgresSlave_CountFailed = 0;
        }

        #endregion

        #region Api

        public static async Task<HttpResponseMessage> ApiGetAsync(string requestUri)
        {
            HttpResponseMessage res;

            using (var client = new HttpClient())
            //using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))       
            {
                res = await client.GetAsync(requestUri);
            }

            return res;
        }

        #endregion

        #region Image

        public static byte[] ConvertBitmapToBytes(Bitmap img)
        {
            try
            {
                if (img != null)
                {
                    ImageConverter converter = new ImageConverter();
                    var imgBytes = (byte[])converter.ConvertTo(img, typeof(byte[]));
                    return imgBytes;
                }

                return null;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
            finally
            {
                if (img != null)
                {
                    img.Dispose();
                }
            }
        }

        #endregion

        #region "RawSql"

        /* In | NotIn
         * 
         */
        public static string BuildSqlIn(string logicOperator, string columnName, List<int> lstId)
        {
            return BuildSqlConditionInOrNot(logicOperator, columnName, SqlOperator.In, lstId);
        }
        public static string BuildSqlIn(string logicOperator, string columnName, List<long> lstId)
        {
            return BuildSqlConditionInOrNot(logicOperator, columnName, SqlOperator.In, lstId);
        }
        public static string BuildSqlIn(string logicOperator, string columnName, List<string> lstId)
        {
            return BuildSqlConditionInOrNot(logicOperator, columnName, SqlOperator.In, lstId);
        }
        public static string BuildSqlNotIn(string logicOperator, string columnName, List<long> lstId)
        {
            return BuildSqlConditionInOrNot(logicOperator, columnName, SqlOperator.NotIn, lstId);
        }
        public static string BuildSqlConditionInOrNot(string logicOperator, string columnName, string sqlOp, List<int> lstId)
        {
            // condition là AND hoặc OR
            string res = "";
            if (lstId != null && lstId.Count > 0)
            {
                string data = $"({string.Join(SqlOperator.CommonKeySplit, lstId.ToArray())})";
                res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
            }
            return res;
        }
        public static string BuildSqlConditionInOrNot(string logicOperator, string columnName, string sqlOp, List<long> lstId)
        {
            // condition là AND hoặc OR
            string res = "";
            if (lstId != null && lstId.Count > 0)
            {
                string data = $"({string.Join(SqlOperator.CommonKeySplit, lstId.ToArray())})";
                res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
            }
            return res;
        }
        public static string BuildSqlConditionInOrNot(string logicOperator, string columnName, string sqlOp, List<string> lstId)
        {
            // condition là AND hoặc OR
            string res = "";
            if (lstId != null && lstId.Count > 0)
            {
                for (int i = 0; i < lstId.Count; i++)
                {
                    if (!lstId[i].StartsWith("'"))
                    {
                        lstId[i] = $"'{ lstId[i] }'";
                    }
                }
                string data = $"({string.Join(SqlOperator.CommonKeySplit, lstId.ToArray())})";
                res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
            }
            return res;
        }

        /* Equal | NotEqual
         * 
         */
        public static string BuildSqlEqual(string logicOperator, string columnName, object objData)
        {
            return BuildSqlConditionEqualOrNot(logicOperator, columnName, SqlOperator.Equal, objData);
        }
        public static string BuildSqlNotEqual(string logicOperator, string columnName, object objData)
        {
            return BuildSqlConditionEqualOrNot(logicOperator, columnName, SqlOperator.NotEqual, objData);
        }
        public static string BuildSqlConditionEqualOrNot(string logicOperator, string columnName, string sqlOp, object objData)
        {
            // condition là AND hoặc OR
            string res = "";
            if (objData != null)
            {
                if (objData != null)
                {
                    if (objData.IsBoolean())
                    {
                        bool? boolValue = CommonMethods.ConvertToBoolean(objData, null);
                        if (boolValue.HasValue)
                        {
                            string data = $"'{boolValue.ToString()}'";
                            res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                        }
                    }
                    else
                    {
                        string data = objData.ToString();

                        if (data.Equals(SqlOperator.DbNull))
                        {
                            if (sqlOp == SqlOperator.Equal)
                            {
                                sqlOp = "IS";
                            }
                            else if (sqlOp == SqlOperator.NotEqual)
                            {
                                sqlOp = "IS NOT";
                            }
                            data = SqlOperator.NullValue;
                        }
                        else
                        {
                            if (objData.IsString() || objData.IsDatetime() || objData.IsBoolean())
                            {
                                data = $"'{data}'";
                            }
                        }

                        res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                    }
                }
            }
            return res;
        }

        /* Like | NotLike
         * 
         */
        public static string BuildSqlLike(string logicOperator, string columnName, string data, bool lowerAndAsciiColumnName = true)
        {
            if (lowerAndAsciiColumnName)
            {
                // nguyencuongcs 20180714
                // Hàm này rất chậm nên tạm ngưng ko sử dụng
                // Có thể phải lấy dữ liệu lên code trước rồi mới dùng ToAsciiAndLower()
                //columnName = $"LOWER(dbo.fnRemoveNonASCII({columnName}))";
            }
            return BuildSqlConditionLikeOrNot(logicOperator, columnName, SqlOperator.Like, data);
        }
        public static string BuildSqlNotLike(string logicOperator, string columnName, string data, bool lowerAndAsciiColumnName = true)
        {
            if (lowerAndAsciiColumnName)
            {
                // nguyencuongcs 20180714
                // Hàm này rất chậm nên tạm ngưng ko sử dụng
                // Có thể phải lấy dữ liệu lên code trước rồi mới dùng ToAsciiAndLower()
                //columnName = $"LOWER(dbo.fnRemoveNonASCII({columnName}))";
            }
            return BuildSqlConditionLikeOrNot(logicOperator, columnName, SqlOperator.NotLike, data);
        }
        public static string BuildSqlConditionLikeOrNot(string logicOperator, string columnName, string sqlOp, string data)
        {
            // condition là AND hoặc OR
            string res = "";
            if (!string.IsNullOrEmpty(data))
            {
                data = $"N'%{data.ToASCIIAndLower()}%'";
                res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
            }
            return res;
        }

        /* Greater | GreaterOrEqual | Less |  LessOrEqual
         *  
         */
        public static string BuildSqlGreater(string logicOperator, string columnName, object objData, bool getDateAndTime = false)
        {
            return BuildSqlConditionGreaterOrLess(logicOperator, columnName, SqlOperator.Greater, objData, getDateAndTime);
        }
        public static string BuildSqlGreaterOrEqual(string logicOperator, string columnName, object objData, bool getDateAndTime = false)
        {
            return BuildSqlConditionGreaterOrLess(logicOperator, columnName, SqlOperator.GreaterOrEqual, objData, getDateAndTime);
        }
        public static string BuildSqlLess(string logicOperator, string columnName, object objData, bool getDateAndTime = false)
        {
            return BuildSqlConditionGreaterOrLess(logicOperator, columnName, SqlOperator.Less, objData, getDateAndTime);
        }
        public static string BuildSqlLessOrEqual(string logicOperator, string columnName, object objData, bool getDateAndTime = false)
        {
            return BuildSqlConditionGreaterOrLess(logicOperator, columnName, SqlOperator.LessOrEqual, objData, getDateAndTime);
        }
        public static string BuildSqlConditionGreaterOrLess(string logicOperator, string columnName, string sqlOp, object objData, bool getDateAndTime)
        {
            // condition là AND hoặc OR
            string res = "";
            if (objData != null)
            {
                if (objData != null)
                {
                    if (objData.IsNumber())
                    {
                        // nguyencuongcs 20180713: Chấp nhận các trường hợp số lớn như Int64 chưa xử lý vì thấy chưa có chỗ nào dùng
                        int number = CommonMethods.ConvertToInt32(objData, -1).Value;
                        if (number > 0)
                        {
                            string data = number.ToString();
                            res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                        }
                    }
                    else if (objData.IsDatetime())
                    {
                        DateTime? date = CommonMethods.ToDateTime(objData, null);
                        if (date.HasValue)
                        {
                            // nguyencuongcs 20180729: khi nào có nhu cầu xử lý time thì bổ sung options
                            string data = "";

                            if (!getDateAndTime)
                            {
                                data = $"'{CommonMethods.FormatDateTimeMMDDYYYY(date.Value)}'";
                                columnName = $"CAST({columnName} AS DATE)";
                            }
                            else
                            {
                                data = $"'{CommonMethods.FormatDateTimeMMDDYYYY_FULL(date.Value)}'";
                                columnName = $"CAST({columnName} AS DATETIME)";
                            }

                            res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                        }
                    }
                    else if (objData.IsBoolean())
                    {
                        //if (date.HasValue)
                        {
                            string data = $"'{objData.ToString()}'";
                            res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                        }
                    }
                    //else // Nếu có trờng hợp nào đặc biệt thì xử lý thêm
                    //{
                    //    string data = objData.ToString();
                    //    res = BuildSqlCondition(logicOperator, columnName, sqlOp, data);
                    //}
                }
            }
            return res;
        }

        /* Common
         * 
         */
        public static string BuildSqlCondition(string logicOperator, string columnName, string sqlOp, string data)
        {
            columnName = '"' + columnName + '"';
            // condition là AND hoặc OR
            string res = "";
            if (!string.IsNullOrEmpty(data))
            {
                res = $" {logicOperator} ({columnName} {sqlOp} {data})";
            }
            return res;
        }

        #endregion

        #region "Random"

        public static string GenerateRandomSixDigital()
        {
            Random generator = new Random();
            string code = generator.Next(0, 999999).ToString("D6");
            return code;
        }

        #endregion

        public static string AddCapDoString(int pCapDo)
        {
            var defaultTemplate = "----";
            var spaceString = "----";
            var addString = "";
            if (pCapDo > 1)
            {
                for (var i = 1; i < pCapDo; i++)
                {
                    if (i + 1 == pCapDo)
                    {
                        addString += defaultTemplate;
                    }
                    else
                    {
                        addString += spaceString;
                    }
                }
                addString = "|" + addString;
            }

            return addString;
        }

        public static string GenerateIdMd5(long id)
        {
            var md5 = GetEncryptMD5(Variables.PreMd5 + id).ToString();

            return md5.ToUpper();
        }

        public static string GetRandomMaGioiThieu(long idThanhVien)
        {
            var strDate = DateTime.Now.ToString(Variables.DDMMYYYY_FULL);

            var Result = CommonMethods.GenerateRandomSixDigital() + idThanhVien;

            return Result.ToUpper();
        }

        public static string GetRandomCodeSpecialDay()
        {
            var strDate = DateTime.Now.ToString(Variables.DDMMYYYY_FULL);
            var encrypted = CommonMethods.GetEncryptMD5(strDate);
            var Result = Variables.MACAUHINH_PREFIX_SPECIALDAY + encrypted.Substring(1, 2) + encrypted.Substring(encrypted.Length - 2) + strDate.Substring(strDate.Length - 2);

            return Result.ToUpper();
        }

        //- 1: Point(Tặng Point)
        //- 2: %        => MaxValue: 50000 (Giảm 2%, tối đa 50.000)
        //- 3: Cash(Giảm trực tiếp tiền mặt)
        public static decimal CalcValueMarketingPlan(decimal value, string valueMarketingPlan, int valueTypeMarketingPlan, string maxValueMarketingPlan = "")
        {
            var convertValueMarketingPlan = CommonMethods.ConvertToDecimal(valueMarketingPlan);
            switch (valueTypeMarketingPlan)
            {
                case 1:
                    value = value + convertValueMarketingPlan;
                    break;

                case 2:
                    var convertMaxValue = CommonMethods.ConvertToDecimal(maxValueMarketingPlan);
                    var valueAfterSales = (convertValueMarketingPlan * value) / 100;
                    if (valueAfterSales > convertMaxValue)
                    {
                        valueAfterSales = convertMaxValue;
                    }
                    value = value - valueAfterSales;
                    break;

                case 3:
                    value = value - convertValueMarketingPlan;
                    break;
            }

            return value;
        }

        //- 1: Point(Tặng Point)
        //- 2: %        => MaxValue: 50000 (Giảm 2%, tối đa 50.000)
        //- 3: Cash(Giảm trực tiếp tiền mặt)
        public static long CalcValueMarketingPlan(long value, string valueMarketingPlan, int valueTypeMarketingPlan, string maxValueMarketingPlan = "")
        {
            var convertValueMarketingPlan = CommonMethods.ConvertToInt64(valueMarketingPlan);
            switch (valueTypeMarketingPlan)
            {
                case 1:
                    value = value + convertValueMarketingPlan;
                    break;

                case 2:
                    var convertMaxValue = CommonMethods.ConvertToInt64(maxValueMarketingPlan);
                    var valueAfterSales = (convertValueMarketingPlan * value) / 100;
                    if (valueAfterSales > convertMaxValue)
                    {
                        valueAfterSales = convertMaxValue;
                    }
                    value = value - valueAfterSales;
                    break;

                case 3:
                    value = value - convertValueMarketingPlan;
                    break;
            }

            return value;
        }

        public static ConnectionInfo ConnectToLinux()
        {
            var keyFile = new PrivateKeyFile(Variables.Linux_SshKey, Variables.Linux_PassPharse);
            var keyFiles = new[] { keyFile };

            var methods = new List<AuthenticationMethod>();
            methods.Add(new PasswordAuthenticationMethod(Variables.Linux_UserName, Variables.Linux_PassPharse));
            methods.Add(new PrivateKeyAuthenticationMethod(Variables.Linux_UserName, keyFiles));

            return new Renci.SshNet.ConnectionInfo(Variables.Linux_IpMaster, 22, Variables.Linux_UserName, methods.ToArray());
        }
    }

    public class SqlOperator
    {
        public static string In = "IN";
        public static string NotIn = "NOT IN";
        public static string Equal = "=";
        public static string NotEqual = "!=";
        public static string Like = "LIKE";
        public static string NotLike = "NOT LIKE";

        public static string Greater = ">";
        public static string GreaterOrEqual = ">=";
        public static string Less = "<";
        public static string LessOrEqual = "<=";

        public static string And = "AND";
        public static string Or = "OR";

        public static string CommonKeySplit = ",";
        public static string DbNull = "DbNull"; // Thêm $ Để ktra
        public static string NullValue = "NULL"; // Thêm $ Để ktra
    }
}