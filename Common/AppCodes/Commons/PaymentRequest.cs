﻿using Newtonsoft.Json.Linq;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;
using raoVatHistoryApi.ModelsRaoVat;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Common
{
    class PaymentRequest
    {
        public PaymentRequest()
        {
        }

        public static string sendPaymentRequest(string endpoint, string postJsonString)
        {
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);

                var postData = postJsonString;

                var data = Encoding.UTF8.GetBytes(postData);

                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";

                httpWReq.ContentLength = data.Length;
                httpWReq.ReadWriteTimeout = 30000;
                httpWReq.Timeout = 15000;
                Stream stream = httpWReq.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();

                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

                string jsonresponse = "";

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string temp = null;
                    while ((temp = reader.ReadLine()) != null)
                    {
                        jsonresponse += temp;
                    }
                }

                //todo parse it
                return jsonresponse;
            }
            catch (WebException e)
            {
                return e.Message;
            }
        }

        public static string receiveTransactionStatusRequest(string endpoint, string postJsonString)
        {
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);

                var postData = postJsonString;

                var data = Encoding.UTF8.GetBytes(postData);

                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";

                httpWReq.ContentLength = data.Length;
                httpWReq.ReadWriteTimeout = 30000;
                httpWReq.Timeout = 15000;
                Stream stream = httpWReq.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();

                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();

                string jsonresponse = "";

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string temp = null;
                    while ((temp = reader.ReadLine()) != null)
                    {
                        jsonresponse += temp;
                    }
                }

                return jsonresponse;
            }
            catch (WebException e)
            {
                return e.Message;
            }
        }

        public static bool checkSignature(InterfaceParam interfaceParam, out string signature)
        {
            string rawHash = "partnerCode=" +
            Variables.PartnerCode + "&accessKey=" +
            Variables.AccessKey + "&requestId=" +
            interfaceParam.requestId + "&amount=" +
            interfaceParam.amount + "&orderId=" +
            interfaceParam.orderId + "&orderInfo=" +
            interfaceParam.orderInfo + "&orderType=" +
            interfaceParam.orderType + "&transId=" +
            interfaceParam.transId + "&message=" +
            interfaceParam.message + "&localMessage=" +
            interfaceParam.localMessage + "&responseTime=" +
            interfaceParam.responseTime + "&errorCode=" +
            interfaceParam.errorCode + "&payType=" +
            "qr" + "&extraData=" +
           interfaceParam.extraData;

            MoMoSecurity crypto = new MoMoSecurity();
            //sign signature SHA256
            signature = crypto.signSHA256(rawHash, Variables.SerectKey);

            if (interfaceParam.signature == signature)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool checkTransactionStatus(InterfaceParam interfaceParam, out string errorCode)
        {
            string rawHash = "partnerCode=" +
              Variables.PartnerCode + "&accessKey=" +
              Variables.AccessKey + "&requestId=" +
              interfaceParam.requestId + "&orderId=" +
              interfaceParam.orderId + "&requestType=" +
              "transactionStatus";

            MoMoSecurity crypto = new MoMoSecurity();
            //sign signature SHA256
            var signatureTransactionStatus = crypto.signSHA256(rawHash, Variables.SerectKey);

            JObject objCheckTransactionType = new JObject
                {
                    { "partnerCode", Variables.PartnerCode },
                    { "accessKey", Variables.AccessKey },
                    { "requestId", interfaceParam.requestId },
                    { "orderId", interfaceParam.orderId },
                    { "requestType", "transactionStatus"},
                    { "signature", signatureTransactionStatus },
                };

            JObject jObjectTransactionStatus = CommonMethods.ConvertToJObject(PaymentRequest.receiveTransactionStatusRequest(Variables.DomainMomoTransaction, objCheckTransactionType.ToString()));
            errorCode = jObjectTransactionStatus["errorCode"].ToString();
            if (CommonMethods.ConvertToInt32(errorCode) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
