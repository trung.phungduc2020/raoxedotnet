﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace raoVatApi.HttpFeatures
{
    internal class HttpSendFileFeature : IHttpSendFileFeature
    {
        private readonly IHttpSendFileFeature _originalSendFileFeature;
        //private readonly ResponseCachingStream _responseCachingStream;

        public HttpSendFileFeature(IHttpSendFileFeature originalSendFileFeature) //, ResponseCachingStream responseCachingStream)
        {
            _originalSendFileFeature = originalSendFileFeature;
            //_responseCachingStream = responseCachingStream;
        }

        // Flush and disable the buffer if anyone tries to call the SendFile feature.
        public Task SendFileAsync(string path, long offset, long? length, CancellationToken cancellation)
        {
            //_responseCachingStream.DisableBuffering();
            return _originalSendFileFeature.SendFileAsync(path, offset, length, cancellation);
        }
    }
}