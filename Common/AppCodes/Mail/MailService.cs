﻿using GmailApi;
using raoVatApi.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace raoVatApi.ModelsRaoVat
{
    public class MailService
    {
        public bool SendEmail(string subject, string body, string mailTo)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Variables.Constants.EmailSend_host);

                mail.From = new MailAddress(Variables.Constants.EmailSend_name);
                mail.To.Add(mailTo);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpServer.Port = Variables.Constants.EmailSend_port;
                SmtpServer.Credentials = new System.Net.NetworkCredential(Variables.Constants.EmailSend_name, Variables.Constants.EmailSend_pass);
                SmtpServer.EnableSsl = Variables.Constants.EMAILSEND_ISSSL;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool SendEmailToDev(string subject, string body, string mailTo)
        {
            try
            {
                var receiveMail = mailTo;
                var tieuDe = subject;
                var noiDung = body;

                // send email
                if (Variables.EnvironmentIsProduction == true)
                {
                    GmailApiCore gac = new GmailApiCore();
                    var ccEmail = string.Empty;
                    var bccEmail = Variables.EmailToCollectError_Name;
                    var res = gac.SendMessage(receiveMail, ccEmail, bccEmail, tieuDe, noiDung);
                }
                else
                {
                    SendEmail(tieuDe, noiDung, receiveMail);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Send email has ads image
        /// </summary>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="mailTo">Mail to</param>
        /// <param name="adsImageUrls">List ads image url</param>
        /// <returns>Result send mail</returns>
        //public bool SendEmailHasAdsImage(string subject, string body, string mailTo, List<string> adsImageUrls)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        SmtpClient SmtpServer = new SmtpClient(Variables.Constants.EMAILSEND_HOST);

        //        mail.From = new MailAddress(Variables.Constants.EMAILSEND_NAME);
        //        mail.To.Add(mailTo);
        //        mail.Subject = subject;
        //        mail.IsBodyHtml = true;

        //        SmtpServer.Port = Variables.Constants.EMAILSEND_PORT;
        //        SmtpServer.Credentials = new System.Net.NetworkCredential(Variables.Constants.EMAILSEND_NAME, Variables.Constants.EMAILSEND_PASSWORD);
        //        SmtpServer.EnableSsl = Variables.Constants.EMAILSEND_ISSSL;

        //        if ((adsImageUrls.Count > 0) && (body.Contains(Variables.Constants.PARAM_ADS_INFO)))
        //        {
        //            var adsImageContent = string.Empty;
        //            var imgResourceList = new List<LinkedResource>();
        //            foreach (var adsImageUrl in adsImageUrls)
        //            {
        //                var physcialUrlImage = string.Format(@"{0}{1}{2}{3}", Variables.Constants.PHYSICAL_DIR_PATH_ROOT_CP_SYSTEM, Variables.Constants.PHYSICAL_DIR_PATH_ROOT_COUPON, Variables.Constants.PHYSICAL_DIR_PATH_ROOT_IMAGE_ADS, adsImageUrl);
        //                if (File.Exists(physcialUrlImage) == true)
        //                {
        //                    var adsImage = new LinkedResource(physcialUrlImage);
        //                    adsImage.ContentId = Guid.NewGuid().ToString();
        //                    adsImageContent += string.Format(@"<img max-width='{0}' max-height='{1}' src=""cid:{2}"" />", Variables.Constants.MAIL_DEFAULT_WIDTH_ADS_IMAGE, Variables.Constants.MAIL_DEFAULT_HEIGHT_ADS_IMAGE, adsImage.ContentId);
        //                    imgResourceList.Add(adsImage);
        //                }
        //            }

        //            body = body.Replace(Variables.Constants.PARAM_ADS_INFO, 'c'); //adsImageContent);

        //            var view = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
        //            foreach (var imgResorce in imgResourceList)
        //            {
        //                view.LinkedResources.Add(imgResorce);
        //            }
        //            mail.AlternateViews.Add(view);
        //        }

        //        SmtpServer.Send(mail);
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonMethods.WriteLog(ex.Message);
        //        return false;
        //    }

        //    return true;
        //}
    }
}