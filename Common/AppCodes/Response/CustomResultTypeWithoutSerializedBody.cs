using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Repository;
using System.Net;
using raoVatApi.Response;
using raoVatApi.Models;
using raoVatApi.Common;
using raoVatApi.ModelsRaoVat;

namespace Response
{
    public class CustomResultTypeWithoutSerializedBody<T>
    {
        private int _intResult = 1;      // -1: Het session, 0: Không có dữ liệu, 1: OK, -2: Lỗi
        private string _strResult = string.Empty;
        private IEnumerable<T> _dataResult;
        private string _message = string.Empty;
        private string _gotoLink = string.Empty;
        private PaginationHeader _pagination = new PaginationHeader();

        private JObject _dataResultJObject;

        public CustomResultTypeWithoutSerializedBody()
        {

        }

        public CustomResultTypeWithoutSerializedBody(string requestUri, string httpType, string bodyData, params HttpParam[] arrParams)
        {
            string res = "";
            try
            {
                ApiGetCore<T> core = new ApiGetCore<T>();
                res = core.GetAsyncReturnStringWithoutSerializedBody(requestUri, httpType, bodyData, arrParams).Result;

                if (!string.IsNullOrEmpty(res))
                {
                    JObject resObj;
                    try
                    {
                        resObj = JObject.Parse(res);
                    }
                    catch
                    {
                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(res);
                        resObj = JObject.Parse(obj.ToString());
                    }
                    LoadProperty(resObj);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                
                // string error = ex.ToString();
                if (res.ToLower() == CommonMethods.ConvertToString(HttpStatusCode.BadRequest).ToLower())
                {
                    Message = Messages.ERR_Http_BadRequest;
                }
                else if (res.ToLower() == CommonMethods.ConvertToString(HttpStatusCode.Unauthorized).ToLower())
                {
                    Message = Messages.ERR_Http_UnAuthorized;
                }
                else
                {
                    Message = Messages.ERR_004;
                }
            }
        }

        public CustomResultTypeWithoutSerializedBody(JObject obj)
        {
            LoadProperty(obj);
        }

        private void LoadProperty(JObject obj)
        {
            if (obj != null)
            {
                var rootJToken = obj.Root;
                //CommonMethods.DeserializeObject<IEnumerable<CauHinhHeThong>>(obj.Root["DataResult"]);
                if (rootJToken != null)
                {
                    // Vì property Set của Message gán lại _intResult nên phải lấy trước
                    Message = rootJToken["Message"] != null ? rootJToken["Message"].ToString() : string.Empty;
                    StrResult = rootJToken["StrResult"] != null ? rootJToken["StrResult"].ToString() : string.Empty;
                    DataResult = CommonMethods.DeserializeObject<List<T>>(rootJToken["DataResult"]);
                    Pagination = CommonMethods.DeserializeObject<PaginationHeader>(rootJToken["Pagination"]);
                    GotoLink = rootJToken["GotoLink"] != null ? rootJToken["GotoLink"].ToString() : string.Empty;

                    IntResult = CommonMethods.DeserializeObject<int>(rootJToken["IntResult"]);


                    DataResultJObject = obj;
                }
            }
        }

        public int IntResult
        {
            get { return this._intResult; }
            set { this._intResult = value; }
        }
        public string StrResult
        {
            get { return this._strResult; }
            set { this._strResult = value; }
        }
        public IEnumerable<T> DataResult
        {
            get { return this._dataResult; }
            set
            {
                this._dataResult = value;
                this._intResult = this._dataResult != null ? 1 : 0;
            }
        }
        public JObject DataResultJObject
        {
            get { return this._dataResultJObject; }
            set
            {
                this._dataResultJObject = value;                
            }
        }
        public PaginationHeader Pagination
        {
            get { return this._pagination; }
            set { this._pagination = value; }
        }
        public string Message
        {
            get { return this._message; }
            set
            {
                this._message = value;
                this._intResult = 0;
            }
        }
        public string GotoLink
        {
            get { return this._gotoLink; }
            set { this._gotoLink = value; }
        }
        public void SetMessageLogout()
        {
            this._intResult = -1;
            this._message = Variables.MessageSessionTimeOut;
        }
        public void SetMessageInvalidRole()
        {
            this._intResult = -1;
            this._message = Variables.MessageSessionInvalidRole;
        }
        public void SetException(Exception ex)
        {
            this._intResult = -2;
            this._message = ex.Message;
        }
        public void SetException(string msg)
        {
            this._intResult = -2;
            this._message = msg;
        }
        public void AddPagination(int currentPage, int displayItems, int resultCount, long totalItems)
        {
            var totalPages = (long)Math.Round((double)totalItems / displayItems, 0, MidpointRounding.AwayFromZero);

            this.Pagination.CurrentPage = currentPage;
            this.Pagination.DisplayItems = displayItems;
            this.Pagination.ResultCount = resultCount;
            this.Pagination.TotalItems = totalItems;

            if (totalPages < 1) { totalPages = 1; }

            this.Pagination.TotalPages = totalPages;
        }
    }
}