using raoVatApi.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace raoVatApi.Response
{

    public static class RequestExtensions
    {
        public static string Host(this HttpRequest request)
        {
            // request.Host.ToString() => co :port => localhost:5000
            // request.Host.Value => localhost 
            return request.Host.HasValue ? request.Host.ToString() : string.Empty;
        }

        public static string HeaderOrigin(this HttpRequest request)
        {
            // http://localhost:5000
            // HeaderHost: without http:// => localhost:5000

            return request.Headers["Origin"].ToString();
        }

        public static string HeaderReferer(this HttpRequest request)
        {
            // http://localhost:5000/admin/nhomkhachhang?displayPage=2&displayItems=20
            return request.Headers["Referer"].ToString();
        }

        public static string Path(this HttpRequest request)
        {
            return request.Path.HasValue ? request.Path.Value : string.Empty;
        }

        public static string PathAndQueryString(this HttpRequest request)
        {
            return request.Path() + (request.QueryString.HasValue ? request.QueryString.Value : string.Empty);
        }

        public static string RawUrl(this HttpRequest request)
        {
            // nguyencuongcs 20171010: tạm thời dùng Header["Referer"] để chứa đường path gốc trước khi RewriteLink             

            string urlAndQuery = request.RawUrlWithQuery();
            if (string.IsNullOrEmpty(urlAndQuery))
            {
                urlAndQuery = request.Path(); //nguyencuongcs 20171005: remove request.Host()
            }

            string[] arrayStr = urlAndQuery.Split("?");
            string urlWithoutQuery = urlAndQuery;
            if (arrayStr != null && arrayStr.Length > 1)
            {
                urlWithoutQuery = arrayStr[0];
            }

            return urlWithoutQuery;
        }

        public static string RawUrlWithQuery(this HttpRequest request)
        {
            // nguyencuongcs 20171010: tạm thời dùng Header["Referer"] để chứa đường path gốc trước khi RewriteLink             
            // nguyencuongcs 20180517: đã thêm QueryString vào Referer ở RewriteLink
            string urlAndQuery = request.Headers[HeaderNames.Referer].ToString().ToLower().Trim();

            return urlAndQuery;
        }

        public static string GetPathCurrentHost(this HttpRequest request)
        {
            return CommonMethods.FormatUrlPath(request.Host());
        }

        public static string GetCurrentPath(this HttpRequest request)
        {
            string link = request.RawUrl();
            if (!link.StartsWith(request.Host()))
            {
                if (!link.Contains(request.Host()))
                {
                    link = request.Host() + link;
                }
            }

            string res = CommonMethods.FormatUrlPath(link);
            return res;
        }

        public static string GetCurrentLastPath(this HttpRequest request)
        {
            // HttpContext.Current.Request.RawUrl: domain.com/page.html?s=nguyen => /page.html?s=nguyen
            // HttpContext.Current.Request.Path: domain.com/page.html?s=nguyen => /page.html
            return request.GetCurrentLastPathByLink(request.RawUrl());
        }

        public static string GetCurrentLastPathByLink(this HttpRequest request, string pLink)
        {
            // Remove query string
            pLink = pLink.Split(new string[] { "/?" }, StringSplitOptions.None)[0];
            pLink = pLink.Split('?')[0];

            // Remove ký tự cuối cùng /
            if (pLink.EndsWith("/"))
            {
                pLink = pLink.Substring(0, pLink.Length - 1);
            }
            string[] arrRewrite = pLink.Split('/');
            return arrRewrite[arrRewrite.Length - 1];
        }

        public static string GetCurrentRelativePath(this HttpRequest request)
        {
            string url = request.RawUrl();
            url = GetCurrentAbsolutePathFile(url);
            return url.Substring(url.LastIndexOf("/") + 1);
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="pLink">http://ceoquang.com/admin/MenuList.aspx/Search</param>
        /// <returns>http://ceoquang.com/admin/MenuList.aspx</returns>
        public static string GetCurrentAbsolutePathFile(string pLink)
        {
            return System.Text.RegularExpressions.Regex.Replace(pLink, "([^.]+).(html|aspx|asmx)(.*?)$", "$1.$2", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
        }

        //public static Uri GetAbsoluteUri(this HttpRequest request)
        //{            
        //    UriBuilder uriBuilder = new UriBuilder();
        //    uriBuilder.Scheme = request.Scheme;
        //    uriBuilder.Host = request.Host.ToString();
        //    uriBuilder.Path = request.Path.ToString();
        //    uriBuilder.Query = request.QueryString.ToString();
        //    return uriBuilder.Uri;
        //}

        public static String GetAbsoluteUri(this HttpRequest request)
        {
            try
            {
                UriBuilder uriBuilder = new UriBuilder();
                uriBuilder.Scheme = request.Scheme;
                uriBuilder.Host = request.Host.ToString();
                uriBuilder.Path = request.Path.ToString();
                uriBuilder.Query = request.QueryString.ToString();
                return uriBuilder.Uri.ToString();
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return request.ToString();
            }
        }

        public static int GetTokenIdAdmin(this HttpRequest request)
        {
            int res = request.HttpContext.GetTokenIdAdmin();
            return res;
        }

        public static string GetTokenHoTenAdmin(this HttpRequest request)
        {
            var res = request.HttpContext.GetTokenHoTenAdmin();
            return res;
        }

        public static bool CheckIsAdmin(this HttpRequest request)
        {
            return request.GetTokenIdAdmin() > 0;
        }

        public static void Redirect301(this RewriteContext rewriteContext, string newLink)
        {
            rewriteContext.HttpContext.Response.StatusCode = 301;
            rewriteContext.HttpContext.Response.Headers[HeaderNames.ContentLocation] = newLink;
            rewriteContext.HttpContext.Response.Redirect(newLink);
            rewriteContext.Result = RuleResult.EndResponse;
        }

        public static void Redirect302(this RewriteContext rewriteContext, string newLink)
        {
            rewriteContext.HttpContext.Response.StatusCode = 302;
            rewriteContext.HttpContext.Response.Headers[HeaderNames.ContentLocation] = newLink;
            rewriteContext.HttpContext.Response.Redirect(newLink);
            rewriteContext.Result = RuleResult.EndResponse;
        }

        public static void Redirect404(this RewriteContext rewriteContext, string newLink)
        {
            rewriteContext.HttpContext.Response.StatusCode = 404;
            rewriteContext.HttpContext.Response.Headers[HeaderNames.ContentLocation] = newLink;
            rewriteContext.HttpContext.Response.Redirect(newLink);
            rewriteContext.Result = RuleResult.EndResponse;
        }

        public static void SetCookie(this HttpRequest request, string key, string value, int? expireMinutes, bool isUseForAdmin)
        {
            try
            {
                CookieOptions option = new CookieOptions();

                if (expireMinutes.HasValue)
                {
                    option.Expires = DateTime.Now.AddMinutes(expireMinutes.Value);
                    // option.HttpOnly = true; // Chỉ truy xuất ở server
                }
                else
                {
                    option.Expires = DateTime.Now.AddDays(365);
                }

                if (isUseForAdmin)
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Admin]);
                    arrValue = AddOrUpdateKeyInDic(arrValue, key, value);
                    request.HttpContext.Response.Cookies.Append(Variables.Cookie_Group_Admin, CommonMethods.SerializeObject(arrValue), option);
                }
                else
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Client]);
                    arrValue = AddOrUpdateKeyInDic(arrValue, key, value);
                    request.HttpContext.Response.Cookies.Append(Variables.Cookie_Group_Client, CommonMethods.SerializeObject(arrValue), option);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
        }

        public static string GetCookie(this HttpRequest request, string key)
        {

            // //read cookie from IHttpContextAccessor  
            // string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["key"];  

            //read cookie from Request object  
            string cookieValueFromReq = ""; // request.GetCookie(key, value, expireMinutes);

            try
            {
                if (request.PathAndQueryString().Contains("/admin"))
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Admin]);
                    cookieValueFromReq = GetValueInDic(arrValue, key);
                }
                else
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Client]);
                    cookieValueFromReq = GetValueInDic(arrValue, key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog("GetCookie key '" + key + "'");
            }

            return CommonMethods.DeserializeObject<string>(cookieValueFromReq.NullIsEmpty());
        }

        public static string GetCookie(this HttpRequest request, string key, bool isUseForAdmin)
        {

            // //read cookie from IHttpContextAccessor  
            // string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["key"];  

            //read cookie from Request object  
            string cookieValueFromReq = ""; // request.GetCookie(key, value, expireMinutes);

            try
            {
                if (isUseForAdmin)
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Admin]);
                    cookieValueFromReq = GetValueInDic(arrValue, key);
                }
                else
                {
                    Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Client]);
                    cookieValueFromReq = GetValueInDic(arrValue, key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                CommonMethods.WriteLog("GetCookie key '" + key + "'");
            }

            return CommonMethods.DeserializeObject<string>(cookieValueFromReq.NullIsEmpty());
        }

        public static void RemoveCookie(this HttpRequest request, string key, bool isUseForAdmin)
        {
            // Remove nếu key nằm trong cấu trúc json
            if (isUseForAdmin)
            {
                Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Admin]);
                arrValue = RemoveKeyInDic(arrValue, key);
                request.HttpContext.Response.Cookies.Append(Variables.Cookie_Group_Admin, CommonMethods.SerializeObject(arrValue), null);
            }
            else
            {
                Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[Variables.Cookie_Group_Client]);
                arrValue = RemoveKeyInDic(arrValue, key);
                request.HttpContext.Response.Cookies.Append(Variables.Cookie_Group_Admin, CommonMethods.SerializeObject(arrValue), null);
            }

            // Remove key nếu nằm ngoài danh sách cookie
            // Chạy hàm này sau vì nếu vô trường hợp muốn xóa key Cookie_Group_Admin hoặc Cookie_Group_Client thì 2 hàm trên sẽ tạo lại key mới nếu xóa trước
            request.HttpContext.Response.Cookies.Delete(key);
        }

        public static void SetCookieForClient(this HttpRequest request, string key, string value, int? expireMinutes)
        {
            request.SetCookie(key, value, expireMinutes, false);
        }

        public static void SetCookieForAdmin(this HttpRequest request, string key, string value, int? expireMinutes)
        {
            request.SetCookie(key, value, expireMinutes, true);
        }

        public static string GetCookieForClient(this HttpRequest request, string key)
        {
            return request.GetCookie(key, false);
        }

        public static string GetCookieForAdmin(this HttpRequest request, string key)
        {
            return request.GetCookie(key, true);
        }

        public static void RemoveCookieForCLient(this HttpRequest request, string key)
        {
            request.RemoveCookie(key, false);
        }

        public static void RemoveCookieForAdmin(this HttpRequest request, string key)
        {
            request.RemoveCookie(key, true);
        }

        public static void SetSession(this HttpRequest request, string key, string value, bool isUseForAdmin)
        {
            try
            {
                //request.HttpContext.Session.SetString(key, value);
                // nguyencuongcs 20180820: giá trị session lưu ở server nên ko cần lưu theo cấu trúc group by project name như cooke
                key = BuildSessionKey(key, isUseForAdmin);
                request.HttpContext.Session.SetString(key, CommonMethods.SerializeObject(value));

                /*
                if (CommonParams.groupKeyAccessArray.Contains(key))
                {
                    request.HttpContext.Session.SetString(CommonParams.groupKeyAccess, CommonMethods.SerializeObject(new Dictionary<string, object> { { key, value } }));
                }
                else
                {
                    request.HttpContext.Session.SetString(key, CommonMethods.SerializeObject(value));
                }
                */

                request.HttpContext.Session.CommitAsync();
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
        }

        public static string GetSession(this HttpRequest request, string key, bool isUseForAdmin)
        {

            // //read cookie from IHttpContextAccessor  
            // string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["key"];  

            //read cookie from Request object  
            request.HttpContext.Session.LoadAsync();
            string cookieValueFromReq = ""; // request.HttpContext.Session.GetString(key);
                                            // nguyencuongcs 20180820: giá trị session lưu ở server nên ko cần lưu theo cấu trúc group by project name như cooke

            key = BuildSessionKey(key, isUseForAdmin);
            var value = request.HttpContext.Session.GetString(key);
            cookieValueFromReq = value == null ? "" : value;

            /*
            if (CommonParams.groupKeyAccessArray.Contains(key))
            {
                Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.HttpContext.Session.GetString(CommonParams.groupKeyAccess));
                cookieValueFromReq = (arrValue == null || arrValue[key] == null) ? "" : arrValue[key];
            }
            else
            {
                var value = request.HttpContext.Session.GetString(key);
                cookieValueFromReq = value == null ? "" : value;
            } 
            */

            return CommonMethods.DeserializeObject<string>(cookieValueFromReq.NullIsEmpty());
        }

        public static void RemoveSession(this HttpRequest request, string key, bool isUseForAdmin)
        {
            key = BuildSessionKey(key, isUseForAdmin);
            request.HttpContext.Session.Remove(key);
        }

        public static void SetSessionForClient(this HttpRequest request, string key, string value)
        {
            request.SetSession(key, value, false);
        }
        public static void SetSessionForAdmin(this HttpRequest request, string key, string value)
        {
            request.SetSession(key, value, true);
        }

        public static string GetSessionForClient(this HttpRequest request, string key)
        {
            return request.GetSession(key, false);
        }
        public static string GetSessionForAdmin(this HttpRequest request, string key)
        {
            return request.GetSession(key, true);
        }
        public static void RemoveSessionForClient(this HttpRequest request, string key)
        {
            request.RemoveSession(key, false);
        }
        public static void RemoveSessionForAdmin(this HttpRequest request, string key)
        {
            request.RemoveSession(key, true);
        }

        /// <summary>
        /// Retrieve the raw body as a string from the Request.Body stream
        /// </summary>
        /// <param name="request">Request instance to apply to</param>
        /// <param name="encoding">Optional - Encoding, defaults to UTF8</param>
        /// <returns></returns>
        public static async Task<string> GetRawBodyStringAsync(this HttpRequest request, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;

            using (StreamReader reader = new StreamReader(request.Body, encoding))
                return await reader.ReadToEndAsync();
        }

        /// <summary>
        /// Retrieves the raw body as a byte array from the Request.Body stream
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static async Task<byte[]> GetRawBodyBytesAsync(this HttpRequest request)
        {
            using (var ms = new MemoryStream(2048))
            {
                await request.Body.CopyToAsync(ms);
                return ms.ToArray();
            }
        }

        public static int GetTokenIdAdmin(this HttpContext context)
        {
            //request.HttpContext
            int res = -1;

            try
            {
                var claimsIdentity = (ClaimsIdentity)context.User.Identities.FirstOrNewObject();
                IEnumerable<Claim> claims = claimsIdentity.Claims;
                foreach (Claim cl in claims)
                {
                    if (cl.Type.ToLower() == "id")
                    {
                        res = CommonMethods.ConvertToInt32(cl.Value);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }

            return res;
        }

        public static string GetTokenHoTenAdmin(this HttpContext context)
        {
            //request.HttpContext
            var res = string.Empty;

            try
            {
                var claimsIdentity = (ClaimsIdentity)context.User.Identities.FirstOrNewObject();
                return claimsIdentity.Name;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }

            return res;
        }

        public static string GetSessionOrCookie(this HttpRequest request, string key, bool isUseForAdmin)
        {
            // Ưu tiên lấy session trước mới đầy đủ thông tin, vì nếu người dùng liên tục click mở nhiều tin ra tab mới, mỗi request gửi đi đã chứa
            // cookie cũ => bị ghi đè thiếu thông tin lẫn nhau
            string res = "";
            try
            {
                res = request.GetSession(key, isUseForAdmin);
                if (string.IsNullOrEmpty(res))
                {
                    res = request.GetCookie(key, isUseForAdmin);
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
            return res.NullIsEmpty();
        }

        public static string GetSessionOrCookieForAdmin(this HttpRequest request, string key)
        {
            return request.GetSessionOrCookie(key, true);
        }

        public static string GetSessionOrCookieForClient(this HttpRequest request, string key)
        {
            return request.GetSessionOrCookie(key, false);
        }

        public static void SetSessionOrCookie(this HttpRequest request, string key, string value, int? expireMinutes, bool isUseForAdmin)
        {
            request.SetSession(key, value, isUseForAdmin);
            request.SetCookie(key, value, expireMinutes, isUseForAdmin);
        }

        public static void RemoveSessionAndCookie(this HttpRequest request, string key, bool isUseForAdmin)
        {
            request.RemoveSession(key, isUseForAdmin);
            request.RemoveCookie(key, isUseForAdmin);
        }

        public static void SetSessionOrCookieForAdmin(this HttpRequest request, string key, string value, int? expireMinutes)
        {
            request.SetSessionOrCookie(key, value, expireMinutes, false);
        }
        public static void SetSessionOrCookieForClient(this HttpRequest request, string key, string value, int? expireMinutes)
        {
            request.SetSessionOrCookie(key, value, expireMinutes, true);
        }
        public static void RemoveSessionAndCookieForAdmin(this HttpRequest request, string key)
        {
            request.RemoveSessionAndCookie(key, false);
        }
        public static void RemoveSessionAndCookieForClient(this HttpRequest request, string key)
        {
            request.RemoveSessionAndCookie(key, true);
        }

        public static int GetRequestInt(this HttpRequest request, string constName)
        {
            int tmp = CommonMethods.ConvertToInt32(request.Query[constName]);
            return tmp;
        }

        public static string GetRequestString(this HttpRequest request, string constName)
        {
            string res = CommonMethods.ConvertToString(request.Query[constName]);
            return res;
        }

        public static string GetTokenRequireRole(this HttpRequest request)
        {
            string res = request.HttpContext.GetTokenRequireRole();
            return res;
        }

        public static string GetTokenRequireRole(this HttpContext context)
        {
            //request.HttpContext
            string res = "";

            try
            {
                var claimsIdentity = (ClaimsIdentity)context.User.Identities.FirstOrNewObject();
                IEnumerable<Claim> claims = claimsIdentity.Claims;
                foreach (Claim cl in claims)
                {
                    if (cl.Type.ToLower() == ClaimTypes.Role.ToLower())
                    {
                        res = CommonMethods.ConvertToString(cl.Value);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }

            return res;
        }

        public static int GetTokenIdThanhVien(this HttpRequest request)
        {
            int res = request.HttpContext.GetTokenIdThanhVien();
            return res;
        }

        public static int GetTokenIdThanhVien(this HttpContext context)
        {
            //request.HttpContext
            int res = -1;

            try
            {
                var claimsIdentity = (ClaimsIdentity)context.User.Identities.FirstOrNewObject();
                IEnumerable<Claim> claims = claimsIdentity.Claims;
                foreach (Claim cl in claims)
                {
                    if (cl.Type.ToLower() == "id")
                    {
                        res = CommonMethods.ConvertToInt32(cl.Value);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }

            return res;
        }

        public static string GetTokenFromHeader(this HttpRequest request)
        {
            try
            {
                var headers = request.Headers;
                Dictionary<string, object> headerDic = headers.AsDictionary();
                string tokenFromHeader = headerDic.GetValueFromKey_ReturnString("HeaderAuthorization", "");
                return tokenFromHeader;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        #region  "Mobile"
        public static bool CheckAndSetMobile(this HttpRequest request, bool isUseForAdmin)
        {
            bool res = false;
            string userAgent = request.Headers["User-Agent"].ToString();
            if (userAgent.ToLower().Contains(Variables.DEVICE_MOBILE)
                //&& (userAgent.ToLower().Contains("android") || userAgent.ToLower().Contains("ios"))
                )
            {
                res = true;
                request.SetMobileVersionToSessionAndCookie(res.ToString().ToLower(), isUseForAdmin);
            }

            return res;
        }

        public static void SetMobileVersionToSessionAndCookie(this HttpRequest request, string boolValue, bool isUseForAdmin)
        {
            request.SetSessionOrCookie(Variables.SMobile, boolValue.ToLower(), null, isUseForAdmin);
        }

        public static bool GetMobileVersionFromSessionAndCookie(this HttpRequest request, bool isUseForAdmin)
        {
            bool mobileVersion = false;
            try
            {
                mobileVersion = CommonMethods.ConvertToBoolean(request.GetSessionOrCookie(Variables.SMobile, isUseForAdmin));
            }
            catch (Exception ex)
            {
                CommonMethods.WriteLog("GetMobileVersionFromSessionAndCookie error: " + ex.ToString());
            }
            return mobileVersion;
        }

        public static void SetMobileVersionToSessionAndCookieForAdmin(this HttpRequest request, string boolValue)
        {
            request.SetMobileVersionToSessionAndCookie(boolValue, false);
        }
        public static void SetMobileVersionToSessionAndCookieForClient(this HttpRequest request, string boolValue)
        {
            request.SetMobileVersionToSessionAndCookie(boolValue, true);
        }

        public static bool GetMobileVersionFromSessionAndCookieForAdmin(this HttpRequest request)
        {
            return request.GetMobileVersionFromSessionAndCookie(false);
        }
        public static bool GetMobileVersionFromSessionAndCookieForClient(this HttpRequest request)
        {
            return request.GetMobileVersionFromSessionAndCookie(true);
        }

        private static Dictionary<string, string> AddOrUpdateKeyInDic(Dictionary<string, string> arrValue, string key, string value)
        {
            if (arrValue == null)
            {
                arrValue = new Dictionary<string, string>();
            }

            if (!arrValue.ContainsKey(key))
            {
                arrValue.Add(key, "");
            }

            arrValue[key] = value;
            return arrValue;
        }

        private static Dictionary<string, string> RemoveKeyInDic(Dictionary<string, string> arrValue, string key)
        {
            if (arrValue != null && arrValue.ContainsKey(key))
            {
                arrValue.Remove(key);
            }
            return arrValue;
        }

        private static string GetValueInDic(Dictionary<string, string> arrValue, string key)
        {
            string value = "";
            if (arrValue != null && arrValue.ContainsKey(key))
            {
                value = arrValue[key];
            }
            return value;
        }

        private static string BuildSessionKey(string key, bool isUseForAdmin)
        {
            string prefix = isUseForAdmin ? Variables.Session_Prefix_Admin : Variables.Session_Prefix_Client;
            return prefix + key;
        }

        public static string GetTokenDecrypted(this HttpRequest request)
        {
            // Ưu tiên lấy session trước mới đầy đủ thông tin, vì nếu người dùng liên tục click mở nhiều tin ra tab mới, mỗi request gửi đi đã chứa
            // cookie cũ => bị ghi đè thiếu thông tin lẫn nhau
            string res = "";
            try
            {
                string encryptedTokenData = request.GetSessionOrCookieForClient(CommonParams.token_data);
                string encryptedTokenKey = request.GetSessionOrCookieForClient(CommonParams.publicKey);

                var access_token = CommonMethods.GetTokenFromTokenObjectEcryptedFromUI(encryptedTokenData, encryptedTokenKey);

                //var access_token = dicToken.GetValueFromKey_ReturnString(CommonParams.access_token, ""); // ? CommonMethods.ConvertToString(rootJToken[]) : string.Empty;
                //var expires_in = rootJToken[CommonParams.expires_in] != null ? CommonMethods.ConvertToString(rootJToken[CommonParams.expires_in]) : string.Empty;
                //var admin = CommonMethods.DeserializeObject<List<Admin>>(rootJToken[CommonParams.admin]);

                res = access_token;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }
            return res.NullIsEmpty();
        }

        #endregion
    }
}