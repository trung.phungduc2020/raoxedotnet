// using System;
// using System.Collections.Generic;
// using Commons;
// using Microsoft.AspNetCore.Http;
// using Newtonsoft.Json;

// public static class CookieExtensions
// {
//     public static void Set<T>(this IResponseCookies cookie, string key, T value, int? expireMinutes)
//     {
//         CookieOptions option = new CookieOptions();

//         if (expireMinutes.HasValue)
//         {
//             option.Expires = DateTime.Now.AddMinutes(expireMinutes.Value);
//             // option.HttpOnly = true; // Chỉ truy xuất ở server
//         }
//         else
//         {
//             option.Expires = DateTime.Now.AddDays(365);
//         }

//         if (CommonParams.groupKeyAccessArray.Contains(key))
//         {
//             cookie.Append(CommonParams.groupKeyAccess, JsonConvert.SerializeObject(new KeyValuePair<string, T>(key, value)), option);
//         }
//         else
//         {
//             cookie.Append(key, JsonConvert.SerializeObject(value), option);
//         }
//     }

//     public static T GetCookie<T>(this HttpRequest request, string key)
//     {

//         if (CommonParams.groupKeyAccessArray.Contains(key))
//         {
//             Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(request.Cookies[CommonParams.groupKeyAccess]);
//             return (arrValue == null || arrValue[key] == null) ? default(T) :
//                                  JsonConvert.DeserializeObject<T>(arrValue[key]);
//         }
//         else
//         {
//             var value = request.Cookies[key];
//             return value == null ? default(T) :
//                                   JsonConvert.DeserializeObject<T>(value);
//         }

//     }
// }