using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Repository;
using System.Net;
using raoVatApi.Models;
using raoVatApi.ModelsRaoVat;

namespace Response
{
    public class RootTokenResultType
    {
        JToken _dataResult;
        public JToken DataResult
        {
            get { return this._dataResult; }
            set { this._dataResult = value; }
        }
        public RootTokenResultType(string requestUri, string httpType, object bodyData, string tokenDefaultValue,  params HttpParam[] arrParams)
        {
            string res = "";
            try
            {
                ApiGetCore<JToken> core = new ApiGetCore<JToken>();
                res = core.GetAsyncReturnString(requestUri, httpType, bodyData, tokenDefaultValue, arrParams).Result;

                if (!string.IsNullOrEmpty(res))
                {
                    JObject resObj;
                    try
                    {
                        resObj = JObject.Parse(res);
                    }
                    catch
                    {
                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(res);
                        resObj = JObject.Parse(obj.ToString());
                    }

                    if (resObj != null)
                    {
                        DataResult = resObj.Root;
                    }
                    else { DataResult = null; }
                }
                else
                {
                    DataResult = null;
                }
            }
            catch (Exception ex)
            {
                DataResult = null;
            }
        }

    }
}