// using System.Collections.Generic;
// using Commons;
// using Microsoft.AspNetCore.Http;
// using Newtonsoft.Json;

// public static class SessionExtensions
// {
//     public static void Set<T>(this ISession session, string key, T value)
//     {
//         if (CommonParams.groupKeyAccessArray.Contains(key))
//         {
//             session.SetString(CommonParams.groupKeyAccess, JsonConvert.SerializeObject(new KeyValuePair<string, T>(key, value)));
//         }
//         else
//         {
//             session.SetString(key, JsonConvert.SerializeObject(value));
//         }
//     }

//     public static T Get<T>(this ISession session, string key)
//     {

//         if (CommonParams.groupKeyAccessArray.Contains(key))
//         {
//             Dictionary<string, string> arrValue = CommonMethods.DeserializeObject<Dictionary<string, string>>(session.GetString(CommonParams.groupKeyAccess));
//             return (arrValue == null || arrValue[key] == null) ? default(T) :
//                                  JsonConvert.DeserializeObject<T>(arrValue[key]);
//         }
//         else
//         {
//             var value = session.GetString(key);
//             return value == null ? default(T) :
//                                   JsonConvert.DeserializeObject<T>(value);
//         }

//     }
// }