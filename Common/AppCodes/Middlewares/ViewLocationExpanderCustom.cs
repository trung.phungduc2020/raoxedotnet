﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Middlewares
{
    public class ViewLocationExpanderCustom : IViewLocationExpander
    {

        /// <summary>
        /// Used to specify the locations that the view engine should search to 
        /// locate views.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="viewLocations"></param>
        /// <returns></returns>
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            //{2} is area, {1} is controller,
            /*{0} is: 
             * ViewComponent AutoRender return View("ExecComponent") => {0} = Components/AutoRender/ExecComponent
             
            */
            string viewName = context.ViewName;

            if (viewName.Contains(@"/") && !viewName.Contains(@"NewComponentData") && !viewName.Contains(@"NewComponent"))
            {
                string[] splitViewName = viewName.Split(@"/");
                if (splitViewName != null && splitViewName.Length > 0)
                {
                    int lenght = splitViewName.Length;
                    viewName = splitViewName[lenght - 1];
                }
            }

            string[] locations = new string[] {
                    "~/Areas/RaoVat/Views/{1}/{0}.cshtml",
                    //"~/RaoVat/Views/{1}/ComponentData/{0}.cshtml", //nguyencuongcs 20170930: chuyển xuống DB cho rõ ràng
                    "~/Areas/RaoVat/Views/Shared/{0}.cshtml",
                    //"~/RaoVat/Views/Shared/ComponentData/{0}.cshtml", //nguyencuongcs 20170930: chuyển xuống DB cho rõ ràng
                    "~/Areas/RaoVat/Views/Shared/" + viewName + ".cshtml",
                    "~/Areas/RaoVat/Views/Shared/ComponentData/" + viewName + ".cshtml",
            };

            //locations = locations.Union(viewLocations);          //Add mvc default locations after ours

            return locations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            context.Values["customviewlocation"] = nameof(ViewLocationExpanderCustom);
        }
    }
}
