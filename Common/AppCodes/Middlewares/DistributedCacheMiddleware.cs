﻿
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Threading.Tasks;

namespace raoVatApi.Middlewares
{
    /* nguyencuongcs 20180623
     * Middleware này dùng để truy vấn IDistributedCache kết nối tới Redis mà ko cần thông qua Depenjency Injection
     */
    public class DistributedCacheMiddleware
    {
        private readonly RequestDelegate _next;
        private static IDistributedCache _distributedCache;
        public static IDistributedCache DistributedCache { get => _distributedCache; set => _distributedCache = value; }

        public DistributedCacheMiddleware(RequestDelegate next, IDistributedCache cache)
        {            
            DistributedCache = cache;
            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            return _next(context);
        }

    }
}