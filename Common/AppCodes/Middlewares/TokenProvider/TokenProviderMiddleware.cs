

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using System.IdentityModel.Tokens.Jwt;
using raoVatApi.Models;
using raoVatApi.Common;
using raoVatApi.Response;

namespace raoVatApi.Middlewares
{

    public class TokenProviderMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;

        public TokenProviderMiddleware(
            RequestDelegate next,
            IOptions<TokenProviderOptions> options)
        {
            _next = next;
            _options = options.Value;
        }

        //[EnableCors("CorsPolicy")]
        //[EnableCors("AllowAll")]
        public Task Invoke(HttpContext context)
        {
            Variables.UrlFromClient = Variables.ApiRaoXe.Replace("/api/raovat/", "") + context.Request.Path + context.Request.QueryString;
            // If the request path doesn't match, skip
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                // nguyencuongcs:
                // Sau này có thể dựng hàm Authenticate tập trung ở đây để phân quyền vô các đường dẫn. Thay vì dùng mặc định ở Startup

                if (context.Request.Path.ToString().ToLower().StartsWith("/api/raovat/")
                    && !context.Request.Path.ToString().ToLower().Contains("interfacedata")
                    && !context.Request.Path.ToString().ToLower().Contains("elasticsearch"))
                {
                    // lấy thêm requireRole để kiểm tra là admin hay thành viên
                    string requireRole = context.Request.GetTokenRequireRole();

                    Variables.TokenAuthorization = context.Request.Headers["Authorization"];

                    if (requireRole == CommonParams.policy_requireThanhVienRoleName)
                    {
                        int idThanhVien = context.Request.GetTokenIdThanhVien();

                        bool isDeletedIdThanhVien = idThanhVien < 1 ? true : false;  // Nếu không có idThanhVien trong token thì xem như đã bị xóa

                        if (!isDeletedIdThanhVien)
                        {
                            // Kiểm tra trong biến chứa các id admin bị xóa hoặc trangthai != 1
                            if (Variables.ListDeletedIdThanhVien != null && Variables.ListDeletedIdThanhVien.Count > 0)
                            {
                                // Nếu tồn tại trong ListDeletedIdAdmin => đã bị xóa
                                isDeletedIdThanhVien = Variables.ListDeletedIdThanhVien.Contains(idThanhVien);
                            }
                        }

                        // Tạm thời ko ktra chỗ này vì khi tạo mới admin (post trong admincontroller) chưa lấy lại mới ds ListAvailableIdAdmin (Hữu báo chứ Cường check check)
                        //// Kiểm tra trong biến chứa các id admin đang còn live
                        ///if (!isDeletedIdAdmin)
                        ///{
                        //if (Variables.ListAvailableIdAdmin != null && Variables.ListAvailableIdAdmin.Count > 0)
                        //{
                        //    // Nếu không tồn tại trong ListAvailableIdAdmin => đã bị xóa
                        //    isDeletedIdAdmin = !Variables.ListAvailableIdAdmin.Contains(idAdmin);
                        //}
                        //}

                        if (isDeletedIdThanhVien)
                        {
                            CustomResult cusRes = new CustomResult();
                            cusRes.SetExceptionInvalidAccount();

                            context.Response.ContentType = "application/json";
                            return context.Response.WriteAsync(CommonMethods.SerializeObject(cusRes));
                        }
                    }
                    else { }
                }

                return _next(context);
            }
            // Request must be POST with Content-Type: application/x-www-form-urlencoded
            if (!context.Request.Method.Equals("POST")
               || !context.Request.HasFormContentType)
            {
                context.Response.StatusCode = 400;
                return context.Response.WriteAsync("Bad request.");
            }

            //return GenerateToken(context);
            return context.Response.WriteAsync("System not already work.");
        }
    }
}