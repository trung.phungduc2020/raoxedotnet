
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace raoVatApi.Middlewares
{
    public class TokenProviderOptions
    {
        public string Path { get; set; } = @"/api/RaoVat/token";

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public TimeSpan Expiration { get; set; } = TimeSpan.FromMinutes(30 * 1440);  // 1440: minutes in day

        public SigningCredentials SigningCredentials { get; set; }
    }
}