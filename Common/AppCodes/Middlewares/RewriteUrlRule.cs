﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Rewrite;
using System.Net;
using Microsoft.Net.Http.Headers;
using raoVatApi.Response;
using raoVatApi.Common;
using Microsoft.AspNetCore.Http;

namespace Middlewares
{
    public class RewriteUrlRule : Microsoft.AspNetCore.Rewrite.IRule
    {
        public int StatusCode { get; } = (int)HttpStatusCode.Found; //MovedPermanently
        public bool ExcludeLocalhost { get; set; } = true;

        public void ApplyRule(RewriteContext context)
        {
            var request = context.HttpContext.Request;           

            string hostName = request.Host();
            string appPath = request.PathBase;
            string link = request.RawUrl().ToLower();
            string linkFile = RequestExtensions.GetCurrentAbsolutePathFile(link);            
            //string serverPath = Server.MapPath("~");
            string directoryFolder = string.Empty;
            string currentPath = request.GetCurrentPath();

            ProcessURL pu = new ProcessURL(context.HttpContext);


            if (!CheckRedirectLink(context, currentPath))
            {
                if (!CheckRedirectDomain(context, hostName, link))
                {
                    string res = "";// pu.GetLinkRewrite(link);
                    if (!string.IsNullOrEmpty(res) && res != link)
                    {
                        request.Path = res;
                        context.Result = RuleResult.SkipRemainingRules;
                    }
                }
            }                

            /*
            string newPath = request.Scheme + "://" + CommonFileManager.Driver_DomainNameAndPort host.Value + request.PathBase + request.Path + request.QueryString;
            string newPath = request.Scheme + @"://" + request.Host.Host + @":" + "5005" host.Value + request.PathBase + request.Path + request.QueryString;

            newPath = "/Read";

            if (request.Path == "/")
            {
                // Rewrite path
                request.Path = newPath;
                context.Result = RuleResult.SkipRemainingRules;
                //context.Result = RuleResult.EndResponse; ko được EndResponse để thực thi tiếp request Path mới
            }

            // var response = context.HttpContext.Response;
            // if (request.Path.HasValue && request.Path.Value.ToLower().StartsWith("/image/"))
            // {                
            //     //response.HttpContext.Request.Host = new Microsoft.AspNetCore.Http.HostString("localhost", 5005);
            //     response.StatusCode = 302;
            //     //response.Headers[HeaderNames.ContentLocation] = newPath;
            //     response.Redirect(newPath);
            //     context.Result = RuleResult.EndResponse; // Do not continue processing the request
            // }

            //response.Headers[HeaderNames.Location] = newPath;

            //context.HttpContext.Request.Host = new Microsoft.AspNetCore.Http.HostString("localhost", 8000);
            //context.Result = RuleResult.EndResponse; // Do not continue processing the request        
            */
        }

        public bool CheckRedirectLink(RewriteContext rewriteContext, string link)
        {
            bool res = false;
            Dictionary<string, string> lstLink = null; // InterfaceBLL.GetRedirectLink();

            if (lstLink != null && lstLink.ContainsKey(link))
            {
                res = true;
                Redirect301(rewriteContext, lstLink[link]);
            }

            return res;
        }

        public void Redirect301(RewriteContext rewriteContext, string newLink)
        {
            rewriteContext.HttpContext.Response.StatusCode = 301;
            rewriteContext.HttpContext.Response.Headers[HeaderNames.ContentLocation] = newLink;
            rewriteContext.HttpContext.Response.Redirect(newLink);
            rewriteContext.Result = RuleResult.EndResponse;
        }

        public bool CheckRedirectDomain(RewriteContext rewriteContext, string domain, string link)
        {
            bool res = false;
            string redirectDomain = domain;

            if (Variables.DomainList != null && Variables.DomainList.ContainsKey(domain))
            {
                redirectDomain = Variables.DomainList[domain];
            }

            if (!string.IsNullOrEmpty(redirectDomain) && domain != redirectDomain)
            {
                res = true;
                link = CommonMethods.FormatUrlPath(redirectDomain) + link;
                Redirect301(rewriteContext, link);
            }

            // Check https
            if (!res && CommonMethods.CheckUseSSL())
            {
                string url = rewriteContext.HttpContext.Request.GetAbsoluteUri();
                if (url.StartsWith("http://"))
                {
                    url = rewriteContext.HttpContext.Request.GetCurrentPath();
                    res = true;
                    Redirect301(rewriteContext, url);
                }
            }

            if ((rewriteContext.HttpContext.Request.Host() != "RaoVat.com.vn" && 
                !rewriteContext.HttpContext.Request.Host().EndsWith(".RaoVat.com.vn")) && 
                rewriteContext.HttpContext.Request.Host() != "ceoquang.com")
            {
                Redirect301(rewriteContext, "http://1onnet.com/ban-quyen-web.html");
                return true;
            }

            return res;
        }
    }
}
