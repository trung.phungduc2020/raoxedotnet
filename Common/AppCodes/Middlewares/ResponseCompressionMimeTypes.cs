﻿using System.Collections.Generic;
using System.Linq;
using static Microsoft.AspNetCore.ResponseCompression.ResponseCompressionDefaults;

namespace raoVatApi.Middlewares
{
    public static class ResponseCompressionMimeTypes
    {
        public static IEnumerable<string> Defaults
            => MimeTypes.Concat(new[]
                                {
                                    "image/svg+xml",
                                    "text/plain",
                                    "text/css",
                                    "text/html",
                                    "text/xml",
                                    "application/xml",
                                    "text/json",
                                    "application/json",
                                    "application/javascript",
                                    "application/font-woff2"
                                });
    }
}