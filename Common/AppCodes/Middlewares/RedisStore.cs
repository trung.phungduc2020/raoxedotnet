﻿using System;
using System.Linq;
using StackExchange.Redis;

public class RedisStore
{
    static RedisStore()
    {
    }

    public static ConnectionMultiplexer Connection
    {
        get
        {
            return lazyConnection.Value;
        }
    }

    private static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() => {
        var configurationOptions = new ConfigurationOptions
        {
            EndPoints = { Variables.RedisConnectionString },
            KeepAlive = 10,
            AllowAdmin = true,
            Password = Variables.RedisPassword,
            AbortOnConnectFail = false
        };
        return ConnectionMultiplexer.Connect(configurationOptions);
    });

    public static IDatabase RedisDatabase => Connection.GetDatabase();
    public static IServer RedisServer => Connection.GetServer(Variables.RedisConnectionString);
}