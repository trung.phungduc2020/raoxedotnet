﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

/// <summary>   
public static class TrippleDESLib
{
    /* nguyencuongcs 20181026: key phải đảm bảo đúng 24 byte mới đáp ứng được thuật toán này
     * d@ilyXe@@@2018SanXeTotVN -> ZEBpbHlYZUBAQDIwMThTYW5YZVRvdFZO
    */
    private static string _key = "d@ilyXe@@@2018SanXeTotVN";
    public static string Key
    {
        get
        {
            return _key;
        }
    }

    public static string Encrypt(string plainText, byte[] inputKey, byte[] IV)
    {
        TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
        MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

        byte[] byteHash;
        byte[] byteBuff;

        //byteHash = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes(_key));
        //desCryptoProvider.Key = byteHash;
        desCryptoProvider.Key = inputKey;
        desCryptoProvider.Mode = CipherMode.ECB; //ECBCBC, CFB
        desCryptoProvider.Padding = PaddingMode.PKCS7;

        byteBuff = Encoding.UTF8.GetBytes(plainText);

        string encoded =
            Convert.ToBase64String(desCryptoProvider.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
        return encoded;
    }

    public static string Encrypt(string plainText, string key)
    {
        if (string.IsNullOrEmpty(key))
        {
            // _key đã là dạng base64 nên ko cần EncodeTo64_UTF8 nữa
            key = _key;
        }

        key = StardardizeKeyFor3DES(key, true);
        string encrypted = null;

        try
        {
            // Create 3DES that generates a new key and initialization vector (IV).  
            // Same key must be used in encryption and decryption  
            using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
            {
                tdes.Key = Convert.FromBase64String(key);
                // Encrypt string  
                encrypted = Encrypt(plainText, tdes.Key, tdes.IV);
            }
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
        }
        return encrypted;
    }

    public static string Decrypt(string cipherText, byte[] inputKey, byte[] IV)
    {
        TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
        MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

        byte[] byteHash;
        byte[] byteBuff;

        //byteHash = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes(_key));
        //desCryptoProvider.Key = byteHash;

        desCryptoProvider.Key = inputKey;
        desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB
        desCryptoProvider.Padding = PaddingMode.PKCS7;
        byteBuff = Convert.FromBase64String(cipherText);

        string plaintext = Encoding.UTF8.GetString(desCryptoProvider.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
        return plaintext;
    }

    public static string Decrypt(string cipherText, string key)
    {
        string plainText = "";
        if (string.IsNullOrEmpty(key))
        {            
            key = _key;
        }

        key = StardardizeKeyFor3DES(key, true);

        try
        {
            // Create 3DES that generates a new key and initialization vector (IV).  
            // Same key must be used in encryption and decryption  
            using (TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider())
            {
                tdes.Key = Convert.FromBase64String(key);
                plainText = Decrypt(cipherText, tdes.Key, tdes.IV);
            }
        }
        catch (Exception ex)
        {
            string error = ex.ToString();
        }
        return plainText;
    }

    public static string GetEncryptMD5(string pValue)
    {
        MD5 algorithm = MD5.Create();
        byte[] data = algorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(pValue));
        string result = "";
        for (int i = 0; i < data.Length; i++)
        {
            result += data[i].ToString("x2").ToUpperInvariant();
        }
        return result;
    }

    private static string EncodeTo64_ASCII(string toEncode)
    {
        byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static public string DecodeFrom64_ASCII(string encodedData)
    {
        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
        string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        return returnValue;
    }

    public static string EncodeTo64_UTF8(string toEncode)
    {
        byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(toEncode);
        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static public string DecodeFrom64_UTF8(string encodedData)
    {
        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
        string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
        return returnValue;
    }

    public static string StardardizeKeyFor3DES(string key, bool autoCheckBase64)
    {
        bool isBase64 = IsBase64(key);
        if (isBase64)
        {
            return StardardizeBase64KeyFor3DES(key);
        }
        else
        {
            return StardardizeKeyFor3DES(key);
        }
    }

    public static string StardardizeKeyFor3DES(string key)
    {
        int defaultByteCount = 24;
        string newKey = "";                
        
        int byteCount = System.Text.Encoding.UTF8.GetByteCount(key);
      
        if (byteCount > defaultByteCount)
        {
            key = key.RemoveLastChar();
            newKey = StardardizeKeyFor3DES(key);
        }
        else if (byteCount < defaultByteCount)
        {
            key = key.AddLastChar("@");
            newKey = StardardizeKeyFor3DES(key);
        }
        else
        {
            newKey = key;
        }

        newKey = EncodeTo64_UTF8(newKey);
        return newKey;
    }

    public static string StardardizeBase64KeyFor3DES(string base64Key)
    {
        string newKey = "";
        try
        { 
            newKey = DecodeFrom64_UTF8(base64Key);
            newKey = StardardizeKeyFor3DES(newKey);            
        }
        catch(Exception ex)
        {
            string error = ex.ToString();
        }
        return newKey;        
    }

    public static bool IsBase64(string base64String)
    {
        if (base64String.Replace(" ", "").Length % 4 != 0)
        {
            return false;
        }

        try
        {
            Convert.FromBase64String(base64String);
            return true;
        }
        catch (FormatException exception)
        {
            // Handle the exception
        }
        return false;
    }

    //private public string DecodeFrom64_ASCII(string encodedData)
    //{
    //    byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
    //    string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
    //    return returnValue;
    //}
}