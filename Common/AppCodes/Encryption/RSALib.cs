﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

/// <summary>   
public class RSALib
{
    private RSA _privateKeyRsaProvider;
    private RSA _publicKeyRsaProvider; 
    private HashAlgorithmName _hashAlgorithmName = HashAlgorithmName.SHA256;
    private Encoding _encoding = Encoding.UTF8;

    // nguyencuongcs 20181026: phương thức ToXmlString chưa hỗ trợ trên dotnet core 2.2
    //static RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
    //static string txtPublicKey = rsa.ToXmlString(false);
    //static string txtrivateKey = rsa.ToXmlString(true);
    // Generate online: http://travistidwell.com/jsencrypt/demo/
    private string _rsaPrivateKey = @"MIICXAIBAAKBgHCIiepJqVZwdN+ERYLWUU0+Vte1fHFJvfzbmonAluZxg3A5ETHh2CshAbKSQxwdcGZ4z81kFpCOPX/3ppATjftHNI1jvB84BbG70psjFR/C1SWrswea8YE4Qn801KqJzEWiu7t0IdsAjsXfXKouWxmrfR5F6rn5BpvterfHAmUxAgMBAAECgYA6mI4fpZ18OfBRVqGPQCNzcitsBL84iipYWMz+P38TnTXQoJZ4f928j2I/5nL1V8roQK4Lh7tMKBST5pMfYF9ELgb06/ObIB+lHs3eKgERpBEz5slYKPJPH9+MbjB/4NNd9nFsEvui9m/lwYMZ+DMaKc93tJGRWMsxjh0vqvjTMQJBAMjpjdTQzFZio1XFfudTHyHrpGJTN3YZDGcpptIFbBG1tnOuET7Av9F2mp8Hd/aWACR6oyFNsq+BsBre69fccx0CQQCPY3wRHffRlbGTOjJP8Ylmo5pq9zNO9vz9GQXk4He8UqxOrZ9fcbsMJPke+SUj0IPIstCxkh0q1SpofVc/3yolAkEAhhDzh9NcxzhC7fU/3XTmDWiE5OfK1o+Yay6sFvFvT9IF0eORtmJAnbGbNAtzzaQCrFT5jjuexOaxcPzYO87rMQJAVlASbNNXS+gKHo/XnVvN0+UDnQP09MGkhE49yAp+yPQTGMFWwENbzvGqrERZYPexfGx3R4ym1hO/FWrtzFrvqQJBAKGpI7JXvOQwhyFKIbdWa4mGga5onNGn8XoMcnRxEni62p16MdFBKSw7zgouBW18YteSRZzizlrGeDRU67m/5Ac=";
    private string _rsaPublicKey = @"MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHCIiepJqVZwdN+ERYLWUU0+Vte1fHFJvfzbmonAluZxg3A5ETHh2CshAbKSQxwdcGZ4z81kFpCOPX/3ppATjftHNI1jvB84BbG70psjFR/C1SWrswea8YE4Qn801KqJzEWiu7t0IdsAjsXfXKouWxmrfR5F6rn5BpvterfHAmUxAgMBAAE=";

    private string _rsaPrivateKeyForUI = @"MIICXQIBAAKBgQCAxhFc9tk9SJ8H2dyG9IJf5sfeojG3tpbUE/Jvks1nK+B+8eWmSO4c+jXpgW1k+HK42/gc8jf1tl6ay2lWybF2C65Jef7oJztCPrZy0ZU1WNB/mpI8617d8AsCLH8NJnDhHEk3f6L9Wx4xHtlDUzyOJWJ3tU9FTISSMaQRVw8+JQIDAQABAoGACLFWKrhfIcvtMFJ8mH+Y7XBevaClSomA1QAjtXRreTN1DBy4K+lwaXRaf1DFWbOLyv7OWOXg8S1GZZyll741xHGW1+QtaUsN1KRhY5LrZXdk5hczvwjkqUBlm2pgjPQtFjZDXaE/8QEeTCg4ht3Eva9lpcOQ65N8O2GfoqVjNQECQQDHR46FKexzWc2kSvy0Ap5OWxDcPbgfHVsSrrLCiR4DCo/wPfYf+TkvRyKbAbX5t4Q30h/w0G7b7ZW0kUA65mh5AkEApW0gKcMTSP6jOyuUa2eD5jaepp1M+lXBh8jgxlErgi6gmiFALkUuHusezaPBwmVcFwjeaOlcbzZMsw/KzR9wDQJBAKf2khN/IIKhIIjng1MeGdwlOXLIupXPImH+yUDaXWdm0adGMlsErsRAitRnfr/5hAGgo7dPlwDboaDOBHsglNECQH988TueHl+z9oJkXJbFiQ5Da9NGCQwoSOTnd+r+pURHwPfnxjmikR+83dlnaazyRp1t9VHu/pCqzUN2WTnqD4UCQQCMLe+CRPwc5hpa+bXJBAmy4kukt6dh2OVKUKPGacSXymuh3YjjDNqhzdINl1Rs3K6LvnwUJfX2B4W8gjmNgmw/";
    private string _rsaPublicKeyForUI = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAxhFc9tk9SJ8H2dyG9IJf5sfeojG3tpbUE/Jvks1nK+B+8eWmSO4c+jXpgW1k+HK42/gc8jf1tl6ay2lWybF2C65Jef7oJztCPrZy0ZU1WNB/mpI8617d8AsCLH8NJnDhHEk3f6L9Wx4xHtlDUzyOJWJ3tU9FTISSMaQRVw8+JQIDAQAB";

    public RSA PublicKeyRsaProvider {
        get
        {
            if(_publicKeyRsaProvider == null)
            {
                _publicKeyRsaProvider = CreateRsaProviderFromPublicKey(RsaPublicKey);
            }
            return _publicKeyRsaProvider;
        }        
    }

    public RSA PrivateKeyRsaProvider
    {
        get
        {
            if (_privateKeyRsaProvider == null)
            {
                _privateKeyRsaProvider = CreateRsaProviderFromPrivateKey(_rsaPrivateKey);
            }
            return _privateKeyRsaProvider;
        }
    }

    public string RsaPublicKey { get => _rsaPublicKey; set => _rsaPublicKey = value; }

    /// <summary>      
    public RSALib(bool forUI = false)
    {
        //if (!string.IsNullOrEmpty(privateKey))
        //{
        //    _privateKeyRsaProvider = CreateRsaProviderFromPrivateKey(privateKey);
        //}

        //if (!string.IsNullOrEmpty(publicKey))
        //{
        //    _publicKeyRsaProvider = CreateRsaProviderFromPublicKey(publicKey);
        //}

        //_hashAlgorithmName = rsaType == RSAType.RSA ? HashAlgorithmName.SHA1 : HashAlgorithmName.SHA256;
        if (forUI)
        {            
            _privateKeyRsaProvider = CreateRsaProviderFromPrivateKey(_rsaPrivateKeyForUI);            
            _publicKeyRsaProvider = CreateRsaProviderFromPublicKey(_rsaPublicKeyForUI);
        }
    }
    
    #region

    public string Sign(string data)
    {
        byte[] dataBytes = _encoding.GetBytes(data);

        var signatureBytes = PrivateKeyRsaProvider.SignData(dataBytes, _hashAlgorithmName, RSASignaturePadding.Pkcs1);

        return Convert.ToBase64String(signatureBytes);
    }

    #endregion

    #region

    public bool Verify(string data, string sign)
    {
        byte[] dataBytes = _encoding.GetBytes(data);
        byte[] signBytes = Convert.FromBase64String(sign);

        var verify = PublicKeyRsaProvider.VerifyData(dataBytes, signBytes, _hashAlgorithmName, RSASignaturePadding.Pkcs1);

        return verify;
    }

    #endregion

    #region

    public string Decrypt(string cipherText)
    {
        if (PrivateKeyRsaProvider == null)
        {
            throw new Exception("_privateKeyRsaProvider is null");
        }
        return Encoding.UTF8.GetString(PrivateKeyRsaProvider.Decrypt(Convert.FromBase64String(cipherText), RSAEncryptionPadding.Pkcs1));
    }

    #endregion

    #region

    public string Encrypt(string text)
    {
        if (PublicKeyRsaProvider == null)
        {
            throw new Exception("_publicKeyRsaProvider is null");
        }
        return Convert.ToBase64String(PublicKeyRsaProvider.Encrypt(Encoding.UTF8.GetBytes(text), RSAEncryptionPadding.Pkcs1));
    }

    #endregion

    #region

    public RSA CreateRsaProviderFromPrivateKey(string privateKey)
    {
        privateKey = privateKey.Replace("\r\n", "").Replace(" ", "");
        var privateKeyBits = Convert.FromBase64String(privateKey);

        var rsa = RSA.Create();
        var rsaParameters = new RSAParameters();

        using (BinaryReader binr = new BinaryReader(new MemoryStream(privateKeyBits)))
        {
            byte bt = 0;
            ushort twobytes = 0;
            twobytes = binr.ReadUInt16();
            if (twobytes == 0x8130)
                binr.ReadByte();
            else if (twobytes == 0x8230)
                binr.ReadInt16();
            else
                throw new Exception("Unexpected value read binr.ReadUInt16()");

            twobytes = binr.ReadUInt16();
            if (twobytes != 0x0102)
                throw new Exception("Unexpected version");

            bt = binr.ReadByte();
            if (bt != 0x00)
                throw new Exception("Unexpected value read binr.ReadByte()");

            rsaParameters.Modulus = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.Exponent = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.D = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.P = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.Q = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.DP = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.DQ = binr.ReadBytes(GetIntegerSize(binr));
            rsaParameters.InverseQ = binr.ReadBytes(GetIntegerSize(binr));
        }

        rsa.ImportParameters(rsaParameters);
        return rsa;
    }

    #endregion

    #region

    public RSA CreateRsaProviderFromPublicKey(string publicKey)
    {
        // encoded OID sequence for  PKCS #1 rsaEncryption szOID_RSA_RSA = "1.2.840.113549.1.1.1"
        byte[] seqOid = { 0x30, 0x0D, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x01, 0x01, 0x01, 0x05, 0x00 };
        byte[] seq = new byte[15];

        publicKey = publicKey.Replace("\r\n","").Replace(" ", "");
        var x509Key = Convert.FromBase64String(publicKey);

        // ---------  Set up stream to read the asn.1 encoded SubjectPublicKeyInfo blob  ------
        using (MemoryStream mem = new MemoryStream(x509Key))
        {
            using (BinaryReader binr = new BinaryReader(mem))  //wrap Memory Stream with BinaryReader for easy reading
            {
                byte bt = 0;
                ushort twobytes = 0;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                seq = binr.ReadBytes(15);       //read the Sequence OID
                if (!CompareBytearrays(seq, seqOid))    //make sure Sequence for OID is correct
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8103) //data read as little endian order (actual data order for Bit String is 03 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8203)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                bt = binr.ReadByte();
                if (bt != 0x00)     //expect null byte next
                    return null;

                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130) //data read as little endian order (actual data order for Sequence is 30 81)
                    binr.ReadByte();    //advance 1 byte
                else if (twobytes == 0x8230)
                    binr.ReadInt16();   //advance 2 bytes
                else
                    return null;

                twobytes = binr.ReadUInt16();
                byte lowbyte = 0x00;
                byte highbyte = 0x00;

                if (twobytes == 0x8102) //data read as little endian order (actual data order for Integer is 02 81)
                    lowbyte = binr.ReadByte();  // read next bytes which is bytes in modulus
                else if (twobytes == 0x8202)
                {
                    highbyte = binr.ReadByte(); //advance 2 bytes
                    lowbyte = binr.ReadByte();
                }
                else
                    return null;
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };   //reverse byte order since asn.1 key uses big endian order
                int modsize = BitConverter.ToInt32(modint, 0);

                int firstbyte = binr.PeekChar();
                if (firstbyte == 0x00)
                {   //if first byte (highest order) of modulus is zero, don't include it
                    binr.ReadByte();    //skip this null byte
                    modsize -= 1;   //reduce modulus buffer size by 1
                }

                byte[] modulus = binr.ReadBytes(modsize);   //read the modulus bytes

                if (binr.ReadByte() != 0x02)            //expect an Integer for the exponent data
                    return null;
                int expbytes = (int)binr.ReadByte();        // should only need one byte for actual exponent data (for all useful values)
                byte[] exponent = binr.ReadBytes(expbytes);

                // ------- create RSACryptoServiceProvider instance and initialize with public key -----
                var rsa = RSA.Create();
                RSAParameters rsaKeyInfo = new RSAParameters
                {
                    Modulus = modulus,
                    Exponent = exponent
                };
                rsa.ImportParameters(rsaKeyInfo);

                return rsa;
            }

        }
    }

    #endregion

    #region

    private int GetIntegerSize(BinaryReader binr)
    {
        byte bt = 0;
        int count = 0;
        bt = binr.ReadByte();
        if (bt != 0x02)
            return 0;
        bt = binr.ReadByte();

        if (bt == 0x81)
            count = binr.ReadByte();
        else
        if (bt == 0x82)
        {
            var highbyte = binr.ReadByte();
            var lowbyte = binr.ReadByte();
            byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
            count = BitConverter.ToInt32(modint, 0);
        }
        else
        {
            count = bt;
        }

        while (binr.ReadByte() == 0x00)
        {
            count -= 1;
        }
        binr.BaseStream.Seek(-1, SeekOrigin.Current);
        return count;
    }

    private bool CompareBytearrays(byte[] a, byte[] b)
    {
        if (a.Length != b.Length)
            return false;
        int i = 0;
        foreach (byte c in a)
        {
            if (c != b[i])
                return false;
            i++;
        }
        return true;
    }

    #endregion

}

public enum RSAType
{
    /// <summary>
    /// SHA1
    /// </summary>
    RSA = 0,
    /// <summary>
    /// RSA2
    /// SHA256
    /// </summary>
    RSA2
}
