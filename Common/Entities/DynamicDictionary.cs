﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace raoVatApi.Models
{
    // The class derived from DynamicObject.
    public class DynamicDictionary : DynamicObject
    {
        // The inner dictionary.
        private Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        private Dictionary<string, object> _dictionaryToLower = new Dictionary<string, object>();

        public Dictionary<string, object> Dictionary
        {
            get
            {
                return _dictionary;
            }
        }

        public KeyValuePair<string, object> GetMember(string memberName)
        {
            string memberNameToLower = memberName.NullIsEmpty().ToLower();
            return new KeyValuePair<string, object>(memberName, _dictionaryToLower.ContainsKey(memberNameToLower) ? _dictionaryToLower[memberNameToLower] : "Member not found!");
        }

       
        public object GetMemberValue(string memberName)
        {
            string memberNameToLower = memberName.NullIsEmpty().ToLower();
            return _dictionaryToLower.ContainsKey(memberNameToLower) ? _dictionaryToLower[memberNameToLower] : null;
        }

        public object GetValue(string memberName)
        {
            return GetMemberValue(memberName);
        }

        public object this[string memberName]
        {
            get
            {
                return GetMemberValue(memberName);
            }            
        }
      
        // This property returns the number of elements
        // in the inner dictionary.
        public int Count
        {
            get
            {
                return Dictionary.Count;
            }
        }

        // If you try to get a value of a property 
        // not defined in the class, this method is called.
        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            string name = binder.Name.ToLower();

            // If the property name is found in a dictionary,
            // set the result parameter to the property value and return true.
            // Otherwise, return false.
            return _dictionaryToLower.TryGetValue(name, out result);
        }

        // If you try to set a value of a property that is
        // not defined in the class, this method is called.
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.

            Dictionary[binder.Name] = value;
            _dictionaryToLower[binder.Name.ToLower()] = value;

            // You can always add a value to a dictionary,
            // so this method always returns true.
            return true;
        }

        public bool TrySetMember(
            string memberName, object value)
        {
            Dictionary[memberName] = value;
            _dictionaryToLower[memberName.ToLower()] = value;
            return true;
        }
    }
}
