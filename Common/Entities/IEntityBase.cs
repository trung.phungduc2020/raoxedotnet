using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.Models
{
    public interface IEntityBase
    {        
        // int Id { get; set; }
        
        [NotMapped]
        int TotalRow { get; set; }

        [NotMapped]
        IEnumerable<string> PropertyList { get; set; }
    }
}