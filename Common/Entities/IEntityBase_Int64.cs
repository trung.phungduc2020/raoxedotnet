using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace raoVatApi.Models
{
    public interface IEntityBase_Int64
    {
        // int Id { get; set; }

        [NotMapped]
        long TotalRow { get; set; }
    }
}