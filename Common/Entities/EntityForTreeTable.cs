using raoVatApi.Common;
using raoVatApi.Models;
using raoVatApi.Models.Base;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace raoVatApi.Models
{
    public partial class EntityForTreeTable<T> where T : class, IEntityBase
    {

        [NotMapped]
        public int TotalRow { get; set; }

        [NotMapped]
        public int Id { get; set; }

        [NotMapped]
        public T data { get; set; } // Co tinh viet thuong de ku Huu de xu ly

        [NotMapped]
        public EntityForTreeTable<T>[] children { get; set; }  // Co tinh viet thuong de ku Huu de xu ly

        [NotMapped]
        private Expression<Func<T, bool>> ChildrenSearchTerms { get; set; }

        private IEntityBaseRepository<T> _repo;
        [NotMapped]
        IEntityBaseRepository<T> Repo
        {
            get
            {
                if (_repo == null)
                {
                    HttpContextAccessor accessor = new HttpContextAccessor();
                    _repo = new EntityBaseRepository<T>(accessor.HttpContext.Request);
                }
                return _repo;
            }
            set
            {
                _repo = value;
            }
        }

        public EntityForTreeTable()
        {

        }

        public EntityForTreeTable(IEntityBaseRepository<T> repo, long id, int totalRow, Expression<Func<T, bool>> childrenSearchTerms)
        {
            Repo = repo;
            ChildrenSearchTerms = childrenSearchTerms;
            T entity = Repo.GetSingle(id).Result;
            TotalRow = totalRow;

            if (entity != null)
            {
                BuildTForTreeTable(entity);
            }
        }

        /*
         * nguyencuongcs 20180801: bổ sung hàm này để dùng nguyên source data truyền vào, dùng để build giả lập data dạng tree (children  sẽ luôn = null)
         * 1. Tiết kiệm lượt gọi Repo.GetSingle(id).Result;
         * 2. Lấy được các giá trị khóa ngoại trong source data đã build. Ví dụ: RowIndex & DisplayPage của files khi tìm theo idSearchRowIndex
         * 3. Dùng dạng này thì childrenT = Repo.GetDataByIdParent
         */
        public EntityForTreeTable(IEnumerable<T> srcData, long id, int totalRow, Expression<Func<T, bool>> childrenSearchTerms)
        {
            Repo = null;
            ChildrenSearchTerms = childrenSearchTerms;
            T entity = srcData != null ? srcData.Where(s => CommonMethods.ConvertToInt32(s.GetValue("Id")) == id).FirstOrDefault() : null;
            TotalRow = totalRow;

            if (entity != null)
            {
                BuildTForTreeTable(entity);
            }
        }

        public void BuildTForTreeTable(T entity)
        {
            if (entity != null)
            {
                Id = CommonMethods.ConvertToInt32(entity.GetValue("Id"));
                data = entity;
                IEnumerable<T> childrenT = null;
                if (Repo != null)
                {
                    childrenT = Repo.GetDataByIdParent(CommonMethods.ConvertToInt32(entity.GetValue("Id")), ChildrenSearchTerms).Result;

                    if (childrenT != null)
                    {
                        int count = childrenT.Count();
                        if (children == null)
                        {
                            children = new EntityForTreeTable<T>[count];
                        }

                        int i = 0;
                        int childrenId = -1;
                        int totalRow = 0;
                        foreach (T item in childrenT)
                        {
                            childrenId = CommonMethods.ConvertToInt32(item.GetValue("Id"));
                            totalRow = CommonMethods.ConvertToInt32(item.GetValue("TotalRow"));
                            if (childrenId != Id)
                            {
                                children[i] = (new EntityForTreeTable<T>(Repo, childrenId, totalRow, ChildrenSearchTerms));
                            }
                            i++;
                        }
                    }
                }
            }
        }
    }
}