using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using raoVatApi.Models;
using Microsoft.AspNetCore.Http;

namespace raoVatApi.Models.Base
{
    public interface IEntityBaseRepository<T> where T : class, IEntityBase
    {
        HttpRequest Request { get; set; }
        DbContext Context { get; set; }
        DbSet<T> Entities { get; set; }
        int TotalPages(int totalRows, int displayItems);
        Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> searchTerm, int currentPage, int displayItems, string orderString, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, int currentPage, int displayItems, string orderString, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, string orderString, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetDataByIdParent(long idParent, Expression<Func<T, bool>> predicate);
        Task<T> GetSingle(long id);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<int> Insert(T entity);
        Task<IEnumerable<T>> InsertReturnEntity(T entity);
        Task<int> UpdateAfterAutoClone(T newEntity);
        Task<T> CloneEntityToUpdate(T dbEntity, T newEntity);
        Task<T> CloneEntityToUpdateFromDic<X>(T dbEntity, X sourceEntity);
        Task<int> UpdateWhere(Expression<Func<T, bool>> predicate, Action<T> updateVoid, bool setIdAdminCapNhat = true);
        Task<int> Delete(T entity);
        Task<int> Delete(T[] entity);
        Task<int> Delete(IEnumerable<T> entity);
        Task<int> DeleteWhere(Expression<Func<T, bool>> predicate);
        Task<int> DeleteMultiIds(string ids, char splitChar = ',');
        Task<int> DeleteMultiIds(List<int> lstIds);
        Task<int> Commit();
        //Task UpdateSeoPage(T entity);
        Task<int> GetMaxId();
    }
}