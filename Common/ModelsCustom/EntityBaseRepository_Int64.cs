using raoVatApi.Common;
using raoVatApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace raoVatApi.Models.Base
{
    public class EntityBaseRepository_Int64<T> : IEntityBaseRepository_Int64<T> where T : class, IEntityBase_Int64
    {
        public virtual long CountAfterSearch { get; set; }

        public virtual IEnumerable<T> SetTotalRow(IEnumerable<T> data)
        {
            if (data != null)
            {                
                foreach (T item in data)
                {
                    item.TotalRow = CountAfterSearch;
                }
            }
            return data;
        }        

        private DbContext _context;

        public DbContext Context
        {
            get
            {               
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        private DbSet<T> _entities;

        public DbSet<T> Entities
        {
            get
            {
                return this._entities;
            }
            set
            {
                _entities = value;
            }
        }

        string errorMessage = string.Empty;

        public EntityBaseRepository_Int64()
        {           
        }

        public EntityBaseRepository_Int64(DbContext context)
        {
            _context = context;            

            _entities = _context.Set<T>();
        }
        
        public virtual long TotalPages(long totalRows, int displayItems)
        {
            return (long)Math.Round((double)totalRows / displayItems, 0, MidpointRounding.AwayFromZero);
        }

        public virtual async Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> searchTerm, long currentPage = 1, int displayItems = -1, string orderString = "", params Expression<Func<T, object>>[] includeProperties)
        {
            IEnumerable<T> res = await Search(searchTerm, orderString, includeProperties);

            if (currentPage > 0 && displayItems > 0)
            {
                res = Paging(res, currentPage, displayItems);
            }
            else
            {
                res = SetTotalRow(res);
            }

            return res;
        }

        public virtual IEnumerable<T> Paging(IEnumerable<T> data, long currentPage, int displayItems)
        {
            if (data != null && data.Count() > 0)
            {
                if (currentPage < 1)
                {
                    currentPage = 1;
                }

                long bigNumber = (currentPage - 1) * displayItems;
                while (bigNumber > int.MaxValue)
                {
                    data = data.Skip(int.MaxValue);
                    bigNumber -= int.MaxValue;
                }

                data = data.Skip(CommonMethods.ConvertToInt32(bigNumber));

                if (displayItems > 0)
                {
                    data = data.Take(displayItems).AsEnumerable();
                }

                data = SetTotalRow(data);
                return data;
            }

            return new List<T>().AsEnumerable(); ;
        }

        public virtual async Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, string orderString = "", params Expression<Func<T, object>>[] includeProperties)
        {
            IEnumerable<T> res = null;
            if (predicate != null)
            {
                string includePropertyNameAsString = string.Empty;
                if (includeProperties != null)
                {
                    string temp = string.Empty;
                    foreach (var includeProperty in includeProperties)
                    {
                        try
                        {
                            temp = CommonMethods.ConvertToString(includeProperty.Body.GetValue("Member").GetValue("Name"));
                        }
                        catch
                        {
                            temp = string.Empty;
                        }

                        if (!includePropertyNameAsString.Contains(temp))
                        {
                            includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? temp : ("." + temp);
                        }
                    }
                }

                List<string> lstEntityFromOrder = CommonMethods.GetIncludePropertyFromOrderString(orderString);
                string coalesceEntityFrmOrder = string.Empty;

                if (lstEntityFromOrder != null)
                {
                    coalesceEntityFrmOrder = string.Join(".", lstEntityFromOrder.ToArray());
                }

                if (!string.IsNullOrEmpty(coalesceEntityFrmOrder))
                {
                    string[] split = coalesceEntityFrmOrder.Split('.');
                    if (split != null)
                    {
                        for (int i = 0; i < split.Length; i++)
                        {
                            if (!includePropertyNameAsString.Contains(split[i]))
                            {
                                includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? split[i] : ("." + split[i]);
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(includePropertyNameAsString))
                {
                    res = await _entities.Where(predicate).Include(includePropertyNameAsString).ToListAsync();
                }
                else
                {
                    res = await _entities.Where(predicate).ToListAsync();
                }

                if (res != null)
                {
                    if (!string.IsNullOrEmpty(orderString))
                    {
                        res = res.AsQueryable().ExecuteOrderExpression(orderString);
                    }

                    // async ko chạy được dạng này
                    //if (includeProperties != null)
                    //{                        
                    //    foreach (var includeProperty in includeProperties)
                    //    {
                    //        res = res.AsQueryable().Include(includeProperty);
                    //    }
                    //}
                }
                CountAfterSearch = res != null ? res.LongCount() : 0;
            }

            return res;
        }

        public virtual async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await _entities.Where(predicate).ToListAsync();
        }

        public virtual async Task<T> GetSingle(long id)
        {
            return await _entities.SingleOrDefaultAsync(s => CommonMethods.ConvertToInt64(s.GetValue(Id)) == id);
        }

        public virtual async Task<T> GetSingle(Expression<Func<T, bool>> predicate)
        {
            return await _entities.FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<T> GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            T res = null;
            string includePropertyNameAsString = string.Empty;
            if (includeProperties != null)
            {
                string temp = string.Empty;
                foreach (var includeProperty in includeProperties)
                {
                    try
                    {
                        temp = CommonMethods.ConvertToString(includeProperty.Body.GetValue("Member").GetValue("Name"));
                    }
                    catch
                    {
                        temp = string.Empty;
                    }

                    if (!includePropertyNameAsString.Contains(temp))
                    {
                        includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? temp : ("." + temp);
                    }
                }
            }

            if (!string.IsNullOrEmpty(includePropertyNameAsString))
            {
                res = await _entities.Where(predicate).Include(includePropertyNameAsString).FirstOrDefaultAsync();
            }
            else
            {
                res = await _entities.Where(predicate).FirstOrDefaultAsync();
            }

            return res;
        }

        public virtual async Task<IEnumerable<T>> InsertReturnEntity(T entity)
        {
            long newId = await Insert(entity);
            return new List<T>() { await GetSingle(newId) }.AsEnumerable();
        }

        public virtual async Task<long> Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            await _entities.AddAsync(entity);

            long res = -1;

            if (await this.Commit() > 0)
            {
                Context.Entry<T>(entity).GetDatabaseValues();
                res = CommonMethods.ConvertToInt64(entity.GetValue(Id));
            }

            return res;
        }
        
        public virtual async Task<long> UpdateAfterAutoClone(T newEntity)
        {
            long res = -1;

            if (newEntity != null)
            {
                long id = CommonMethods.ConvertToInt32(newEntity.GetValue(Id));
                T dbEntity = GetSingle(id).Result; // get db entity
                if (dbEntity != null)
                {
                    dbEntity = await CloneEntityToUpdate(dbEntity, newEntity); // update to dbentity
                    res = await this.Commit(); // await save to db
                }
            }
            return res;
        }

        private const string Id = "Id";

        private string[] propertyKeys = { Id };

        public virtual async Task<T> CloneEntityToUpdate(T dbEntity, T newEntity)
        {
            try
            {
                if (dbEntity != null && newEntity != null)
                {
                    var properties = typeof(T).GetProperties();

                    if (properties != null)
                    {
                        object newPropValue;

                        foreach (var propInfo in properties)
                        {
                            if (!propertyKeys.Contains(propInfo.Name))
                            {
                                newPropValue = newEntity.GetValue(propInfo.Name);
                                if (dbEntity.HasProperty(propInfo.Name) && newPropValue != null)
                                {
                                    dbEntity.SetValue(propInfo.Name, newPropValue);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dbEntity;
        }

        public virtual async Task<long> UpdateWhere(Expression<Func<T, bool>> predicate, Action<T> updateVoid)
        {
            IEnumerable<T> entities = await _entities.Where(predicate).ToListAsync();

            long res = 0;
            foreach (var entity in entities)
            {
                // Cập nhật dữ liệu mới vào entity
                updateVoid(entity);

                // Lưu xuống database
                res += this.Commit().Result;// thay cho await UpdateAfterAutoClone(entity);
            }

            return res;
        }

        public virtual async Task<long> Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _entities.Remove(entity);
            return await this.Commit();
        }

        public virtual async Task<long> DeleteWhere(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> entities = _entities.Where(predicate);

            foreach (var entity in entities)
            {
                Context.Entry<T>(entity).State = EntityState.Deleted;
            }

            return await this.Commit();
        }

        public virtual async Task<long> DeleteMultiIds(string ids, char splitChar = ',')
        {

            List<long> lstId = CommonMethods.SplitStringToInt64(ids);
            return await DeleteWhere(s => lstId.Contains(CommonMethods.ConvertToInt64(s.GetValue(Id))));
        }

        public virtual async Task<long> DeleteMultiIds(List<long> lstIds)
        {
            return await DeleteWhere(s => lstIds.Contains(CommonMethods.ConvertToInt64(s.GetValue(Id))));
        }

        public virtual async Task<long> Commit()
        {
            return await Context.SaveChangesAsync();
        }

        /* Extend methods *//* Extend methods */
    }
}