﻿using raoVatApi.Common;
using raoVatApi.Middlewares;
using raoVatApi.Models;
using raoVatApi.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Data.Common;
using Npgsql;
using System.Text;
using raoVatApi.ModelsRaoVat;
using raoVatHistoryApi.ModelsRaoVat;
using GmailApi;

namespace raoVatApi.Models.Base
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, IEntityBase
    {


        private string[] propertyKeys = { PropertyParams.Id };

        private string _cachedFromEntityBase = "FromEntityBase";
        protected bool EnabledCache = false;
        protected bool EnabledAutoClearCacheAfterChanged = true; // nguyencuongcs 20180710: bật lên bằng true để tự clearcache khi thêm/sửa/xóa. Đã xử lý gọi clear cache ở web farms
        // nguyencuongcs 20180624: set mặc định để không cache như cũ, cần test nhiều hơn        
        // Phải xử lý được ko cache toàn bộ Admin, có thể dùng bảng định nghĩa hoặc prefix AdminBll hoặc NoCache...  

        // nguyencuongcs 20180627: Tạo riêng EntityBaseRepositoryRaoVatForClient bặt EnabledCache = false
        protected int CacheRule = (int)CommonMethods.CacheRule.KhongCache;
        private IDistributedCache _distributedCache;
        protected IDistributedCache DistributedCache
        {
            get
            {
                // nguyencuongcs 20180627: đang nghi ngờ do cái này mà đơ khi clear all cache
                //if (_distributedCache == null)
                {
                    try
                    {
                        _distributedCache = DistributedCacheMiddleware.DistributedCache;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                    }
                }

                return _distributedCache;
            }
            set => _distributedCache = value;
        }

        public virtual int CountAfterSearch { get; set; }

        public virtual string connectionDb { get; set; }

        public virtual IEnumerable<T> SetTotalRow(IEnumerable<T> data)
        {
            // nguyencuongcs 20180622: có thể tăng tốc bằng cách ktra nếu count > 500 thì chạy đa luồng để tăng tốc
            // Tương tự GetFilesize_IdFilesAsync trong CommonFileController
            // Hiện giờ thấy chưa cần thiết
            if (data != null)
            {
                int count = 0;

                foreach (T item in data)
                {
                    item.TotalRow = CountAfterSearch;

                    if (count == 0)
                    {
                        //item.PropertyList = CommonMethods.GetPropertyNameList(typeof(T));
                    }

                    count++;
                }
            }
            return data;
        }
        MemoryCacheControllerBase _cacheController;

        //private IEnumerable<string> PropertyNameList { get; set; }

        private DbContext _context;

        public DbContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        private DbSet<T> _entities;

        public DbSet<T> Entities
        {
            get
            {
                return this._entities;
            }
            set
            {
                this._entities = value;
            }
        }

        private HttpRequest _request;
        public HttpRequest Request
        {
            get
            {
                if (_request == null)
                {
                    HttpContextAccessor access = new HttpContextAccessor();
                    _request = access.HttpContext.Request;
                }
                return this._request;
            }
            set
            {
                this._request = value;
            }
        }

        protected MemoryCacheControllerBase CacheController { get => _cacheController; set => _cacheController = value; }

        string errorMessage = string.Empty;

        public EntityBaseRepository(HttpRequest request)
        {
            Request = request;
            CacheController = new MemoryCacheControllerBase();
            //string basedd = "test how many times initial EntityBase foreach request";
        }

        public EntityBaseRepository(DbContext context, HttpRequest request)
        {
            Context = context;
            Entities = Context.Set<T>();
            Request = request;
            CacheController = new MemoryCacheControllerBase();
        }

        public EntityBaseRepository()
        {
        }

        public virtual int TotalPages(int totalRows, int displayItems)
        {
            return (int)Math.Round((decimal)totalRows / displayItems, 0, MidpointRounding.AwayFromZero);
        }

        public virtual async Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> searchTerm, int currentPage = 1, int displayItems = -1, string orderString = "",
            params Expression<Func<T, object>>[] includeProperties)
        {
            try
            {
                IEnumerable<T> res = await Search(searchTerm, currentPage, displayItems, orderString, includeProperties);

                res = SetTotalRow(res);

                return res;

            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Exception ", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public virtual async Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, int currentPage, int displayItems, string orderString, params Expression<Func<T, object>>[] includeProperties)
        {
            try
            {
                //int count = 0;
                //if (typeof(T).Name.ToLower() == "files")
                //{
                //    count++;
                //}
                IEnumerable<T> res = null;
                if (predicate != null)
                {
                    // Nghiên cứu dùng để build tổng quát tongHopBien từ predicate
                    //var methodInfo = ((MethodCallExpression)predicate.Body).Method;
                    //var names = methodInfo.GetParameters().Select(pi => pi.Name);

                    string includePropertyNameAsString = string.Empty;
                    if (includeProperties != null)
                    {
                        string temp = string.Empty;
                        foreach (var includeProperty in includeProperties)
                        {
                            try
                            {
                                temp = CommonMethods.ConvertToString(includeProperty.Body.GetValue("Member").GetValue("Name"));
                            }
                            catch
                            {
                                temp = string.Empty;
                            }

                            if (!includePropertyNameAsString.Contains(temp))
                            {
                                includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? temp : ("." + temp);
                            }
                        }
                    }

                    List<string> lstEntityFromOrder = CommonMethods.GetIncludePropertyFromOrderString(orderString);
                    string coalesceEntityFrmOrder = string.Empty;

                    if (lstEntityFromOrder != null)
                    {
                        coalesceEntityFrmOrder = string.Join(".", lstEntityFromOrder.ToArray());
                    }

                    if (!string.IsNullOrEmpty(coalesceEntityFrmOrder))
                    {
                        string[] split = coalesceEntityFrmOrder.Split('.');
                        if (split != null)
                        {
                            for (int i = 0; i < split.Length; i++)
                            {
                                if (!includePropertyNameAsString.Contains(split[i]))
                                {
                                    includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? split[i] : ("." + split[i]);
                                }
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(orderString))
                    {
                        orderString = PropertyParams.Id;
                    }

                    // Calculate paging
                    if (currentPage < 1)
                    {
                        currentPage = 1;
                    }
                    int skipItem = (currentPage - 1) * displayItems;

                    /* nguyencuongcs 20180623: Cache toàn hệ thống theo quy tắc 
                     * 1. Không cache, dùng cho tác vụ Admin hoặc dành cho các bảng thống kê realtime (như ThongKeTruyCap hoặc có thể tối ưu hơn là dùng cách 2 nhưng cài triggerApiService tự clear cache mỗi 10" (= thời gian hết session))
                     * 2 (Default). Cache toàn bộ trước khi search & paging: dành cho các bảng dự báo ít data, khoảng dưới 5000 row là hợp lý
                     * 3. Chỉ cache theo điều kiện search & paging sau khi đã lấy từ db vì dữ liệu rất lớn, chấp nhận query nhiều lần vào db cho mỗi bộ search & paging, ví dụ: bảng Files, Tintuc                 
                     */
                    // includePropertyNameAsString hiện tại ko sử dụng Include dạng này nữa vì dư thừa không cần thiết, sau 1 thời gian sẽ xem xét xóa bỏ
                    if (!string.IsNullOrEmpty(includePropertyNameAsString))
                    {

                        res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).Include(includePropertyNameAsString).ToListAsync();
                        CountAfterSearch = res != null ? res.Count() : 0;

                        if (displayItems > 0)
                        {
                            // nguyencuongcs 20180624: chấp nhận đi theo hướng cũ (tìm tất cả trước khi phân trang) để lấy được TotalRow: CountAfterSearch = res != null ? res.Count() : 0;                        
                            res = res != null ? res.AsQueryable().Skip(skipItem).Take(displayItems).AsEnumerable() : null;
                        }
                    }
                    else
                    {
                        if (!EnabledCache)
                        {
                            CountAfterSearch = await Entities.Where(predicate).CountAsync();

                            if (displayItems > 0)
                            {

                                res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).Skip(skipItem).Take(displayItems).ToListAsync();
                            }
                            else
                            {
                                res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).ToListAsync();
                            }
                        }
                        else
                        {
                            // CacheRule 1.
                            if (DistributedCache == null || CacheRule == (int)CommonMethods.CacheRule.KhongCache)
                            {
                                CountAfterSearch = await Entities.Where(predicate).CountAsync();

                                if (displayItems > 0)
                                {

                                    res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).Skip(skipItem).Take(displayItems).ToListAsync();
                                }
                                else
                                {
                                    res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).ToListAsync();
                                }
                            }
                            // nguyencuongcs 20180712: đã tìm được cách tính count ko cần pull data lên CountAfterSearch = await Entities.Where(predicate).CountAsync();
                            // Nên kỳ vọng cache toàn bộ theo dạng CacheTheoDieuKien => cẩn xử lý được predicate => chuỗi condition
                            // CacheRule 2.
                            else if (CacheRule == (int)CommonMethods.CacheRule.CacheToanBo)
                            {
                                string keyCache = KeyCacheParams.BuildKeyCacheByVariables<T>(_cachedFromEntityBase);

                                // Lấy data từ cache trước xem có không
                                object cacheValue = await CacheController.GetCacheByKey(keyCache);
                                if (cacheValue != null)
                                {
                                    res = CommonMethods.DeserializeObject<IEnumerable<T>>(cacheValue);
                                }
                                // Nếu không có lấy từ DB & lưu vào cache
                                else
                                {
                                    res = await Entities.ToListAsync();
                                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
                                }

                                res = res != null ? res.AsQueryable().Where(predicate).ExecuteOrderExpression(orderString).AsEnumerable() : new List<T>();
                                // Get TotalCount theo điều kiện predicate
                                CountAfterSearch = res != null ? res.Count() : 0;

                                // Phân trang
                                if (displayItems > 0)
                                {
                                    res = res != null ? res.AsQueryable().Skip(skipItem).Take(displayItems).AsEnumerable() : new List<T>();
                                }
                            }
                            // CacheRule 3. Điều kiện là predicate,currentPage,displayItems,orderString
                            // Trường hợp này không cần get CountAfterSearch vì Skip & Take trực tiếp trước khi lấy data nên CountAfterSearch lấy được không đúng là tổng Row nữa
                            // Vậy trường hợp CacheTheoDieuKien chỉ dùng nếu lấy theo Id cho nhanh hoặc ko cần Total Row (thường ở giao diện frontend)
                            else if (CacheRule == (int)CommonMethods.CacheRule.CacheTheoDieuKien)
                            {
                                // Todo: predicate.ToString() hiện tại luôn trả về luôn luôn giống nhau => lưu cache trùng                            

                                string md5Predicate = CommonMethods.GetEncryptMD5(predicate.ToString());
                                string tongHopBien = CommonMethods.CoalesceParams(KeyCacheParams.coalesceString, _cachedFromEntityBase, md5Predicate, currentPage, displayItems, orderString);
                                string keyCache = KeyCacheParams.BuildKeyCacheByVariables<T>(tongHopBien);

                                // Lấy data từ cache trước xem có không
                                object cacheValue = await CacheController.GetCacheByKey(keyCache);
                                if (cacheValue != null)
                                {
                                    res = CommonMethods.DeserializeObject<IEnumerable<T>>(cacheValue);
                                }
                                // Nếu không có lấy từ DB & lưu vào cache
                                else
                                {
                                    // Thực thi tìm kiếm & sắp xếp                                
                                    if (displayItems > 0)
                                    {
                                        res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).Skip(skipItem).Take(displayItems).ToListAsync();
                                    }
                                    else
                                    {
                                        res = await Entities.Where(predicate).ExecuteOrderExpression(orderString).ToListAsync();
                                    }
                                    await CacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
                                }
                            }
                        }
                    }
                }

                // chồng chéo: 2 luồng use 1 dbcontext lỗi -> tạo cái mới
                // 25/12/2020: Tr test
                //RawSqlRepository rawSql = new RawSqlRepository(Context);
                //rawSql.KillConnection();
                //(Context as RaoVatContext_Custom).KillConnection();
                //KillConnection();
                return res;
            }
            catch (Exception ex)
            {
                //Console.Write("ERROR Search");
                //MailService ms = new MailService();
                //foreach (var email in Variables.EmailSupportError_Name)
                //{
                //    ms.SendEmailToDev("Error: Search", Variables.UrlFromClient + "-" + ex.Message, email);
                //}
                // Ready to delete if above worked
                //(Context as RaoVatContext_Custom).KillConnection();
                KillConnection();
                return null;
            }
        }

        public virtual async Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, string orderString, params Expression<Func<T, object>>[] includeProperties)
        {
            return await Search(predicate, -1, -1, orderString, includeProperties);
        }

        public virtual async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await Entities.Where(predicate).ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetDataByIdParent(long idParent, Expression<Func<T, bool>> predicate)
        {
            return new List<T>();
        }

        public virtual async Task<T> GetSingle(long id)
        {
            //return await Entities.SingleOrDefaultAsync(s => CommonMethods.ConvertToInt32(s.GetValue(Id)) == id);

            // nguyencuongcs - 20171106
            return await Context.FindAsync<T>(id);
        }

        public virtual async Task<T> GetSingle(Expression<Func<T, bool>> predicate)
        {
            return await Entities.FirstOrDefaultAsync(predicate);

            // nguyencuongcs - 20171106
            //return await Context.FindAsync<T>(predicate);
        }

        public virtual async Task<T> GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            try
            {
                T res = null;
                string includePropertyNameAsString = string.Empty;
                if (includeProperties != null)

                {
                    string temp = string.Empty;
                    foreach (var includeProperty in includeProperties)
                    {
                        try
                        {
                            temp = CommonMethods.ConvertToString(includeProperty.Body.GetValue("Member").GetValue("Name"));
                        }
                        catch
                        {
                            temp = string.Empty;
                        }

                        if (!includePropertyNameAsString.Contains(temp))
                        {
                            includePropertyNameAsString += string.IsNullOrEmpty(includePropertyNameAsString) ? temp : ("." + temp);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(includePropertyNameAsString))
                {
                    res = await Entities.Where(predicate).Include(includePropertyNameAsString).FirstOrDefaultAsync();

                    // nguyencuongcs - 20171106
                    //res = await (Context.FindAsync<T>(predicate) as DbSet<T>).Include(includePropertyNameAsString).FirstOrDefaultAsync();

                }
                else
                {
                    res = await Entities.Where(predicate).FirstOrDefaultAsync();

                    // nguyencuongcs - 20171106
                    //res = await (Context.FindAsync<T>(predicate) as DbSet<T>).FirstOrDefaultAsync();
                }

                return res;
            }
            catch (Exception ex)
            {
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: GetSingle", Variables.UrlFromClient + "-" + ex.Message, email);
                }
                return null;
            }
        }

        public virtual async Task<IEnumerable<T>> InsertReturnEntity(T entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                // process từ ngữ tục tiểu
                ProcessConvertObsceneToASCII(entity);

                int newId = await Insert(entity);

                // nguyencuongcs 20180813: chuyển vào trong hàm Insert return int
                /*if (newId > 0)
                {
                    await UpdateSeoPage(entity);
                    await ClearCacheByEntityName("InsertReturnEntity");
                }*/

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"InsertReturnEntity for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return new List<T>() { await GetSingle(newId) }.AsEnumerable();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: InsertReturnEntity", Variables.UrlFromClient + rawSql.ConnectionString + "-" + error + "(*)" + innerError, email);
                }
                return null;
            }
        }

        public virtual async Task<int> Insert(T entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                //string checkSchemaMaxLength = await Schema_CheckMaximumLength(entity);
                //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                //{
                //    throw new Exception(checkSchemaMaxLength);
                //}

                // process từ ngữ tục tiểu
                ProcessConvertObsceneToASCII(entity);

                SetIdAdminTao(entity);
                await Entities.AddAsync(entity);

                int res = -1;

                if (await this.Commit() > 0)
                {
                    Context.Entry<T>(entity).GetDatabaseValues();
                    res = CommonMethods.ConvertToInt32(entity.GetValue(PropertyParams.Id));

                    //await UpdateSeoPage(entity);
                    await ClearCacheByEntityName("Insert");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"Insert for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Insert", Variables.UrlFromClient + rawSql.ConnectionString +"-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> InsertWithouClearCache(T entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                //string checkSchemaMaxLength = await Schema_CheckMaximumLength(entity);
                //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                //{
                //    throw new Exception(checkSchemaMaxLength);
                //}

                // process từ ngữ tục tiểu
                ProcessConvertObsceneToASCII(entity);

                SetIdAdminTao(entity);
                await Entities.AddAsync(entity);

                int res = -1;

                if (await this.Commit() > 0)
                {
                    Context.Entry<T>(entity).GetDatabaseValues();
                    res = CommonMethods.ConvertToInt32(entity.GetValue(PropertyParams.Id));

                    //await UpdateSeoPage(entity);
                    //await ClearCacheByEntityName("Insert");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"Insert for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: InsertWithouClearCache", Variables.UrlFromClient + "-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> UpdateAfterAutoClone(T newEntity)
        {
            try
            {
                int res = -1;

                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (newEntity != null)
                {
                    //string checkSchemaMaxLength = await Schema_CheckMaximumLength(newEntity);
                    //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                    //{
                    //    throw new Exception(checkSchemaMaxLength);
                    //}

                    // process từ ngữ tục tiểu
                    ProcessConvertObsceneToASCII(newEntity);

                    int id = CommonMethods.ConvertToInt32(newEntity.GetValue(PropertyParams.Id));
                    T dbEntity = GetSingle(id).Result; // get db entity
                    if (dbEntity != null)
                    {
                        dbEntity = await CloneEntityToUpdate(dbEntity, newEntity); // update to dbentity
                        SetIdAdminCapNhat(dbEntity);
                        res = await this.Commit(); // await save to db

                        //await UpdateSeoPage(dbEntity);
                        await ClearCacheByEntityName("UpdateAfterAutoClone");
                    }
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"UpdateAfterAutoClone for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateAfterAutoClone", Variables.UrlFromClient + "(" + rawSql.ConnectionString + ")-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        // To update SoLuotXem => not clear cache
        public virtual async Task<int> UpdateAfterAutoCloneWithoutClearCache(T newEntity)
        {
            try
            {
                int res = -1;

                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (newEntity != null)
                {
                    //string checkSchemaMaxLength = await Schema_CheckMaximumLength(newEntity);
                    //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                    //{
                    //    throw new Exception(checkSchemaMaxLength);
                    //}

                    // process từ ngữ tục tiểu
                    ProcessConvertObsceneToASCII(newEntity);

                    int id = CommonMethods.ConvertToInt32(newEntity.GetValue(PropertyParams.Id));
                    T dbEntity = GetSingle(id).Result; // get db entity
                    if (dbEntity != null)
                    {
                        dbEntity = await CloneEntityToUpdate(dbEntity, newEntity); // update to dbentity
                        SetIdAdminCapNhat(dbEntity);
                        res = await this.Commit(); // await save to db
                    }
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"UpdateAfterAutoClone for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateAfterAutoCloneWithoutClearCache", Variables.UrlFromClient + "-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> UpdateWhere(Expression<Func<T, bool>> predicate, Action<T> updateVoid, bool setIdAdminCapNhat = true)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                IEnumerable<T> entities = await Entities.Where(predicate).ToListAsync();

                int res = 0;
                foreach (var entity in entities)
                {
                    //string checkSchemaMaxLength = await Schema_CheckMaximumLength(entity);
                    //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                    //{
                    //    throw new Exception(checkSchemaMaxLength);
                    //}
                    // Cập nhật dữ liệu mới vào entity
                    updateVoid(entity);

                    if (setIdAdminCapNhat)
                    {
                        SetIdAdminCapNhat(entity);
                    }
                    // Lưu xuống database
                    res += this.Commit().Result;// thay cho await UpdateAfterAutoClone(entity);

                    // Thường hàm này dùng để cập nhật các biến int như ThuTu, TrangThai -> ko cần gọi updateSeoPage làm chậm & k cần thiết & cũng ko có biến Seo để lấy data
                    //if (res > 0)
                    //{                    
                    //    await UpdateSeoPage(entity);
                    //}
                }

                if (res > 0)
                {
                    await ClearCacheByEntityName("UpdateWhere");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"UpdateWhere for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateWhere", Variables.UrlFromClient + "(" + rawSql.ConnectionString + ")-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> UpdateWhereWithoutClearCache(Expression<Func<T, bool>> predicate, Action<T> updateVoid, bool setIdAdminCapNhat = true)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                IEnumerable<T> entities = await Entities.Where(predicate).ToListAsync();

                int res = 0;
                foreach (var entity in entities)
                {
                    //string checkSchemaMaxLength = await Schema_CheckMaximumLength(entity);
                    //if (!string.IsNullOrEmpty(checkSchemaMaxLength))
                    //{
                    //    throw new Exception(checkSchemaMaxLength);
                    //}
                    // Cập nhật dữ liệu mới vào entity
                    updateVoid(entity);

                    if (setIdAdminCapNhat)
                    {
                        SetIdAdminCapNhat(entity);
                    }
                    // Lưu xuống database
                    res += this.Commit().Result;// thay cho await UpdateAfterAutoClone(entity);

                    // Thường hàm này dùng để cập nhật các biến int như ThuTu, TrangThai -> ko cần gọi updateSeoPage làm chậm & k cần thiết & cũng ko có biến Seo để lấy data
                    //if (res > 0)
                    //{                    
                    //    await UpdateSeoPage(entity);
                    //}
                }

                if (res > 0)
                {
                    //await ClearCacheByEntityName("UpdateWhere");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"UpdateWhere for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                //(Context as RaoVatContext_Custom).KillConnection();
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: UpdateWhereWithoutClearCache", Variables.UrlFromClient + "(" + rawSql.ConnectionString + ")-" + error + "(*)" + innerError, email);
                }
                return -1;
            }
        }

        public virtual async Task<T> CloneEntityToUpdate(T dbEntity, T newEntity)
        {
            try
            {
                if (dbEntity != null && newEntity != null)
                {
                    var properties = typeof(T).GetProperties();

                    if (properties != null)
                    {
                        object newPropValue;

                        foreach (var propInfo in properties)
                        {
                            if (!propertyKeys.Contains(propInfo.Name))
                            {
                                newPropValue = newEntity.GetValue(propInfo.Name);
                                if (dbEntity.HasProperty(propInfo.Name) && newPropValue != null)
                                {
                                    try
                                    {
                                        if (propInfo.IsDateTime())
                                        {
                                            newPropValue = CommonMethods.CheckMinSqlDateTime(propInfo, newPropValue);
                                        }
                                        dbEntity.SetValue(propInfo.Name, newPropValue);
                                    }
                                    catch (Exception ex)
                                    {
                                        string error = ex.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: CloneEntityToUpdate", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return null;
            }

            return dbEntity;
        }

        public virtual async Task<T> CloneEntityToUpdateFromDic<X>(T dbEntity, X sourceEntity)
        {
            try
            {
                if (dbEntity != null && sourceEntity != null)
                {
                    var properties = typeof(T).GetProperties();

                    if (properties != null)
                    {
                        object newPropValue;

                        foreach (var propInfo in properties)
                        {
                            if (!propertyKeys.Contains(propInfo.Name))
                            {
                                try
                                {
                                    newPropValue = sourceEntity.GetValue(propInfo.Name);
                                    if (dbEntity.HasProperty(propInfo.Name) && newPropValue != null)
                                    {
                                        if (propInfo.IsDateTime())
                                        {
                                            newPropValue = CommonMethods.CheckMinSqlDateTime(propInfo, newPropValue);
                                        }
                                        dbEntity.SetValue(propInfo.Name, newPropValue);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string error = ex.ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: CloneEntityToUpdateFromDic", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return null;
            }

            return dbEntity;
        }

        public virtual async Task<int> Delete(T entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                Entities.Remove(entity);
                int res = await this.Commit();

                if (res > 0)
                {
                    await ClearCacheByEntityName("Delete");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"Delete for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Delete", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> Delete(T[] entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                Entities.RemoveRange(entity);
                int res = await this.Commit();

                if (res > 0)
                {
                    await ClearCacheByEntityName("Delete Array");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"Delete Array for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Delete[T]", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> Delete(IEnumerable<T> entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                int res = await Delete(entity.ToArray());

                if (res > 0)
                {
                    await ClearCacheByEntityName("Delete IEnumerable");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"Delete IEnumerable for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Delete(IEnumerable<T> entity)", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> DeleteWhere(Expression<Func<T, bool>> predicate)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                IEnumerable<T> entities = await Entities.Where(predicate).ToListAsync();

                foreach (var entity in entities)
                {
                    Context.Entry<T>(entity).State = EntityState.Deleted;
                }

                int res = await this.Commit();
                if (res > 0)
                {
                    await ClearCacheByEntityName("DeleteWhere");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"DeleteWhere for params[{typeof(T).Name}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error:  DeleteWhere(Expression<Func<T, bool>> predicate)", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> DeleteMultiIds(string ids, char splitChar = ',')
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                List<int> lstId = CommonMethods.SplitStringToInt(ids, false);
                int res = await DeleteWhere(s => lstId.Contains(CommonMethods.ConvertToInt32(s.GetValue(PropertyParams.Id))));
                if (res > 0)
                {
                    await ClearCacheByEntityName("DeleteMultiIds by ids");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"DeleteMultiIds by string ids for params[{typeof(T).Name}, {ids}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error:  DeleteMultiIds(string ids, char splitChar = ',')", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        public virtual async Task<int> DeleteMultiIds(List<int> lstIds)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;

                int res = await DeleteWhere(s => lstIds.Contains(CommonMethods.ConvertToInt32(s.GetValue(PropertyParams.Id))));
                if (res > 0)
                {
                    await ClearCacheByEntityName("DeleteMultiIds by lstIds");
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                CommonMethods.WriteLogEntityBaseToDbTest($"DeleteMultiIds by lstIds for params[{typeof(T).Name}, {string.Join(",", lstIds.ToArray())}] : start at {start} - done in " + elapsedMs + " ms");

                return res;
            }
            catch (Exception ex)
            {
                RawSqlRepository rawSql = new RawSqlRepository(Context);
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error:  DeleteMultiIds(string ids, char splitChar = ',')", Variables.UrlFromClient + rawSql.ConnectionString + "-" + ex.Message, email);
                }
                return -1;
            }
        }

        private void KillConnection()
        {
            if (connectionDb == "raovat")
            {
                CommonMethods.WriteLog("Action raovat: KillConnection - TEST ");
                //(Context as ModelsRaoVat.RaoVatContext_Custom).KillConnection();
            }
            else
            {
                //(Context as raoVatHistoryApi.ModelsRaoVat.RaoVatContext_Custom).KillConnection();
            }
        }

        public virtual async Task<int> Commit()
        {
            int res = -1;
            //RawSqlRepository rawSql = new RawSqlRepository(Context);
            try
            {
                res = await Context.SaveChangesAsync();

                //KillConnection();

                // nguyencuongcs 20191108: ko kill connection thường xuyên như này vì sẽ giảm tốc độ. Chỉ kill khi nghi vấn có vấn đề ở catch bên dưới
                // kill connection idle
                // Tr test
                //await rawSql.KillConnection();
                //(Context as RaoVatContext_Custom).KillConnection();
            }
            catch (Exception ex)
            {
                //RawSqlRepository rawSql = new RawSqlRepository(Context);
                //rawSql.KillConnection();
                Console.WriteLine("ERROR COMMIT");
                string error = ex.Message;
                string innerError = ex.InnerException.Message;
                MailService ms = new MailService();
                foreach (var email in Variables.EmailSupportError_Name)
                {
                    ms.SendEmailToDev("Error: Commit", Variables.UrlFromClient  + "-" + error  + "(*)" + innerError, email);
                }

                GmailApiCore gac = new GmailApiCore();
                string thongBao = Variables.UrlFromClient + "[-]" + " báo lỗi: " + ex.ToString();
                gac.PostThongBaoHeThong_CommitDb(thongBao);

                CommonMethods.WriteLog(thongBao);
                //(Context as RaoVatContext_Custom).KillConnection();
                //rawSql.KillConnection();
                KillConnection();

                //ClearPool();
            }
            return res;
        }

        //public virtual async Task UpdateSeoPage(T entity)
        //{

        //}

        //public virtual async Task ClearPool()
        //{
        //    MailService ms = new MailService();
        //    try
        //    {
        //        (Context as RaoVatContext_Custom).ClearPool();
        //        foreach (var email in Variables.EmailSupportError_Name)
        //        {
        //            ms.SendEmailToDev("Success: ClearPool ", "", email);
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        foreach (var email in Variables.EmailSupportError_Name)
        //        {
        //            ms.SendEmailToDev("Error: ClearPool ", Variables.UrlFromClient + "-" + ex.Message, email);
        //        }
        //    }
        //}

        public virtual async Task<int> GetMaxId()
        {
            // nguyencuongcs 20180807: chưa test cách mới
            return CommonMethods.ConvertToInt32(await _entities.MaxAsync(s => s.GetValue(PropertyParams.Id)));
            //T entity = await _entities.OrderByDescending(P_Id).FirstOrDefaultAsync();
            //return entity != null ? CommonMethods.ConvertToInt32(entity.GetValue(P_Id)) : 0;
        }

        /* Extend methods */
        private void SetIdAdminTao(T entity)
        {
            if (Request != null && entity != null)
            {
                try
                {
                    entity.SetValue(PropertyParams.IdAdminTao, Request.GetTokenIdAdmin());

                    // nguyencuongcs 20180331: khi tạo thì lưu luôn NgayCapNhat để order theo NgayCapNhat cho đúng (vì những NgayCapNhat null sẽ được đưa lên đầu cho dù NgayCapNhat:asc
                    // đáp ứng được QUản lý files -> order mặc định theo NgayCapNhat:desc
                    SetIdAdminCapNhat(entity);
                }
                catch
                {

                }
            }
        }

        private void SetIdAdminCapNhat(T entity)
        {
            if (Request != null && entity != null)
            {
                try
                {
                    entity.SetValue(PropertyParams.IdAdminCapNhat, Request.GetTokenIdAdmin());
                    entity.SetValue(PropertyParams.NgayCapNhat, CommonMethods.GetServerDate(Context));
                }
                catch
                {

                }
            }
        }

        private async Task ClearCacheByEntityName(string callFrom)
        {
            if (EnabledAutoClearCacheAfterChanged)
            {
                await CacheController.ClearCacheAuto($"EntityBaseRepository: {callFrom}", typeof(T).Name, "");
            }
        }

        private async Task<string> Schema_CheckMaximumLength(T entity)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var start = DateTime.Now;
                // the code that you want to measure comes here

                string exceptionMessage = "";
                if (entity != null)
                {
                    var properties = typeof(T).GetProperties();

                    if (properties != null)
                    {
                        string newPropValue;

                        foreach (var propInfo in properties)
                        {
                            try
                            {
                                if (propInfo.IsString())
                                {
                                    newPropValue = CommonMethods.ConvertToString(entity.GetValue(propInfo.Name));
                                    if (!string.IsNullOrEmpty(newPropValue))
                                    {
                                        int maxLength = Schema_GetMaximumLength(typeof(T).Name, propInfo.Name);
                                        // Trường hợp maxLength = 0 có nghĩa propInfo.Name ko tồn tại trong schema, khóa ngoại tạo thêm
                                        if (maxLength > 0 && newPropValue.Length > maxLength)
                                        {
                                            exceptionMessage += string.IsNullOrEmpty(exceptionMessage) ? $"{propInfo.Name}({newPropValue.Length} ký tự) phải nhỏ hơn {maxLength} ký tự" : Variables.COMMON_KEY_SPLIT + " " + $"{propInfo.Name} phải nhỏ hơn {maxLength} ký tự";
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                string error = ex.ToString();
                            }
                        }
                    }
                }

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;

                CommonMethods.WriteLogPerformanceTest($"Schema_CheckMaximumLength {typeof(T).Name}: start at {start} - done in " + elapsedMs + " ms");

                return exceptionMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // entityName = tableName
        // columnName = propertyName
        private int Schema_GetMaximumLength(string entityName, string columnName)
        {
            int res = 0;
            if (Variables.Schema_ListColumnMaximumLengthFromDb != null && Variables.Schema_ListColumnMaximumLengthFromDb.Count() > 0)
            {
                var lstColumnByEntityName = Variables.Schema_ListColumnMaximumLengthFromDb.Where(s => s.GetValueFromKey_ReturnString(Variables.SchemaColumn_TableName, "") == entityName);
                if (lstColumnByEntityName != null && lstColumnByEntityName.Count() > 0)
                {
                    var column = lstColumnByEntityName.Where(s => s.GetValueFromKey_ReturnString(Variables.SchemaColumn_ColumnName, "") == columnName).FirstOrDefault();
                    if (column != null)
                    {
                        res = CommonMethods.ConvertToInt32(column.GetValueFromKey_ReturnInt32(Variables.SchemaColumn_CharacterMaximumLength));
                    }
                }
            }

            return res;
        }

        //private void KillConnection(string query, string connectionString)
        //{
        //    using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
        //    {
        //        try
        //        {
        //            connection.Open();
        //            NpgsqlCommand command = new NpgsqlCommand(query, connection);
        //            NpgsqlDataReader reader = command.ExecuteReader();
        //            while (reader.Read()) { }
        //            reader.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            CommonMethods.WriteLog(ex.ToString());
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        private static void ProcessConvertObsceneToASCII(T newEntity)
        {
            try
            {
                List<string> lstColumnContainString = CommonParams.lstColumnContainString;
                Type myType = newEntity.GetType();
                IEnumerable<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                foreach (var pro in props)
                {
                    if (lstColumnContainString.Contains(pro.Name))
                    {
                        string valueString = (newEntity.GetValue(pro.Name) ?? string.Empty).ToString();
                        valueString = CommonMethods.ConvertObsceneToASCII(valueString.Trim());
                        newEntity.SetValue(pro.Name, valueString);
                    }
                }
            }
            catch
            {

            }
        }
    }
}

