﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Reflection;
using System.Dynamic;
using System.Text;
using static Variables;
using raoVatApi.Common;
using raoVatApi.Models.Base;
using Microsoft.Extensions.Caching.Distributed;
using raoVatApi.Middlewares;

namespace raoVatApi.Models
{
    public class CoreTemplate
    {
        #region Variables

        private string _cachedFromCoreTemplate = "CachedFromCoreTemplate";
        private MemoryCacheControllerBase _cacheController;

        #endregion

        #region "Properties"

        private string _tableName = string.Empty;
        private string _id = string.Empty;
        private DbContext _context;
        private IDistributedCache _distributedCache;

        protected string TableName { get => _tableName; set => _tableName = value; }
        protected string Id { get => _id; set => _id = value; }
        protected DbContext Context { get => _context; set => _context = value; }
        protected IDistributedCache DistributedCache
        {
            get
            {
                // nguyencuongcs 20180627: đang nghi ngờ do cái này mà đơ khi clear all cache
                //if (_distributedCache == null)
                {
                    try
                    {
                        _distributedCache = DistributedCacheMiddleware.DistributedCache;
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                    }
                }

                return _distributedCache;
            }
            set => _distributedCache = value;
        }

        #endregion

        #region "Constructor"

        public CoreTemplate(DbContext context, string tableName, string idName = PropertyParams.Id)
        {
            Context = context;
            TableName = tableName;
            Id = idName;

            _cacheController = new MemoryCacheControllerBase();
        }

        #endregion

        #region "Common"

        public void CalPagination(int currentPage, int displayItems, ref int from, ref int to)
        {
            if (currentPage < 1 || displayItems < 1)
            {
                from = -1;
                to = -1;
                return;
            }

            from = displayItems * (currentPage - 1) + 1;
            to = displayItems * currentPage;
        }

        #endregion

        #region "Search"

        // nguyencuongcs 20180711
        // Giả sử condition = "Id = {0} AND IdMenu = {1}" thì thứ tự parameterValues phải đúng ở vị trí thứ 0 là giá trị của Id & thứ 2 là của IdMenu
        // Test performance trước cho Quản lý Files, sẽ nâng cấp tổng quát hơn nếu hiệu quả
        // arrOrderSql nên sử dụng khi order nhiều thuộc tính cùng lúc mà asc & desc khác nhau
        public async Task<IEnumerable<Dictionary<string, object>>> Search(bool useCache, int currentPage, int displayItems, RawSqlOrder[] arrOrderSql, string select, string condition, object[] parameterValues)
        {
            string textOrder = "";

            if (arrOrderSql != null && arrOrderSql.Length > 0)
            {
                // Ví dụ: ThuTu asc, Name desc
                foreach (RawSqlOrder item in arrOrderSql)
                {
                    textOrder += string.IsNullOrEmpty(textOrder) ? item.FullText : ("," + item.FullText);
                }
            }

            return await Search(useCache, currentPage, displayItems, textOrder, select, condition, parameterValues);
        }

        public async Task<IEnumerable<Dictionary<string, object>>> Search(bool useCache, int currentPage, int displayItems, string textOrder, string select, string condition, object[] parameterValues)
        {
            IEnumerable<Dictionary<string, object>> res = null;

            string tongHopBien = CommonMethods.CoalesceParams(KeyCacheParams.coalesceString, _cachedFromCoreTemplate, currentPage, displayItems, textOrder, select, condition);
            string keyCache = KeyCacheParams.BuildKeyCacheByVariables(TableName, tongHopBien);

            // Lấy data từ cache trước xem có không
            object cacheValue = null;
            if (useCache)
            {
                cacheValue = await _cacheController.GetCacheByKey(keyCache);                
            }

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<Dictionary<string, object>>>(cacheValue);
            }
            else
            {

                int from = 0;
                int to = 0;
                CalPagination(currentPage, displayItems, ref from, ref to);

                string strSql = "WITH ViewSplitPages AS ( SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowIndex, " + select + " FROM \"" + this.TableName + "\" {1} ) , GetTotalRow AS (SELECT MAX(RowIndex) AS TotalRow from ViewSplitPages) SELECT * FROM ViewSplitPages, GetTotalRow " + (from == -1 || to == -1 ? "" : " WHERE RowIndex BETWEEN " + from + " AND " + to);
                StringBuilder sbCondition = new StringBuilder();

                if (!string.IsNullOrEmpty(condition))
                {
                    if (sbCondition.Length > 0)
                    {
                        sbCondition.Append(" and (" + condition + ")");
                    }
                    else { sbCondition.Append(condition); }
                }

                if (sbCondition.Length > 0)
                {
                    sbCondition.Insert(0, " where ");
                }

                // nguyencuongcs 20180711: textOrder truyền dạng dynamic sẽ có dấu : => replace = khoảng trắng để đúng với sql
  
                List<string> newOrder = new List<string>();
                if (!string.IsNullOrEmpty(textOrder))
                {
                    //textOrder = textOrder.Replace(":", " ");
                    string[] listOrder = textOrder.Split(',');
                    foreach (var item in listOrder)
                    {
                        if (item.Contains(":"))
                        {
                            string[] listSeperation = item.Split(':');
                            newOrder.Add('"' + listSeperation[0] + '"' + " " + listSeperation[1]);
                        }
                        else
                        {
                            newOrder.Add('"' + item + '"');
                        }
                    }
                    textOrder = string.Join(',', newOrder);
                }

                strSql = string.Format(strSql, textOrder, sbCondition.ToString());

                CommonMethods.WriteLogCoreTemplate($"{TableName}: " + strSql);

                RawSqlRepository repo = new RawSqlRepository(Context);
                res = await repo.ExecuteSqlQuery_ReturnDic(strSql, parameterValues);

                await _cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return res;
        }

        public async Task<IEnumerable<T>> Search<T>(bool useCache, int currentPage, int displayItems, string textOrder, string select, string condition, object[] parameterValues)
            where T : IEntityBase
        {
            IEnumerable<T> res = null;

            string tongHopBien = CommonMethods.CoalesceParams(KeyCacheParams.coalesceString, _cachedFromCoreTemplate, currentPage, displayItems, textOrder, select, condition);
            string keyCache = KeyCacheParams.BuildKeyCacheByVariables(TableName, tongHopBien);

            // Lấy data từ cache trước xem có không
            object cacheValue = null;
            if (useCache)
            {
                cacheValue = await _cacheController.GetCacheByKey(keyCache);
            }
            
            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<T>>(cacheValue);
            }
            else
            {
                IEnumerable<Dictionary<string, object>> dicData = await Search(false, currentPage, displayItems, textOrder, select, condition, parameterValues);                
                res = dicData.ToObject<T>();
                await _cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return res;
        }

        #endregion

        /*
         * nguyencuongcs 20180731: thí điểm searchRowIndex theo Id IN(...), index sẽ động phụ thuộc vào điều kiện tìm kiếm & order
         */
        #region "SearchRowIndex"

        public async Task<IEnumerable<Dictionary<string, object>>> SearchRowIndex(bool useCache, int currentPage, int displayItems, string textOrder, string select, string condition, string conditionSearchRowIndex, object[] parameterValues)
        {
            IEnumerable<Dictionary<string, object>> res = null;

            string tongHopBien = CommonMethods.CoalesceParams(KeyCacheParams.coalesceString, _cachedFromCoreTemplate, currentPage, displayItems, textOrder, select, condition);
            string keyCache = KeyCacheParams.BuildKeyCacheByVariables(TableName, tongHopBien);

            // Lấy data từ cache trước xem có không
            object cacheValue = null;
            if (useCache)
            {
                cacheValue = await _cacheController.GetCacheByKey(keyCache);
            }

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<Dictionary<string, object>>>(cacheValue);
            }
            else
            {

                int from = 0;
                int to = 0;
                CalPagination(currentPage, displayItems, ref from, ref to);
                string strSql = "";

                if (string.IsNullOrEmpty(conditionSearchRowIndex))
                {
                    strSql = "WITH ViewSplitPages AS ( SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowIndex, " + select + " FROM \"" + this.TableName + "\" {1} ) , GetTotalRow AS (SELECT MAX(RowIndex) AS TotalRow from ViewSplitPages) SELECT * FROM ViewSplitPages, GetTotalRow " + (from == -1 || to == -1 ? "" : " WHERE RowIndex BETWEEN " + from + " AND " + to);
                }
                else
                {
                    if (!conditionSearchRowIndex.StartsWith("WHERE"))
                    {
                        conditionSearchRowIndex = $" WHERE {conditionSearchRowIndex}";
                    }
                    strSql = "WITH ViewSplitPages AS ( SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowIndex, " + select + " FROM \"" + this.TableName + "\" {1} ) , GetTotalRow AS (SELECT MAX(RowIndex) AS TotalRow from ViewSplitPages) SELECT * FROM ViewSplitPages, GetTotalRow " + conditionSearchRowIndex;
                }

                StringBuilder sbCondition = new StringBuilder();

                if (!string.IsNullOrEmpty(condition))
                {
                    if (sbCondition.Length > 0)
                    {
                        sbCondition.Append(" and (" + condition + ")");
                    }
                    else { sbCondition.Append(condition); }
                }

                if (sbCondition.Length > 0)
                {
                    sbCondition.Insert(0, " where ");
                }

                // nguyencuongcs 20180711: textOrder truyền dạng dynamic sẽ có dấu : => replace = khoảng trắng để đúng với sql
                List<string> newOrder = new List<string>();
                if (!string.IsNullOrEmpty(textOrder))
                {
                    //textOrder = textOrder.Replace(":", " ");
                    string[] listOrder = textOrder.Split(',');
                    foreach(var item in listOrder)
                    {
                        if(item.Contains(":"))
                        {
                            string[] listSeperation = item.Split(':');
                            newOrder.Add('"' + listSeperation[0] + '"' + " " + listSeperation[1]);
                        }
                        else
                        {
                            newOrder.Add('"' + item + '"');
                        }
                    }
                    textOrder = string.Join(',', newOrder);
                }

                strSql = string.Format(strSql, textOrder, sbCondition.ToString());

                CommonMethods.WriteLogCoreTemplate($"{TableName}: " + strSql);

                RawSqlRepository repo = new RawSqlRepository(Context);
                res = await repo.ExecuteSqlQuery_ReturnDic(strSql, parameterValues);

                await _cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return res;
        }

        public async Task<IEnumerable<Dictionary<string, object>>> SearchRowIndex_ReturnListByDisplayItem(bool useCache, int currentPage, int displayItems, string textOrder, string select, string condition, string conditionSearchRowIndex, object[] parameterValues)
        {
            // Hàm này ưu tiên conditionSearchRowIndex, nếu '' mới thực thi tính theo currentPage & displayItems
            IEnumerable<Dictionary<string, object>> lstRowIndex = await SearchRowIndex(useCache, currentPage, displayItems, textOrder, select, condition, conditionSearchRowIndex, parameterValues);
            
            IEnumerable<Dictionary<string, object>> res = null;

            // Tính page để load danh sách có chứa conditionSearchRowIndex
            if (displayItems > 0 && !string.IsNullOrEmpty(conditionSearchRowIndex))
            {
                Dictionary<string, object> dicRowIndex = lstRowIndex != null && lstRowIndex.Count() > 0 ? lstRowIndex.ElementAt(0) : new Dictionary<string, object>();
                int rowIndex = dicRowIndex.GetValueFromKey_ReturnInt32("RowIndex", -1);
                int displayPage = rowIndex % displayItems == 0 ? (rowIndex / displayItems) : ((rowIndex / displayItems) + 1);

                if(displayPage > 0)
                {
                    res = await SearchRowIndex(useCache, displayPage, displayItems, textOrder, select, condition, "", parameterValues);
                    
                    // Add displayPage vào record đầu tiên để UI update lại controls pagination đúng page có chứa conditionSearchRowIndex
                    if (res != null && res.Count() > 0)
                    {
                        res.ElementAt(0).Add(CommonParams.displayPage, displayPage);                        
                    }
                }
            }
            else
            {
                res = lstRowIndex;
            }

            return res;
        }

        public async Task<IEnumerable<T>> SearchRowIndex_ReturnListByDisplayItem<T>(bool useCache, int currentPage, int displayItems, string textOrder, string select, string condition, string conditionSearchRowIndex, object[] parameterValues)
            where T : IEntityBase
        {
            IEnumerable<T> res = null;

            string tongHopBien = CommonMethods.CoalesceParams(KeyCacheParams.coalesceString, _cachedFromCoreTemplate, currentPage, displayItems, textOrder, select, condition);
            string keyCache = KeyCacheParams.BuildKeyCacheByVariables(TableName, tongHopBien);

            // Lấy data từ cache trước xem có không
            object cacheValue = null;
            if (useCache)
            {
                cacheValue = await _cacheController.GetCacheByKey(keyCache);
            }

            if (cacheValue != null)
            {
                res = CommonMethods.DeserializeObject<IEnumerable<T>>(cacheValue);
            }
            else
            {
                IEnumerable<Dictionary<string, object>> dicData = await SearchRowIndex_ReturnListByDisplayItem(false, currentPage, displayItems, textOrder, select, condition, conditionSearchRowIndex, parameterValues);
                res = dicData.ToObject<T>();
                await _cacheController.AddCache(keyCache, CommonMethods.SerializeObject(res));
            }

            return res;
        }

        #endregion
    }
}