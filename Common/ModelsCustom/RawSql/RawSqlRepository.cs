using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using raoVatApi.ModelsCrm;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Reflection;
using System.Dynamic;
using System.Data.Common;
using System.Data;
using raoVatApi.Common;
using System.Data.SqlClient;
using Npgsql;
using raoVatApi.ModelsRaoVat;

namespace raoVatApi.Models
{
    public class RawSqlRepository
    {
        private DbContext _context;
        private DbConnection _con;
        private int _timeoutSecond = 15;
        public string ConnectionString = "";
        public string DatabaseName = "";

        public RawSqlRepository(DbContext context)
        {
            _context = context;
            // nguyencuongcs 20180726: phát hiện nhiều lệnh gây lỗi timeout nhưng khi thực thi trực tiếp dưới sql thì bình thường. Chưa hiểu
            // do tham chiếu vòng Id & ParentId (chưa biết tại sao) -> làm cho fnBuildFolderSortColumn & fnGetCapDoFolder bị lặp vô tận -> treo sql
            _context.Database.SetCommandTimeout(TimeSpan.FromSeconds(_timeoutSecond));

            // nguyencuongcs 20180803: ko dùng cái này nữa vì nó gắn với dbcontext map entity -> phải có db trùng tên với dbcontext:dlx
            //_con = _context.Database.GetDbConnection();

            // Chuyển sang dùng động theo cấu hình
            //_con = new SqlConnection(Variables.ConnectionString_RaoVat);
            //_con = new SqlConnection(Variables.ConnectionStringPostgresql_RaoVat);
            //_con = new NpgsqlConnection(Variables.ConnectionStringPostgresql_RaoVat_Slave);

            _con = new NpgsqlConnection(Variables.ConnectionStringPostgresql_RaoVat_Master);

            ConnectionString = _con.ConnectionString;
            DatabaseName = _con.Database;
        }

        public RawSqlRepository(DbContext context, bool readOnly)
        {
            _context = context;
            // nguyencuongcs 20180726: phát hiện nhiều lệnh gây lỗi timeout nhưng khi thực thi trực tiếp dưới sql thì bình thường. Chưa hiểu
            // do tham chiếu vòng Id & ParentId (chưa biết tại sao) -> làm cho fnBuildFolderSortColumn & fnGetCapDoFolder bị lặp vô tận -> treo sql
            _context.Database.SetCommandTimeout(TimeSpan.FromSeconds(_timeoutSecond));

            // nguyencuongcs 20180803: ko dùng cái này nữa vì nó gắn với dbcontext map entity -> phải có db trùng tên với dbcontext:dlx
            //_con = _context.Database.GetDbConnection();

            // Chuyển sang dùng động theo cấu hình
            //_con = new SqlConnection(Variables.ConnectionString_RaoVat);
            //_con = new SqlConnection(Variables.ConnectionStringPostgresql_RaoVat);
            //_con = new NpgsqlConnection(Variables.ConnectionStringPostgresql_RaoVat_Slave);

            // nguyencuongcs 20191108: tạm dùng RawSqlRepository có thêm biến readOnly để dùng cho các chỗ khởi tạo kết nối chỉ đọc
            // Tránh sửa nhiều chỗ, ko có time nên tách ra constructire riêng này
            readOnly = true;
            _con = new NpgsqlConnection(Variables.ConnectionStringPostgresql_RaoVat_Slave);

            ConnectionString = _con.ConnectionString;
            DatabaseName = _con.Database;
        }

        private void OpenConnect()
        {
            if (_con.State != ConnectionState.Open)
            {
                _con.Open();
            }
        }

        public async Task<string> ExecuteSqlQuery_ReturnString(string sql, params object[] parameters)
        {
            string res = "";

            try
            {
                OpenConnect();
                //using (var context = new CRMContext())
                {
                    //DbCommand command = _con.CreateCommand();
                    //command.CommandText = sql;
                    //command.CommandType = CommandType.Text;
                    //DbDataReader reader = command.ExecuteReader();

                    //DbCommand cmd = new DbCommand();
                    //cmd.Connection = _con;

                    RelationalDataReader data = await _context.Database.ExecuteSqlQueryAsync(sql, parameters: parameters);


                    var reader = data.DbDataReader;
                    while (reader.Read())
                    {
                        res = CommonMethods.ConvertToString(reader[0]);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                string strError = "";
                foreach (var item in parameters)
                {
                    strError += item + ",";
                }
                CommonMethods.WriteLogCoreTemplateError("Sql này gây ra lỗi: " + sql + strError);
                CommonMethods.WriteLogCoreTemplateError(error);
                _con.Close();
                _con.Dispose();
                throw;
            }
            finally
            {
                _con.Close();
                _con.Dispose();
            }

            return res;
        }

        public async Task<IEnumerable<T>> ExecuteSqlQuery<T>(string sql, params object[] parameters) where T : class, IEntityBase
        {
            List<T> res = new List<T>();

            try
            {
                OpenConnect();
                //using (var context = new CRMContext())
                {
                    //DbCommand command = _con.CreateCommand();
                    //command.CommandText = sql;
                    //command.CommandType = CommandType.Text;
                    //DbDataReader reader = command.ExecuteReader();
                    
                    //DbCommand cmd = new DbCommand();
                    //cmd.Connection = _con;

                    RelationalDataReader data = await _context.Database.ExecuteSqlQueryAsync(sql, parameters: parameters);
                    

                    var reader = data.DbDataReader;
                    while (reader.Read())
                    {
                        var entityProperties = typeof(T).GetProperties();
                        T getEntity = Activator.CreateInstance<T>();

                        if (entityProperties != null && getEntity != null)
                        {
                            foreach (PropertyInfo prop in entityProperties)
                            {
                                try
                                {
                                    getEntity.SetValue(prop.Name, reader[prop.Name]);
                                }
                                catch
                                {
                                    getEntity.SetValue(prop.Name, null);
                                }
                            }
                            res.Add(getEntity);
                        }
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                string strError = "";
                foreach (var item in parameters)
                {
                    strError += item + ",";
                }
                CommonMethods.WriteLogCoreTemplateError("Sql này gây ra lỗi: " + sql + strError);
                CommonMethods.WriteLogCoreTemplateError(error);
                _con.Close();
                _con.Dispose();
                throw;
            }
            finally{
                _con.Close();
                _con.Dispose();
            }

            return res.AsEnumerable();
        }

        public async Task<IEnumerable<DynamicDictionary>> ExecuteSqlQuery(string sql, params object[] parameters)
        {
            List<DynamicDictionary> res = new List<DynamicDictionary>();

            try
            {
                OpenConnect();
                //await KillConnection();
                //using (var context = new CRMContext())
                {
                    RelationalDataReader data = await _context.Database.ExecuteSqlQueryAsync(sql, parameters: parameters);

                    var reader = data.DbDataReader;
                    while (reader.Read())
                    {
                        string fieldName = string.Empty;
                        DynamicDictionary dynDic = new DynamicDictionary();

                        //IExtensibleDataObject

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            fieldName = reader.GetName(i);
                            dynDic.TrySetMember(fieldName, reader[fieldName]);
                        }

                        res.Add(dynDic);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                string strError = "";
                foreach (var item in parameters)
                {
                    strError += item + ",";
                }
                CommonMethods.WriteLogCoreTemplateError("Sql này gây ra lỗi: " + sql + strError);
                CommonMethods.WriteLogCoreTemplateError(error);
                _con.Close();
                _con.Dispose();
                throw;
            }
            finally
            {
                _con.Close();
                _con.Dispose();
            }

            return res;
        }

        public async Task<IEnumerable<Dictionary<string, object>>> ExecuteSqlQuery_ReturnDic(string sql, params object[] parameters)
        {
            List<Dictionary<string, object>> res = new List<Dictionary<string, object>>();

            try
            {
                OpenConnect();
                //using (var context = new CRMContext())
                {
                    RelationalDataReader data = parameters != null ? await _context.Database.ExecuteSqlQueryAsync(sql, parameters: parameters) : await _context.Database.ExecuteSqlQueryAsync(sql);

                    var reader = data.DbDataReader;
                    while (reader.Read())
                    {
                        string fieldName = string.Empty;
                        Dictionary<string, object> dic = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            fieldName = reader.GetName(i);
                            dic[fieldName] = Common.CommonMethods.ConvertToString(reader[fieldName]); //nguyencuongcs 20170210: add Common.CommonMethods.ConvertToString
                        }

                        res.Add(dic);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                string strError = "";
                foreach (var item in parameters)
                {
                    strError += item + ",";
                }
                CommonMethods.WriteLogCoreTemplateError("Sql này gây ra lỗi: " + sql + strError);
                CommonMethods.WriteLogCoreTemplateError(error);

                _con.Close();
                _con.Dispose();
                throw;
            }
            finally
            {
                _con.Close();
                _con.Dispose();
            }

            return res;
        }

        //public async Task KillConnection()
        //{
        //    //string sqlKillConnection = "exec prKillConnectionFailed";
        //    //string sqlKillConnection = CommonParams.postgresCallFunctionKillConnectionstring;
        //    //RawSqlRepository newRawSql = new RawSqlRepository(_context);
        //    //await newRawSql.ExecuteSqlQuery_ReturnString(sqlKillConnection);

        //    using (NpgsqlConnection connection = new NpgsqlConnection(ConnectionString))
        //    {
        //        MailService ms = new MailService();
        //        try
        //        {
        //            connection.Open();
        //            string query = CommonParams.postgresCallFunctionKillConnectionstring;
        //            NpgsqlCommand command = new NpgsqlCommand(query, connection);
        //            NpgsqlDataReader reader = command.ExecuteReader();
        //            while (reader.Read()) {
        //                reader.GetInt32(0);
        //            }
        //            reader.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            foreach (var email in Variables.EmailSupportError_Name)
        //            {
        //                ms.SendEmailToDev("Exception: KillConnection", Variables.UrlFromClient + "-" + ex.Message, email);
        //            }
        //            CommonMethods.WriteLog(ex.ToString());
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
    }
}