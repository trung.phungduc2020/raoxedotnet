using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using raoVatApi.Models;

namespace raoVatApi.Models.Base
{
    public interface IEntityBaseRepository_Int64<T> where T : class, IEntityBase_Int64
    {
        DbContext Context { get; set; }
        DbSet<T> Entities { get; set; }
        long TotalPages(long totalRows, int displayItems);
        Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> searchTerm, long currentPage, int displayItems, string orderString, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> Search(Expression<Func<T, bool>> predicate, string orderString, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<T> GetSingle(long id);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<long> Insert(T entity);
        Task<IEnumerable<T>> InsertReturnEntity(T entity);
        Task<long> UpdateAfterAutoClone(T newEntity);
        Task<T> CloneEntityToUpdate(T dbEntity, T newEntity);
        Task<long> UpdateWhere(Expression<Func<T, bool>> predicate, Action<T> updateVoid);
        Task<long> Delete(T entity);
        Task<long> DeleteWhere(Expression<Func<T, bool>> predicate);
        Task<long> DeleteMultiIds(string ids, char splitChar = ',');
        Task<long> DeleteMultiIds(List<long> lstIds);
        Task<long> Commit();
    }
}