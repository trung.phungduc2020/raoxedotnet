﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;
using raoVatApi.Response;
using raoVatApi.Common;
using raoVatApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Distributed;
using StackExchange.Redis;
using raoVatApi.Middlewares;
using Repository;
using StackExchange.Redis;

namespace raoVatApi.Models.Base
{
    public class MemoryCacheControllerBase : Controller
    {
        private bool _isClearingRedisDb = false;

        public MemoryCacheControllerBase()
        {
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddCache(string keyCache, string value, TimeSpan? expiry = null)
        {

            CustomResult cusres = new CustomResult();
            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    await RedisStore.RedisDatabase.StringSetAsync(keyCache, value, expiry); // Không Serialize to object nữa

                    // nguyencuongcs - 20171218: Xảy ra sau sự kiện _cache.Set nên cho dù có lỗi thì vẫn lưu được vào _cache
                    //Dictionary<string, object> body = new Dictionary<string, object>();
                    //body.Add(CommonParams.keyCache, keyCache);

                    // nguyencuongcs - 20180322: ko insert riêng lẻ & tự động vào Cache nữa, khi ddos sẽ bị nghẽn bảng Cache trước
                    // thêm api Chủ động Insert từ ui bằng Variables.CacheList
                    //await InsertKeyCache(body);

                    cusres.IntResult = 1;
                }
                catch (Exception ex)
                {
                    Variables.CacheHealthy = false;
                    cusres.SetException(ex, "AddCache error");
                }
            }

            return Response.OkObjectResult(cusres);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> IsExistedKeyCache()
        {
            CustomResult cusRes = new CustomResult();

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    IQueryCollection queryParams = Request.Query;
                    string keyCache = "";
                    if (queryParams != null)
                    {
                        keyCache = CommonMethods.ConvertToString(queryParams[CommonParams.keyCache]);
                    }

                    bool res = await IsExistedKeyCache(keyCache);
                    cusRes.StrResult = CommonMethods.ConvertToString(res);
                }
                catch (Exception ex)
                {
                    cusRes.SetException(ex);
                    Variables.CacheHealthy = false;
                }
            }

            return Response.OkObjectResult(cusRes);
        }

        public async Task<bool> IsExistedKeyCache(string keyCache)
        {
            object value = await GetCacheByKey(keyCache);
            return value != null;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetCacheByKey()
        {
            IQueryCollection queryParams = Request.Query;
            string keyCache = "";
            if (queryParams != null)
            {
                keyCache = CommonMethods.ConvertToString(queryParams[CommonParams.keyCache]);
            }

            return Ok(await GetCacheByKey(keyCache));
        }

        // HttpDelete sẽ ko có body nên dùng Put
        [HttpPut("[action]")]
        public async Task<IActionResult> ClearCacheByKey([FromBody]Dictionary<string, object> body)
        {
            CustomResult cusRes = new CustomResult();

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    string keyCache = "";
                    bool isContains = false;
                    bool isStartsWith = false;

                    //IQueryCollection queryParams = Request.Query;                
                    //if (queryParams != null)
                    //{
                    //    keyCache = CommonMethods.ConvertToString(queryParams[CommonParams.keyCache]);
                    //}

                    if (body != null)
                    {
                        keyCache = body.GetValueFromKey_ReturnString(CommonParams.keyCache, "");

                        isContains = CommonMethods.ConvertToBoolean(body.GetValueFromKey_ReturnString(CommonParams.isContains, ""));
                        isStartsWith = CommonMethods.ConvertToBoolean(body.GetValueFromKey_ReturnString(CommonParams.isStartsWith, ""));
                    }

                    cusRes.IntResult = await ClearCacheByKeyAndConditions(keyCache, isContains, isStartsWith);
                }
                catch (Exception ex)
                {
                    cusRes.SetException(ex);
                    Variables.CacheHealthy = false;
                }
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ClearCacheAuto()
        {
            string body = await Request.GetRawBodyStringAsync();
            CustomResult cusRes = new CustomResult();
            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    Dictionary<string, object> bodyJson = CommonMethods.DeserializeObject<Dictionary<string, object>>(body);
                    var body64 = CommonMethods.DecodeFrom64_UTF8(bodyJson.GetValueFromKey_ReturnString(CommonParams.token_data64, ""));
                    Dictionary<string, object> body64Json = CommonMethods.DeserializeObject<Dictionary<string, object>>(body64);

                    string entityName = body64Json.GetValueFromKey_ReturnString(CommonParams.entityName, "");
                    string id = body64Json.GetValueFromKey_ReturnString(CommonParams.id, "");

                    /*
                    string pageId = bodyJson.GetValueFromKey_ReturnString(CommonParams.pageId, "");
                    string pageLayoutId = bodyJson.GetValueFromKey_ReturnString(CommonParams.pageLayoutId, "");
                    string compDataId = bodyJson.GetValueFromKey_ReturnString(CommonParams.componentDataId, "");
                    string apiAction = bodyJson.GetValueFromKey_ReturnString(CommonParams.action, ""); // Ví dụ: action = SearchMenuByMaComponentData => phải tìm tìm EntityName đúng trong action để clear
                    */

                    // 
                    // Có thể dùng cái này để lọc IP web UI gọi tới ra khỏi lệnh broadcast
                    string callFrom = Request.HeaderOrigin();
                    await ClearCacheAuto(callFrom, entityName, id);
                }
                catch (Exception ex)
                {
                    cusRes.SetException(ex);
                    Variables.CacheHealthy = false;
                }
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ClearAllCache()
        {
            CustomResult cusRes = new CustomResult();

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    //var redisConfiguration = new RedisConfiguration()
                    //{
                    //    AbortOnConnectFail = true,
                    //    KeyPrefix = Variables.RedisServerName,
                    //    Hosts = new RedisHost[]
                    //    {
                    //        new RedisHost(){Host = "127.0.0.1", Port = 6379},
                    //        //new RedisHost(){Host = "192.168.0.11",  Port =6379},
                    //        //new RedisHost(){Host = "192.168.0.12",  Port =6379}
                    //    },
                    //    AllowAdmin = true,
                    //    ConnectTimeout = 3000,
                    //    Database = 0,
                    //    Ssl = false,
                    //    Password = "my_super_secret_password",
                    //    ServerEnumerationStrategy = new ServerEnumerationStrategy()
                    //    {
                    //        Mode = ServerEnumerationStrategy.ModeOptions.All,
                    //        TargetRole = ServerEnumerationStrategy.TargetRoleOptions.Any,
                    //        UnreachableServerAction = ServerEnumerationStrategy.UnreachableServerActionOptions.Throw
                    //    }
                    //};

                    //StackExchange.Redis.ConfigurationOptions stackRedisConfig = new StackExchange.Redis.ConfigurationOptions()
                    //{
                    //    AbortOnConnectFail = true,                        
                    //    AllowAdmin = true,
                    //    ConnectTimeout = 3000,                        
                    //    Ssl = false,
                    //    Password = "my_super_secret_password"                        
                    //}               
                    using (var _redis = StackExchange.Redis.ConnectionMultiplexer.Connect(Variables.RedisFullConnectionString))
                    {
                        var server = _redis.GetServer(Variables.RedisConnectionString);

                        _isClearingRedisDb = true;

                        Task clearAll = server.FlushDatabaseAsync(); // Hiện giờ chỉ xài 1 Database thứ 0 nên để mặc định                 
                                                                     //await server.FlushAllDatabasesAsync(StackExchange.Redis.CommandFlags.HighPriority);           
                        clearAll.Wait();

                        _isClearingRedisDb = false;

                        cusRes.IntResult = 1;

                        // Chờ xóa xong redis thì mới xóa cache ở web farms, tránh tình trạng redis chưa xóa xong đã bị web farms truy xuất => sẽ có khả năng lấy data chưa được xóa cache trong redis
                        ApiWebFarmDal webFarm = new ApiWebFarmDal();
                        await webFarm.ClearAllCache();
                    }
                }
                catch (Exception ex)
                {
                    cusRes.SetException(ex);
                    Variables.CacheHealthy = false;
                }
            }

            return Response.OkObjectResult(cusRes);
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> ClearAllWebFarmCache()
        {            
            CustomResult cusRes = new CustomResult();

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {

                    ApiWebFarmDal webFarm = new ApiWebFarmDal();
                    await webFarm.ClearAllCache();
                    cusRes.IntResult = 1;
                }
                catch (Exception ex)
                {
                    cusRes.SetException(ex);
                    Variables.CacheHealthy = false;
                }
            }

            return Response.OkObjectResult(cusRes);
        }

        public async Task<object> GetCacheByKey(string keyCache)
        {
            object valueCache = null;

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                // nguyencuongcs 20180611: khi code development thì ko trả về cache để dễ test
                //if (Variables.Environment == "appsettings.Production.json")
                {
                    // nguyencuongcs 20180330: try catch ở đây để nếu Redis ko start thành công thì hệ thống vẫn chạy bình thường ko thông qua cache
                    try
                    {
                        // nguyencuongcs 20180919: update lên StackExchange Redis(tránh tình trạng chạy await Miscrosoft redis bị hang on), ko tự + serverName ở đầu nữa
                        //if (keyCache.StartsWith(Variables.RedisServerName))
                        //{
                        //    // Remove Variables.RedisServerName trước khi tìm vì trong hàm Cache.GetStringAsync đã tự add thêm RedisServerName do định nghĩa cấu hình chung redis ở startup
                        //    // Nêu ko remove sẽ bị dư và ko khớp key => ko lấy được data
                        //    keyCache = keyCache.Replace(Variables.RedisServerName, "");
                        //}
                        var redisValue = await RedisStore.RedisDatabase.StringGetAsync(keyCache); // Không cân deserialed ở đây, chỗ nào gọi ra tự hiểu cấu trúc để deserialed cho đúng mục đích
                        valueCache = redisValue.HasValue ? redisValue.ToString() : null;
                    }
                    catch (Exception ex)
                    {
                        Variables.CacheHealthy = false;
                        CommonMethods.WriteLog("GetCacheByKey error:" + ex, ToString());
                    }
                }
            }

            return valueCache;
        }

        public async Task<int> ClearCacheByKeyAndConditions(string inputTemplate, bool isContains, bool isStartsWith)
        {
            int res = 0;
            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    inputTemplate = inputTemplate.ToLower().Replace("{0}", "");
                    string[] arrTemplate = inputTemplate.Split(KeyCacheParams.splitKeyCacheChar);
                    if (arrTemplate != null && arrTemplate.Count() > 0)
                    {
                        // Select các key contain keycache
                        string pattern = "";
                        foreach (string template in arrTemplate)
                        {
                            // Tránh tình trạng vô tình template = '' sẽ clear tất cả, nếu muốn clear tất cả phải dùng ClearAllCache
                            if (!string.IsNullOrEmpty(template))
                            {
                                if (isContains)
                                {
                                    pattern = "*" + template + "*";
                                }
                                else if (isStartsWith)
                                {
                                    pattern = template + "*";
                                }
                                else
                                {
                                    pattern = template;
                                }

                                // Get keys in redis
                                var redisKeys = RedisStore.RedisServer.Keys(pattern: pattern);
                                if (redisKeys != null)
                                {
                                    string strKey = "";
                                    foreach (var item in redisKeys)
                                    {
                                        strKey = item.ToString();
                                        await RedisStore.RedisDatabase.KeyDeleteAsync(strKey); // Xóa trong redis db                                                                        
                                        res++;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CommonMethods.WriteLog(ex.ToString());
                    Variables.CacheHealthy = false;
                }
            }

            return res;
        }

        public async Task<int> ClearCacheByEntityName(string entityName, string keyCacheVariables = "", string paramData = "")
        {
            int res = 0;

            if (Variables.CacheHealthy && !_isClearingRedisDb)
            {
                try
                {
                    entityName = entityName.ToLower();
                    string[] arrEntityName = entityName.Split(",");
                    int countEntityName = arrEntityName != null ? arrEntityName.Length : 0;

                    // Nếu xóa theo variables thì chỉ clear 1 entityName 1 lúc, tránh cắt theo dấu "," lỗi vì trong paramData có thể chứa dấu ","
                    // Dùng để clear theo ById | ByParams | ...
                    if (!string.IsNullOrEmpty(keyCacheVariables) && !string.IsNullOrEmpty(paramData) && countEntityName == 1)
                    {
                        keyCacheVariables = keyCacheVariables.ToLower();
                        paramData = paramData.ToLower();

                        string keyCache = KeyCacheParams.BuildKeyCacheByVariables(entityName, keyCacheVariables);
                        keyCache = string.Format(keyCache, paramData);

                        await ClearCacheByKeyAndConditions(keyCache, true, false);
                    }
                    else
                    {
                        if (arrEntityName != null && arrEntityName.Length > 0)
                        {
                            // Xử lý khi UI tự động detect ComponentData (hoặc NewComponentData, nếu update db bỏ chữ New thì khỏe) khóa ngooại. Ví dụ: ComponentDataBaiViet, ComponentDataTinTuc...
                            // Cắt tự động entity đi kèm để clear cache vì đang lưu keyCache cho các entity lấy theo compData bằng cách <tbl>entityName</tbl>byIdComponentData:{0}
                            string strComponentData = "componentdata";
                            string strPageLayout = "pagelayout";
                            string strPageLayoutEntityName = "newpagelayout";

                            if (entityName.Contains(strComponentData))
                            {
                                string foreignEntity = "";
                                int indexOfComponentData = 0;
                                List<string> lstNewItem = new List<string>();
                                foreach (string item in arrEntityName)
                                {
                                    try
                                    {
                                        indexOfComponentData = item.LastIndexOf(strComponentData) + strComponentData.Length;
                                        foreignEntity = item.Substring(indexOfComponentData, item.Length - indexOfComponentData);

                                        // Build dạng <tbl>TinTuc</tbl>byidcomponentdata để xóa TinTuc đi theo componentdata thôi thay vì toàn bộ TinTuc
                                        // foreignEntity = KeyCacheParams.BuildKeyCacheByVariables(foreignEntity, KeyCacheParams.ByIdComponentDataWithoutParams);
                                        // Phải clear theo entity vì đang cache toàn bộ ở EntityBaseRepo

                                        lstNewItem.Add(foreignEntity); // Hiện tại chỉ cần tự clear cache cho entity đi kèm, ko cần clear ComponentData(hoặc NewComponentData) vì sẽ làm chậm hệ thống và ko lưu data khóa ngooại theo dạng <tbl>ComponentData</tbl>
                                    }
                                    catch (Exception ex)
                                    {
                                        string error = ex.ToString();
                                    }
                                }

                                lstNewItem.AddRange(arrEntityName);
                                arrEntityName = lstNewItem.ToArray();
                            }

                            if (entityName.Contains(strPageLayout))
                            {
                                List<string> lstNewItem = new List<string>();
                                foreach (string item in arrEntityName)
                                {
                                    try
                                    {
                                        // Xóa cache PageLaoyout khi cập nhật PageLayoutDataComponent
                                        lstNewItem.Add(strPageLayoutEntityName);
                                    }
                                    catch (Exception ex)
                                    {
                                        string error = ex.ToString();
                                    }
                                }

                                lstNewItem.AddRange(arrEntityName);
                                arrEntityName = lstNewItem.ToArray();
                            }

                            string formatItemTable = "";
                            foreach (string item in arrEntityName)
                            {
                                if (item.Contains(KeyCacheParams.tagTableOpen) || item.Contains(KeyCacheParams.tagTableClose))
                                {
                                    formatItemTable = item;
                                }
                                else
                                {
                                    formatItemTable = KeyCacheParams.tagTableOpen + item + KeyCacheParams.tagTableClose;
                                }
                                await ClearCacheByKeyAndConditions(formatItemTable, true, false);
                            }
                        }
                    }

                    res = 1;
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    Variables.CacheHealthy = false;
                }
            }

            return res;
        }

        public async Task<int> ClearCacheAuto(string callFrom, string entityName, string id)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var start = DateTime.Now;

            int res = 0;

            // Nếu muốn clear theo điều kiện variables thì chỉ truyền 1 entityName 1 lúc
            string keyCacheVariables = "";
            if (!string.IsNullOrEmpty(id))
            {
                keyCacheVariables = KeyCacheParams.ById;
            }

            // nguyencuongcs 20180626: tạm thời tắt clear theo variables byId
            id = "";
            await ClearCacheByEntityName(entityName, keyCacheVariables, id);

            /*
             * nguyencuongcs 20180710
             * Nếu muốn tăng tốc giao diện, có thể sau mỗi lần clear cache thì tự gọi hàm để load lại cache
            var objReloadCache = bodyJson[CommonParams.reloadCache]; 
            */

            /*
             Broadcast tới các web server trong farms
            */
            ApiWebFarmDal webFarm = new ApiWebFarmDal();
            webFarm.ClearCacheAuto(callFrom, entityName, id);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            CommonMethods.WriteLogPerformanceTest($"ClearCacheAuto callFrom {callFrom} for params[{entityName}, {id}] : start at {start} - done in " + elapsedMs + " ms");
            res = 1;
            return res;
        }
    }
}